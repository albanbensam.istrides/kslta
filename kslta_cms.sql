-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 15, 2020 at 01:13 PM
-- Server version: 5.7.32
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kslta545_ksltacms`
--

-- --------------------------------------------------------

--
-- Table structure for table `adminusers`
--

CREATE TABLE `adminusers` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `rights` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `affilatedclubs`
--

CREATE TABLE `affilatedclubs` (
  `id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `address1` varchar(100) NOT NULL,
  `address2` varchar(100) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `fax` varchar(100) NOT NULL,
  `email` varchar(300) NOT NULL,
  `website` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `affilatedclubs`
--

INSERT INTO `affilatedclubs` (`id`, `state_id`, `name`, `address1`, `address2`, `mobile`, `phone`, `fax`, `email`, `website`) VALUES
(2, 17, 'BHADRA SPORTS CLUB', 'Club Road, Balehonnur-577112, Karnataka', '', '08266-250405', '', '', 'bhadraclubinfo@gmail.com', 'www.bhadrasportsclub.com'),
(3, 17, 'PLANTERS CLUB', 'JPost Box No.7, SAKLESHPUR - 573134', '', '08173-230279 / 230158  08173-230246', '', '', 'plantersclubsakleshpur@rediffmail.com', 'www.plantersclub.in'),
(4, 17, 'TUMKUR CLUB', 'Gandhinagar, Tumkur-572102', '', '0816-2278366', '', '', 'tumkurclub@gmail.com', 'www.tumkurclub.in'),
(5, 17, 'BELUR PLANTER\'S CLUB', 'Post Box No-4, Belur - 573115, Hassan District', '', '9880872892 / 9448070104', '', '', '', ''),
(6, 17, 'COTTON COUNTY RESORTS & ESTATES PVT LTD', 'Opp.To Airport, Gokul Road, Hubli-30', '', '0836-6563466 / 2335799  0484-2381596', '', '', 'cottoncountyclub@gmail.com', 'www.cottoncountyclub.com'),
(7, 17, 'MUDIGERE CLUB', 'Post Box No.28. Mudigere-577132, Chickamagalur District', '', '08263-220191', '', '', 'mudigereclub@yahoo.com', ''),
(8, 17, 'THE PUTTUR CLUB', 'P.O.Darbe, Puttur D.K.574 202', '', '9980410423 / 9448277385', '', '', 'putturclub@gmail.com', 'www.theputturclub.com'),
(9, 17, 'MALNAD CLUB', 'Balebylu, Agumbe Road, Thirthahalli - 577432 Shivamogga Dist', '', '08181-228220 / 220220', '', '', 'malnadclubth@gmail.com', 'www.themalnadclub.com'),
(10, 18, 'THE TRIVANDRUM TENNIS CLUB', '  Kowdiar P.o. Trivandru', '', '0471-2722737 / 592 / 2310832  ', '0471-2727475', '0471-2727475', 'ttctvm@gmail.com', 'www.ttc.org.in'),
(11, 18, 'LOTUS CLUB ', ' Waarruam Road, Ernakulam, Cochin-68201', '', '04847-2352456 / 2366737', '', '', 'lotusclubcoachin@yahoo.co.in', '  www.lotusclub.in'),
(12, 18, 'COCHIN SUBURBAN CLUB  ', 'Thrikkakara P.O, Cochin-6820', '', '0484-2575871 / 2576571', '', '', 'cochinsuburbanclub1980@gmail.com', 'www.cochinsuburbanclub.com'),
(13, 21, 'UNITED SERVICE CLUB  ', 'Robert Road, Near R C Church, Colaba, Mumbai - 400005', '', '022-22150881, 22151480  ', '022-22150881, 22160139', '', 'admin@usclub.co.in', '  www.usclub.co.in'),
(14, 21, 'ROYAL CONNAUGHT BOAT CLUB  ', '7/8, Boat Club Road, Pune-411001', '', '', '020-26163512 / 13 / 14  020-26163511', '', 'info@boatclubpune.com', '  www.boatclubpune.com'),
(15, 29, 'JAISAL CLUB', 'Jethwai Road,Jaisalmer-345001', '', '02992-255555, 254999  0291-2435349', '', '', 'jaisalclub24x7@gmail.com', '  www.jaisalclub.com'),
(16, 29, 'FIELD CLUB', 'Fatehpura, Udaipur-313001', '', '0294-24161499 / 2560105  0294 - 2421312', '0294-241614', '', 'fieldclubindia@fieldclubindia.com', 'www.fieldclubindia.com'),
(17, 2, 'I.B.P CENTURY CLUB', 'D.No.15-3-15, Maharanipeta, Visakhapatnam-530002', '', '0891-2562573 / 2567471  0891-2714102', '', '', 'secretary@ibpcenturyclub.in', 'www.ibpcenturyclub.in'),
(18, 31, 'THE KERALA CLUB COIMBATORE', '78-80,A.T.T.Colony, Coimbatore-641018', '', '', '2216231 / 2215178 / 2213642', '', 'thekeralaclub@gmail.com', ''),
(19, 17, 'THE COSMOPOLITAN CLUB', 'Dr.Radhakrishna Avenue,\r\nChamarajapuram\r\nMysore-570005  \r\n', '', '', 'Ph : 0821-2423881', '', 'ccclub_mys@yahoo.co.in', 'www.cosmomysore.com'),
(20, 17, 'COSMOPOLITAN CLUB', 'K.R.Extension, B.H.Road\r\nTiptur-572202\r\n\r\n', '', '', '08134-251140', '', '', ''),
(21, 17, 'THE HERITAGE CLUB', '#816/274, \'C\' Block\r\nVijayanagar 3rd Stage,Mysuru \r\n', '', '', '0821-2412377', '', 'heritageclubmys@gmail.com', 'www.heritageclubmys.in'),
(22, 36, 'CALCUTTA ROWING CLUB', '15, Rabindra Sarobar,\r\n\r\nKolkata-700029\r\n', '', '', '2419-8915 /  2419-8914', '', 'hony.secy@calcuttarowingclub.com', 'www.calcuttarowingclub.co.in'),
(23, 17, 'PAVANPUTRA SPORTS & CULTURAL CLUB', 'Plot No.114,116,117\r\nKIADB Industrial Area,\r\nH.N.Pura Road, Hassan - 573201\r\n', '', '', '(08172) 243391 / 92 / 93', '', '', 'www.pavanputrasportsclub.com'),
(24, 21, 'DADAR CLUB', 'Lokmanya Tilak Colony\r\nLane No.3, Dadar (East)\r\nMumbai - 400014\r\n', '', '', '022-24143627 / 24150590 / 24154282', '', 'admin@dadarclub.com', 'www.dadarclub.com'),
(25, 17, 'WESTIND COUNTRY CLUB', '31A, Shivalloy Village,\r\nAlevoor Road', 'Manipal-576104', 'Mob : 9535829980', '0820-2573812', '', 'westindclubmanipal@gmail.com', 'www.wccmanipal.club'),
(26, 29, 'JODHPUR CLUB', '774, 1st Flr, \'Narayan\'\r\nNear Bafna Hospital,', '5th Chopasani Road\r\nJodhpur-342003', '', '0291-2611555', '', 'info@jodhpurclub.com', 'www.jodhpurclub.com'),
(27, 28, 'CHANDIGARH CLUB LTD', 'Sector-1, Chandigarh - 160001\r\n', '', '', '0172-2743388, 2740144', '0172- 2742326', 'chandigarhclubltd@gmail.com', 'www.chandigarhclubltd.com'),
(28, 12, 'SUNCITY CLUB & RESORT - VADODARA', 'Gotri-Sherkhi Road, Vadodara-391330\r\n', '', '', '0265-6888881/ 82/83/84', '', 'info@suncityclub.in', 'www.suncityclub.in'),
(29, 17, 'KANARA CLUB', '15-11-633, Kadri, Mangaluru\r\n', 'D.K.District, Karnataka-575002\r\n', '', '0824 - 2214191 / 2211086', '0824-4266158', 'kanaraclub@gmail.com', 'www.thekanaraclub.com'),
(30, 18, 'BEKAL CLUB', 'Near Padannakad ROB, NH-66, Padannakas P.O\r\n', 'Kanhangad, Kasaragod Dist,Kerala-671314\r\n', '0999582406', '0467-2204609 ', '', 'bekalclub@gmail.com', 'www.bekalclub.com'),
(31, 11, 'CLUBE TENNIS DE GASPAR DIAS', 'Dr. Jack Sequeira Road\r\n\r\n', 'Miramar, Panaji-403001, Goa', '', ' 0832- 2462220 / 2462221', '', 'gm@clubegaspardias.com', 'www.clubegaspardias.com'),
(32, 31, 'NANI\'S NOOK', 'Yellanahalli - Mynalai Road, Ketti\r\nYellanahalli Post, Coonoor Taluk,\r\n', 'Nilgiris,Ooty,Tamilnadu-643243\r\n', '9886672582', '', '', 'santosh.k@nanisnook.club', 'www.nanisnook.club'),
(33, 18, 'AQUATICS CLUB', 'Aristo Road, Mission Quarters,\r\nThrissur, Kerala\r\n\r\n', '', '', '0487-2428824', '', 'aquaticsclub@gmail.com', 'www.aquaticsclub.com'),
(34, 17, 'KODAGU PLANTERS CLUB', 'Post Box No.17,\r\n', 'Gonikoppal-571213\r\n', '9480743373', '08274-247707', '', 'kodaguplantersclub@gmail.com', '');

-- --------------------------------------------------------

--
-- Table structure for table `dropdown_management`
--

CREATE TABLE `dropdown_management` (
  `dropdown_id` bigint(20) NOT NULL,
  `dropdown_key` varchar(50) NOT NULL,
  `dropdown_value` varchar(500) NOT NULL,
  `dropdown_order` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dropdown_management`
--

INSERT INTO `dropdown_management` (`dropdown_id`, `dropdown_key`, `dropdown_value`, `dropdown_order`) VALUES
(1, 'tourmament_event_category', 'Boys', 1),
(2, 'tourmament_event_category', 'Girls', 2),
(3, 'tourmament_event_category', 'Boys Doubles', 3),
(4, 'tourmament_event_category', 'Girls Doubles', 4),
(5, 'tourmament_event_category', 'Mens', 5),
(6, 'tourmament_event_category', 'Womens', 6),
(7, 'tourmament_event_category', 'Mens Doubles', 7),
(8, 'tourmament_event_category', 'Womens Doubles', 8),
(9, 'tourmament_event_subcategory', 'Under 10', 1),
(10, 'tourmament_event_subcategory', 'Under 12', 2),
(11, 'tourmament_event_subcategory', 'Under 14', 3),
(12, 'tourmament_event_subcategory', 'Under 18', 5),
(13, 'tourmament_event_subcategory', 'Mens', 5),
(14, 'tourmament_event_subcategory', 'Womens', 6),
(15, 'tourmament_event_subcategory', 'ITF', 7),
(16, 'tourmament_event_subcategory', 'WTA', 8),
(17, 'tourmament_event_subcategory', 'DAVIS CUP', 9),
(18, 'page_display_tag', 'Home', 1),
(19, 'page_display_tag', 'News', 2),
(20, 'page_display_tag', 'Committee', 3),
(21, 'page_display_tag', 'Tournaments', 4),
(22, 'page_display_tag', 'Coaching', 5),
(23, 'page_display_tag', 'Calander', 6),
(24, 'page_display_tag', 'Events', 7),
(25, 'page_display_tag', 'Facilities', 8),
(26, 'page_display_tag', 'Contact', 9),
(29, 'tourmament_event_subcategory', 'Under 16', 4);

-- --------------------------------------------------------

--
-- Table structure for table `filesupload`
--

CREATE TABLE `filesupload` (
  `id` int(100) NOT NULL,
  `filename` varchar(100) NOT NULL,
  `filelink` varchar(1000) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `filesupload`
--

INSERT INTO `filesupload` (`id`, `filename`, `filelink`, `created_at`, `updated_at`) VALUES
(43, 'home-bg', 'fileupload/home-bg.jpg', '2017-04-08 00:00:00', '2017-04-08 15:16:00'),
(44, 'csu-14', 'fileupload/csu-14.pdf', '2017-05-12 00:00:00', '2017-05-12 13:31:03'),
(45, 'crsgu-14', 'fileupload/crsgu-14.pdf', '2017-05-12 00:00:00', '2017-05-12 18:30:00'),
(46, 'crsu-12b', 'fileupload/crsu-12b.pdf', '2017-06-02 00:00:00', '2017-06-02 14:14:05'),
(47, 'crsu-12g', 'fileupload/crsu-12g.pdf', '2017-06-02 00:00:00', '2017-06-02 14:20:53'),
(48, 'crsu-12gd', 'fileupload/crsu-12gd.pdf', '2017-06-03 00:00:00', '2017-06-03 09:13:43'),
(51, 'CS Girls U-12', 'fileupload/CS Girls U-12.pdf', '2017-08-16 00:00:00', '2017-08-28 18:30:00'),
(52, 'CS Boys U-12', 'fileupload/CS Boys U-12.pdf', '2017-08-16 00:00:00', '2017-08-28 18:30:00'),
(58, 'CSGirls-12', 'fileupload/CSGirls-12.pdf', '2017-09-02 00:00:00', '2017-09-02 08:17:11'),
(59, 'CSBoys-12', 'fileupload/CSBoys-12.pdf', '2017-09-02 00:00:00', '2017-09-02 12:19:04'),
(64, 'Bgdoublesdraw', 'fileupload/Bgdoublesdraw.pdf', '2017-09-04 00:00:00', '2017-09-04 08:47:28'),
(77, 'Boyscs12', 'fileupload/Boyscs12.pdf', '2017-09-07 00:00:00', '2017-09-07 10:35:10'),
(80, 'Entry_Form_Players_Wheel_Chair', 'fileupload/Entry_Form_Players_Wheel_Chair.pdf', '2017-10-28 00:00:00', '2017-10-27 18:30:00'),
(81, 'Fact-Sheet-IWTT-Tabebuia-open-2017', 'fileupload/Fact-Sheet-IWTT-Tabebuia-open-2017.pdf', '2017-10-28 00:00:00', '2017-10-28 16:30:01'),
(82, 'banner-web', 'fileupload/banner-web.jpg', '2017-11-17 00:00:00', '2017-11-17 09:03:57'),
(83, 'banner-web-ticket', 'fileupload/banner-web-ticket.jpg', '2017-11-17 00:00:00', '2017-11-17 09:04:47'),
(84, 'OP ATP SAT', 'fileupload/OP ATP SAT.pdf', '2017-11-18 00:00:00', '2017-11-18 08:31:13'),
(85, 'ATP Draw', 'fileupload/ATP Draw.pdf', '2017-11-18 00:00:00', '2017-11-18 08:37:21'),
(86, 'ATP Sun', 'fileupload/ATP Sun.pdf', '2017-11-18 00:00:00', '2017-11-18 17:21:11'),
(87, 'ATP Sun Draw', 'fileupload/ATP Sun Draw.pdf', '2017-11-18 00:00:00', '2017-11-18 17:23:18'),
(88, 'ATP Sun Draw', 'fileupload/ATP Sun Draw.pdf', '2017-11-19 00:00:00', '2017-11-19 08:43:32'),
(89, 'ATP Sun Draw', 'fileupload/ATP Sun Draw.pdf', '2017-11-19 00:00:00', '2017-11-19 09:01:39'),
(90, 'ATP Sun OP', 'fileupload/ATP Sun OP.pdf', '2017-11-19 00:00:00', '2017-11-19 09:02:44'),
(91, 'ATP Mon OP', 'fileupload/ATP Mon OP.pdf', '2017-11-19 00:00:00', '2017-11-19 11:02:19'),
(92, 'ATP Tue OP', 'fileupload/ATP Tue OP.pdf', '2017-11-20 00:00:00', '2017-11-20 16:27:08'),
(93, 'ATP Mon MDS', 'fileupload/ATP Mon MDS.pdf', '2017-11-21 00:00:00', '2017-11-21 06:25:29'),
(94, 'ATP Mon MDD', 'fileupload/ATP Mon MDD.pdf', '2017-11-21 00:00:00', '2017-11-21 06:26:55'),
(96, 'ATP Tue OP', 'fileupload/ATP Tue OP.pdf', '2017-11-21 00:00:00', '2017-11-21 16:04:36'),
(102, 'ATP Wed MDS', 'fileupload/ATP Wed MDS.pdf', '2017-11-21 00:00:00', '2017-11-21 16:21:37'),
(103, 'ATP  Wed OP', 'fileupload/ATP  Wed OP.pdf', '2017-11-21 00:00:00', '2017-11-21 16:24:02'),
(104, 'Bangalore-Open-2017-OP-22nd-Nov', 'fileupload/Bangalore-Open-2017-OP-22nd-Nov.pdf', '2017-11-21 00:00:00', '2017-11-21 16:30:11'),
(105, 'ATP Thu OP', 'fileupload/ATP Thu OP.pdf', '2017-11-22 00:00:00', '2017-11-22 14:50:34'),
(106, 'ATP Thu MDS', 'fileupload/ATP Thu MDS.pdf', '2017-11-22 00:00:00', '2017-11-22 15:47:36'),
(107, 'ATP Mon MDD', 'fileupload/ATP Mon MDD.pdf', '2017-11-22 00:00:00', '2017-11-22 15:48:20'),
(108, 'ATP Thu MDD', 'fileupload/ATP Thu MDD.pdf', '2017-11-22 00:00:00', '2017-11-22 16:18:11'),
(109, 'ATP Fri OP', 'fileupload/ATP Fri OP.pdf', '2017-11-23 00:00:00', '2017-11-23 17:28:05'),
(110, 'ATP Fri MDS', 'fileupload/ATP Fri MDS.pdf', '2017-11-23 00:00:00', '2017-11-23 17:28:47'),
(111, 'ATP Fri MDD', 'fileupload/ATP Fri MDD.pdf', '2017-11-23 00:00:00', '2017-11-23 17:29:36'),
(112, 'ATP Sat OP.', 'fileupload/ATP Sat OP..pdf', '2017-11-24 00:00:00', '2017-11-24 15:34:14'),
(113, 'ATP Sat MDD', 'fileupload/ATP Sat MDD.pdf', '2017-11-24 00:00:00', '2017-11-24 15:35:44'),
(114, 'ATP Sat MDS', 'fileupload/ATP Sat MDS.pdf', '2017-11-24 00:00:00', '2017-11-24 15:37:35'),
(115, 'ATP Sat Final MDS', 'fileupload/ATP Sat Final MDS.pdf', '2017-11-26 00:00:00', '2017-11-26 06:35:20'),
(116, 'CORE SPORTS ACADEMY', 'fileupload/CORE SPORTS ACADEMY.pdf', '2017-12-22 00:00:00', '2017-12-22 12:00:52'),
(117, 'Core Sports Girls', 'fileupload/Core Sports Girls.pdf', '2017-12-22 00:00:00', '2017-12-22 12:05:27'),
(118, 'CORE SPORTS ACADEMYgirls', 'fileupload/CORE SPORTS ACADEMYgirls.pdf', '2017-12-23 00:00:00', '2017-12-23 10:58:33'),
(119, 'Stepak Brochure 2018', 'fileupload/Stepak Brochure 2018.pdf', '2017-12-25 00:00:00', '2017-12-25 08:03:27'),
(120, 'STEPAK Entry Form', 'fileupload/STEPAK Entry Form.pdf', '2017-12-25 00:00:00', '2017-12-25 08:49:26'),
(121, 'Mem Appl Form', 'fileupload/Mem Appl Form.pdf', '2018-01-11 00:00:00', '2018-01-12 05:27:31'),
(122, 'Stepak Mem From', 'fileupload/Stepak Mem From.doc', '2018-01-13 00:00:00', '2018-01-13 08:42:53'),
(123, 'ENTRY FORM 2018', 'fileupload/ENTRY FORM 2018.xls', '2018-01-13 00:00:00', '2018-01-13 08:45:57'),
(124, 'Stepak', 'fileupload/Stepak.pdf', '2018-01-24 00:00:00', '2018-01-24 12:21:57'),
(125, 'Stepak Draws', 'fileupload/Stepak Draws.pdf', '2018-01-24 00:00:00', '2018-01-24 15:30:39'),
(126, 'Stepak Doubles', 'fileupload/Stepak Doubles.pdf', '2018-01-25 00:00:00', '2018-01-25 14:01:45'),
(127, 'Stepak Singles', 'fileupload/Stepak Singles.pdf', '2018-01-25 00:00:00', '2018-01-25 14:25:12'),
(128, 'Stepak Fixtures Singles and Doubles', 'fileupload/Stepak Fixtures Singles and Doubles.pdf', '2018-01-26 00:00:00', '2018-01-26 14:11:41'),
(129, 'Stepak Singles and Doubles Fixtures', 'fileupload/Stepak Singles and Doubles Fixtures.pdf', '2018-01-27 00:00:00', '2018-01-27 11:22:10'),
(130, 'Stepak Evening prog', 'fileupload/Stepak Evening prog.pdf', '2018-01-27 00:00:00', '2018-01-27 11:46:08'),
(131, 'Web-Poster_Tennis', 'fileupload/Web-Poster_Tennis.jpg', '2018-03-15 00:00:00', '2018-03-15 09:07:20'),
(132, 'Web-Poster_Swimming', 'fileupload/Web-Poster_Swimming.jpg', '2018-03-15 00:00:00', '2018-03-15 09:09:06'),
(133, 'crsboysu-16', 'fileupload/crsboysu-16.pdf', '2018-04-06 00:00:00', '2018-04-06 13:41:29'),
(134, 'crsgirlsu-16', 'fileupload/crsgirlsu-16.pdf', '2018-04-06 00:00:00', '2018-04-06 13:51:51'),
(135, 'crsgirlsu16', 'fileupload/crsgirlsu16.pdf', '2018-04-07 00:00:00', '2018-04-07 07:57:44'),
(136, 'crsboysu16', 'fileupload/crsboysu16.pdf', '2018-04-07 00:00:00', '2018-04-07 12:06:09'),
(137, 'Boysunder18qfdraw', 'fileupload/Boysunder18qfdraw.pdf', '2018-04-27 00:00:00', '2018-04-27 10:55:33'),
(138, 'Girlsunder18qfdraw', 'fileupload/Girlsunder18qfdraw.pdf', '2018-04-27 00:00:00', '2018-04-27 10:55:46'),
(139, 'Orderofplaysaturday', 'fileupload/Orderofplaysaturday.pdf', '2018-04-27 00:00:00', '2018-04-27 10:56:22'),
(140, 'Boysunder18qfdraws', 'fileupload/Boysunder18qfdraws.pdf', '2018-04-28 00:00:00', '2018-04-28 12:35:59'),
(141, 'Girlsunder18qfdraws', 'fileupload/Girlsunder18qfdraws.pdf', '2018-04-28 00:00:00', '2018-04-28 12:36:14'),
(142, 'Orderofplaysunday', 'fileupload/Orderofplaysunday.pdf', '2018-04-28 00:00:00', '2018-04-28 12:36:29'),
(143, 'Rgnsboysu18md', 'fileupload/Rgnsboysu18md.pdf', '2018-04-29 00:00:00', '2018-04-29 10:54:57'),
(144, 'Rgnsboysu18md', 'fileupload/Rgnsboysu18md.pdf', '2018-04-29 00:00:00', '2018-04-29 10:55:54'),
(145, 'Rgnsboysu18md', 'fileupload/Rgnsboysu18md.pdf', '2018-04-29 00:00:00', '2018-04-29 10:56:27'),
(146, 'Rgnsgirlsu18md', 'fileupload/Rgnsgirlsu18md.pdf', '2018-04-29 00:00:00', '2018-04-29 10:58:17'),
(147, 'Rgnsgirlsu18md', 'fileupload/Rgnsgirlsu18md.pdf', '2018-04-29 00:00:00', '2018-04-29 10:58:33'),
(148, 'Rgnsgirlsu18md', 'fileupload/Rgnsgirlsu18md.pdf', '2018-04-29 00:00:00', '2018-04-29 10:59:04'),
(149, 'Rgnsgirlsu18md', 'fileupload/Rgnsgirlsu18md.pdf', '2018-04-29 00:00:00', '2018-04-29 10:59:15'),
(150, 'Rgnsgirlsu18md', 'fileupload/Rgnsgirlsu18md.pdf', '2018-04-29 00:00:00', '2018-04-29 10:59:52'),
(151, 'Rgnsgirlsu18md', 'fileupload/Rgnsgirlsu18md.pdf', '2018-04-29 00:00:00', '2018-04-29 11:00:19'),
(152, 'Rgnsgirlsu18md', 'fileupload/Rgnsgirlsu18md.pdf', '2018-04-29 00:00:00', '2018-04-29 11:00:20'),
(153, 'Rgnsgirlsu18md', 'fileupload/Rgnsgirlsu18md.pdf', '2018-04-29 00:00:00', '2018-04-29 11:00:22'),
(154, 'Rgnsgirlsu18md', 'fileupload/Rgnsgirlsu18md.pdf', '2018-04-29 00:00:00', '2018-04-29 11:00:57'),
(155, 'Rgnsgirlsu18md', 'fileupload/Rgnsgirlsu18md.pdf', '2018-04-29 00:00:00', '2018-04-29 11:01:31'),
(156, 'Rgnsgirlsu18md', 'fileupload/Rgnsgirlsu18md.pdf', '2018-04-29 00:00:00', '2018-04-29 11:01:33'),
(157, 'Rgnsopmon', 'fileupload/Rgnsopmon.pdf', '2018-04-29 00:00:00', '2018-04-29 11:01:59'),
(158, 'Rgnsopmon', 'fileupload/Rgnsopmon.pdf', '2018-04-29 00:00:00', '2018-04-29 11:02:01'),
(159, 'Rgnsopmon', 'fileupload/Rgnsopmon.pdf', '2018-04-29 00:00:00', '2018-04-29 11:03:29'),
(160, 'Rgnsopmon', 'fileupload/Rgnsopmon.pdf', '2018-04-29 00:00:00', '2018-04-29 11:04:24'),
(161, 'Rgnsopmon', 'fileupload/Rgnsopmon.pdf', '2018-04-29 00:00:00', '2018-04-29 11:05:45'),
(162, 'Rgnsgirlsu18md', 'fileupload/Rgnsgirlsu18md.pdf', '2018-04-29 00:00:00', '2018-04-29 11:08:01'),
(163, 'Rgnsopmon', 'fileupload/Rgnsopmon.pdf', '2018-04-29 00:00:00', '2018-04-29 11:10:11'),
(164, 'Rgnsopmon', 'fileupload/Rgnsopmon.pdf', '2018-04-29 00:00:00', '2018-04-29 11:11:30'),
(165, 'Rgnsgirlsu18md', 'fileupload/Rgnsgirlsu18md.pdf', '2018-04-29 00:00:00', '2018-04-29 11:12:05'),
(166, 'Rgnsopmon', 'fileupload/Rgnsopmon.pdf', '2018-04-29 00:00:00', '2018-04-29 11:14:54'),
(167, 'Rgnsopmon', 'fileupload/Rgnsopmon.pdf', '2018-04-29 00:00:00', '2018-04-29 11:14:56'),
(168, 'Rgnsopmon', 'fileupload/Rgnsopmon.pdf', '2018-04-29 00:00:00', '2018-04-29 11:14:57'),
(169, 'Rgnsopmon', 'fileupload/Rgnsopmon.pdf', '2018-04-29 00:00:00', '2018-04-29 11:15:04'),
(170, 'Rgnsopmon', 'fileupload/Rgnsopmon.pdf', '2018-04-29 00:00:00', '2018-04-29 11:15:07'),
(171, 'Rgnsopmon', 'fileupload/Rgnsopmon.pdf', '2018-04-29 00:00:00', '2018-04-29 11:15:11'),
(172, 'Rgnsopmon', 'fileupload/Rgnsopmon.pdf', '2018-04-29 00:00:00', '2018-04-29 11:15:12'),
(173, 'Rgnsopmon', 'fileupload/Rgnsopmon.pdf', '2018-04-29 00:00:00', '2018-04-29 11:15:13'),
(174, 'Rgnsopmon', 'fileupload/Rgnsopmon.pdf', '2018-04-29 00:00:00', '2018-04-29 11:15:14'),
(175, 'OrderofplayTuesday', 'fileupload/OrderofplayTuesday.pdf', '2018-04-30 00:00:00', '2018-04-30 14:06:11'),
(176, 'RG-NS-Boys-Under-18-Main-Draw', 'fileupload/RG-NS-Boys-Under-18-Main-Draw.pdf', '2018-04-30 00:00:00', '2018-04-30 15:15:45'),
(177, 'RG-NS-Girls-Under-18-Main-Draw', 'fileupload/RG-NS-Girls-Under-18-Main-Draw.pdf', '2018-04-30 00:00:00', '2018-04-30 15:17:21'),
(178, 'OrderofplayTuesday', 'fileupload/OrderofplayTuesday.pdf', '2018-04-30 00:00:00', '2018-05-01 03:06:34'),
(179, 'Rgnsoptue', 'fileupload/Rgnsoptue.pdf', '2018-04-30 00:00:00', '2018-05-01 04:12:20'),
(180, 'Order of Play-Wednesday 1', 'fileupload/Order of Play-Wednesday 1.pdf', '2018-05-01 00:00:00', '2018-05-01 14:11:47'),
(181, 'OrderofPlayThur', 'fileupload/OrderofPlayThur.pdf', '2018-05-02 00:00:00', '2018-05-02 14:44:19'),
(182, 'OrderofPlayFriday', 'fileupload/OrderofPlayFriday.pdf', '2018-05-03 00:00:00', '2018-05-03 13:31:59'),
(183, 'Order of Play-Saturday', 'fileupload/Order of Play-Saturday.pdf', '2018-05-04 00:00:00', '2018-05-04 11:02:41'),
(184, 'Mens Qualify', 'fileupload/Mens Qualify.pdf', '2018-10-19 00:00:00', '2018-10-19 10:53:07'),
(185, 'Notice', 'fileupload/Notice.pdf', '2018-10-19 00:00:00', '2018-10-19 11:10:39'),
(186, 'mensqualify2', 'fileupload/mensqualify2.pdf', '2018-10-20 00:00:00', '2018-10-20 13:14:57'),
(187, 'maindraw', 'fileupload/maindraw.pdf', '2018-10-21 00:00:00', '2018-10-21 10:07:52'),
(188, 'mdd', 'fileupload/mdd.pdf', '2018-10-21 00:00:00', '2018-10-21 14:10:11'),
(189, 'oop', 'fileupload/oop.pdf', '2018-10-21 00:00:00', '2018-10-21 14:41:08'),
(190, 'mensqf', 'fileupload/mensqf.pdf', '2018-10-22 00:00:00', '2018-10-22 14:24:21'),
(191, 'menssingles', 'fileupload/menssingles.pdf', '2018-10-22 00:00:00', '2018-10-22 14:24:33'),
(193, 'tueop', 'fileupload/tueop.pdf', '2018-10-22 00:00:00', '2018-10-22 14:25:18'),
(194, 'mensdoubles', 'fileupload/mensdoubles.pdf', '2018-10-22 00:00:00', '2018-10-22 14:27:00'),
(195, 'mensingles', 'fileupload/mensingles.pdf', '2018-10-23 00:00:00', '2018-10-23 11:57:17'),
(196, 'mendoubles', 'fileupload/mendoubles.pdf', '2018-10-23 00:00:00', '2018-10-23 11:57:26'),
(197, 'wedop', 'fileupload/wedop.pdf', '2018-10-23 00:00:00', '2018-10-23 11:57:36'),
(198, 'msingles', 'fileupload/msingles.pdf', '2018-10-24 00:00:00', '2018-10-24 12:32:53'),
(199, 'mdoubles', 'fileupload/mdoubles.pdf', '2018-10-24 00:00:00', '2018-10-24 12:33:03'),
(200, 'thuop', 'fileupload/thuop.pdf', '2018-10-24 00:00:00', '2018-10-24 12:33:14'),
(201, 'mdoubles', 'fileupload/mdoubles.pdf', '2018-10-24 00:00:00', '2018-10-24 12:34:25'),
(202, 'ms', 'fileupload/ms.pdf', '2018-10-25 00:00:00', '2018-10-25 11:02:45'),
(203, 'md ', 'fileupload/md .pdf', '2018-10-25 00:00:00', '2018-10-25 11:02:57'),
(204, 'oop', 'fileupload/oop.pdf', '2018-10-25 00:00:00', '2018-10-25 11:03:10'),
(205, 'ms', 'fileupload/ms.pdf', '2018-10-26 00:00:00', '2018-10-26 12:30:58'),
(206, 'md', 'fileupload/md.pdf', '2018-10-26 00:00:00', '2018-10-26 12:31:11'),
(207, 'oop', 'fileupload/oop.pdf', '2018-10-26 00:00:00', '2018-10-26 12:31:22'),
(208, 'Tue-OP', 'fileupload/Tue-OP.pdf', '2018-11-12 00:00:00', '2018-11-12 12:03:47'),
(209, 'mdd', 'fileupload/mdd.pdf', '2018-11-12 00:00:00', '2018-11-12 16:25:28'),
(210, 'mds', 'fileupload/mds.pdf', '2018-11-12 00:00:00', '2018-11-12 16:25:41'),
(211, 'bangalore-open-2018', 'fileupload/bangalore-open-2018.jpg', '2018-11-12 00:00:00', '2018-11-13 05:27:16'),
(212, 'Wed-Op', 'fileupload/Wed-Op.pdf', '2018-11-13 00:00:00', '2018-11-13 16:22:08'),
(213, 'wmdd', 'fileupload/wmdd.pdf', '2018-11-13 00:00:00', '2018-11-13 16:42:24'),
(214, 'wmds', 'fileupload/wmds.pdf', '2018-11-13 00:00:00', '2018-11-13 16:42:32'),
(215, 'Fri-Op', 'fileupload/Fri-Op.pdf', '2018-11-15 00:00:00', '2018-11-15 15:36:35'),
(216, 'fmdd', 'fileupload/fmdd.pdf', '2018-11-15 00:00:00', '2018-11-15 15:37:02'),
(217, 'fmds', 'fileupload/fmds.pdf', '2018-11-15 00:00:00', '2018-11-15 15:37:19'),
(218, 'wmds', 'fileupload/wmds.pdf', '2018-11-16 00:00:00', '2018-11-16 16:38:37'),
(219, 'wmdd', 'fileupload/wmdd.pdf', '2018-11-16 00:00:00', '2018-11-16 16:39:12'),
(220, 'MSat-OP', 'fileupload/MSat-OP.pdf', '2018-11-16 00:00:00', '2018-11-16 16:41:18'),
(221, 'mdd', 'fileupload/mdd.pdf', '2018-11-16 00:00:00', '2018-11-16 16:43:12'),
(222, 'mds', 'fileupload/mds.pdf', '2018-11-16 00:00:00', '2018-11-16 16:43:31'),
(223, 'STEPAK NOTICE', 'fileupload/STEPAK NOTICE.pdf', '2019-01-03 00:00:00', '2019-01-03 10:56:11'),
(225, 'Stepak-Brochure', 'fileupload/Stepak-Brochure.pdf', '2019-01-03 00:00:00', '2019-01-03 10:59:03'),
(230, 'Stepak Entry Form', 'fileupload/Stepak Entry Form.pdf', '2019-01-03 00:00:00', '2019-01-03 11:06:45'),
(231, 'Stepak Entry form', 'fileupload/Stepak Entry form.xls', '2019-01-03 00:00:00', '2019-01-03 11:11:21'),
(232, 'Stepak Entry form', 'fileupload/Stepak Entry form.xls', '2019-01-03 00:00:00', '2019-01-03 11:45:21'),
(233, 'Stepak Notice', 'fileupload/Stepak Notice.pdf', '2019-01-09 00:00:00', '2019-01-09 12:51:10'),
(234, 'Stepak Draw sheet', 'fileupload/Stepak Draw sheet.pdf', '2019-01-24 00:00:00', '2019-01-24 07:07:08'),
(235, 'Stepak Draw Sheet', 'fileupload/Stepak Draw Sheet.pdf', '2019-01-24 00:00:00', '2019-01-24 10:29:01'),
(236, 'Stepak Draw Singles', 'fileupload/Stepak Draw Singles.pdf', '2019-01-24 00:00:00', '2019-01-24 10:46:26'),
(237, 'Stepak Doubles Draw', 'fileupload/Stepak Doubles Draw.pdf', '2019-01-24 00:00:00', '2019-01-24 12:49:11'),
(238, 'Stepak Doubles Draw', 'fileupload/Stepak Doubles Draw.pdf', '2019-01-24 00:00:00', '2019-01-24 12:49:15'),
(239, 'Stepak Doubles Draw', 'fileupload/Stepak Doubles Draw.pdf', '2019-01-24 00:00:00', '2019-01-24 12:49:20'),
(240, 'Stepak Doubles Draw', 'fileupload/Stepak Doubles Draw.pdf', '2019-01-24 00:00:00', '2019-01-24 12:49:22'),
(241, 'Stepak Doubles Draw', 'fileupload/Stepak Doubles Draw.pdf', '2019-01-24 00:00:00', '2019-01-24 12:49:28'),
(242, 'Stepak Doubles Draw', 'fileupload/Stepak Doubles Draw.pdf', '2019-01-24 00:00:00', '2019-01-24 12:49:42'),
(243, 'Stepaksinglesdraw', 'fileupload/Stepaksinglesdraw.pdf', '2019-01-25 00:00:00', '2019-01-25 14:18:17'),
(244, 'stepaksd', 'fileupload/stepaksd.pdf', '2019-01-25 00:00:00', '2019-01-25 14:49:33'),
(245, 'stepaksdraw', 'fileupload/stepaksdraw.pdf', '2019-01-25 00:00:00', '2019-01-25 14:52:42'),
(246, 'singles draw', 'fileupload/singles draw.pdf', '2019-01-25 00:00:00', '2019-01-25 14:54:58'),
(247, 'stepakdoubles', 'fileupload/stepakdoubles.pdf', '2019-01-25 00:00:00', '2019-01-25 15:05:29'),
(248, 'stepakdoubles', 'fileupload/stepakdoubles.pdf', '2019-01-25 00:00:00', '2019-01-25 15:05:37'),
(249, 'Singlesdrawsheet', 'fileupload/Singlesdrawsheet.pdf', '2019-01-26 00:00:00', '2019-01-26 13:12:42'),
(250, 'Doublesdraw', 'fileupload/Doublesdraw.pdf', '2019-01-26 00:00:00', '2019-01-26 13:53:07'),
(251, 'Doublesdraw', 'fileupload/Doublesdraw.pdf', '2019-01-26 00:00:00', '2019-01-26 13:53:23'),
(252, 'Stepak-Brochure-2020', 'fileupload/Stepak-Brochure-2020.pdf', '2020-02-03 00:00:00', '2020-02-03 14:20:03'),
(253, 'STEPAK ENTRY FORM', 'fileupload/STEPAK ENTRY FORM.pdf', '2020-02-03 00:00:00', '2020-02-03 14:50:52'),
(254, 'Singles Draw Sheet', 'fileupload/Singles Draw Sheet.pdf', '2020-02-09 00:00:00', '2020-02-09 12:56:30'),
(255, 'Qualifying Singles ', 'fileupload/Qualifying Singles .pdf', '2020-02-09 00:00:00', '2020-02-09 12:56:39'),
(256, 'Monday OP', 'fileupload/Monday OP.pdf', '2020-02-09 00:00:00', '2020-02-09 12:56:49'),
(257, 'Doubles Draw Sheet', 'fileupload/Doubles Draw Sheet.pdf', '2020-02-09 00:00:00', '2020-02-09 12:56:59'),
(258, 'Tuesday OP', 'fileupload/Tuesday OP.pdf', '2020-02-10 00:00:00', '2020-02-10 16:15:53'),
(259, 'Singles Draw sheetm', 'fileupload/Singles Draw sheetm.pdf', '2020-02-10 00:00:00', '2020-02-10 16:16:00'),
(260, 'Doubles Draw Sheett', 'fileupload/Doubles Draw Sheett.pdf', '2020-02-11 00:00:00', '2020-02-11 15:23:36'),
(261, 'Singles Draw sheett', 'fileupload/Singles Draw sheett.pdf', '2020-02-11 00:00:00', '2020-02-11 15:23:45'),
(262, 'Wednesday OP', 'fileupload/Wednesday OP.pdf', '2020-02-11 00:00:00', '2020-02-11 15:23:52'),
(263, 'Thursday OP', 'fileupload/Thursday OP.pdf', '2020-02-12 00:00:00', '2020-02-12 15:54:02'),
(264, 'Singles Draw sheetw', 'fileupload/Singles Draw sheetw.pdf', '2020-02-12 00:00:00', '2020-02-12 15:54:11'),
(265, 'Doubles Draw Sheetw', 'fileupload/Doubles Draw Sheetw.pdf', '2020-02-12 00:00:00', '2020-02-12 15:54:20'),
(266, 'sponsor', 'fileupload/sponsor.jpg', '2020-02-13 00:00:00', '2020-02-13 07:33:41'),
(267, 'Doubles Draw Sheett', 'fileupload/Doubles Draw Sheett.pdf', '2020-02-13 00:00:00', '2020-02-13 16:35:12'),
(268, 'Friday OP', 'fileupload/Friday OP.pdf', '2020-02-13 00:00:00', '2020-02-13 16:35:22'),
(269, 'Singles Draw sheett', 'fileupload/Singles Draw sheett.pdf', '2020-02-13 00:00:00', '2020-02-13 16:35:31'),
(270, 'Doubles Draw Sheetf', 'fileupload/Doubles Draw Sheetf.pdf', '2020-02-14 00:00:00', '2020-02-14 17:39:14'),
(271, 'Saturday OP', 'fileupload/Saturday OP.pdf', '2020-02-14 00:00:00', '2020-02-14 17:39:27'),
(272, 'Singles Draw sheetf', 'fileupload/Singles Draw sheetf.pdf', '2020-02-14 00:00:00', '2020-02-14 17:39:38'),
(273, 'Doubles Draw Sheets', 'fileupload/Doubles Draw Sheets.pdf', '2020-02-15 00:00:00', '2020-02-15 16:23:44'),
(274, 'Singles Draw sheets', 'fileupload/Singles Draw sheets.pdf', '2020-02-15 00:00:00', '2020-02-15 16:24:01'),
(275, 'Sunday OP', 'fileupload/Sunday OP.pdf', '2020-02-15 00:00:00', '2020-02-15 16:24:11'),
(276, 'Singles Draw sheet', 'fileupload/Singles Draw sheet.pdf', '2020-02-16 00:00:00', '2020-02-16 15:33:50'),
(277, 'AITATS', 'fileupload/AITATS.pdf', '2020-11-19 00:00:00', '2020-11-19 06:01:44'),
(278, 'Boys Under 16 Draw', 'fileupload/Boys Under 16 Draw.pdf', '2020-11-19 00:00:00', '2020-11-19 11:17:29'),
(279, 'Girls U-16 Draw', 'fileupload/Girls U-16 Draw.pdf', '2020-11-19 00:00:00', '2020-11-19 11:17:38'),
(280, 'Order of play', 'fileupload/Order of play.pdf', '2020-11-19 00:00:00', '2020-11-19 11:17:44'),
(281, 'Op Sat', 'fileupload/Op Sat.pdf', '2020-11-20 00:00:00', '2020-11-20 11:15:14'),
(282, 'Boys Under 16 Draw', 'fileupload/Boys Under 16 Draw.pdf', '2020-11-20 00:00:00', '2020-11-20 15:32:36'),
(283, 'Girls U16 Draw', 'fileupload/Girls U16 Draw.pdf', '2020-11-20 00:00:00', '2020-11-20 15:32:51'),
(284, 'Bu16draw', 'fileupload/Bu16draw.pdf', '2020-11-21 00:00:00', '2020-11-21 10:29:23'),
(285, 'Gu16draw', 'fileupload/Gu16draw.pdf', '2020-11-21 00:00:00', '2020-11-21 10:29:30'),
(286, 'OPsun', 'fileupload/OPsun.pdf', '2020-11-21 00:00:00', '2020-11-21 10:29:36'),
(287, 'Girlsu14draw', 'fileupload/Girlsu14draw.pdf', '2020-11-26 00:00:00', '2020-11-26 11:05:08'),
(288, 'Boysunder14draw', 'fileupload/Boysunder14draw.pdf', '2020-11-26 00:00:00', '2020-11-26 11:05:15'),
(289, 'Opfriday', 'fileupload/Opfriday.pdf', '2020-11-26 00:00:00', '2020-11-26 11:05:25'),
(290, 'OOPfri', 'fileupload/OOPfri.pdf', '2020-11-26 00:00:00', '2020-11-26 12:23:08'),
(291, 'Girlsu14D', 'fileupload/Girlsu14D.pdf', '2020-11-26 00:00:00', '2020-11-26 12:23:19'),
(292, 'girlsu14dw', 'fileupload/girlsu14dw.pdf', '2020-11-26 00:00:00', '2020-11-26 12:31:40'),
(293, 'Boysu14dw1', 'fileupload/Boysu14dw1.pdf', '2020-11-27 00:00:00', '2020-11-27 13:06:19'),
(294, 'Girlsu14dw1', 'fileupload/Girlsu14dw1.pdf', '2020-11-27 00:00:00', '2020-11-27 13:06:26'),
(295, 'oopsat', 'fileupload/oopsat.pdf', '2020-11-27 00:00:00', '2020-11-27 13:06:33'),
(296, 'boysu14drs', 'fileupload/boysu14drs.pdf', '2020-11-28 00:00:00', '2020-11-28 12:04:31'),
(297, 'girlsu14drs', 'fileupload/girlsu14drs.pdf', '2020-11-28 00:00:00', '2020-11-28 12:04:38'),
(298, 'oop29th', 'fileupload/oop29th.pdf', '2020-11-28 00:00:00', '2020-11-28 12:04:48');

-- --------------------------------------------------------

--
-- Table structure for table `footer_menu`
--

CREATE TABLE `footer_menu` (
  `id` bigint(20) NOT NULL,
  `menu_name` varchar(255) NOT NULL,
  `menu_link` varchar(255) NOT NULL,
  `order_number` bigint(12) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `footer_menu`
--

INSERT INTO `footer_menu` (`id`, `menu_name`, `menu_link`, `order_number`, `created`) VALUES
(1, 'calender', 'aita-tournament', 1, '2017-10-24 02:10:48'),
(2, 'News', 'news', 2, '2017-10-24 02:10:39'),
(3, 'Affilated Clubs', 'affilated_clubs', 3, '2017-10-24 02:10:16'),
(4, 'COACHING', 'advance', 4, '2017-10-24 02:10:48'),
(7, 'Contact', 'contactus', 5, '2020-10-06 12:10:45');

-- --------------------------------------------------------

--
-- Table structure for table `guestroombooking`
--

CREATE TABLE `guestroombooking` (
  `id` int(11) NOT NULL,
  `checkindate` date DEFAULT NULL,
  `checkoutdate` date DEFAULT NULL,
  `adultcount` int(11) DEFAULT NULL,
  `childrencount` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guestroombooking`
--

INSERT INTO `guestroombooking` (`id`, `checkindate`, `checkoutdate`, `adultcount`, `childrencount`, `name`, `mobile`, `email`, `status`) VALUES
(1, '2016-12-17', '2016-12-17', 2, 2, 'test', '34343434', 'vinothviv@gmail.com', '-'),
(2, '2016-12-17', '2016-12-17', 3, 3, '34', '343434', 'sdfsdf@sdf.dfg', '-'),
(3, '2016-12-17', '2016-12-17', 3, 3, '34', '343434', 'sdfsdf@sdf.dfg', '-'),
(4, '2016-12-17', '2016-12-17', 3, 5, '55555', '5555', '55#@5.o3', '-'),
(5, '2016-12-17', '2016-12-17', 2, 1, 'Subeen', '9008181177', 'focus.subeen@gmail.com', '-'),
(6, '2016-12-19', '2016-12-19', 5, 4, '4545', '45666', 'ASAFASDFF@gmail.com', 'Request'),
(7, '2016-12-19', '2016-12-19', 2, 1, 'Subeen', '9008181177', 'focus.subeen@gmail.com', 'Request'),
(8, '2016-12-19', '2016-12-19', 3, 3, 'sdf', '34343434', '34@sdf.com', 'Request'),
(9, '2016-12-20', '2016-12-20', 3, 2, 'sdfsdfsdf', '232', '23', 'Request'),
(10, '2016-12-21', '2016-12-21', 1, 1, 'Subeen', '9008181177', 'focus.subeen@gmail.com', 'Approvaled'),
(11, '2017-01-17', '2017-01-17', 1, 1, 'ASHOK', '9371023842', 'ashok.plastic@yahoo.com', 'Request'),
(12, '2017-05-09', '2017-05-09', 3, 2, 'G.Mahendrakumar', '9443374608', 'sridhaind@gmail.com', 'Request'),
(13, '2017-06-01', '2017-06-01', 3, 1, 'Valli', '9952308444', 'nsvalli@hotmail.com', 'Request'),
(14, '2018-02-18', '2018-02-18', 2, 1, 'Mrs. Rekha Dixit', '9869034746', 'rekhakpmg@gmail.com', 'Request'),
(15, '2018-02-18', '2018-02-18', 2, 1, 'Mrs. Rekha Dixit', '9869034746', 'rekhakpmg@gmail.com', 'Request'),
(16, '2018-03-29', '2018-03-29', 2, 2, 'Sujatha Ramanan', '9791154846', 'sujamulu@gmail.com', 'Request'),
(17, '2018-05-20', '2018-05-20', 2, 2, 'Niranjan', '9819601414', 'niranjan.mehta@gmail.com', 'Request'),
(18, '2019-08-25', '2019-08-25', 2, 1, 'Varun K', '9964994000', 'reddi.varun4000@gmail.com', 'Request');

-- --------------------------------------------------------

--
-- Table structure for table `headlines`
--

CREATE TABLE `headlines` (
  `id` bigint(20) NOT NULL,
  `headlines_name` varchar(255) NOT NULL,
  `headline_link` varchar(100) NOT NULL,
  `order_no` bigint(12) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `headlines`
--

INSERT INTO `headlines` (`id`, `headlines_name`, `headline_link`, `order_no`, `created_date`) VALUES
(10, 'Biggest ATP Challenger returns to Bengaluru', '164', 1, '2019-11-17 02:11:48'),
(11, 'Reshma is runner up in  Asian Tennis Tour  $3000', '163', 2, '2019-11-17 02:11:01'),
(12, 'Aayush P Bhat  won the U - 14 singles title at the Fenesta Open  ', '162', 3, '2019-11-17 02:11:17');

-- --------------------------------------------------------

--
-- Table structure for table `homepage_news`
--

CREATE TABLE `homepage_news` (
  `id` bigint(100) NOT NULL,
  `news` varchar(2000) NOT NULL,
  `created_by` varchar(2000) NOT NULL,
  `sort_order` bigint(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('A','I') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homepage_news`
--

INSERT INTO `homepage_news` (`id`, `news`, `created_by`, `sort_order`, `created_at`, `status`) VALUES
(13, 'Davis Cup from April 7 - 9 2017', 'Admin', 1, '2017-04-26 16:38:09', 'A'),
(14, 'Dear Member Pls update your Email-ID Mob No', 'admin', 1, '2017-07-24 17:34:11', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `homepage_scroll_news`
--

CREATE TABLE `homepage_scroll_news` (
  `id` bigint(100) NOT NULL,
  `news` varchar(2000) NOT NULL,
  `status` enum('A','I') NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(2000) NOT NULL,
  `sort_order` bigint(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `home_facilities`
--

CREATE TABLE `home_facilities` (
  `id` bigint(12) NOT NULL,
  `facility_name` varchar(255) NOT NULL,
  `facility_url` varchar(255) NOT NULL,
  `facility_content` text NOT NULL,
  `order_no` bigint(12) NOT NULL,
  `image_upload` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_facilities`
--

INSERT INTO `home_facilities` (`id`, `facility_name`, `facility_url`, `facility_content`, `order_no`, `image_upload`, `created_at`) VALUES
(1, 'Party Halls', 'partyhall', '', 2, 'uploads/facility/192386.jpg', '2017-10-24 09:10:00'),
(3, 'Guest Room', 'guestroom', '', 1, 'uploads/facility/Rooms53358.jpg', '2018-01-29 06:01:38'),
(4, 'Swimming Pool', 'swimming_pool', '', 3, 'uploads/facility/DSC_049838188.jpg', '2018-01-29 06:01:33'),
(5, 'HEALTH CLUB', 'table_tennis', 'Table Tennis & Billiards', 4, 'uploads/facility/Gym11383.jpg', '2018-01-29 06:01:35'),
(6, 'Bar & Restaurant', 'barrestaurant', '', 5, 'uploads/facility/Restaurant22099.jpg', '2018-01-29 06:01:01');

-- --------------------------------------------------------

--
-- Table structure for table `home_notification`
--

CREATE TABLE `home_notification` (
  `id` bigint(20) NOT NULL,
  `notification_name` varchar(255) NOT NULL,
  `notification_url` varchar(255) NOT NULL,
  `order_no` bigint(12) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ksl_coaching_pages`
--

CREATE TABLE `ksl_coaching_pages` (
  `coaching_id` bigint(20) NOT NULL,
  `gallery_id` bigint(20) DEFAULT NULL,
  `coaching_top_main_heading` varchar(1000) NOT NULL,
  `coaching_top_sub_heading` text CHARACTER SET utf8 NOT NULL,
  `coaching_top_background_img` text CHARACTER SET utf8,
  `content_portfolio_img` text CHARACTER SET utf8,
  `remove_content_img` tinyint(1) NOT NULL,
  `coaching_page_title` varchar(500) NOT NULL,
  `coaching_page_name` varchar(250) NOT NULL,
  `coaching_content_heading` varchar(500) NOT NULL,
  `coaching_tab_one_title` varchar(500) NOT NULL,
  `coaching_tab_one_content` text CHARACTER SET utf8 NOT NULL,
  `coaching_tab_two_title` varchar(500) NOT NULL,
  `coaching_tab_two_content_1` text CHARACTER SET utf8 NOT NULL,
  `coaching_tab_two_content_2` text CHARACTER SET utf8 NOT NULL,
  `coaching_tab_three_title` varchar(500) NOT NULL,
  `coaching_file_upload` varchar(500) DEFAULT NULL,
  `coaching_tab_three_content_1` text CHARACTER SET utf8,
  `coaching_meta_tag_name` text CHARACTER SET utf8,
  `coaching_meta_tag_description` text CHARACTER SET utf8,
  `coaching_assign_sidebar` tinyint(1) NOT NULL,
  `coaching_created_at` datetime NOT NULL,
  `coaching_modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `coaching_status` enum('A','I') NOT NULL COMMENT 'A=Active, I=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ksl_coaching_pages`
--

INSERT INTO `ksl_coaching_pages` (`coaching_id`, `gallery_id`, `coaching_top_main_heading`, `coaching_top_sub_heading`, `coaching_top_background_img`, `content_portfolio_img`, `remove_content_img`, `coaching_page_title`, `coaching_page_name`, `coaching_content_heading`, `coaching_tab_one_title`, `coaching_tab_one_content`, `coaching_tab_two_title`, `coaching_tab_two_content_1`, `coaching_tab_two_content_2`, `coaching_tab_three_title`, `coaching_file_upload`, `coaching_tab_three_content_1`, `coaching_meta_tag_name`, `coaching_meta_tag_description`, `coaching_assign_sidebar`, `coaching_created_at`, `coaching_modified_at`, `coaching_status`) VALUES
(2, 17, 'Regular Coaching', 'KSLTA Tennis Coaching', 'uploads/bannerimages/ksltapage265231536_KSLTA.jpg', 'uploads/bannerimages/stadium231557_KSLTA.jpg', 0, 'Regular Coaching - KSLTA Tennis Coaching Programme', 'regular', 'REGULAR COACHING', 'RULES & REGULATIONS', '<ol>\r\n<li>\r\n<h5><span style=\"color: #000000;\">Forms duly completed along with two passport size photos &amp; Cash / Account Payee Cheque in favour of Karnataka State Lawn Tennis Association (KSLTA) should be submitted during 11-00 a.m. to 5-30 p.m. in the KSLTA Office.</span></h5>\r\n</li>\r\n<li>\r\n<h5><span style=\"color: #000000;\">Trainees have to pay minimum three months&rsquo; fee in advance (for all age group) Physical fitness training will also be included in the coaching session.</span></h5>\r\n</li>\r\n<li>\r\n<h5><span style=\"color: #000000;\">Coaching fees stipulated is applicable per calendar month basis (viz. 1st to 30th/ 31st) And no prorated reductions permitted.</span></h5>\r\n</li>\r\n<li>\r\n<h5><span style=\"color: #000000;\">No Outstation Cheques will be accepted.</span></h5>\r\n</li>\r\n<li>\r\n<h5><span style=\"color: #000000;\">Continuing Trainees will be given first preference for admission.</span></h5>\r\n</li>\r\n<li>\r\n<h5><span style=\"color: #000000;\">Coaching fee will not be refunded under any circumstances, Non-adjustable against Absence.</span></h5>\r\n</li>\r\n<li>\r\n<h5><span style=\"color: #000000;\">\"Trainees shall attend coaching classes regularly and punctually\".</span></h5>\r\n</li>\r\n<li>\r\n<h5><span style=\"color: #000000;\">Trainees (including adults) will be required to abide by the instructions given to them by the coaches from time to time. Coaching programme will involve Physical Fitness, Exercise, Drill, Etc.</span></h5>\r\n</li>\r\n<li>\r\n<h5><span style=\"color: #000000;\">The Trainee shall wear proper Tennis Shoes on the Courts. (Bermuda or Full Pant, Cross Trousers &amp; Jogging Shoes are not allowed).</span></h5>\r\n</li>\r\n<li>\r\n<h5><span style=\"color: #000000;\">Missed session will not be compensated.</span></h5>\r\n</li>\r\n<li>\r\n<h5><span style=\"color: #000000;\">No change in timings will be entertained.</span></h5>\r\n</li>\r\n<li>\r\n<h5><span style=\"color: #000000;\">No Trainee will be allowed for two continuous sessions in a day (Morning &amp; Evening in any circumstances except Tournament Group)</span></h5>\r\n</li>\r\n<li>\r\n<h5><span style=\"color: #000000;\">Parents are not allowed on the Tennis Courts during the Coaching Hours.</span></h5>\r\n</li>\r\n<li>\r\n<h5><span style=\"color: #000000;\">It is compulsory for Trainees as a part of coaching School Curriculum, to act as Ball Person, Score Keeper, Lines Person or Umpire for Tournaments Conducted by KSLTA as required. Besides it is compulsory for all trainees to watch tournament held at KSLTA.</span></h5>\r\n</li>\r\n</ol>', 'ADMISSION CHARGES', '<div class=\"col-md-6\">\r\n<div class=\"bulleted_list\">\r\n<h3><span style=\"color: #000000;\"><strong>COACHING PROGRAMME</strong></span></h3>\r\n<ul>\r\n<li><span style=\"color: #000000;\">6 days a week - Monday to Saturday</span></li>\r\n<li><span style=\"color: #000000;\">3 days a week - Monday &amp; Wednesday and Friday</span></li>\r\n<li><span style=\"color: #000000;\">3 days a week - Tuesday &amp; Thursday and Saturday</span></li>\r\n</ul>\r\n</div>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;<span style=\"color: #000000;\"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br /></strong></span></p>\r\n<p>&nbsp;</p>\r\n<h3 class=\"bulleted_list\">&nbsp;<span style=\"color: #000000;\"><strong>&nbsp; TIMINGS FOR COACHING SESSIONS</strong></span><span style=\"color: #000000;\"><strong>:</strong></span></h3>\r\n<div class=\"bulleted_list\">&nbsp;&nbsp;</div>\r\n<div class=\"bulleted_list\">\r\n<ul>\r\n<li><span style=\"color: #000000;\">6-30 &ndash; 7-30 a.m. &amp; 7-30 &ndash; 8-30 a.m.</span></li>\r\n<li><span style=\"color: #000000;\">4-30 &ndash; 5-30 p.m. &amp; 5-30 &ndash; 6-30 p.m.</span></li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n</div>\r\n</div>\r\n<div class=\"col-md-6\">\r\n<div class=\"bulleted_list\">\r\n<h3><span style=\"color: #000000;\"><strong>FEE STRUCTURE PER MONTH</strong></span></h3>\r\n</div>\r\n<div class=\"bulleted_list\">\r\n<ul style=\"list-style-type: disc;\">\r\n<li><span style=\"color: #000000;\">Rs.4500/- for 6 days - Children from (6-16yrs) </span></li>\r\n<li><span style=\"color: #000000;\">Rs.3500/- for 3 days &ndash; Children from (6-16yrs)</span></li>\r\n<li><span style=\"color: #000000;\">Rs.5000/- for 6 days &ndash; (Adult)</span></li>\r\n<li><span style=\"color: #000000;\">Rs.4000/- for 3 days &ndash; (Adult)</span></li>\r\n</ul>\r\n</div>\r\n<p>&nbsp;</p>\r\n<p><span style=\"color: #000000;\"><strong>&nbsp;</strong></span></p>\r\n</div>', '', 'FORMS', 'uploads/coaching/regular-coaching130337_KSLTA.pdf', '', 'kslta regular coaching', 'kslta regular coaching', 0, '0000-00-00 00:00:00', '2020-11-24 13:15:15', 'A'),
(3, 17, 'KSLTA SUMMER COACHING CAMP TENNIS', 'COACHING WITH BEST IN CLASS.', 'uploads/bannerimages/about031001_KSLTA.jpg', 'uploads/bannerimages/stadium031001_KSLTA.jpg', 0, 'KSLTA SUMMER COACHING CAMP TENNIS', 'summer_coaching_camp_tennis', 'SUMMER COACHING CAMP TENNIS', 'RULES & REGULATIONS ', '<ol>\r\n<li>Forms duly completed along with two passport size photos &amp; Cash / Account Payee Cheque in favour of Karnataka State Lawn Tennis Association (KSLTA) should be submitted during 11-00 a.m. to 5-30 p.m. in the KSLTA Office on all working days.</li>\r\n<li>&nbsp;Continuing Trainees will be given first preference for admission.</li>\r\n</ol>\r\n<ol start=\"3\">\r\n<li>Coaching fee will not be <strong>refunded</strong> under any circumstances, Non-adjustable against Absence.</li>\r\n</ol>\r\n<ol start=\"4\">\r\n<li>\"Trainees shall attend coaching classes regularly and punctually\".</li>\r\n</ol>\r\n<ol start=\"5\">\r\n<li>Trainees (including adults) will be required to abide by the instructions given to them by the coaches from time to time. Coaching programme will involve Physical Fitness, Exercise, Drill, Etc.</li>\r\n</ol>\r\n<ol start=\"6\">\r\n<li>The Trainee shall wear proper Tennis Shoes on the Courts. (Bermuda or Full Pant, Cross Trousers &amp; Jogging Shoes are not allowed).</li>\r\n</ol>\r\n<ol start=\"7\">\r\n<li>Missed session will not be compensated.</li>\r\n</ol>\r\n<ol start=\"8\">\r\n<li>No change in timings will be entertained.</li>\r\n</ol>\r\n<ol start=\"9\">\r\n<li>No Trainee will be allowed for two continuous sessions in a day (Morning &amp; Evening in any circumstances except Tournament Group)</li>\r\n</ol>\r\n<ol start=\"10\">\r\n<li>Parents are not allowed on the Tennis Courts during the Coaching Hours.</li>\r\n</ol>\r\n<p>&nbsp;</p>', 'ADMISSION CHARGES', '<p><strong>SUMMER COACHING PROGRAMME AT KSLTA / KANTEERAVA STADIUM</strong></p>\r\n<p style=\"text-align: left;\"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; From 1st Apr to 31st May 2019</strong></p>\r\n<p><strong>5 days a week&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Monday to Friday</strong></p>\r\n<p><strong><u>TIMINGS FOR COACHING SESSIONS:</u></strong></p>\r\n<p><strong>KSLTA STADIUM&nbsp; (HARD COURT)&nbsp;</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong> KANTEERAVA STADIUM (CLAY COURT )</strong></p>\r\n<p>6-30 &ndash; 7-30 a.m. (Adult )&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 6-30 &ndash; 7-30 a.m.</p>\r\n<p>7-30 &ndash; 8-30 a.m. ( Children)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 7-30 &ndash; 8-30 a.m.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>4-30 &ndash; 5-30 p.m. (Children)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4-30 &ndash; 5-30 p.m.</p>\r\n<p>5-30 &ndash; 6-30 p.m. (Children)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5-30 &ndash; 6-30 p.m.</p>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n<p><strong><u>FEE STRUCTURE PER MONTH</u></strong></p>\r\n<p><strong>KSLTA STADIUM &amp; KANTEERAVA STADIUM.&nbsp;&nbsp;&nbsp;</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n<p>One Month &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp; Rs.6,500/- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n<p>Two Month&nbsp; &nbsp;&nbsp;&nbsp;-&nbsp; Rs.11,000/-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n<p>&nbsp;<br /><strong>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong></p>\r\n<p><strong>&nbsp;</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; </strong><strong>NOTE :</strong></p>\r\n<ol>\r\n<li>Players should report at KSLTA at 9-30 a.m. on 31<sup>st </sup>May 2019. Both KSLTA / Kanteerava Stadium Players.</li>\r\n</ol>\r\n<ol start=\"2\">\r\n<li>Chief Coach will inform about the Batches &amp; Kitting for the players.</li>\r\n</ol>\r\n<ol start=\"3\">\r\n<li>Coaching Camp will be Monday to Friday.</li>\r\n</ol>\r\n<p>&nbsp;<strong>For any details contact : Office &ndash; 080-22869797/3636/1010 </strong></p>\r\n<p><strong>&nbsp;</strong></p>\r\n<p>&nbsp;</p>', '', 'FORMS', 'uploads/coaching/Tennis051834_KSLTA.pdf', '', 'SUMMER COACHING CAMP TENNIS', 'SUMMER COACHING CAMP TENNIS', 0, '0000-00-00 00:00:00', '2019-03-29 10:48:17', 'A'),
(10, 7, 'KSLTA SUMMER COACHING CAMP SWIMMING', 'COACHING WITH BEST IN CLASS.', 'uploads/bannerimages/about031716_KSLTA.jpg', 'uploads/bannerimages/stadium031716_KSLTA.jpg', 0, 'KSLTA SUMMER COACHING CAMP SWIMMING', 'summer_coaching_camp_swimming', ' SUMMER COACHING CAMP SWIMMING', 'RULES & REGULATIONS', '<p>&nbsp;</p>\r\n<table width=\"706\">\r\n<tbody>\r\n<tr>\r\n<td width=\"23\">1</td>\r\n<td colspan=\"9\" width=\"683\">Forms Duly Completed along with 2 Passport Size Photos &amp; Cash / Account Payee Cheque In Favour of&nbsp;&nbsp;&nbsp;&nbsp; KSLTA Should be Submitted During 11-00 a.m to 5-30 p.m in the KSLTA office.</td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td colspan=\"9\">Admission is only for children above 5 years</td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td colspan=\"9\">Right of Admission is Reserved</td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td colspan=\"9\">Members have to follow strictly the time slots allocated</td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td colspan=\"9\">Coaching fees as applicable must be paid in advance</td>\r\n</tr>\r\n<tr>\r\n<td>6</td>\r\n<td colspan=\"9\">Coaching fee will not be refunded under any circumstances,Non-adjustabale against absence.</td>\r\n</tr>\r\n<tr>\r\n<td>7</td>\r\n<td colspan=\"9\">Coaching Class shall attend regularly and punctually, missed session will not be compensated.</td>\r\n</tr>\r\n<tr>\r\n<td>8</td>\r\n<td colspan=\"9\">Entrance with contagious diseases are prohibited</td>\r\n</tr>\r\n<tr>\r\n<td>9</td>\r\n<td colspan=\"9\">Please do not shout, Sing, Whistle, Spit, Smoke or Make any Objectionable signs / gestures.</td>\r\n</tr>\r\n<tr>\r\n<td>10</td>\r\n<td colspan=\"9\">For all purposes the instructor / official - incharge of the pool will be authority.</td>\r\n</tr>\r\n<tr>\r\n<td>11</td>\r\n<td colspan=\"9\">Take shower, bath and wash your legs before getting in to the pool.</td>\r\n</tr>\r\n<tr>\r\n<td>12</td>\r\n<td colspan=\"9\">Coloured swimming trunks or swimming suits must be used.</td>\r\n</tr>\r\n<tr>\r\n<td>13</td>\r\n<td colspan=\"9\">White and transparent thin swimming trunks or suits are not allowed.</td>\r\n</tr>\r\n<tr>\r\n<td>14</td>\r\n<td colspan=\"9\">Headcap is a must while swimming.</td>\r\n</tr>\r\n<tr>\r\n<td>15</td>\r\n<td colspan=\"9\">Members are requested to remove the shoes before entering the pool.</td>\r\n</tr>\r\n<tr>\r\n<td>16</td>\r\n<td colspan=\"9\">Walking with shoe will not be permitted in and around the pool area.</td>\r\n</tr>\r\n<tr>\r\n<td>17</td>\r\n<td colspan=\"9\">No person should enter the pool without the knowledge of the instructor</td>\r\n</tr>\r\n<tr>\r\n<td>18</td>\r\n<td colspan=\"9\">This a shallow pool, juming / diving is strictly prohibited.</td>\r\n</tr>\r\n<tr>\r\n<td>19</td>\r\n<td colspan=\"9\">The Management will not be responsible for the loss of valuables, cloth etc of the swimmers / parents / members etc.</td>\r\n</tr>\r\n<tr>\r\n<td>20</td>\r\n<td colspan=\"9\">The management will not be responsible for any injury / loss of life to the person on any account.</td>\r\n</tr>\r\n<tr>\r\n<td>21</td>\r\n<td colspan=\"9\">No other game except swimming is allowed in the premises.</td>\r\n</tr>\r\n<tr>\r\n<td>22</td>\r\n<td colspan=\"9\">Use of oil befor swimming is prohibited</td>\r\n</tr>\r\n<tr>\r\n<td>23</td>\r\n<td colspan=\"9\">Photography is strictly prohibited.</td>\r\n</tr>\r\n<tr>\r\n<td>24</td>\r\n<td colspan=\"9\">Pets are not allowed inside the premises.</td>\r\n</tr>\r\n<tr>\r\n<td>25</td>\r\n<td colspan=\"9\">The Management reserves the right to close the pool at is discreations.</td>\r\n</tr>\r\n<tr>\r\n<td>26</td>\r\n<td colspan=\"9\">Parents are requested to submit ID Proof / Age prrof of the Children enrolled to the office along with the application and it is compulsory (Birth Certficate / Aadhar Card / School cerficate / SSLC Marks Card / Passport ) to be enclosed.</td>\r\n</tr>\r\n<tr>\r\n<td>27</td>\r\n<td colspan=\"9\">Children below 5 years are not allowed for coaching.</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'ADMISSION CHARGES', '<p style=\"text-align: left;\"><strong>THERE WILL BE NO COACHING ON SUNDAYS</strong></p>\r\n<p style=\"text-align: left;\"><strong>CHANGE OF BATCH WILL NOT BE ALLOWED ONCE ALLOTTED</strong></p>\r\n<p style=\"text-align: left;\"><strong>AMOUNT PAID WILL NOT BE REFUNDED</strong></p>\r\n<p><span style=\"color: #000000;\">&nbsp;</span></p>\r\n<p><span style=\"color: #000000;\"><strong>SWIMMING COACHING SCHEDULE</strong></span></p>\r\n<p><strong>FIRST CAMP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp; 1st Apr to 20th Apr.2019</p>\r\n<p><strong>SECOND CAMP&nbsp;</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; : 22nd Apr to 11th May.2019</p>\r\n<p><strong>THIRD CAMP&nbsp;&nbsp;&nbsp;</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : 13th May to 31st May.2019</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>TIMINGS</strong></p>\r\n<p><strong>MORNING SESSION&nbsp;</strong>&nbsp; :&nbsp; 07.30am to 8.30am</p>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; 10.00am to 11.00am</p>\r\n<p>&nbsp;</p>\r\n<p><strong>EVENING SESSION</strong>&nbsp;&nbsp;&nbsp; : 03.30pm to 0 4.30pm</p>\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 04.30pm to&nbsp; 05.30pm &nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>FEES STRUCTURE</strong></p>\r\n<p>MEMBER\'S CHILDREN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp; - Rs.2,000/-</p>\r\n<p>NON- MEMBER\'S CHILDREN &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - Rs.3,000/-</p>', '', 'FORMS', 'uploads/coaching/Swim045245_KSLTA.pdf', '', 'SUMMER COACHING CAMP SWIMMING', 'SUMMER COACHING CAMP SWIMMING', 0, '2016-09-28 14:46:25', '2019-03-14 10:19:26', 'A'),
(11, 6, 'GUEST ROOM', 'PHOTO GALLERY WITH THE BEST IN CLASS.', 'uploads/bannerimages/about99258_KSLTA.jpg', 'uploads/bannerimages/guest-house053809_KSLTA.jpg', 0, 'Guest Rooms - Karnataka State Lawn & Tennis Association', 'guestroom', 'KSLTA FACILITIES', 'TERMS & CONDITIONS', '<h3><strong>TERMS AND CONDITIONS</strong></h3>\r\n<div class=\"bulleted_list\">\r\n<p>1. Rooms shall be reserved only for Members of the Club, their guests and affiliated club members.</p>\r\n<p>2. Right of admission is reserved and rests with the Management.</p>\r\n<p>3. The tariff (excludes food and beverages) per day per room for the Seven (07) Air- Conditioned Deluxe Room is</p>\r\n<p>&nbsp; i. Rs.2,200+ Applicable taxes as per GOK/GOI rules</p>\r\n<p>&nbsp; ii. Rs.600 (for extra bed). Applicable taxes as per GOK/GOI rules</p>\r\n<p>iii. Rs.2500+Applicabe taxes for&nbsp; Affiliated Club Members.</p>\r\n<p>4. Affiliated club members cannot bring in any guest along with them to stay at the club, all the occupants should be member of the affiliated club.</p>\r\n<p>5. Affiliated club members, including spouse &amp; children are requested to produce introduction letter and club ID from the respective club, at the time of check-in.</p>\r\n<p>6. All other guest must produce the Passport/Photo ID proof during check in.</p>\r\n<p>7. Affiliated club members should sent the DD in advance for confirmation, which should reach 4 days before their date of arrival, failing which the booking stands cancelled without any prior intimation and the club will not be responsible in this regard.</p>\r\n<p>8. Rates are shown for double occupancy, extra person charges applicable for more than two persons including children above 6 years of age for all types of rooms.</p>\r\n<p>9. Stay for max. 7 days for members guest and affiliated club member, members no limit.</p>\r\n<p>10. Luxury taxes and services charges will be levied as applicable.</p>\r\n<p>11. Not more than 3 persons are allowed in the room.</p>\r\n<p>12. Payment for food, drink and other services shall be made only by coupons and not by cash (coupons should be purchased in advance).</p>\r\n<p>13. Playing cards, keeping pet animals or allowing persons unauthorized stay in the room are prohibited.</p>\r\n<p>14. playing of cassettes, CDs or DVDs or use of television with high volume are prohibited.</p>\r\n<p>15. Occupants shall not wash clothes or hang clothes on the windows or walls.</p>\r\n<p>16. Damages to the fixtures and fittings shall have to be paid before check out.</p>\r\n<p>17. When the occupants go out, windows be kept closed, switch off the lights and heater.</p>\r\n<p>18. One daily English or Kannada News Paper will be provided</p>\r\n<p>19. Telephone service shall be available from 7.00 AM to 11.00 PM</p>\r\n<p>20. The Time For Check-In &amp; Check Out Shall be 12 Noon. If check out time exceeds by two (02) hours 25% of the Room rent Shall be charged, for more than Two (02) hours and up to Six (06) hours 50% of the Room Rent Shall be charged and if it Exceeds Six (06) hours, Full day rent shall charged and the occupants will be allowed to stay subject to availability.</p>\r\n<p>21. Occupants shall inform the Room Supervisor/Bearer in advance to kept the bills ready before check out.</p>\r\n<p>22. Main gate will be closed at 10.30P.M. Guest with vehicle can check-out at 8.00A.M. on week days</p>\r\n</div>', 'CANCELLATION CHARGES', '<h3><strong>CANCELLATION CHARGES</strong></h3>\r\n<div class=\"bulleted_list\">\r\n<ul>\r\n<li>I. If cancelled before 72 hrs, no charges</li>\r\n<li>ii. If cancelled between 72 - 48 hrs, 25% rent</li>\r\n<li>iii. If cancelled between 48 &ndash; 24 hrs, 50% rent</li>\r\n<li>iv. If cancelled less than 24 hrs, 1 day rent</li>\r\n<li>vi. Advance check outs shall be treated as cancellation for the remaining period and the rule for cancellation shall apply.</li>\r\n</ul>\r\n</div>', '', 'CATERING & BAR HOURS', '', '<h4><strong>CATERING</strong></h4>\r\n<table>\r\n<tbody>\r\n<tr style=\"height: 49px;\">\r\n<td style=\"width: 71.0333px; height: 49px;\">S:NO</td>\r\n<td style=\"width: 165.217px; height: 49px;\">DETAILS</td>\r\n<td style=\"width: 375.35px; height: 49px;\">TIMING</td>\r\n<td style=\"width: 259.4px; height: 49px;\">REMARKS</td>\r\n</tr>\r\n<tr style=\"height: 49px;\">\r\n<td style=\"width: 71.0333px; height: 49px;\">1</td>\r\n<td style=\"width: 165.217px; height: 49px;\">Breakfast</td>\r\n<td style=\"width: 375.35px; height: 49px;\">7.30 to 08.30 AM</td>\r\n<td style=\"width: 259.4px; height: 49px;\">Complimentary</td>\r\n</tr>\r\n<tr style=\"height: 25.8px;\">\r\n<td style=\"width: 71.0333px; height: 25.8px;\">2</td>\r\n<td style=\"width: 165.217px; height: 49px;\">Snacks</td>\r\n<td style=\"width: 375.35px; height: 25.8px;\">4.30 to 07.30 PM</td>\r\n<td style=\"width: 259.4px; height: 25.8px;\">At Rates in the Menu Card</td>\r\n</tr>\r\n<tr style=\"height: 49px;\">\r\n<td style=\"width: 71.0333px; height: 49px;\">3</td>\r\n<td style=\"width: 165.217px; height: 49px;\">Lunch &amp; Dinner</td>\r\n<td style=\"width: 375.35px; height: 49px;\">Will be served in the Main Dinning Hall</td>\r\n<td style=\"width: 259.4px; height: 49px;\">At Rates in the Menu Card</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h4><strong>Bar Service (To the rooms): 11.00 AM to 10.30 PM</strong></h4>', 'kslta-guest room', 'kslta-guest room', 1, '2016-09-29 08:57:05', '2019-07-06 10:11:36', 'A'),
(15, 5, 'Party Hall', 'KSLTA Facilities', 'uploads/bannerimages/about77043_KSLTA.jpg', 'uploads/bannerimages/party-hall62094_KSLTA.jpg', 0, 'Party Hall Facilities - Karnataka State Lawn Tennis Association', 'partyhall', 'Party Hall', 'RULES & REGULATIONS', '<h3><strong>RULES &amp; REGULATION</strong></h3>\r\n<ol>\r\n<li>Members desiring to reserve party halls for hosting their own functions shall apply to the Hon. Jt. Secretary in the prescribed form available with the office and club house manager.</li>\r\n<li>During office hour the reservation form and advance amount to be made over to Accounts/ Club House Manager.</li>\r\n<li>The member hosting the party / event must be present during its duration and sign the bill at the end of it, Guest signatures on bills are invalid.</li>\r\n<li>DJ / Live Music is permitted subject to noise levels being within the permissible limits</li>\r\n<li>Sound / Lightings, projector, screen &amp; DJ will be sourced only from club approved vendors and must be paid for by the members.</li>\r\n<li>Banners and placards are not permitted outside the areas being used for the party.</li>\r\n<li>Any Damages and Breakage to club property will be to the member account.</li>\r\n<li>Food, Liquors or other beverages from outside shall not be permitted to be brought in.</li>\r\n<li>Fire works and fire lanterns are nit allowed within the premises.</li>\r\n<li>The menu and the arrangements must be confirmed 72 hours before of the party.</li>\r\n<li>50% of the Amount should be paid at the time of booking.</li>\r\n<li>Party bill should be settled immediately after the party.</li>\r\n</ol>\r\n<h3><strong>NOTE</strong></h3>\r\n<ol>\r\n<li>Party Hall Charges 18% GST Applicable.</li>\r\n</ol>', 'HALL CHARGES', '<div style=\"padding: 30px;\">\r\n<h3><strong>HALL CHARGES</strong></h3>\r\n<ol>\r\n<li>VIP Lounge - Rs.2500 + Tax</li>\r\n<li>Media Center - Rs.3000 + Tax</li>\r\n<li>Garden Area - Rs.15,000 + Tax</li>\r\n<li>Executive Hall - Rs.900 + Tax</li>\r\n</ol>\r\n</div>', '', 'CANCELATION CHARGES', NULL, '<div style=\"padding: 20px;\">\r\n<h3><strong>CANCELATION CHARGES</strong></h3>\r\n<p>Cancellation charges for party halls will be forfeited on the rental charges and not on the advance paid for the food, which will be refunded fully in the event of a cancellation. The cancellations charges will be as follows.</p>\r\n<p style=\"padding-left: 30px;\">1. If cancelled 5 days prior to the party -25% will be forfeited.</p>\r\n<p style=\"padding-left: 30px;\">2. If cancelled 4 days prior to the party -50% will be forfeited.</p>\r\n<p style=\"padding-left: 30px;\">3. If cancelled 3 days prior to the party -70% will be forfeited.</p>\r\n<p style=\"padding-left: 30px;\">4. If cancelled less than 3 days prior to the date of the party 80% will be forfeited.</p>\r\n</div>', 'Party Hall Facilities', 'Party Hall Facilities', 1, '2016-10-14 08:13:19', '2019-04-17 10:14:21', 'A'),
(18, 7, 'Swimming Pool', 'KSLTA Facilities', 'uploads/bannerimages/about60392_KSLTA.jpg', 'uploads/bannerimages/swimming-pool60548_KSLTA.jpg', 0, 'Swimming Pool - Karnataka State Lawn Tennis Association', 'swimming_pool', 'SWIMMING CLASS TIMING', 'RULES & REGULATIONS', '<h4><strong>RULES &amp; REGULATIONS</strong></h4>\r\n<p style=\"padding-left: 30px;\">1. Before using the swimming pool each individual member must deposit his / her identity card with the pool attendant<br />2. Please sign the respective register before entering the pool.<br />3. Every one must have a shower before entering the pool.<br />4. Swimming is allowed only in proper swimming costume.<br />5. Smoking / drinking / eating around the pool and in the dressing room is strictly prohibited.<br />6. Use oil / Greasy lubricants on the body before entering the pool is strictly prohibited.<br />7. Please do not swim if you are not well. Persons having wounds, epilepsy, heart diseases etc.<br />8. pets are not allowed near the pool<br />9. Pool area is not a place to throw loose objects, neither should it be used to wash hands and legs etc.<br />10. Timings to be strictly adhered to and no exceptions will be allowed.<br />11. do not swim after consuming alcohol<br />12. No diving &amp; games are permitted in the pool<br />13. Kids should be accompanied by adult<br />14. None, including those knowing swimming, should enter the pool without the presence of the life guard. This is a stator requirement.<br />15. Any one causing any damages to the pool or any other club property will have to reimburse the cost for rectification.<br />16. please do not bring valuables with you, the club is not responsible for loss / damage to your personal properties<br />17. The club will take care of all safety aspects but still swimming is one&rsquo;s personal risk, club is not responsible for any injury.<br />18. Please note that we are constrained to allow only one hour for swimming in a day during such season.<br />19. Any violation of the above rules by any member will be viewed very seriously.</p>', 'TIMINGS', '<p><strong><span style=\"color: #000000;\">Monday to Saturday</span></strong></p>\r\n<p>Session I :&nbsp;&nbsp; 7.00 a.m to 11.00 a.m<br /><br />Session II :&nbsp; 4.30 p.m to 08.30 p.m</p>\r\n<p><br /><strong><span style=\"color: #000000;\">Sunday &amp; General Holidays :</span></strong></p>\r\n<p>Session I :&nbsp;&nbsp; 7.00 a.m to&nbsp;&nbsp; 01 .00 p.m<br /><br />Session II :&nbsp; 4.30 p.m to 08. 30 p.m<br /><br /><span style=\"color: #000000;\"><strong>Monday Holiday</strong></span></p>', '', 'POOL CHARGES', NULL, '<h4>POOL CHARGES</h4>\r\n<p style=\"padding-left: 30px;\">a) Members Monthly Subscriptions - 250 + Tax&nbsp; <br />B) Dependent Monthly subscription &ndash; 250 + TAX ( Dependant below 21yrs)<br />c) Member Guest per head Week Days &ndash; 100 + Tax<br />d) Members Guest per head Week end &amp; General Holidays - 125 + TAX</p>', 'Description', 'Swimming Pool', 0, '2016-10-24 05:47:02', '2019-05-09 14:08:58', 'A'),
(20, 20, 'Tennis Timing & Charges', 'KSLTA Facilities', 'uploads/bannerimages/about26007_KSLTA.jpg', 'uploads/bannerimages/stadium070350_KSLTA.jpg', 0, 'Tennis Timing & Charges - KSLTA Facilities', 'tennis_timing_charges', 'Tennis Timing & Charges', 'RULES & REGULATIONS', '<h3><strong>RULES &amp; REGULATION</strong></h3>\r\n<ol style=\"list-style-type: upper-alpha;\">\r\n<li>The Occupation of courts will be on first come first basis. Every member should write his/her name ore instruct the marker or write his/her name as per the seniority existing on the black board. Black boards are provided to all the courts). The total time limit per court per session is to a limit of 45 minutes only.<br />&nbsp;&nbsp;&nbsp;</li>\r\n<li>The member(s) should check out after the completion of a set and check in if he/she have a chance to play according to the seniority on the black board.</li>\r\n<li>The member shall be present at the tennis court when his/her chance to play comes up. If he/she is not present and if another member enters the tennis court, then the member who lost the chance cannot demand entry into the tennis court for that set.</li>\r\n<li>If some members have played two sets already and are awaiting along with other members who have not played two sets, the eligibility to play would be determined as follows:<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; A.&nbsp; A fresher (if more members are in this category, then, member who arrived earlier is eligible.<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; B.&nbsp; Member who has played only one set (if more members are in this category, then, who arrived is eligible.<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; C.&nbsp; If all the members have played two sets, then, the earliest among this category will be eligible to play.</li>\r\n<li>Singles can be played if there are less than four members per court. When two members are playing singles and if the total number of games played in the set is six games, these two members should be allowed to complete the set (set means &lsquo;Eleven&rsquo; games)</li>\r\n<li>If a member is playing singles with the &lsquo;Marker&rsquo;, if a member arrives and if the total number of games is &lsquo;Three&rsquo; or less, then the Marker should vacate and the member can enter and start a fresh set. If the total number of games is more than three games, the, it is left to the discretion of the member who has arrived about playing. If he/she intends to join, the play will be continued without any change in the score at that point and will be considered that the member has played a set</li>\r\n<li>If the member is not playing a set but only rallying with the marker and a member arrives, then, the marker should vacate the court and allow the member to play.</li>\r\n<li>Those members who would like to play with their own playing partners can book court No. 4 (from 6.30 to 8.30 p.m.) in advance and the playing time is maximum of 40 minutes (whether it is rallying or playing a set). The booking is by writing on the black board available in the court. The members who intend to play on this court are not eligible to play on the remaining three courts and vice versa, provided the courts are empty.<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; A.&nbsp; Suppose a member &lsquo;A&rsquo; has booked the court and is playing singles with a member &lsquo;B, then the member &lsquo;B&rsquo; cannot book for the next session immediately. Both members &lsquo;A&rsquo; &lsquo;B&rsquo; chances will be after the next batch of fresher complete playing , i.e. their seniority will be last in the order.<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; B.&nbsp; Even in doubles the above condition (i) applies<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; C.&nbsp; Only a member can do the booking of this court. No dependant member can book and play on this court.<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; D.&nbsp; A dependant member can play on this court provided the parent of the dependant books and plays with him/her.<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; E.&nbsp; Member(s) cannot book this court and allow ether dependant(s) to play. The member should be involved in playing.</li>\r\n<li>Ball &amp; Racquet abuse, use of foul language on the tennis court, repeated talking during play, which causes disturbance to the other members on the court or to the one in the neighboring court, shall be strictly dealt with.</li>\r\n<li>Member(s) should register the name(s) of the/their guest(s) by paying the guest fee of Rs. 150/- + Tax(Rupees One Hundred Fifty only ) on week Days &amp; Weekend and General Holidays 200+Tax per guest at the security table. Members introducing a guest to the club shall be responsible for his/her compliance of the rules of the Club House.</li>\r\n<li>A Member may bring in not more than three guests at a time. The Member should be present with the guest(s) at all times and play with the guests.</li>\r\n<li>No member will introduce as his/her guest more than thrice a month. Suppose &lsquo;X&rsquo; is a guest for a member &lsquo;P&rsquo; three times in a particular month, then, any other member to the club cannot entertain this guest &lsquo;X&rsquo; in that particular month.</li>\r\n<li>If a member intends to play with a coach of KSLTA or a ward of the coaching, the member should register him/her as a guest only. The rule 14 applies in this case also.</li>\r\n<li>Dependant members cannot entertain guests to the club.</li>\r\n<li>Members are requested to follow the dress code mentioned below.<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; A.&nbsp; T-Shirt, shorts or tracksuit and tennis shoes for boys/men.<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; B.&nbsp; T-Shirt, Shorts or frock or tracksuit and tennis shoes for girls/ladies.</li>\r\n<li>Members are requested not to pressurize the marker/manager for extension of playing hours and making the courts available for play when the courts are wet and not fit for play. Such incidents shall be dealt with strictly.</li>\r\n<li>If the marker(s) has/have played continuously with member(s), members are requested to give the marker(s) ample time to recoup themselves.</li>\r\n<li>Members are requested not to abuse the staff of the club house if there is any problem or indiscipline by the staff of the club house. The same may be registered in the complaint book available with the security personnel or in writing about the incident to the Secretary.</li>\r\n<li>During Major National/International Tournaments held under the auspices of KSLTA, the facilities at the club house shall not be available to the member (Rule 14.04).<strong><br /></strong></li>\r\n<li>Members on Sat &amp; Sundays the inner 3 Courts are available from 6.30a.m to 10.30a.m&nbsp; and from 4.30p.m to 8.30p.m. the 4th court is available from 6.30a.m to 8.30p.m.</li>\r\n</ol>', 'TIMINGS', '<h3><strong>COURT TIMINGS</strong></h3>\r\n<p>&nbsp;ONLY FOR CLUB HOUSE MEMBERS GUEST</p>\r\n<table style=\"height: 20px; margin: 20px; width: 868px; float: left;\" cellspacing=\"15\" cellpadding=\"20px\">\r\n<thead>\r\n<tr style=\"height: 49px;\">\r\n<td style=\"height: 49px; width: 50px; text-align: left;\">S.NO</td>\r\n<td style=\"height: 49px; width: 150px; text-align: left;\">SESSION</td>\r\n<td style=\"height: 49px; width: 300px; text-align: left;\">DAYS</td>\r\n<th style=\"height: 49px; width: 250.867px; text-align: left;\" scope=\"row\">TIMINGS</th>\r\n<td style=\"height: 49px; width: 149.133px; text-align: left;\">REMARKS</td>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr style=\"height: 22.4px; padding: 20px;\">\r\n<td style=\"height: 22.4px; width: 50px; text-align: left;\">1</td>\r\n<td style=\"height: 22.4px; width: 150px; text-align: left;\">Morning</td>\r\n<td style=\"height: 22.4px; width: 300px; text-align: left;\">Weekdays</td>\r\n<td style=\"height: 22.4px; width: 250.867px; text-align: left;\">\r\n<p>6.30 a.m to 8.30 a.m</p>\r\n<p>8.30 a.m to 9.30 a.m</p>\r\n</td>\r\n<td style=\"height: 22.4px; width: 149.133px; text-align: left;\">\r\n<p>Courts 1 &amp; 2</p>\r\n<p>All 4 Courts</p>\r\n</td>\r\n</tr>\r\n<tr style=\"height: 49px; padding: 20px;\">\r\n<td style=\"height: 49px; width: 50px; text-align: left;\">2</td>\r\n<td style=\"height: 49px; width: 150px; text-align: left;\">&nbsp;</td>\r\n<td style=\"height: 49px; width: 300px; text-align: left;\">Sundays and *General Holidays</td>\r\n<td style=\"height: 49px; width: 250.867px; text-align: left;\">\r\n<p>6.30 a.m to 10.30 a.m</p>\r\n<p>10.30 a.m to 12.00p.m</p>\r\n</td>\r\n<td style=\"height: 49px; width: 149.133px; text-align: left;\">\r\n<p>All 4 courts</p>\r\n<p>4th Court Only</p>\r\n</td>\r\n</tr>\r\n<tr style=\"height: 49px; padding: 20px;\">\r\n<td style=\"height: 49px; width: 50px; text-align: left;\">3</td>\r\n<td style=\"height: 49px; width: 150px; text-align: left;\">Evening</td>\r\n<td style=\"height: 49px; width: 300px; text-align: left;\">Weekdays</td>\r\n<td style=\"height: 49px; width: 250.867px; text-align: left;\">\r\n<p>4.30 p.m to 6.30 p.m</p>\r\n<p>6.30 p.m to 8.30 p.m</p>\r\n</td>\r\n<td style=\"height: 49px; width: 149.133px; text-align: left;\">\r\n<p>Courts 1 &amp; 2</p>\r\n<p>All 4 courts</p>\r\n</td>\r\n</tr>\r\n<tr style=\"height: 49px; padding: 20px;\">\r\n<td style=\"height: 49px; width: 50px; text-align: left;\">4</td>\r\n<td style=\"height: 49px; width: 150px; text-align: left;\">&nbsp;</td>\r\n<td style=\"height: 49px; width: 300px; text-align: left;\">Sundays and *General Holidays</td>\r\n<td style=\"height: 49px; width: 250.867px; text-align: left;\">\r\n<p>12.&nbsp; p.m to 3.30 p.m</p>\r\n<p>3.30 p.m to 8.30 p.m</p>\r\n</td>\r\n<td style=\"height: 49px; width: 149.133px; text-align: left;\">\r\n<p>4th Court Only</p>\r\n<p>All 4 Courts</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<p><strong>* General Holidays are those that are adopted by Government of Karnataka.</strong></p>', '', 'TENNIS CHARGES', NULL, '<div style=\"padding: 10px 30px;\">\r\n<h3><strong>COURT CHARGES</strong></h3>\r\n<ol style=\"list-style-type: upper-alpha;\">\r\n<li><strong>Members Monthly Subscription</strong> - ₹ 325.00 + Tax Per Month</li>\r\n<li><strong>Dependant Monthly Subscrption</strong> - ₹ 200.00 + Tax Per Month</li>\r\n<li><strong>Week Days Guest Charges</strong> &ndash; ₹ 150.00 + Tax</li>\r\n<li><strong>Weekend &amp; General Holidays</strong> - ₹ 200.00 + Tax<strong><br /></strong></li>\r\n</ol>\r\n</div>', 'Description', 'KSLTA Management implemented tennis timings and charges for Tennis Coaching.', 0, '2016-10-24 07:03:50', '2019-07-06 09:58:49', 'A');
INSERT INTO `ksl_coaching_pages` (`coaching_id`, `gallery_id`, `coaching_top_main_heading`, `coaching_top_sub_heading`, `coaching_top_background_img`, `content_portfolio_img`, `remove_content_img`, `coaching_page_title`, `coaching_page_name`, `coaching_content_heading`, `coaching_tab_one_title`, `coaching_tab_one_content`, `coaching_tab_two_title`, `coaching_tab_two_content_1`, `coaching_tab_two_content_2`, `coaching_tab_three_title`, `coaching_file_upload`, `coaching_tab_three_content_1`, `coaching_meta_tag_name`, `coaching_meta_tag_description`, `coaching_assign_sidebar`, `coaching_created_at`, `coaching_modified_at`, `coaching_status`) VALUES
(21, 8, 'HEALTH CLUB', 'KSLTA Facilities', 'uploads/bannerimages/about44332_KSLTA.jpg', '', 1, 'HEALTH CLUB Facilities - Karnataka State Lawn Tennis Association', 'healthclub', 'HEALTH CLUB TERMS & CONDITIONS', 'GYM ', '<h3><strong>RULES &amp; REGULATIONS</strong></h3>\r\n<ol style=\"list-style-type: upper-alpha;\">\r\n<li>Members and Dependent Members should sign the register on entering the Gym and are required to produce their I.D. Card on demand.</li>\r\n<li>Only the Gym Instructor operate the Music System.</li>\r\n<li>Members / Dependents / Guests are requested not to operate the T.V. The Gym Instructor will play any of the following Channels (without sound) on specific instructions from the Chairman, Health Club.\r\n<pre style=\"border: none; padding: 5px;\"> i. Sports Channel</pre>\r\n<pre style=\"border: none; padding: 5px;\"> ii. National Geographic / Discovery</pre>\r\n</li>\r\n<li>Members are requested to write their names in the &lsquo;Q&rsquo; for any one Tread Machine / Cross Trainer only.</li>\r\n<li>Members who are waiting for the Tread Machine / Cross Trainer are not permitted to give their chance / turn for other members who come late or their turn is at last.</li>\r\n<li>Members are not allowed to write other members name if they are physically not present in the Gym.</li>\r\n<li>Members are requested to use the Tread Machine for 20 Minutes and Cross Trainer for 15 Minutes only.</li>\r\n<li>Members are requested not to Bring outside Coaches for Health Club. It Should Strictly be Followed.</li>\r\n<li>Member(s) should register the name(s) of the/their guest(s) by paying the guest fee of Rs. 150/- + Tax(Rupees One Hundred Fifty only ) on week Days &amp; Weekend and General Holidays 200+Tax per guest at the Gym table. Members introducing a guest to the club shall be responsible for his/her compliance of the rules of the Club House</li>\r\n<li>A Member may bring in not more than three guests at a time. The Member should be present with the guest(s) at all times and play with the guests.</li>\r\n<li>No member will introduce as his/her guest more than thrice a month. Suppose &lsquo;X&rsquo; is a guest for a member &lsquo;P&rsquo; three times in a particular month, then, any other member to the club cannot entertain this guest &lsquo;X&rsquo; in that particular month.</li>\r\n</ol>\r\n<h4>&nbsp;</h4>\r\n<h3><strong>Dress Code For Gymnasium</strong></h3>\r\n<p><strong>Gents: </strong>Should wear Track Pant &amp; T-Shirt / Shorts &amp; T-Shirts with only Sports Shoe.<strong><br />Ladies: </strong>Track Pant &amp; T-Shirt / Shorts &amp; T-Short / Salwar Kameez with only Sports Shoe. Bare feet &amp; Slippers are not allowed.</p>\r\n<p>&nbsp;</p>\r\n<h3><strong>GYM Timings</strong></h3>\r\n<h6>6.30 a.m. to 2.30 p.m. &amp; 4.00 p.m. to 9.00 p.m</h6>\r\n<p>&nbsp;</p>\r\n<h3><strong>CHARGES</strong></h3>\r\n<h6><strong>Members Monthly Subscription - ₹ 300 + Tax Per Month</strong></h6>\r\n<h6><strong>Dependant Monthly Subscrption - ₹ 200 + Tax Per Month</strong></h6>\r\n<h6><strong>Week Days Guest Charges &ndash; ₹ 150 + Tax</strong></h6>\r\n<h6><strong>Weekend &amp; General Holidays - ₹ 200 + Tax</strong></h6>', 'SOUNA & STEAM ', '', '', 'DE- STRESS SPA ', 'uploads/coaching/1 - (Copy)081123_KSLTA.jpg', '<table style=\"width: 776px;\">\r\n<tbody>\r\n<tr style=\"height: 22px;\">\r\n<td style=\"height: 22px; width: 383px;\">\r\n<h4 style=\"text-align: left;\"><span style=\"color: #ff0000;\"><strong>Theraphy Name</strong></span></h4>\r\n</td>\r\n<td style=\"height: 22px; width: 92px;\">\r\n<h4 style=\"text-align: center;\"><span style=\"color: #ff0000;\"><strong>Duration</strong></span></h4>\r\n</td>\r\n<td style=\"height: 22px; width: 93.2167px;\">\r\n<h4 style=\"text-align: center;\"><span style=\"color: #ff0000;\"><strong>As Per Price</strong></span></h4>\r\n</td>\r\n<td style=\"height: 22px; width: 73.7833px;\">\r\n<h4 style=\"text-align: center;\"><span style=\"color: #ff0000;\"><strong>Discount @ 20%</strong></span></h4>\r\n</td>\r\n<td style=\"height: 22px; width: 112px;\">\r\n<h4 style=\"text-align: center;\"><span style=\"color: #ff0000;\"><strong>After Disount Price</strong></span></h4>\r\n</td>\r\n</tr>\r\n<tr style=\"height: 24px;\">\r\n<td style=\"height: 24px; width: 383px;\">\r\n<h5><strong><span style=\"color: #ff0000;\">Pain Relief Programme</span></strong></h5>\r\n</td>\r\n<td style=\"height: 24px; width: 92px;\">&nbsp;</td>\r\n<td style=\"height: 24px; width: 93.2167px;\">&nbsp;</td>\r\n<td style=\"height: 24px; width: 73.7833px;\">&nbsp;</td>\r\n<td style=\"height: 24px; width: 112px;\">&nbsp;</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Swedish Massage</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">1899.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 380.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,519.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Swedish Massage with Wintergreen Oil</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">2249.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 450.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,799.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Back Massage</td>\r\n<td style=\"height: 48px; width: 92px;\">30 min</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">1049.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 210.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 839.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Back Massage with Wintergreen Oil</td>\r\n<td style=\"height: 48px; width: 92px;\">30 min</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">1199.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 240.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 959.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Green Tea &amp; Spice Scrubassage</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">2549.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 510.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2,039.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Car Commuters\' Package - Classic</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr 30 min</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">2749.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 550.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2,199.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Car Commuters\' Package - Premium</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr 30 min</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">3299.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 660.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2,639.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Deep Relaxation Package</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr 30 min</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">3099.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 620.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2,479.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">\r\n<h5><strong><span style=\"color: #ff0000;\">Anxiety Relief Programme</span></strong></h5>\r\n</td>\r\n<td style=\"height: 48px; width: 92px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Aromatherapy Massage (Relaxing)</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">2249.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 450.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,799.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Head-Neck-Shoulder Massage</td>\r\n<td style=\"height: 48px; width: 92px;\">30 min</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">1049.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 210.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 839.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Working Professionals\' Package - Classic</td>\r\n<td style=\"height: 48px; width: 92px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">1999.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 400.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,599.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Working Professionals\' Package - Premium</td>\r\n<td style=\"height: 48px; width: 92px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">2149.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 430.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,719.00</td>\r\n</tr>\r\n<tr style=\"height: 20.25px;\">\r\n<td style=\"height: 20.25px; width: 383px;\">\r\n<h5><strong><span style=\"color: #ff0000;\">Better Sleep Programme</span></strong></h5>\r\n</td>\r\n<td style=\"height: 20.25px; width: 92px;\">&nbsp;</td>\r\n<td style=\"height: 20.25px; width: 93.2167px;\">&nbsp;</td>\r\n<td style=\"height: 20.25px; width: 73.7833px;\">&nbsp;</td>\r\n<td style=\"height: 20.25px; width: 112px;\">&nbsp;</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Shirodhara</td>\r\n<td style=\"height: 48px; width: 92px;\">45 min</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">2249.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 450.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,799.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Frequent Fliers\' Package</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr 45 min</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">4299.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 860.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3,439.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">\r\n<h5><strong><span style=\"color: #ff0000;\">Anti - Fatigue Programme</span></strong></h5>\r\n</td>\r\n<td style=\"height: 48px; width: 92px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Aromatherapy Massage (Energising)</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">2249.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 450.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,799.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Thai Massage</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">2149.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 430.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,719.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Foot Reflexology</td>\r\n<td style=\"height: 48px; width: 92px;\">30 min</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">&nbsp; 949.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 190.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 759.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Home Makers\' Package - Classic</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr 30 min</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">2649.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 530.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2,119.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Home Makers\' Package - Premium</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr 30 min</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">3049.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 610.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2,439.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">\r\n<h5><strong><span style=\"color: #ff0000;\">AO Detox Programme</span></strong></h5>\r\n</td>\r\n<td style=\"height: 48px; width: 92px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Palmarosa &amp; Ylang Ylang Body Polish</td>\r\n<td style=\"height: 48px; width: 92px;\">30 min</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">1899.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 380.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,519.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Natural Protein Body Wrap</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">2649.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 530.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2,119.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Immunity Booster Programme</td>\r\n<td style=\"height: 48px; width: 92px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Ayurvedic Massage</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">1899.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 380.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,519.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">\r\n<h5><strong><span style=\"color: #ff0000;\">Skin Whitening Programme</span></strong></h5>\r\n</td>\r\n<td style=\"height: 48px; width: 92px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Aromatherapy Massage (Radiating)</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">2399.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 480.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,919.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Coffee &amp; Cane Sugar Body Polish</td>\r\n<td style=\"height: 48px; width: 92px;\">30 min</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">1899.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 380.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,519.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Sea-Weed Body Wrap</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">3049.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 610.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2,439.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Lightening Facial</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">1999.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 400.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,599.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Lightening Facial with Ocean Mist</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">2349.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 470.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,879.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Healing Facial (Acne-prone Skin)</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">1999.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 400.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,599.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Hydrating Facial (Normal &amp; Dry Skin)</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">1899.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 380.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,519.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Rejuvenating Facial (Combination &amp; Oily Skin)</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">1899.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 380.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,519.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">\r\n<h5><strong><span style=\"color: #ff0000;\">Age Reversal Programme</span></strong></h5>\r\n</td>\r\n<td style=\"height: 48px; width: 92px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Vinotherapy Massage</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">2549.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 510.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2,039.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Vinotherapy Body Wrap</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">3049.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 610.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2,439.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Bamboo Lime Scrub</td>\r\n<td style=\"height: 48px; width: 92px;\">30 min</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">1899.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 380.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,519.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Green Apple &amp; Cinnamon Body Wrap</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">2649.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 530.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2,119.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Age-Defying Facial</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">1999.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 400.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,599.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Age-Defying Facial with Sea Silt Mask</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">2349.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 470.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,879.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\"><strong><span style=\"color: #ff0000;\">Holistic Packages</span></strong></td>\r\n<td style=\"height: 48px; width: 92px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Ladies Sojourn - Classic</td>\r\n<td style=\"height: 48px; width: 92px;\">2 hrs 30 min</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">5699.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,140.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4,559.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Ladies` Day Out - Premium</td>\r\n<td style=\"height: 48px; width: 92px;\">3 hrs</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">6899.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,380.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5,519.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Gentlemen\'s Retreat - Classic</td>\r\n<td style=\"height: 48px; width: 92px;\">2 hrs 30 min</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">5349.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,070.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4,279.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Gentlemen\' Retreat - Premium</td>\r\n<td style=\"height: 48px; width: 92px;\">3 hr</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">6599.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,320.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5,279.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Oriental Secret</td>\r\n<td style=\"height: 48px; width: 92px;\">2 hrs 30 min</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">4749.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 950.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3,799.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Ayurvedic Wisdom</td>\r\n<td style=\"height: 48px; width: 92px;\">1 hr 45 min</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">3899.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 780.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3,119.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">\r\n<h5><strong><span style=\"color: #ff0000;\">Bride &amp; Groom Packages</span></strong></h5>\r\n</td>\r\n<td style=\"height: 48px; width: 92px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Beautiful Bride Package</td>\r\n<td style=\"height: 48px; width: 92px;\">2 sessions</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">10499.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2,100.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 8,399.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Glorious Groom Package</td>\r\n<td style=\"height: 48px; width: 92px;\">2 sessions</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">9899.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,980.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 7,919.00</td>\r\n</tr>\r\n<tr style=\"height: 10px;\">\r\n<td style=\"height: 10px; width: 383px;\">\r\n<h5><strong><span style=\"color: #ff0000;\">Add-Ons</span></strong></h5>\r\n</td>\r\n<td style=\"height: 10px; width: 92px;\">&nbsp;</td>\r\n<td style=\"height: 10px; width: 93.2167px;\">&nbsp;</td>\r\n<td style=\"height: 10px; width: 73.7833px;\">&nbsp;</td>\r\n<td style=\"height: 10px; width: 112px;\">&nbsp;</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Head Massage (Relaxing)</td>\r\n<td style=\"height: 48px; width: 92px;\">10 min</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">349.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 70.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 279.00</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"height: 48px; width: 383px;\">Head Massage (Energising)</td>\r\n<td style=\"height: 48px; width: 92px;\">10 min</td>\r\n<td style=\"height: 48px; width: 93.2167px;\">349.00</td>\r\n<td style=\"height: 48px; width: 73.7833px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 70.00</td>\r\n<td style=\"height: 48px; width: 112px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 279.00</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'Description', 'Gym Facilities at KSLTA', 0, '2016-10-24 12:00:24', '2018-02-21 10:12:59', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `ksl_event`
--

CREATE TABLE `ksl_event` (
  `id` bigint(20) NOT NULL,
  `event_title` varchar(255) NOT NULL,
  `event_header` varchar(255) NOT NULL,
  `event_name` varchar(255) NOT NULL,
  `event_url` varchar(255) NOT NULL,
  `order_no` bigint(12) NOT NULL,
  `event_img` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ksl_event`
--

INSERT INTO `ksl_event` (`id`, `event_title`, `event_header`, `event_name`, `event_url`, `order_no`, `event_img`, `created_at`) VALUES
(1, 'RT NARAYANAN MEMORIAL CUP', 'Karman – Druthi Claim Second Crown In Two Weeks Druthi Claim Second Crown In Two Weeks Druthi Claim Second Crown In Two Weeks', 'Top seed and world No 476 Prerna Bhambri completed a hat-trick of wins at Kalaburagi adding the RGUHS ITF Kalaburagi Open to the KUDA ITF', 'table_tennis', 1, 'uploads/title/Karman-Druthi-Claim-Second-Crown-In-Two-Weeks50106.jpg', '2017-10-13 01:10:14');

-- --------------------------------------------------------

--
-- Table structure for table `ksl_event_drawsheet`
--

CREATE TABLE `ksl_event_drawsheet` (
  `draw_id` bigint(20) NOT NULL,
  `draw_tournamentid` bigint(20) NOT NULL,
  `draw_eventid` bigint(20) NOT NULL,
  `draw_filename` text NOT NULL,
  `draw_filepath` text NOT NULL,
  `draw_datetime` datetime NOT NULL,
  `draw_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ksl_event_drawsheet`
--

INSERT INTO `ksl_event_drawsheet` (`draw_id`, `draw_tournamentid`, `draw_eventid`, `draw_filename`, `draw_filepath`, `draw_datetime`, `draw_created`) VALUES
(6, 1, 1, 'realizing-cosmic-totality.jpg', 'uploads/drawsheets/realizing-cosmic-totality84225.jpg', '2016-09-13 07:28:56', '2016-09-13 10:10:17'),
(10, 13, 74, 'Sunset.jpg', 'uploads/drawsheets/Sunset85330.jpg', '2016-09-13 09:49:13', '2016-09-13 07:49:13'),
(11, 13, 75, 'Blue hills.jpg', 'uploads/drawsheets/Blue hills42327.jpg', '2016-09-13 14:25:31', '2016-09-13 12:25:32'),
(12, 13, 76, 'Winter.jpg', 'uploads/drawsheets/Winter56164.jpg', '2016-09-13 14:30:28', '2016-09-13 12:30:28'),
(13, 1, 1, 'Shipbazar_30-08-2016-154635.xls', 'uploads/drawsheets/Shipbazar_30-08-2016-1546354550.xls', '2016-09-13 14:48:16', '2016-09-13 12:48:16'),
(15, 13, 74, 'Winter.jpg', 'uploads/drawsheets/Winter53024.jpg', '2016-09-13 15:49:32', '2016-09-13 13:49:32'),
(16, 13, 75, 'Water lilies.jpg', 'uploads/drawsheets/Water lilies4177.jpg', '2016-09-13 15:49:55', '2016-09-13 13:49:55'),
(17, 13, 76, 'Sunset.jpg', 'uploads/drawsheets/Sunset67120.jpg', '2016-09-13 15:50:27', '2016-09-13 13:50:27'),
(18, 13, 77, 'Blue hills.jpg', 'uploads/drawsheets/Blue hills18957.jpg', '2016-09-13 15:50:41', '2016-09-13 13:50:41'),
(19, 13, 77, 'Water lilies.jpg', 'uploads/drawsheets/Water lilies90554.jpg', '2016-09-13 15:50:50', '2016-09-13 13:50:50'),
(21, 14, 81, 'Fall-2015-Newsletter.pdf', 'uploads/drawsheets/Fall-2015-Newsletter94012.pdf', '2016-09-27 07:00:50', '2016-09-27 05:00:50'),
(22, 18, 101, 'B-U-10.jpg', 'uploads/drawsheets/B-U-1066960.jpg', '2016-10-21 00:44:52', '2016-10-21 05:44:52'),
(23, 18, 102, 'G-U-10.jpg', 'uploads/drawsheets/G-U-1025083.jpg', '2016-10-21 00:45:13', '2016-10-21 05:45:13'),
(24, 18, 103, 'Womens-Doubles-friday.jpg', 'uploads/drawsheets/Womens-Doubles-friday65001.jpg', '2016-10-21 00:45:40', '2016-10-21 05:45:40'),
(25, 18, 104, 'BOYS-DOUBLES-NEW-TIME.jpg', 'uploads/drawsheets/BOYS-DOUBLES-NEW-TIME61268.jpg', '2016-10-21 00:46:03', '2016-10-21 05:46:03'),
(26, 92, 457, 'BOYS SINGLES.pdf', 'uploads/drawsheets/BOYS SINGLES26669.pdf', '2017-01-28 04:51:59', '2017-01-28 10:51:59'),
(27, 92, 458, 'Girls Singles Maindraw.pdf', 'uploads/drawsheets/Girls Singles Maindraw20048.pdf', '2017-01-28 04:52:13', '2017-01-28 10:52:13'),
(28, 92, 459, 'BOYS DOUBLES.pdf', 'uploads/drawsheets/BOYS DOUBLES4824.pdf', '2017-01-28 04:52:25', '2017-01-28 10:52:25'),
(29, 92, 460, 'GIRLS DOUBLES.pdf', 'uploads/drawsheets/GIRLS DOUBLES43340.pdf', '2017-01-28 04:52:38', '2017-01-28 10:52:38'),
(30, 97, 439, 'mens-draw.pdf', 'uploads/drawsheets/mens-draw89702.pdf', '2017-02-10 02:30:19', '2017-02-10 08:30:19'),
(31, 97, 440, 'm-db.pdf', 'uploads/drawsheets/m-db16925.pdf', '2017-02-10 02:30:35', '2017-02-10 08:30:35'),
(32, 92, 457, 'BOYS SINGLES26669.pdf', 'uploads/drawsheets/BOYS SINGLES2666950546.pdf', '2017-02-11 02:43:28', '2017-02-11 08:43:28'),
(33, 92, 457, 'BOYS SINGLES26669-1.jpg', 'uploads/drawsheets/BOYS SINGLES26669-153535.jpg', '2017-02-11 02:46:23', '2017-02-11 08:46:23'),
(34, 92, 458, 'G-S-U14.jpg', 'uploads/drawsheets/G-S-U146296.jpg', '2017-02-15 23:56:32', '2017-02-16 05:56:32'),
(35, 92, 459, 'B-D-U14.jpg', 'uploads/drawsheets/B-D-U1418890.jpg', '2017-02-15 23:56:44', '2017-02-16 05:56:44'),
(36, 92, 460, 'G-D-U14.jpg', 'uploads/drawsheets/G-D-U1424890.jpg', '2017-02-15 23:56:55', '2017-02-16 05:56:55'),
(37, 97, 439, 'Mens Singles.jpg', 'uploads/drawsheets/Mens Singles72083.jpg', '2017-02-16 00:05:12', '2017-02-16 06:05:12'),
(38, 97, 440, 'Mens Doubles.jpg', 'uploads/drawsheets/Mens Doubles67378.jpg', '2017-02-16 00:05:24', '2017-02-16 06:05:24'),
(39, 118, 493, 'Davis Draw.jpg', 'uploads/drawsheets/Davis Draw87284.jpg', '2017-04-19 23:40:16', '2017-04-20 04:40:16'),
(40, 147, 594, 'Boys CS U-12.pdf', 'uploads/drawsheets/Boys CS U-1230803.pdf', '2017-09-08 03:16:20', '2017-09-08 08:16:20'),
(41, 147, 595, 'Girls U-12 Draw.pdf', 'uploads/drawsheets/Girls U-12 Draw75072.pdf', '2017-09-08 03:16:38', '2017-09-08 08:16:38'),
(42, 147, 594, 'Boys Cs U-12.jpg', 'uploads/drawsheets/Boys Cs U-1248921.jpg', '2017-09-08 03:19:20', '2017-09-08 08:19:20'),
(43, 147, 595, 'Girls Cs U-12.jpg', 'uploads/drawsheets/Girls Cs U-1298510.jpg', '2017-09-08 03:19:32', '2017-09-08 08:19:32');

-- --------------------------------------------------------

--
-- Table structure for table `ksl_event_matches`
--

CREATE TABLE `ksl_event_matches` (
  `em_id` int(11) NOT NULL,
  `em_tournament_id` varchar(50) NOT NULL,
  `em_event_id` varchar(50) NOT NULL,
  `em_no_of_matches` bigint(20) NOT NULL,
  `em_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ksl_event_matches`
--

INSERT INTO `ksl_event_matches` (`em_id`, `em_tournament_id`, `em_event_id`, `em_no_of_matches`, `em_created`) VALUES
(1, '1', '4', 4, '2016-07-01 12:27:43'),
(2, '1', '1', 4, '2016-07-01 12:27:16'),
(3, '1', '3', 32, '2016-06-30 14:38:29'),
(4, '3', '42', 4, '2016-06-02 12:49:33'),
(5, '1', '2', 2, '2016-06-02 13:10:55'),
(6, '8', '57', 16, '2016-07-02 06:03:49'),
(7, '13', '74', 16, '2016-07-06 06:49:40'),
(8, '13', '75', 32, '2016-07-06 06:49:46'),
(9, '13', '76', 32, '2016-07-06 06:49:51'),
(10, '13', '77', 16, '2016-07-06 06:49:56'),
(11, '14', '81', 2, '2016-09-26 10:59:12'),
(12, '18', '101', 32, '2016-10-21 05:44:02'),
(13, '18', '102', 16, '2016-10-21 05:44:10'),
(14, '18', '103', 8, '2016-10-21 05:44:21'),
(15, '18', '104', 8, '2016-10-21 05:44:28'),
(16, '27', '135', 32, '2016-12-25 08:18:45'),
(17, '92', '429', 64, '2017-01-22 10:32:14'),
(18, '92', '430', 64, '2017-01-22 10:32:30'),
(19, '92', '431', 16, '2017-01-22 10:34:38'),
(20, '92', '432', 16, '2017-01-22 10:34:44'),
(21, '92', '457', 2, '2017-03-10 07:24:57'),
(22, '92', '458', 64, '2017-01-28 10:49:31'),
(23, '92', '459', 16, '2017-01-28 10:49:38'),
(24, '92', '460', 16, '2017-01-28 10:49:44'),
(25, '29', '150', 32, '2017-02-10 08:26:45'),
(26, '97', '439', 32, '2017-02-10 08:29:57'),
(27, '97', '440', 16, '2017-02-10 08:30:03'),
(28, '105', '465', 32, '2017-02-25 10:05:51'),
(29, '105', '466', 16, '2017-02-25 10:06:00'),
(30, '118', '493', 4, '2017-04-20 04:39:28'),
(31, '147', '594', 32, '2017-09-08 08:14:41'),
(32, '147', '595', 32, '2017-09-08 08:14:47');

-- --------------------------------------------------------

--
-- Table structure for table `ksl_event_tbl`
--

CREATE TABLE `ksl_event_tbl` (
  `event_id` bigint(20) NOT NULL,
  `tourn_id` bigint(20) NOT NULL,
  `event_category` bigint(20) NOT NULL,
  `event_subcategory` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ksl_event_tbl`
--

INSERT INTO `ksl_event_tbl` (`event_id`, `tourn_id`, `event_category`, `event_subcategory`) VALUES
(1, 1, 1, 3),
(2, 1, 1, 5),
(3, 1, 2, 3),
(4, 1, 2, 4),
(36, 2, 1, 3),
(37, 2, 1, 4),
(38, 2, 2, 4),
(39, 2, 2, 5),
(40, 2, 6, 3),
(42, 3, 1, 3),
(49, 4, 6, 3),
(50, 5, 2, 4),
(51, 5, 2, 5),
(52, 5, 7, 5),
(55, 6, 2, 3),
(56, 7, 6, 4),
(57, 8, 6, 4),
(58, 8, 6, 5),
(59, 8, 6, 3),
(60, 8, 2, 5),
(61, 8, 2, 4),
(62, 9, 6, 3),
(63, 9, 1, 4),
(64, 10, 2, 5),
(65, 11, 2, 4),
(74, 13, 1, 4),
(75, 13, 6, 4),
(76, 13, 2, 4),
(77, 13, 7, 4),
(83, 14, 1, 3),
(84, 14, 1, 4),
(85, 14, 3, 10),
(86, 12, 6, 3),
(87, 12, 7, 5),
(88, 15, 2, 9),
(93, 17, 7, 11),
(94, 17, 7, 13),
(95, 17, 6, 17),
(96, 17, 2, 9),
(101, 18, 1, 9),
(102, 18, 2, 9),
(103, 18, 3, 9),
(104, 18, 4, 9),
(105, 19, 1, 9),
(106, 20, 1, 9),
(111, 21, 1, 9),
(112, 21, 2, 9),
(113, 21, 3, 9),
(114, 21, 4, 9),
(115, 22, 5, 13),
(116, 22, 6, 14),
(117, 22, 7, 13),
(118, 22, 8, 14),
(123, 24, 5, 13),
(124, 24, 6, 14),
(125, 24, 7, 13),
(126, 24, 8, 14),
(127, 25, 1, 12),
(128, 25, 2, 12),
(129, 25, 3, 12),
(130, 25, 4, 12),
(131, 26, 5, 13),
(132, 26, 6, 14),
(133, 26, 7, 13),
(134, 26, 8, 14),
(135, 27, 1, 9),
(136, 27, 2, 9),
(137, 27, 3, 9),
(138, 27, 4, 9),
(139, 28, 1, 11),
(140, 28, 2, 11),
(141, 28, 3, 11),
(142, 28, 4, 11),
(150, 29, 5, 13),
(151, 30, 1, 12),
(152, 30, 2, 12),
(153, 30, 3, 12),
(154, 30, 4, 12),
(155, 31, 1, 10),
(156, 31, 2, 10),
(157, 31, 3, 10),
(158, 31, 4, 10),
(159, 32, 6, 14),
(160, 32, 8, 14),
(161, 33, 1, 11),
(162, 33, 2, 11),
(163, 33, 3, 11),
(164, 33, 4, 11),
(165, 34, 1, 10),
(166, 34, 2, 10),
(167, 34, 3, 10),
(168, 34, 4, 10),
(169, 35, 1, 12),
(170, 35, 2, 12),
(171, 35, 3, 12),
(172, 35, 4, 12),
(173, 36, 1, 29),
(174, 36, 2, 29),
(175, 36, 3, 29),
(176, 36, 4, 29),
(177, 37, 1, 9),
(178, 37, 2, 9),
(179, 37, 3, 9),
(180, 37, 4, 9),
(181, 38, 1, 29),
(182, 38, 2, 29),
(183, 38, 3, 29),
(184, 38, 4, 29),
(185, 39, 1, 9),
(186, 39, 2, 9),
(187, 39, 3, 9),
(188, 39, 4, 9),
(189, 40, 1, 9),
(190, 40, 2, 9),
(191, 40, 3, 9),
(192, 40, 4, 9),
(193, 41, 1, 11),
(194, 41, 2, 11),
(195, 41, 3, 11),
(196, 41, 4, 11),
(197, 42, 1, 9),
(198, 42, 2, 9),
(199, 42, 3, 9),
(200, 42, 4, 9),
(201, 43, 1, 11),
(202, 43, 2, 11),
(203, 43, 3, 11),
(204, 43, 4, 11),
(209, 45, 1, 29),
(210, 45, 2, 29),
(211, 45, 3, 29),
(212, 45, 4, 29),
(213, 46, 1, 10),
(214, 46, 2, 10),
(215, 46, 3, 10),
(216, 46, 4, 10),
(217, 47, 1, 11),
(218, 47, 2, 11),
(219, 47, 3, 11),
(220, 47, 4, 11),
(221, 48, 1, 29),
(222, 48, 2, 29),
(223, 48, 3, 29),
(224, 48, 4, 29),
(225, 49, 1, 12),
(226, 49, 2, 12),
(227, 49, 3, 12),
(228, 49, 4, 12),
(229, 50, 1, 11),
(230, 50, 2, 11),
(231, 50, 3, 11),
(232, 50, 4, 11),
(233, 51, 5, 13),
(234, 52, 1, 9),
(235, 52, 2, 9),
(236, 52, 3, 9),
(237, 52, 4, 9),
(238, 53, 1, 29),
(239, 53, 2, 29),
(240, 53, 3, 29),
(241, 53, 4, 29),
(242, 54, 6, 14),
(243, 55, 1, 11),
(244, 55, 2, 11),
(245, 55, 3, 11),
(246, 55, 4, 11),
(247, 56, 5, 13),
(248, 57, 1, 29),
(249, 57, 2, 29),
(250, 57, 3, 29),
(251, 57, 4, 29),
(252, 58, 1, 9),
(253, 58, 2, 9),
(254, 58, 3, 9),
(255, 58, 4, 9),
(256, 59, 1, 11),
(257, 59, 2, 11),
(258, 59, 3, 11),
(259, 59, 4, 11),
(260, 60, 1, 10),
(261, 60, 2, 10),
(262, 60, 3, 10),
(263, 60, 4, 10),
(264, 61, 1, 29),
(265, 61, 2, 29),
(266, 61, 3, 29),
(267, 61, 4, 29),
(268, 62, 1, 9),
(269, 62, 2, 9),
(270, 62, 3, 9),
(271, 62, 4, 9),
(272, 63, 1, 11),
(273, 63, 2, 11),
(274, 63, 3, 11),
(275, 63, 4, 11),
(276, 64, 1, 12),
(277, 64, 2, 12),
(278, 64, 3, 12),
(279, 64, 4, 12),
(280, 65, 1, 10),
(281, 65, 2, 10),
(282, 65, 3, 10),
(283, 65, 4, 10),
(284, 66, 1, 11),
(285, 66, 2, 11),
(286, 66, 3, 11),
(287, 66, 4, 11),
(288, 67, 1, 9),
(289, 67, 2, 9),
(290, 67, 3, 9),
(291, 67, 4, 9),
(292, 68, 1, 29),
(293, 68, 2, 29),
(294, 68, 3, 29),
(295, 68, 4, 29),
(296, 69, 1, 10),
(297, 69, 2, 10),
(298, 69, 3, 10),
(299, 69, 4, 10),
(316, 73, 1, 29),
(317, 73, 2, 29),
(318, 73, 3, 29),
(319, 73, 4, 29),
(320, 72, 1, 9),
(321, 72, 2, 9),
(322, 72, 3, 9),
(323, 72, 4, 9),
(324, 71, 1, 11),
(325, 71, 2, 11),
(326, 71, 3, 11),
(327, 71, 4, 11),
(328, 70, 1, 12),
(329, 70, 2, 12),
(330, 70, 3, 12),
(331, 70, 4, 12),
(336, 75, 1, 11),
(337, 75, 2, 11),
(338, 75, 3, 11),
(339, 75, 4, 11),
(340, 76, 1, 10),
(341, 76, 2, 10),
(342, 76, 3, 10),
(343, 76, 4, 10),
(344, 77, 1, 29),
(345, 77, 2, 29),
(346, 77, 3, 29),
(347, 77, 4, 29),
(348, 74, 1, 11),
(349, 74, 2, 11),
(350, 74, 3, 11),
(351, 74, 4, 11),
(352, 78, 1, 9),
(353, 78, 2, 9),
(354, 78, 3, 9),
(355, 78, 4, 9),
(376, 80, 1, 10),
(377, 80, 2, 10),
(378, 80, 3, 10),
(379, 80, 4, 10),
(382, 81, 1, 11),
(383, 81, 2, 11),
(384, 82, 1, 29),
(385, 82, 2, 29),
(386, 83, 1, 9),
(387, 83, 2, 9),
(388, 84, 1, 9),
(389, 84, 2, 9),
(390, 85, 1, 11),
(391, 85, 2, 11),
(392, 86, 1, 29),
(393, 86, 2, 29),
(394, 86, 3, 29),
(395, 86, 4, 29),
(396, 87, 1, 10),
(397, 87, 2, 10),
(405, 90, 1, 11),
(406, 90, 2, 11),
(407, 90, 3, 11),
(408, 90, 4, 11),
(409, 91, 1, 9),
(410, 91, 2, 9),
(415, 93, 1, 29),
(416, 93, 2, 29),
(417, 93, 3, 29),
(418, 93, 4, 29),
(421, 89, 6, 14),
(422, 89, 8, 14),
(423, 88, 5, 13),
(424, 88, 7, 13),
(433, 94, 1, 9),
(434, 94, 2, 9),
(435, 95, 1, 10),
(436, 95, 2, 10),
(437, 96, 1, 29),
(438, 96, 2, 29),
(439, 97, 5, 13),
(440, 97, 7, 13),
(441, 98, 1, 29),
(442, 98, 2, 29),
(443, 99, 5, 13),
(444, 99, 7, 13),
(445, 100, 6, 14),
(446, 100, 8, 14),
(447, 101, 1, 29),
(448, 101, 2, 29),
(449, 101, 3, 29),
(450, 101, 4, 29),
(451, 102, 1, 9),
(452, 102, 2, 9),
(453, 103, 1, 12),
(454, 103, 2, 12),
(455, 103, 3, 12),
(456, 103, 4, 12),
(457, 92, 1, 11),
(458, 92, 2, 11),
(459, 92, 3, 11),
(460, 92, 4, 11),
(461, 104, 5, 13),
(462, 104, 7, 13),
(467, 106, 1, 9),
(468, 106, 2, 9),
(469, 107, 1, 11),
(470, 107, 2, 11),
(471, 108, 1, 29),
(472, 108, 2, 29),
(473, 109, 5, 13),
(474, 109, 7, 13),
(475, 110, 6, 14),
(476, 110, 8, 14),
(477, 111, 1, 11),
(478, 111, 2, 11),
(479, 112, 1, 10),
(480, 112, 2, 10),
(481, 113, 1, 11),
(482, 113, 2, 11),
(483, 113, 3, 11),
(484, 113, 4, 11),
(485, 114, 5, 13),
(486, 114, 7, 13),
(487, 115, 6, 14),
(488, 115, 8, 14),
(489, 116, 1, 10),
(490, 116, 2, 10),
(491, 117, 5, 15),
(492, 117, 7, 15),
(495, 119, 1, 11),
(496, 119, 2, 11),
(497, 120, 1, 10),
(498, 120, 2, 10),
(499, 121, 1, 11),
(500, 121, 2, 11),
(505, 123, 5, 13),
(506, 123, 7, 13),
(507, 124, 1, 10),
(508, 124, 2, 10),
(509, 125, 1, 29),
(510, 125, 2, 29),
(511, 126, 1, 10),
(512, 126, 2, 10),
(513, 127, 1, 29),
(514, 127, 2, 29),
(515, 128, 1, 9),
(516, 128, 2, 9),
(519, 130, 1, 12),
(520, 130, 2, 12),
(521, 131, 1, 11),
(522, 131, 2, 11),
(523, 129, 1, 11),
(524, 129, 2, 11),
(529, 122, 1, 11),
(530, 122, 2, 11),
(531, 122, 3, 11),
(532, 122, 4, 11),
(533, 132, 1, 11),
(534, 132, 2, 11),
(535, 132, 3, 11),
(536, 132, 4, 11),
(537, 133, 1, 12),
(538, 133, 2, 12),
(539, 134, 1, 11),
(540, 134, 2, 11),
(543, 135, 1, 9),
(544, 135, 2, 9),
(545, 136, 5, 13),
(546, 136, 7, 13),
(548, 137, 6, 14),
(549, 137, 8, 14),
(552, 139, 1, 10),
(553, 139, 2, 10),
(554, 138, 1, 10),
(555, 138, 2, 10),
(560, 142, 5, 13),
(561, 142, 7, 13),
(562, 140, 5, 13),
(563, 140, 7, 13),
(564, 141, 6, 14),
(565, 141, 8, 14),
(566, 143, 6, 14),
(567, 143, 8, 14),
(568, 118, 5, 17),
(569, 118, 7, 17),
(570, 105, 5, 15),
(571, 105, 7, 15),
(572, 144, 1, 9),
(573, 144, 2, 9),
(576, 146, 6, 14),
(577, 146, 8, 14),
(582, 148, 1, 10),
(583, 148, 2, 10),
(584, 149, 1, 11),
(585, 149, 2, 11),
(586, 149, 3, 11),
(587, 149, 4, 11),
(588, 150, 1, 9),
(589, 150, 2, 9),
(590, 151, 1, 12),
(591, 151, 2, 12),
(592, 145, 5, 13),
(593, 145, 7, 13),
(594, 147, 1, 10),
(595, 147, 2, 10),
(596, 152, 1, 9),
(597, 152, 2, 9),
(598, 153, 1, 12),
(599, 153, 4, 12),
(600, 154, 1, 29),
(601, 154, 2, 29),
(602, 155, 1, 29),
(603, 155, 2, 29),
(604, 155, 3, 29),
(605, 155, 4, 29),
(606, 156, 1, 29),
(607, 156, 2, 29),
(608, 157, 1, 11),
(609, 157, 2, 11),
(610, 158, 1, 10),
(611, 158, 2, 10),
(612, 159, 1, 10),
(613, 159, 2, 10),
(618, 162, 1, 10),
(619, 162, 3, 10),
(620, 162, 2, 10),
(621, 162, 4, 10),
(622, 163, 1, 11),
(623, 163, 3, 11),
(624, 163, 2, 11),
(625, 163, 4, 11),
(626, 164, 5, 13),
(627, 164, 7, 13),
(628, 165, 6, 14),
(629, 165, 8, 14),
(634, 167, 1, 12),
(635, 167, 2, 12),
(636, 167, 3, 12),
(637, 167, 4, 12),
(638, 168, 1, 11),
(639, 168, 2, 11),
(642, 161, 6, 14),
(643, 161, 8, 14),
(644, 160, 5, 13),
(645, 160, 7, 13),
(646, 166, 1, 10),
(647, 166, 2, 10),
(648, 166, 3, 10),
(649, 166, 4, 10),
(650, 79, 1, 29),
(651, 79, 2, 29),
(652, 79, 3, 29),
(653, 79, 4, 29),
(654, 44, 1, 11),
(655, 44, 2, 11),
(656, 44, 3, 11),
(657, 44, 4, 11),
(658, 23, 1, 11),
(659, 23, 2, 11),
(660, 23, 3, 11),
(661, 23, 4, 11),
(662, 169, 1, 12),
(663, 169, 2, 12),
(664, 170, 1, 11),
(665, 170, 2, 11),
(666, 170, 3, 11),
(667, 170, 4, 11),
(668, 171, 1, 29),
(669, 171, 2, 29),
(670, 172, 1, 10),
(671, 172, 2, 10),
(672, 173, 1, 11),
(673, 173, 2, 11),
(674, 174, 1, 9),
(675, 174, 2, 9),
(676, 175, 1, 29),
(677, 175, 2, 29),
(678, 175, 3, 29),
(679, 175, 4, 29),
(680, 176, 1, 10),
(681, 176, 2, 10),
(682, 177, 5, 13),
(683, 177, 7, 13),
(684, 178, 1, 10),
(685, 178, 2, 10),
(686, 179, 1, 12),
(687, 179, 2, 12),
(688, 179, 3, 12),
(689, 179, 4, 12),
(690, 180, 1, 10),
(691, 180, 2, 10),
(692, 180, 3, 10),
(693, 180, 4, 10),
(694, 181, 1, 29),
(695, 181, 2, 29),
(696, 181, 3, 29),
(697, 181, 4, 29),
(698, 182, 1, 12),
(699, 182, 2, 12),
(700, 182, 3, 12),
(701, 182, 4, 12),
(702, 183, 1, 9),
(703, 183, 2, 9),
(704, 184, 1, 9),
(705, 184, 2, 9),
(706, 185, 1, 10),
(707, 185, 2, 10),
(708, 185, 3, 10),
(709, 185, 4, 10),
(710, 186, 5, 13),
(711, 186, 7, 13),
(712, 187, 5, 13),
(713, 187, 4, 13),
(714, 188, 1, 10),
(715, 188, 2, 10),
(716, 188, 3, 10),
(717, 188, 4, 10),
(718, 189, 1, 11),
(719, 189, 2, 11),
(724, 191, 1, 10),
(725, 191, 2, 10),
(726, 191, 1, 11),
(727, 191, 2, 11),
(728, 192, 1, 11),
(729, 192, 2, 11),
(730, 192, 3, 11),
(731, 192, 4, 11),
(732, 193, 1, 11),
(733, 193, 2, 11),
(734, 193, 3, 11),
(735, 193, 4, 11),
(736, 194, 1, 29),
(737, 194, 2, 29),
(738, 194, 3, 29),
(739, 194, 4, 29),
(740, 195, 1, 10),
(741, 195, 2, 10),
(742, 195, 3, 10),
(743, 195, 4, 10),
(744, 196, 1, 11),
(745, 196, 2, 11),
(746, 196, 3, 11),
(747, 196, 4, 11),
(748, 197, 5, 13),
(749, 197, 6, 14),
(750, 197, 7, 13),
(751, 197, 8, 14),
(752, 198, 1, 9),
(753, 198, 2, 9),
(754, 199, 1, 12),
(755, 199, 2, 12),
(756, 200, 5, 13),
(757, 200, 7, 13),
(758, 201, 6, 14),
(759, 201, 8, 14),
(760, 202, 5, 13),
(761, 202, 7, 13),
(762, 203, 1, 29),
(763, 203, 2, 29),
(764, 190, 1, 11),
(765, 190, 2, 11),
(766, 190, 3, 11),
(767, 190, 4, 11),
(768, 204, 1, 10),
(769, 204, 2, 10),
(774, 205, 5, 13),
(775, 205, 7, 13),
(776, 206, 6, 14),
(777, 206, 8, 14),
(778, 207, 1, 9),
(779, 207, 2, 9),
(780, 208, 1, 11),
(781, 208, 2, 11),
(782, 208, 3, 11),
(783, 208, 4, 11),
(784, 209, 1, 29),
(785, 209, 2, 29),
(786, 210, 1, 10),
(787, 210, 2, 10),
(788, 211, 1, 29),
(789, 211, 2, 29),
(790, 211, 3, 29),
(791, 211, 4, 29),
(792, 212, 5, 13),
(793, 212, 7, 13),
(794, 213, 6, 14),
(795, 213, 8, 14),
(800, 215, 1, 9),
(801, 215, 2, 9),
(802, 216, 1, 10),
(803, 216, 2, 10),
(804, 216, 3, 10),
(805, 216, 4, 10),
(806, 217, 1, 10),
(807, 217, 2, 10),
(808, 217, 3, 10),
(809, 217, 4, 10),
(810, 218, 1, 12),
(811, 218, 2, 12),
(812, 219, 1, 29),
(813, 219, 2, 29),
(814, 220, 1, 11),
(815, 220, 2, 11),
(816, 220, 3, 11),
(817, 220, 4, 11),
(818, 221, 1, 9),
(819, 221, 2, 9),
(820, 222, 1, 12),
(821, 222, 2, 12),
(822, 223, 5, 13),
(823, 224, 6, 14),
(824, 224, 8, 14),
(825, 225, 1, 29),
(826, 225, 2, 29),
(827, 225, 3, 29),
(828, 225, 4, 29),
(829, 226, 1, 10),
(830, 226, 2, 10),
(831, 226, 3, 10),
(832, 226, 4, 10),
(833, 227, 5, 13),
(834, 227, 7, 13),
(835, 228, 5, 13),
(836, 228, 7, 13),
(837, 214, 1, 12),
(838, 214, 2, 12),
(839, 214, 3, 12),
(840, 214, 4, 12),
(841, 229, 1, 29),
(842, 229, 2, 29),
(843, 230, 1, 12),
(844, 230, 2, 12),
(847, 232, 1, 11),
(848, 232, 2, 11),
(849, 232, 3, 11),
(850, 232, 4, 11),
(851, 231, 1, 10),
(852, 231, 2, 10),
(853, 231, 3, 10),
(854, 231, 4, 10),
(855, 233, 1, 9),
(856, 233, 2, 9),
(857, 234, 1, 10),
(858, 234, 2, 10),
(859, 234, 3, 10),
(860, 234, 4, 10),
(861, 235, 1, 29),
(862, 235, 2, 29),
(865, 236, 1, 9),
(866, 236, 2, 9),
(867, 237, 1, 29),
(868, 237, 2, 29),
(869, 237, 3, 29),
(870, 237, 4, 29),
(871, 238, 1, 11),
(872, 238, 2, 11),
(873, 239, 1, 10),
(874, 239, 2, 10),
(875, 240, 1, 10),
(876, 240, 2, 10),
(881, 243, 1, 11),
(882, 243, 2, 11),
(883, 243, 3, 11),
(884, 243, 4, 11),
(885, 244, 1, 29),
(886, 244, 2, 29),
(887, 244, 3, 29),
(888, 244, 4, 29),
(889, 245, 1, 29),
(890, 245, 2, 29),
(891, 245, 3, 29),
(892, 245, 4, 29),
(893, 246, 1, 11),
(894, 246, 2, 11),
(895, 246, 3, 11),
(896, 246, 4, 11),
(901, 242, 6, 14),
(902, 242, 8, 14),
(903, 241, 5, 13),
(904, 241, 7, 13),
(905, 247, 1, 11),
(906, 247, 2, 11),
(907, 248, 1, 12),
(908, 248, 2, 12),
(909, 249, 1, 9),
(910, 249, 2, 9),
(911, 250, 1, 10),
(912, 250, 2, 10),
(913, 251, 1, 9),
(914, 251, 2, 9),
(915, 252, 1, 11),
(916, 252, 2, 11),
(917, 252, 3, 11),
(918, 252, 4, 11),
(923, 253, 1, 10),
(924, 253, 2, 10),
(925, 254, 1, 11),
(926, 254, 2, 11),
(927, 255, 1, 11),
(928, 255, 2, 11),
(929, 255, 4, 11),
(930, 255, 3, 11),
(935, 257, 1, 10),
(936, 257, 2, 10),
(937, 258, 1, 29),
(938, 258, 2, 29),
(939, 258, 3, 29),
(940, 258, 4, 29),
(941, 256, 1, 29),
(942, 256, 2, 29),
(943, 256, 3, 29),
(944, 256, 4, 29),
(945, 259, 5, 13),
(946, 259, 7, 13),
(951, 261, 5, 13),
(952, 261, 7, 13),
(953, 262, 1, 12),
(954, 262, 2, 12),
(955, 263, 1, 10),
(956, 263, 2, 10),
(957, 263, 3, 10),
(958, 263, 4, 10),
(959, 264, 1, 10),
(960, 264, 2, 10),
(961, 264, 3, 10),
(962, 264, 4, 10),
(963, 265, 1, 11),
(964, 265, 2, 11),
(965, 265, 3, 11),
(966, 265, 4, 11),
(967, 266, 1, 10),
(968, 266, 2, 10),
(969, 266, 3, 10),
(970, 266, 4, 10),
(971, 267, 1, 11),
(972, 267, 2, 11),
(973, 267, 3, 11),
(974, 267, 4, 11),
(975, 268, 1, 29),
(976, 268, 2, 29),
(977, 269, 1, 10),
(978, 269, 2, 10),
(979, 270, 1, 9),
(980, 270, 2, 9),
(981, 271, 1, 9),
(982, 271, 2, 9),
(987, 273, 1, 9),
(988, 273, 2, 9),
(989, 274, 1, 11),
(990, 274, 2, 11),
(993, 276, 1, 10),
(994, 276, 2, 10),
(995, 277, 5, 13),
(996, 277, 7, 13),
(997, 278, 6, 14),
(998, 278, 7, 14),
(999, 279, 1, 11),
(1000, 279, 2, 11),
(1001, 279, 3, 11),
(1002, 279, 4, 11),
(1003, 280, 1, 29),
(1004, 280, 2, 29),
(1005, 275, 1, 29),
(1006, 275, 2, 29),
(1007, 275, 3, 29),
(1008, 275, 4, 29),
(1009, 272, 1, 11),
(1010, 272, 2, 11),
(1011, 272, 3, 11),
(1012, 272, 4, 11),
(1013, 281, 1, 11),
(1014, 281, 2, 11),
(1015, 281, 3, 11),
(1016, 281, 4, 11),
(1017, 282, 1, 29),
(1018, 282, 2, 29),
(1019, 282, 3, 29),
(1020, 282, 4, 29),
(1025, 285, 1, 10),
(1026, 285, 2, 10),
(1027, 286, 1, 11),
(1028, 286, 2, 11),
(1029, 286, 3, 11),
(1030, 286, 4, 11),
(1031, 287, 5, 13),
(1032, 287, 7, 13),
(1033, 288, 6, 14),
(1034, 288, 8, 14),
(1043, 291, 1, 29),
(1044, 291, 2, 29),
(1045, 290, 1, 12),
(1046, 290, 2, 12),
(1047, 290, 3, 12),
(1048, 290, 4, 12),
(1049, 289, 1, 29),
(1050, 289, 2, 29),
(1051, 289, 3, 29),
(1052, 289, 4, 29),
(1053, 283, 1, 11),
(1054, 283, 2, 11),
(1055, 284, 1, 29),
(1056, 284, 2, 29),
(1057, 292, 1, 9),
(1058, 292, 2, 9),
(1059, 293, 1, 9),
(1060, 293, 2, 9),
(1061, 294, 1, 12),
(1062, 294, 2, 12),
(1063, 295, 5, 13),
(1064, 295, 7, 13),
(1065, 296, 6, 14),
(1066, 296, 7, 14),
(1067, 297, 1, 9),
(1068, 297, 2, 9),
(1069, 298, 1, 11),
(1070, 298, 2, 11),
(1071, 299, 1, 9),
(1072, 299, 2, 9),
(1073, 300, 1, 10),
(1074, 300, 2, 10),
(1075, 300, 3, 10),
(1076, 300, 4, 10),
(1079, 301, 1, 11),
(1080, 301, 2, 11),
(1081, 301, 3, 11),
(1082, 301, 4, 11),
(1083, 302, 5, 13),
(1084, 302, 7, 13),
(1085, 303, 1, 12),
(1086, 303, 2, 12),
(1087, 304, 1, 11),
(1088, 304, 2, 11),
(1091, 306, 1, 9),
(1092, 306, 2, 9),
(1093, 307, 1, 9),
(1094, 307, 2, 9),
(1095, 308, 1, 10),
(1096, 308, 2, 10),
(1097, 308, 3, 10),
(1098, 308, 4, 10),
(1099, 309, 1, 29),
(1100, 309, 2, 29),
(1101, 310, 5, 13),
(1102, 310, 7, 13),
(1103, 311, 1, 9),
(1104, 311, 2, 9),
(1105, 305, 1, 12),
(1106, 305, 2, 12),
(1107, 312, 1, 10),
(1108, 312, 2, 10),
(1109, 313, 1, 11),
(1110, 313, 2, 11),
(1111, 314, 1, 11),
(1112, 314, 2, 11),
(1113, 314, 3, 11),
(1114, 314, 4, 11),
(1119, 315, 1, 29),
(1120, 315, 2, 29),
(1121, 315, 3, 29),
(1122, 315, 4, 29),
(1123, 316, 5, 13),
(1124, 316, 7, 13),
(1125, 317, 6, 14),
(1126, 317, 8, 14),
(1129, 319, 6, 14),
(1130, 319, 8, 14),
(1131, 320, 1, 10),
(1132, 320, 2, 10),
(1133, 320, 3, 10),
(1134, 320, 4, 10),
(1135, 321, 1, 9),
(1136, 321, 2, 9),
(1137, 322, 5, 13),
(1138, 322, 7, 13),
(1139, 323, 1, 11),
(1140, 323, 2, 11),
(1141, 323, 3, 11),
(1142, 323, 4, 11),
(1143, 318, 5, 13),
(1144, 318, 7, 13),
(1149, 324, 1, 10),
(1150, 324, 2, 10),
(1151, 324, 3, 10),
(1152, 324, 4, 10),
(1153, 325, 1, 29),
(1154, 325, 2, 29),
(1155, 325, 3, 29),
(1156, 325, 4, 29),
(1161, 326, 1, 29),
(1162, 326, 2, 29),
(1163, 326, 3, 29),
(1164, 326, 4, 29),
(1165, 327, 1, 11),
(1166, 327, 2, 11),
(1167, 328, 1, 29),
(1168, 328, 2, 29),
(1169, 328, 3, 29),
(1170, 328, 4, 29),
(1171, 329, 1, 9),
(1172, 329, 2, 9),
(1173, 330, 1, 9),
(1174, 330, 2, 9),
(1191, 331, 1, 11),
(1192, 331, 2, 11),
(1193, 332, 1, 29),
(1194, 332, 2, 29),
(1195, 333, 1, 9),
(1196, 333, 2, 9),
(1197, 334, 1, 10),
(1198, 334, 2, 10),
(1199, 334, 3, 10),
(1200, 334, 4, 10),
(1205, 335, 1, 11),
(1206, 335, 3, 11),
(1207, 335, 2, 11),
(1208, 335, 4, 11),
(1213, 336, 1, 10),
(1214, 336, 3, 10),
(1215, 336, 2, 10),
(1216, 336, 4, 10),
(1217, 337, 1, 11),
(1218, 337, 3, 11),
(1219, 337, 2, 11),
(1220, 337, 4, 11),
(1223, 339, 1, 11),
(1224, 339, 2, 11),
(1225, 339, 3, 11),
(1226, 339, 4, 11),
(1227, 338, 1, 10),
(1228, 338, 2, 10),
(1229, 338, 3, 10),
(1230, 338, 4, 10),
(1231, 340, 1, 29),
(1232, 340, 2, 29),
(1233, 340, 3, 29),
(1234, 340, 4, 29),
(1235, 341, 1, 10),
(1236, 341, 2, 10),
(1237, 341, 3, 10),
(1238, 341, 4, 10),
(1239, 342, 1, 9),
(1240, 342, 2, 9),
(1241, 343, 1, 10),
(1242, 343, 2, 10),
(1243, 343, 3, 10),
(1244, 343, 4, 10),
(1249, 344, 1, 11),
(1250, 344, 2, 11),
(1251, 344, 3, 11),
(1252, 344, 4, 11),
(1257, 346, 1, 9),
(1258, 346, 2, 9),
(1259, 347, 1, 9),
(1260, 347, 2, 9),
(1265, 349, 1, 11),
(1266, 349, 2, 11),
(1267, 349, 3, 11),
(1268, 349, 4, 11),
(1277, 348, 1, 10),
(1278, 348, 2, 10),
(1279, 348, 3, 10),
(1280, 348, 4, 10),
(1281, 351, 1, 9),
(1282, 351, 2, 9),
(1283, 352, 1, 9),
(1284, 352, 2, 9),
(1285, 353, 1, 10),
(1286, 353, 2, 10),
(1287, 354, 1, 9),
(1288, 354, 2, 9),
(1289, 355, 1, 29),
(1290, 355, 2, 29),
(1291, 356, 1, 11),
(1292, 356, 2, 11),
(1293, 356, 3, 11),
(1294, 356, 4, 11),
(1299, 345, 1, 12),
(1300, 345, 2, 12),
(1301, 345, 3, 12),
(1302, 345, 4, 12),
(1303, 350, 1, 29),
(1304, 350, 2, 29),
(1305, 350, 3, 29),
(1306, 350, 4, 29),
(1307, 357, 1, 11),
(1308, 357, 2, 11),
(1309, 357, 3, 11),
(1310, 357, 4, 11),
(1311, 358, 1, 11),
(1312, 358, 2, 11),
(1313, 359, 1, 29),
(1314, 359, 2, 29),
(1315, 360, 5, 13),
(1316, 360, 7, 13),
(1317, 361, 1, 10),
(1318, 361, 2, 10),
(1319, 362, 1, 11),
(1320, 362, 2, 11),
(1321, 363, 6, 14),
(1322, 363, 8, 14),
(1323, 364, 1, 11),
(1324, 364, 2, 11),
(1325, 365, 1, 29),
(1326, 365, 2, 29),
(1327, 366, 1, 11),
(1328, 366, 2, 11),
(1329, 367, 1, 29),
(1330, 367, 2, 29),
(1333, 368, 1, 9),
(1334, 368, 2, 9),
(1335, 369, 1, 11),
(1336, 369, 2, 11),
(1337, 369, 3, 11),
(1338, 369, 4, 11),
(1339, 370, 1, 11),
(1340, 370, 2, 11),
(1341, 371, 1, 29),
(1342, 371, 2, 29),
(1343, 372, 1, 10),
(1344, 372, 2, 10),
(1345, 373, 1, 11),
(1346, 373, 2, 11),
(1347, 374, 5, 13),
(1348, 374, 7, 13),
(1349, 375, 1, 29),
(1350, 375, 2, 29),
(1351, 375, 3, 29),
(1352, 375, 4, 29),
(1353, 376, 1, 12),
(1354, 376, 2, 12),
(1355, 376, 3, 12),
(1356, 376, 4, 12),
(1357, 377, 1, 10),
(1358, 377, 2, 10),
(1359, 378, 1, 11),
(1360, 378, 2, 11),
(1361, 379, 1, 11),
(1362, 379, 2, 11),
(1363, 380, 1, 29),
(1364, 380, 2, 29),
(1365, 381, 1, 11),
(1366, 381, 1, 29),
(1367, 382, 1, 29),
(1368, 382, 2, 29),
(1369, 383, 1, 10),
(1370, 383, 2, 10),
(1371, 384, 1, 11),
(1372, 384, 2, 11),
(1373, 385, 1, 9),
(1374, 385, 2, 9),
(1375, 386, 1, 10),
(1376, 386, 2, 10),
(1377, 387, 1, 11),
(1378, 387, 2, 11),
(1381, 389, 1, 11),
(1382, 389, 2, 11),
(1383, 390, 1, 29),
(1384, 390, 2, 29),
(1385, 391, 1, 11),
(1386, 391, 2, 11),
(1387, 392, 1, 29),
(1388, 392, 2, 29),
(1389, 393, 1, 11),
(1390, 393, 2, 11),
(1391, 393, 3, 11),
(1392, 393, 4, 11),
(1397, 396, 1, 11),
(1398, 396, 2, 11),
(1399, 397, 1, 29),
(1400, 397, 2, 29),
(1401, 395, 1, 11),
(1402, 395, 2, 11),
(1403, 394, 1, 10),
(1404, 394, 2, 10),
(1405, 398, 1, 10),
(1406, 398, 2, 10),
(1407, 399, 1, 11),
(1408, 399, 2, 11),
(1409, 400, 1, 10),
(1410, 400, 2, 10),
(1411, 401, 5, 13),
(1412, 401, 7, 13),
(1413, 402, 6, 14),
(1414, 402, 8, 14),
(1415, 403, 1, 10),
(1416, 403, 2, 10),
(1418, 405, 5, 13),
(1423, 408, 1, 9),
(1424, 408, 2, 9),
(1425, 409, 1, 9),
(1426, 409, 2, 9),
(1427, 410, 1, 29),
(1428, 410, 2, 29),
(1429, 410, 3, 29),
(1430, 410, 4, 29),
(1431, 411, 1, 12),
(1432, 411, 2, 12),
(1433, 411, 3, 12),
(1434, 411, 4, 12),
(1435, 412, 1, 9),
(1436, 412, 2, 9),
(1437, 407, 6, 14),
(1438, 407, 8, 14),
(1439, 406, 5, 13),
(1440, 406, 7, 13),
(1441, 413, 1, 29),
(1442, 413, 2, 29),
(1443, 413, 3, 29),
(1444, 413, 4, 29),
(1445, 414, 1, 11),
(1446, 414, 2, 11),
(1447, 414, 3, 11),
(1448, 414, 4, 11),
(1449, 415, 1, 11),
(1450, 415, 2, 11),
(1451, 415, 3, 11),
(1452, 415, 4, 11),
(1467, 424, 5, 13),
(1468, 424, 7, 13),
(1471, 425, 1, 29),
(1472, 425, 2, 29),
(1473, 423, 1, 11),
(1474, 423, 2, 11),
(1475, 422, 1, 10),
(1476, 422, 2, 10),
(1477, 421, 1, 11),
(1478, 421, 2, 11),
(1479, 420, 1, 29),
(1480, 420, 2, 29);

-- --------------------------------------------------------

--
-- Table structure for table `ksl_gallery_image_tbl`
--

CREATE TABLE `ksl_gallery_image_tbl` (
  `gal_image_id` int(11) NOT NULL,
  `gal_image_rootid` varchar(100) NOT NULL,
  `gal_image_path` varchar(300) NOT NULL,
  `gal_image_type` varchar(50) NOT NULL,
  `gal_image_order` bigint(20) NOT NULL,
  `gal_image_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ksl_gallery_image_tbl`
--

INSERT INTO `ksl_gallery_image_tbl` (`gal_image_id`, `gal_image_rootid`, `gal_image_path`, `gal_image_type`, `gal_image_order`, `gal_image_created`) VALUES
(23, '4', '4_1.jpg', '.image/jpeg', 0, '2016-06-13 09:09:25'),
(24, '4', '4_2.jpg', '.image/jpeg', 1, '2016-06-13 09:09:25'),
(25, '4', '4_3.jpg', '.image/jpeg', 2, '2016-06-13 09:09:25'),
(26, '4', '4_4.jpg', '.image/jpeg', 3, '2016-06-13 09:09:25'),
(31, '5', '5_4.jpg', '.image/jpeg', 3, '2016-06-13 09:10:57'),
(32, '5', '5_5.jpg', '.image/jpeg', 4, '2016-06-13 09:10:57'),
(33, '5', '5_6.jpg', '.image/jpeg', 5, '2016-06-13 09:10:57'),
(34, '5', '5_7.jpg', '.image/jpeg', 6, '2016-06-13 09:10:57'),
(35, '5', '5_8.jpg', '.image/jpeg', 7, '2016-06-13 09:10:57'),
(36, '5', '5_9.jpg', '.image/jpeg', 8, '2016-06-13 09:10:57'),
(37, '5', '5_10.jpg', '.image/jpeg', 9, '2016-06-13 09:10:57'),
(38, '5', '5_11.jpg', '.image/jpeg', 10, '2016-06-13 09:10:57'),
(39, '6', '6_1.jpg', '.image/jpeg', 0, '2016-06-13 09:12:34'),
(40, '6', '6_2.jpg', '.image/jpeg', 3, '2016-09-29 06:48:43'),
(41, '6', '6_3.jpg', '.image/jpeg', 1, '2016-09-29 06:48:43'),
(42, '6', '6_4.jpg', '.image/jpeg', 2, '2016-09-29 06:48:43'),
(43, '6', '6_5.jpg', '.image/jpeg', 4, '2016-06-13 09:12:34'),
(44, '7', '7_1.jpg', '.image/jpeg', 0, '2016-06-13 09:13:54'),
(45, '7', '7_2.jpg', '.image/jpeg', 1, '2016-06-13 09:13:54'),
(46, '7', '7_3.jpg', '.image/jpeg', 2, '2016-06-13 09:13:54'),
(47, '7', '7_4.jpg', '.image/jpeg', 3, '2016-06-13 09:13:54'),
(48, '7', '7_5.jpg', '.image/jpeg', 4, '2016-06-13 09:13:54'),
(49, '7', '7_6.jpg', '.image/jpeg', 5, '2016-06-13 09:13:54'),
(50, '8', '8_1.jpg', '.image/jpeg', 1, '2016-08-23 14:21:29'),
(51, '8', '8_2.jpg', '.image/jpeg', 0, '2016-08-23 14:21:29'),
(52, '8', '8_3.jpg', '.image/jpeg', 2, '2016-06-13 09:15:37'),
(53, '8', '8_4.jpg', '.image/jpeg', 3, '2016-06-13 09:15:37'),
(54, '8', '8_5.jpg', '.image/jpeg', 4, '2016-06-13 09:15:37'),
(55, '9', '9_1.jpg', '.image/jpeg', 0, '2016-06-13 09:18:28'),
(56, '9', '9_2.jpg', '.image/jpeg', 1, '2016-06-13 09:18:28'),
(57, '9', '9_3.jpg', '.image/jpeg', 2, '2016-06-13 09:18:28'),
(58, '9', '9_4.jpg', '.image/jpeg', 3, '2016-06-13 09:18:28'),
(68, '10', '10_2.jpg', '.image/jpeg', 1, '2016-06-16 08:35:42'),
(69, '10', '10_3.jpg', '.image/jpeg', 2, '2016-06-16 08:35:42'),
(70, '10', '10_4.jpg', '.image/jpeg', 3, '2016-06-16 08:35:42'),
(71, '10', '10_5.jpg', '.image/jpeg', 4, '2016-06-16 08:35:42'),
(72, '10', '10_6.jpg', '.image/jpeg', 5, '2016-06-16 08:35:42'),
(73, '10', '10_7.jpg', '.image/jpeg', 6, '2016-06-16 08:35:42'),
(74, '10', '10_8.jpg', '.image/jpeg', 7, '2016-06-16 08:35:42'),
(75, '10', '10_9.jpg', '.image/jpeg', 8, '2016-06-16 08:35:42'),
(76, '10', '10_10.jpg', '.image/jpeg', 9, '2016-06-16 08:35:42'),
(77, '10', '10_12.jpg', '.image/jpeg', 10, '2016-06-16 08:35:42'),
(78, '10', '10_13.jpg', '.image/jpeg', 11, '2016-06-16 08:35:42'),
(79, '10', '10_14.jpg', '.image/jpeg', 12, '2016-06-16 08:35:42'),
(81, '11', '11_1.jpg', '.image/jpeg', 1, '2016-06-16 08:36:42'),
(82, '11', '11_2.jpg', '.image/jpeg', 0, '2016-06-16 08:36:42'),
(83, '11', '11_3.jpg', '.image/jpeg', 2, '2016-06-16 08:36:28'),
(84, '11', '11_4.jpg', '.image/jpeg', 3, '2016-06-16 08:36:28'),
(85, '11', '11_5.jpg', '.image/jpeg', 4, '2016-06-16 08:36:28'),
(86, '11', '11_6.jpg', '.image/jpeg', 5, '2016-06-16 08:36:28'),
(87, '11', '11_7.jpg', '.image/jpeg', 6, '2016-06-16 08:36:28'),
(88, '11', '11_8.jpg', '.image/jpeg', 7, '2016-06-16 08:36:28'),
(89, '12', '12_1.jpg', '.image/jpeg', 0, '2016-06-16 08:37:11'),
(90, '12', '12_2.jpg', '.image/jpeg', 1, '2016-06-16 08:37:11'),
(91, '12', '12_3.jpg', '.image/jpeg', 2, '2016-06-16 08:37:11'),
(92, '12', '12_4.jpg', '.image/jpeg', 3, '2016-06-16 08:37:11'),
(93, '12', '12_5.jpg', '.image/jpeg', 4, '2016-06-16 08:37:11'),
(94, '12', '12_6.jpg', '.image/jpeg', 5, '2016-06-16 08:37:11'),
(95, '12', '12_7.jpg', '.image/jpeg', 6, '2016-06-16 08:37:11'),
(96, '12', '12_8.jpg', '.image/jpeg', 7, '2016-06-16 08:37:11'),
(97, '12', '12_9.jpg', '.image/jpeg', 8, '2016-06-16 08:37:11'),
(98, '13', '13_1.jpg', '.image/jpeg', 0, '2016-06-16 08:37:38'),
(99, '13', '13_2.jpg', '.image/jpeg', 1, '2016-06-16 08:37:38'),
(100, '13', '13_3.jpg', '.image/jpeg', 2, '2016-06-16 08:37:38'),
(101, '13', '13_4.jpg', '.image/jpeg', 3, '2016-06-16 08:37:38'),
(102, '13', '13_5.jpg', '.image/jpeg', 4, '2016-06-16 08:37:38'),
(103, '13', '13_6.jpg', '.image/jpeg', 5, '2016-06-16 08:37:38'),
(104, '13', '13_7.jpg', '.image/jpeg', 6, '2016-06-16 08:37:38'),
(105, '13', '13_8.jpg', '.image/jpeg', 7, '2016-06-16 08:37:38'),
(106, '13', '13_9.jpg', '.image/jpeg', 8, '2016-06-16 08:37:38'),
(107, '14', '14_1.jpg', '.image/jpeg', 0, '2016-06-16 08:37:59'),
(108, '14', '14_2.jpg', '.image/jpeg', 1, '2016-06-16 08:37:59'),
(109, '14', '14_3.jpg', '.image/jpeg', 2, '2016-06-16 08:37:59'),
(110, '14', '14_4.jpg', '.image/jpeg', 3, '2016-06-16 08:37:59'),
(111, '14', '14_5.jpg', '.image/jpeg', 4, '2016-06-16 08:37:59'),
(112, '14', '14_6.jpg', '.image/jpeg', 5, '2016-06-16 08:37:59'),
(113, '14', '14_7.jpg', '.image/jpeg', 6, '2016-06-16 08:37:59'),
(114, '14', '14_8.jpg', '.image/jpeg', 7, '2016-06-16 08:37:59'),
(115, '14', '14_9.jpg', '.image/jpeg', 8, '2016-06-16 08:37:59'),
(116, '14', '14_10.jpg', '.image/jpeg', 9, '2016-06-16 08:37:59'),
(117, '15', '15_1.jpg', '.image/jpeg', 0, '2016-06-16 08:38:24'),
(118, '15', '15_2.jpg', '.image/jpeg', 1, '2016-06-16 08:38:24'),
(119, '15', '15_3.jpg', '.image/jpeg', 2, '2016-06-16 08:38:24'),
(120, '15', '15_4.jpg', '.image/jpeg', 3, '2016-06-16 08:38:24'),
(121, '15', '15_5.jpg', '.image/jpeg', 4, '2016-06-16 08:38:24'),
(122, '15', '15_6.jpg', '.image/jpeg', 5, '2016-06-16 08:38:24'),
(123, '15', '15_7.jpg', '.image/jpeg', 6, '2016-06-16 08:38:24'),
(124, '15', '15_8.jpg', '.image/jpeg', 7, '2016-06-16 08:38:24'),
(125, '15', '15_9.jpg', '.image/jpeg', 8, '2016-06-16 08:38:24'),
(126, '16', '16_Chrysanthemum.jpg', '.image/jpeg', 1, '2016-06-16 10:40:23'),
(127, '16', '16_Desert.jpg', '.image/jpeg', 2, '2016-06-16 10:40:23'),
(128, '16', '16_Koala.jpg', '.image/jpeg', 0, '2016-06-16 10:40:23'),
(129, '16', '16_Lighthouse.jpg', '.image/jpeg', 3, '2016-06-16 10:40:12'),
(130, '17', '17_1.jpg', '.image/jpeg', 0, '2016-08-27 05:02:02'),
(131, '17', '17_2.jpg', '.image/jpeg', 3, '2016-08-27 05:02:03'),
(132, '17', '17_3.jpg', '.image/jpeg', 2, '2016-08-27 05:02:05'),
(133, '17', '17_4.jpg', '.image/jpeg', 1, '2016-08-27 05:02:05'),
(134, '17', '17_stadium.jpg', '.image/jpeg', 4, '2016-06-18 07:34:40'),
(135, '19', '19_Chrysanthemum.jpg', '.image/jpeg', 0, '2016-10-22 07:52:07'),
(136, '19', '19_Desert.jpg', '.image/jpeg', 1, '2016-10-22 07:52:07'),
(137, '19', '19_Hydrangeas.jpg', '.image/jpeg', 2, '2016-10-22 07:52:07'),
(138, '20', '20_1.jpg', '.image/jpeg', 0, '2016-10-24 11:23:08'),
(139, '20', '20_2.jpg', '.image/jpeg', 1, '2016-10-24 11:23:08'),
(140, '20', '20_3.jpg', '.image/jpeg', 2, '2016-10-24 11:23:08'),
(163, '23', '23_Mr Dan Bloxham`s Parents workshop, Road to Wimbledon(RTW), Kslta, Cubbon park, Bangalore, India, 23rd January 2017 (5).jpg', '.image/jpeg', 0, '2017-01-23 19:29:06'),
(164, '23', '23_Mr Dan Bloxham`s Parents workshop, Road to Wimbledon(RTW), Kslta, Cubbon park, Bangalore, India, 23rd January 2017.jpg', '.image/jpeg', 1, '2017-01-23 19:29:06'),
(165, '23', '23_Mr Dan Bloxham`s Parents workshop, Road to Wimbledon(RTW), Kslta, Cubbon park, Bangalore, India, 23rd January 2017 (1).jpg', '.image/jpeg', 2, '2017-01-23 19:29:06'),
(166, '23', '23_Mr Dan Bloxham`s Parents workshop, Road to Wimbledon(RTW), Kslta, Cubbon park, Bangalore, India, 23rd January 2017 (2).jpg', '.image/jpeg', 3, '2017-01-23 19:29:06'),
(167, '23', '23_2.jpg', '.image/jpeg', 0, '2017-01-23 19:29:26'),
(168, '23', '23_1.jpg', '.image/jpeg', 1, '2017-01-23 19:29:26'),
(169, '23', '23_Mr Dan Bloxham`s fun activity fun shoot at cubbon park, Bangalore, India, The Road to Wimbledon (RTW), 23rd January 2017 (3).jpg', '.image/jpeg', 0, '2017-01-23 19:29:54'),
(170, '23', '23_Mr Dan Bloxham`s fun activity fun shoot at cubbon park, Bangalore, India, The Road to Wimbledon (RTW), 23rd January 2017 (4).jpg', '.image/jpeg', 1, '2017-01-23 19:29:54'),
(171, '23', '23_Mr Dan Bloxham`s fun activity fun shoot at cubbon park, Bangalore, India, The Road to Wimbledon (RTW), 23rd January 2017 (5).jpg', '.image/jpeg', 2, '2017-01-23 19:29:54'),
(172, '23', '23_Mr Dan Bloxham`s fun activity fun shoot at cubbon park, Bangalore, India, The Road to Wimbledon (RTW), 23rd January 2017 (2).jpg', '.image/jpeg', 0, '2017-01-23 19:30:37'),
(173, '23', '23_Mr Dan Bloxham`s fun activity fun shoot at cubbon park, Bangalore, India, The Road to Wimbledon (RTW), 23rd January 2017.jpg', '.image/jpeg', 1, '2017-01-23 19:30:37'),
(174, '23', '23_Mr Dan Bloxham`s fun activity fun shoot at cubbon park, Bangalore, India, The Road to Wimbledon (RTW), 23rd January 2017 (1).jpg', '.image/jpeg', 2, '2017-01-23 19:30:37'),
(198, '23', '23_2.JPG', '.image/jpeg', 0, '2017-01-28 09:50:33'),
(199, '23', '23_1.JPG', '.image/jpeg', 1, '2017-01-28 09:50:33'),
(200, '24', '24_2.jpg', '.image/jpeg', 0, '2017-02-10 08:57:36'),
(201, '24', '24_3.jpg', '.image/jpeg', 1, '2017-02-10 08:57:36'),
(202, '24', '24_4.jpg', '.image/jpeg', 2, '2017-02-10 08:57:36'),
(203, '24', '24_6.jpg', '.image/jpeg', 3, '2017-02-10 08:57:36'),
(204, '24', '24_7.jpg', '.image/jpeg', 4, '2017-02-10 08:57:36'),
(205, '24', '24_', '.', 0, '2017-02-10 08:57:45'),
(206, '24', '24_', '.', 0, '2017-02-10 08:57:51'),
(207, '25', '25_Chrysanthemum.jpg', '.image/jpeg', 0, '2017-02-11 08:40:13'),
(208, '25', '25_Desert.jpg', '.image/jpeg', 1, '2017-02-11 08:40:13'),
(209, '25', '25_Hydrangeas.jpg', '.image/jpeg', 2, '2017-02-11 08:40:13'),
(210, '25', '25_Jellyfish.jpg', '.image/jpeg', 3, '2017-02-11 08:40:13'),
(211, '25', '25_Koala.jpg', '.image/jpeg', 4, '2017-02-11 08:40:13'),
(212, '25', '25_Lighthouse.jpg', '.image/jpeg', 5, '2017-02-11 08:40:13'),
(213, '25', '25_Penguins.jpg', '.image/jpeg', 6, '2017-02-11 08:40:13'),
(214, '25', '25_Tulips.jpg', '.image/jpeg', 7, '2017-02-11 08:40:13'),
(215, '26', '26_Chrysanthemum.jpg', '.image/jpeg', 0, '2017-02-11 09:00:29'),
(216, '26', '26_Desert.jpg', '.image/jpeg', 1, '2017-02-11 09:00:29'),
(217, '26', '26_Hydrangeas.jpg', '.image/jpeg', 2, '2017-02-11 09:00:29'),
(218, '26', '26_Jellyfish.jpg', '.image/jpeg', 3, '2017-02-11 09:00:29'),
(219, '26', '26_Lighthouse.jpg', '.image/jpeg', 4, '2017-02-11 09:00:29'),
(220, '26', '26_Penguins.jpg', '.image/jpeg', 5, '2017-02-11 09:00:29'),
(221, '26', '26_Tulips.jpg', '.image/jpeg', 6, '2017-02-11 09:00:29'),
(241, '27', '27_1.jpg', '.image/jpeg', 0, '2017-02-11 09:52:31'),
(242, '27', '27_Coaches Clinic.jpg', '.image/jpeg', 1, '2017-02-11 09:52:31'),
(248, '27', '27_IMG_20170125_121853225.jpg', '.image/jpeg', 7, '2017-02-11 09:52:31'),
(250, '27', '27_Mr Dan Bloxham`s fun activity fun shoot at cubbon park, Bangalore, India, The Road to Wimbledon (RTW), 23rd January 2017.jpg', '.image/jpeg', 9, '2017-02-11 09:52:31'),
(251, '27', '27_Mr Dan Bloxham`s fun activity fun shoot at cubbon park, Bangalore, India, The Road to Wimbledon (RTW), 23rd January 2017 (1).jpg', '.image/jpeg', 10, '2017-02-11 09:52:31'),
(252, '27', '27_Mr Dan Bloxham`s fun activity fun shoot at cubbon park, Bangalore, India, The Road to Wimbledon (RTW), 23rd January 2017 (2).jpg', '.image/jpeg', 11, '2017-02-11 09:52:31'),
(253, '27', '27_Mr Dan Bloxham`s fun activity fun shoot at cubbon park, Bangalore, India, The Road to Wimbledon (RTW), 23rd January 2017 (3).jpg', '.image/jpeg', 12, '2017-02-11 09:52:31'),
(254, '27', '27_Mr Dan Bloxham`s fun activity fun shoot at cubbon park, Bangalore, India, The Road to Wimbledon (RTW), 23rd January 2017 (4).jpg', '.image/jpeg', 13, '2017-02-11 09:52:31'),
(255, '27', '27_Mr Dan Bloxham`s fun activity fun shoot at cubbon park, Bangalore, India, The Road to Wimbledon (RTW), 23rd January 2017 (5).jpg', '.image/jpeg', 14, '2017-02-11 09:52:31'),
(256, '27', '27_Mr Dan Bloxham`s fun activity fun shoot at cubbon park, Bangalore, India, The Road to Wimbledon (RTW), 23rd January 2017 (7).jpg', '.image/jpeg', 15, '2017-02-11 09:52:31'),
(257, '27', '27_Mr Dan Bloxham`s fun activity fun shoot at cubbon park, Bangalore, India, The Road to Wimbledon (RTW), 23rd January 2017 (8).jpg', '.image/jpeg', 16, '2017-02-11 09:52:31'),
(258, '27', '27_Mr Dan Bloxham`s Parents workshop, Road to Wimbledon(RTW), Kslta, Cubbon park, Bangalore, India, 23rd January 2017.jpg', '.image/jpeg', 17, '2017-02-11 09:52:31'),
(259, '27', '27_Mr Dan Bloxham`s Parents workshop, Road to Wimbledon(RTW), Kslta, Cubbon park, Bangalore, India, 23rd January 2017 (2).jpg', '.image/jpeg', 18, '2017-02-11 09:52:31'),
(261, '28', '28_27_Coaches Clinic.jpg', '.image/jpeg', 1, '2017-02-11 10:10:52'),
(262, '28', '28_27_IMG_20170125_121853225.jpg', '.image/jpeg', 2, '2017-02-11 10:10:52'),
(263, '28', '28_27_Mr Dan Bloxham`s fun activity fun shoot at cubbon park, Bangalore, India, The Road to Wimbledon (RTW), 23rd January 2017 (1).jpg', '.image/jpeg', 3, '2017-02-11 10:10:52'),
(264, '28', '28_27_Mr Dan Bloxham`s fun activity fun shoot at cubbon park, Bangalore, India, The Road to Wimbledon (RTW), 23rd January 2017 (2).jpg', '.image/jpeg', 4, '2017-02-11 10:10:52'),
(265, '28', '28_27_Mr Dan Bloxham`s fun activity fun shoot at cubbon park, Bangalore, India, The Road to Wimbledon (RTW), 23rd January 2017 (3).jpg', '.image/jpeg', 5, '2017-02-11 10:10:52'),
(266, '28', '28_27_Mr Dan Bloxham`s fun activity fun shoot at cubbon park, Bangalore, India, The Road to Wimbledon (RTW), 23rd January 2017 (4).jpg', '.image/jpeg', 6, '2017-02-11 10:10:52'),
(267, '28', '28_27_Mr Dan Bloxham`s fun activity fun shoot at cubbon park, Bangalore, India, The Road to Wimbledon (RTW), 23rd January 2017 (5).jpg', '.image/jpeg', 7, '2017-02-11 10:10:52'),
(268, '28', '28_27_Mr Dan Bloxham`s fun activity fun shoot at cubbon park, Bangalore, India, The Road to Wimbledon (RTW), 23rd January 2017 (7).jpg', '.image/jpeg', 8, '2017-02-11 10:10:52'),
(269, '28', '28_27_Mr Dan Bloxham`s fun activity fun shoot at cubbon park, Bangalore, India, The Road to Wimbledon (RTW), 23rd January 2017 (8).jpg', '.image/jpeg', 9, '2017-02-11 10:10:52'),
(270, '28', '28_27_Mr Dan Bloxham`s fun activity fun shoot at cubbon park, Bangalore, India, The Road to Wimbledon (RTW), 23rd January 2017.jpg', '.image/jpeg', 10, '2017-02-11 10:10:52'),
(271, '28', '28_27_Mr Dan Bloxham`s Parents workshop, Road to Wimbledon(RTW), Kslta, Cubbon park, Bangalore, India, 23rd January 2017 (2).jpg', '.image/jpeg', 11, '2017-02-11 10:10:52'),
(272, '28', '28_27_Mr Dan Bloxham`s Parents workshop, Road to Wimbledon(RTW), Kslta, Cubbon park, Bangalore, India, 23rd January 2017.jpg', '.image/jpeg', 12, '2017-02-11 10:10:52'),
(274, '29', '29_2.jpg', '.image/jpeg', 1, '2017-02-11 10:16:04'),
(275, '29', '29_3.jpg', '.image/jpeg', 2, '2017-02-11 10:16:04'),
(276, '29', '29_5.jpg', '.image/jpeg', 3, '2017-02-11 10:16:04'),
(277, '29', '29_6.jpg', '.image/jpeg', 4, '2017-02-11 10:16:04'),
(278, '29', '29_7.jpg', '.image/jpeg', 5, '2017-02-11 10:16:04'),
(279, '29', '29_8.jpg', '.image/jpeg', 6, '2017-02-11 10:16:04'),
(280, '29', '29_9.jpg', '.image/jpeg', 7, '2017-02-11 10:16:04'),
(281, '29', '29_10.jpg', '.image/jpeg', 8, '2017-02-11 10:16:04'),
(282, '29', '29_12.jpg', '.image/jpeg', 9, '2017-02-11 10:16:04'),
(283, '29', '29_13.jpg', '.image/jpeg', 10, '2017-02-11 10:16:04'),
(284, '29', '29_4.jpg', '.image/jpeg', 0, '2017-02-11 10:18:14'),
(285, '29', '29_11.jpg', '.image/jpeg', 1, '2017-02-11 10:18:14'),
(289, '3', '3_8.jpg', '.image/jpeg', 1, '2017-04-18 09:55:08'),
(308, '30', '30_Doubles Winner1.JPG', '.image/jpeg', 0, '2017-04-18 10:03:32'),
(309, '30', '30_Doubles Winner12.JPG', '.image/jpeg', 1, '2017-04-18 10:03:32'),
(310, '30', '30_DSC_1856.JPG', '.image/jpeg', 2, '2017-04-18 10:03:32'),
(311, '30', '30_DSC_1861.JPG', '.image/jpeg', 3, '2017-04-18 10:03:32'),
(312, '30', '30_DSC_1866.JPG', '.image/jpeg', 4, '2017-04-18 10:03:32'),
(313, '30', '30_DSC_1870.JPG', '.image/jpeg', 5, '2017-04-18 10:03:32'),
(314, '30', '30_DSC_1946.JPG', '.image/jpeg', 6, '2017-04-18 10:03:32'),
(315, '30', '30_Final.JPG', '.image/jpeg', 7, '2017-04-18 10:03:32'),
(316, '30', '30_ITF Doubles Winner & Runner.JPG', '.image/jpeg', 8, '2017-04-18 10:03:32'),
(317, '30', '30_ITF Singles Winner & Runner.JPG', '.image/jpeg', 9, '2017-04-18 10:03:32'),
(318, '31', '31_5.jpg', '.image/jpeg', 0, '2017-04-18 10:13:44'),
(319, '31', '31_8.jpg', '.image/jpeg', 1, '2017-04-18 10:13:44'),
(320, '31', '31_9.jpg', '.image/jpeg', 2, '2017-04-18 10:13:44'),
(321, '31', '31_10.jpg', '.image/jpeg', 3, '2017-04-18 10:13:44'),
(322, '31', '31_11.jpg', '.image/jpeg', 4, '2017-04-18 10:13:44'),
(323, '31', '31_12.jpg', '.image/jpeg', 5, '2017-04-18 10:13:44'),
(324, '31', '31_13.jpg', '.image/jpeg', 6, '2017-04-18 10:13:44'),
(325, '31', '31_14.jpg', '.image/jpeg', 7, '2017-04-18 10:13:44'),
(326, '31', '31_15.jpg', '.image/jpeg', 8, '2017-04-18 10:13:44'),
(327, '31', '31_16.jpg', '.image/jpeg', 9, '2017-04-18 10:13:44'),
(328, '31', '31_17.jpg', '.image/jpeg', 10, '2017-04-18 10:13:44'),
(329, '31', '31_18.jpg', '.image/jpeg', 11, '2017-04-18 10:13:44'),
(330, '31', '31_19.jpg', '.image/jpeg', 12, '2017-04-18 10:13:44'),
(331, '31', '31_20.jpg', '.image/jpeg', 13, '2017-04-18 10:13:44'),
(332, '31', '31_23.jpg', '.image/jpeg', 14, '2017-04-18 10:13:44'),
(333, '31', '31_25.jpg', '.image/jpeg', 15, '2017-04-18 10:13:44'),
(334, '31', '31_26.jpg', '.image/jpeg', 16, '2017-04-18 10:13:44'),
(335, '31', '31_27.jpg', '.image/jpeg', 17, '2017-04-18 10:13:44'),
(336, '31', '31_28.jpg', '.image/jpeg', 18, '2017-04-18 10:13:44'),
(337, '31', '31_29.jpg', '.image/jpeg', 19, '2017-04-18 10:13:44'),
(341, '20', '20_Davis Cup_Ind_Uzb_2017_Deepthi Indukuri-0524.jpg', '.image/jpeg', 0, '2017-07-22 09:45:05'),
(342, '20', '20_Davis Cup_Ind_Uzb_2017_Deepthi Indukuri-0535.jpg', '.image/jpeg', 1, '2017-07-22 09:45:05'),
(345, '20', '20_DJI_0056.jpg', '.image/jpeg', 0, '2017-07-22 09:47:13'),
(346, '20', '20_DJI_0065 (2).jpg', '.image/jpeg', 0, '2017-07-22 09:49:07'),
(347, '20', '20_DJI_0018.jpg', '.image/jpeg', 0, '2017-07-22 09:49:21'),
(348, '32', '32_winner 2.jpg', '.image/jpeg', 0, '2017-09-08 08:22:53'),
(349, '32', '32_Winner.jpg', '.image/jpeg', 1, '2017-09-08 08:22:53'),
(350, '32', '32_Winner1.jpg', '.image/jpeg', 0, '2017-09-08 08:23:04'),
(352, '1', '1_2.jpg', '.image/jpeg', 1, '2017-12-06 07:41:35'),
(353, '1', '1_3.jpg', '.image/jpeg', 2, '2017-12-06 07:41:35'),
(354, '1', '1_4.jpg', '.image/jpeg', 3, '2017-12-06 07:41:35'),
(355, '1', '1_5.jpg', '.image/jpeg', 4, '2017-12-06 07:41:35'),
(356, '1', '1_6.jpg', '.image/jpeg', 5, '2017-12-06 07:41:35'),
(357, '1', '1_7.jpg', '.image/jpeg', 6, '2017-12-06 07:41:35'),
(358, '1', '1_8.jpg', '.image/jpeg', 7, '2017-12-06 07:41:35'),
(359, '1', '1_9.jpg', '.image/jpeg', 8, '2017-12-06 07:41:35'),
(360, '5', '5_3.jpg', '.image/jpeg', 0, '2018-08-20 12:51:44'),
(361, '5', '5_1.jpg', '.image/jpeg', 1, '2018-08-20 12:51:44'),
(362, '5', '5_2.jpg', '.image/jpeg', 2, '2018-08-20 12:51:44'),
(363, '33', '33_1.jpeg', '.image/jpeg', 0, '2018-10-13 06:54:35'),
(364, '33', '33_2.jpeg', '.image/jpeg', 1, '2018-10-13 06:54:35'),
(365, '33', '33_3.jpeg', '.image/jpeg', 2, '2018-10-13 06:54:35'),
(366, '33', '33_4.jpeg', '.image/jpeg', 3, '2018-10-13 06:54:35'),
(367, '33', '33_5.jpeg', '.image/jpeg', 4, '2018-10-13 06:54:35'),
(368, '33', '33_6.jpeg', '.image/jpeg', 5, '2018-10-13 06:54:35'),
(369, '33', '33_7.jpeg', '.image/jpeg', 6, '2018-10-13 06:54:35'),
(370, '33', '33_9.jpeg', '.image/jpeg', 7, '2018-10-13 06:54:35'),
(371, '33', '33_10.jpeg', '.image/jpeg', 8, '2018-10-13 06:54:35'),
(373, '3', '3_2.jpg', '.image/jpeg', 1, '2018-12-28 09:32:06'),
(379, '3', '3_2.jpg', '.image/jpeg', 1, '2018-12-28 09:32:23'),
(381, '3', '3_4.jpeg', '.image/jpeg', 3, '2018-12-28 09:32:23'),
(383, '3', '3_6.jpg', '.image/jpeg', 5, '2018-12-28 09:32:23'),
(384, '34', '34_1.jpeg', '.image/jpeg', 0, '2018-12-28 09:36:59'),
(386, '34', '34_3.jpeg', '.image/jpeg', 2, '2018-12-28 09:36:59'),
(387, '34', '34_4.jpeg', '.image/jpeg', 3, '2018-12-28 09:36:59'),
(388, '34', '34_5.jpeg', '.image/jpeg', 4, '2018-12-28 09:36:59'),
(389, '3', '3_1.jpg', '.image/jpeg', 0, '2020-02-16 17:47:39'),
(390, '3', '3_2.jpg', '.image/jpeg', 1, '2020-02-16 17:47:39'),
(391, '3', '3_3.jpg', '.image/jpeg', 2, '2020-02-16 17:47:39'),
(392, '3', '3_4.jpg', '.image/jpeg', 3, '2020-02-16 17:47:39'),
(393, '3', '3_5.jpg', '.image/jpeg', 0, '2020-02-16 17:49:45');

-- --------------------------------------------------------

--
-- Table structure for table `ksl_gallery_tbl`
--

CREATE TABLE `ksl_gallery_tbl` (
  `gallery_id` int(11) NOT NULL,
  `gallery_title` varchar(500) NOT NULL,
  `gallery_display_tag` varchar(100) NOT NULL,
  `gallery_link_address` varchar(500) NOT NULL,
  `gallery_link_status` enum('A','I') NOT NULL DEFAULT 'A' COMMENT 'A=Link-Active, I=Link-Inactive',
  `gallery_content_image` varchar(250) NOT NULL,
  `gallery_posted_by` varchar(200) NOT NULL,
  `gallery_picture_courtesy` varchar(200) NOT NULL,
  `gallery_from_date` date NOT NULL,
  `gallery_to_date` date NOT NULL,
  `gallery_created_date` datetime NOT NULL,
  `gallery_lastupdated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `gallery_flat` enum('A','D') NOT NULL DEFAULT 'A' COMMENT 'A=Active, D=Deleted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ksl_gallery_tbl`
--

INSERT INTO `ksl_gallery_tbl` (`gallery_id`, `gallery_title`, `gallery_display_tag`, `gallery_link_address`, `gallery_link_status`, `gallery_content_image`, `gallery_posted_by`, `gallery_picture_courtesy`, `gallery_from_date`, `gallery_to_date`, `gallery_created_date`, `gallery_lastupdated_date`, `gallery_flat`) VALUES
(3, 'KALABURAGI OPEN GALLERY.', '8', '', 'A', 'KALABURAGI OPEN GALLERY. Details & Files', 'KSLTA Admin', 'KALABURAGI OPEN GALLERY.', '0000-00-00', '0000-00-00', '0000-00-00 00:00:00', '2016-06-08 14:29:16', 'A'),
(4, 'KSLTA BAR  & RESTAURANT.', '25', '', 'A', 'KSLTA BAR  & RESTAURANT. Images', 'Kslta-Admin', 'Nil', '2016-09-30', '2019-12-31', '0000-00-00 00:00:00', '2017-08-11 11:28:30', 'A'),
(5, 'KSLTA PARTY HALL  & VIP LOUNGE.', '9', 'galleryimage/116940.jpg', 'A', 'KSLTA PARTY HALL  & VIP LOUNGE. Images', 'Kslta-Admin', 'Nil', '2016-06-23', '0000-00-00', '0000-00-00 00:00:00', '2016-06-16 08:11:56', 'A'),
(6, 'GUEST ROOMS TERMS.', '9', 'galleryimage/111541.jpg', 'A', 'GUEST ROOMS TERMS IMAGES.', 'Kslta-Admin', 'Nil', '2016-06-14', '0000-00-00', '0000-00-00 00:00:00', '2016-06-16 08:11:44', 'A'),
(7, 'SWIMMING CLASS TIMING.', '9', 'galleryimage/129745.jpg', 'A', 'SWIMMING CLASS TIMING iMAGES.', 'Kslta-Admin', 'Nil', '2016-06-15', '0000-00-00', '0000-00-00 00:00:00', '2016-06-16 08:11:46', 'A'),
(8, 'Health Club', '25', '', 'A', 'KSLTA Health Club', 'Kslta-Admin', 'Nil', '2016-06-08', '2025-11-30', '0000-00-00 00:00:00', '2017-07-27 07:59:04', 'A'),
(9, 'TABLE TENNIS  & BILLIARDS.', '9', 'galleryimage/139004.jpg', 'A', 'TABLE TENNIS  & BILLIARDS.', 'Kslta-Admin', 'Nil', '2016-06-24', '0000-00-00', '0000-00-00 00:00:00', '2016-06-16 08:11:53', 'A'),
(10, 'Day 2 Dinner Party', '9', 'galleryimage/342153.jpg', 'A', 'Day 2', 'Kslta-Admin', 'Nil', '2016-06-14', '2016-06-25', '2016-06-16 10:29:28', '2016-06-16 08:29:28', 'A'),
(11, 'OCT18TH MATCH', '9', 'galleryimage/347744.jpg', 'A', 'October 18th Match', 'Kslta-Admin', 'Nil', '2016-06-14', '2016-06-30', '2016-06-16 10:30:29', '2016-06-16 08:30:29', 'A'),
(12, 'OCT20TH MATCH', '9', 'galleryimage/317761.jpg', 'A', 'OCT20TH MATCH', 'Kslta-Admin', 'Nil', '2016-06-20', '2016-06-16', '2016-06-16 10:32:02', '2016-06-16 08:32:02', 'A'),
(13, 'OCT 21ST GALLERY', '9', 'galleryimage/296054.jpg', 'A', 'OCT 21ST GALLERY', 'Kslta-Admin', 'Nil', '2016-06-06', '2016-06-25', '2016-06-16 10:33:12', '2016-06-16 08:33:12', 'A'),
(14, 'OCT 22ND GALLERY', '9', 'galleryimage/824130.jpg', 'A', 'OCT 22ND GALLERY', 'Kslta-Admin', 'Nil', '2016-06-23', '2016-06-16', '2016-06-16 10:33:52', '2016-06-16 08:33:52', 'A'),
(15, 'OCT 23RD GALLERY', '9', 'galleryimage/646459.jpg', 'A', 'OCT 23RD GALLERY', 'Kslta-Admin', 'Nil', '2016-06-08', '2016-06-18', '2016-06-16 10:34:36', '2016-06-16 08:34:37', 'A'),
(17, 'KSLTA REGULAR  TENNIS COACHING PROGRAMME.', '9', 'galleryimage/stadium45932.jpg', 'A', 'KSLTA REGULAR  TENNIS COACHING PROGRAMME.', 'Kslta-Admin', 'Nil', '2016-06-09', '2016-06-17', '2016-06-18 09:34:07', '2016-06-18 07:35:15', 'A'),
(19, 'test', '20', 'galleryimage/ksltapage26550412.jpg', 'A', '', 'test', 'tet', '2016-10-12', '2016-10-28', '2016-10-22 02:50:27', '2016-10-22 07:50:27', 'A'),
(20, 'Tennis', '25', 'galleryimage/Davis Cup_Ind_Uzb_2017_Deepthi Indukuri-053547092.jpg', 'A', '', 'Admin', 'KSLTA', '1969-12-31', '1969-12-31', '2016-10-24 06:22:29', '2017-07-22 09:43:42', 'A'),
(22, 'NEW YEAR CELEBRATION', '18', 'galleryimage/DSC_19172600.jpg', 'A', '', 'Admin', 'NEW YEAR CELEBRATION', '2017-01-22', '2017-01-31', '2017-01-22 04:44:12', '2017-01-22 10:44:12', 'A'),
(24, 'MENS 50 K ', '23', 'galleryimage/79918.jpg', 'A', '', 'Admin', 'MENS 50 K ', '2017-02-10', '2017-02-20', '2017-02-10 02:57:19', '2017-10-24 14:55:17', 'A'),
(29, 'RTW', '23', 'galleryimage/842116.jpg', 'A', 'RTW', 'Admin', 'RTW Gallery', '1969-12-31', '1969-12-31', '2017-02-11 04:15:29', '2017-10-24 14:54:46', 'A'),
(30, 'ITF - 2017', '23', 'galleryimage/98773.jpg', 'A', '', 'Admin', 'ITF', '2017-03-25', '2017-04-30', '2017-04-18 05:02:50', '2017-10-24 14:54:00', 'A'),
(31, 'Davis Cup-2017', '23', 'galleryimage/529788.jpg', 'A', '', 'Admin', 'Davis Cup-17', '2017-04-01', '2017-05-31', '2017-04-18 05:13:03', '2017-10-24 14:51:06', 'A'),
(32, 'KSLTA Championship Series U-12 Boys & Girls', '23', 'galleryimage/Winner131753.jpg', 'A', '', 'Admin', 'KSLTA Championship Series U-12 Boys & Girls', '2017-09-08', '1969-12-31', '2017-09-08 03:22:31', '2017-09-08 08:22:31', 'A'),
(33, 'Bengaluru Open-2017 ATP Challenger', '21', 'galleryimage/images14401.jpg', 'A', '', 'Bengaluru Open-2017 ATP Challenger', '', '1969-12-31', '1969-12-31', '2018-10-13 01:54:09', '2018-10-13 06:54:09', 'A'),
(34, 'Bengaluru Open-2018 ATP Challenger ', '21', 'galleryimage/LOGO Bengaluru Open-1887278.jpg', 'A', '', 'BENGALURU OPEN ATP CHALLENGER', 'BENGALURU OPEN ATP CHALLENGER', '2018-12-28', '2019-01-15', '2018-12-28 03:34:33', '2018-12-28 09:36:27', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `ksl_gallery_title`
--

CREATE TABLE `ksl_gallery_title` (
  `tit_id` int(11) NOT NULL,
  `tit_value` text NOT NULL,
  `tit_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ksl_gallery_title`
--

INSERT INTO `ksl_gallery_title` (`tit_id`, `tit_value`, `tit_timestamp`) VALUES
(1, 'KSLTA - TOURNAMENT', '2018-12-28 09:32:57');

-- --------------------------------------------------------

--
-- Table structure for table `ksl_images_view`
--

CREATE TABLE `ksl_images_view` (
  `image_id` bigint(20) NOT NULL,
  `image_visible_page` varchar(500) NOT NULL,
  `image_title` varchar(1000) NOT NULL,
  `image_over_title` varchar(500) NOT NULL,
  `image_top_title` varchar(1000) NOT NULL,
  `image_sub_title` text NOT NULL,
  `image_file_location` text NOT NULL,
  `image_file_name` varchar(1000) NOT NULL,
  `image_type` varchar(100) NOT NULL,
  `image_visible_status` enum('P','A') NOT NULL DEFAULT 'P' COMMENT 'P=Pending, A=Active',
  `image_modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `image_delete_status` enum('A','I') NOT NULL COMMENT 'A=Active, I=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ksl_images_view`
--

INSERT INTO `ksl_images_view` (`image_id`, `image_visible_page`, `image_title`, `image_over_title`, `image_top_title`, `image_sub_title`, `image_file_location`, `image_file_name`, `image_type`, `image_visible_status`, `image_modified_at`, `image_delete_status`) VALUES
(1, '20', 'commitee', '1', 'SHRI R ASHOKA', 'PRESIDENT', 'uploads/pages_images/R-Ashoka-Sr.Vice-President_kslta_43_kslta_81.jpg', 'R-Ashoka-Sr.Vice-President_kslta_43_kslta_81.jpg', 'jpg', 'P', '2020-03-11 13:39:35', 'A'),
(2, '20', 'commitee', '2', 'SHRI PRIYANK M KHARGE', 'SR.VICE PRESIDENT', 'uploads/pages_images/Priyank_kslta_10.jpg', 'Priyank_kslta_10.jpg', 'jpg', 'P', '2020-03-11 13:41:50', 'A'),
(3, '20', 'commitee', '3', 'SHRI.M.LAKSHMINARAYAN IAS', 'HON.LIFE VICE PRESIDENT', 'uploads/pages_images/Lakshmi_kslta_82.jpg', 'Lakshmi_kslta_82.jpg', 'jpg', 'P', '2020-03-11 13:44:13', 'A'),
(4, '20', 'commitee', '5', 'SHRI P R RAMASWAMY', 'VICE PRESIDENT', 'uploads/pages_images/RAMASWAMY_kslta_82.jpg', 'RAMASWAMY_kslta_82.jpg', 'jpg', 'P', '2020-09-11 09:41:19', 'A'),
(7, '20', 'commitee', '8', 'SHRI.MAHESHWAR RAO IAS', 'HON.SECRETARY', 'uploads/pages_images/MAHESHWAR-RAO_kslta_48.jpg', 'MAHESHWAR-RAO_kslta_48.jpg', 'jpg', 'P', '2020-09-11 09:35:27', 'A'),
(8, '20', 'commitee', '9', 'SHRI.SUNIL YAJAMAN', 'HON.JT.SECRETARY', 'uploads/pages_images/SUNIL-YAJAMAN_kslta_77.jpg', 'SUNIL-YAJAMAN_kslta_77.jpg', 'jpg', 'P', '2020-09-11 09:36:35', 'A'),
(9, '20', 'commitee', '91', 'SHRI NAGANAND DORASWAMY', 'HON.TREASURER', 'uploads/pages_images/NAGANAND-DORASWAMY_kslta_2.jpg', 'NAGANAND-DORASWAMY_kslta_2.jpg', 'jpg', 'P', '2020-09-11 09:42:04', 'A'),
(11, '18', 'main-home-banner', 'link', 'main-home-banner', 'main-home-banner', 'uploads/pages_images/member-tournament-banner_kslta_4.jpg', 'member-tournament-banner_kslta_4.jpg', 'jpg', 'P', '2016-09-29 13:12:55', 'A'),
(13, '21', 'tournament', 'tournament', '2', 'single_completed', 'uploads/pages_images/Womens-Singles-completed_kslta_78.jpg', 'Womens-Singles-completed_kslta_78.jpg', 'jpg', 'P', '2016-09-29 13:16:36', 'A'),
(14, '21', 'tournament', 'tournament', '3', 'double-completed', 'uploads/pages_images/Womens-Doubles-complated_kslta_96.jpg', 'Womens-Doubles-complated_kslta_96.jpg', 'jpg', 'P', '2016-09-29 13:16:39', 'A'),
(15, '21', 'tournament', 'tournament', '4', 'OOP-Friday', 'uploads/pages_images/OOP-Friday_kslta_20.jpg', 'OOP-Friday_kslta_20.jpg', 'jpg', 'P', '2016-09-29 13:16:42', 'A'),
(16, '18', 'test', 'test', 'test', 'test', 'uploads/pages_images/party-hall62094_KSLTA_kslta_8.jpg', 'party-hall62094_KSLTA_kslta_8.jpg', 'jpg', 'P', '2017-02-11 09:25:05', 'A'),
(17, '18', 'Fountains', 'Tennis come home', 'Tennis come home', 'Fountain', 'uploads/pages_images/NEWS-PAPER-AD-A4_kslta_26.jpg', 'NEWS-PAPER-AD-A4_kslta_26.jpg', 'jpg', 'P', '2020-02-12 05:42:46', 'A'),
(18, '20', 'commitee', '4', 'SHRI.M B DYABERI IAS (RETD)', 'SHRI.M B DYABERI IAS', 'uploads/pages_images/SHRI-M-B-DYABERI-IAS_kslta_97_kslta_25.jpg', 'SHRI-M-B-DYABERI-IAS_kslta_97_kslta_25.jpg', 'jpg', 'P', '2020-03-18 08:25:37', 'A'),
(19, '20', 'commitee', '7', 'SHRI.ROHAN BOPANNA', 'SHRI.ROHAN BOPANNA', 'uploads/pages_images/SHRI-ROHAN-BOPANNA_kslta_46.jpg', 'SHRI-ROHAN-BOPANNA_kslta_46.jpg', 'jpg', 'P', '2020-09-11 09:33:54', 'A'),
(20, '20', 'commitee', '6', 'Ashish Puravnankara', 'Vice President', 'uploads/pages_images/Ashish-Puravnankara_kslta_77.jpg', 'Ashish-Puravnankara_kslta_77.jpg', 'jpg', 'P', '2020-09-11 09:40:21', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `ksl_newsletter_online`
--

CREATE TABLE `ksl_newsletter_online` (
  `newsletter_id` bigint(20) NOT NULL,
  `newsletter_name` varchar(500) NOT NULL,
  `newsletter_email` varchar(500) NOT NULL,
  `newsletter_mobile` varchar(15) NOT NULL,
  `newsletter_email_view_status` enum('P','V','N') NOT NULL DEFAULT 'P' COMMENT 'P=Pending, A=Active, N=None',
  `newsletter_created_at` datetime NOT NULL,
  `newsletter_modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `newsletter_active_status` enum('A','I') NOT NULL COMMENT 'A=Active, I=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ksl_news_tbl`
--

CREATE TABLE `ksl_news_tbl` (
  `news_id` int(11) NOT NULL,
  `news_title` varchar(500) NOT NULL,
  `news_display_tag` varchar(100) NOT NULL,
  `news_link_address` varchar(500) NOT NULL,
  `news_link_status` enum('U','A','R') NOT NULL DEFAULT 'U' COMMENT 'U=Unapproved, A=Approved, R=Rejected',
  `news_content_image` varchar(250) DEFAULT NULL,
  `news_editor_content` text NOT NULL,
  `news_posted_by` varchar(150) NOT NULL,
  `news_posted_at` date NOT NULL,
  `news_located_city` varchar(100) NOT NULL,
  `news_located_country` varchar(100) NOT NULL,
  `news_from_date` date NOT NULL,
  `news_to_date` date NOT NULL,
  `news_created_date` datetime NOT NULL,
  `news_lastupdated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `news_flat` enum('A','D') NOT NULL DEFAULT 'A' COMMENT 'A=Active, D=Deleted',
  `news_tournament_id` varchar(50) NOT NULL,
  `news_meta_name` varchar(100) NOT NULL,
  `news_meta_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ksl_news_tbl`
--

INSERT INTO `ksl_news_tbl` (`news_id`, `news_title`, `news_display_tag`, `news_link_address`, `news_link_status`, `news_content_image`, `news_editor_content`, `news_posted_by`, `news_posted_at`, `news_located_city`, `news_located_country`, `news_from_date`, `news_to_date`, `news_created_date`, `news_lastupdated_date`, `news_flat`, `news_tournament_id`, `news_meta_name`, `news_meta_content`) VALUES
(2, 'Hockey Returns', 'Home', 'www.kslta.com/hockey-title1', 'R', 'uploadimage/German-Rubtsov-Team-Russia-U18-featured-640x4265624.png', '<h1 class=\"entry-title\"><span style=\"color: #ff0000;\">Doping scandal sees entire Russian U18 team replaced at World Championships</span></h1>\r\n<p><span style=\"color: #000000;\">Lorem ipsum represents a long-held tradition for designers, typographers and the like. Some people hate it and argue for its demise, but others ignore the hate as they create awesome tools to help create filler text for everyone from bacon lovers to Charlie Sheen fans. </span></p>\r\n<p style=\"text-align: center;\"><span style=\"color: #000000;\"><img src=\"http://puckjunk.com/wp-content/uploads/2007/10/cover_zoom.jpg\" alt=\"data1\" width=\"407\" height=\"600\" /></span></p>\r\n<p><span style=\"color: #000000;\">Lorem ipsum represents a long-held tradition for designers, typographers and the like. Some people hate it and argue for its demise, but others ignore the hate as they create awesome tools to help create filler text for everyone from bacon lovers to Charlie Sheen fans. </span></p>\r\n<p><span style=\"color: #000000;\">Lorem ipsum represents a long-held tradition for designers, typographers and the like. Some people hate it and argue for its demise, but others ignore the hate as they create awesome tools to help create filler text for everyone from bacon lovers to Charlie Sheen fans. </span></p>\r\n<p>&nbsp;</p>', 'Hockey-team', '2016-04-04', 'Mumbai', '', '2016-04-07', '2016-07-14', '2016-04-07 13:41:00', '2016-06-14 07:12:43', 'A', '2', '', ''),
(3, 'Tennis - open', 'Home', 'www.kslta.com/tennis-roger', 'R', 'uploadimage/Roger_Federer_Wimb_3372540b3630.jpg', '<p><img src=\"http://a4.espncdn.com/combiner/i?img=%2Fphoto%2F2016%2F0118%2Fr44964_1296x518_5-2.jpg\" alt=\"ro\" width=\"500\" height=\"200\" /></p>\r\n<h1><span style=\"color: #000000;\">Our can\'t-miss predictions for rest of 2016</span></h1>\r\n<p><span style=\"color: #000000;\">The ATP and WTA just finished providing us with their first-quarter reports. One was quite a surprise; the other, not so much.</span></p>\r\n<p><span style=\"color: #000000;\">Surely, no one who has been paying attention since 2011 can be puzzled by <a style=\"color: #000000;\" href=\"http://sports.espn.go.com/sports/tennis/players/profile?playerId=296\">Novak Djokovic</a>\'s success so far this year. Conversely, who would have guessed that the first three months would go by without <a style=\"color: #000000;\" href=\"http://sports.espn.go.com/sports/tennis/players/profile?playerId=394\">Serena Williams</a> winning a title? That\'s a genuine stunner.</span></p>\r\n<p><span style=\"color: #000000;\">The two tours couldn\'t possibly be driven by different narrative forces. The men\'s game is all about the relentless march of top-ranked Djokovic; can anyone stop him? The compelling WTA theme is the scramble among a dizzying array of talented players -- from credentialed Grand Slam champions to erratic but undeniably gifted youngsters -- who will try to exploit the sudden, apparent weakness of the woman who still towers over them all, Williams. Can she really be eclipsed, if not dethroned?</span></p>', 'Roger-Manager', '2016-04-05', '', '', '2016-04-07', '2016-05-15', '2016-04-07 13:45:32', '2016-04-15 06:11:26', 'A', '', '', ''),
(4, 'Tabel Tennis - New', 'Others', 'www.kslta.com/tt-news', 'R', 'uploadimage/animaatjes-tafeltennis-610178296.jpg', '<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"https://www.homeleisuredirect.com/Assets/HLD/User/8951-tabletennis512.jpg\" alt=\"Table Tennis\" width=\"512\" height=\"288\" /></p>\r\n<p><span style=\"color: #000000;\"><strong>Table tennis</strong>, also known as <strong>ping pong</strong>, is a sport in which two or four players hit a lightweight ball back and forth across a table using a small paddle. The game takes place on a hard table divided by a net. Except for the initial serve, the rules are generally as follows: Players must allow a ball played toward them to bounce one time on their side of the table, and must return it so that it bounces on the opposite side at least once. Points are scored when a player fails to return the ball within the rules. Play is fast and demands quick reactions. Spinning the ball alters its trajectory and limits an opponent\'s options, giving the hitter a great advantage. When doing so the hitter has a better chance of scoring if the spin is successful.</span></p>\r\n<p>&nbsp;</p>', 'Coach', '2016-04-06', '', '', '2016-04-07', '2016-05-11', '2016-04-07 13:53:44', '2016-04-11 10:02:10', 'A', '', '', ''),
(6, 'The Bombay HC also slammed the state government ', 'Home', 'www.kslta.com/IPL-2016', '', 'uploadimage/476824-gautam-gambhir-kkr-pull-7004753.jpg', '<p><img src=\"http://images.indianexpress.com/2016/04/suresh-raina-m.jpg\" alt=\"IPL\" width=\"759\" height=\"422\" /></p>\r\n<p>E<span style=\"color: #000000;\">arlier, the High Court had reprimanded MCA for the many litres of water it will use to prepare pitches for the world\'s richest cricket tournament at a time when large parts of the state are reeling under drought.</span></p>\r\n<p><span style=\"color: #000000;\">\"How can you waste water like this? Are people more important or IPL? How can you be so careless,\" the court said while hearing a petition, and also, \"This is criminal wastage. You know the situation in Maharashtra.\"</span></p>\r\n<p><span style=\"color: #000000;\">The MCA, in their defence, argued that it purchases water for its use and also that this water is non-potable or water you cannot drink.</span></p>\r\n<p><span style=\"color: #000000;\">The ninth edition of the cash-rich Twenty20 league is set to begin from April 9 with the first match to be played in Mumbai&rsquo;s Wankhede Stadium while the opening ceremony will be held on April 8.</span></p>\r\n<p><span style=\"color: #000000;\"><img src=\"http://images.indianexpress.com/2016/04/miller-m.jpg\" alt=\"TEST\" width=\"759\" height=\"422\" /></span></p>\r\n<p><span style=\"color: #000000;\">A total of 20 matches will be played in Mumbai, Pune and Nagpur. The final is also scheduled to be held at the Wankhede Stadium on May 29. The HC has now asked the government to inform them what they propose to do and the steps it plans to take in such a situation.</span></p>\r\n<div id=\"stcpDiv\" style=\"position: absolute; top: -1999px; left: -1988px;\">The other new team to enter IPL-9 is Gujarat Lions who will be playing their matches in Rajkot. He may not be performing well with the bat&nbsp;as it was visible during the World T20, but IPL will give him the chance to regain form. Moreover, Raina will get a chance to showcase his leadership skills alongside. Raina has played the most number of matches in the IPL and is the competition&rsquo;s all-time leading run scorer. Like Dhoni, Raina also stood by CSK throughout the IPL history. - See more at: http://indianexpress.com/article/sports/cricket/ipl-2016-brief-look-at-the-captains/#sthash.NZk9Ejob.dpuf</div>\r\n<div id=\"stcpDiv\" style=\"position: absolute; top: -1999px; left: -1988px;\">The other new team to enter IPL-9 is Gujarat Lions who will be playing their matches in Rajkot. He may not be performing well with the bat&nbsp;as it was visible during the World T20, but IPL will give him the chance to regain form. Moreover, Raina will get a chance to showcase his leadership skills alongside. Raina has played the most number of matches in the IPL and is the competition&rsquo;s all-time leading run scorer. Like Dhoni, Raina also stood by CSK throughout the IPL history. - See more at: http://indianexpress.com/article/sports/cricket/ipl-2016-brief-look-at-the-captains/#sthash.NZk9Ejob.dpuf</div>', 'ipL', '2016-04-01', '', '', '2016-04-07', '2016-05-08', '2016-04-07 15:56:42', '2016-04-08 12:40:33', 'A', '', '', ''),
(7, 'Shivekar in line for double crown at Cancer Caregivers Club Cup Rs 1.25 Lakh AITA women\'s Tournament', 'Home', 'www.gowdtham.com', 'R', 'uploadimage/Chrysanthemum27352.jpg', '<p style=\"margin: 0in 0in 4.5pt; line-height: 14.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><span style=\"font-size: 14pt; font-family: Calibri, sans-serif; color: #141823; letter-spacing: 0.4pt;\"><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"https://cbsdallas.files.wordpress.com/2015/03/483456245.jpg\" alt=\"test\" width=\"625\" height=\"352\" /></span></p>\r\n<p style=\"margin: 0in 0in 4.5pt; text-align: justify; line-height: 14.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\">&nbsp;</p>\r\n<p style=\"margin: 0in 0in 4.5pt; text-align: justify; line-height: 14.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><span style=\"font-size: 14pt; font-family: Calibri, sans-serif; color: #141823; letter-spacing: 0.4pt;\">Shivekar in line for double crown at Cancer Caregivers Club Cup Rs 1.25 Lakh AITA women\'s Tennis Tournament</span></p>\r\n<p style=\"margin: 0in 0in 4.5pt; text-align: justify; line-height: 14.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><span style=\"font-size: 14pt; font-family: Calibri, sans-serif; color: #141823; letter-spacing: 0.4pt;\">&nbsp;</span></p>\r\n<p style=\"text-align: justify; margin: 4.5pt 0in; line-height: 14.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><span style=\"font-size: 14pt; font-family: Calibri, sans-serif; color: #141823; letter-spacing: 0.4pt;\">Pune, April 7: It will be an all Maharashtra clash for the title as top-seeded Aastha Dhargude will take on Pareen Shivekar in the women\'s singles finals at the Rs.1.25 lakh Cancer Caregivers Club Cup AITA women\'s Tennis Tournament being organized by Deccan Gymkhana club and Cancer Caregivers Club and played on the Deccan Gymkhana tennis court.</span></p>\r\n<p style=\"margin: 4.5pt 0in; text-align: justify; line-height: 14.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><span style=\"font-size: 14pt; font-family: Calibri, sans-serif; color: #141823; letter-spacing: 0.4pt;\">&nbsp;</span></p>\r\n<p style=\"text-align: justify; margin: 4.5pt 0in; line-height: 14.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><span style=\"font-size: 14pt; font-family: Calibri, sans-serif; color: #141823; letter-spacing: 0.4pt;\">In the singles<span class=\"apple-converted-space\">&nbsp;</span><span class=\"textexposedshow\">semifinals, top-seeded Aastha Dargude spent 1 hour and 55 minutes to get past fourth-seeded Sahan Shetty of Karnataka 6-4, 1-6, 6-3</span></span></p>\r\n<p style=\"margin: 0in 0in 4.5pt; text-align: justify; line-height: 14.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><span style=\"font-size: 14pt; font-family: Calibri, sans-serif; color: #141823; letter-spacing: 0.4pt;\">&nbsp;</span></p>\r\n<p style=\"margin: 0in 0in 4.5pt; text-align: justify; line-height: 14.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><span style=\"font-size: 14pt; font-family: Calibri, sans-serif; color: #141823; letter-spacing: 0.4pt;\">In the second semifinals, second-seeded Pareen Shivekar also of Maharashtra stopped the winning run of fifth-seeded Simran Kejriwal 7-5, 6-4</span></p>\r\n<p style=\"margin: 4.5pt 0in; text-align: justify; line-height: 14.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><span style=\"font-size: 14pt; font-family: Calibri, sans-serif; color: #141823; letter-spacing: 0.4pt;\">&nbsp;</span></p>\r\n<p style=\"margin: 4.5pt 0in; text-align: justify; line-height: 14.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><span style=\"font-size: 14pt; font-family: Calibri, sans-serif; color: #141823; letter-spacing: 0.4pt;\">Mumbai girl Shivekar also put herself in line for a double crown as she won the doubles title partnering Harsha Sai Challa . In doubles finals, Shivekar and Challa scored a straight set win over Sindhu Janagam and Tanasha Khandpur 6-4, 6-2</span></p>\r\n<p style=\"margin: 4.5pt 0in; text-align: justify; line-height: 14.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><span style=\"font-size: 14pt; font-family: Calibri, sans-serif; color: #141823; letter-spacing: 0.4pt;\">&nbsp;</span></p>\r\n<p style=\"margin: 4.5pt 0in; text-align: justify; line-height: 14.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><span style=\"font-size: 14pt; font-family: Calibri, sans-serif; color: #141823; letter-spacing: 0.4pt;\">The finals will be played tomorrow from</span></p>\r\n<p style=\"margin: 4.5pt 0in; text-align: justify; line-height: 14.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><span style=\"font-size: 14pt; font-family: Calibri, sans-serif; color: #141823; letter-spacing: 0.4pt;\">&nbsp;</span></p>\r\n<p style=\"margin: 4.5pt 0in; text-align: justify; line-height: 14.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><span style=\"font-size: 14pt; font-family: Calibri, sans-serif; color: #141823; letter-spacing: 0.4pt;\">Following are the results: Singles: Semifinal Round:</span></p>\r\n<p style=\"margin: 4.5pt 0in; text-align: justify; line-height: 14.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><span style=\"font-size: 14pt; font-family: Calibri, sans-serif; color: #141823; letter-spacing: 0.4pt;\">Aastha Dargude(Mah,1) bt Sahan Shetty(Kar,4) 6-4, 1-6, 6-3<br />Pareen Shivekar(Mah,2) bt Simran Kejriwal(Mah,5) 7-5, 6-4</span></p>\r\n<p style=\"margin: 4.5pt 0in; text-align: justify; line-height: 14.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><span style=\"font-size: 14pt; font-family: Calibri, sans-serif; color: #141823; letter-spacing: 0.4pt;\">&nbsp;</span></p>\r\n<p style=\"margin: 4.5pt 0in; text-align: justify; line-height: 14.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><span style=\"font-size: 14pt; font-family: Calibri, sans-serif; color: #141823; letter-spacing: 0.4pt;\">Doubles Final:</span></p>\r\n<p style=\"margin: 4.5pt 0in; text-align: justify; line-height: 14.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><span style=\"font-size: 14pt; font-family: Calibri, sans-serif; color: #141823; letter-spacing: 0.4pt;\">&nbsp;</span></p>\r\n<p style=\"margin: 4.5pt 0in; text-align: justify; line-height: 14.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><span style=\"font-size: 14pt; font-family: Calibri, sans-serif; color: #141823; letter-spacing: 0.4pt;\">Pareen Shivekar/ Harsha Sai Challa bt Sindhu Janagam/ Tanasha Khandpur 6-4, 6-2</span></p>\r\n<p>&nbsp;</p>', 'KSLTA Staff', '2016-08-04', ' Kalaburagi', 'India', '2016-04-08', '2016-05-09', '2016-04-08 08:00:54', '2016-04-11 06:55:55', 'A', '', '', ''),
(8, 'Champions League: Can Real script comeback against Wolfsburg at the Bernabeu?', 'Home', 'www.google.com', 'R', 'uploadimage/11real-madrid2322.jpg', '<p><span style=\"font-size: 36px;\">R</span>eal Madrid are determined to create an intimidating atmosphere for visitors VfL Wolfsburg as they try to overturn a two-goal deficit in Tuesday\'s Champions League quarter-final second leg at the Santiago Bernabeu stadium.</p>\r\n<p>&ldquo;Tuesday will be like a war, with 80,000 spectators; we are going to try and walk all over Wolfsburg,\" said defender Dani Carvajal.</p>\r\n<p>&ldquo;We&rsquo;re going to play to the death and with 80,000 souls behind us we will do it,&rdquo; added winger Lucas Vazquez.</p>\r\n<p>Forward Cristiano Ronaldo told the club website (<a href=\"http://www.realmadrid.com\" target=\"_blank\">www.realmadrid.com</a>): &ldquo;We need to have cold hearts and play with patience and know how to suffer. Getting through is our big objective, it&rsquo;s the only thing that will let the players and fans leave the stadium happy.\"</p>\r\n<p>Madrid are third in La Liga and enjoyed a 4-0 win over Eibar on Saturday to close the gap with leaders Barcelona to four points, lifting their confidence after the shock 2-0 defeat at Wolfsburg last Wednesday.</p>\r\n<p><img src=\"http://im.rediff.com/sports/2016/apr/11ronaldo.jpg\" alt=\"reee\" width=\"670\" height=\"553\" /></p>', 'Ksla-staff', '2016-06-04', 'Bangalore', 'India', '2016-04-11', '2016-05-11', '2016-04-11 09:03:09', '2016-04-11 10:02:17', 'A', '', '', ''),
(9, 'Playing international cricket for South Africa is definitely an option, says Kevin Pietersen', 'Home', 'www.google.com/fff1', 'R', 'uploadimage/Kevin-Pietersen-Getty80911.jpg', '<p><img src=\"http://s4.firstpost.in/wp-content/uploads/2016/04/Kevin-Pietersen-Getty.jpg\" alt=\"kp\" width=\"380\" height=\"285\" /></p>\r\n<p>An English call-up is definitely still an option. I am in a great frame of mind now.\"</p>\r\n<p>One of the major fallouts during Pietersen\'s England career came during the 2012 series against South Africa when he sent derogatory texts about then captain Andrew Strauss to Graeme Smith.</p>\r\n<p>The batsman was left out for the final Test of that series before returning in India, where he helped England win the series, before relations between him and the ECB took another turn for worse.</p>\r\n<p>Pietersen left his home country in 2004 to play for England. He scored 8,181 runs in his 104-Test career, notching up 23 hundreds at an average of 47.28.</p>', 'Ksla-staff', '1970-01-01', '', 'India', '2016-04-11', '2016-06-05', '2016-04-11 09:21:34', '2016-05-05 05:44:58', 'A', '', '', ''),
(17, 'Prerna Hat-Trick At Kalaburagi', 'Home', 'www.kslta.com', 'R', 'uploadimage/39-solarpowered97357.jpg', '<p>dfsdf</p>', 'kslta-admin', '2016-05-05', '', 'India', '2016-05-05', '2016-07-17', '2016-05-05 11:34:08', '2016-06-17 09:45:29', 'A', '', '', ''),
(18, 'Prerna Hat-Trick At Kalaburagi', 'Others', 'www.kslta.com', 'R', 'uploadimage/appleasf_559_04271610125633514.jpg', '<p>sdf</p>', 'kslta-data', '2016-05-05', 'Kalaburagi', 'India', '2016-05-05', '2016-07-01', '2016-05-05 11:36:46', '2016-05-31 12:16:17', 'A', '', '', ''),
(19, 'Suresh Raina and IPL records: Two sides of the same coin', 'Home', 'www.kslta.com', 'R', 'uploadimage/big_353404_142190015383801.jpg', '<p>sdfsdf</p>', 'kslta-admin', '2016-04-05', 'Chennai', 'India', '2016-05-05', '2016-07-17', '2016-05-05 11:42:02', '2016-06-17 09:45:27', 'A', '', '', ''),
(24, 'test33333', 'Home', 'sdf sdsdf', 'R', '', '<p>sdf</p>', 'sdf', '2016-05-06', 'sdf', 'sdf', '2016-06-06', '2016-07-14', '2016-06-06 15:26:47', '2016-06-14 06:47:12', 'A', '9', '', ''),
(25, 'sdf', 'Home', 'sdf', 'R', '', '<p>sdf</p>', 'sdf', '2016-06-06', 'sdf', 'sf', '2016-06-06', '2016-07-17', '2016-06-06 15:30:17', '2016-06-17 09:45:23', 'A', '8', '', ''),
(26, 'sdf', 'Home', 'sdf', 'R', '', '<p>sdf</p>', 'sdf', '2016-06-06', 'sdf', 'sf', '2016-06-06', '2016-07-17', '2016-06-06 15:30:48', '2016-06-17 09:45:21', 'A', '8', '', ''),
(27, 'test ddddddd', 'Home', 'sdfsdf', 'R', '', '<p>sdf</p>', 'sdf', '2016-01-06', 'sdf', 'sdf', '2016-06-06', '2016-07-17', '2016-06-06 16:09:03', '2016-06-17 09:45:19', 'A', '7', '', ''),
(28, 'testgfs', 'Others', 'testfs', 'R', 'uploadimage/654998.jpg', '<p>test</p>', 'test', '2016-06-06', 'stest', 'test', '2016-06-13', '2016-07-17', '2016-06-13 15:56:04', '2016-06-17 09:45:17', 'A', '12', '', ''),
(29, 'test11111', 'Others', 'testfs', 'R', '', '<p>sdfaddasasdf</p>', 'sda', '1970-01-01', 'asdf', 'sdfa', '2016-06-14', '2016-07-17', '2016-06-14 07:56:03', '2016-06-17 09:45:15', 'A', '9', '', ''),
(30, 'ssssssssss', 'Others', 'sssssssss', 'R', '', '<p>sssssssssss</p>', 'sss', '2016-09-06', 'sss', 'sss', '2016-06-14', '2016-07-17', '2016-06-14 08:25:55', '2016-06-17 09:45:13', 'A', '9', '', ''),
(31, 'SHWETA RANA PREVAILS IN LONG DRAWN BATTLE.', 'Home', 'www.kslta.com', 'R', 'uploadimage/Shweta-Rana-Prevails-In-Long-Drawn-Battle82739.jpg', '<p class=\"desc_news\">In a long drawn battled between two unseeded Indian players, Swetha Rana prevailed over Amrita Mukherjee 6-0, 5-7, 7-6 (4) in the first round of the RGUHS ITF Kalaburagi Open here at the Chandrashekar Patil stadium on Tuesday. The two players slugged it on court for 2 hours and 41 minutes before Rana managed to edge into the second round of the $10,000 tournament.</p>\r\n<p class=\"desc_news\">There was not much difference between the players who traded serves other than in the first set when Rana looked like she would breeze through with ease when she wrapped up the first set 6-0 her Bengali opponent in the 1, 3 and 5th games. The second set saw players breaking serves and restoring parity at 5-all before Mukherjee held to go up 6-5 and then broke Rana in the 12th game to clinch the set.</p>\r\n<p class=\"desc_news\">The decider went the way of the second set and the tie-breaker had to be enforced. Rana took a 5-2 lead before Mukherjee clinched two points. But Rana held on to her serves to win the tie-break 7-4. Meanwhile seeded players advanced without too much of a fuss baring fifth seed Sharmadaa Baluu who pulled out with injury. Top seed Prerna Bhambri breezed past Xi-Yao Wang 6-1, 6-1, seventh seed Eetee Maheta sailed past qualifier Sindhu Janagam 6-3, 6-2 and third seed Druthi Tatachar bested Varunya Chandrashekar 6-2, 6-0.</p>\r\n<p class=\"desc_news\">Karnataka&rsquo;s Sharmadaa Baluu retired from her match against Sneha Padmata after one game. The tournament fifth seed was suffering from an inflamed ankle injury and decided not to push the injury.</p>\r\n<p class=\"desc_news\">Singles 1st round: (All Indians unless mentioned): (Q) Sahan Shetty bt (Q) Bhargavi Nitin Mangudkar 6-0, 6-1; (7) Eetee Maheta bt (Q) Sindhu Janagam 6-3, 6-2; Sneha Padamata bt (5) Sharrmadaa Baluu (retired) 1-0; Shweta Rana bt Amrita Mukherjee 6-0, 5-7, 7-6 (4); Sai Samhitha Chamarthi bt Arthi Muniyan 6-1, 6-0; (1) Prerna Bhambri bt Xi-Yao Wang 6-1, 6-1; (3) Druthi Tatachar Venugopal bt Varunya Chandrashekar 6-2, 6-0.</p>\r\n<p class=\"desc_news\">Doubles: Round of 16: (2) Nidhi Chilumula / Eetee Maheta bt Sindhu Janagam / Amreen Naaz 6-0, 6-1; Riya Bhatia / Shweta Rana bt Sri Sai Shivani / Sahan Shetty 6-3, 6-0; Tamachan Momkoonthod (Tha) / Plobrung Plipuech (Tha) bt Amrita Mukhejee / Sansriti Ranjan 6-2, 6-1.</p>\r\n<p class=\"desc_news important_news data\">by KSLTA Staff Nov 24, 2015 - Kalaburagi, India</p>', 'test', '2016-09-06', 'Kalaburagi', 'Kalaburagi', '2016-06-14', '2016-07-17', '2016-06-14 09:19:01', '2016-06-17 09:45:11', 'A', '', '', ''),
(32, 'testasdf', 'Others', 'sdfsdaf', 'R', '', '<p>sssssssssssss</p>', 'jjjjjjj', '2016-02-06', 'fffffff', 'hhhhhh', '2016-06-16', '2016-07-17', '2016-06-16 12:35:10', '2016-06-17 05:25:06', 'A', '12', '', ''),
(33, 'tester', 'Others', 'sdfsdf', 'R', '', '<p>asdfsdfa</p>', 'sdfaasdf', '2016-10-06', 'sdfsdfa', 'asdfasdf', '2016-06-16', '2016-07-17', '2016-06-16 13:08:54', '2016-06-17 09:45:06', 'A', '12', '', ''),
(45, 'India VS NZ', 'Others', 'cricinfo.com', 'R', 'uploadimage/240105.jpg', '<p>India won by 5 wickets</p>', 'Admin', '2016-10-04', 'Dharmasala', 'India', '2016-10-18', '2016-11-18', '2016-10-18 00:26:16', '2016-10-18 05:27:38', 'A', '', 'Karman – Druthi Claim Second Crown In Two Weeks', 'Karman – Druthi Claim Second Crown In Two Weeks'),
(46, 'India VS NZ', 'Others', 'cricinfo.com', 'R', 'uploadimage/188479.jpg', '<p>dsfsdf</p>', 'Admin', '2016-10-16', 'Dharmasala', 'India', '2016-10-18', '2016-11-18', '2016-10-18 00:29:46', '2016-10-18 05:30:34', 'A', '', 'Karman – Druthi Claim Second Crown In Two Weeks', 'Karman – Druthi Claim Second Crown In Two Weeks'),
(47, 'India VS NZ', 'Others', 'cricinfo.com', 'R', 'uploadimage/261298.jpg', '<p>asadsa</p>', 'Admin', '2016-10-10', 'Dharmasala', 'India', '2016-10-18', '2016-11-18', '2016-10-18 00:39:35', '2016-10-18 05:41:46', 'A', '13', 'Karman – Druthi Claim Second Crown In Two Weeks', 'Karman – Druthi Claim Second Crown In Two Weeks'),
(49, 'Yuki to lead challenge in Air Asia Open', 'Home', 'Yuki-to-lead-challenge-in-Air-Asia-Open', 'R', 'uploadimage/AirAsia54712.jpg', '<p class=\"desc_news\">ATP Challenger returns to the Garden City Bengaluru after twelve (12) years with the Air Asia Open. India&rsquo;s Yuki Bhambri with an ATP ranking of 104 will spearhead the challenge in this ATP $50,000 Challenger, organized by Karnataka State Lawn Tennis Association (KSLTA).</p>\r\n<p class=\"desc_news\">The draw also has five other Indians in the main draw and Somdev Devvarman is expected to be one of the strong contender apart from other Davis Cuppers Saketh Myneni, Sanam Singh, Ramkumar Ramanathan and Vijay Sunder Prashant would be vying for honors.</p>\r\n<p class=\"desc_news\">The Air Asia Open starts on Monday, 19 October and the final is slated for Sunday, 25 October 2015. The qualifiers on October 17 and 18 will precede the main draw.</p>\r\n<p class=\"desc_news\">Spain&rsquo;s Adrian Menendez-Maceiras will be the No.2 with a 132 ATP ranking amongst players from 15 countries. Adrian Menendez-Maceiras and James Ward, the second and third seeds at Bengaluru are part of their respective country&rsquo;s Davis Cup squad for 2015.</p>\r\n<blockquote>\r\n<p class=\"desc_news important_news\"><em>Speaking on the occasion Mr. M. Laxminarayan IAS, Principal Secretary, GOK &amp; Chairman, Organising Committee said, &ldquo;Karnataka has a cultural history that has supported sports. Events like the Air Asia Open go a long way in encouraging our own backyard talent and helps build Bengaluru and Karnataka as a top International sporting destination thus promoting sports tourism. We at KSLTA are proud to host this ATP Challenger event. We hope our boys grab the opportunity to showcase their talent and perform to potential to better their own ATP rankings. We expect this event to be a treat to Bengalureans as we would witness the top Indian players and tomorrow&rsquo;s champions in action. We welcome Air Asia as the title sponsor of this event and thank them profusely for their support and all the other sponsors. I take pleasure in announcing a Wild Card for Karnataka&rsquo;s top player Suraj Prabodh from Mysore.</em></p>\r\n</blockquote>\r\n<p class=\"desc_news\">The Air Asia Open offers $7,200 (Rs.4,71,00) and 80 ATP points to the winner while the runners up gets $4,053 ( Rs. 2,65,000) and 55 ATP points. The Semifinalist will get $2,216 (Rs 145,000 each) and 33 points, while the Quarter Finalists stand to earn $1,267 (Rs. 83,000 each) and 17 points. The First round winner gets $ 760 (Rs. 50,000 each) and 8 ATP points while the Main Draw player gets $444 (Rs 29000 each) and 0 points. However the qualifier for the Main draw gets 5 ATP points.</p>\r\n<p class=\"desc_news\">While expressing excitement about Air Asia&rsquo;s association with this event, the Commercial Director of Air Asia India, Mr. Gaurav Rathore said &ldquo;Sports is a constant vertical that Air Asia has been supporting and is passionate about for a very long time. There\'s a long list of sports that we have associated with in the past with a single-minded objective of being out there where the action is. Tennis is something that is hugely followed in our country and coming on board as the Title Sponsor for ATP gives the entire team at Air Asia India immense pride. We are glad to be associate with a brand like ATP which is widely followed all over the world.\"</p>\r\n<p class=\"desc_news\">The return of an ATP Challenger to the IT capital of India after a gap of 12 years re-asserts Bengaluru&rsquo;s position as an international tennis destination in the world. Bengaluru has been in the forefront of hosting Tennis of international stature for a long time having conducted a plethora of events like the Davis Cup, WTA Tier-II, Tier-III and ITF professional circuit tennis tournaments.</p>', 'KSLTA Admin', '2015-10-16', 'Bangalore', 'India', '2016-10-25', '2017-02-03', '2016-10-25 11:54:39', '2017-01-03 11:50:06', 'A', '', 'Description', 'ATP Challenger returns to the Garden City Bengaluru after twelve (12) years with the Air Asia Open. India’s Yuki Bhambri with an ATP ranking of 104 will spearhead the challenge in this ATP $50,000 Challenger'),
(50, 'Yuki withdraws from Air Asia Open', 'Home', 'Yuki-withdraws-from-Air-Asia-Open', 'R', 'uploadimage/yuki-withdraws-Airasia-Open57951.jpg', '<p class=\"desc_news\">In a major disappointment to the organizers of the Air Asia Open, $50,000 Challenger event, Indian top seed Yuki Bhambri withdrew from the event late on Friday night.</p>\r\n<p class=\"desc_news\">The Davis Cupper has been on a roll winning a few challenger events this year and making the last four or above of a few more events.</p>\r\n<p class=\"desc_news\">After losing in the semi-finals of a $125,000 event at Uzbekistan on Saturday, in a message Yuki said, &ldquo;I was really looking forward to come to Bangalore but unfortunately I&rsquo;m having to pull out. I have played a lot of tennis over the last few weeks and have a few niggles. My body requires to recharge to play well in my remaining tournaments for this year. My apologies to tennis fans of Air Asia Open and I wish the tournament the best&rdquo;.</p>\r\n<p class=\"desc_news\">Russian Ivan Nedelko also pulled out allowing for next in line Jeevan Nedunchezhiyan (Ind), who was granted a wild card on Friday, make the main draw. That Wild Card was handed out to another Indian, former National Champion Mohit Mayur Jayaprakash with an ATP ranking of 597.</p>\r\n<h4>FIVE INDIANS TAKE A STEP CLOSER</h4>\r\n<p class=\"desc_news\">In a battle between Indians in the round of 16 of the qualifiers on the Air Asia Open, $50,000 Challenger event being conducted by the Karnataka State Lawn Tennis Association, Christopher Marquis, Chandril Sood and Jayesh Pungliya were stretched before prevailing on Saturday.</p>\r\n<p class=\"desc_news\">Jayesh had to rally back from a set down and dig deep into his reserves to dispose of Niki K Poonacha 3-6, 6-3, 6-3 while Marquis dropped the second before claiming the decider after winning the first set with a break in the 11th game against Vignesh Peranamallur.</p>\r\n<p class=\"desc_news\">Chandril Sood found competition with Wild Card entrant Rashein Samuel. The duo played out a competitive contest, Sood winning the first set 6-4, losing the second 1-6 with Rashein roaring back. However Sood held his nerves in the decider to sail a step closer to qualifying for the main draw that begins on Monday, October 19.</p>\r\n<p class=\"desc_news\">Prajnesh Gunneswaran spent the least time on court when he easily defeated Wild Card Ribhav Ravikiran 6-0, 6-2.</p>\r\n<p class=\"desc_news\">In the last match of the day Lakshit Sood breezed past Anvit Bendre in straight sets, winning 6-3, 7-5.</p>', 'KSLTA Admin', '2015-10-17', 'Bangalore', 'India', '2016-10-25', '2017-02-03', '2016-10-25 12:01:49', '2017-01-03 11:50:11', 'A', '', 'Description', 'In a major disappointment to the organizers of the Air Asia Open, $50,000 Challenger event, Indian top seed Yuki Bhambri withdrew from the event late on Friday night.'),
(51, 'Three Indians Make Main Draw', 'Home', 'Three-Indians-Make-Main-Draw', 'R', 'uploadimage/Three-Indians-Make-Main-Draw92163.jpg', '<p class=\"desc_news\">Three more Indians made it to the main draw of the Air Asia Open $50,000 event on Sunday, taking the tally of Indians in the main draw to 11, two winning their respective matches and Lakshit Sood as a lucky loser, in the final round of the qualifiers.</p>\r\n<p class=\"desc_news\">Sidharth Rawat (Ind), ranked 663 on the ATP list downed another Indian Jayesh Pungliya 6-4, 6-0 and Prajnesh Gunneswaran (845) bested Lakshit Sood by an identical margin.</p>\r\n<p class=\"desc_news\">However, with Indian Jeevan Nedunchezhiyan pulling out of the main drawn citing fever, Lakshit Sood squeezed into the main draw as a lucky loser.</p>\r\n<p class=\"desc_news\">Two others to make the main draw from the qualifying rounds was Temur Ismailov (Uzb), a convincing 6-3, 7-6 (6) victor over Indian Christopher Marquis and Petros Chrysochos (Cyp) who carved out a 7-6 (5), 6-3 verdict over Chandril Sood.</p>\r\n<p class=\"desc_news\">Top seed Adrian Mendez-Maceiras of Spian will take on qualifier Petros Chrysochos of Cyprus in the first round on the opening day on Center Court in the Air Asia Open, at the KSLTA Courts on Monday.</p>\r\n<p class=\"desc_news\">However, Indian Wild Card entrant Suraj Prabodh will step on Center Court in the first match of the day when he clashes with Arthur de Greef of Belgium.</p>\r\n<p class=\"desc_news\">Tournament 5th seed Yannick Mertens (Bel) clashes with N Vijay Sundar Prashanth (Ind), 6th seed Ti Chen (Tpe) takes on qualifier Temur Ismailov of Uzbekistan and former National champion Mohit Mayur Jayaprakash, handed a Wild Card to the main draw, has drawn qualifier and country-mate Prajnesh Gunneswaran.</p>\r\n<p class=\"desc_news\">Indian star and Davis Cupper Somdev DevVarman will be seen in action in the second half of the day when he teams up with Christopher Marquis (Ind) and takes on another Wild Card pair, the Sood Brothers, Chandril and Lakshit in the pre-quarters of the doubles competition.</p>\r\n<p class=\"desc_news\">Top seeds Maximilian Neuchrist (Aut) and Divij Sharan (Ind) will clash with Petros Chrysochos (Cyp) and Temur Ismailov (Uzb).</p>', 'KSLTA Admin', '2015-10-18', 'Bangalore', 'India', '2016-10-25', '2017-02-03', '2016-10-25 12:21:15', '2017-01-03 11:49:45', 'A', '', 'Description', 'Three more Indians made it to the main draw of the Air Asia Open $50,000 event on Sunday, taking the tally of Indians in the main draw to 11, two winning their respective matches and Lakshit Sood as a lucky loser, in the final round of the qualifiers.'),
(52, 'Adrian sails into last 16', 'Home', 'Adrian-sails-into-last-16', 'R', 'uploadimage/Adrian-sails-into-last-1654196.jpg', '<p class=\"desc_news\">Tournament top seed Adrian Menendez-Maceiras of Spain coasted into the last 16 of the Air Asia Open at the KSLTA Courts with a convincing straight sets victory over qualifier Petros Chrysochos of Cyprus 6-4, 6-4 in the first round.</p>\r\n<p class=\"desc_news\">Taking onto Center Court in the second match of the day, the Spaniard broke his lower ranked opponent in the very first game of the contest.</p>\r\n<p class=\"desc_news\">The World 132 ranked player was on roll and suddenly lost concentration to be broken in the sixth game to level game scores. However, the Spaniard scolded himself and came back strongly to earn a break in the very next game to win the first set.</p>\r\n<p class=\"desc_news\">Second set saw the Spaniard not letting his guard down and after breaking the Cyprus player in the first game, was content to hold serve to pocket the set and match.</p>\r\n<p class=\"desc_news\">No 5 seed Yannick Mertens of Belgium raced away to a 6-2, 6-2 victory over Indian N Vijay Sundar Prashanth and No 6 Ti Chen of Chinese Taipei did not raise much of a sweat brushing aside the challenge of Uzbekistan qualifier Temur Ismailov 6-2, 6-4.</p>\r\n<p class=\"desc_news\">In what can be termed an upset, Indian qualifier Prajnesh Gunneswaran demolished former Indian National champion and wild card entrant Mohit Mayur Jayaprakash 6-3, 6-4.</p>\r\n<h4>TOP SEEDS OUSTED</h4>\r\n<p class=\"desc_news\">In doubles play, the tournament top seeds Maximillian Neuchrist of Austria and India&rsquo;s Diviji Sharan were sent packing by the unseeded Uzbek-Cyprus pair of Petros Chrysochos and Temur Ismailov through the Super Tie-breaker.</p>\r\n<p class=\"desc_news\">The top seeds won the first set tie-break at 3 before seeing their unseeded opponents raising the level of their game to not just challenge them but rally back to force the Super Tie-Break.</p>\r\n<p class=\"desc_news\">The top seeds did have their chance when they came back from 0-4 to be 5-6, but Petros and Temur sealed victory with gritty tennis.</p>\r\n<p class=\"desc_news\">Former Olympian and Davis Cupper Vishnu Vardhan of India paired up with Wimbledon Junior doubles crown winner Sumil Nagal and scripted a wonderful victory to storm into the last eight.</p>\r\n<p class=\"desc_news\">After losing the first set to the Czech-Russian pair of Michal Konecny and Alexander Kudryavtsev 4-6, the Indian pair rallied brilliantly to win the second set tie-break at 4 and clinched the decider 10-6.</p>\r\n<p class=\"desc_news\">In an all Indian affair where both the teams earned Wild Cards, the Sood brothers Chandril and Lakshit bested Somdev Devvarman and Christopher Marquis 7-6 (3), 7-6 (2).</p>', 'KSLTA Admin', '2015-10-19', 'Bangalore', 'India', '2016-10-25', '2017-02-03', '2016-10-25 12:24:50', '2017-01-03 11:49:40', 'A', '', 'Description', 'Tournament top seed Adrian Menendez-Maceiras of Spain coasted into the last 16 of the Air Asia Open at the KSLTA Courts with a convincing straight sets victory over qualifier Petros Chrysochos of Cyprus 6-4, 6-4 in the first round.'),
(53, 'Sanam ousts Germain to move into round 2', 'Home', 'Sanam-ousts-Germain-to move-into-round-2', 'R', 'uploadimage/sanam-singh6288.jpg', '<p class=\"desc_news\">Unseeded Indian Sanam Singh caused the biggest upset of the AirAsia Open, $50,000 Challenger tournament accounting for tournament 8th seed Germain Gigounon of Belgium in straight sets 6-4, 6-0 at the KSLTA Courts on Tuesday.</p>\r\n<p class=\"desc_news\">The World No 279 was on a roll against his World 220 ranked opponent slamming in five aces en-route to the second round of the singles play.</p>\r\n<p class=\"desc_news\">The second set took all of 20 minutes as the Indian cashed in on all the three breakpoints he enjoyed to shut out the Belgian.</p>\r\n<p class=\"desc_news\">Indian ace Davis Cupper Somdev DevVarman overcame small hiccups mid-way through the first set to move into the second round with a straight sets victory against Austrian Maximillian Neuchrist, winning 6-4, 6-3.</p>\r\n<p class=\"desc_news\">Somdev claimed a break in the sixth game of the first set when on his second break point his opponent returned long.</p>\r\n<p class=\"desc_news\">But the joy was short lived for the tournament fourth seed who was broken back the very next game to set the set going on serves.</p>\r\n<p class=\"desc_news\">However, Somdev, the World No. 181 bided his time to break the Belgian in the 10th game to claim the first set.</p>\r\n<p class=\"desc_news\">In the second set Somdev stayed with his serve while breaking his opponent in the sixth game to clinch the tie.</p>\r\n<p class=\"desc_news\">Having not enjoyed the best of years on the circuit, Somdev said, &ldquo;It has been a tough year. I have been working hard and have been staying fit. I have had a lot of close contests and have lost more than I have won this year&rdquo;.</p>\r\n<p class=\"desc_news\">When asked whether he felt there was a problem with his game, Somdev said, &ldquo;I have won two Challenger events this year. In the past I have won one. The level of tennis is there. I have struggled to get past the first round in most of the events. I take time to settle down and when I am in the quarters or semis, I begin playing well. Now that I have gone past the first round here, I hope things fall in place and I get going&rdquo;.</p>\r\n<p class=\"desc_news\">Another Indian Saketh Myneni, fresh from defending his Challenger title in Vietnam, surged to a 6-3, 7-6 (4) victory over Serbia&rsquo;s Nikola Milojevic to take his place in the second round.</p>\r\n<p class=\"desc_news\">Tournament second seed James Ward (GBR) hardly raised a sweat demolishing Lucky Loser, Lakshit Sood of India with an identical 6-1, 6-1 score.</p>\r\n<p class=\"desc_news\">Sumit Nagal, the 2015 Jr Wimbledon doubles title winner, given a wild card here, coasted past country mate, qualifier Sidarth Rawat 6-4, 6-3.</p>\r\n<p class=\"desc_news\">In doubles competition, the third seed pair of Dane Propoggia (Aus) and Ti Chen (Tpe) along with Spanish fourth seeds Adrian Menendez-Maceiras and Gerard Granollers moved into the quarter-finals with comfortable victories.</p>', 'KSLTA Admin', '2015-10-20', 'Bangalore', 'India', '2016-10-25', '2017-02-03', '2016-10-25 12:27:04', '2017-01-03 11:50:17', 'A', '', 'Description', 'Unseeded Indian Sanam Singh caused the biggest upset of the AirAsia Open, $50,000 Challenger tournament accounting for tournament 8th seed Germain Gigounon of Belgium in straight sets 6-4, 6-0 at the KSLTA Courts on Tuesday.'),
(54, 'Saketh sails into last eight', 'Home', 'Saketh-sails-into-last-eight', 'R', 'uploadimage/Saketh-sails-into-last-eight80805.jpg', '<p class=\"desc_news\">Tournament 3rd seed Saketh Myneni faced a few challenges from Australian Dane Propoggia before prevailing 6-4, 7-6 (9) to move into the last eight of the Air Asia Open, $50,000 Challenger event at the KSLTA on Wednesday.</p>\r\n<p class=\"desc_news\">In another match, fifth seed Yannick Mertens of Belgium staved off the challenge of Wild Card entrant, current Jr Wimbledon Doubles Champion, Sumit Nagal.</p>\r\n<p class=\"desc_news\">Top seed Adrian Menendez-Maceiras watched qualifier Prajnesh Gunneswaran of India take a set off him before he rallied back to move into the quarters with a hard fought 6-2, 3-6, 6-3 victory.</p>\r\n<p class=\"desc_news\">The match between Myneni and Propoggia saw the first set going the Indian&rsquo;s way. Myneni broke the Australian in the 6th and 10th games after he was broken in the fifth to clinch the first set 6-2 in just 35 minutes.</p>\r\n<p class=\"desc_news\">However, the Aussie clawed back in telling fashion when he began to match the big servng Indian. Myneni broke Propoggia in the second game of the second set to gain the advantage.</p>\r\n<p class=\"desc_news\">But with the Aussie, ranked 443 in the world improving with every point, Myneni was broken in the seventh to take the games back with serves.</p>\r\n<p class=\"desc_news\">The ensuing tie-breaker turned out to be a humdinger with neither player wilting to the pressure that was building up.</p>\r\n<p class=\"desc_news\">However, the 166 ranked Myneni brought his experience to the fore, clinching the set and match with two points that stunned his rival.</p>\r\n<p class=\"desc_news\">The young Nagal was a pale shadow of himself when the contest began. His famed forehand failed to work for him and too many unforced errors saw the fifth seed breaking Nagal in the first and seventh games to race away with the first set 6-2.</p>\r\n<p class=\"desc_news\">Nagal was again broken in the first and third games as the Belgian raced away to a 4-0 lead in the second set. Nagal suddenly found his touch and form and began sending in winners.</p>\r\n<p class=\"desc_news\">The 18-year-old broke his higher ranked opponent in the 8th and 10th games before the tie-breaker was forced by the Belgian.</p>\r\n<p class=\"desc_news\">Here too, Nagal held his game together until the Belgian came up with that little extra effort to run away winner.</p>\r\n<p class=\"desc_news\">In the doubles competition, the Indo-American pair of Vijay Sundar Prashanth and Paul John Fruttero defeated Petros Chrysochos (Cyp) and Temur Ismailov (Uzb) 6-2, 6-2 to storm into the last four.</p>\r\n<p class=\"desc_news\">Joining them in the semi-final line-up was the third seeded pair of Ti Chen (Tpe) and Dane Propoggia (Aus) with a 7-6 (5), 6-4 victory against the Indian Wild card pair of Vishnu Vardhan and Sumit Nagal.</p>\r\n<p class=\"desc_news\">The second seeded Indian pair of Saketh Myneni and Sanam Singh demolished the Belarussian pair of Egor Gerasimov and Ilya Ivashka 7-6 (3); 6-1 to make it to the quarter-finals.</p>', 'KSLTA Admin', '2015-10-21', 'Bangalore', 'India', '2016-10-25', '2017-02-03', '2016-10-25 12:30:35', '2017-01-03 11:50:22', 'A', '', 'Description', 'Tournament 3rd seed Saketh Myneni faced a few challenges from Australian Dane Propoggia before prevailing 6-4, 7-6 (9) to move into the last eight of the Air Asia Open, $50,000 Challenger event at the KSLTA on Wednesday.');
INSERT INTO `ksl_news_tbl` (`news_id`, `news_title`, `news_display_tag`, `news_link_address`, `news_link_status`, `news_content_image`, `news_editor_content`, `news_posted_by`, `news_posted_at`, `news_located_city`, `news_located_country`, `news_from_date`, `news_to_date`, `news_created_date`, `news_lastupdated_date`, `news_flat`, `news_tournament_id`, `news_meta_name`, `news_meta_content`) VALUES
(55, 'Bega shocks Somdev to make quarters', 'Home', 'Bega-shocks-Somdev-to-make-quarters', 'R', 'uploadimage/Bega-shocks-Somdev-to-make-quarters24908.jpg', '<p class=\"desc_news\">Unseeded Italian Alessandro Bega put his best foot forward to oust tournament top seed Somdev DevVarman in straight sets to coast into the last eight of the AirAsia Open, $50,000 Challenger Event here on Thursday.</p>\r\n<p class=\"desc_news\">With an ATP ranking of 331, the 27-year-old Italian fought back from being a break down twice in the first set to force the tie-breaker. Broken in the third and 11th games, Bega leveled back breaking his higher ranked opponent in the eighth and 12th games.</p>\r\n<p class=\"desc_news\">In the tie-breaker, Bega raced away to a 4-0 lead only to see Somdev claw back to level at 6-all. However, the Italian showed determination and with Somdev committing unforced errors, clinched the set winning the tie-breaker 9-7.</p>\r\n<p class=\"desc_news\">The second set too saw some drama with Somedev being broken in the third game. The Indian Davis Cupper came back strongly to level games at four-all by breaking Bega in the 6th game.</p>\r\n<p class=\"desc_news\">But inconsistency crept into Somdev&rsquo;s play and the Indian committed too many unforced errors to be broken in the 11th game and the Italian served out the 12th game to claim the set and match.</p>\r\n<p class=\"desc_news\">In another match, determination and a little luck saw Indian Sanam Singh making it to the last eight with a thrilling victory over Egor Gerasimov of Belarus.</p>\r\n<p class=\"desc_news\">The Belarussian, serving for the match in the final set tie-break saw a return from the Indian hit the net chord forcing him to alter his approach to the net. That split second distraction gave Sanam Singh the chance to level the tie-break scores at 6-all and he then grabbed the opportunity to serve out for the match clinching the next two points, with Egor hitting a return wide.</p>\r\n<p class=\"desc_news\">Earlier, Sanam Singh was on song in the first set breaking the Belarussian World No 323 in the first, third and fifth games to clinch the first set 6-2.</p>\r\n<p class=\"desc_news\">But Egor was not done as he came back strongly to reverse the situation in the second set. He broke the Indian in the second, fourth and eighth games to level set scores.</p>\r\n<p class=\"desc_news\">The decider saw Sanam breaking Egor in the third and ninth games and being broken in the 6th and 10th games.</p>\r\n<p class=\"desc_news\">Tournament No 2 seed James Ward took his appointed place in the quarter-finals with a 6-3, 6-4 verdict over Ilya Ivashka of Belarus and American Daniel Nguyen ousted seventh seed Russian Alexander Kudryavtsev in two tie-break sets, 7-6 (7), 7-6 (3),</p>\r\n<p class=\"desc_news\">The second seeded pair of Saketh Myneni and Sanam Singh made short work of the Sood Brothers, Chandril and Lakshit to move into the last four of the doubles competition with a 6-2, 6-4 victory.</p>', 'KSLTA Admin', '2015-10-22', 'Bangalore', 'India', '2016-10-25', '2017-02-03', '2016-10-25 12:32:33', '2017-01-03 11:50:27', 'A', '', 'Description', 'Unseeded Italian Alessandro Bega put his best foot forward to oust tournament top seed Somdev DevVarman in straight sets to coast into the last eight of the AirAsia Open, $50,000 Challenger Event here on Thursday.'),
(56, 'Saketh lone Indian in last four', 'Home', 'Saketh-lone-Indian-in-last-four', 'R', 'uploadimage/Saketh-lone-Indian-in-last-four74979.jpg', '<p class=\"desc_news\">Saketh Myneni was the last Indian standing when he defeated No 5 seed Yannick Mertens of Belgium in straight sets 6-3, 7-6 (6) to move into the last four of the AirAsia Open, $50,000 Challenger Event at the KSLTA Courts on Friday.</p>\r\n<p class=\"desc_news\">The tournament third seed, who last week claimed a Challenger title in Vietnam, was workman like in the first set, claiming a break in the fourth game to lead 4-1. Myneni closed out the set without any fuss.</p>\r\n<p class=\"desc_news\">However, the Belgian was not one to throw in his towel and fought tooth and nail. Both players held onto serves in the first 10 games before Mertens was broken in the 11th game.</p>\r\n<p class=\"desc_news\">Even there, the World No 183 fought hard but found his higher ranked opponent a step ahead.</p>\r\n<p class=\"desc_news\">The Belgian fought back to break the World No 166 in the very next game to restore parity and force the tie-break. Unforced errors from Myneni and some lovely passing shots saw Mertens breaking Myneni at 15.</p>\r\n<p class=\"desc_news\">In the tie-break, Mertens raced away to a 4-1 lead. But his joy was short lived as the Indian re-invented himself to level at 5-all and then claimed two points from 6-all to seal the match.</p>\r\n<p class=\"desc_news\">Myneni will take on tournament top seed Adrian Menendez-Maceiras of Spain in the semi-finals. Adrian had little problem in ousting Arthyr De Greef of Belgium 6-3, 6-4.</p>\r\n<p class=\"desc_news\">The second semi-final will pit second seed James Ward of Great Britain against Daniel Nguyen of the USA. The Brit defeated Sanam Singh (Ind) 6-3, 6-2 and Nguyen accounted for No Alessandro Bega 7-5, 6-4.</p>\r\n<p class=\"desc_news\">In the doubles semi-finals, Wild Card entrants John Paul Fruttero (USA) and Vijay Sunder Prashanth ousted the third seeded pair of Ti Chen (Tpe) and Dane Propoggia (Aus) to move into the final.</p>\r\n<p class=\"desc_news\">They will take on the second seeded Indian pair of Saketh Myneni and Sanam Singh in the final to be played on Saturday evening.</p>', 'KSLTA Admin', '2015-10-23', 'Bangalore', 'India', '2016-10-25', '2017-02-03', '2016-10-25 12:34:25', '2017-01-03 11:50:32', 'A', '', 'Description', 'Saketh Myneni was the last Indian standing when he defeated No 5 seed Yannick Mertens of Belgium in straight sets 6-3, 7-6 (6) to move into the last four of the AirAsia Open, $50,000 Challenger Event at the KSLTA Courts on Frida'),
(57, 'Saketh-Sanam clinch doubles crown', 'Home', 'Saketh-Sanam-clinch-doubles-crown', 'R', 'uploadimage/Saketh-Sanam-clinch-doubles-crown88289.jpg', '<p class=\"desc_news\">The second seeded Indian pair of Saketh Myneni and Sanam Singh came from behind to claim the AirAsia Open, $50,000 Challenger doubles title with a hard fought 5-7, 6-4, 10-2 victory over Wild Card entrants John Paul Fruttero (USA) and Vijay Sundar Prashanth (India).</p>\r\n<p class=\"desc_news\">The second seeds lost the first set after being broken in the 11th game and the unseeded pair served out for the set.</p>\r\n<p class=\"desc_news\">However, Saketh and Sanam raised the level of their games and despite the opponents trading service games early, held sway over their opponents.</p>\r\n<p class=\"desc_news\">With the John and Vijay serving to stay in the set, some brilliant returns and volleys by the opposition saw the unseeded pair facing two break points.</p>\r\n<p class=\"desc_news\">They saved one but Saketh and Sanam combined to convert the second to pocket the set.</p>\r\n<p class=\"desc_news\">The Super-tie-breaker saw the second seeds racing away to a 4-1 lead which they kept stretching, allowing John and Vijay just one point more to pocket the winner&rsquo;s cheque of $ 3100.</p>\r\n<p class=\"desc_news\">John and Vijay had to be content with the finalist&rsquo;s purse of $1800.</p>\r\n<h4 class=\"desc_news\"><strong>WARD SETS UP FINAL DATE WITH MENENDEZ</strong></h4>\r\n<p class=\"desc_news\">Second seed James Ward of Great Britain rallied from a set deficit to quell the challenge of unseeded American Daniel Nguyen 4-6, 6-4, 6-3 to romp into the final of the AirAsia Open, $50,000 Challenger Singles final.</p>\r\n<p class=\"desc_news\">In the title tilt, Ward will take on top seed Spaniard Adrian Menendez-Maceiras who chalked out a work-man like 6-4, 6-4 victory over No 3 seed Saketh Myneni of India.</p>\r\n<p class=\"desc_news\">Losing the first set, the second seed, World No 159 dug into his bag of tricks to swamp the American in the next two sets to take his appointed place.</p>\r\n<p class=\"desc_news\">The contest between Adrian Menendez-Maceiras and Saketh Myneni began with each player beginning their respective service games with an Ace.</p>\r\n<p class=\"desc_news\">But the Indian looked in visible discomfort and fatigue did show on his person as he was broken in the third game of the opening set.</p>\r\n<p class=\"desc_news\">The Indian had his chance to come back into the game when he held two break points in the 10th game. But he squandered the two chances with weak returns into the net allowing the Spaniard to convert his third advantage.</p>\r\n<p class=\"desc_news\">In the second set, Myneni did show patches of brilliance, but the Spaniard came back stronger when threatened to hold serve. Much against the run of play, Saketh once again faltered and on serve in the ninth game and the Spaniard held three break points, converting the second after the Indian had pumped in an Ace.</p>\r\n<p class=\"desc_news\">The final of the men&rsquo;s singles will be played from 4 P.M. on Sunday, October 25.</p>', 'KSLTA Admin', '2015-10-24', 'Bangalore', 'India', '2016-10-25', '2017-02-03', '2016-10-25 12:46:02', '2017-01-03 11:50:38', 'A', '', 'Description', 'The second seeded Indian pair of Saketh Myneni and Sanam Singh came from behind to claim the AirAsia Open, $50,000 Challenger doubles title with a hard fought 5-7, 6-4, 10-2 victory over Wild Card entrants John Paul Fruttero (USA) and Vijay Sundar Prashanth (India). '),
(58, 'Underprivileged kids on ‘Magic Bus’ at AirAsia Open', 'Home', 'Underprivileged-kids-on-Magic Bus-at-AirAsia-Open', 'R', 'uploadimage/Magic-Buds23120.jpg', '<p class=\"desc_news\">Two under privileged children from the Capital City of Delhi had a field day and had a wish of theirs come true when they got to meet and interact with Great Britain&rsquo;s Davis Cupper James Ward and Indian Davis Cupper Somdev DevVarman at the AirAsia Open on Saturday.</p>\r\n<p class=\"desc_news\">The Children who are part of the &lsquo;Magic Bus&rsquo; (an NGO) programme of using Sports and Activity based approach to educate underprivileged children of Society, were flown into the City of Bengaluru by AirAsia, the tournament sponsors as part of their CSR initiative.</p>\r\n<p class=\"desc_news\">The two children, Prasantan, aged 12 and Badal, aged 14 along with their Youth Leader Fauzia are being hosted by the Karnataka State Lawn Tennis Association and are put up at the Tournament&rsquo;s Official Hotel, Ibis, Bengaluru.</p>\r\n<p class=\"desc_news\">The Children quizzed both James and Somdev on various topics including one for James that asked how he made friends with so much of travelling involved.</p>\r\n<p class=\"desc_news\">The Brit laughed and first said, &ldquo;I don&rsquo;t have many friends&rdquo;, before getting serious and saying, &ldquo;It is difficult, tough and takes a lot of sacrifice as tennis needs lot of travel for me to do well. There are advantages too. Family and friends get to come and watch some f your matches and performances and that gives them joy and for me too it is wonderful at times to have friends and family around. I have my fianc&eacute;e here and so I do have company&rdquo;.</p>\r\n<p class=\"desc_news\">Somdev was asked how he took to tennis considering he likes football a lot. The Indian Davis Cupper had to think hard before saying, &ldquo;Where I grew up, there were lot of tennis courts and no football fields to play. So it was easy for me&rdquo;.</p>\r\n<p class=\"desc_news\">Asked how the game of tennis has changed his life and what he sought to be, Somdev said, &ldquo;My life is tennis. I am happy with where I am. I play tennis because I enjoy it and I am having fun playing the game. I want to just keep going. I feel good and I did not play because I wanted to be NO 1 in the country&rdquo;.</p>\r\n<p class=\"desc_news\">He went on to add, &ldquo;I work hard, exercise and have a trainer who guides me. In the beginning, it was tough, I had to wake up early and put in many hours of hard work. But now it has become a routine and I enjoy the routine. It has become a habit&rdquo;.</p>\r\n<p class=\"desc_news\">The Indian World No 166 urged the children to get into a routine of staying fit and to enjoy whichever game they play before signing off.</p>\r\n<p class=\"desc_news\">The two children got to toss the coin during the second singles and semi-final and doubles final respectively.</p>', 'KSLTA Admin', '2015-10-24', 'Bangalore', 'India', '2016-10-25', '2017-02-03', '2016-10-25 12:48:31', '2017-01-03 11:50:42', 'A', '', 'Description', 'Two under privileged children from the Capital City of Delhi had a field day and had a wish of theirs come true when they got to meet and interact with Great Britain’s Davis Cupper James Ward and Indian Davis Cupper Somdev DevVarman at the AirAsia Open on Saturday. '),
(59, 'James Ward clinches AirAsia Open', 'Home', 'James-Ward-clinches-AirAsia-Open', 'R', 'uploadimage/James-Ward64184.jpg', '<p class=\"desc_news\">James Ward of Great Britain played the best tennis of the week to clinch the AirAsia Open, $50,000 Challenger title defeating tournament top seed Adrian Menendez-Madeira&rsquo;s 6-2, 7-5 at the KSLTA Courts on Sunday.</p>\r\n<p class=\"desc_news\">Watched by a goodly crowd, the English player was all over his Spanish opponent breaking the World No 137 in the 6th and 8th games to wrap the first set.</p>\r\n<p class=\"desc_news\">The tournament No 2 had a chance to gain his first break in the very second game of the first set when he held two break points but Adrian came back strongly to save the two points and hold onto serve.</p>\r\n<p class=\"desc_news\">The Spaniard raised the level of his game in the second set and kept the British Davis Cupper on court longer than expected.</p>\r\n<p class=\"desc_news\">The opponents held onto served for the firsts 11 games engaging each other in some long rallies and coming p with some exciting tennis.</p>\r\n<p class=\"desc_news\">But it was the second seed that held sway despite not gaining a break early.</p>\r\n<p class=\"desc_news\">Ward was clearly at the peak of his game and the Englishman did not disappoint coming up with some superb shots to enjoy two set points in the 12th game, the Spaniard hitting a return long to hand over the title to Ward.</p>\r\n<p class=\"desc_news\">&ldquo;I am happy and I have had a good week&rsquo;, said James ward after collecting the winner&rsquo;s cheque of $7,100.</p>\r\n<p class=\"desc_news\">&ldquo;I think I played by best tennis today and I was serving pretty well throughout the match&rdquo;, said the Englishman adding, &ldquo;The conditions were difficult to play but winning matches is always good and I am happy with the way I have gone through the week and in the final&rdquo;.</p>\r\n<p class=\"desc_news\">Incidentally, this if the first Challenger title for the World No. 159 who collected ATP points for the victory.</p>\r\n<p class=\"desc_news\">The pair head off to Pune to take part in the Challenger there.</p>', 'KSLTA Admin', '2015-10-25', 'Bangalore', 'India', '2016-10-25', '2017-02-03', '2016-10-25 12:52:15', '2017-01-03 11:49:56', 'A', '', 'Description', 'James Ward of Great Britain played the best tennis of the week to clinch the AirAsia Open, $50,000 Challenger title defeating tournament top seed Adrian Menendez-Madeira’s 6-2, 7-5 at the KSLTA Courts on Sunday.'),
(60, 'Bidar ITF Shifted To Kalaburagi', 'Home', 'Bidar-ITF-Shifted-To-Kalaburagi', 'R', 'uploadimage/bidar-itf-shifted-to-kalaburagi35737.jpg', '<p class=\"desc_news\">The Bidar $10,000 ITF women&rsquo;s event which was scheduled to be from November 23 at Bidar has now been shifted to Kalaburagi (Gulbarga) due to unavoidable circumstances. When the situation arouse where the event could not take place at Bidar, KSLTA had no other option but to consider shifting to Bangalore or Gulbarga. Cancellation was not an option as it would have put the players into lot of inconvenience and the very idea of conducting these events is to provide opportunity for the players.</p>\r\n<p class=\"desc_news\">Even shifting to Bangalore would have caused hardship to players. With no option left, KSLTA decided to request the Kalaburagi District Administration to host the event at Kalaburagi.</p>\r\n<p class=\"desc_news\">It is mainly due to the efforts of Mr Radhakrishna, Vice President, KSLTA, who held meetings with Mr Vipul Bansal IAS, Deputy Commissioner and a few other officials of Kalaburagi that the local authorities agreed to host next week&rsquo;s tournament here itself. Dr. Sharan Prakash Patil, Hon\'ble Minister for Medical Education, Government of Karnataka was extremely forthcoming &lrm;to bring the tournament to Gulbarga.</p>\r\n<p class=\"desc_news\">Once Kalaburagi agreed, KSLTA Approached AITA to request ITF for the same. ITF has approved the shift from Bidar to Gulbarga after much deliberation.</p>\r\n<p class=\"desc_news\">The move by the Kalaburagi authorities is welcomed by the players who would have to otherwise travel and find accommodation at Bidar all over again.</p>\r\n<p class=\"desc_news\">Sunder Raju, HON. Jt. Secretary of AITA Said \"AITA &amp; KS&lrm;LTA is indebted to Kalburagi District administration and KALABURAGI Tennis Association for coming forward to host the Bidar ITF event at Kalaburagi. When we were at crossroads as Bidar wasn\'t able to complete arrangements for the event, Kalaburagi has come as a saviour. It\'s surely not easy to host two weeks in a row but the excitement from Kalaburagi is indeed highly supportive. I thank Mr. Aditya Allan Biswas IAS, Regional Commissioner, Mr. Vipul Bansal IAS, Deputy Commissioner, Kalaburagi for extending this hand of support even at the last moment. I earnestly thank Dr. Sharan Prakash Patil, Hon\'ble Minister for Medical Education and Mr. Radhakrishna, Vice PRESIDENT of KSLTA who have been pillars of strength in this situation. The two weeks of International events surely makes Kalaburagi a strong destination on world tennis calendar. I also thank ITF and the players who have happily agreed to continue in Kalaburagi.\"</p>\r\n<p class=\"desc_news\">The qualifiers for the said tournament will be held on Saturday and Sunday, 21 &amp; 22 November at Chandrashekar Patil Stadium itself, main draw would be from 23 - 28 of this month.</p>', 'KSLTA Admin', '2015-11-19', 'Kulbarga', 'India', '2016-10-25', '2017-02-03', '2016-10-25 12:54:56', '2017-01-03 11:49:09', 'A', '', 'Description', 'The Bidar $10,000 ITF women’s event which was scheduled to be from November 23 at Bidar has now been shifted to Kalaburagi (Gulbarga) due to unavoidable circumstances.'),
(61, 'Riya, Eetee cause big upsets', 'Home', 'Riya-Eetee-cause-big-upsets', 'R', 'uploadimage/riya,-eetee-cause-big-upsets24249.jpg', '<p class=\"desc_news\">Ton a day when three-setters became a norm, unseeded Riya Bhatia of India turned giant killer of the tournament ousting country-mate and tournament sixth seed Sharmadaa Baluu in a tough three-setter to move into the semi-finals of the KUDA Kalaburagi ITF $10,000 Women&rsquo;s Tennis Tournament here on Thursday. Riya who accounted for fourth seed Dhruthi Tatachar on Wednesday was in her elements claiming the first set 6-3. However, the sixth seed from Bangalore rose to the occasion to claim the second set 7-5.</p>\r\n<p class=\"desc_news\">But Sharmadaa could not handle the pace and angles of Riya and succumbed in the third set to bow out of the tournament. Sharmadaa had the upper hand in the first set winning the first three games, breaking Riya in the second game. But the Delhi Lass showed grit and determination, raising the level of the game to win six straights games in a row.</p>\r\n<p class=\"desc_news\">There was a reversal of roles in the second set. Riya leading 5-3 was stopped in her tracks by Sharmadaa who clinched four games straight to level set scores at 1-all. The third set turned out to be a one-sided affair with Sharmadaa committing too many unforced errors and Riya capitalizing on it. Riya broke the Bangalore Lady in the second and eighth games to canter into the last four.</p>\r\n<p class=\"desc_news\">In another upset eighth seed Eetee Maheta (Ind) sent second seed Natasha Palha packing with a 5-7, 6-4, 7-6 (3) verdict to make the last four, that match lasting 3 hours and 26 minutes. The match was almost a replica of the earlier match. Maheta was up 5-2 in the first set before the second seed Goan packed a punch to win the next five games to claim the first set.</p>\r\n<p class=\"desc_news\">The second set saw Palha leading 4-1 before Maheta clinched the next five games to win the set. The decider saw Maheta breaking Palha in the first game to lead 3-1. Palha broke back in the sixth to make it 3-all and the players then held on to serves to force the tie-breaker. From 2-all, Maheta clinched the next five points to surge into the last four where she will take on Riya Bhatia.</p>\r\n<p class=\"desc_news\">Top seed Prerna Bhambri rallied from a set down to defeat the impressive Kanika Vaidya 4-6, 6-4, 6-2 and will take on Thailand&rsquo;s Tamachan Momkoonthod who bested No 3 seed Snehadevi S Reddy 6-4, 7-6 (3). In doubles competition, the fourth seeded pair of Dhruthi Tatachar Venugopal and Karman Kaur Thandi breezed past top seeds Sharmadaa Baluu and Natasha Palah 7-5, 6-3 to book their spot in the final.</p>\r\n<p class=\"desc_news\">They will take on No 3 seeds Prerna Bhambri and Kanika Vaidya, who defeated another Indian pair, second seeds Rana Shweta and Sneha Devi Reddy 6-2, 1-6, 13-11, in the final on Friday afternoon.</p>', 'KSLTA Admin', '2015-11-19', 'Kulbarga', 'India', '2016-10-25', '2017-02-03', '2016-10-25 13:06:42', '2017-01-03 11:49:04', 'A', '', 'Description', 'Ton a day when three-setters became a norm, unseeded Riya Bhatia of India turned giant killer of the tournament ousting country-mate and tournament sixth seed Sharmadaa Baluu in a tough three-setter to move into the semi-finals'),
(62, 'Riya Earns Right To Battle Prerna Final', 'Home', 'Riya-Earns-Right-To-Battle-Prerna-Final', 'R', 'uploadimage/riya-earns-right-to-battle-prerna-final55343.jpg', '<p class=\"desc_news\">Top seed Prerna Bhambri breezed into the final of the KUDA Kalaburagi ITF $10,000 Women&rsquo;s Tennis Tournament with straight sets 6-3, 6-2 victory over unseeded Tamachan Momkoonthod of Thailand here at the Chandrashekar Patil Stadium here on Friday. The World No. 479 spent 57 minutes on court to dispose of the World 837 in the first semi-final of the day.</p>\r\n<p class=\"desc_news\">Prerna was in no mood to play around and made her intentions clear, hitting winners after winners to demoralize her Thai opponent. Prerna broke Thamachan in the sixth and eighth games, while being broken in the seventh of the first set and then broke the Thai in the first and seventh games while successfully holding her serves.</p>\r\n<p class=\"desc_news\">Unseeded Riya Bhatia earned the right to battle Prerna for the tile when she accounted for a third seed en route to the title tilt. In the second semi-final of the day, Riya defeated eight seed Eetee Maheta in straight sets, with an identical score line of 7-5, 7-5.</p>\r\n<p class=\"desc_news\">Having seen off a wild card entrant in the first round, Riya defeated fourth seed Druthi Tatachar, sixth seed Sharmadaa Baluu and eighth seed Eetee Maheta on her way to the final. The contested oscillated. Riya took a 2-0 lead before Eetee clinched four games in a row only to see Riya clinch the next three. At 5-all, Riya held serve to go up 6-5. Eetee serving to stay in the match saved two match points from 0-40, before Riya clinched the set.</p>\r\n<p class=\"desc_news\">The second set saw Riya leading 3-1 before Eetee again ran away with four games to lead 5-3. Riya showed determination that has been her hall mark all week, rallying brilliantly to win the next four games and advance to the final.</p>\r\n<h4 class=\"desc_news\"><strong>Druthi-Karman claim doubles crown</strong></h4>\r\n<p class=\"desc_news\">In the doubles final the fourth seed Indian pair of Druthi Tatachar and Karman Kaur Thandi rallied from being a set down to best third seeds Prerna Bhambri and Kanika Vaidya 1-6, 6-3, 10-7. Prerna and Kanika raced away win the first set breezing past the fourth seeds without a fuss 6-1.</p>\r\n<p class=\"desc_news\">The fourth seeds took a break and came back a changed pair. They got into their groove and had the third seeds running all around the court. With the crowd egging them on Druthi and Karman raised the level of their game and forced the tie breaker which they clinched 10-7 with some wonderful tennis.</p>', 'KSLTA Admin', '2015-11-20', 'Kulbarga', 'India', '2016-10-25', '2017-02-03', '2016-10-25 13:10:16', '2017-01-03 11:48:13', 'A', '', 'Description', 'Top seed Prerna Bhambri breezed into the final of the KUDA Kalaburagi ITF $10,000 Women’s Tennis Tournament with straight sets 6-3, 6-2 victory over unseeded Tamachan Momkoonthod of Thailand here at the Chandrashekar Patil Stadium here on Friday.'),
(63, 'Prerna Bhambri clinches crown', 'Home', 'Prerna-Bhambri-clinches-crown', 'R', 'uploadimage/prerna-bhambri-clinches-crown30395.jpg', '<p class=\"desc_news\">Top seed Prerna Bhambri of India lived up to her top billing, coming from a set down to defeat giant killer, compatriot and un-seeded Riya Bhatia 4-6, 7-5, 6-4 to clinch the singles crown at the KUDA Kalaburagi ITF $10,000 Women&rsquo;s Tennis Tournament here on Saturday. In a battle befitting of a final, Riya and Prerna dished out top class tennis that had the near capacity crowd at the Chandrashekar Patil Stadium cheering every point.</p>\r\n<p class=\"desc_news\">The contest lasted 2 hours and 32 minutes with both players giving it their best, Prerna emerging the better player on the day. The first set saw games going with serves till 5-4 and Prerna serving to stay in the set. Leading 40-15, Prerna committed two double faults to set up deuce. She held advantage twice and each time Riya leveled. The one break point that Riya got, she converted to claim the set.</p>\r\n<p class=\"desc_news\">The second set saw games being traded. Riya claimed the first three breaking Prerna in the second game. Prerna repeated the feat winning the next three. The players then held serves till 5-5, before Prerna broke Riya and then served out to clinch the set 7-5. Prerna moved into top gear in the decider breaking Riya in the first and seventh games while being broken in the sixth.</p>\r\n<p class=\"desc_news\">Riya did have her chances at another break eighth game but the top seed dug deep into her reserves to hold serve and to the delight of the crowd served out for the match to claim the winner&rsquo;s cheque for Rs 1,19,059 and 12 ITF Points. This is the third title for Bhambri who won the Fenesta Open Nationals and the Lucknow Open.</p>\r\n<h4 class=\"desc_news\" style=\"margin-top: 0px; margin-bottom: 0px;\"><strong>Riya had to be content with 7 ITF points</strong></h4>\r\n<p class=\"desc_news\">Doubles winners Druthi Tatachar Venugopal and Karman Kaur Thandi shared $637 while runners-up Prerna Bhambri and Kanika Vaidya shared $343. Druthi and Karman earned 12 doubles points and the runners-up earned 7 points.</p>\r\n<p class=\"desc_news\">Chief Guest, Mr Qamarul Islam, , Minister Municipalities and Local Bodies, Wakf and Minority Welfare, gave away the awards to the winners and finalists in the Presence of KSLTA Sr Vice President Mr Laxminarayan, Vice President Mr Radhakrishna and Hon Jt Secretary, AITA &amp; Hon Secretay KSLTA , Mr Sunder Raju.</p>\r\n<h4 class=\"desc_news\" style=\"margin-top: 0px; margin-bottom: 0px;\"><strong>Soha Gets Wild Card For Kalaburagi Open</strong></h4>\r\n<p>Bangalore girl Soha Sadiq was granted a wild card into the main draw of the Kalaburagi Open, which starts here from Monday. Abhinikka Renganathan, Varunya Chandrashekar and US based student Kanika Vaidya were the other three Indians to be handed out wild cards. The qualifying rounds which started today saw 21 players signing in.</p>', 'KSLTA Admin', '2015-11-21', 'Kulbarga', 'India', '2016-10-25', '2017-02-03', '2016-10-25 13:14:53', '2017-01-03 11:48:04', 'A', '', 'Description', 'Top seed Prerna Bhambri of India lived up to her top billing, coming from a set down to defeat giant killer, compatriot and un-seeded Riya Bhatia 4-6, 7-5, 6-4 to clinch the singles crown at the KUDA Kalaburagi ITF $10,000 Women’s Tennis Tournament here on Saturday.'),
(64, 'Bhambri Chases Hat-Trick At Kalaburagi', 'Home', 'Bhambri-Chases-Hat-Trick-At-Kalaburagi', 'R', 'uploadimage/Mr-Sharan-Patil-272485.jpg', '<p class=\"desc_news\">For the second consecutive week, Prerna Bhambri enjoys top billing when the RGUHS Kalaburagi ITF Open, a $10,000 Women&rsquo;s tennis tournament, main draw begins here from Monday. The Rajeev Gandhi University of Health Sciences, Karnataka have come forward to sponsor the event that has Natasha Palha retaining her second seeding while Druthi Tatachar, Prarthna Thombare, Sharmadaa Baluu move up a rank in the tournament with last week&rsquo;s No 3 seed Snehadevi Reddy not entering this event.</p>\r\n<p class=\"desc_news\">Having reached the final of last week&rsquo;s tournament at the same Chandrashekar Patil Stadium, Riya Bhatia is now seeded No 8. Bhambri will be looking for her fourth title of the year and chasing a hat-trick at Kalaburagi. The Delhi player won the 2012 ITF event here and followed it up by winning last week&rsquo;s title on Saturday.</p>\r\n<p class=\"desc_news\">Karman Kaur Thandi, the 17-year-old who fell early due to fever, while expected to do well last week, will seek to make headway this week and so will the US based student Kanika Vaidya who made it to the quarters before losing to eventual winner Bhambri. Prerna Bhambri takes on China&rsquo;s Xi-Yao Wang while Natasha will clash with qualifier Sai Dedeepaya Yeddula in the first round of the tournament.</p>\r\n<p class=\"desc_news\">In disheartening news for followers of the game Karman Kaur Thandi will take on US based Kanika Vaidya in their opening round. The duo was crowd favourites at last week&rsquo;s event until they bowed out. Prerna Bhambri faces some opposition in the top half of the draw taking on either Karman or Kanika. The top 16 also has No 3 seed Druthi Tatachar, No.5 Sharmadaa Baluu and No 6 Nidhi Chilumula.</p>\r\n<p class=\"desc_news\">In comparison Natasha has a fairly easy way till the quarters where she could meet last week&rsquo;s finalist Riya Bhatia. Four seeds will be seen in action on day one. No 2, Natasha , No. four Prarthana Thombare, No 6 Nidhi Chilumula and No 8 Riya Bhatia play their first round matches on Monday. First set of matches start at 9.30 am.</p>\r\n<h4 class=\"desc_news\" style=\"margin-top: 0px; margin-bottom: 0px;\"><strong>QUALIFIERS SPOTTED</strong><span class=\"point-little\">.</span></h4>\r\n<p><span class=\"point-little\">With no foreign contestants in the qualifying rounds of the tournament, eight Indians made it to the main draw. Sahan Shetty played the shortest of matches recording a 6-0,6-1 victory to lead seven others. Sindhu Janagam, Aastha Dargude, Bhargavi Nitin Mangudkar, Simran Kejriwal, Anusha Kondaveeti, Pragati Solankar and Sai Dedeepya Yeddula entered the main draw by virtue of winning their second and final qualifying round matches.</span></p>', 'KSLTA Admin', '2015-11-22', 'Kulbarga', 'India', '2016-10-25', '2017-02-03', '2016-10-25 13:16:52', '2017-01-03 11:48:59', 'A', '', 'Description', 'For the second consecutive week, Prerna Bhambri enjoys top billing when the RGUHS Kalaburagi ITF Open, a $10,000 Women’s tennis tournament, main draw begins here from Monday.'),
(65, 'Karman Ousts Kanika', 'Home', 'Karman-Ousts-Kanika', 'R', 'uploadimage/karman-ousts-kanika97437.jpg', '<p class=\"desc_news\">Seveenteen-year-old Karman Kaur Thandi played top class tennis to oust US based Kanika Vaidya in straight sets 6-4, 6-4 to move into the last 16 of the RGUHS ITF Kalaburagi Open here on Monday. The battle between the two foreign based players drew a goodly crowd expecting a tight battle. But the World No 778, a resident of Toronto, Canada was clearly streets ahead with her game in comparison to Kanika who seemed out of sorts.</p>\r\n<p class=\"desc_news\">Karman served with power and her returns at times brooked no reply from Kanika. The tall and lanky Karman broke Kanika in the fifth game with some great determination. Trailing love-40 on Kanika&rsquo;s serve, Karman snatched five points in a row with some amazing tennis to claim the break. She held serves like a Pro to clinch the first set.</p>\r\n<p class=\"desc_news\">The second set saw Kanika breaking Karman in the second game with the Canadian resident committing too many unforced errors. Just when it seemed that the match would witness a decider, Karman stormed back into the contest breaking Kanika in the third and fifth games while ensuring she held serves to move into the next round where she awaits the winner of the contest between top seed Prerna Bhambri of India and Xi-Yao Wang of China.</p>\r\n<p class=\"desc_news\">Meanwhile four seeds, No 2, Natasha Palha, No. 4 Prarthna Thombare (Ind), No 6 seed Nidhi Chilumula (Ind) and No 8 Riya Bhatia breezed into the second round with comfortable victories over their respective opponents. Thombare ousted wild card entrant Abinikka Renganathan 7-5, 6-2 and Chilumula bested qualifier Simran Kejriwal 6-1, 6-0. Natasha puffed out Sai Dedeepya Yeddula 6-1, 6-1 and Riya, who made the final of last week&rsquo;s tournament, sent out Thailand&rsquo;s Plobrung Plipuech 7-6 (8), 6-2.</p>\r\n<h4 class=\"desc_news\" style=\"margin-top: 0px; margin-bottom: 0px;\"><strong>Results: (Prefix denotes seedings):</strong><span class=\"point-little\">.</span></h4>\r\n<p class=\"desc_news\">Singles 1st round: (All Indians unless mentioned): Karman Kaur Thandi bt Kanika Vaidya 6-4, 6-4; Anusha Kondavetti bt Preethi Ujjini 6-4, 6-4; Aastha Dargude bt He Chun-Yan (Chn) 7-5, 2-6, 6-2; (4) Prarthana G Thombare bt (WC)Abinikka Renganathan 7-5, 6-2;(6) Nidhi Chilumula bt (Q) Simran Kejriwal 6-1, 6-0; Ram Moulika bt Soha Sadiq 6-7 (8), 6-1, 6-3; (8) Riya Bhatia b Plobrung Plipuech (Tha) 7-6 (8); 6-2; Tamachan Momkoonthod (Tha) bt Pragati Solankar 6-1, 6-0.</p>\r\n<p class=\"desc_news\">Doubles: Round of 16: (3) Prerna Bhambri / Kanika Vaidya bt Anusha Kondaveeti / Sweta Nalekala 6-1, 6-1; (4) Druthi Tatachar Venugopal / Karman Kaur Thandi bt Varunya Chandrasekhar / Simran KKejriwal 6-0, 6-2; Sai Samhitha Chamarthi / He Chun-Yan (Chn) bt Sneha Padamata / Moulika Ram 6-2, 4-6, 12-10; Abinikka Renganathan / Soha Sadiq bt Dakshata Girishkumar Patel / Sai Dedeepya Yeddula 7-6 (3), 6-3.</p>', 'KSLTA Admin', '2015-11-23', 'Kulbarga', 'India', '2016-10-25', '2017-02-03', '2016-10-25 13:19:24', '2017-01-03 11:48:54', 'A', '', 'Description', 'Seveenteen-year-old Karman Kaur Thandi played top class tennis to oust US based Kanika Vaidya in straight sets 6-4, 6-4 to move into the last 16 of the RGUHS ITF Kalaburagi Open here on Monday.'),
(66, 'Shweta Rana Prevails In Long Drawn Battle', 'Home', 'Shweta-Rana-Prevails-In-Long-Drawn-Battle', 'R', 'uploadimage/Shweta-Rana-Prevails-In-Long-Drawn-Battle22256.jpg', '<p class=\"desc_news\">In a long drawn battled between two unseeded Indian players, Swetha Rana prevailed over Amrita Mukherjee 6-0, 5-7, 7-6 (4) in the first round of the RGUHS ITF Kalaburagi Open here at the Chandrashekar Patil stadium on Tuesday. The two players slugged it on court for 2 hours and 41 minutes before Rana managed to edge into the second round of the $10,000 tournament.</p>\r\n<p class=\"desc_news\">There was not much difference between the players who traded serves other than in the first set when Rana looked like she would breeze through with ease when she wrapped up the first set 6-0 her Bengali opponent in the 1, 3 and 5th games. The second set saw players breaking serves and restoring parity at 5-all before Mukherjee held to go up 6-5 and then broke Rana in the 12th game to clinch the set.</p>\r\n<p class=\"desc_news\">The decider went the way of the second set and the tie-breaker had to be enforced. Rana took a 5-2 lead before Mukherjee clinched two points. But Rana held on to her serves to win the tie-break 7-4. Meanwhile seeded players advanced without too much of a fuss baring fifth seed Sharmadaa Baluu who pulled out with injury. Top seed Prerna Bhambri breezed past Xi-Yao Wang 6-1, 6-1, seventh seed Eetee Maheta sailed past qualifier Sindhu Janagam 6-3, 6-2 and third seed Druthi Tatachar bested Varunya Chandrashekar 6-2, 6-0.</p>\r\n<p class=\"desc_news\">Karnataka&rsquo;s Sharmadaa Baluu retired from her match against Sneha Padmata after one game. The tournament fifth seed was suffering from an inflamed ankle injury and decided not to push the injury.</p>\r\n<h4 class=\"desc_news\" style=\"margin-top: 0px; margin-bottom: 0px;\"><strong>Results: (Prefix denotes seeding):</strong><span class=\"point-little\">.</span></h4>\r\n<p class=\"desc_news\">Singles 1st round: (All Indians unless mentioned): (Q) Sahan Shetty bt (Q) Bhargavi Nitin Mangudkar 6-0, 6-1; (7) Eetee Maheta bt (Q) Sindhu Janagam 6-3, 6-2; Sneha Padamata bt (5) Sharrmadaa Baluu (retired) 1-0; Shweta Rana bt Amrita Mukherjee 6-0, 5-7, 7-6 (4); Sai Samhitha Chamarthi bt Arthi Muniyan 6-1, 6-0; (1) Prerna Bhambri bt Xi-Yao Wang 6-1, 6-1; (3) Druthi Tatachar Venugopal bt Varunya Chandrashekar 6-2, 6-0.</p>\r\n<p class=\"desc_news\">Doubles: Round of 16: (2) Nidhi Chilumula / Eetee Maheta bt Sindhu Janagam / Amreen Naaz 6-0, 6-1; Riya Bhatia / Shweta Rana bt Sri Sai Shivani / Sahan Shetty 6-3, 6-0; Tamachan Momkoonthod (Tha) / Plobrung Plipuech (Tha) bt Amrita Mukhejee / Sansriti Ranjan 6-2, 6-1.</p>', 'KSLTA Admin', '2015-11-24', 'Kulbarga', 'India', '2016-10-25', '2017-02-03', '2016-10-25 13:21:05', '2017-01-03 11:48:47', 'A', '', 'Description', 'In a long drawn battled between two unseeded Indian players, Swetha Rana prevailed over Amrita Mukherjee 6-0, 5-7, 7-6 (4) in the first round of the RGUHS ITF Kalaburagi Open here at the Chandrashekar Patil stadium on Tuesday. '),
(67, 'Prerna Quells Karman Challenge', 'Home', 'Prerna-Quells-Karman-Challenge', 'R', 'uploadimage/Prerna-quells-Karman-challenge47321.jpg', '<p class=\"desc_news\">Top seed Prerna Bhambri had to dig deep into her reserves to quell the challenge of 17-year-old Karman Kaur Thandi to enter the quarter-finals of the RGUHS ITF Kalaburagi Open here on Wednesday. The world No 476 had to raise the level of her game many notches in the Rajiv Gandhi University of Health Sciences sponsored $10,000 event to stay ahead of the Toronto based world No 778 in the second round.</p>\r\n<p class=\"desc_news\">In what turned out to be an intense and high class tennis battle, Bhambri just proved a notch better than Thandi to prevail 6-3, 7-6 (5). The first set saw Karman make a few unforced errors that allowed Bhambri break her in the fourth game which was enough for her to claim the first set, holding onto her serves carefully.</p>\r\n<p class=\"desc_news\">The second set saw the rival&rsquo;s trade serves in the first two games and then began the struggle for supremacy. Aces were pounded, passing shots whizzed by and cross court winners were hit with tenacity as Prerna and Karman looked for that one break and ensuring they hold serves. With each screaming themselves into focus, the set was pushed into the tie-breaker.</p>\r\n<p class=\"desc_news\">Here too the players traded the first two points and then held serves until Bhambri found herself on match point at 6-5. The top seed grabbed the opportunity to push Karman to the left of the court with a crisp return of serve and then found the court open, hitting a forehand down the line winner to Karman&rsquo;s right to clinch the tie.</p>\r\n<p class=\"desc_news\">&ldquo;I knew her game and I knew that I had to raise my game. She hits hard and loves to attack. I had to make those adjustments and it worked for me. It was a tough game and she (Karman) is a good player&rdquo;, said Bhambri after the contest.</p>\r\n<p class=\"desc_news\">&ldquo;I had to play her game and began attacking and that worked. I am happy I won and actually relieved&rdquo;, added the top seed. Third seed Druthi Tatachar became the first casualty of this event when she went down tamely to Thailand&rsquo;s Tamachan Momkoonthod in straight sets 6-2, 6-4. Last week&rsquo;s finalist Riya Bhatia, seeded 8th bowed out with a straight sets defeat to unseeded Sai Samhitha Chamarthi 4-6, 2-6.</p>\r\n<h4 class=\"desc_news\" style=\"margin-top: 0px; margin-bottom: 0px;\"><strong>Results: (Prefix denotes seeding):</strong><span class=\"point-little\">.</span></h4>\r\n<p class=\"desc_news\">Singles R16: (All Indians unless mentioned): (1) Prerna Bhambri bt Kaman Kaur Thandi 6-3, 7-6 (5); (6) Nidhi Chilumula bt Anusha Kondaveeti 6-2, 6-0; Tamachan Momkoonthod (Tha) bt (3) Druthi Tatachar Venugopal 6-2, 6-4; (2) Natasha Palha bt Shweta Rana 6-1, 6-2; (7) Eetee Maheta bt Sahan Shetty 6-1, 7-5; Sai Samhitha Chamarthi bt (8) Riya Bhatia 6-4, 6-2; (4) Prarthana Thombare bt Moulika Ram 6-2, 6-3; Aastha Dargude bt Sneha Padamata 6-4, 7-5.</p>\r\n<p class=\"desc_news\">Doubles: Quarter-finals: (4) Dhruthi Tatachar Venugopal / Karman Kaur Thandi bt Tamachan Momkoonthod (Tha) / Plobrung Plipuech (Tha) 6-2, 6-1; (2) Nidhi Chilumula / Eetee Maheta bt Sai Samhitha Chamarthi / Chun-Yan He (Chn) 6-1, 6-2; (3) Prerna Bhambri / Kanika Vaidya bt Abinikka Renganathan / Soha Sadiq 6-2, 6-2.</p>', 'KSLTA Admin', '2015-11-25', 'Kulbarga', 'India', '2016-10-25', '2017-02-03', '2016-10-25 13:23:41', '2017-01-03 11:48:42', 'A', '', 'Description', 'Top seed Prerna Bhambri had to dig deep into her reserves to quell the challenge of 17-year-old Karman Kaur Thandi to enter the quarter-finals of the RGUHS ITF Kalaburagi Open here on Wednesday. '),
(68, 'Eetee Bests Prarthana For Last Four Slot', 'Home', 'Eetee-Bests-Prarthan-For-Last-Four-Slot', 'R', 'uploadimage/Eetee-Bests-Prarthana-For-Last-Four-Slot53724.jpg', '<p class=\"desc_news\">Perna Bhambri was tested on court for the second successive day, but the top seed showcased her class rallying each time she was down to script a 6-4, 7-5 victory over sixth seed Nidhi Chilumula to storm into the singles semi-finals of the RGUHS ITF Kalaburagi Open here on Thursday.</p>\r\n<p class=\"desc_news\">In an upset at the tournament sponsored by the Rajiv Gandhi University of Health Sciences, Karnataka, seventh seed Eetee Maheta sent fourth seed Prarthana Thombare packing with a convincing 6-1, 6-3 verdict to get into the last four. &ldquo;Everything went wrong today&rdquo;, lamented Prarthana after the loss.</p>\r\n<p class=\"desc_news\">&ldquo;If even one shot was working I would have been alright. I could not get that one shot going and it was a terrible day&rdquo;, said the Maharashtra girl who trains at Hyderabad. &ldquo;My serves was also bad and my returns were bad&rdquo;, she added.</p>\r\n<p class=\"desc_news\">The quarter-final contest between Prerna and Nidhi began with the players trading breaks in the first two games. They then fought hard to hold serves till the ninth game. Serving to stay in the match, Nidhi found Prerna&rsquo;s returns too hot handle and she also made it difficult for herself by serving two double faults to lose the first set.</p>\r\n<p class=\"desc_news\">The second set Nidhi holding a 4-2 lead after she had broken Bhambri in the third game. But the top seed showed her prowess winning the next three games to lead 5-4. Bhambri saw two set points being saved by Nidhi in the 10th game. However, in the 12th game, Bhambri enjoyed two match points, converting the first opportunity to surge ahead.</p>\r\n<p class=\"desc_news\">The match between Eetee and Prarthana turned out to be one-sided. The higher seeded managed to hold serve in the fifth game of the first set as her opponent raised the level of her game to breeze to a 6-1 win in the first set. The second set saw Prarthana attempting to make a match of it, rallying from 0-3 to level at 3-all. But her form slacked again and Eetee claimed the next three games to wrap up the match.</p>\r\n<p class=\"desc_news\">Thailand&rsquo;s Tamachan Momkoonthod made short work of qualifier Aastha Dargude breezing into the last four with a 6-2, 6-0 and second seed Natasha Palha bested unseeded compatriot Sai Samhitha Chamarthi 6-1, 6-4. Bhambri will take on Momkoonthod and Natasha will clash with Eetee in the semi-finals on Friday.</p>\r\n<h4 class=\"desc_news\" style=\"margin-top: 0px; margin-bottom: 0px;\"><strong>Druthi-Kamran vs Nidhi-Eetee in doubles final</strong><span class=\"point-little\">.</span></h4>\r\n<p class=\"desc_news\">In the doubles competition, fourth seeds Druthi and Karman outclassed unseeded Riya and Shweta 6-1, 6-3 and will take on second seeds Nidhi Chilumula and Eetee Maheta who defeated third seeds and last week&rsquo;s finalists Prerna Bhambri and Kanika Vaidya 7-6 (7), 6-3.</p>\r\n<h4 class=\"desc_news\" style=\"margin-top: 0px; margin-bottom: 0px;\"><strong>Results: (Prefix denotes seeding): (All Indians unless mentioned)</strong><span class=\"point-little\">.</span></h4>\r\n<p class=\"desc_news\">Singles (Quarter-finals): (1) Prerna Bhambri bt (6) Nidhi Chilumula 6-4, 7-5; (7) Eetee Maheta bt (4) Prarthana Thombare 6-1, 6-3; Tamachan Momkoonthod bt Aastha Dargude 6-2, 6-0; (2) Natasha Palha bt Sai Samhitha Chamarthi 6-1, 6-4.</p>\r\n<p class=\"desc_news\">Doubles (Semi-finals): (4) Druthi Tatachar Venugopal / Karman Kaur Thandi bt Riya Bhatia / Shweta Rana 6-1, 6-3; (2) Nidhi Chilumula / Eetee Maheta bt Prerna Bhambri / Kanika Vaidya 7-6 (7), 6-3.</p>', 'KSLTA Admin', '2015-11-26', 'Kulbarga', 'India', '2016-10-25', '2017-02-03', '2016-10-25 13:25:39', '2017-01-03 11:48:38', 'A', '', 'Description', 'Perna Bhambri was tested on court for the second successive day, but the top seed showcased her class rallying each time she was down to script a 6-4, 7-5 victory over sixth seed Nidhi Chilumula to storm into the singles semi-finals of the RGUHS ITF Kalaburagi Open here on Thursday.');
INSERT INTO `ksl_news_tbl` (`news_id`, `news_title`, `news_display_tag`, `news_link_address`, `news_link_status`, `news_content_image`, `news_editor_content`, `news_posted_by`, `news_posted_at`, `news_located_city`, `news_located_country`, `news_from_date`, `news_to_date`, `news_created_date`, `news_lastupdated_date`, `news_flat`, `news_tournament_id`, `news_meta_name`, `news_meta_content`) VALUES
(69, 'Karman – Druthi Claim Second Crown In Two Weeks', 'Home', 'Karman – Druthi-Claim-Second-Crown-In-Two-Weeks', 'R', 'uploadimage/Karman-Druthi-Claim-Second-Crown-In-Two-Weeks49683.jpg', '<p class=\"desc_news\">The top two seeds, Prerna Bhambri and Natasha Palha lived up to their billing setting up a title clash with convincing victories in the semi-finals of the RGUHS ITF Kalaburagi Open here on Friday. The fourth seeded pair of Karman Kaur Thandi and Druthi Tatachar Venugopal proved too good for second seeds Nidhi Chilumula and Eetee Maheta, winning their second successive double title here on Friday.</p>\r\n<p class=\"desc_news\">Last week the Karman-Druthi pair claimed the KUDA ITF title at the same stadium and with this win has added 12 more points to their kitty and shared $637 between them. Nidhi and Eetee had to be content with seven points and $343 between them. &ldquo;It has been a great two weeks for us and we are happy with the back-to back titles,&rdquo; said Karman and Druthi after collecting their cheques.</p>\r\n<p class=\"desc_news\">Second seed Natasha Palha downed an off colour Eetee Maheta 6-1, 6-3 and top seed Prerna Bhambri, chasing a hat-trick of titles at Kalaburagi, breezed past Thailand&rsquo;s Tamachan Momkoonthod 6-1, 6-2. Natasha, playing good tennis through the week, raced away to a 3-1 lead in the first set breaking her opponent in the very second game.</p>\r\n<p class=\"desc_news\">Eetee, who dished out class tennis all through her path to the last four, was all at sea against the determined second seed, managing to hold serve just once in the first set, in game four, with Natasha winning the next three to clinch the set. In superb form, Natasha broke Eetee in the very first game of the second set. But The Gujarat lass showed glimpses of coming back when she broke the second seed in the sixth game to level game scores at 3-all.</p>\r\n<p class=\"desc_news\">However, that was all Eetee could manage as the Goan second seed upped her game to win the next three games breaking Eetee in the seventh and ninth games to move into the final. The second semi-final was a lop-sided affair with Prerna Bhambri spending all of 48 minutes on court to send the Thai player packing. &ldquo;I am happy to be in my third final here and yes, I am looking forward to a hat-trick of wins here&rdquo;, said Bhambri after her semi-final victory.</p>\r\n<p class=\"desc_news\">&ldquo;I am excited and at the same time I must say, I played my best tennis of the two weeks here in this semi-final (against Eetee). So I am confident going into the final&rdquo;, added the top seed who is also chasing her fourth title of the year, having won the Nationals, the Ludhiana Open and last week&rsquo;s KUDA ITF here. The final promises to be a fitting finale to the two back-to-back ITF events with both finalists showing the class of top seeded players.</p>\r\n<p class=\"desc_news\">Inspector General of Police (IGP) of Northern Eastern Karnataka, Mr Sunil Agarwal presided and handed over the winner&rsquo;s trophies to Karman and Druthi. The singles final will be played on Saturday from 10.30 a.m.</p>\r\n<h4 class=\"desc_news\" style=\"margin-top: 0px; margin-bottom: 0px;\"><strong>Results: (Prefix denotes seeding): </strong><span class=\"point-little\">.</span></h4>\r\n<p class=\"desc_news\">Singles semi-final (All Indians unless mentioned): (2) Natasha Palha bt (7) Eetee Maheta 6-1, 6-3; (1) Perna Bhambri bt Tamachan Momkoonthod (Tha) 6-1, 6-2.</p>\r\n<p class=\"desc_news\">Doubles final: (4) Karman Kaur Thandi / Druthi Tatachar Venugopal bt (2) Nidhi Chilumula / Eetee Maheta 6-4, 6-7 (5), 10-7.</p>', 'KSLTA Admin', '2015-11-27', 'Kulbarga', 'India', '2016-10-25', '2017-02-03', '2016-10-25 13:27:59', '2017-01-03 11:48:33', 'A', '', 'Description', 'The top two seeds, Prerna Bhambri and Natasha Palha lived up to their billing setting up a title clash with convincing victories in the semi-finals of the RGUHS ITF Kalaburagi Open here on Friday.'),
(70, 'Prerna Completes Hat-Trick At Kalaburagi', 'Home', 'Prerna-Completes-Hat-Trick-At-Kalaburagi', 'R', 'uploadimage/Prerna-Bhambri98405.jpg', '<p class=\"desc_news\">Top seed and world No 476 Prerna Bhambri completed a hat-trick of wins at Kalaburagi adding the RGUHS ITF Kalaburagi Open to the KUDA ITF title she won last week and her first title here in 2012, defeating second seed Natasha Palha 6-0, 6-4 here on Saturday. The top seed was in supreme form, in the Rajiv Gandhi University of Health Sciences sponsored final, clinching the first set 6-0, breaking her opponent in the 1st, 3rd and 5th games.</p>\r\n<p class=\"desc_news\">The second set saw Palha put up a fight till the sixth game. Bhambri broke Palha in the third game but was broken in the fourth to get the games back on serve. Bhambri, as has been her style, lifted the level of her game and won three games on the trot, breaking Palha in the seventh game. Palha had three game points in her grip in the 8th game, but Bhambri fought had to hold serve.</p>\r\n<p class=\"desc_news\">The top seed then served out for the match and won in an hour and 14 minutes. Mr Sunil Agarwal, Inspector General of Police, North Eastern Karnataka region, gave away the awards. &ldquo;I am really happy with the way I played and I am thrilled of having won three-titles in a row over here&rdquo;, said Prerna after collecting her Trophy and Cheque for $1568.</p>\r\n<p class=\"desc_news\">The win also fetched Prerna 12 ITF points. &ldquo;Gulbarga is a lucky place for me&rdquo; said Prerna who has now won five ITF titles, three in a row this year and fourth in all including the National title. &ldquo;I was nervous. I did not want to end up losing&rdquo;, said Natasha after the final. &ldquo;I only thought I wanted to win the final and ended up losing badly&rdquo;, she added after collecting 7 points and a cheque for $980.</p>\r\n<h4 class=\"desc_news\" style=\"margin-top: 0px; margin-bottom: 0px;\"><strong>Results: (all Indians): (Prefix denotes seeding): </strong><span class=\"point-little\">.</span></h4>\r\n<p class=\"desc_news\">Singles final: (1) Prerna Bhambri bt (2) Natasha Palha 6-0, 6-4.</p>', 'KSLTA Admin', '2015-11-28', 'Kulbarga', 'India', '2016-10-25', '2017-02-03', '2016-10-25 13:29:32', '2017-01-03 11:48:26', 'A', '', 'Description', 'Top seed and world No 476 Prerna Bhambri completed a hat-trick of wins at Kalaburagi adding the RGUHS ITF Kalaburagi Open to the KUDA ITF title she won last week and her first title here in 2012, defeating second seed Natasha Palha 6-0, 6-4 here on Saturday.'),
(71, 'R.T Narayan Memorial Cup Begins from Feb 15th', 'Home', 'R.T-Narayan-Memorial-Cup-Begins-from-Feb-15th', 'R', 'uploadimage/1138068.jpg', '<p class=\"desc_news\" style=\"text-align: justify;\">R.T Narayana Memorial Trust along with KSLTA is going to start tennis tournament from February 15th</p>\r\n<p class=\"desc_news\" style=\"text-align: justify;\">The AITA national series R.T. Narayan Memorial Cup (Under 16) may begin here on February 15. The event, to be held in the memory o, a famous tennis patron late R.T Narayan, The tournament is going to be jointly conducted by the R.T Narayana Memorial Trust which is run by his daughter Ramya, actor and former Member of Parliament and the KSLTA.</p>\r\n<p class=\"desc_news\" style=\"text-align: justify;\">&ldquo;My father was generous and magnanimous, and served lots of individuals,&rdquo; Ramya told a press gathering on Thursday. &ldquo;I don&rsquo;t know many of them. However when they reveal these stories and recognises the help, that satisfies me and it makes me happy. I wish to continue that history. Money shouldn&rsquo;t be hindrance for any player.&rdquo;</p>\r\n<p class=\"desc_news\" style=\"text-align: justify;\">KSLTA secretary C.S. Sunder Raju said the event was a means of thanking Narayan.</p>\r\n<p class=\"desc_news\" style=\"text-align: justify;\">&ldquo;His desire for people was not to you need to be any another organization however the best. However for him we wouldn&rsquo;t have experienced that perspective.</p>\r\n<p class=\"desc_news\" style=\"text-align: justify;\">&ldquo;He believed the Under 16 degree was the most crucial for change from the junior to some pro.&rdquo;</p>\r\n<p class=\"desc_news\" style=\"text-align: justify;\">Top people within the tournament includes Dhakshineswar Suresh, ranked No. 3 in India, and Abhimanyu Vannemreddy (4) among boys and Humera Shaik (7) and Shivani Amineni (9) one of the girls. The girls&rsquo; and boys&rsquo; winners will get cash prizes of Rs. 15,000 each as well as the runners up Rs. 10,000. Throughout the event, Ms. Ramya gave away a check for Rs. 1 lakh to B.R Nikshep a promising youngster from the state.</p>', 'KSLTA Admin', '2016-02-12', 'Bangalore', 'India', '2016-10-25', '2017-02-03', '2016-10-25 13:37:08', '2017-01-03 11:51:37', 'A', '', 'Description', 'R.T Narayana Memorial Trust along with KSLTA is going to start tennis tournament from February 15th'),
(72, 'KSLTA Sponsor', 'Home', 'kslta-sponsor', 'A', 'uploadimage/kslta-sponsor73930.jpg', '<p>KSLTA is proud to have amongst our staff, the Asst. Coach, Shri.Shekar Veeraswamy, Wheelchair Tennis Player,who is India ranked No. 1 and ITF Ranked No. 224 in the country. He has been invited to take part in tournaments both in Malaysia and Bangkok during the current month, the total expenses of which would be around Rs. 1.00 lakh (Rupees one lakh only), Mr. K.P. Balaraj, a tennis enthusiast and a leading businessman from Bangalore, has donated today the said sum of Rs. l.00 lakh to Mr. Shekar Veeraswamy to participate in the said tournaments abroad.</p>', 'KSLTA Admin', '2016-10-12', 'Bangalore', 'India', '2016-10-25', '2017-02-22', '2016-10-25 13:45:25', '2017-01-22 09:15:41', 'A', '', 'Description', 'KSLTA is proud to have amongst our staff, the Asst. Coach, Shri.Shekar Veeraswamy, Wheelchair Tennis Player,who is India ranked No. 1 and ITF Ranked No. 224 in the country.'),
(73, 'Dan Bloxham to conduct clinics for Players Parents', 'Home', 'Dan Bloxham - KSLTA', 'A', 'uploadimage/143934.jpg', '<p>Dan Bloxham, the Head Coach of the All England Tennis Club will conduct a series of free clinics &nbsp;under the auspices of AITA and KSLTA &nbsp;at the KSLTA Stadium here &nbsp;during the course of ` Road &nbsp;To Wimbledon\' Under-14 National Series tournament&nbsp; The clinic for tennis parents will be held <span data-term=\"goog_1228259139\">on Monday</span>, January.23,at <span data-term=\"goog_1228259140\">11 a.m.</span></p>', 'Admin', '2017-01-22', 'Bangalore', 'India', '2017-01-22', '2017-02-24', '2017-01-22 02:48:35', '2017-01-24 16:57:54', 'A', '', 'Description', 'Dan Bloxham, the Head Coach of the All England Tennis Club will conduct a series of free clinics  under the auspices of AITA and KSLTA  at the KSLTA Stadium here  during the course of ` Road  To Wimbledon\' Under-14 National Series tournament which begins on Monday.   The clinic for tennis parents will be held on Monday, January.23,at 11 a.m., followed by clinic for players on January.24 between 5 p.m.-6 p.m. and lastly a workshop for coaches on January.25  between 10 a.m -11 a.m.'),
(74, 'Road  To Wimbledon’  in Bengaluru', 'Home', 'RTW-Bengaluru', 'A', 'uploadimage/RTWL62700.jpg', '<p>The KSLTA will host the second leg of &nbsp;HSBC ` Road To Wimbledon&rsquo;&nbsp; tennis &nbsp;tournament &nbsp;for Under-14 &nbsp;&nbsp;boys and girls at the KSLTA Stadium here from January.23 to 29. The popular&nbsp; Series&nbsp; offers&nbsp; the under-14 players&nbsp; a chance to play in the UK National Under-14 Championship scheduled to be held at Wimbledon in August.</p>\r\n<p>The AITA has sanction the &nbsp;` Road To Wimbledon&rsquo; event in &nbsp;&nbsp;four cities- Pune, (from Jan.16, 2017 ) , Bengaluru ( from <span data-term=\"goog_1228259143\">Jan.23</span> ), &nbsp;Chandigarh&nbsp;( from &nbsp;<span data-term=\"goog_1228259144\">Feb.6</span>) and Kolkata &nbsp;( from Feb.13 ).&nbsp; The series will be played on National Series format and culminate with a Masters tournament in New Delhi scheduled&nbsp; from April. 10 . Sixteen players will be selected &nbsp;for the Masters tournament based on their best three performances in the four preliminary &nbsp;tournaments . The finalists of &nbsp;both boys and girls singles in the Masters will be chosen for the &nbsp;UK National Under-14 Championship at Wimbledon</p>', 'Admin', '2016-12-22', 'Bangalore', 'India', '2017-01-22', '2017-02-24', '2017-01-22 03:50:45', '2017-01-24 08:40:33', 'A', '', 'Description', 'The KSLTA will host the second leg of  HSBC ` Road To Wimbledon’  tennis  tournament  for Under-14   boys and girls at the KSLTA Stadium here from January.23 to 29. The popular  Series  offers  the under-14 players  a chance to play in the UK National Under-14 Championship scheduled to be held at Wimbledon in August.  The AITA has sanction the  ` Road To Wimbledon’ event in   four cities- Pune, (from Jan.16, 2017 ) , Bengaluru ( from Jan.23 ),  Chandigarh ( from  Feb.6) and Kolkata  ( from Feb.13 ).  The series will be played on National Series format and culminate with a Masters tournament in New Delhi scheduled  from April. 10 . Sixteen players will be selected  for the Masters tournament based on their best three performances in the four preliminary  tournaments . The finalists of  both boys and girls singles in the Masters will be chosen for the  UK National Under-14 Championship at Wimbledon'),
(75, 'Road  To Wimbledon Boys U-14 Monday  10.30PM and 11.30PM  Matches are Postponed to 24th,Jan (TUE)', 'Home', 'RTW-KSLTA', 'R', 'uploadimage/NOTICE69850.jpg', '', 'Admin', '2017-01-23', 'Bangalore', 'India', '2017-01-23', '2017-02-25', '2017-01-23 04:28:23', '2017-01-25 08:46:23', 'A', '', 'Description', ''),
(76, 'Dan Bloxham to conduct clinics for Players', 'Home', 'Dan Bloxham - Parents', 'A', 'uploadimage/IMG_20170124_18200328768579.jpg', '<p>Dan Bloxham, the Head Coach of the All England Tennis Club will conduct a series for clinics under the auspices of AITA and KSLTA at KSLTA Stadium here during the course of \'Road to Wimbledon U-14 National Series Tennis Tournament.</p>\r\n<p>&nbsp;</p>', 'Admin', '2017-01-24', 'Bangalore', 'India', '2017-01-24', '2017-02-24', '2017-01-24 10:50:42', '2017-01-24 16:55:25', 'A', '', 'Description', 'Dan Bloxham to conduct clinics tennis parents'),
(77, 'Dan Bloxham to conduct workshop for coaches', 'Home', 'Dan Bloxham - Coaches', 'A', 'uploadimage/Coaches Clinic57005.jpg', '<p>Dan Bloxham, the Head Coach of All England Tennis Club will Conduct a series of free clinics under the auspices of AITA and KSLTA at the KSLTA Stadium here during the course of Road To Wimbledon under -14 National Series Tennis Tournament.</p>\r\n<p>Workshop for Coaches on 25th,Jan.2017 from 10.00am to 11.00am</p>', 'Admin', '2017-01-25', 'Bangalore', 'India', '2017-01-25', '2017-02-25', '2017-01-25 02:45:37', '2017-01-25 08:45:45', 'A', '', 'Description', 'Dan Bloxham to conduct workshop for coaches'),
(78, 'Media Interaction with Dan Bloxham', 'Home', 'Media-Bloxham', 'A', 'uploadimage/IMG_20170125_1216344711401.jpg', '<p><strong>The HSBC&nbsp; Road To Wimbledon&nbsp; National &nbsp;14 and Under Tournament in the UK&nbsp;</strong>is the largest junior tournament on grass in the UK. It &nbsp;Began in 2002 involving clubs, parks &amp; schools for juniors 14 &amp; Under. &nbsp; About800 clubs, parks and schools participate through to 44 county finals and 128 boys &amp; girls qualifying for the National Finals played on grass at Wimbledon.</p>\r\n<p>&nbsp;In 2014, the All Englad Lawn Tennis Club launched the&nbsp;<a href=\"http://www.wimbledon.com/en_GB/news/press_releases/2013-10-23/201310141381754360840.html\">HSBC Road to Wimbledon in India</a>,&nbsp;a new initiative in partnership with the All India Tennis Association, aimed at boosting the development of junior tennis in the country. India is the first country where Wimbledon has run an event of this kind outside the UK. &nbsp;&nbsp;After three successful years, The 2017 edition of Road To Wimbledon India &nbsp;is already underway in the country. It has &nbsp;four preliminary events, which are being played &nbsp;in AITA&rsquo;s National Series format for the juniors. The first tournament was held at Pune last week ( January.16 ) and &nbsp;KSLTA is conducting the second &nbsp;event in Bengaluru &nbsp;from &nbsp;Jan.23) and it will be followed &nbsp;by one &nbsp;in Chandigarh ( from February 6 ) and Kolkata ( Kolkata, from Feb.13 ) &nbsp;and &nbsp;finally the series &nbsp;conclude with a Masters finals in New Deli. ( April.10 to 15 ). The winner and runner-up in both the boys and girls singles &nbsp;in the Masters&nbsp; tournament will qualify for &nbsp;the &nbsp;HSBC r&nbsp; Road To Wimbledon finals &nbsp;at Wimbledon, scheduled to be held from &nbsp;August.14 to 19. .</p>\r\n<p>&nbsp;Following the success of Road to Wimbledon India, in 2016 the Road to Wimbledon China was launched in Nanjing,&nbsp;in partnership with Rolex and the Tennis Association China, to support the development of Chinese tennis.&nbsp;Following a week of events hosted at the Tennis Academy of China in Nanjing, the top two boys and girls travelled to Wimbledon to take part in the UK National Finals.</p>\r\n<p>The<strong>&nbsp;<a href=\"http://www.wimbledon.com/en_GB/about_aeltc/201205091336568307582.html\">WJTI</a>&nbsp;</strong>(Wimbledon Junior Tennis Initiative)&nbsp;&nbsp;is the trail blazing community tennis initiative run by The All England Lawn Tennis Club.&nbsp;&nbsp; It is a unique scheme which broadens the base at grassroots level through its school visits and offers players the chance to develop their skills and enjoy the game in the sessions held at the Club.&nbsp; To date over 180,000 children have enjoyed their first experience of tennis with the WJTI with 2,500 children receiving scholarships for free tennis sessions at The All England Club.&nbsp; The Initiative has attracted much praise and attention as it succeeds in all areas from playground to performance.&nbsp;</p>', 'Admin', '2017-01-25', 'Bangalore', 'India', '2017-01-25', '2017-02-25', '2017-01-25 09:22:29', '2017-01-25 15:22:40', 'A', '', 'Description', 'Media Interaction with Dan Bloxham'),
(79, 'Road  To Wimbledon U-14  Girls Doubles Winner & Runner', 'Home', 'Winner-Runner', 'A', 'uploadimage/Girls D W & R720638789.jpg', '<p><strong>Winner :&nbsp;&nbsp;&nbsp; Ms.Srujana Rayarala (Telangana) &nbsp; /&nbsp; Ms. Mushrath Shaik ( AP )&nbsp;&nbsp; -&nbsp;&nbsp;&nbsp; 6/1 , 6/1<br /></strong></p>\r\n<p><strong>Runner-up : Ms. Renee Singh (Rajasthan)&nbsp;&nbsp;&nbsp; /&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ms. Renne Singla ( Hariyana)</strong>&nbsp;</p>', 'Admin', '2017-01-27', 'Bangalore', 'India', '2017-01-28', '2017-03-11', '2017-01-28 02:57:19', '2017-02-11 07:39:18', 'A', '', 'Description', 'Road  To Wimbledon U-14  Girls Doubles Winner & Runner'),
(80, 'Road  To Wimbledon U-14  Boys Doubles Winner & Runner', 'Home', 'Boys W-Runner', 'A', 'uploadimage/Boys D W & R 27.01.201735267.jpg', '<p><strong>Winner : Mr.Tangri Dhruv&nbsp;&nbsp; /&nbsp; Mr.Hooda Krishnan&nbsp;&nbsp; 6/3 , 7/6(5)<br /></strong></p>\r\n<p><strong>Runner-up: Mr. Sushant Dabas&nbsp; /&nbsp;&nbsp;&nbsp;&nbsp; Mr. Denim Yadav <br /></strong></p>', 'Admin', '2017-01-27', 'Bangalore', 'India', '2017-01-28', '2017-03-11', '2017-01-28 03:01:15', '2017-02-11 07:27:54', 'A', '', 'Description', 'Road  To Wimbledon U-14  Boys Doubles Winner & Runner'),
(81, 'Road  To Wimbledon U-14  Girls Singles  Winner & Runner', 'Home', 'S Winner-Runner', 'A', 'uploadimage/Girls S W&R 28.01.201795542.jpg', '<p><strong>Winner&nbsp; : Ms. Sandeepti Singh Rao (Hariyana)&nbsp;&nbsp;&nbsp; 6/3 , 6/1</strong></p>\r\n<p><strong>Runner-up : Ms. Sarah Dev (Punjab)</strong></p>\r\n<p>&nbsp;</p>', 'Admin', '2017-01-28', 'Bangalore', 'India', '2017-01-28', '2017-03-11', '2017-01-28 03:06:07', '2017-02-11 07:33:34', 'A', '', 'Description', 'Road  To Wimbledon U-14  Girls Singles Winner & Runner'),
(82, 'Road  To Wimbledon U-14  Boys Singles Winner & Runner', 'Home', 'Boys S W-Runner', 'A', 'uploadimage/Boys S W&R2400146354.jpg', '<p><strong>&nbsp;Ajay Mailk &nbsp; Beat&nbsp; Hooda Krishnan 6/3,7/6(6)</strong></p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', 'Admin', '2017-01-28', 'Bangalore', 'India', '2017-01-28', '2017-03-11', '2017-01-28 03:14:07', '2017-02-11 07:36:03', 'A', '', 'Description', 'Road  To Wimbledon U-14  Boys Singles Winner & Runner'),
(83, 'Karnataka State Olympic Games-2017', 'Home', 'KSO-G', 'A', 'uploadimage/Home8428519479.jpg', '', 'Admin', '2017-02-01', 'Bangalore', 'India', '2017-02-03', '2017-03-11', '2017-02-03 05:22:59', '2017-02-11 07:42:10', 'A', '', 'Description', 'Karnataka State Olympic Games-2017'),
(84, 'MENS 50K TOURNAMENT FORMAT', 'Home', 'MENS-FORMAT', 'R', 'uploadimage/TFT56429.jpg', '', 'Admin', '2017-02-03', 'Bangalore', 'Indian', '2017-02-03', '2017-03-08', '2017-02-03 09:22:52', '2017-02-08 07:51:22', 'A', '', 'Description', 'MENS 50K TOURNAMENT FORMAT'),
(85, 'AITA MEN\'S 50 K TENNIS TOURNAMENT ', 'Home', 'aita-men', 'A', 'uploadimage/172434.jpg', '<p><strong>SINGLES FINALS RESULT&nbsp; ( 10.02.2017 )</strong></p>\r\n<p><strong>JAY SONI&nbsp;&nbsp;&nbsp; BEAT S PRITHVI&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp; 7-5, 7-6(2)</strong></p>\r\n<p><strong>DOUBLES FINALS RESULT ( 10.02.2017 )</strong></p>\r\n<p><strong>VIVEK GAUTHAM&nbsp; /&nbsp; BIBASWAN&nbsp; &nbsp;&nbsp;&nbsp; BEAT&nbsp;&nbsp;&nbsp; ALOK ARADHYA /&nbsp; NAESAR JEWOOR&nbsp; -&nbsp; 6-3, 3-6,10-4<br /></strong></p>\r\n<p>&nbsp;</p>', 'Admin', '2017-02-10', 'Bangalore', 'India', '2017-02-10', '2017-03-10', '2017-02-10 03:05:17', '2017-02-10 09:08:58', 'A', '', 'Description', 'AITA MEN\'S 50 K TENNIS TOURNAMENT '),
(86, 'Bengaluru to Play Davis Cup Tie India vs Uzbekistan ', 'Home', 'Davic cup', 'A', 'uploadimage/DAVIS CUP-1794754.jpg', '<p style=\"margin: 0px; text-align: justify; font-size: 12px; font-family: cambria;\">&nbsp;The IT &amp; Start up Capital of India, Bengaluru will host the Davis Cup tie between India and Uzbekistan from&nbsp;<span class=\"m_2896912168386981099m_-295314064368487235gmail-m_8649445103249184895m_46350904425486456gmail-aBn\"><span class=\"aBn\" tabindex=\"0\" data-term=\"goog_156377965\"><span class=\"aQJ\">7-9 April 2017</span></span></span>. This would be the 2<sup>nd</sup>&nbsp;round of the Asia Oceania zone. India defeated New Zealand in the first round at Pune early this month.</p>\r\n<p style=\"margin: 0px; font-size: 12px; font-family: helvetica; min-height: 14px;\">&nbsp;</p>\r\n<p style=\"margin: 0px; text-align: justify; font-size: 12px; font-family: cambria;\">Bengaluru last hosted the Davis Cup World Group play off tie against Serbia during 2014. The Garden City has been the players&rsquo; choice too for this tie and AITA agreed to this while forwarding the proposal to ITF which was accepted.&nbsp;</p>\r\n<p style=\"margin: 0px; text-align: justify; font-size: 12px; font-family: cambria; min-height: 14px;\">&nbsp;</p>\r\n<p style=\"margin: 0px; text-align: justify; font-size: 12px; font-family: cambria;\"><strong><em>Mr. Pramod Madhwaraj,&nbsp;</em>Hon&rsquo;ble Minister for Youth Empowerment and Sports, Government of Karnataka</strong><span style=\"font-family: helvetica;\"><strong><em>,</em></strong></span>&nbsp;said, &ldquo;We are glad that Bengaluru has been chosen as the venue for this prestigious tie and Davis Cup is returning to Bengaluru after two years. Our Chief Minister, Mr. Siddaramaiah readily accepted to partner this event and we would like to welcome all players to Karnataka. Along with KSLTA, we will ensure the teams will have a wonderful experience. &nbsp;</p>\r\n<p style=\"margin: 0px; text-align: justify; font-size: 12px; font-family: cambria; min-height: 14px;\">&nbsp;</p>\r\n<p style=\"margin: 0px; text-align: justify; font-size: 12px; font-family: cambria;\"><strong>Senior Vice President of KSLTA and Chairman of Organising Committee<em>, Mr. R. Ashoka, Former Deputy Chief Minister of Karnataka</em></strong>&nbsp;welcomed this nomination and said, &ldquo;KSLTA has always been in the forefront of promiting the sport of Tennis and has been playing host to some amazing International Tennis events. We are thankful to the Hon&rsquo;ble Chief Minister of Karnataka for having extended his support to make this possible. We are confident of making this a successful event and this should be a treat to the sports lover of Bangalore.&rdquo;</p>\r\n<p style=\"margin: 0px; text-align: justify; font-size: 12px; font-family: cambria; min-height: 14px;\">&nbsp;</p>\r\n<p style=\"margin: 0px; text-align: justify; font-size: 12px; font-family: cambria;\">The KSLTA Tennis Stadium has played host to Davis Cup ties, WTA Tour events and many other International tournaments.&nbsp;</p>\r\n<p style=\"margin: 0px; text-align: justify; font-size: 12px; font-family: cambria; min-height: 14px;\">&nbsp;</p>\r\n<p style=\"margin: 0px; text-align: justify; font-size: 12px; font-family: cambria;\"><strong>Hon&rsquo;ble Minister for IT, BT and Tourism, Government of Karnataka<em>&nbsp;Mr. Priyank Kharge</em></strong>&nbsp;said, &ldquo;This adds another feather to the Cap of Bengaluru which was judged as the most Dynamic City. Karnataka has been a leading tourist detsination in the country and with our current impetus of making it a complete destination, this Davis Cup tie, should go a long way in promoting Karnataka&rsquo;s sports tourism.&rdquo;&nbsp;</p>\r\n<p style=\"margin: 0px; text-align: justify; font-size: 12px; font-family: cambria; min-height: 14px;\">&nbsp;</p>\r\n<p style=\"margin: 0px; text-align: justify; font-size: 12px; font-family: cambria;\">Speaking on this decision, new&nbsp;<strong>Non Playing Captain of the Indian Davis Cup team,<em>&nbsp;Mahesh Bhupathi&nbsp;</em></strong>who will be making his debut in his hometown of Bengaluru stated, &ldquo;With the current roster of available players for this Davis Cup tie, I&rsquo;m confident that the surface at KSLTA will work well for the team. The conditions in Bangalore will be complementary to their big serves, putting them at an advantage as they take on Uzbekistan.&rdquo;</p>\r\n<p style=\"margin: 0px; text-align: justify; font-size: 12px; font-family: cambria; min-height: 14px;\">&nbsp;</p>\r\n<p style=\"margin: 0px; font-size: 12px; font-family: cambria;\"><strong><em>Mr Hironmoy Chatterjee</em></strong>, Hon.&nbsp; Secretary General, All India Tennis Association (AITA) said, &ldquo;We are pleased to confirm Bengaluru as the venue for the Davis Cup tie between India and Uzbekistan. We are sure that Bengaluru will embrace this event and witness some high quaity Tennis.&rdquo;</p>\r\n<p style=\"margin: 0px; font-size: 12px; font-family: helvetica; min-height: 14px;\">&nbsp;</p>', 'Admin', '2017-02-17', 'Bangalore', 'India', '2017-02-17', '2017-03-17', '2017-02-17 00:36:56', '2017-02-17 07:12:48', 'A', '', 'Description', 'BENGALURU TO PLAY DAVIS CUP TIE'),
(88, 'Karnataka Girls Create history national international Tennis Tourney', 'Home', 'school-games', 'A', 'uploadimage/tennis_achiever_mysore81689.jpg', '<p>Mysuru: For the first time in the history of Karnataka, the state team has entered the finals in the U-17 girls&rsquo; national level inter school tennis tournament organised by the Department of Public Instruction in association with School Games Federation of India at Mysuru Tennis Club.</p>\r\n<p>&nbsp;On Thursday, during semi-finals, the Karnataka girls displayed class, temperament and discipline on the court to defeat compatriot Gujarat state by 2-0. In the first singles, top seed S B Apoorva came out with a brilliant all round game to beat compatriot Bhakthi Parvan by 6-3. Both the players displayed a good game of tennis but Apoorva with her outstanding backhand skills routed Bhakthi. Later in the second singles, Amar Vidula, backed with good service defeated Divya Bharad by 6-3.&nbsp;&nbsp;Karnataka will play against Chhattisgarh in the finals which will be held&nbsp;<span data-term=\"goog_1688672866\">on Saturday</span>. In the other semi-finals, Chhattisgarh defeated Maharashtra by 2-1.</p>\r\n<p>Apoorva, sharing her happiness with newskarnataka said, &ldquo;Before coming to this competition we had put in lot of hard work. Really we are delighted to enter the finals and we assure people of Karnataka that we will go all out in our efforts to win the title for our state.&rdquo;&nbsp;&nbsp;Amar Vidula said, &ldquo;As we are in tenth standard, this was our last chance to perform in the inter school competition. We had come here with a firm decision to win this tournament. I am glad that our hard work has yielded good result and everything is going on as per our plan.&rdquo;</p>\r\n<p>M K Mamatha, proud coach of the team said, &ldquo;The girls came out with a great performance. We have talented tennis players but we need more facilities for them. If the government starts providing more facilities definitely in days to come, we can consistently do well in the competitions.&rdquo; M G Biradar, General Manager of Karnataka team and Mariyappa, Mysuru District Physical Education officer expressed joy over the state&rsquo;s performance and achievement.</p>\r\n<p>&nbsp;</p>', 'Admin', '2017-01-19', 'Bangalore', 'India', '2017-02-27', '2017-03-27', '2017-02-27 07:29:29', '2017-02-27 13:29:38', 'A', '', 'Description', 'Karnataka Girls Create history national international Tennis Tourney'),
(89, 'ITF PRESS RELEASE', 'Home', 'ITF-Press', 'R', 'uploadimage/itf33480.jpg', '<p><strong>KSLTA &ndash; is organizing INDIA F5 Futures ITF Men&rsquo;s Circuit, 15000 $ prize Money tennis Tournament, from 13<sup>th</sup> to 18<sup>th</sup> March 2017at Karnataka State Lawn Tennis Association.. The qualifying rounds will be played on 11<sup>th</sup> &amp; 12<sup>th</sup> March &ndash; 2017.</strong></p>\r\n<p><strong>KSLTA - is very proud to announce that some of the top players of the country like Prajnesh Gunneswaran[342] , Balaji Sriram[363] and Vishnu Vardhan[439]&nbsp; who are also Davis Cup Players are participating in this tournament and other prominent player like Vijay Sunder Prashanth[557] ,Ranjeet Virali Murugesan[876]&nbsp; are also in the tournament. </strong></p>\r\n<p><strong>Besides this we have overseas players, Hongkit Wong [906] from Honkong, Sami Reinwein[574] &amp; Pirmin Haenle[942] both from Germany and Jui chen Hung[919] from Taipei participating in this tournament.</strong></p>\r\n<p><strong>On behalf of KSLTA, it is requested to publish the same in</strong> <strong>the leading News papers. It is also requested to publish this event under city beat &amp; oblige.</strong>&nbsp;&nbsp;</p>\r\n<p><strong>P.R.Ramaswamy</strong></p>\r\n<p><strong>Hon.Jt.Secretary</strong></p>\r\n<p><strong>* Note: - The ranking in the brackets against the players is ITF ranking</strong></p>', 'Admin', '2017-03-11', 'Bangalore', 'India', '2017-03-11', '2017-05-20', '2017-03-11 04:45:52', '2017-04-20 05:00:27', 'A', '', 'Description', 'ITF PRESS RELEASE'),
(90, 'Karnataka\'s Vasisht ousted ITF Bengaluru', 'Home', 'Deccan-herald', 'A', 'uploadimage/Karun uday53588.jpg', '<p><strong>Karnataka&rsquo;s Vasisht Cheruku went down to another youngster Karunuday Singh 6-3, 3-6, 4-6 in the men&rsquo;s singles opening round of the ITF Futures tennis tournament at the KSLTA Stadium here on Monday.</strong><br /><br />The contest saw some sparkling tennis from both the players. Vasisht began brightly with his sharp volleys and strong forehand returns to seize the initiative in the first set. Breaking Karunuday in the second game, he took a 3-0 lead and maintained the momentum to stretch it to 5-2 before wrapping the set at 6-3. <br /><br />Karunuday, however, came back strongly in the second set as he battled Vasisht to five deuces in the sixth game, before finally breaking him to grab a 4-2 lead and then take the set 6-3.<br /><br />The hard hitting Karunuday then stepped up the tempo and broke Vasisht in the third game and just hung on to that vital break to win the set and tie 6-4.<br /><br />Meanwhile, fourth seed Mukund Sai Kumar and fifth seed N Vijay Sundar Prashanth had fairly comfortable wins.<br /><br />Prashanth defeated the qualifier Jatin Dahiya 6-4, 7-5 while Mukund made short work of Jeson Patrombon of Phllippines 6-3, 6-1. The promising Mumbai youngster Aryan Goveas went down fighting to Hong Kit Wong of Hong Kong 6-4, 4-6, 2-6 and Mohit Mayur, after dropping the first set, rallied to beat Kaliyanda Poonacha 3-6, 6-4, 6-3.<br /><br />Results (first round, Indians unless mentioned): Men&rsquo;s singles: Mohit Mayur bt Kaliyanda Poonacha 3-6, 6-4, 6-4; Karunuday Singh bt Vasisht Cheruku 3-6, 6-3, 6-4; Siddarth Rawat bt A Shanmugham 6-3, 7-5; N Vijay Sundar Prashanth bt Jatin Dahiya 6-4, 7-5; Rishab Agrawal bt Aditya Hari Sasongko (Ina) 6-0, 6-3; Mukund Sai Kumar bt Jeson Patrombon (Phi) 6-3, 6-1; Hong Kit Wong (HK) bt Aryan Goveas 4-6, 6-4, 6-2.</p>', 'Admin', '2017-03-14', 'Bangalore', 'India', '2017-03-14', '2017-04-14', '2017-03-14 00:02:53', '2017-03-14 05:04:44', 'A', '', 'Description', 'Karnataka\'s Vasisht ousted ITF Bengaluru'),
(91, 'Prajnesh Gunneswaran, Sriram Balaji toil ahead', 'Home', 'P-S', 'A', 'uploadimage/Prajna Gunnes56735.jpg', '<p>Davis Cup player Prajnesh ousted German Pirmin Haenle 6-3, 7-6 (1) in an effervescent morning tie on centre court.<br /><br /><strong>Bengaluru:</strong> The top two seeds Prajnesh Gunneswaran and N Sriram Balaji had their work cut out against lively oppositions, but pulled through to the last-16 after the first round of men&rsquo;s singles ties at the $15,000&nbsp; F5 ITF Futures tennis tournament hosted by the KSLTA Stadium on Tuesday.<br /><br />Davis Cup player Prajnesh ousted German Pirmin Haenle 6-3, 7-6 (1) in an effervescent morning tie on centre court. The southpaw ran amok in the first set, leading 4-0 before being pegged back slightly. He closed out the set with a blistering forehand to put the ball back in the German&rsquo;s court, and Haenle responded.<br />&nbsp;<br />The players then held serve and traded strokes until the tie-breaker, where 342-ranked Prajnesh took full advantage to once again beat the man he had dismantled earlier in the month at ITF Chattisgarh.<br /><br />Second seed Balaji had a tougher outing against Tamil Nadu compatriot V.M Ranjeet but justified his seeding to win 7-6 (2), 7-5. Balaji broke Ranjeet in the fourth game of the first set and raced to a 5-2 lead. The 876-ranked Ranjeet showed tremendous grit to haul himself back, using slices, drop shots and forehand passes aplenty.<br /><br />A tie-break ensued and Balaji used his booming serve to clinch the first set. The second set transpired in a similir vein, but with Balaji taking his game to his opponent and playing shorter points; a tactic which worked a treat.<br /><br />On Court 3, third seed Vishnu Vardhan packed off Jayesh Pungliya 6-4, 6-1 to ensure no upsets on Day 2.<br /><br />Results: Singles: Siddarth Vishwakarma bt Jui Chen Hung 6-3, 6-2;&nbsp; G. Prajnesh bt Pirmin Haenle 6-3, 7-6 (1); Vishnu Vardhan bt Jayesh Pungliya 6-4, 6-1; Hadin Bawa bt Dalbir Singh 6-3, 6-3; Sami Reinwein bt Vijayant Malik 6-1, 6-1; Suraj Prabodh bt Kunal Anand 6-2, 6-4; Dakshineshwar Suresh bt Sivadeep Kosaraju 6-3, 6-0; N. Sriram Balaji bt V.M Ranjeet 7-6 (2), 7-5; Paramveer Singh Bajwa bt Tejas Chaukular 6-4, 2-6, 7-5.</p>', 'Admin', '2017-03-15', 'Bangalore', 'India', '2017-03-15', '2017-04-18', '2017-03-15 00:33:37', '2017-03-18 13:08:29', 'A', '', 'Description', 'Prajnesh Gunneswaran, Sriram Balaji toil ahead'),
(92, 'ITF Futures tennis tournament: Prajnesh, Balaji in contrasting wins', 'Home', 'p-b', 'A', 'uploadimage/itf111113935.jpg', '<p><strong>Bengaluru:</strong> The top seeds continued their dominance on Wednesday as they breezed into the quarterfinals at the F5 ITF Futures tennis tournament at the KSLTA courts.<br /><br />Top seeded Prajnesh Gunneswaran made light work of Hong Kit Wong 6-2, 6-1 while second seed Sriram Balaji had to overcome a late fight-back from Siddarth Vishwakarma during his 6-2, 7-5 win. Vishnu Vardhan also registered a comfortable 6-2, 6-1 over Dakshineshwar Suresh.<br />&nbsp;<br />Prajnesh broke his opponent from Hong Kong on the third game and wrapped up the first set 6-2 with minimum hassle. In the second, he broke in the first and fifth game, saved two break points in the sixth to wrap up the set, and match, 6-1.<br />Meanwhile, Balaji breezed through the first set 6-2 but found Vishwakarma in a more stubborn mood in the second. Vishwakarma broke the second seed to go 5-3 up but Balaji broke right back and held his serve to put the pressure back on the challenger.<br /><br />Serving to stay in the match, Vishwakarma double faulted when on deuce and netted the following shot to surrender the match.<br /><br />Earlier, Vishnu found no resistance from Suresh as the third seed powered his way in the last-eight. Among other seeds, Vijay Sundar Prashanth, seeded five, defeated Mohit Mayur 6-2, 6-3 while Sami Reinwein from Germany went the distance before prevailing against Paramveer Singh Bajwa 6-3, 3-6, 6-1.<br /><br />Results (second round): Singles: Vijay Sundar Prashanth (5) bt Mohit Mayur 6-2, 6-3;&nbsp; G Prajnesh (1) bt Hong Kit Wong (HK) 6-2, 6-1;&nbsp; Vishnu Vardhan (3) bt Dakshineshwar Suresh 6-2, 6-1; Sami Reinwein (Ger 6) bt Paramveer Singh Bajwa 6-3, 3-6, 6-1; Hadin Vava bt Rishab Agarwal 6-4, 6-4;&nbsp; Siddarth Rawat (7) bt Suraj Prabodh 7-6 (4), 7-6 (3);&nbsp; Karunaday Singh bt Mukund Sasi Kumar 7-6 (4), 6-3;&nbsp; Sriram Balaji (2) bt Siddarth Vishwakarma 6-2, 7-5.</p>', 'Admin', '2017-03-16', 'Bangalore', 'India', '2017-03-16', '2017-04-18', '2017-03-16 00:25:06', '2017-03-18 13:00:18', 'A', '', 'Description', 'ITF Futures tennis tournament: Prajnesh, Balaji in contrasting wins'),
(93, 'Davis Cup Press Conference', 'Home', 'D-Press', 'A', 'uploadimage/Davis Cup Press Confere53281.jpg', '<p style=\"text-align: justify;\"><strong>Davis Cup Fiesta returns to Bangalore : </strong>Government of Karnataka extends support to the event<br /><br />Leander Paes and Rohan Bopanna likely to combine under the Non playing captaincy of Mahesh Bhupathi with Yuki Bhambri &amp; Ramkumar Ramanathan taking over singles responsibilities.<br /><br /><strong><em>Will Leander break the World record for most Davis Cup Doubles victories?</em></strong><br /><br />Bangalore: It is indeed another feather in the Karnataka State Lawn Tennis Association&rsquo;s Cap to be hosting another Davis Cup fixture within two years, 3rd edition in the last 4 years.<br /><br />Tennis buffs of the Garden City can look forward to a pulse pounding finish when Bengaluru hosts the India Vs Uzbekistan tie from April 7 to 9, 2017.<br /><br />Having conducted a plethora of events starting from the Juniors level to a Tier II WTA event to the Davis Cup is no bed of roses and KSLTA has been in the forefront of promoting the game.<br /><br />The last Davis Cup, hosted by us, the Davis Cup World Group play-off tie between India and a top notch team, Serbia, sans the famous Novak Djokovic, went to the wire and this time around with the teams evenly matched; another humdinger is on the cards.<br /><br />The Govt of Karnataka has extended a huge support to the event by supporting KSLTA to host this mega event.<br /><br />Speaking on the occasion, the Chairman of the Organizing Committee and the Minister for Youth Empowerment &amp; Sports, GOK, Mr. Pramod Madhwaraj said, &ldquo;Government if Karnataka is proud to support the prestigious Davis Cup Tie between India &amp; Uzbekistan and privileged to host this tie in Bengaluru. Government of Karnataka has been a huge supporter of sports and is working towards making Bengaluru, the sports capital of the Country. I take this opportunity to welcome the visiting team Uzbekistan and the Indian teams to Bengaluru and people of the city should be treated to World class tennis. We shall work closely with KSLTA to ensure this event is organized in the best possible manner and also showcase the culture and tradition of the state to all.&rdquo;<br /><br />The home team consists of players, the likes of local lad Rohan Bopanna, the evergreen Leander Paes, Yuki Bhambri, Ramkumar Ramnathan under the new Non playing Captain Mahesh Bhupathi who shall be making his debut in this tie.<br /><br />The visitors have in their ranks Denis Istomin, a player who sent Djokovic (World No 2 then) packing in the second round of the Australian Open 2017. Others who can rally around him are Sanjar Fayziev, Temur Ismailov and Farrukh Dustov.<br /><br />Sponsors, led by Karnataka Tourism are coming forward to be a partner for this tie and that includes Radisson Blu which would be the official Tie Hotel, Advaith Hyndai which would be the transport partner, Bookmyshow.com which would be the ticketing partner. More sponsors are likely to be finalized in the coming week.<br /><br />Speaking on the arrangements, Principal Secretary to GOK &amp; Vice President of KSLTA, Mr. M. Laxminarayan IAS, said, &ldquo;KSLTA has a&nbsp; rich experience in hosting mega events and this too shall be held in a grand manner. The courts at KSLTA are being relaid and KSLTA is working closely with Bangalore Police, Fire Service, Horticulture, Sports Department and Tourism department for the success of the event. KSLTA is extremely thankful to Hon&rsquo;ble Chief Minister of Karnataka Mr. Siddaramaiah for supporting the event with a financial grant. My gratitude to also Mr. Pramod Madhwaraj and Mr. Priyank Kharge, Minister for IT &amp; Tourism, who have taken up the responsibility of spearheading the Davis Cup Organisation.&rdquo;<br /><br />Mr. Anupam Agarwal IPS, Director, Youth Empowerment &amp; Sports, GOK said &ldquo;We are very proud that Bengaluru has been chosen to host this prestigious event. This shall go a long way in promoting Sports Tourism and establish the IT Capital as the Sports capital of the Country as the climate and the metropolitan nature of the city supports this.&rdquo;<br /><br />The tie will be played under floodlights, with timings being 3 pm, 6 pm and 3 pm on Fri, 7 April, Sat, 8 April and Sunday, 9 April respectively. The neutral officials for the tie appointed by ITF are:<br /><br /><strong>Referee:</strong> Wayne McKewen (AUS)<br /><br /><strong>Chair Umpire:</strong> Nacho Forcadell (ESP)<br /><br /><strong>Chair Umpire:</strong> Adel Borghei (IRI)<br /><br />The sale of tickets for this, expected to be a high-tension tie will be available on Bookmyshow.com and during the last week will also be available at booths at the KSLTA, Cubbon Park, Bengaluru. The teams are expected to arrive from Saturday, 1 April onwards. Sunil Yajaman, Former National Coach is the Chief Organiser of the event on behalf of AITA &amp; KSLTA.</p>', 'Admin', '2017-03-17', 'Bangalore', 'India', '2017-03-17', '2017-04-18', '2017-03-17 07:20:58', '2017-03-18 13:01:57', 'A', '', 'Description', 'Davis Cup Press Conference'),
(94, 'ITF Doubles winner and Runner ', 'Home', 'ITF-Doubles', 'A', 'uploadimage/ITF Doubles Winner & Runner58186.jpg', '<p>ITF&nbsp; Doubles Finals Result</p>\r\n<p><strong>Chandril and Lakshit</strong> sood Beat&nbsp; <strong>Sriram N Balaji and Vishnu Vardhan</strong>&nbsp; 2-6, 64-4 (10-6)</p>', 'Admin', '2017-03-17', 'Bangalore', 'India', '2017-03-18', '2017-04-18', '2017-03-18 06:52:50', '2017-03-18 13:02:41', 'A', '', 'Description', 'ITF Doubles winner and Runner '),
(95, 'ITF Singles winner and Runner', 'Home', 'ITF - Singles', 'A', 'uploadimage/ITF Singles Winner & Runner71610.jpg', '<p>ITF Singles Final Result</p>\r\n<p><strong>Sriram N Balaji</strong>&nbsp; Beat&nbsp;&nbsp; <strong>Prajnesh Gunneswaran</strong>&nbsp; 2-6, 6-3, 6-4</p>', 'Admin', '2017-03-18', 'Bangalore', 'India', '2017-03-18', '2017-04-18', '2017-03-18 06:57:13', '2017-03-18 13:03:20', 'A', '', 'Description', 'ITF Singles winner and Runner'),
(96, 'Davis Cup: India go 2-0 up against Uzbekistan', 'Home', 'Davis', 'A', 'uploadimage/Singles Winner52642.jpg', '<p>Ramkumar Ramanathan and Prajnesh Gunneswaran handed India a 2-0 lead over Uzbekistan as they staved off a spirited challenge in their respective singles matches in the Davis Cup Asia/Oceania Group I tie here today.<br /><br />In the first match, 267th ranked Ramanathan overcame Temur Isamilov 6-2 5-7 6-2 7-5, while debutant Gunneswaran, ranked 187, outwitted Sanjar Fayzieb 7-5, 3-6, 6-3, 6-4.<br /><br />Ismailov is ranked 406, while Fayzieb was played in place of Denis Istomin, who had pulled out of the second round tie due to a foot injury. To his credit, Ismailov made life difficult for the Indian despite suffering cramps in his right hamstring. <br /><br />Ramkumar dominated the proceedings before a bout of double faults handed Ismailov a small opening, which the Uzbek grabbed with both hands to make a match of it.<br /><br />The Indian sealed the opening set in a jiffy and was leading 4-3 in the second when he saw himself down 0-40. He saved two breakpoints but netted a backhand on the third to allow Ismailov stay alive.<br /><br />The Uzbek played freely while Ramkumar put himself under pressure to finish the job quickly. The nerves got to Ramkumar and, in utter desperation, he squandered four set points and was eventually broken in the 12th game.<br /><br />The Uzbek had snatched the momentum but suffered cramps in his right leg in the third set, making the job of the Indian easy. Ramkumar ran away with the third set as Ismailov\'s restricted movements gave him easy points.<br /><br />Ismailov, though, recovered remarkably after a medical timeout and was battle ready for the fourth set. Ramkumar was always catching up with the Uzbek, holding his serves without fuss, and soon it was 5-5.<br /><br />Two unforced errors by Ismailov gave Ramkumar three break chances. He converted the third when Ismailov buried a return to the net. Ramkumar\'s unforced errors continued as twice he sent down double faults on match points. He though sealed the match with a forehand volley winner.<br /><br />In the second singles of the day, Guneshwaran won the first, third and fourth set.In the second set, Gunneswaran trailed Fayziev 1-4 and the Uzbek levelled the match 1-1.<br /><br />Fayziev led the third set 3-1 as Gunneswaran committed some silly errors, which made his task even more difficult. But, to his credit, Gunneswaran refused to get bogged down and raced past the Uzbek to take a 4-3 lead.<br /><br />Gunneswaran won the third set, and led the tie 2-1, before he overwhelmed Sanjar 6-4 in the fourth set and sealed the match in his favour. Ramkumar later said encouraging words from Leander Paes and Rohan Bopanna helped him in getting India off to a winning start.<br /><br />\"Before the match I spoke to Leander and he told me to play for the country and Davis Cup is a great adventure, so give your best,\" Ramkumar said. Bopanna too, \"likewise\", offered him an advice. The winner of the tie will qualify for the World Group play-offs, to be played in September.</p>\r\n<div style=\"border-bottom: 1px solid; color: #b5b5b5; text-align: left;\">&nbsp;</div>', 'Admin', '2017-04-07', 'Bengaluru', 'India', '2017-04-19', '2017-05-19', '2017-04-19 12:53:26', '2017-04-19 18:12:36', 'A', '', 'Description', 'Davis Cup: India go 2-0 up against Uzbekistan');
INSERT INTO `ksl_news_tbl` (`news_id`, `news_title`, `news_display_tag`, `news_link_address`, `news_link_status`, `news_content_image`, `news_editor_content`, `news_posted_by`, `news_posted_at`, `news_located_city`, `news_located_country`, `news_from_date`, `news_to_date`, `news_created_date`, `news_lastupdated_date`, `news_flat`, `news_tournament_id`, `news_meta_name`, `news_meta_content`) VALUES
(97, 'Davis Cup: Bopanna and Balaji seal India\'s place in World Group play-offs', 'Home', 'Day-2', 'A', 'uploadimage/Davis cup Doubles58964.jpg', '<p>BENGALURU: India on Saturday sealed their place in the <a class=\"key_underline\" href=\"http://timesofindia.indiatimes.com/topic/Davis-Cup\">Davis Cup</a> World Group Play-offs with <a class=\"key_underline\" href=\"http://timesofindia.indiatimes.com/topic/Rohan-Bopanna\">Rohan Bopanna</a> and N <a class=\"key_underline\" href=\"http://timesofindia.indiatimes.com/topic/Sriram-Balaji\">Sriram Balaji</a> routing their rivals with clinical precision for an unassailable 3-0 lead against Uzbekistan, here. <br /> <br /> Bopanna and debutant Balaji defeated Farrukh Dustov and Sanjar Fayziev 6-2, 6-4, 6-1 in the doubles rubber of the <a class=\"key_underline\" href=\"http://timesofindia.indiatimes.com/topic/%28Asia/Oceania-Group%29\">Asia/Oceania Group</a> I second round contest at the KSLTA stadium. <br /> <br /> Uzbekistan needed to win the doubles rubber to keep the tie alive but the Indians proved too good for the visitors with Balaji making an impressive debut. <br /> <br /> <a class=\"key_underline\" href=\"http://timesofindia.indiatimes.com/topic/Ramkumar-Ramanathan\">Ramkumar Ramanathan</a> and debutant <a class=\"key_underline\" href=\"http://timesofindia.indiatimes.com/topic/Prajnesh-Gunneswaran\">Prajnesh Gunneswaran</a> had handed India a comfortable 2-0 lead yesterday by winning the their singles against <a class=\"key_underline\" href=\"http://timesofindia.indiatimes.com/topic/Temur-Ismailov\">Temur Ismailov</a> and Fayziev respectively. <br /> <br /> Sunday\'s reverse singles now will have no bearing on the outcome of the tie and will be the best of three sets. <br /> <br /> India have now qualified for the World Group Play-offs for the fourth straight year. They lost to top teams -- Serbia (2014 in Bangalore), Czech Republic (2015 in New Delhi) and Spain (2016 in New Delhi) in previous three years in the Play-offs. <br /> <br /> The Play-offs will be held in September and India\'s next opponents will be known later when draw will be made in London. <br /> <br /> It was a tremendous start for <a class=\"key_underline\" href=\"http://timesofindia.indiatimes.com/topic/Mahesh-Bhupathi\">Mahesh Bhupathi</a> as captain of the team. Bhupathi had chosen Bopanna over veteran Leander Paes, who is one win away from making the world record for most doubles victories in Davis Cup history. <br /> <br /> Balaji not only served well he showed skills at the net and gradually started to return well. Bopanna, whose weapon is his booming serve, carried the teammate nicely and his single-handed returns proved too hot to handle for the Uzbeks. <br /> <br /> The Indian duo decimated the Uzbekistan pair which was reflected in 16 aces they sent down against the rivals\' just one. They also grabbed points on their first serves at the rate of 92 per cent compared to the rivals\' 62. <br /> <br /> The Indian duo also hardly bungled when they missed an opportunity on the pressured second serves. They served at the rate of 94 per cent as against Uzbekistan\'s 50 per cent. <br /> <br /> Both Bopanna and Balaji served extremely well and it was key to their stupendous success. The ball zipped off the court and with Bopanna and Balaji being tall customers, they were able to extract bounce. <br /> <br /> They targeted Fayziev throughout the match and the strategy paid off as they broke the Uzbek twice in the opening set and early in the third to put the foot on gas. <br /> <br /> Dustov fought hard but he too felt the heat and was broken in the 10th game of the second set, which was the best in the match. It was on serve till the ninth game when Dustov came out to serve to keep the set alive. <br /> <br /> A foot fault at 15-30 and the following double fault put him down 15-40, facing two breakpoints. With Bopanna making superb returns, Dustov struggled to handle a ferocious shot from him and netted it, handing India a double set lead. <br /> <br /> Uzbeks failed to post any fight in the third set with Bopanna and Balaji racing to a 4-0 lead with double break.</p>', 'Admin', '2017-04-08', 'Bengaluru', 'India', '2017-04-19', '2017-05-19', '2017-04-19 13:02:58', '2017-04-19 18:44:12', 'A', '', 'Description', 'Davis Cup: Bopanna and Balaji seal India\'s place in World Group play-offs'),
(98, 'Davis Cup: Ramkumar wins as India beat Uzbekistan 4-1', 'Home', 'D-3', 'A', 'uploadimage/ramkumar ra59686.jpg', '<p>BENGALURU: A clean-sweep could not come through but India stamped their authority over Uzbekistan by winning the <a class=\"key_underline\" href=\"http://timesofindia.indiatimes.com/topic/Davis-Cup\">Davis Cup</a> tie 4-1 to advance to the World Group Play-off on Sunday. <br /> <br /> India\'s place in the World Group Play-offs, to be held in September, was secured once a 3-0 lead was grabbed on Saturday in the Asia/Oceania Group I second round tie. The team was eyeing a whitewash but fell short in the second reverse singles\' match. <br /> <br /> <a class=\"key_underline\" href=\"http://timesofindia.indiatimes.com/topic/Ramkumar-Ramnathan\">Ramkumar Ramnathan</a> continued India\'s domination of the home tie by outclassing <a class=\"key_underline\" href=\"http://timesofindia.indiatimes.com/topic/Sanjar-Fayziev\">Sanjar Fayziev</a> 6-3, 6-2 in just 67 minutes in the first reverse singles at the KSLTA stadium. <br /> <br /> Left-handed <a class=\"key_underline\" href=\"http://timesofindia.indiatimes.com/topic/Prajnesh\">Prajnesh</a> Gunneswaran, though, had a hard battle at hand against the big-serving 406th ranked Ismailov. Ismailov negotiated the conditions much better than Fayziev and emerged a deserving 7-5, 6-3 winner in the second reverse singles. <br /> <br /> Both the players served big but debutant Prajnesh buckled under pressure in crucial moments and that made a difference to the outcome of the match. <br /> <br /> Ismailov\'s victory meant that India could not force a whitewash. The last time India enjoyed a clean-sweep was in February 2014 when they hammered Chinese Taipei in Indore. <br /> <br /> However, it was a brilliant start for new captain <a class=\"key_underline\" href=\"http://timesofindia.indiatimes.com/topic/Mahesh-Bhupathi\">Mahesh Bhupathi</a>, who has introduced a few rules for the players and has his own style of carrying out business. <br /> <br /> Fayziev struggled to adjust to the bounce and speed of the court while Ramkumar turned it into his advantage. He consistently sent down bouncy serves, enticing errors from his opponent. <br /> <br /> In his first service game, Fayziev was down 0-40 and was broken when he smashed an overhead volley long and wide on the second breakpoint. <br /> <br /> Ramkumar quickly ran away with a 3-0 lead with an easy hold and could have been 4-0 up when Fayziev committed three consecutive double faults but the Uzbek managed to hold. <br /> <br /> A backhand passing winner handed Ramkumar a breakpoint in the sixth game but the Uzbek served well to save that. <br /> <br /> The Indian also approached the net better in the opening set, easily putting away Fayziev\'s feeble returns from the baseline. He served out the set in the ninth game. <br /> <br /> After squandering two break chances in the opening game of the second set, Ramkumar earned a third with a brilliant lob and converted when Fayziev fluffed a volley. <br /> <br /> Fayziev served better in the following games and returned well but Ramkumar still managed to get another break in the seventh game to open up a 5-2 lead and served out the match in the next game. <br /> <br /> \"I played clever today. The tie was sealed so I focused on a few things. I did not want to commit a lot of double faults, I wanted to return better. I played smart and all the effort paid off,\" Ramkumar said after his match. <br /> <br /> \"I will take this confidence into the upcoming tournaments (on the Tour),\" said the 22-year-old. <br /> <br /> In the final match of the tie, both Prajnesh and Ismailov served extremely well and it was on serve before Prajnesh made three unusual forehand errors that put him down by three breakpoints in the 12th game.</p>', 'Admin', '2017-04-09', 'Bengaluru', 'India', '2017-04-19', '2017-05-19', '2017-04-19 13:10:16', '2017-04-20 04:38:04', 'A', '', 'Description', 'Davis Cup: Ramkumar wins as India beat Uzbekistan 4-1'),
(99, 'CRS TRUST CHAMPIOSHIP SERIES U-14', 'Home', 'crs-trust', 'R', 'uploadimage/CRS46614.jpg', '<p>AITA Championship series&nbsp; Boys &amp; Girls U-14yrs</p>\r\n<p>Boys U-14&nbsp; :&nbsp;&nbsp;&nbsp; Sridhar Deepak&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Beat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ninaad Ravi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 6-2 , 6-3</p>\r\n<p>Girls U-14 :&nbsp;&nbsp;&nbsp;&nbsp; Lakshanya Vushwanath&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Beat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kushi Santhosh&nbsp;&nbsp;&nbsp;&nbsp; 6-3 , 6-4</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', 'Admin', '2017-05-17', 'Bangalore', 'India', '2017-05-17', '2017-10-20', '2017-05-17 02:27:35', '2017-09-20 10:50:14', 'A', '', 'Description', 'CRS TRUST CHAMPIOSHIP SERIES U-14'),
(100, 'French Open 2017: Rohan Bopanna-Gabriela Dabrowski Beat Robert Farah-Lena Gronefeld To Claim Mixed Doubles Title', 'Home', 'Rohan French Open', 'A', 'uploadimage/Rohan Bopanna24005.jpg', '<h6 style=\"text-align: justify;\"><span style=\"font-size: 12.0pt;\">On behalf of&nbsp; Office Bearers, Council member&nbsp; and Members of KSLTA , congratulate you on your tremendous victory in the Mixed Doubles at French open title. &nbsp;Wishing you&nbsp;all the best.. The Indo-Canadian pair of Rohan Bopanna and Gabriela Dabrowski beat the Columbian-German pair of Robert Farah and Anna-Lena Gronefeld 2-6, 6-2, 12-10 to win the mixed doubles French Open title.</span></h6>\r\n<h6 style=\"text-align: justify;\"><span style=\"font-size: 12.0pt;\">&nbsp;</span></h6>\r\n<p><span style=\"font-size: 12.0pt;\"><span style=\"font-family: Arial; font-size: small;\">&nbsp;</span></span></p>', 'Admin', '2017-06-09', 'Bangalore', 'India', '2017-06-10', '2017-08-27', '2017-06-10 06:43:50', '2017-07-27 05:59:40', 'A', '', 'Description', 'French Open 2017: Rohan Bopanna-Gabriela Dabrowski Beat Robert Farah-Lena Gronefeld To Claim Mixed Doubles Title'),
(101, 'S B Apoorva Felicitated.', 'Home', 'Apoorva', 'A', 'uploadimage/P.R.Ramaswamy. Apoorva  S B1104.jpg', '<p>The Karnataka State Lawn Tennis Association presented state junior tennis player , S B Apoorva with a Cheque of Rs.25,000/- for her Solid Performance in the National School Games in Mysuru. Apoorva won a silver Medal in the Singles and a Gold medal in the team event in the games Conducted by the School Games Federation of India. P R Ramaswamy, Joint Secretary of the KSLTA presented Apoorva with the Cheque.</p>', 'Admin', '2017-06-20', 'Bangalore', 'India', '2017-06-20', '2017-07-20', '2017-06-20 02:57:37', '2017-06-20 11:12:47', 'A', '', 'Description', 'Apoorva '),
(102, 'Aayush Bhat Double delight', 'Home', 'A-B', 'A', 'uploadimage/Aayush Bhat73475.jpg', '<p>It was a double delight for Karnataka youngster Aayush Bhat who bagged the boys Singles and doubles titles of the National under-12 tennis tournament in mumbai on saturday, Aaysuh defeated fifth seed arunava Majumdar of West Bengal 6-1, 7-6(7) in the singles final. He had combined with Aryan Shah of Gujarat to clinch the doubles crown on Friday.</p>', 'Admin', '2017-05-21', 'Bangalore', 'India', '2017-06-24', '2017-08-27', '2017-06-24 06:15:34', '2017-07-27 08:33:41', 'A', '', 'Description', 'Aayush Bhat Double delight'),
(103, 'AITA Super Series National Tournament  : Mustafa - Satdeep Duo Win Boy\'s  Doubles', 'Home', 'Mustafa-Satdeep', 'A', 'uploadimage/Mustafa-Satdeep84720.jpg', '<p>Mustafa M Raja and Satdeep Nair&nbsp; From Karnataka Won the AITA Super Series U-12 Title in Guwati .</p>\r\n<p>The Duo , in the Finals , defeated Second Seeds Harsh Fogat and Omansh Choudary of Delhi a tight three set encounter 6-2 , 4-6 , 10-8 which lasted for more than two hours.</p>', 'Admin', '2017-07-21', 'Bangalore', 'India', '2017-08-14', '2017-09-14', '2017-08-14 04:06:58', '2017-08-14 09:07:27', 'A', '', 'Description', 'Mustafa-Satdeep'),
(104, 'Aayush Bhat won the  Boys-U14 AITA Super Series Held in Guwahati ', 'Home', 'Aayush Bhat', 'A', 'uploadimage/Aayush Bhat46186.jpg', '<p>Aayush won the Boys U14 AITA Super Series held in Guwahati in the week of July 17th, 2017. In the finals he beat the top seed Kritanta Sarma 7-6(4), 6-1.</p>', 'Admin', '2017-07-21', 'Bangalore', 'India', '2017-08-14', '2017-09-14', '2017-08-14 04:14:41', '2017-08-14 09:15:22', 'A', '', 'Description', 'Aayush Bhat'),
(105, 'BNS Reddy IPS, Won singles Gold Medal at USA', 'Home', 'BNS', 'A', 'uploadimage/BNS2477.jpg', '<p>Shri. B N S Reddy, IPS (Karnataka), IGP &amp; Director KSRTC &amp; Treasurer KSLTA, Won Gold In Singles In The World Police &amp; Fire Games Held at Los Angels (USA) on 13th Aug.2017 Defeating Mr. Bruce Barrios of USA ( 7-6, 6-4, 7-5 ), Beside he has won two consecutive Gold Medals for India in the Last Years Two Police Games held at Fairfax &amp; Virginia (USA).</p>', 'Admin', '2017-08-14', 'Bangalore', 'India', '2017-08-26', '2017-12-20', '2017-08-26 02:40:54', '2017-11-20 16:07:20', 'A', '', 'Description', 'BNS Reddy IPS, Won singles Gold Medal at USA'),
(106, 'S B Apoorva wins Bronze at Indonesia', 'Home', 'w-b', 'A', 'uploadimage/SCHOOL TENNIS12001.jpg', '<p>S B Apoorva, Karnataka&nbsp; won the bronze medal in Girls team event at the Asia School (U-18) Tennis Chamionship in sol, Indonesia in the 3rd Place play-off, India beat Singapore 2-1,</p>', 'Admin', '2017-08-31', 'Bangalore', 'India', '2017-09-05', '2017-10-05', '2017-09-05 05:30:13', '2017-09-05 10:30:21', 'A', '', 'Description', 'S B Apoorva wins Bronze'),
(107, 'AITA Championship Series U-12 Winner', 'Home', 'C-S', 'A', 'uploadimage/Winner162194.jpg', '<p>KSLTA Conducting AITA Championship Series Boys &amp; Girls U-12 From 04.09.2017 to 08.09.2017., BOYS Singles SKANDHA PRASANNA RAO (S-1) BT SATDEEP SATESSH NAIR (S-4):- 6-1, 7-5 GIRLS Singles VAIBHAVI SAXENA (S-1) BT SOHA SINGH (S-2):- 6-3, 6-3</p>', 'Admin', '2017-09-08', 'Bangalore', 'India', '2017-09-08', '2017-12-20', '2017-09-08 04:23:24', '2017-11-20 16:06:23', 'A', '', 'Description', 'AITA Championship Series U-12 Winner'),
(108, 'CIRCULAR - GOK Directorate of Youth Empowerment & Sports', 'Home', 'C-GOK', 'R', 'uploadimage/DYSS72378.jpg', '<p>Procedure for applying online for Sports Benefits :</p>\r\n<p>Step 1 : Log on to <a href=\"http://www.dyes.kar.nic.in\">www.dyes.kar.nic.in</a> on URL Page.</p>\r\n<p>Step 2 : Select Apply Online Tab</p>\r\n<p>Step 3 : Select any scheme for applying for sports benefit it will ask \' Have you registered as a sports person\'</p>\r\n<p>Step 4 : Select \'Yes\' if registered online in Sportspersons Registration\' module of the portal.&nbsp; Selct \'No\' if not registered online in the portal.</p>\r\n<p>Step 5 : if registered, Enter the sportsperson ID and Proceed.</p>\r\n<p>Step 6 : If not registered, Click on \'Click here to register online for applying\' You will be directed to \'Sportsperson registration\' Page. Enter all&nbsp;&nbsp;&nbsp; the details, upload the documents and sumit. A unique Sportsperson ID is generated with details. Take a printput of this and use this ID for applying for all sports benefits in future.</p>\r\n<p>Step 7 : With this Sportsperson ID : repeat the above steps 3, 4 &amp; 5</p>\r\n<p>Step 8 : Fill all the details and upload the documents and submit.</p>\r\n<p><span style=\"text-decoration: underline;\"><strong>Uses of Sportsperson ID :</strong> </span></p>\r\n<ul>\r\n<li>The ID is unique to the Sportsperson</li>\r\n<li>This ID only, will be used for all future sports benefit napplications.</li>\r\n<li>This ID will be used to track the benefits given to sportspersons and also to track their progress.</li>\r\n<li>This will avoid submission of basic documents repetedly</li>\r\n<li>This will avoid applications / documents being lost or misplaced</li>\r\n<li>This will bring in lot of transparency in the process.</li>\r\n</ul>\r\n<p>&nbsp;</p>', 'Admin', '2017-09-08', 'Bangalore', 'India', '2017-09-11', '2017-10-20', '2017-09-11 07:31:42', '2017-09-20 10:49:59', 'A', '', 'Description', 'CIRCULAR - GOK Directorate of Youth Empowerment & Sports'),
(109, 'KSLTA Felicitates  Young Champs', 'Home', 'KSLTA - Fali', 'A', 'uploadimage/C S U-12 FELEI61323.jpg', '<p>The young winners and runners-up of the KSLTA-AITA Under-12 Championship Series held on 4th to 8th Sep.2017, were felicitated by the KSLTA at the KSLTA Stadium here on Friday. In a welcome gesture, the players were presented with scholarships for kits by Mr. P.R. Ramaswamy, join secretary of the KSLTA. The champions Skanda Prasanna ( boys ) and Vaibhavi Saxena ( girls ) received Rs .25000 each while the runners-up Satdeep Sateesh Nair ( boys ) ad Soha Singh ( girls ) received Rs.15,000 each `` We are committed to encourage young talent and `` As a token of appreciation of their performances, we re presenting them with the scholarship which they could utilise to upgrade their kits&rsquo;&rsquo; ,, said Mr. Ramaswamy. Mr. K.C. Nagaraj, council member of the KSLTA and also the director of the tournament, commended the players.</p>', 'Admin', '2017-09-22', 'Bangalore', 'India', '2017-09-22', '2017-12-20', '2017-09-22 03:48:56', '2017-11-21 05:16:29', 'A', '', 'Description', 'AITA CS-12'),
(110, 'Skandha Prasanna won U14 & U12 Championship Series', 'Home', 'S-P', 'A', 'uploadimage/Skanda Prasanna44942.jpg', '<p>Skandha Prasanna from Karnataka won U14 &amp; U12 Championship Series(CS7) singles &amp; U14 runner up titles held at Coimbatore (25 Sep-29 Sep).</p>', 'Admin', '2017-10-03', 'Bangalore', 'India', '2017-10-03', '2017-12-20', '2017-10-03 09:11:12', '2017-11-20 16:05:43', 'A', '', 'Description', 'Skandha Prasanna won U14 & U12 Championship Series'),
(111, 'Bengaluru Open-2017 ATP Challenger', 'Home', 'B-o', 'A', 'uploadimage/B O72590.jpg', '<p>Bengaluru, Nov 9: It is celebration time for the tennis enthusiasts of Bengaluru as the Garden City will host the Bengaluru Open, an event on the ATP Challenger Tour at the Karnataka State Lawn Tennis Association here from Nov 20-25. The USD 100,000 prize money Bengaluru Open is being sponsored by Karnataka Tourism and is the richest prize money Challenger event in Southern Asia. The event envisaged and driven by Karnataka Tourism returns to Bengaluru after a gap of two years and has become bigger and better with the prize money being doubled in this edition. The nomenclature of the event comes on the heels of Karnataka Tourism&rsquo;s aim to consolidate Bengaluru as the most dynamic city using sports tourism and enhance brand Bengaluru which has become the hub of IT and Start Up capital of India. Speaking to the newsmen while launching the countdown for the Bengaluru Open, Minister for Fisheries and Youth Services and Sports, Pramodh Madhwaraj said that the government intends to promote sports at the highest level. &ldquo;The Government of Karnataka is happy to provide support to the Bengaluru Open which is one of the biggest event on the ATP circuit in the Asian region. Through our support, we intend to not only promote but help in growing the sport,&rdquo; said the Minister who was the Chief Guest. &ldquo;We have in the past, rotated this event amongst different districts in Karnataka like Dharwad, Belgaum, Kalburgi and Chikmagalur and will endeavor to promote the sport in tier-2 and tier-3 cities of Karnataka,&rdquo; he added while applauding the efforts of KSLTA in the promotion of the sport. Minister for Tourism, IT, BT, Science &amp; Technology Priyank Kharge who was the Guest of Honour said that such events were a vehicle to showcase our culture and tourism. &ldquo;We have adopted a unique approach to promote tourism and have taken the initiative to use sports as a platform which will not only put us on the global map but establish as a preferred tourist destination,&rdquo; said the Minister. &ldquo;We believe that sports go a long way in boosting tourism and events such as Bengaluru Open should be a long term commitment. I urge the community of Bengaluru to come together to help this event grow. On behalf of Karanataka Tourism, I assure a support to host this event for Five years,&rdquo; he added. On the competition front, there would be a draw of 32 with 22 players earning a direct entry while there would be four wildcards, four qualifiers (draw of 32) and two from special exempted category. The top seed for the event is Slovakian Blaz Kavcic who is ranked under top-100 in the world followed by Radu Albot from Moldova. As many as three Indians &ndash; Yuki Bhambri who is the top-ranked amongst the hosts, Prajnesh Gunneswaran, and Ramkumar Ramanathan all a part of the Davis Cup team have made it to the main draw. The local challenge comes in the form of Mysore lads Suraj R Prabodh, a runner-up in National Championship and Prajwal Dev who will have to battle it out to make it to the qualifiers&rsquo; category. Sumit Nagal, Saket Myneni and Sriram Balaji are the other Indians to watch for. Doubles would be a keenly contested affair as Divij Sharan who has breached his career best of top-50 and Olympian Vishnu Vardhan would be the key attraction. Bengaluru will be the final stop of the 5-legged ATP sojourn in Asia with Vietnam hosting two of them, China and Pune (India) hosting one each. The Bengaluru carries points that are allotted for a US $ 125,000 prize money event as apart from the prize money, the hospitality is also being taken care of by the organisers. Former National coach and Development Officer AITA, Sunil Yajaman would be the Tournament Director while Frenchman Stephane Cretois would be the ATP Supervisor. Speaking on the occasion M Lakshminarayan, Addl. Chief Secretary for Govt. of Karnataka and Hon. Vice President, KSLTA said: &ldquo;We are happy to host the ATP Challenger in Bengaluru. We have left no stone unturned to ensure a smooth conduct of the event. We hope to host many such events in the future,&rdquo; he added. CS Sundar Raju, Hon. Vice President AITA and Hon. Secretary KSLTA who was also present on the occasion assured that the players will be well looked after. &ldquo;We will give our best to ensure that the players take back happy memories,&rdquo; he said. The KSLTA is not averse to conducting big ticket events having hosted many National and International events including ATP World doubles championship, Davis Cup and the WTA where the famed William sisters &ndash; Serena and Venus have graced these courts. With a world class facility set amidst a serene backdrop and some quality tennis, a tennis buff cannot ask for more.</p>', 'Admin', '2017-11-08', 'Bangalore', 'India', '2017-11-14', '2017-12-20', '2017-11-14 00:58:16', '2017-11-20 16:01:57', 'A', '', 'Description', 'Bengaluru Open-2017 ATP Challenger'),
(112, 'Gojo sends second seed home at Bengaluru Open 2017', 'Home', 'G-', 'A', 'uploadimage/Gojo89321.jpg', '<p>Bengaluru, Nov 21: Borna Gojo took a two-year break from tennis and went to study. Having played a<br />few ITF Futures event, Gojo is currently ranked 682 and a runner-up finish at an ITF Futures event in<br />Malaysia two seasons ago has been his career best thus far. However, at the Karnataka State Lawn<br />Tennis Association, the Croatian who entered the main draw as a qualifier, sent the second seed Adrian<br />Menendez-Maceiras of Spain home in the opening round of the US $ 100,000 Bengaluru Open 2017, an<br />event on the Challenger Tour being played at the KSLTA Stadium here on Tuesday. The 19-year- old was a<br />6-3, 1-6, 7-5 winner over the fancied player.<br />Top seed Blaz Kavcic of Slovania was stretched by Bosnian Tomislav Brkic before prevailing 6-2, 6-7, 7-6<br />in a match that lasted two hours and 32 minutes. India&rsquo;s Suraj Prabodh who had been given a wildcard<br />went down fighting to fourth seed Elias Ymer 4-6, 6-7.<br />In a major setback for Indian fans, leading contender Saketh Myneni had to withdraw owing to a nagging<br />right shoulder injury. He is been replaced by lucky loser Antoine Escoffier. &ldquo;I had this injury and I tried<br />for a couple of days during practice but I didn&rsquo;t want to take a risk,&rdquo; said Myneni.<br />Mysore lad Suraj who is been a promising youngster admitted that he was intimidated by his opponents<br />ranking and the players he had felled, before the start of the game. However, with two breaks, he went<br />4-2 up in the opening set and lost the momentum after that to go down 4-6, thanks to a number of<br />unforced errors. In the second set, he did make a late comeback and took the set into a tie-breaker,<br />from where he went down tamely.<br />Meanwhile in a minor upset in the doubles, the Kazakhistan pair of Timur Kabibulin and Alexandr<br />Nedovyesov defeated the fourth seeds Scott Clayton and Jonny O&rsquo;Mara of Great Britain 6-4, 4-6, 10-7.<br />The top seeded pair of Mikhail Elgin of Russia and Divij Sharan of India downed the challenge of<br />Frenchman Geoffrey Blancaneaux and Jay Clarke of Great Britain 5-7, 6-4, 10-5.<br />Results<br />Round-1<br />Singles<br />1-Blaz Kavcic (SLO) bt Tomislav Brkic (BIH) 6-2, 6-7 (0), 7-6 (6); Q-Borna Gojo (CRO) bt 2-Adrian<br />Menendez-Maceiras (ESP) 6-3, 1-6, 7-5; Suraj Prabodh (IND) lost to 4-Elias Ymer (SWE) 4-6, 6-7 (2);<br />Kaichi Uchida (JPN) lost to Ante Pavcic 3-6, 4-6; Aleksandr Nedovyesov (KAZ) bt Matej Sabanov (CRO) 6-<br />4, 6-1; Tsung-Hua Yang (TPE) bt Bernabe Zapata Miralles (ESP) 7-6 (1), 3-0 (retd); WC-Vishnu Vardhan<br />(IND) lost to Mario Vilella Martinez (ESP) 3-6, 3-6; Naoki Nagakawa (JPN) lost to Pedro Martinez (ESP) 7-6<br />(5), 6-3<br />Doubles<br />Timur Kabibulin (KAZ) / Alexandr Nedovyesov (KAZ) bt 4- Scott Clayton (GBR) / Jonny O&rsquo;Mara (GBR) 6-4,<br />4-6, 10-7; 1-Mikhail Elgin (RUS) / Divij Sharan (IND) bt Geoffrey Blancaneaux (FRA) / Jay Clarke (GBR) 5-7,<br />6-4, 10-5; Sumit Nagal (IND) / Elias Ymer (SWE) bt Jeevan Nedunchezhiyan (IND) / N Vijay Sundar<br />Prashanth (IND) 6-4, 1-6, 10-7</p>', 'Admin', '2017-11-21', 'Bangalore', 'India', '2017-11-22', '2017-12-22', '2017-11-22 10:35:06', '2017-11-22 16:35:31', 'A', '', 'Description', 'Gojo sends second seed home at Bengaluru Open 2017'),
(113, 'Fancied players bite the dust at Bengaluru Open', 'Home', 'F-', 'A', 'uploadimage/Yuki Bhambri5479.jpg', '<p>Bengaluru, Nov 22: On a day when the fancied players bit the dust, Ante Pavic caused the biggest ripple<br />as he showed fourth seed Elias Ymer the door in a pre-quarterfinal match of the US $ 100,000 Bengaluru<br />Open at the KSLTA Stadium here on Wednesday. The unheralded Croatian who was a 6-3, 6-7(2), 7-6(5)<br />winner, will clash with Jay Clarke of Great Britain who upset fifth seed Ramkumar Ramnathan of India in<br />a grueling three-setter 6-7(3), 6-2, 6-4 in the quarterfinal.<br />In another upset, lucky loser Antoinne Escoffier of France sent home eighth seed Aleksandr Nedovyesov<br />of Khazakistan 6-3, 2-6, 6-3. However the top seed Slovanian Blaz Kavcic of Croatia and third seed Yuki<br />Bhambri of India, although stretched by their respective opponents, made it to the last eight stage.<br />While Kavcic was stretched by Spaniard Mario Villela Martinez before prevailing 6-4, 3-6, 6-3, Bhambri<br />quelled the challenge of Pedro Martinez (ESP) 6-2, 7-6 (0).<br />Prajnesh Gunneswaran who had felled sixth seed Evan King in the opening round, continued his fine run<br />eliminating Australian Marc Polmans 6-2, 6-7 (1), 6-1 in a fast paced game. Also advancing to the<br />quarterfinals was wildcard Sumit Nagal who was a 6-4, 4-6, 7-5 winner over Brydan Klein of Great Britain<br />and Tsung-Hua Yang of Teipei who overcame qualifier Borna Gojo 2-6, 6-4, 6-2. Gojo had earlier ousted<br />second seed Adrian Menendez-Maceiras in the opening round.<br />Kavcic who has been struggling with a knee pain, raced to a 4-0 lead, thanks to breaks in the first and<br />third games. However, the Spainard fought back to win three games in a row and showed the promise<br />of a comeback which he did in the second set which he clinched 6-3. A disputed line call was playing up<br />on Kavcic&rsquo;s mind which did make him lose his concentration. In the deciding set, Martinex lost his serve<br />in the opening game but broke back. A similar story followed in the fifth and sixth game. Kavcic send<br />down an ace to hold serve in the seventh game and achieved a break in the next to go 5-3 up. Martinez<br />saved three match points, thanks to two double faults but before he could lose control, the top seed<br />sealed the match.<br />&ldquo;I am not used to playing in high altitudes. The long games are putting pressure on my knee,&rdquo; said Kavcic<br />who by the virtue of his second round win has almost sealed a spot in the Australian Open in January.<br />Prajnesh began on the right note breaking his opponent twice and racing to a 4-0 lead. The 28-year- old<br />served with fervour and achieved great results as he clinched the first set 6-2. In the second set,<br />Polmans made a remarkable recovery from being 2-4 down and took the set to a tie-breaker and won.<br />However, in the decider, Prajnesh didn&rsquo;t give a semblance of a chance to his opponent as he sent down<br />winners past his tiring rival.<br />Results (Pre-quarters)<br />Singles (prefix indicated seeding)<br />1-Blaz Kavcic (SLO) bt Mario Villela Martinez (ESP) 6-4, 3-6, 6-3; 3-Yuki Bhambri (IND) bt Pedro Martinez<br />(ESP) 6-2, 7-6 (0); 5-Ramkumar Ramnathan (IND) lost to Jay Clarke (GBR) 7-6(3), 2-6, 4-6; 8-Aleksandr<br />Nedovyesov (KAZ) lost LL-Antoinne Escoffier (FRA) 3-6, 6-2, 3-6; Ante Pavic (CRO) bt 4-Elias Ymer (SWE)<br /><br />6-3, 6-7 (2), 7-6 (5); Marc Polmans (AUS) lost to Prajnesh Gunneswaran (IND) 6-2, 6-7 (1), 6-1; Tsung-Hua<br />Yang (TPE) bt Q-Borna Gojo 2-6, 6-4, 6-2; Sumit Nagal (IND) bt Brydan Klein (GBR) 6-4, 4-6, 7-5.<br />Doubles (quarterfinals)<br />1-Mikhail Elgin (RUS) / Divij Sharan (IND) bt Sumit Nagal (IND) / Elias Ymer (SWE) 6-3, 6-3; Ivan Sabanov<br />(CRO) / Matej Sabanov (CRO) bt 2-Brydan Klein (GBR) / Marc Polmans (AUS) 6-1, 2-6, 10-8; 3-N Sriram<br />Balaji (IND) / Vishnu Vardhan (IND) bt Prajnesh Guneswaran (IND) / Ramkumar Ramanathan 6-3, 6-3;<br />Tomislav Brkic (BIH) / Ante Pavic (CRO) bt Timur Khabibulin (KAZ) / Aleksandr Nedovyesov (KAZ) 6-1, 4-6,<br />10-7<br />Quarterfinal line-up Singles<br />1-Blaz Kavcic (SLO) Vs. Sumit Nagal (IND); 3-Yuki Bhambri (IND) Vs. Prajnesh Gunneswaran (IND); Jay<br />Clarke (GBR) Vs. Ante Pavic (CRO); LL-Antoine Escoffier Vs. Tsung-Hua Yang (TPE)<br />Semifinal line-up Doubles<br />1-Mikhail Elgin (RUS) / Divij Sharan (IND) Vs. 3-N Sriram Balaji (IND) / Vishnu Vardhan (IND)<br />Ivan Sabanov (CRO) / Matej Sabanov (CRO) Vs. Tomislav Brkic (BIH) / Ante Pavic (CRO)</p>', 'Admin', '2017-11-22', 'Bangalore', 'India', '2017-11-22', '2018-03-14', '2017-11-22 10:39:35', '2018-02-14 06:05:21', 'A', '', 'Description', 'Fancied players bite the dust at Bengaluru Open'),
(114, 'Nagal stuns top seed to storm into semifinals at Bengaluru Open', 'Home', 'N-', 'A', 'uploadimage/Nagal29736.jpg', '<p>Nagal stuns top seed to storm into semifinals at Bengaluru Open<br />Bangalore, Nov 23: Sumit Nagal, ranked 219 places below the Slovanian Blaz Kavcic, could not have<br />justified his last minute entry into the main draw from being awarded a wild card in a better manner<br />than to show the top seed the door in the quarterfinals of the US $ 100,000 Bengaluru Open 2017, an<br />event on the ATP Challenger Tour being held at the KSLTA Courts here on Thursday.<br />The Delhi-ite took just 76 minutes to emerge as a 6-3, 6-4 winner and will now meet compatriot Yuki<br />Bhambri in the semifinals. Bhambri, seeded third, took one more step closer in his attempt to register a<br />back to back victory, quelling the challenge of Prajnesh Gunneswaran in straight sets 7-5, 6-2 to advance<br />to the last four stage.<br />In the other quarterfinals, Tsung-Hua Yang of Teipei stopped the progress of lucky loser Antoine<br />Escoffier 6-4, 6-4 while Jay Clarke of Great Britain overcame Ante Pavic of Croatia 6-2, 4-6, 7-6 (1). The<br />respective winners will face off in the second semifinal.<br />Nagal who took on the top seed didn&rsquo;t have any expectations out of the match. &ldquo;I wasn&rsquo;t thinking<br />anything before the match. I just wanted to play some good tennis,&rdquo; said the 20-year- old who has never<br />reached the quarterfinal stage of the many Challenger events he has played thus far. After both the<br />players held on their respective serves, Nagal achieved the crucial break in the fourth game to go 3-1 up<br />from where he didn&rsquo;t look back and took the first set 6-3.<br />Nagal was quick on the court as Kavcic depended heavily on his serves. After both the players had<br />broken each other in the first two games, the former broke again in the third game, thanks to a double<br />fault by Kavcic and went 2-1 up. Both the players held their respective serves as the diminutive player<br />clinched the set and match 6-4. &ldquo;I have never played Bhambri before and I guess it will be a good<br />match,&rdquo; said Nagal ahead of his semifinal clash.<br />In the other match which oozed of power, Bhambri and Gunneswaran went hammer and tongs from the<br />word go while holding on to their respective serves until the 10 th game. Gunneswaran was the fiercer of<br />the two sending down seven aces in the first set. Bhambri had to pull out all his experience to counter<br />the attack and managed to achieve a break in the 11 th game. Serving for the set, the Delhi lad was two<br />break points down and came back to win the set. &ldquo;It was crucial for me to win those points as anything<br />could have happened in the tie-breaker,&rdquo; said Bhambri after the game.<br />In the second set, Bhambri seemed to be in much better control as he broke the Chennai based player&rsquo;s<br />serve in the first game and was answered in a similar manner in the second. The trend continued until<br />the third before Bhambri held on and broke once again in the fifth to take the set and the match. &ldquo;It<br />does not matter whom I play in the semifinal as I have planned to take game by game,&rdquo; said the winner.<br />Meanwhile in the doubles semifinals the top seeded pair of Mikhail Elgin of Russia and Indian Divij<br />Sharan overcame the third seeds M Sriram Balaji and Vishnu Vardhan 6-4, 4-6, 10-8 to reach the final<br />where they will meet Ivan Sabanov and Matej Sabanov of Croatia, who were 7-6 (3), 6-4 winners over<br />Bosnian Tomislav Brkic and Croatian Ante Pavic.<br /><br />Results (Prefix indicates seeding)<br />Singles Quarterfinals<br />1-Blaz Kavcic (SLO) lost to Sumit Nagal (IND) 3-6, 4-6<br />3-Yuki Bhambri (IND) bt Prajnesh Gunneswaran (IND) 7-5, 6-2<br />LL-Antoine Escoffier (FRA) lost to Tsung-Hua Yang (TPE) 4-6, 4-6<br />Jay Clarke (GBR) bt Ante Pavic (CRO) 6-2, 4-6, 7-6 (1)<br />Doubles semifinals<br />Ivan Sabanov / Matej Sabanov (CRO) bt Tomislav Brkic (BIH) / Ante Pavic (CRO) 7-6 (3), 6-4<br />1-Mikhail Elgin (RUS) / Divij Sharan (IND) bt 3-M Sriram Balaji (IND) / Vishnu Vardhan (IND) 6-4, 4-6, 10-8<br />Semifinal line-up singles<br />Tsung-Hua Yang (TPE) Vs. Jay Clarke (GBR)<br />3-Yuki Bhambri (IND) Vs. Sumit Nagal (IND)<br />Doubles final<br />1-Mikhail Elgin (RUS) / Divij Sharan (IND) Vs. Ivan Sabanov / Matej Sabanov (CRO) bt Tomislav Brkic (BIH)</p>', 'Admin', '2017-11-23', 'Bangalore', 'India', '2017-11-24', '2017-12-24', '2017-11-24 03:12:20', '2017-11-24 09:12:29', 'A', '', 'Description', 'Nagal stuns top seed to storm into semifinals at Bengaluru Open'),
(115, 'Rohan Bopanna accorded ‘Roll of Honour’ at KSLTA', 'Home', 'R-B', 'A', 'uploadimage/Rohan255669.jpg', '<p>To partner Vasselin in doubles next year</p>\r\n<p>Bengaluru, Nov 24: Rohan Bopanna who virtually grew up playing tennis at the KSLTA courts and went<br />on to achieve phenomenal success at the National and the International level, was given the pride of<br />place amongst the many legends who adorn the walls of KSLTA by unveiling a portrait of Bopanna&rsquo;s<br />biggest success &ndash; the Mixed Doubles title at the French Open, earlier this year.<br />&ldquo;I am touched by KSLTA&rsquo;s gesture. I am truly honoured to have received this appreciation. I will be ever<br />grateful to KSLTA for having nurtured me and supported me in my growing years,&rdquo; said the 37-year- old<br />who was hailed as the only true son of the soil who has brought the highest honours in tennis.<br />Meanwhile, Bopanna and KSLTA have tied up to set up the Rohan Bopanna Tennis Academy at the<br />KSLTA and will be operational from Dec 1.<br />Speaking on the sidelines of the Bengaluru Open 2017 ATP Challenger, Bopanna felt that the Challengers<br />are the way to go. &ldquo;When I was growing up, we did not have a lot of Challengers in India. There is<br />nothing like playing at home, having your home support. Last week in Pune, we had two Indians in a<br />Challenger final. And again today to see Sumit and Yuki competing against each other. I think this<br />Challenger plays a huge role in grooming a player and getting exposure to world class and international<br />level of tennis, and players coming in to your home country,&rdquo; said the Kodava while expressing his<br />happiness over the growing standards of Indian tennis.<br />A combination of Futures and Challengers was a heady mix for the success of the sport in India. &ldquo;I feel<br />that the more competitions we have in India the better the players get. If each state has one Challenger<br />and one Future, we will definitely produce a lot more players, not only trying to get into the top 100 but<br />all the way up to 1000,&rdquo; said the champ.<br />Talking about his future plans, Bopanna said that we would be partnering World No.26 Edouard Roger-<br />Vasselin in the doubles for the next season. &ldquo;One of the key things I learnt with the numerous partner<br />changes is that I had to completely focus on my game irrespective of who my partner is. I chose Edouard<br />because he is now purely going to be doubles player and we can practice more with each other. I<br />decided to find someone just to play doubles. It always helps playing with someone you have played<br />before and we did well in that short period of time, and, of course, once again the focus is on Grand<br />Slams and qualify for the London Masters,&rdquo; said the seasoned pro who admitted that it has been about<br />10-12 years since he has taken a break for himself.<br />Speaking about the Davis Cup squad where he has been shouldering the responsibility as a doubles<br />player, Bopanna said that there is a need to balance the team. &ldquo;I think one of the major factors next<br />year is that it is going to be a two day event and singles is going to be of 3 sets. Although I&rsquo;m the<br />only one playing doubles at the moment, there is no pressure at all. Being with the young guys,<br />motivating them and also being the senior member of the team motivates me and I&rsquo;ve done this for<br />some time now and I thrive at such situations,&rdquo; said the lanky pro.<br />Bopanna who is ranked 18 th in the World doubles ranking is inspired by Roger Federer. &ldquo;Well, if you look<br />at Roger Federer playing singles at 36 then playing doubles at 37 is not a big challenge. The fact that<br />making sure that you are cooling down well, focusing on yourself. If I am fit, then Tokyo is definitely my<br /><br />target. But tennis is a sport, which you cannot think of that far ahead and you have to take it in steps, as<br />there&rsquo;s still a long way to go for Tokyo,&rdquo; said the Kannidiga who has won three titles in 2017 including<br />the French Open.</p>', 'Admin', '2017-11-24', 'Bangalore', 'India', '2017-11-24', '2018-03-14', '2017-11-24 08:51:00', '2018-02-14 06:04:06', 'A', '', 'Description', 'Rohan Bopanna accorded ‘Roll of Honour’ at KSLTA'),
(116, 'Unheralded Sumit stuns Bhambri, storms into the finals of Bengaluru Open', 'Home', 'D-B', 'A', 'uploadimage/Doubles Winner57149.jpg', '<p>Captions: 1. Doubles winners: From left: Kumar Pushkar, MD KSTDC, Surendran, CEO Airtel Karnataka Circle,<br />Ivan Sabanov, Matej Sabanov, Mikhail Elgin, Divij Sharan, Bishwajit Mishra, Director BMTC,<br />Ramaswamy, Joint Secretary KSLTA, Sunil Yajaman, Tournament Director and Stephane Cretois,<br />ATP Supervisor.<br />2. Clarke: Jay Clarke of Great Britain exults after winning his semifinal against Tsung-Hua Yang<br />3. Sumit Nagal in action against Yuki Bhambri<br /><br />Unheralded Sumit stuns Bhambri, storms into the finals of Bengaluru Open<br />Bengaluru, Nov 24: Unheralded Sumit Nagal of India brushed aside compatriot and seed No.3 Yuki<br />Bhambri in straight sets to storm into the finals of the US $ 100,000 Bengaluru Open at the KSLTA<br />Stadium here on Friday.<br />The 20-year- old Nagal won 6-4, 6-0 in just 67 minutes to set up a title tilt with Jay Clarke of Great Britain<br />who quelled the challenge of Tsung-Hua Yang of Teipei 6-3, 4-6 in the other semifinals. Both Nagal and<br />Clarke will be vying for their maiden title.<br />Meanwhile in the doubles final, the top seeded pair of Divij Sharan of India and Mikhail Elgin of Russia<br />brushed aside the challenge of Sabanov brothers - Ivan and Matej of Bosnia 6-3, 6-0 to lift the doubles<br />title. With this victory, Divij breaks into the top 50 in world rankings in doubles.<br />Sumit after stunning the top seed Blaz Kavcic in the quarterfinals yesterday, oozed with confidence as he<br />stepped on to the court. Bhambri looked a bit jaded. With both of them holding on to their serves, Nagal<br />achieved a break in the sixth game to go 4-2 up, thanks to his opponent who committed several<br />unforced errors. Bhambri returned the favour in the next game and both traded breaks for the next<br />three games as Nagal pocketed the first set 6-4.<br />&ldquo;I went into the game that I have nothing to lose against Yuki because he is looking to break into the top<br />100 and was under pressure. I think I was more aggressive today than my usual self and covered the<br />court very well, thanks to my fitness coach Yash who has kept me in good stead during these last three<br />days of strenuous games,&rdquo; said Nagal.<br />If Nagal has shown sparks of brilliance in the opening set, he built on it in the second. Bhambri never<br />looked confident as his younger opponent didn&rsquo;t give him a semblance of a chance to recover. &ldquo;I knew if<br />I gave him chances of long rallies, it would build his confidence and a chance to comeback. Hence I tried<br />to kill it at the earliest,&rdquo; said Nagal. Earning a break in the second game, Nagal broke Bhambri&rsquo;s serve in<br />the fourth and the sixth to wrap up the issue.<br />&ldquo;I am also surprised by my victory. I did not think it would be that easy, it just happened,&rdquo; said the<br />finalist. &ldquo;I have played with him (Clarke) in the Junior Tour and I know about his game and I would adopt<br />the same strategy as yesterday and today,&rdquo; said a confident Nagal.<br /><br /><br />Earlier, in the first semifinal, Clarke and Yang began in the right earnest keeping a fast pace until the<br />former broke Yang&rsquo;s serve in the fourth game to go 3-1 up and both held their respective serves until<br />Clarke took the first set 6-3. However in the second set, Yang took control of the game and with breaks<br />in the second and the fourth game, went 4-0 up. However, Clarke has other plans in mind and broke his<br />opponents serve in the next game and won six games in a row to clinch the match.<br /><br />Results (Prefix indicates seeding)<br />Semifinal singles<br />Tsung-Hua Yang (TPE) lost to Jay Clarke (GBR) 3-6, 4-6<br />3-Yuki Bhambri (IND) lost to Sumit Nagal (IND) 4-6, 0-6<br />Doubles final<br />1-Mikhail Elgin (RUS) / Divij Sharan (IND) Vs. Ivan Sabanov / Matej Sabanov (CRO) 6-3,6- 0<br />Final singles (6.30 p.m. on Nov 25)<br />Jay Clarke (GBR) Vs. Sumit Nagal (IND)</p>', 'Admin', '2017-11-24', 'Bangalore', 'India', '2017-11-25', '2018-03-14', '2017-11-25 03:12:12', '2018-02-14 06:04:53', 'A', '', 'Description', 'Unheralded Sumit stuns Bhambri, storms into the finals of Bengaluru Open'),
(117, 'Karnataka Men\'s Team Bagged Bronze medal at Bhilai', 'Home', 'K-M', 'A', 'uploadimage/Inter state Boyrs40744.jpg', '<p>Karnataka Men\'s Team bagged bronze medal in the All india interstate mens nationals in Bhilai <span class=\"aBn\" tabindex=\"0\" data-term=\"goog_736867642\"><span class=\"aQJ\">on Friday</span></span>. However the team went down to Maharashtra in the semifinals 2-0. Nikshep.B.R lost to Aryan goveas 1-6,1-6 and Rishi reddy lost to Arjun Khade 2-6,1-6.&nbsp;</p>', 'Admin', '2018-02-27', 'Bhilai', 'India', '2018-02-27', '2018-03-27', '2018-02-27 05:56:14', '2018-02-27 11:56:24', 'A', '', 'Description', 'Karnataka Men\'s Team Bagged Bronze medal at Bhilai'),
(118, 'Karnataka Women\'s Team bagged bronze medal', 'Home', 'W-T', 'A', 'uploadimage/Girls Team88451.jpg', '<div style=\"font-size: 13px;\">\r\n<p>Karnataka Women\'s Team bagged bronze medal in the All india interstate mens nationals in Bhilai <span class=\"aBn\" tabindex=\"0\" data-term=\"goog_736867642\"><span class=\"aQJ\">on Thursday</span></span>.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n</div>\r\n<p>&nbsp;</p>', 'Admin', '2018-03-02', 'Bhilai', 'India', '2018-03-03', '2018-04-03', '2018-03-03 00:33:49', '2018-03-03 06:35:00', 'A', '', 'Description', 'Karnataka Women\'s Team bagged bronze medal'),
(119, 'Naisha in Indian Team', 'Home', 'N-T', 'A', 'uploadimage/Naisha18390.jpg', '<p>KSLTA is happy to announce that Naisha Srivastav has been selected to be part of the Indian team for the Asian-Oceania World Junior (U-14) tennis championships to be held in Bangkok from April 1st to 8. KSLTA wishes Naisha all the best for a successful tournament and bring glory to the nation &amp; State.</p>\r\n<p>Hon.Jt.Secretary / KSLTA</p>\r\n<p>&nbsp;</p>', 'Admin', '2018-03-29', 'Bangalore', 'India', '2018-03-29', '2018-04-29', '2018-03-29 04:29:32', '2018-03-29 09:29:45', 'A', '', 'Description', 'Naisha in Indian Team'),
(120, 'Asian Tennis Championships Aayush Bhat', 'Home', 'A-B', 'A', 'uploadimage/Ayush46633.jpg', '<p>Asian Tennis Championship, Indian Tennis Academy,Delhi . Singles finals - Aayush Bhat (USA)&nbsp; Bt&nbsp; Shivam Kadam(Ind) 6-3, 6-1.</p>\r\n<p>Doubles Finals Ajay Singh(Ind) and Sukhpreet Singh Jhole (Ind)&nbsp; Bt Aayush Bhat (USA) and Ayushmaan Arjeria (Ind) 6-1,3-6,10-7</p>', 'Admin', '2018-04-14', 'Delhi', 'India', '2018-04-16', '2018-05-16', '2018-04-16 00:21:22', '2018-04-16 05:21:32', 'A', '', 'Description', 'Asian Tennis Championships Aayush Bhat'),
(121, 'KSLTA felicitated the players for their good performance in the All India tournaments. ', 'Home', 'K-F', 'A', 'uploadimage/Felisi29230.jpg', '<p>Karnataka state Lawn Tennis Association (KSLTA) is pleased to inform that today, we have felicitated the following players, with cash award, for their good performance in the All India Tennis Tournaments held during the year 2016 and 2017: </p>\r\n<ol>\r\n<li><strong>SURAJ R PRABODH</strong> , Runner up in the Men Singles Fenesta open Nationals 2017 &ndash; Rs.25,000</li>\r\n<li><strong>MUSTAFA M RAJA</strong>, Most promising player in Children U-10 - 25,000/-</li>\r\n<li><strong>AKARSH V GAONKAR</strong>, U-12 Nationals runner up in singles and winner in Doubles 2016.&nbsp; - Rs. 25,000/-</li>\r\n</ol>\r\n<ol start=\"4\">\r\n<li><strong>AAYUSH P BHAT</strong>, under 12, was ranked AITA No. 1 in the year end 2017 - Rs.50,000/-</li>\r\n</ol>\r\n<p>&nbsp;Sri P. R. Ramaswamy, Hon.Jt.Secretary, Sri Mohan Gurjar and Ms. Gayathri Rao, Executive Council Members&nbsp; of KSLTA, were present during the occasion.</p>', 'Admin', '2018-04-27', 'Bangalore', 'India', '2018-04-27', '2018-05-27', '2018-04-27 05:10:53', '2018-04-27 10:11:07', 'A', '', 'Description', 'KSLTA felicitated the players for their good performance in the All India tournaments. '),
(122, 'Ronin lotlikar is the Runner-up in singles U 14 ', 'Home', 'R-L', 'A', 'uploadimage/Ronin Lotlikar1027.jpg', '<p>Ronin lotlikar is the Runner-up in singles U 14 in the MSLTA- Yonex Sunrise Hotel Ravine National Series Tennis tournament.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', 'Admin', '2018-05-13', 'PANCHGANI', 'India', '2018-05-14', '2018-06-14', '2018-05-14 04:56:36', '2018-05-14 09:56:50', 'A', '', 'Description', 'Ronin lotlikar is the Runner-up in singles U 14 '),
(123, 'Aayush Bhat & Anmay Devaraj won the Doubles U-14', 'Home', 'D-U', 'A', 'uploadimage/Aayush Bhat29344.jpg', '<p>Aayush Bhat &amp; Anmay Devaraj won the Doubles U-14&nbsp; in the MSLTA- Yonex Sunrise Hotel Ravine National Series Tennis tournament.</p>\r\n<p>&nbsp;</p>', 'Admin', '2018-05-13', 'PANCHGANI', 'India', '2018-05-14', '2018-06-14', '2018-05-14 05:00:26', '2018-05-14 10:01:04', 'A', '', 'Description', 'Aayush Bhat & Anmay Devaraj won the Doubles U-14');
INSERT INTO `ksl_news_tbl` (`news_id`, `news_title`, `news_display_tag`, `news_link_address`, `news_link_status`, `news_content_image`, `news_editor_content`, `news_posted_by`, `news_posted_at`, `news_located_city`, `news_located_country`, `news_from_date`, `news_to_date`, `news_created_date`, `news_lastupdated_date`, `news_flat`, `news_tournament_id`, `news_meta_name`, `news_meta_content`) VALUES
(124, 'Mustafa Doubles winner Super series U-12', 'Home', 'M-D', 'A', 'uploadimage/Mustafa47219.jpg', '<p>The one with the red coat is Mustafa R raja from Mysuru, he has won the super series U-12 doubles held in Hyderabad on 13.08.2018. his partner From TN</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', 'Admin', '2018-08-20', 'Hyderabad', 'India', '2018-08-20', '2018-09-22', '2018-08-20 07:58:55', '2018-08-22 08:32:12', 'A', '', 'Description', 'Mustafa Doubles winner Super series U-12'),
(125, 'KSLTA felicitated the players ', 'Home', 'F-T', 'A', 'uploadimage/133743.jpg', '<p><strong><em>Karnataka state Lawn Tennis Association (KSLTA) felicitated the following players, with cash award of Rs.50,000/- to each Players for winning the bronze Medal in the All India Inter State Team Tennis Championship held at Bhilai from 26<sup>th </sup>Feb to 3<sup>rd</sup> Mar.2018</em></strong></p>\r\n<ol>\r\n<li><strong>SOHA S</strong></li>\r\n<li><strong>VANSHITA PATHANIA </strong></li>\r\n<li><strong>APOORVA S B </strong></li>\r\n<li><strong>PRATHIBA PRASAD NARAYAN</strong></li>\r\n</ol>\r\n<p>&nbsp;</p>\r\n<p><strong>Sri P. R. Ramaswamy, Hon.Jt.Secretary, Sri . K C Nagaraj, Executive Council Member of KSLTA, were present during the occasion</strong>.</p>', 'Admin', '2018-08-22', 'Bangalore', 'India', '2018-08-22', '2018-09-22', '2018-08-22 03:29:45', '2018-08-22 08:32:45', 'A', '', 'Description', 'KSLTA felicitated the players '),
(126, 'Ayush bags a double in boys U-14', 'Home', 'A-D', 'A', 'uploadimage/Ayush P Bhat30580.jpg', '<p>Ayush P Bhat bagged a double in Boys U-14 in the AITA National Series tennis that concluded at Sania Mirza Tennis Academy on saturday. in the singles final Bhat to defeat Ayushmaan Arjeria 3-6, 6-2, 7-5 and in the doubles Ayush Bhat and Vishesh jainish Patel to beat Deep Munim and Ayush Maan Arjeria 6-7(1), 6-4, 13-11.</p>\r\n<p>&nbsp;</p>', 'Admin', '2018-08-27', 'Telangana', 'India', '2018-08-27', '2018-09-27', '2018-08-27 05:31:50', '2018-08-27 10:33:57', 'A', '', 'Description', 'Ayush bags a double in boys U-14'),
(127, 'KSLTA Congratulates Mr.Rohan Bopanna', 'Home', 'R-B', 'A', 'uploadimage/RB72277.jpg', '<p><strong><em>Karnataka state Lawn Tennis Association (KSLTA) Congratulates Mr. Rohan Bopanna on Winning The Men&rsquo;s Doubles Gold Medal For India at Asian Games 2018 Held in Indonesia . We Wish him Good Luck in his Future Endeavors</em></strong>.&nbsp;</p>', 'Admin', '2018-08-24', 'Bangalore', 'India', '2018-08-27', '2018-09-27', '2018-08-27 05:43:06', '2018-08-27 10:43:15', 'A', '', 'Description', 'KSLTA Congratulates Mr.Rohan Bopanna'),
(128, 'Bengaluru Open-18 ATP Challenger', 'Home', '18', 'A', 'uploadimage/Snapseed (1)7568.jpg', '<p>Asia&rsquo;s biggest ATP Challenger, Bengaluru Open returns to the city.</p>\r\n<p style=\"text-align: justify;\">Organized by the Karnataka State Lawn Tennis Association, the richest ATP event internationally now has new event to select Wild Card player.</p>\r\n<p style=\"text-align: justify;\">&nbsp;<strong>Bengaluru, Oct 11:</strong> The Bengaluru Open, one of the most important ATP Challenger tournaments in the circuit, will return to the city. Organized by the Karnataka State Lawn Tennis Association, the Bangalore Open will be held between Nov 12-17 this year. The Bengaluru Open will offer a prize purse of USD 150,000, an increase of USD 50,000 over last edition. The tournament will be a perfect platform for National and International players, as well as aspiring local players, to compete at an International Challenger stage, and improve their ATP rankings, and compete at Grand Slams.</p>\r\n<p style=\"text-align: justify;\">A public 30-day countdown to the event was initiated today at a press conference by the Honorable Deputy Chief Minister of Karnataka, Dr. G Parameshwara. Speaking about the tournament, Dr G Parameshwara said, &ldquo;Brand Bengaluru is associated with Technology, with culture, with gardens and greenery. But Bengaluru City is also known for its sporting prowess and spirit. Bengaluru Open is all about that. Bengaluru Open we hope will be a platform to bring international and national tennis players together and offer them a fantastic playing field. It will be the perfect stage to demonstrate to the world Bengaluru is a truly dynamic, multi-faceted city&rdquo;</p>\r\n<p style=\"text-align: justify;\">&nbsp;&ldquo;I am sure the event will not only attract the top players but also be a great learning experience for the young tennis players in the city. This will be a fantastic opportunity for the youth to see their favorite sport stars in person and experience international tennis right from the centre court,&rdquo; he added.</p>\r\n<p style=\"text-align: justify;\">The ATP Challenger Bengaluru Open, apart from earning the winner USD 21600, will also give a chance to homegrown players to earn valuable ATP points and improve their ranking on the ATP circuit. Sumit Nagal, who is currently the defending champion, had jumped 96 places and reached a career best ranking of 225 after his fairytale victory in the 2017 Bengaluru Open.</p>\r\n<p style=\"text-align: justify;\">Addressing the press, Shri Priyank Kharge, Honorable Minister of Social Welfare, Government of Karnataka, and Chairman, Organizing Committee, Bengaluru Open, said, &ldquo;The Government of Karnataka is happy to provide support to the Bengaluru Open which is now the biggest event on the ATP circuit in the Asian region.</p>\r\n<p>&nbsp;Through our support, we intend to not only promote tennis but help in growing the sport and ensure all deserving players get a chance to compete. We hope more and more youngsters take to the sport and bring our city, great pride with their victories.&rdquo;</p>\r\n<p style=\"text-align: justify;\">The Minister remarked that the KSLTA was leaving no stone unturned to make the event a huge success.</p>\r\n<p style=\"text-align: justify;\">The KSLTA will be organizing a national-level ranking tournament, from Oct 22 to Oct 27. The winner of the men&rsquo;s singles will earn a direct entry into the main round of the ATP Challenger as a wild card. Additionally, winners will earn a prize money of up to Rs. 3 lakhs. Meanwhile at the Bengaluru Open, a main draw of 32 players will compete to lift the cup and proceed to the top of the rankings. The 32 players will comprise of - 22 players who have earned a direct entry, 4 wildcard players, four qualifiers (draw of 32) and 2 players from special exempted category.</p>\r\n<p style=\"text-align: justify;\">KSLTA will also be scouting for ball kids for the tournament amongst the trainees from the various academies located in the City.&nbsp;</p>\r\n<p style=\"text-align: justify;\">Presiding over the function, former Dy. Chief Minister and Senior Vice President of KSLTA, Shri R Ashoka lauded the efforts of the Government of Karnataka in supporting the event. &ldquo;We are happy to host the ATP Challenger in Bengaluru. On behalf of KSLTA, we profusely thank the Government of Karnataka, especially the Hon. Deputy CM Dr. G Parameshwara for the support to the event,&rdquo; he said. &ldquo;We also appreciate the effort of Shri Priyank Kharge, Minister for Social Welfare for taking the lead in organizing this mega event aimed at promoting the city of Bengaluru as a truly global city,&rdquo; he added.</p>\r\n<p style=\"text-align: justify;\">Former National coach and AITA Development Officer, Sunil Yajaman who will be the tournament Director, while welcoming the gathering said that he was hopeful of some big names signing on to compete. &ldquo;With last year&rsquo;s success, the event has created a ripple amongst the tour regulars. We are hoping for a better field than the last edition. We will give our best to ensure that the players take back happy memories,&rdquo; he said.&nbsp;&nbsp;</p>\r\n<p style=\"text-align: justify;\">For its part, the KSLTA hopes to conduct big ticket events. The Association has already hosted many National and International events including ATP World doubles championship, Davis Cup and the WTA where the famed William sisters &ndash; Serena and Venus - once graced these courts. With a world class facility set amidst a serene backdrop and some quality tennis, a tennis buff cannot ask for more.</p>\r\n<p style=\"text-align: justify;\">The dignitaries and professionals were united in hoping that the Bengaluru Open will soon be a regular fixture in the sporting calendar of the country, and be an important stop for international players in search of that Grand Slam victory.</p>', 'Admin', '2018-10-11', 'Bengaluru', 'India', '2018-10-15', '2018-11-15', '2018-10-15 04:03:23', '2018-10-15 09:05:29', 'A', '', '', 'Bengaluru Open-18 ATP Challenger'),
(129, 'Strong field for Bengaluru Open 2018', 'Home', 'S-F', 'A', 'uploadimage/LOGO Bengaluru Open-1880646.jpg', '<p><strong>Strong field for Bengaluru Open 2018</strong></p>\r\n<ul>\r\n<li><strong>Two players under top-100</strong></li>\r\n<li><strong>Eight players under top-150</strong></li>\r\n<li><strong>Prajnesh only Indian to get direct entry</strong></li>\r\n</ul>\r\n<p><strong>Bengaluru, Oct 23:</strong> The second edition of the Bengaluru Open is all set to see some pulsating action considering the field that has signed in for the richest ATP Challenger Tour event to be held at the KSLTA from Nov 12-17. Two players &ndash; Radu Albot of Moldova and Russian Evgeny Donskoy ranked 89<sup>th</sup> and 99<sup>th</sup> respectively lead the strong field which was released by the ATP today. Prajnesh Gunneswaran (ranked 146<sup>th</sup>) is the lone Indian to get into the main draw of 32 in the USD 150,000 prize money event. Apart from the prize money, the hospitality is also being taken care of by the organisers.</p>\r\n<p>Jordan Thomson (Australia, No. 101), Ricardas Berankia (Lithuania, No. 131), Elias Ymer (Sweden, No. 133), Marc Polmans (Australia, No. 145) and Simone Bolelli (Italy (No. 147) make up the under-150 list. The cut-off for direct entry is rank No. 244 which personifies the quality of the field. The main draw of 22 includes players from 19 countries while the players list in the Alternates consists of players from eight other countries, thus giving the semblance of a truly international event. &nbsp;</p>\r\n<p>Called as the &lsquo;Machine&rsquo; by fellow players, the 28-year-old Radu who will start the Bengaluru Open with the tag of the top seed, started playing tennis at the age of eight. He recorded his biggest win of his career when he beat No. 12 seed Carreno Busta in five sets to reach the third round of Wimbledon this year. Evgeny Donskoy, meanwhile, who grew up idolizing Yevgeny Kafelnikov, was ranked 71<sup>st</sup> when he began the season and but had dropped in ranking owing to an injury. Australian Jordan Thompson who will be seeded third, will be looking for another success on Indian soil after his win at the Chennai Open in February this year. &nbsp;&nbsp;&nbsp;</p>\r\n<p>The event apart from making the winner richer by USD 21600, will give a chance, especially the homegrown players to earn valuable ATP points and improve their ranking on the ATP circuit. Defending champion Sumit Nagal will start as a qualifier, owing to dip in his ranking while the top seed for the last edition Blaz Kavcic of Slovia has managed to get an entry in the main round with a ranking of 225.</p>\r\n<p>&ldquo;It is very heartening to note that the Bengaluru Open has achieved the status of an important event on the Challenger Circuit in such as a short period. The strong field means our boys will get a chance to rub shoulders with the best in the field and a great opportunity to improve, apart from earning valuable points that will catapult them into getting an entry into bigger events. On behalf of Government of Karnataka and KSLTA, I welcome sporting ambassadors from more than 20 countries to Bengaluru and we shall ensure they have a splendid experience here,&rdquo; said Priyank Kharge, Minister of Social Welfare, Govt. of Karnataka who is also the Chairman of the Organising Committee.</p>\r\n<p>The main draw of 32 comprises of 22 players who get a direct entry, four wild cards and four qualifiers with two from special exempted category making up the final numbers. One of the wild card would be given to the winner of the on-going Bengaluru Open Wild Card event.</p>\r\n<p>Sunil Yajaman who is the Tournament Director was mighty pleased with field. &ldquo;There is a great depth in the quality of the field and it is a high cut-off draw,&rdquo; said Sunil. &ldquo;It would be a treat for budding tennis players and sports lovers of Bengaluru&rdquo; he added.</p>\r\n<p><strong>The acceptances for Main Draw (Read as Player, Country, Rank)</strong></p>\r\n<p>Radu Albot (MDA, 89); Andrej Martin (SVK, 187); Maverick Banes (AUS, 240); Aleksandr Nedovyesov (KAZ, 242); Ricardas Berankis (LTU 131); Filip Peliwo (CAN), 188); Simone Bolelli (ITA, 147); Marc Polmans (AUS 145); Jay Clarke (GBR, 177); Mohamed Safwat (EGY, 205); Evgeny Donskoy (RUS, 99); Brayden Schnur (CAN, 222); Scott Griekspoor (NED, 223); Jordan Thompson (AUS 101), &nbsp;Prajnesh Gunneswaran (IND, 146); Marco Trungelliti (ARG, 119); Quentin Halys (FRA, 152); James Ward (GBR, 215); Cem Ilkel (TUR, 233); Elias Ymer (SWE, 133); Blaz Kavcic (SLO, 225).</p>', 'Admin', '2018-10-23', 'Bengaluru', 'India', '2018-10-24', '2018-11-24', '2018-10-24 00:06:55', '2018-10-24 05:07:01', 'A', '', '.', 'Strong field for Bengaluru Open 2018'),
(130, 'Suraj get wild card entry for Bengaluru ATP Challenger ', 'Home', 'S-P', 'A', 'uploadimage/Winner Suraj Prabodh49139.jpg', '<p><strong>Pic caption: Suraj Prabodh with the coveted signia of his Wild Card entry into the Bengaluru Open ATP Challenger event. Suraj defeated Mohit Mayur Jayaprakash in the final of the Bengaluru Open Wild Card AITA ranking tennis tournament at the KSLTA on Saturday.</strong></p>\r\n<p><strong>Bengaluru, Oct 27: </strong>Local lad and tournament favourite Suraj Prabodh earned a well-deserved wild card entry into the ensuing Bengaluru Open a USD 150,000 prize money ATP Challenger on the virtue of his victory in the Bengaluru Open Wild Card AITA ranking tennis tournament which concluded at the KSLTA here on Saturday.</p>\r\n<p>In a pulsating final, the top seed Suraj overcame a fighting fourth seed Mohit Mayur Jayaprakash of Tamil Nadu 7-6 (4), 4-6, 6-3 to take home the winner&rsquo;s cheque of Rs 39,000 while Mohit was richer by Rs 27,000. &nbsp;</p>\r\n<p>Suraj began in the right earnest brining in some cross court winners and took advantage of the unforced errors by Mohit to race to a 3-0 lead, breaking his opponent in the first and third games. Mohit, egged on by his friends from Chennai who were a part of a sizeable crowd that witnessed the final, adopted an aggressive approach in the fourth game while breaking Suraj&rsquo;s serve and holding on in the next to make a comeback. He broke again in the eighth game to level the scores. Both the players broke each other&rsquo;s serve in the next two games while Mohit held his serve in the 11<sup>th</sup> game to take the lead for the first time.&nbsp; With Suraj holding serve in the 12<sup>th</sup>, the set was decided via tie-breaker which the local favourite won 7-4.</p>\r\n<p>The match seemed to get more interesting in the second set second set seemed to grow more competitive with every point being fiercely fought before each of the players held their serve until the fifth game with Suraj breaking the serve in the sixth game to go 4-2 up. Mohit won three games in a row including two breaks in the 7<sup>th</sup> and 9<sup>th</sup> games to take the lead 5-4 and while also serving out to win the set 6-4.</p>\r\n<p>The decider too followed a similar suit until the sixth game where Suraj, with some good shots broke Mohit&rsquo;s serve to go 4-2 up and held on to take a 5-2 advantage. Serving to stay in the match, Mohit served with vigor to hold his serve. However, Suraj did not take much time to win the next game and the match.</p>\r\n<p><strong>&ldquo;</strong>It feels great to be on the winning side again. I think I have found the mojo I was looking for,&rdquo; said Suraj after the victory. &ldquo;It give a great sense of relief to get into the main round of the ATP Challenger,&rdquo; he added.</p>\r\n<p><strong>Men&rsquo;s Singles Final</strong></p>\r\n<p>1-Suraj Prabodh (KA) bt 4-Mohit Mayur Jayaprakash (TN) 7-6 (4), 4-6, 6-3.&nbsp;</p>', 'Admin', '2018-10-27', 'Bengaluru', 'India', '2018-10-29', '2018-11-29', '2018-10-29 09:28:00', '2018-10-29 14:28:05', 'A', '', '', 'Suraj get wild card entry for Bengaluru ATP Challenger '),
(131, 'Sumit begins campaign with an upset win at Bengaluru Open', 'Home', 's-b', 'A', 'uploadimage/Sumit89075.jpg', '<p><strong>Bengaluru Nov 12:</strong> &nbsp;On a day of upsets, Sumit Nagal, awarded a Wild Card, took the first step towards defending his title when in a rematch of last year&rsquo;s final, ousted seventh seed Jay Clarke in the opening round of the US $ 150,000 Bengaluru Open ATP Challenger at the KSLTA here on Monday. The 21-year-old took just about two hours to register a straight set 6-4, 7-5 victory amidst loud cheers from stands.</p>\r\n<p>&ldquo;I am happy to begin with a win. As I said earlier, I did not expect anything from the match and will take one match at a time,&rdquo; said the victor.</p>\r\n<p>&nbsp;In another upset, Turkey&rsquo;s Cem Ilkel outlasted second seed Marco Trungelliti of Argentina in 6-2, 3-6, 7-6 (3) in over two and a quarter hours. The only other seed to play today &ndash; fifth seed Marc Polmans of Australia was stretched by his countrymate Maverick Banes before overcoming 1-6, 6-4, 6-2.</p>\r\n<p>&nbsp;In the doubles, it he turn of the Indian duo of Arjun Kadhe and Saketh Myneni to send the favourites fourth seeded Ratiwatana brothers Sanchai and Sonchat Ratiwatana of Thailand 6-3, 3-6, 11-9 to enter the last eight stage.</p>\r\n<p>&nbsp;In the match played on centre court, it seemed as if defending champion Sumit Nagal took off from where he had left about a year ago. The match between Sumit and Clarke promised fireworks from the word go with both the players trading shots in a fast paced game. Sumit, in particular was visibly aggressive in his approach. After both the players had held their respective serves for the first eight games, Sumit broke Clarke&rsquo;s serve in the ninth to go 5-4 up and served out for the set.</p>\r\n<p>Maintaining the same tempo in the second set, Sumit put his forehand to good use and virtually had Clarke on the wrong foot on many occasions and earned an early break. However, he could not maintain the tempo, losing the advantage, thanks to some unforced errors. Parity was restored in the sixth game. Both players then held on to their serves until Sumit broke the Englishman in the 11<sup>th</sup> game and held his serve in the next to win the match.</p>\r\n<p>&ldquo;I think it was a better match last year with both of us in good shape,&rdquo; admitted Sumit.</p>\r\n<p>Meanwhile, India&rsquo;s Mukund Sasi Kumar breezed past Frenchman Mick Lescure 6-1, 6-1 to make it to the main draw. The other Indian Siddharth Vishwakarma went down to Youssef Hossam of Egypt 4-6, 4-6 in the final qualifying round.</p>\r\n<p><strong>Results (Round-1) (Prefix denoted seeding, parenthesis denotes country)</strong></p>\r\n<p><strong>Men&rsquo;s Singles</strong></p>\r\n<p>5- Marc Polmans (Aus) bt Maverick Banes (AUS) 1-6, 6-4, 6-2;</p>\r\n<p>W-Sumit Nagal bt 7-Jay Clarke 6-4, 7-5</p>\r\n<p>Aleksandr Nedovyesov (KAZ) bt Scot Griekspoor (NED) 6-3, 6-2;</p>\r\n<p>Cem Ilkel (TUR) bt 2-Marco Trungelliti (ARG) 6-2, 3-6, 7-6 (3)</p>\r\n<p>Tsung-Hua Yang (TPE) Vs. James Ward (GBR)</p>\r\n<p><strong>&nbsp;</strong></p>\r\n<p><strong>Men&rsquo;s Doubles</strong></p>\r\n<p>W-SD Prajwal Dev (IND) / Niki Poonacha (IND) bt Alexander Pavlioutchenkov (RUS) / Filip Peliwo (CAN) 1-6, 7-6 (3), 10-2</p>\r\n<p>Max Purcell (AUS) / Luke Saville (AUS) bt Chandril Sood (IND) / Lakshit Sood (IND) 6-2, 6-2.</p>\r\n<p>Arjun Kadhe (IND) / Saketh Myneni (IND) bt 4-Sanchai Ratiwatana (THA) / Sonchat Ratiwatana (THA) 6-3, 3-6, 11-9</p>\r\n<p>Blaz Kavcic (SLO) / Tristan-Samuei Weissborn (AUT) bt Radu Albot (MDA) / Marc Polmans (AUS) 6-4, 7-5</p>\r\n<p><strong>&nbsp;</strong></p>\r\n<p><strong>Qualifiers Round -3</strong></p>\r\n<p>Youssef Hossam (EGY) bt W-Siddharth Vishwakarma (IND) 6-4, 6-4;</p>\r\n<p>7-Mukund Sasi Kumar (IND) bt Mick Lescure (FRA) 6-1, 6-1</p>\r\n<p>4-Piros Zsombor (HUN) bt 8-Zizou Bergs (BEL) 6-7 (6), 7-5, 6-3</p>\r\n<p>Sebastian Fanselow (GER) bt Khumoyun Sultanov (UZB) 5-7, 6-4, 6-1&nbsp;</p>', 'Admin', '2018-11-12', 'Bengaluru', 'India', '2018-11-13', '2018-12-13', '2018-11-13 10:47:12', '2018-11-13 16:47:19', 'A', '', 'Admin', 'Sumit begins campaign with an upset win at Bengaluru Open'),
(132, 'Top seeds bite the dust at Bengaluru Open', 'Home', 't-s', 'A', 'uploadimage/Hossam12362.jpg', '<p><strong>Bengaluru Nov 13: </strong>Coming from a country which does not boast much of a tennis legacy, Egyptian Youssef Hossam created the biggest flutter of the tournament when he toppled the top seed Radu Albot of Moldova, ranked 383 ranks above him, in the first round of the US $ 150,000 Bengaluru Open ATP Challenger being played at the KSLTA here on Tuesday. The 20-year-old, who came through three tough qualifying rounds, was a 2-6, 6-2, 6-2 winner.</p>\r\n<p>&ldquo;I was both nervous and confident. Nervous because I was playing the top seed and was cursing the draw and confident because I did not have anything to lose,&rdquo; said Hossam after the victory.</p>\r\n<p>In a minor upset, Slovenian Blaz Kavcic who was seeded No.1 in the last edition but unseeded this year, breezed past eighth seed Filip Peliwo of Canada 6-2, 6-0.&nbsp; Amongst the Indians, fourth seed Prajnesh Gunneswaran wasted no time in brushing aside the challenge of Russian Ivan Nedelko with a 6-2, 6-2 victory while Wild Card entrant Saketh Myneni beat fellow Wild Card Adil Kalyanpur 6-3, 7-6(3). Qualifier Sasi Kumar Mukund also advanced to the last 16 stage defeating American Collin Altamirano 7-6 (6), 6-3. The last Indian in the main draw and wild card entrant Suraj Prabodh went down tamely to Frenchman Quentin Halys 3-6, 1-6.</p>\r\n<p>The doubles matches also saw the top seeded Indo-German pair of Jeevan Nedunchezhiyan &nbsp;and Kevin Krawietz crash out of the tournament losing to the duo of Slovakian Andrej Martin and Chilean Hans Podlipnik-Castillo 3-6, 6-3, 7-10.</p>\r\n<p>&nbsp;Apart from a ITF Futures title he won in Portugal in August this year, &nbsp;Hossam, has struggled to get past the second round on the Challenger Tour. However, today he was a different man on the court. After a nervous start, where he committed many unforced errors and lost his serve in the first and third games to be 0-4 down. Before he could stabilize himself, he had lost the first set, although without losing his serve, by which, he grew in confidence that reflected in the next two sets.</p>\r\n<p>&nbsp;&ldquo;I stopped committing errors and out the ball on the court which kind of unsettled my opponent,&rdquo; said the winner who didn&rsquo;t lose his serve while achieving breaks in the fourth and eight game to garner the second set. In the decider, Hossam seemed to lose the grip for a brief while when he lost his serve in the third game but came back brilliantly to win the next five games on a trot, breaking his rival&rsquo;s serve in the 4<sup>th</sup>, 6<sup>th</sup> and 8<sup>th</sup> games to win the set and the match. &ldquo;In big matches, there are times when the underdogs perform well and today was such a day,&rdquo; said the 29-year-old top seed who had reached the third round of the US Open last year.</p>\r\n<p>&nbsp;Prajnesh Gunneswaran meanwhile who has had a good run in 2018, achieving a career high ranking of 141 last week, with a Challenger title and a loss in the final in China last month, put his forehand to good use against the Russian Nedelko. With two breaks in the 4<sup>th</sup> and 8<sup>th</sup> game, the Indian pocketed the first set 6-2 and in a fast paced game, broke Nedelko&rsquo;s serve in the 6<sup>th</sup> and 8<sup>th</sup> games to advance to the next round.</p>\r\n<p>&nbsp;<strong>Results (Round-1) (Prefix denoted seeding, parenthesis denotes country)</strong></p>\r\n<p>Q-Youssef Hossam (EGY)&nbsp;bt 1-Radu Albot (MDA) 2-6, 6-2, 6-2; <br /> W-Saketh Myneni (IND) bt W-Adil Kalyanpur (IND) 6-3, 7-6(3);</p>\r\n<p>Blaz Kavcic (SLO) bt 8-Filip Peliwo (CAN) 6-2, 6-0;<br /> Q-Sasi Kumar Mukund (IND) bt Collin Altamirano (USA) 7-6 (6), 6-3;&nbsp;<br /> Q-Sebastian Fanselow (GER) bt Q-Zzombor Piros (HUN) 7-6 (3), 6-4;&nbsp;<br /> 4-Prajnesh Gunneswaran (IND) bt Ivan Nedelko (RUS) 6-2, 6-2;&nbsp;<br /> 6-Quentin Halys (FRA) bt W-Suraj Prabodh (IND) 6-3, 6-1;</p>\r\n<p>Brayden Schnur (CAN) bt Max Purcell (AUS) 6-4, 7-6 (3);</p>\r\n<p>LL-Zizou Bergs bt Andrej Martin (SVK) 6-1, 6-2;&nbsp;<br /> Frederico Ferreira Silva (POR) bt Danilo Petrovic (SRB) 6-3, 3-6, 6-4</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Doubles</strong></p>\r\n<p>Andrej Martin (SVK) / Hans Podlipnik-Castillo (CHI)&nbsp;bt 1-Kevin Krawietz (GER) / Jeevan Nedunchezhiyan (IND) 6-3, 3-6, 10-7;</p>\r\n<p>Cheng-Peng Hsieh (TPE) / Tsung-Hua Yang (TPE) bt Aleksandr Nedovyesov (KAZ) / Marco Trungelliti (ARG) 6-1, 3-6, 10-3;</p>\r\n<p>2-Purav Raja (IND) / Antonio Sancic (CRO) bt Romain Arneodo (MON) / Quentin Halys (FRA) 6-4, 6-7 (6), 11-9;</p>\r\n<p>3-N Sriram Balaji (IND) / Vishnu Vardhan (IND) bt W-Sumit Nagal (IND) / Brayden Schnur (CAN) 6-4, 6-3&nbsp;</p>', 'Admin', '2018-11-13', 'Bengaluru', 'India', '2018-11-13', '2018-12-13', '2018-11-13 10:49:15', '2018-11-13 16:49:41', 'A', '', 'admin', 'Top seeds bite the dust at Bengaluru Open'),
(133, 'Two more seeds bite the dust; 4 Indians in last eight of Bengaluru Open', 'Home', 't-m', 'A', 'uploadimage/Saketh Myneni98159.jpg', '<p><strong>Bengaluru, Nov 14:</strong> As many as four Indians figure in the last eight stage of the ongoing US $150,000 Bengaluru Open ATP Challenger. In the pre-quarterfinal fixture played at the KSLTA here on Wednesday, Sasi Kumar Mukund, playing his fourth round in as many days (entered as a qualifier), sent home the fancied Slovenian, Blaz Kavcic after the latter threw in the towel, trailing by a set and down 1-3 in the second set.</p>\r\n<p>&nbsp;Defending champion Sumit Nagal was a 6-3, 7-6 (4) victor over James Ward of Great Britain while Saketh Myneni quelled the challenge of giant killer Youssef Hossam 6-1, 3-6, 6-1 to advance to quarterfinals. Joining the trio was fourth seed Prajnesh Gunneswaran who came back from the brink of defeat against qualifier Sebastian Fanswelow of Germany before prevailing 4-6, 6-4, 7-5. Incidentally, Prajnesh is the only seed left in fray. Brayden Schnur of Canada sidelined sixth seed Quentin Halys of France 6-4, 7-6 (3) while Kazak Aleksandr Nedovyesov ousted fifth seed Marc Polsman of Canada in straight sets 6-2, 6-4</p>\r\n<p>&nbsp;In an interesting duel, after both the players had traded a set each with a similar score-line of 6-4 with Sebastian winning the first, Prajnesh, was trailing 3-5 with Sebastian just a match point away from victory. The 26-year-old Indian who is currently ranked 144<sup>th</sup>, did not just come back but went on to break his serve and won the next three games on a trot with a break in the 11<sup>th</sup> game and served out to win the set 7-5 and thus the match.</p>\r\n<p>&nbsp;Meanwhile, Sasi Kumar Mukund may be just 21 year old but uses sagely words while talking about his game and life in general, thanks to the lessons learnt from the arduous life on the Tour. The Chennai born, Hyderabad-based lad did not have any plans against a better ranked opponent. After both the players held their respective serves in the first five games, it was Kavcic who took advantage of an error-prone Mukund to achieve the first break in the sixth game and went 4-2 up. However, with his knee giving a problem, Kavcic lost his serve in the ninth game, the set was decided on a tie-break which Mukund pocketed 7-2 and even broke his rival&rsquo;s serve in the first game of the next set and retired after four games.</p>\r\n<p>&nbsp;Mukund who took to tennis at a very early age thought of making tennis his career when he was barely eight, was hitherto playing on the Futures Tour and achieved a reasonable success last year. However, training with his new coach in Austria, gave a new outlook to his game as he graduated to Challenger Tour this year. &ldquo;Rankings do not matter if you are not in the top 150 on ATP. The sooner you start aiming towards that, the better and hence the decision to play on the Challenger Tour,&rdquo; said the soft spoken player. Talking about his preparation for the tougher Tour, Mukund said that he was working on two aspects. &ldquo;Firstly, we have to prepare mentally and physically strong to cope up with the so called lonely life on the Tour and then work on the various aspects of the game,&rdquo; said Mukund who clashes with Prajnesh in the quarterfinal.</p>\r\n<p>&nbsp;<strong>Results (Pre-quarterfinals) (Prefix denoted seeding, parenthesis denotes country)</strong></p>\r\n<p>Q-Sasi Kumar Mukund (IND) bt Blaz Kavcic (SLO) 7-6 (2), 3-1 (Retd);</p>\r\n<p>WC-Sumit Nagal (IND) bt James Ward (GBR) 6-3, 7-6 (4);</p>\r\n<p>4-Prajnesh Gunneswaran (IND) bt Q-Sebastian Fanselow (GER) 4-6, 6-4, 7-5;</p>\r\n<p>WC-Saketh Myneni (IND) bt Q-Youssef Hossam (EGY)&nbsp;6-1, 3-6, 6-1;</p>\r\n<p>Brayden Schnur (CAN) bt 6-Quentin Halys (FRA) 6-4, 7-6 (3);</p>\r\n<p>Aleksandr Nedovyesov (KAZ) bt 5-Marc Polsman (CAN) 6-2, 6-4</p>\r\n<p>Cem Ilkel (TUR) bt LL-Zizou Bergs (BEL) 7-5, 6-3</p>\r\n<p>Daniel Masur (Ger) Vs. Frederico Ferreira Silva (POR)</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Doubles (Quarterfinals)</strong></p>\r\n<p>3-N Sriram Balaji (IND) / Vishnu Vardhan (IND) Vs. Max Purcell (AUS) / Luke Seville (AUS)</p>\r\n<p>Arjun Kadhe (IND) Vs. Saketh Myneni (IND) bt WC-SD Prajwal Dev (IND) / Niki Poonacha (IND) 6-3, 7-6 (5);</p>\r\n<p>Cheng-Peng Hsieh (TPE) / Tsung-Hua Yang (TPE) bt Andrej Martin (SVK) / Hans Podlipnik-Castillo (CHI) 4-6, 6-2, 10-2;</p>\r\n<p>2-Purav Raja (IND) / Antonio Sancic (CRO) W/O Blaz Kavcic (SLO) / Tristan-Samuel Weissborn</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Quarterfinal line-up (Singles) 2 p.m. onwards</strong></p>\r\n<p>Q-Sasi Kumar Mukund (IND) Vs. 4-Prajnesh Gunneswaran (IND)</p>\r\n<p>WC-Saketh Myneni (IND) Vs. WC-Sumit Nagal (IND)</p>\r\n<p>Brayden Schnur (CAN) Vs. Cem Ilkel (TUR)</p>\r\n<p>Aleksandr Nedovyesov (KAZ) Vs. D. Masur (GER) or F. Ferreira Silva (POR)</p>\r\n<p>&nbsp;</p>', 'Admin', '2018-11-14', 'Bengaluru', 'India', '2018-11-15', '2018-12-15', '2018-11-15 23:53:20', '2018-11-16 05:53:28', 'A', '', '', 'Two more seeds bite the dust; 4 Indians in last eight of Bengaluru Open'),
(134, 'Prajnesh, Saketh reach semifinals at Bengaluru Open ATP Challenger   ', 'Home', 'p-s', 'A', 'uploadimage/Saketh Myneni116232.jpg', '<p><strong>Bengaluru, Nov 15:</strong> Saketh Myneni, a Wild Card entrant disposed off defending champion Sumit Nagal&rsquo;s challenge in straight sets 6-4, 6&mdash;4 to reach the semifinals of the US $ 150,000 + H Bengaluru Open ATP Challenger at the KSLTA here on Thursday. Joining him was fourth seed Prajnesh Gunneswaran who did not have to break a sweat as his hard working opponent Sasi Kumar Mukund had to withdraw owing to a severe back pain.</p>\r\n<p>&nbsp;Saketh will clash with Aleksandr Nedovyesov of Kazakhstan who was a 6-4, 6-2 winner over Frederico Ferreira Silva of Portugal in the first semifinal while Prajnesh will meet Canadian Brayden Schnur in the other semifinal. Schnur defeated Cem Ilkel of Turkey 6-3, 6-4.</p>\r\n<p>&nbsp;Meanwhile, the second seeded Indo-Croatian pair of Purav Raja and Antonio Sancic were stretched by the Indian pair of Arjun Kadhe and Saketh Myneni before prevailing 3-6, 6-2, 10-8. The duo will meet the Australian pair of Max Purcell and Luke Seville in the final.</p>\r\n<p>&nbsp;Mukund who served only thrice, but without the regular jump, lost all three points before pulling out writhing in pain. &ldquo;It was at the fag end of my warm up that the pain showed up from somewhere and I could not walk anymore with pain shooting in my lower back,&rdquo; said Mukund who had displayed a good performance thus far. &ldquo;I feel very bad tor the school kids who came to watch with enthusiasm and it had to end like this. Although it is not my fault but I still will not be able to forgive myself for that,&rdquo; said the 21 year old.</p>\r\n<p>&nbsp;In the Saketh-Sumit match, the former tested his experience while Sumit was as aggressive as ever. Playing long and deep shots, the 31-year-old Saketh seemed to conserve and, at times, caught his opponent on the wrong foot. With a break in the very first game, Saketh who is currently ranked 213, took the first set 6-4 as both of them held on to their respective serves.</p>\r\n<p>&nbsp;The second set saw both the rivals swerving on their serves, owing to an extra bit of a wind. The set took a different pattern from the fifth game, where the Delhiite, with two game points up, lost his serve. The serves were broken for the next four games as well. Sumit lost his serve in the 7<sup>th</sup> and 9<sup>th</sup> games because of double faults while Saketh was broken in the 6<sup>th</sup> and 8<sup>th</sup> games, thanks to unforced errors. Finally, Saketh kept the ball in play when it mattered and closed out the set and match 6-4, 6-4.</p>\r\n<p>&nbsp;&ldquo;I know that he (Sumit) is an aggressive player. I know him off the court as well. And considering the conditions, I was serving well for the first set and half and then things changed a bit. However, I was very much in control,&rdquo; said Saketh. &nbsp;&ldquo;It is unfortunate that two Indians played in the same match. I hate this when this happens as we should be playing against the world and not each other,&rdquo; said Saketh. &ldquo;I am not thinking of any plans for the future. I would go week by week and this week is priority for me as I need to gauge my fitness levels before I decide on playing.&rdquo;</p>\r\n<p>&nbsp;Commenting on the depth of talent in Indian tennis, Saketh said: &ldquo;It is great that four Indians had reached the quarterfinal stage of the one of the biggest ATP Challengers. We have the backing of home crowd and a chance to earn so many points. We should have more of these.&rdquo;</p>\r\n<p>&nbsp;<strong>Results (Quarterfinals) (Prefix denoted seeding, parenthesis denotes country)</strong></p>\r\n<p><strong>Singles Quarterfinals 2 p.m. onwards</strong></p>\r\n<p>4-Prajnesh Gunneswaran (IND) W/O Q-Sasi Kumar Mukund (IND)</p>\r\n<p>WC-Saketh Myneni (IND) bt WC-Sumit Nagal (IND) 6-4, 6-4;</p>\r\n<p>Aleksandr Nedovyesov (KAZ) bt Frederico Ferreira Silva (POR) 6-4, 6-2;</p>\r\n<p>Brayden Schnur (CAN) bt Cem Ilkel (TUR) 6-3, 6-4</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Doubles (Semifinals)</strong></p>\r\n<p>Max Purcell (AUS) / Luke Seville (AUS) bt Cheng-Peng Hsieh (TPE) / Tsung-Hua Yang (TPE) 6-3, 6-4;</p>\r\n<p>2- Purav Raja (IND) / Antonio Sancic (CRO) bt Arjun Kadhe (IND) / Saketh Myneni (IND) 3-6, 6-2, 10-8.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Tomorrow&rsquo;s matches</strong></p>\r\n<p><strong>Men&rsquo;s Singles Semifinals</strong></p>\r\n<p>4-Prajnesh Gunneswaran (IND) Vs. Brayden Schnur (CAN)</p>\r\n<p>WC-Saketh Myneni (IND) Vs. Aleksandr Nedovyesov (KAZ)</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Men&rsquo;s Doubles Finals</strong></p>\r\n<p>Max Purcell (AUS) / Luke Seville (AUS) Vs. 2- Purav Raja (IND) / Antonio Sancic (CRO)</p>', 'Admin', '2018-11-15', 'Bengaluru', 'India', '2018-11-15', '2018-12-15', '2018-11-15 23:56:16', '2018-11-16 05:56:23', 'A', '', 'Admin', 'Prajnesh, Saketh reach semifinals at Bengaluru Open ATP Challenger   '),
(135, 'ATP players impart tips to juniors', 'Home', 'A-p', 'A', 'uploadimage/Clinic99843.jpg', '<p><strong>Bengaluru, Nov 15:</strong> Some of the top players playing on the ATP Challenger Circuit gave some helpful tips to the budding youngsters at a tennis clinic organized by the KSLTA on the sidelines of the US $ 150,000 Bengaluru Open ATP Challenger being played at the KSLTA here on Thursday.</p>\r\n<p>Over 50 kids aged between 12 and 18 participated in the clinic and went over the various aspects of the game including mental exercises. India&rsquo;s leading professional and seeded fourth for the event, Prajnesh Gunneswaran, top ranked doubles player Vishnu Vardhan were amongst the players who imparted the lessons.</p>\r\n<p>&nbsp;&ldquo;It always feel better when you contribute to the game that has given me so much. I am excited with the enthusiasm shown by these youngsters,&rdquo; said Prajnesh.</p>', 'Admin', '2018-11-16', 'Bengaluru', 'India', '2018-11-16', '2018-12-16', '2018-11-16 00:06:09', '2018-11-16 06:06:17', 'A', '', 'Admin', 'ATP players impart tips to juniors'),
(136, 'All Indian final at Bengaluru Open ATP Challenger', 'Home', 'a-i', 'A', 'uploadimage/Doubles winers53046.jpg', '<p>The home crowd could not have asked for a better finish to the US $ 150,000 Bengaluru Open ATP Challenger than an all Indian final. In contrasting semifinals played at the KSLTA here on Friday, Saketh Myneni came back from a set down to win against Khazak Aleksandr Nedovyesov 4-6, 6-4, 6-4 while fourth seed Prajnesh Gunneswaran brushed aside the challenge of Brayden Schnur 6-4, 6-1. Incidentally it is only the second time in the Challenger history that two Indians will be figuring in a final.</p>\r\n<p>&nbsp;The Australian duo of Max Purcell and Luke Seville upset the second seeds Purav Raja of India and Antonio Sancic of Crotia 7-6 (3), 6-3 to lift the doubles crown. Incidentally, this is the fourth finals for Purav in six final appearances, winning the other two.</p>\r\n<p>&nbsp;In a match that lasted for a little over two hours, both the players began on an aggressive note and held on to their serves until the fourth game before Aleksandr achieved the all-important break in the fifth game of the first set, thanks to unforced errors on the part of Saketh.</p>\r\n<p>&nbsp;The games got longer in the second set and so did the errors by both the players. Aleksandr who had reached a career best ranking of 72, four seasons ago, saved two break points in the second game and went on to break his opponent&rsquo;s serve in the third to gain a 2-1 advantage. However, once again unforced errors caused him to lose his serve in the sixth game when the scores were level. This proved to be the turning point for Saketh, who, egged on by the crowd, upped his game and played some good cross court winners. &ldquo;The crowd here was awesome and I owe my victory to them. I have never done &ldquo;come-on&rdquo; and that kind of stuff. But today, I did as I think the crowd deserved it,&rdquo; admitted the lanky player who broke Alekdandr&rsquo;s serve in the 10<sup>th</sup> game to take the set 6-4.</p>\r\n<p>&nbsp;The decider was seeming to be a formality with the Indian firing on all guns and racing to a 5-0 lead with breaks in the first and third games. Things started to take a turn as 31-year-old Kazhak staged a fightback and held his serve in the sixth. Serving for the match, Saketh double faulted four times in three games and a couple of silly errors at the net saw him losing his serve in the 7<sup>th</sup> and 9<sup>th</sup> games as the lead narrowed down to 5-4. &ldquo;I lost my concentration for a brief while. These kind of mistakes should not happen but then I am happy that all went well for me,&rdquo; said Saketh who sent down six aces during the match. It was the turn of the Aleksandr who had found his mojo late in the third set, to commit errors as he hit into the net giving Saketh an entry into the final. &ldquo;I have recovered considerably after an injury-prone season where I could not play for eight months. I still have a match to go and hope to keep it this way,&rdquo; said the winner.</p>\r\n<p>&nbsp;In a power packed second semifinal which lasted for a mere 57 minutes, saw Prajesh in the best of his form which has been phenomenal in the last couple of months. The left-handed displayed some superb tennis shots, especially his low forehand returns. After achieving a break in the very first game, he kept the pressure on and cornered the first set at 6-4. In the second, the 29-yearold began in a similar fashion in the second set and broke his rival&rsquo;s serve once again in the fifth and seventh games to clinch the match. &ldquo;I am happy to be in the final. I think today was the best that I have played this week,&rdquo; said Gunneswaran who by the virtue of his win today will be crowned as India No.1. &ldquo;I am delighted to know that I am no.1. I have been waiting for a while to reach this spot. Although it has still not sunk in, I feel very happy,&rdquo; said the Chennai-born who was instrumental in India&rsquo;s victory against China in the Davis Cup this year. &ldquo;Saketh is not a conventional player and has his own style of game which is difficult to predict. But I think it will be a good match,&rdquo; said the finalist.</p>\r\n<p><strong>Results (Semifinals) (Prefix denoted seeding, parenthesis denotes country)</strong></p>\r\n<p>WC-Saketh Myneni (IND) bt Aleksandr Nedovyesov (KAZ) 4-6, 6-4, 6-4</p>\r\n<p>4-Prajnesh Gunneswaran (IND) bt Brayden Schnur (CAN) 6-4, 6-1</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Doubles (Finals)</strong></p>\r\n<p>Max Purcell (AUS) / Luke Seville (AUS) bt 2- Purav Raja (IND) / Antonio Sancic (CRO) 7-6 (3), 6-3</p>\r\n<p>&nbsp;</p>\r\n<p>Final line-up</p>\r\n<p>WC-Saketh Myneni (IND) Vs.&nbsp;4-Prajnesh Gunneswaran (IND) 6 p.m.</p>', 'Admin', '2018-11-16', 'Bengaluru', 'India', '2018-11-21', '2018-12-21', '2018-11-21 06:00:20', '2018-11-21 12:00:27', 'A', '', 'Admin', 'All Indian final at Bengaluru Open ATP Challenger'),
(137, 'Prajnesh wears the crown at Bengaluru Open', 'Home', 'P-w', 'A', 'uploadimage/Winner74820.jpg', '<p>It seemed as if all the roads led to Karnataka State Lawn Tennis Association as the crowd thronged to the venue of the final of the US $ 150,000 Bengaluru Open ATP Challenger, hoping for a humdinger of a match. Fireworks did flow but only from the racquet of fourth seed Prajnesh Gunneswaran who took just 56 minutes to brush aside the challenge of Wild Card entrant Saketh Myneni in straight sets 6-2, 6-2 to wear the crown here on Saturday.</p>\r\n<p>&ldquo;I am happy to have won here. I think I played good tennis through the week and the hard work has paid off,&rdquo; said an elated Prajnesh who garnered 125 ATP points as well as the winners cheque of US $ 21,600 and could see a jump of over 30 ranks and if he continues in the same vein in Pune, he could well be in for the first Grand Slam of the Year.</p>\r\n<p>In an elaborate prize presentation held amidst a cheerful crowd, the Deputy Chief Minister of Karnataka Dr. G Parameshwara gave away the prizes. Priyank Kharge, the Minister for Social Welfare who is also the organizing committee chairman was also present during the ceremony.</p>\r\n<p>Going in the match with a break in the very first game, Prajwal grew in confidence as the game progressed and never looked in any kind of trouble but for a few unforced errors which were more in number by his opponent. While Prajnesh was consistent, Saketh, for his part, showed flashes of brilliance but committed too many errors which forced him to concede his serve in the seventh game after which Prajnesh served out for the set. &ldquo;I began on a good note and built on it. I must congratulate Saketh for reaching the finals after an injury prone season.</p>\r\n<p>Beginning the second set in a similar fashion, Prajnesh seemed to be in complete control and his opponent complimented him with his share of errors. &ldquo;I did too many mistakes at the wrong time,&rdquo; admitted Saketh who on the virtue of reaching the final has got a special entry for the upcoming Pune Challenger.</p>\r\n<p>One more break in the fifth game, despite two good shots from Saketh, gave Prajnesh a route to for a a firm finish in closing out the match by the second set itself. &ldquo;Going into the game, I had expected a good match. However, the early break in both the sets helped me a lot,&rdquo; said Prajnesh who is hoping to cap the season with a good finish in Pune next week.</p>\r\n<p><strong>Results</strong></p>\r\n<p><strong>Men&rsquo;s singles finals</strong></p>\r\n<p>4-Prajnesh Gunneswaran (IND) bt W-Saketh Myneni 6-2, 6-2 &nbsp;&nbsp;&nbsp;</p>', 'Admin', '2018-11-17', 'Bengaluru', 'India', '2018-11-21', '2018-12-21', '2018-11-21 06:02:00', '2018-11-21 12:02:08', 'A', '', 'Admin', 'Prajnesh wears the crown at Bengaluru Open'),
(138, 'Karnataka emerged as the champion in the National Inter-State women’s tennis championship', 'Home', 'i-s', 'A', 'uploadimage/217281.jpg', '<p><strong>Karnataka emerged as the champion in the National Inter-State women&rsquo;s tennis championship beating Madhya Pradesh 2-0 in the final on Friday at Bhilai.</strong></p>\r\n<p><strong>&nbsp;Soha Sadiq of Karnataka defeated Rashi 6-0, 6-2 in the opening singles and later, skipper Prathibha Prasad sealed the issue for Karnataka beating&nbsp; Sara Yadav 7-5, 6-2. Earlier in the semi-finals, Karnataka beat Telangana 2-0</strong></p>\r\n<p><strong>Mr. P.R. Ramaswamy, Jt secretary of the KSLTA congratulated the team and announced Rs . 1 Lakh cash award to each of the four players of the team.</strong></p>\r\n<p><strong>&nbsp;The results ( final ) :</strong></p>\r\n<p><strong>&nbsp;Karnataka beat MP 2-0 ( Soha Sadiq bt &nbsp;Rashi 6-0, 6-2; Prathibha Prasad bt&nbsp; Sara Yadav 7-5, 6-2 ).</strong></p>', 'Admin', '2018-12-28', 'Bengaluru', 'India', '2018-12-28', '2019-01-29', '2018-12-28 04:35:02', '2018-12-29 13:24:45', 'A', '', 'Admin', 'Karnataka emerged as the champion in the National Inter-State women’s tennis championship'),
(139, 'Ronin Rohit Lotlikar of Karnataka is the winner of  Singles Boys U - 16 super series', 'Home', 'R-R', 'A', 'uploadimage/165826.jpg', '<p>KSLTA congratulate both the players for their achievement and Wish both of them Good luck and also Wish them to achieve more laurels in their future endeavours.</p>', 'Admin', '2019-04-07', 'Bengaluru', 'India', '2019-04-08', '2019-05-08', '2019-04-08 07:00:36', '2019-04-08 12:00:48', 'A', '', '', 'Ronin Rohit Lotlikar of Karnataka is the winner of  Singles Boys U - 16 super series'),
(140, 'Vaidehi Chaudhari wins Asian ranking women’s tennis title', 'Home', 'v-c', 'A', 'uploadimage/Soha86519.jpg', '<p>Wild card entrant Vaidehi Chaudhari outplayed Soha Sadiq in the final of the $3,000 FinIQ Asian ranking women&rsquo;s tennis tournament organised by Navnath Shete Sports Academy at the Metrocity Courts in Pune on Sunday.</p>\r\n<p>The champion collected 40 ranking points and Rs.31,500. The runner-up gained 25 points and Rs.21,000.</p>\r\n<p>A trainee of coach Jignesh Raval in Ahmedabad, the 19-year-old Vaidehi had pulled through a tough three-setter against top seed Sai Samhitha in the semifinals. Jignesh and another leading coach in Ahmedabad, Shrimal Bhat, along with Alexander Tennis University have combined to provide the best of expertise to the trainees.</p>\r\n<p>The results (final): Vaidehi Chaudhari bt Soha Sadiq 6-2, 6-1.</p>', 'Admin', '2019-05-06', 'Bengaluru', 'India', '2019-05-10', '2019-06-10', '2019-05-10 02:04:45', '2019-05-10 07:04:51', 'A', '', 'Vaidehi Chaudhari wins Asian ranking women’s tennis title', 'Admin');
INSERT INTO `ksl_news_tbl` (`news_id`, `news_title`, `news_display_tag`, `news_link_address`, `news_link_status`, `news_content_image`, `news_editor_content`, `news_posted_by`, `news_posted_at`, `news_located_city`, `news_located_country`, `news_from_date`, `news_to_date`, `news_created_date`, `news_lastupdated_date`, `news_flat`, `news_tournament_id`, `news_meta_name`, `news_meta_content`) VALUES
(141, 'Good Footwork most crucial for success on tennis court, says Former World No.1 Arantxa Sanchez Vicario ', 'Home', 'T-C', 'A', 'uploadimage/RPG_405760719.jpg', '<p>He International Event Ambassador for TCS World 10K 2019 shared inspiring stories with young tennis players in the city at the Tennis Clinic organised by Procam International supported by KSLTA and sports goods partner Asics&nbsp; &nbsp;<br />&nbsp;<br /><strong>Bengaluru, 18th May 2019:</strong>&nbsp; About 37 aspiring young tennis stars from Bengaluru who feature in the top list of AITA rankings had a once-in-a-lifetime opportunity when they served and volleyed with the 14-time Grand Slam Champion Arantxa Sanchez Vicario, International Event Ambassador for TCS World 10k on Saturday morning here at the Karnataka State Lawn Tennis Association (KSLTA). <br />&nbsp;<br />The Tennis Clinic organised by Procam International supported by KSLTA and sports goods Partner for TCS World 10K -- Asics, had budding tennis players, coaches and parents mesmerized as an exuberant Sanchez Vicario held her sway on centre court at the KSLTA where she played nonstop, with the same teenage vigor, for an hour alongside kids varying from as young as six-years-old to 18 years of age. She also played against India\'s No.1 Wheelchair tennis player Shekar Veeraswamy at the Clinic that lasted over 90 minutes. <br />&nbsp;<br />Among the talented set of players, it was six-year-old Srishti Kiran, of Baldwin Girls School in the city, who surprised the tennis ace with her powerful shots and ability to hit back to Sanchez who tested each player\'s skill, and ability to remain agile and swift on the court. \"I see these kids put in a lot of passion into their game but when I shifted from hitting shots to them and made them move around, they seemed a bit lazy on their footwork,\" Sanchez said without mincing words. \"You must move lateral to the ball than forward towards the ball. Don\'t wait for the ball to come to you, but you must go after the ball and always try to do what your opponent does not like -- something as simple as hitting the ball away from them,\" she adviced the kids. <br />&nbsp;<br />Former World No.1 Sanchez, loved world-wide for her stern on-court disposition, often showcasing that slugger\'s mentality on the baseline, she always sent out a message to her opponents with each stinging forehand shot. So when a player asked her about her mental makeup in the grueling 1995 Wimbledon Final where Sanchez was up against Steffi Graf in the final, she said, \"It is important to show your grit and determination against your opponent. You need to work for every point. In tennis, even if you had a bad start, you can always make up for it. You must never give up and always stay positive. It is only a matter of winning one ball and losing the other. I gave my 100 per cent in that Final and never gave up.\" <br />&nbsp;<br />Sanchez not only had advice for the kids but she also told the parents to be more patient with their children. \"I had my parents drive me around for matches and I can imagine it is a tough life for parents because you put in equal effort and money but never put pressure on your kids and be patient with them,\" she said. <br />&nbsp;<br />Speaking about having Arantxa Sanchez Vicario at KSLTA, P R Ramaswamy, Hon.Jt.Secretary said, \"It was a great pleasure for us to have Arantxa Sanchez Vicario at KSLTA today. It was a once-in-a-lifetime opportunity for these budding tennis players to interact and play with her one-on-one. She was articulate in expressing to the kids about the temperament required to play at the highest level and how they handle pressure. I am sure these lessons will stay with them for a long time. We at KSLTA thank Procam International and Arantxa Sanchez for making this happen and we wish her the very best.\"</p>', 'Admin', '2019-05-18', 'Bengaluru', 'India', '2019-05-18', '2019-06-18', '2019-05-18 00:43:50', '2019-05-18 09:06:10', 'A', '', 'Admin', 'Tennis Clinic with Arantxa Sanchez Vicario'),
(142, 'Skandha Prasanna Rao of Karnataka is a finalist in both singles and doubles in AITA National series for U14', 'Home', 'k-f', 'A', 'uploadimage/SKANDHA92732.jpg', '<p>Skandha Prasanna Rao of Karnataka is a finalist in both singles and doubles in AITA National series for U14 which was conducted in Panchgani, Maharashtra from 13 to18 may 2019 and he was also finalist in the U14 Asian Tournament which happened in Bangalore in the month of January.</p>', 'Admin', '2019-05-20', 'Bengaluru', 'India', '2019-05-28', '2019-06-28', '2019-05-28 08:26:25', '2019-05-28 13:26:33', 'A', '', 'Admin', 'Karnataka is a finalist in both singles and doubles in AITA National series for U14'),
(143, 'Naisha Srivastav of Karnataka continued with her impressive form', 'Home', 'N-S', 'A', 'uploadimage/Naisha32317.jpg', '<p>Naisha Srivastav of Karnataka continued with her impressive form and tamed Uttar Pradesh&rsquo;s Tanushri Pandey cruising to a fluent 6-4, 6-0 win in girls&rsquo; singles pre-quarter-final match of the CCI 13th Ramesh Desai Memorial All India Nationals Under-16 Tennis Tournament held on 20th to 25th May 2019</p>', 'Admin', '2019-06-10', 'Bengaluru', 'India', '2019-06-10', '2019-07-10', '2019-06-10 01:21:26', '2019-06-10 06:21:34', 'A', '', 'admin', 'Naisha Srivastav of Karnataka continued with her impressive form'),
(144, 'Sanjana Devineni from Karnataka won Super series under-12 singles title', 'Home', 'S-D', 'A', 'uploadimage/Sanjana Devneni45704.jpg', '<p>Sanjana Devineni from Karnataka won Super series under-12 singles title and also won under-14 doubles title in Karnal, Haryana the week of 10_06_19.&nbsp; She also reached under-14 singles semifinals being unseeded in both categories.In the week 17_06_19 Sanjana conquered yet another Aita Super Series title in girls U12 singles and went down fighting in three sets of girls U14 singles finals. She also won the U14 doubles title. The tournaments were held back to back at Karnal, Haryana.</p>', 'admin', '2019-06-22', 'Karnal', 'India', '2019-06-22', '2019-07-22', '2019-06-22 03:18:43', '2019-06-22 08:18:52', 'A', '', 'Admin', 'Sanjana Devineni from Karnataka won Super series under-12 singles title'),
(145, 'Krish Tyagi - U-12 Super Series Singles Runner up', 'Home', 'U-1', 'A', 'uploadimage/Krish85440.jpg', '<div dir=\"auto\">&nbsp;</div>\r\n<div dir=\"auto\">20th May 2019 U12 SUPER SERIES MUMBAI - BOYS U12 singles runner up.&nbsp;</div>\r\n<div dir=\"auto\"><br />\r\n<div dir=\"auto\">27th May 2019, <span style=\"font-family: sans-serif;\">U12 MUMBAI NATIONALS DOUBLES RUNNER UP and only boy from Karnataka state to reach quarter finals in singles.&nbsp;</span></div>\r\n<div dir=\"auto\">\r\n<div dir=\"auto\">&nbsp;</div>\r\n<div dir=\"auto\">10th Jun 2019 AITA SUPER SERIES KARNAL</div>\r\n<div dir=\"auto\">&nbsp;</div>\r\n<div dir=\"auto\">BOYS U12 singles winner.&nbsp;\r\n<div dir=\"auto\">BOYS U14 doubles runner up.&nbsp;</div>\r\n</div>\r\n</div>\r\n</div>', 'Admin', '2019-06-23', 'Bengaluru', 'India', '2019-06-24', '2019-07-24', '2019-06-24 00:13:11', '2019-06-24 05:20:16', 'A', '', 'Admin', 'U12 SUPER SERIES SINGLES RUNNER UP'),
(146, 'Tanussh Ghildyal - Jalandhar Aita Super Series Boys U-12 Winner', 'Home', 't-g', 'A', 'uploadimage/1270418.jpg', '<p>Tanussh Ghildyal, Jalandhar Aita Super Series (17 June 2019),BOYS U12 Singles winner.</p>', 'admin', '2019-06-23', 'bengaluru', 'India', '2019-06-24', '2019-07-24', '2019-06-24 00:17:26', '2019-06-24 05:18:09', 'A', '', 'admin', 'Tanussh Ghildyal - Jalandhar Aita Super Series Boys U-12 Winner'),
(147, 'Reshma Lift Titles', 'Home', 'R-L', 'A', 'uploadimage/RRRR28629.jpg', '<p>Reshma Maruri of Karnatka were Crowned Champion of the MGC - AITA National Series</p>\r\n<p>U-18 Tennis Tournament held at the Madras Gymkhana Club Courts &nbsp;</p>', 'Admin', '2019-07-13', 'Chennai', 'india', '2019-07-19', '2019-08-19', '2019-07-19 08:38:10', '2019-07-19 13:38:16', 'A', '', 'Admin', 'Reshma Lift Titles'),
(148, 'Golden day for Karnataka in National Level Tennis Championship in Coimbatore', 'Home', 'g-d', 'A', 'uploadimage/WEB NITHI & MARURI95640.jpg', '<p>it\'s golden day for Karnataka team, as players from the State clinched the title in Singles and double category of Coimbatore District Tennis Association Championship (AITA National Series) for under -16 boys and girls held at Levo Sports here in thecity on saturday, more than 100 participants from Tamil nadu, Andhra Pradesh, Karnataka, Maharashtra, Odisha, West Bengal took part in the tournament.</p>\r\n<p>Boys - Nithilan lifted both singles and doubles title.</p>\r\n<p>Girls&nbsp; - Suhitha Maruri Singles and Doubles winner partnering with Reshma Maruri.</p>', 'Admin', '2019-06-22', 'Coimbatore', 'India', '2019-07-19', '2019-08-19', '2019-07-19 09:00:42', '2019-07-19 14:00:50', 'A', '', 'Admin', 'Golden day for Karnataka in National Level Tennis Championship in Coimbatore'),
(149, 'Jason Michael David of Karnataka is the Doubles winner of NS U-14 ', 'Home', 'J-D', 'A', 'uploadimage/JASON30359.jpg', '<p>Jason Michael David of Karnataka is the Doubles winner of NS U 14 held at Chennai from 15th to 20th July - 2019.</p>', 'Admin', '2019-07-21', 'Chennai', 'India', '2019-07-22', '2019-08-22', '2019-07-22 05:47:32', '2019-07-22 10:47:39', 'A', '', 'Admin', 'Jason Michael David of Karnataka is the Doubles winner of NS U 14 '),
(150, 'Reshma Maruri was Singles Runner up and Doubles Winner', 'Home', 'R-M', 'A', 'uploadimage/RM18457.jpg', '<p>Reshma Maruri was Singles Runner up and Doubles Winner&nbsp; in the AITA Women\'s 1 lakh prize money tournament held at Chennai Tennis Centre, Chennai from 29.07.19 to 01.08.19.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', 'Admin', '2019-08-01', 'Chennai', 'India', '2019-08-05', '2019-09-05', '2019-08-05 04:11:18', '2019-08-05 09:11:25', 'A', '', 'Admin', 'Reshma Maruri was Singles Runner up and Doubles Winner'),
(151, 'Krish of Karnataka won the AITA Super Series Boys U 12 Singles', 'Home', 'k-s', 'A', 'uploadimage/Krish5186.jpg', '<p>Krish of Karnataka won the AITA Super Series Boys U 12 Singles recently held at Karnal and was also the finalist in the Boys U 14 Doubles.</p>', 'Admin', '2019-08-16', 'Karnal', 'India', '2019-08-17', '2019-09-17', '2019-08-17 07:48:24', '2019-08-17 12:50:38', 'A', '', 'Admin', 'Krish of Karnataka won the AITA Super Series Boys U 12 Singles'),
(152, 'Adith Amarnath of Karnataka was the Runner-up in U 14 Singles ', 'Home', 'a-a', 'A', 'uploadimage/Adith Amar7741.jpg', '<p>Adith Amarnath of Karnataka was the Runner-up in U 14 Singles and Winner in the Doubles U 14 AITA Super Series(SS) held at Karnal week 12_08_19.</p>', 'Admin', '2019-08-16', 'Karnal', 'India', '2019-08-17', '2019-09-20', '2019-08-17 07:50:28', '2019-08-20 12:20:36', 'A', '', 'Admin', 'Adith Amarnath of Karnataka was the Runner-up in U 14 Singles and Winner'),
(153, 'B.N.S. Reddy IPS, Hon. Treasurer KSLTA won the Gold Medal in Tennis Singles', 'Home', 'B-N', 'A', 'uploadimage/BS39297.jpg', '<p>Congratulation Sri. B.N.S. Reddy IPS, Hon. Treasurer KSLTA won the Gold Medal in Tennis Singles defeating Mr. Bruce Barrios of USA in the World Police and Fire Games held from 11 th to 17 Aug 2019 at Chengdu, China.&nbsp; Sri B.N.S. Reddy Achieves Hat-Trick of Gold Medals in tennis Sinles World Police Fire Games ,<br />Virgina USA in 2015, Los angles in 2017 and Chengdu, China in 2019.</p>', 'Admin', '2019-08-18', 'Bengaluru', 'India', '2019-08-21', '2019-09-21', '2019-08-21 04:08:41', '2019-08-21 09:11:02', 'A', '', 'Admin', 'B.N.S. Reddy IPS, Hon. Treasurer KSLTA won the Gold Medal in Tennis Singles'),
(154, 'Kriish finalist in U14 All Asian tennis Championships', 'Home', 'K-T', 'A', 'uploadimage/Krish Tyagi12605.jpg', '<p>Kriish&nbsp; of Karnataka reached finals of doubles U14 All Asian tennis Championships held in Jorhat .</p>', 'Admin', '2019-08-26', 'Bengaluru', 'India', '2019-08-26', '2019-09-26', '2019-08-26 09:19:13', '2019-08-26 14:19:22', 'A', '', 'Admin', 'Kriish finalist in U14 All Asian tennis Championships'),
(155, 'Reshma Maruri of Karnataka was the Runner-up', 'Home', 'R-MC', 'A', 'uploadimage/RS2480.jpg', '<p>Ms. Reshma Maruri of Karnataka was the Runner-up in the Adidas Nationals Girls&nbsp; U - 18 held at Madras Cricket Club, Chennai in the week 12_08_2019. She was also declared as the most Promising player of the tournament.</p>', 'Admin', '2019-08-25', 'Chennai', 'India', '2019-08-27', '2019-09-27', '2019-08-27 00:22:04', '2019-08-27 05:22:13', 'A', '', 'Admin', 'Reshma Maruri of Karnataka was the Runner-up'),
(156, 'Reshma and Suhitha Maruri won the NS U 18 Doubles ', 'Home', 'R-S', 'A', 'uploadimage/Reshma & Suhita Ahmedabad19907.jpg', '<p>Reshma and Suhitha Maruri won the NS U 18 Doubles organised by Altevol Sports Academy at Ahmedabad held on 9th to 13th Sep 2019.</p>', 'Admin', '2019-09-14', 'Ahmedabad', 'India', '2019-09-15', '2019-10-15', '2019-09-15 02:53:28', '2019-09-15 07:53:37', 'A', '', 'Admin', 'Reshma and Suhitha Maruri won the NS U 18 Doubles '),
(157, 'Reshma Maruri was the Runner-up in Singles U 16 N S tournament', 'Home', 'R-R', 'A', 'uploadimage/Reshma Ahmedabad83634.jpg', '<p>Reshma Maruri was the Runner-up in Singles U 16 NS tournament held at Altevol Sports Academy at Ahmedabad Held on 9th to 13th Sep 2019.</p>', 'Admin', '2019-09-14', 'Ahmedabad', 'India', '2019-09-15', '2019-10-21', '2019-09-15 02:56:37', '2019-09-21 10:43:33', 'A', '', 'Admin', 'Reshma Maruri was the Runner-up in Singles U 18 N S tournament'),
(158, 'Reshma Maruri of Karnataka won the Singles U-18', 'Home', 'R-K', 'A', 'uploadimage/Raquet Academy65405.jpg', '<p>Reshma played Under 18 National Series tournament in Ahmedabad Racquet Academy.<br /><br />Singles:<br />Reshma is the Winnner in Singles. She beat Vaishnavi Adkar from Maharashtra with a score of 6-4, 6-2.</p>', 'Admin', '2019-09-21', 'Admin', 'India', '2019-09-21', '2019-10-21', '2019-09-21 05:38:14', '2019-09-21 10:39:06', 'A', '', 'Admin', 'Reshma Maruri of Karnataka won the Singles U-18'),
(159, 'Reshma and Suhitha Maruri  Runner up in Doubles U-18', 'Home', 'R-S', 'A', 'uploadimage/S-R Raquet43811.jpg', '<p>Reshma and Suhitha played Under 18 National Series tournament in Ahmedabad Racquet Academy.</p>\r\n<p>Doubles:<br />Reshma and Suhitha were Runner up in Doubles. 6-3, 4-6, 10-8 (Super Tie Break).</p>', 'Admin', '2019-09-21', 'Ahmedabad', 'India', '2019-09-21', '2019-10-21', '2019-09-21 05:43:22', '2019-09-21 10:43:45', 'A', '', 'Admin', 'Reshma and Suhitha Maruri  Runner up in Doubles U-18'),
(160, 'Amodini Naik of Karnataka was the WINNER in U - 16 and RUNNER-UP  U - 14', 'Home', 'A-N', 'A', 'uploadimage/index39364.jpg', '<p>Amodini Naik of Karnataka was the WINNER in U - 16 and RUNNER-UP in the U - 14 Singles SUPER SERIES held at&nbsp; Guwahati&nbsp; Week 23_09_19</p>', 'Admin', '2019-09-27', ' Guwahati', 'India', '2019-09-28', '2019-10-28', '2019-09-28 04:13:54', '2019-09-28 09:14:01', 'A', '', '', 'Amodini Naik of Karnataka was the WINNER in U - 16 and RUNNER-UP  U - 14'),
(161, 'Reshma Maruri clinched the U - 16 singles title at the Fenesta Open', 'Home', 'R-F', 'A', 'uploadimage/Reshma-Finesta7297.jpg', '<p>Reshma Maruri Clinched the U-16 Girls Singles title at the Finesta Open National Tennis Chamionship held at New Delhi from 7th to 14th Oct 2019. in the final Reshma Beat Pari Singh.</p>', 'Admin', '2019-10-13', 'New Delhi', 'India', '2019-10-14', '2019-11-14', '2019-10-14 05:49:13', '2019-10-14 10:49:21', 'A', '', 'Admin', 'Reshma Maruri clinched the U - 16 singles title at the Fenesta Open'),
(162, 'Aayush P Bhat  won the U - 14 singles title at the Fenesta Open  ', 'Home', 'A-F', 'A', 'uploadimage/Aayush P Bhat Finesta Open23145.jpg', '<p>Aayush Bhat won the U-14 Boys Singles title at the Finesta Open National Tennis Chamionship held at New Delhi from 7th to 14th Oct 2019. in the final Bhat defeated Agriya Yadav.</p>', 'Admin', '2019-10-13', 'New Delhi', 'India', '2019-10-14', '2019-11-14', '2019-10-14 05:56:51', '2019-10-14 10:56:58', 'A', '', 'Admin', 'Aayush P Bhat  won the U - 14 singles title at the Fenesta Open  '),
(163, 'Reshma is runner up in  Asian Tennis Tour  $3000', 'Home', 'R-U', 'A', 'uploadimage/Reshma-ATT98788.jpg', '<p>Reshma Maruri is runner up in MTC Asian Tennis Tour&nbsp; $3000 Tournament, Mysuru. She lost to Akanksha Nitture 6-2,6-2</p>', 'admin', '2019-11-16', 'Bengaluru', 'India', '2019-11-16', '2019-12-16', '2019-11-16 06:08:45', '2019-11-16 12:08:52', 'A', '', 'admin', 'Reshma is runner up in  Asian Tennis Tour  $3000'),
(164, 'Biggest ATP Challenger returns to Bengaluru', 'Home', 'B-A', 'A', 'uploadimage/ATP -2079772.png', '<p style=\"text-align: justify; margin: 0in 0in 8pt; line-height: 107%; font-size: 11pt; font-family: Calibri,sans-serif;\"><strong><span style=\"font-size: 12pt; line-height: 107%;\">Bengaluru, Nov 11:</span></strong><span style=\"font-size: 12pt; line-height: 107%;\"> The Bengaluru Open which has now become a coveted stop on the ATP Challenger Tour, returns to the Garden City after skipping the 2019 calendar of events. The three-month postponement has come as a boon for Bengaluru as, the event, which was usually held every November, which was the fag end of the season, has moved into the new season. The signature event on the ATP Challenger Tour will now be held from February 10-16 at the Karnataka State Lawn Tennis Association.</span></p>\r\n<p style=\"text-align: justify; margin: 0in 0in 8pt; line-height: 107%; font-size: 11pt; font-family: Calibri,sans-serif;\"><span style=\"font-size: 12pt; line-height: 107%;\">The Bengaluru leg will follow the Pune ATP 250, an ATP Tour event, and the Australian Open, thereby ensuring the participation of some of the top players. The Bengaluru Open which boasts of a prize money of US$ 162,000 is the richest on the ATP Challenger circuit in terms of prize money played across the globe. </span></p>\r\n<p style=\"text-align: justify; margin: 0in 0in 8pt; line-height: 107%; font-size: 11pt; font-family: Calibri,sans-serif;\"><span style=\"font-size: 12pt; line-height: 107%;\">The Bengaluru Open, has been receiving a major support from the Government of Karnataka along with the help of corporate sponsors, who, together have helped event grow in stature with each passing season. It has established itself as a perfect platform for National and International players, as well as aspiring local players, to compete at an International Challenger stage, and improve their ATP rankings, and compete at Grand Slams. </span></p>\r\n<p style=\"text-align: justify; margin: 0in 0in 8pt; line-height: 107%; font-size: 11pt; font-family: Calibri,sans-serif;\"><span style=\"font-size: 12pt; line-height: 107%;\">&ldquo;The Bengaluru Open is now the biggest event on the ATP Challenger circuit in the Asian region and it was a proud moment for all of us when the winners of the previous editions played at the main draw of the US Open against legends like Roger Federer and Daniil Medvedev. This shows we have enough talent that can be nurtured through tournaments like Bengaluru Open,&rdquo; said Mr. Priyank Kharge who is the Chairman of the Organising Committee and brainchild behind the Bengaluru Open. </span></p>\r\n<p style=\"text-align: justify; margin: 0in 0in 8pt; line-height: 107%; font-size: 11pt; font-family: Calibri,sans-serif;\"><span style=\"font-size: 12pt; line-height: 107%;\">&ldquo;We have had the backing of the Government of Karnataka all these years and we are hopeful of getting the support this year too. The encouraging part is coming together of stake holders of Bengaluru which involves major corporates to support the event. We hope to have a bigger support from the corporate world in this edition,&rdquo; added Mr. Kharge.</span></p>\r\n<p style=\"text-align: justify; margin: 0in 0in 8pt; line-height: 107%; font-size: 11pt; font-family: Calibri,sans-serif;\"><span style=\"font-size: 12pt; line-height: 107%;\">KSLTA which is not averse to hosting National and International events will be in the cynosure once again with some of the top players in the under-100 bracket (world rankings) clashing for honours. &ldquo;I am sure the event will not only attract the top players but also be a great learning experience for the young tennis players in the city. This will be a fantastic opportunity for the youth to see their favorite sport stars in person and experience international tennis right from the centre court,&rdquo; said Mr. M. Lakshminarayana, IAS, Vice President, KSLTA and Advisor to the Honorable Chief Minister, Govt. of Karnataka. </span></p>\r\n<p style=\"text-align: justify; margin: 0in 0in 8pt; line-height: 107%; font-size: 11pt; font-family: Calibri,sans-serif;\"><span style=\"font-size: 12pt; line-height: 107%;\">Meanwhile, KSLTA will organize a series of events as a build-up to the mega event which will consist of 48 players in the Main draw as against 32 in the last edition.</span></p>\r\n<p style=\"text-align: justify; margin: 0in 0in 8pt; line-height: 107%; font-size: 11pt; font-family: Calibri,sans-serif;\"><a name=\"m_5885213205573857262_m_-434384097756321266__1fob9te\"></a><span style=\"font-size: 12pt; line-height: 107%;\">Former National coach and AITA Development Officer, Sunil Yajaman who is the tournament Director of the prominent event was thrilled that the event has taken a final shape after a worrisome period of uncertainty.&nbsp; &ldquo;I thank the Government of Karnataka for the unflinching support to this event in the recent editions, which, apart from putting Bengaluru on the tennis map of the world, has inked a mark as a preferred sports destination,&rdquo; said Sunil while adding &ldquo;On the competition front, I expect a quality field.&rdquo;</span></p>\r\n<p style=\"text-align: justify; margin: 0in 0in 8pt; line-height: 107%; font-size: 11pt; font-family: Calibri,sans-serif;\"><span style=\"font-size: 12pt; line-height: 107%;\">The ATP Challenger Bengaluru Open, apart from making the winner richer by over USD 22,000, will also give a chance to homegrown players to earn valuable ATP points and improve their ranking on the ATP circuit. Sumit Nagal, a winner here in the 2017 edition is the best example of a player who has exploited the opportunity to the maximum. His famed first set victory against the legendary Roger Federer in the US Open before bowing to the World Champion earlier this year, cannot be forgotten in a hurry. &ldquo;It was the Bengaluru Open victory which boosted my morale and gave me the confidence to tread on to bigger Tours,&rdquo; said Nagal in a telephonic interaction. &ldquo;It was a dream come true when I played Federer at the US Open. I would not have reached that stage that easily had I not played the Challenger circuit,&rdquo; added Nagal emphasizing the need of many more such tournaments in the country.&nbsp;&nbsp;&nbsp; </span></p>\r\n<p style=\"text-align: justify; margin: 0in 0in 8pt; line-height: 107%; font-size: 11pt; font-family: Calibri,sans-serif;\"><span style=\"font-size: 12pt; line-height: 107%;\">For its part, the KSLTA has already hosted many National and International events including ATP World doubles championship, Davis Cup and the WTA where the famed William sisters &ndash; Serena and Venus - once graced these courts. With a world class facility set amidst a serene backdrop and some quality tennis, a tennis buff cannot ask for more.</span></p>', 'Admin', '2019-11-12', 'Bengaluru', 'India', '2019-11-17', '2019-12-17', '2019-11-17 02:23:14', '2019-11-17 08:23:21', 'A', '', '', 'Biggest ATP Challenger returns to Bengaluru'),
(165, 'Kriish Tyagi won both Singles and doubles title', 'Home', 'K-T', 'A', 'uploadimage/Krish Tyagi90312.jpg', '<p>Kriish Tyagi of Karnataka and trainee of Sol Sports won both Singles and doubles title in&nbsp; Asian tennis Federation Championship-2019&nbsp; conducted recently in Raipur.&nbsp;</p>', 'Admin', '2019-11-16', 'Raipur', 'India', '2019-11-20', '2019-12-20', '2019-11-20 06:08:07', '2019-11-20 12:08:15', 'A', '', 'admin', 'Kriish Tyagi won both Singles and doubles title'),
(166, 'Sanjana Devineni of Karnataka has won both Singles and Doubles', 'Home', 'S-D', 'A', 'uploadimage/Sanjana Devineni 22.11.201941254.jpg', '<p>Ms. Sanjana Devineni of Karnataka has won both Singles and Doubles of the AITA Super series held at Shyam Bazar Tennis club, Kolkatta in the week 18_11_2019</p>', 'Admin', '2019-11-22', ' Kolkatta', 'India', '2019-11-25', '2019-12-25', '2019-11-25 06:18:54', '2019-11-25 12:19:03', 'A', '', 'Admin', 'Sanjana Devineni of Karnataka has won both Singles and Doubles'),
(167, 'Reshma Maruri and Suhitha Maruri pair won doubles in ITF J5 Tournament', 'Home', 'R-S', 'A', 'uploadimage/Reshma & Suhitha16985.jpg', '<p>Ms. Reshma Maruri and Ms. Suhitha Maruri pair won doubles in ITF J5 Tournament at Guwahati.</p>\r\n<p>Ms. Reshma Maruri was the Runnerup in Singles of the same tournament. She lost against Sanjana sirimalla 7-5,2-6,4-6.<br />Reshma and Suhitha bt&nbsp; Sarah Dev and Mallika Marathe 6-1,6-0 in the Doubles finals.</p>', 'Admin', '2019-11-22', 'Guwahati', 'India', '2019-11-25', '2019-12-26', '2019-11-25 06:22:59', '2019-11-26 13:01:29', 'A', '', 'Admin', 'Ms. Reshma Maruri and Ms. Suhitha Maruri pair won doubles in ITF J5 Tournament'),
(168, 'Chavana Shree is Doubles runner up in Rajshahi International Junior Tennis Championships 2019', 'Home', 'C-S', 'A', 'uploadimage/Chavana Shree 22.11.201959215.jpg', '<div dir=\"auto\">Chavana shree she mallela sreenath is Doubles Runner up&nbsp; in ITF GRADE 5 at Bangladesh</div>', 'Admin', '2019-11-17', '', 'Bangladesh', '2019-11-25', '2019-12-25', '2019-11-25 06:39:34', '2019-11-25 12:41:58', 'A', '', 'Admin', 'Chavana Shree is Doubles runner up in Rajshahi International Junior Tennis Championships 2019'),
(169, 'Amodini Naik of Karnataka has won the SS UNDER 14 Singles at Practennis', 'Home', 'A-V', 'A', 'uploadimage/Amodini V Naik99290.jpg', '<p>Amodini Naik of Karnataka has won the SS UNDER 14 Singles at Practennis, Mumbai, week 23_11_19 She was also the Runner Up in Doubles.</p>', 'Admin', '2019-11-29', 'Mumbai', 'India', '2019-12-04', '2020-01-04', '2019-12-04 07:00:47', '2019-12-04 13:00:54', 'A', '', 'Admin', 'Amodini Naik of Karnataka has won the SS UNDER 14 Singles at Practennis'),
(170, 'Amodini Vijay Naik of Karnataka was  Runner Up in Singles and Winner in Doubles', 'Home', 'a-v', 'A', 'uploadimage/Amodini U-14 ATF Tams92041.jpg', '<p>Amodini Vijay Naik of Karnataka was&nbsp; Runner Up in Singles and Winner in Doubles in ASIAN TENNIS FEDERATION UNDER 14 Tournament held in Bangalore from 13th to 17th January-2020.</p>', 'Admin', '2020-01-18', 'Bengaluru', 'India', '2020-01-19', '2020-02-19', '2020-01-19 02:45:45', '2020-01-19 08:46:03', 'A', '', 'Admin', 'Amodini Vijay Naik of Karnataka was  Runner Up in Singles and Winner in Doubles'),
(171, 'Karnataka Player Reshma Maruri  won Gold Medal in U17 KHELO INDIA Girls Singles.', 'Home', 'R-P', 'A', 'uploadimage/Reshma & Suhita Khelo India26022.jpg', '<p><em>Karnataka state Lawn Tennis Association (KSLTA) Congratulates Ms. Reshma Maruri &nbsp;on Winning The Girls U-17 Singles Gold Medal &amp;&nbsp; Ms. Reshma And Suhitha Maruri Winning the Girls U-17 Doubles Gold Medal at &nbsp;Khelo India Youth Games 2020 Held at Guwahati, Assam. We Wish Them Good Luck in their Future Endeavors</em>.&nbsp;</p>', 'Admin', '2020-01-22', 'Assam', 'India', '2020-01-22', '2020-02-22', '2020-01-22 06:18:34', '2020-01-22 16:18:44', 'A', '', 'Admin', 'Karnataka Players Reshma Maruri  won Gold Medal in U17 KHELO INDIA Girls Singles.'),
(172, 'Sanjana Devineni of Karnataka Winner of ATF U 14 Girls Singles', 'Home', 'S-k', 'A', 'uploadimage/Sanjana Devineni74961.jpg', '<p>Ms. Sanjana Devineni of Karnataka Winner of ATF U 14 Girls Singles held in Bengalooru week 13_01_2020</p>', 'Admin', '2020-01-18', 'Bengaluru', 'India', '2020-01-23', '2020-02-23', '2020-01-23 07:12:36', '2020-01-23 13:28:21', 'A', '', 'Admin', 'Sanjana Devineni of Karnataka Winner of ATF U 14 Girls Singles'),
(173, 'Reshma won ITF J5 Singles Title', 'Home', 'R-W', 'A', 'uploadimage/Reshma - Telgana4568.jpg', '<p>Reshma won ITF J5 Singles Title today against Sanjana Sirimalla from Telangana with 3-6, 6-4,6-4 score in Indore.</p>', 'Admin', '2020-02-01', 'Indore', 'India', '2020-02-01', '2020-03-01', '2020-02-01 07:45:04', '2020-02-01 13:45:12', 'A', '', 'Admin', 'Reshma won ITF J5 Singles Title'),
(174, 'Bengaluru Open Launch Ceremoney', 'Home', 'B-O', 'A', 'uploadimage/Launch Ceremony6196.jpg', '<p><strong>Bengaluru, Feb 8:</strong> Defending champion Prajnesh Gunneswaran, the highest ranked Indian in the world singles ranking is amongst the six home grown players to get a direct entry into the main draw of the Bengaluru Open , Asia&rsquo;s biggest ATP Challenger event that kicks off at the KSLTA on Monday.</p>\r\n<p>Speaking at a rousing launch ceremony held on the eve of its third edition, Mr. Priyank Kharge, former ITBT, Tourism and Social Welfare Minister, Government of Karnataka, and Chairman, Organizing Committee, Bengaluru Open was over joyous about the growth of the event in such a short span. &ldquo;We have grown over the last two editions and we have seen the quality of the field getting better by each edition. Players from across 24 nations are taking part with some of them having broken the top-30 barrier at one point of their career. As many as 21 players out of a total 40 direct entries are ranked below 200.&rdquo;</p>\r\n<p>&nbsp;&ldquo;We hope that this will be a great learning experience for the young tennis players in the city. It is a fantastic opportunity for the youth to see their favorite sport stars in person and experience international tennis right from the centre court. We also hope more and more youngsters take to the sport and bring our city, great pride with their victories,&rdquo; he added.</p>\r\n<p>Presiding over the launch function, Mr. R Ashoka, Hon&rsquo;ble Minister for Revenue, Govt. of Karnataka and Sr. Vice President, KSLTA was in praise of the Government of Karnataka for the continued support to the event. &ldquo;I am extremely thankful to the Government of Karnataka for their continued support and in this regard would express my gratitude to thank our CM Mr. B.S. Yediyurappa for his big support to the event. My deepest appreciation to the Department of Youth Services &amp; Sports for converting their clay courts to hard courts so that they can now be used as practice courts which was a requirement by ATP due to increased draw size. &rdquo; he said.</p>\r\n<ol>\r\n<li>Ashoka added that making Bengaluru a preferred sporting destination was one of the Government&rsquo;s prerogatives. &ldquo;Brand Bengaluru is associated with Technology, with culture, with start ups and gardens, However, Bengaluru City is also known for its sporting prowess and spirit. We hope that Bengaluru Open will be a platform to bring international and national tennis players together and offer them a fantastic playing field. It will be the perfect stage for Bengaluru to demonstrate to the world that it is a truly dynamic and multi-faceted city.&rdquo;</li>\r\n</ol>\r\n<p>On the competitive front, a main draw of 48 players will compete to lift the cup and proceed to the top of the rankings. The 48 players will comprise of - 40 players who have earned a direct entry, 5 wildcard players, two qualifiers and 1 player from special exempted category.</p>\r\n<p>The Bengaluru Open offers a total prize purse of US $ 162,480 with the winner pocketing US $ 21,600 and 125 ATP points. The runner-up stands to gain US $ 12,720 and 75 points while the semifinalists will take home US $ 7,530 each and 45 ATP points. The winning team in the doubles will be richer by US $ 9,300 but more importantly 125 ATP points.</p>\r\n<p>The tournament is a perfect platform for National and International players, as well as aspiring local players, to compete at an International Challenger stage, and improve their ATP rankings, and compete at Grand Slams. Prajnesh, backed by his victory in the last edition made it to the main draw of five consecutive Grand Slams while Sumit Nagal&rsquo;s fairytale victory in the 2017 edition of the Bengaluru Open has helped him jump over 150 places and reach a career best ranking of 125.</p>\r\n<p>Adding to the sheen will be Indian tennis legend Leander Paes who is set to grace the KSLTA courts in what could be his last ATP event on Indian soil, post his announcement to hang his racquet at the end of the current season.&nbsp;</p>\r\n<p>Mr. CS Sunder Raju, Vice President, Asian Tennis Federation and All India Tennis Association and Secretary, KSLTA while welcoming the gathering said that he was hopeful of an exciting end to the coveted event. &ldquo;It is the most competitive field for a Challenger event that I have seen in the recent past. With both the editions having produced an Indian champion, I am hopeful our boys will continue the tradition,&rdquo; said Mr. Raju while adding &ldquo;We will leave no stone unturned to ensure a smooth conduct of the event so that the players take back happy memories.&rdquo;&nbsp;&nbsp;&nbsp;</p>\r\n<p>Former National coach and AITA Development Officer, Sunil Yajaman is the Tournament Director while Andrey Kornilov from Uzbekistan will be the ATP supervisor.</p>\r\n<p>Meanwhile the host venue KSLTA which has hosted many National and International events including ATP World doubles championship, Davis Cup and the WTA where the famed William sisters &ndash; Serena and Venus &ndash; is all spruced up to host the mega event. With a world class facility set amidst a serene backdrop and some quality tennis, a tennis buff cannot ask for more.</p>', 'Admin', '2020-02-08', 'Bengaluru', 'India', '2020-02-09', '2020-03-09', '2020-02-09 01:45:22', '2020-02-09 07:46:03', 'A', '', 'Admin', 'Bengaluru Open Launch Ceremoney'),
(175, 'Rawat causes biggest flutter at Bengaluru Open', 'Home', 'r-c', 'A', 'uploadimage/Siddharth Rawat85253.jpg', '<p><strong>Bengaluru, Feb 10:</strong> Sidharth Rawat of India created the biggest flutter on the opening day of the Bengaluru Open 2002 when he ousted Borna Gojo ranked more than 200 places above him to make it to the pre-quarterfinals of the US $ 162,500 Challenger event at the KSLTA on Monday. The Delhi lad rallied after being a set down to finally emerge a 3-6, 7-6(3), 7-6(3) winner in a match that lasted 1 hour and 52 minutes.</p>\r\n<p>&nbsp;Slated for the last match of the day, Sidharth was staring at defeat while being down 2-5 in the decider, Rawat, having reserved the best for the last pulled up his socks and came back strongly, mostly with deep shots and was helped by two double faults from the Croatian won the next for games with a break in the ninth game. An early 5-1 lead in the tie-break helped the Indian to cruise through. This was after Gojo had clinched the first set with two breaks in the fourth and eight game while losing his serve in the fifth.</p>\r\n<p>&nbsp;The recovery began from the second set where his Rawat&rsquo;s serve improved and both held their serves to decide via tie-breaker.</p>\r\n<p>&nbsp;&ldquo;I am thrilled with the result, more so with my game as it fell in place when it was needed,&rdquo; said an elated Rawat. &nbsp;</p>\r\n<p>&nbsp;Earlier, in a re-match of the 2018 edition, Mukund Sasikumar sent Slovenian Blaz Kavcic home with a 2-6, 6-3, 6-4 victory. Also advancing to the second round was Saketh Myneni, runner-up at the last edition of the Bengaluru Open, who scored a clinical 6-3, 6-3 win over his Russian rival Aslan Karatsev.</p>\r\n<p>&nbsp;Blaz who is slightly ranked higher than Mukund at 275, began in the right earnest and looked set for an easy victory as he raced to a 4-0 lead with breaks in the 1<sup>st</sup> and 3<sup>rd</sup>. However, the Indian showed a semblance of a comeback winning the next two games but it lasted just for a while as he committed too many unforced errors. &ldquo;I don&rsquo;t know what was going on in my mind. I was trying too many things,&rdquo; said Mukund as he lost the next two games and the set.</p>\r\n<p>&nbsp;After both the players began breaking each other in the second set and repeated the pattern two games later to be tied at 3-3. The 23-year-old then achieved the crucial break in the seventh to surge ahead as Blaz in trying to adopt an aggressive approach, could not control his game and lost the set without winning the rest of the games.</p>\r\n<p>&nbsp;In the deciding set, Mukund started off with a break in the very first game and held the advantage until the 6<sup>th</sup> which he lost but was still on track. Mukund who has been fighting a mental battle as he later accepted, managed to break once again in the 9<sup>th</sup> game and served out for the match. &ldquo;I don&rsquo;t think I deserved to win with the kind of game I played. He (Blaz) was playing really well and I think he too was in a similar spot like me in the second and third set,&rdquo; said Mukund.</p>\r\n<p>&nbsp;In a contrasting encounter, Myneni who is still under the 200 ranking just needed just one break (6th) to grab the opening set 6-3. The second set saw the players holding their serves until the sixth game as Myneni won the rest of the games with breaks in the seventh and ninth to emerge victorious.</p>\r\n<p><strong>&nbsp;Results (Round-1)</strong></p>\r\n<p>Mukund Sasikumar (IND) bt Blaz Kavcic (SLO) 2-6, 6-3, 6-4;</p>\r\n<p>Malek Jaziri (TUN) bt [WC] Arjun Kadhe (IND) 6-2, 7-6(5); &nbsp;&nbsp;</p>\r\n<p>Saketh Myneni (IND) bt Aslan Karatsev (RUS) 6-3, 6-3</p>\r\n<p>Laurent Lokoli (FRA) bt Adam Pavlasek (CZE) 6-3, 7-6(4);</p>\r\n<p>Vaclav Safranek (CZE) bt Fabrizio Ornago (ITA) 7-6(5), 7-5;</p>\r\n<p>Vladyslav Orlov (UKR) bt [WC] Suraj Prabodh (IND) 6-2, 6-2;</p>\r\n<p>Khumoyun Sultanov (UZB) bt [WC] Prajwal Dev (IND) 6-2, 6-1;</p>\r\n<p>Filippo Baldi (ITA) bt Evgeny Karlovskiy (RUS) 6-2, 7-5</p>\r\n<p>Frederico Ferreira Silva (POR) bt Karim-Mohamed Maamoun (EGY) 6-1, 3-6, 6-0</p>\r\n<p>[WC] Niki Poonacha (IND) bt [WC] Adil Kalyanpur (IND) 6-4, 6-7(5), 7-6(4)</p>\r\n<p>Matthew Ebden (AUS) bt Tsung-Hua Yang (TPE) 6-0, 4-6, 7-6(2)</p>\r\n<p>Sidharth Rawat (IND) bt Borna Gojo (CRO) 3-6, 7-6(3), 7-6(3)</p>\r\n<p>&nbsp;<strong>Qualifying (Semifinals)</strong></p>\r\n<p>Anirudh Chandrashekar (IND) bt Nikshep Ravikumar 6-3, 6-4</p>\r\n<p>Abhinav Shanmugam (IND) bt [WC] Rishi Reddy (IND) 6-4, 6-2</p>', 'Admin', '2020-02-10', 'Bengaluru', 'India', '2020-02-10', '2020-03-10', '2020-02-10 10:25:38', '2020-02-10 16:25:57', 'A', '', 'Admin', 'Rawat causes biggest flutter at Bengaluru Open'),
(176, 'Paes enthralls crowd, Prajnesh stretched at Bengaluru Open', 'Home', 'P-e', 'A', 'uploadimage/Ramkumar Ramanathan18094.jpg', '<p><strong>Bengaluru, Feb 12:</strong> A crowd of near-full capacity cheered the Indian tennis legend Leander Paes as he walked into the stadium for his warm-up, minutes before his first round of doubles match of the Bengaluru Open 2020. The aura of Leander Paes spread beyond the Cubbon Park where KSLTA is located as all roads seemed to lead to the venue where, the 18-times Grand Slam Champion and Olympic medal winner Paes, in all probability is playing his last ATP event on Indian soil.</p>\r\n<p>Earlier in the day, his Australian partner Matthew Abden had lost a grueling three setter, each one decided via tie-breaker, to his Egyptian rival Mohamed Safwat in the longest match of the event thus far which lasted a little over three hours. His tiredness seemed to have vanished under the shadow of Paes&rsquo; presence so much so that he looked to be the dominant partner amongst the two as the duo advanced to the second round with a 7-6(2), 6-4 win over the Slovenia-China combine of Blaz Rola and Zhizhen Zhang.</p>\r\n<p>Meanwhile, Indian&rsquo;s top ranked player and defending champion Prajnesh Gunneswaran, seeded seventh here, was stretched by Sebastian Fanselow of Germany enroute to the quarterfinals of the US $ 162,480 prize money event as three seeds were shown the door at the KSLTA on Wednesday.</p>\r\n<p>The other Indians to advance into the next rounds were 17<sup>th</sup> seed Ramanathan Ramkumar who quelled the challenge of the qualifier Abhinav Shanmugam with a 6-1, 6-3 win and Sidharth Rawat who beat Lucky Loser Rishi Redyy 6-2, 6-2.</p>\r\n<p>Meanwhile Italian Julian Ocleppo ousted 10<sup>th</sup> seed Zhizhen Zhang of China in straight sets 6-1, 6-3 while Frenchman Benjamin Bonzi sent home the 12<sup>th</sup> seed Nikola Milojevic of Serbia with a 6-4, 6-1 victory. The last seed to bite the dust was 15<sup>th</sup> seed Kimmer Coppejans of Belgium who went down to Frederico Ferreira Silva of Portugal 3-6, 6-3, 5-7.</p>\r\n<p>Prajnesh who achieved a career high ranking of 75 last year, riding on a spate of good performances which included making the main draw of all the four Grand Slams, took time to settle in his quest to defend his title. Playing against the German who is ranked more than 400 rankings below him, a break in the fifth game gave him the advantage as he once again broke him in the seventh before clinching the opener 6-2.</p>\r\n<p>The 124<sup>th</sup> ranked Indian should have settled the issue in the second after going 3-1 up with a break in the second game. However, a couple of unforced errors added to his woes as he lost his serve in the sixth and 10<sup>th</sup> games to lose the set 4-6.</p>\r\n<p>In the decider, both tested each other&rsquo;s forehand to the maximum and held their respective serves until the seventh game where the German lost his serve, which was enough to set up a win for the Indian who served out for the match.</p>\r\n<p>&ldquo;The tag of the defending champion did not have an effect on me as it has been more than a year,&rdquo; said Prajnesh. &ldquo;Although I committed a few mistakes, which were not necessary, I am playing well and look forward to carrying on the form,&rdquo; said the 30-year-old from Chennai who is currently ranked 124<sup>th</sup> in the world.</p>\r\n<p><strong>Clinic for juniors: </strong>A clinic for junior tennis players will be conducted on Friday at 4 p.m. by a few top players playing here at the Bengaluru Open where the kids can learn the finer tricks of the trade while assimilating the rich experiences of the established players. This is an initiative by KSLTA to give an opportunity to the budding tennis players to interact with some of the best players. <strong>&nbsp;</strong></p>\r\n<p><strong>Results</strong></p>\r\n<p><strong>Singles </strong></p>\r\n<p><strong>Round-2</strong></p>\r\n<p>[7] Prajnesh Gunneswaran (IND) Vs. [Alt] Sebastian Fanselow (GER) 6-2, 4-6, 6-4;</p>\r\n<p>[4] James Duckworth (AUS) bt Khumoyun Sultanov (UZB) 6-4, 7-6(5);</p>\r\n<p>[17] Ramanathan, Ramkumar (IND) bt [Q] Abhinav Shanmugam 6-1, 6-3;</p>\r\n<p>Julian Ocleppo (ITA) bt [10] Zhizhen Zhang (CHN) 6-1, 6-3;</p>\r\n<p>[14] Mohamed Safwat (EGY) bt Matthew Abden (AUS) 7-6(5), 6-7(1), 7-5;</p>\r\n<p>Sidharth Rawat (IND) bt [LL] Rishi Reddy (IND) 6-2, 6-2;</p>\r\n<p>Benjamin Bonzi (FRA) bt [12] Nikola Milojevic (SRB) 6-4, 6-1;</p>\r\n<p>Frederico Ferreira Silva (POR) bt [15] Kimmer Coppejans (BEL) 6-3, 3-6, 7-5</p>\r\n<p>&nbsp;<strong>Doubles (Round-1)</strong></p>\r\n<p>Saketh Myneni (IND)/Matt Reid (AUS) bt [WC] Nikshep Ballekere (IND) Vasisht Cheruku (IND) 6-3, 6-1; [3] Andre Goransson (SWE)/Christopher Rungkat (INA) bt Prajnesh Gunneswaran (IND)/Sumit Nagal (IND) 6-3, 6-4; Lukas Rosol (CZE)/Yuichi Sugita (JPN) bt Andre Begemann (GER)/Daniel Masur (GER) 6-2, 6-3; [2] Jonathan Erlich (ISR)/Andrei Vasilevski (BLR) bt Romain Arneodo (MON)/Benjamin Bonzi (FRA) 4-6, 6-0, 10-7; Matthew Abden (AUS)/Leander Paes (IND) bt Blaz Rola (SLO)/Zhizhen Zhang (CHN) 7-6(2), 6-4</p>', 'Admin', '2020-02-12', 'Bengaluru', 'India', '2020-02-12', '2020-03-12', '2020-02-12 09:58:17', '2020-02-12 15:58:33', 'A', '', 'Admin', 'Paes enthralls crowd, Prajnesh stretched at Bengaluru Open');
INSERT INTO `ksl_news_tbl` (`news_id`, `news_title`, `news_display_tag`, `news_link_address`, `news_link_status`, `news_content_image`, `news_editor_content`, `news_posted_by`, `news_posted_at`, `news_located_city`, `news_located_country`, `news_from_date`, `news_to_date`, `news_created_date`, `news_lastupdated_date`, `news_flat`, `news_tournament_id`, `news_meta_name`, `news_meta_content`) VALUES
(177, 'Paes in doubles final two more seeds crash at Bengaluru Open', 'Home', 'P-I', 'A', 'uploadimage/Paes and Abden91899.jpg', '<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><strong><span style=\"font-family: Calibri,sans-serif;\">Bengaluru, Feb 14: </span></strong><span style=\"font-family: Calibri,sans-serif;\">Tennis was the ultimate winner at the KSLTA courts as almost all the matches &ndash; singles quarterfinals and doubles semifinals went till the wire in the US $ 162,500 Bengaluru Open, giving the viewers their money&rsquo;s worth. Indian tennis legend Leander Paes doubled their joy by reaching the title round along with his partner Matthew Abden. The Indo-Aussie pair quelled the challenge of the second seeds </span><span style=\"font-family: Calibri,sans-serif;\">Jonathan Erlich of Israel and Andrei Vasilevski of Belarus 6-4, 3-6, 10-7 amidst the chanting of Leander&rsquo;s name by the supportive crowd.</span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">&nbsp;</span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">The only match that didn&rsquo;t produce the fireworks was the second semifinal between another Indo-Australian pair of Saketh Myneni (IND) and Matt Reid who had to concede their match to an all Indian pair of Purav Raja and Ramkumar Ramanathan, seeded fourth here. The latter had won the first set 7-5 and were tied 15-15 in the first game of the second set when Reid fell down and hurt his knee.</span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">&nbsp;</span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">Earlier in the day, Italian Julian Ocleppo set the trend of long matches while ousting his fancied opponent and 13<sup>th</sup> seed Ilya Ivashka of Belarus in a grueling match that lasted for 2 hours and 40 minutes with the 22-year-old emerging as a </span><span style=\"font-family: Calibri,sans-serif;\">7-6(4), 6-7(3), 6-4 winner. Ocleppo faces another stiff hurdle in the semifinals where he is pitted against fourth seed James Duckworth of Australia who overcame ninth seed Thomas Fabbiano of Italy 6-7(5), 6-0, 6-2. </span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">Another seed bit the dust in the form of No.3 Yuichi Sugita of Japan who went down to unseeded Frenchman Benjamin Bonzi 7-6(5), 5-7, 4-6. Bonzi now meets second seed Stefano Travaglia of Italy who overcame a stiff challenge from 11<sup>th</sup> seed Blaz Rola of Slovakia before prevailing 6-4, 4-6, 6-2 in 1 hour and 56 minutes. </span></p>\r\n<p style=\"text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; margin-right: 0in; margin-left: 0in; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">Ocleppo was down 2-5 in the opening set after he got broken early in the second game. However, he came back strongly to win three games on the trot to tie at 5-5 and take the set into a tie-breaker which he won. &ldquo;My comeback in the first set was very crucial because the match could have ended by the second set,&rdquo; said Ocleppo. After the opponents had traded a break each in the 3<sup>rd</sup> and 4<sup>th</sup> games, Ocleppo lost the tie-break even as he had two break points. </span></p>\r\n<p style=\"text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; margin-right: 0in; margin-left: 0in; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">A stiff battle ensued in the decider with none giving their rival an inch of their ground. The Italian, who was on a break point in the ninth game, hit an awesome down the line shot to surge 5-4 ahead. &ldquo;That was my best shot of the match today and it was very crucial,&rdquo; said the winner who held on to his serve to win the match. &ldquo;I know Ivashka very well both personally and about his game. It is tough to return his serves and he is a very good player who could have won this tournament. I served very well today and once I got used to returning, it became a little easy. I am doubly happy to have beaten him,&rdquo; said Ocleppo.</span></p>\r\n<p style=\"text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; margin-right: 0in; margin-left: 0in; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">Meanwhile, a clinic for juniors was held were around 40 children attended the session with leading pro Niki Ponnappa giving them the tips. Their joy doubled when their idol Leader Paes too joined them.</span></p>\r\n<p style=\"text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; margin-right: 0in; margin-left: 0in; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><strong><span style=\"font-family: Calibri,sans-serif;\">Results </span></strong></p>\r\n<p style=\"text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; margin-right: 0in; margin-left: 0in; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><strong><span style=\"font-family: Calibri,sans-serif;\">Singles (Quarterfinals)</span></strong></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">Julian Ocleppo (ITA) bt [13] Ilya Ivashka (BLR) 7-6(4), 6-7(3), 6-4; [4] James Duckworth (AUS) bt [9] Thomas Fabbiano (ITA) 6-7(5), 6-0, 6-2; [2] Stefano Travaglia (ITA) bt [11] Blaz Rola (SLO) 6-4, 4-6, 6-2; Benjamin Bonzi (FRA) bt [3] Yuichi Sugita (JPN) 6-7(5), 7-5, 6-4 </span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><strong><span style=\"font-family: Calibri,sans-serif;\">&nbsp;</span></strong></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><strong><span style=\"font-family: Calibri,sans-serif;\">Doubles (Semifinals)</span></strong></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">Matthew Ebden (AUS) / Leander Paes (IND) bt [2] Jonathan Erlich (ISR) / Andrei Vasilevski (BLR) 6-4, 3-6, 10-7</span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">[4] Purav Raja (IND) / Ramkumar Ramanathan (IND) bt Saketh Myneni (IND) / Matt Reid (AUS) 7-5, Conceded </span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">&nbsp;</span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><strong><span style=\"font-family: Calibri,sans-serif;\">Singles Semifinal line-up (2 p.m. onwards)</span></strong></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">Julian Ocleppo (ITA) Vs. [4] James Duckworth (AUS)</span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">Benjamin Bonzi (FRA) Vs. [2] Stefano Travaglia (ITA) &nbsp;</span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">&nbsp;</span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><strong><span style=\"font-family: Calibri,sans-serif;\">Doubles final (6.30 p.m.)</span></strong></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">Matthew Ebden (AUS) / Leander Paes (IND) Vs. [4] Purav Raja (IND) / Ramkumar Ramanathan (IND)</span></p>', 'Admin', '2020-02-14', 'Bengaluru', 'India', '2020-02-14', '2020-03-14', '2020-02-14 11:24:08', '2020-02-14 17:24:15', 'A', '', 'Admin', 'Paes in doubles final two more seeds crash at Bengaluru Open'),
(178, 'James Duckworth of Australia with his prized possession', 'Home', 'j-d', 'A', 'uploadimage/Winner pic53779.jpg', '<p style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><strong><span style=\"font-family: Calibri,sans-serif;\">Bengaluru, Feb 16: </span></strong><span style=\"font-family: Calibri,sans-serif;\">It took just 68 minutes for Australian fourth seed James Duckworth to stop the French Juggernaut Benjamin Bonzi to lift the US $ 162,500 Bengaluru Open title at the KSLTA Courts here on Sunday. Although the match lasted for only two sets, the spectators were treated to some fine attacking tennis seen in a long time in Bengaluru. </span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">&nbsp;</span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">The evening began with an emotional farewell to Indian tennis legend Leander Paes and one expected the proceeding to follow suit of a somberness that preceded the match.</span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">&nbsp;</span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">Duckworth was coming with a reputation of having won 4 titles out of 6 final appearances, and was in fine form after having reached the last four of the ATP 250 in Pune last week. His opponent Bonzi meanwhile had felled four seeds enroute to the final including India&rsquo;s No.1 and defending champion Prajnesh Gunneswaran.&nbsp; So when the two talented men in form met on the court, it was fireworks all the way. &ldquo;I was aware he had beaten some good players in this tournament. I was kind of prepared for this and luckily I played my best today,&rdquo; said Duckworth.</span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">&nbsp;</span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">Both matched stroke for stroke but it was the Frenchman who had the first jibe breaking his opponent in the fourth game. Then the opponents broke each other&rsquo;s serve for next three games before Duckworth held his serve in the eighth game after saving two break points to level scores at 4-4. A break in the ninth game ensured that Duckworth won the set at 6-4 but not before Bonzi had some good answers to his serves.</span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">&nbsp;</span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">In the second set, both served and returned well and held their respective serves until the fourth game. Duckworth achieved a break in the 5<sup>th</sup> game to go 3-2 up. The rivals then broke each other in the next two games before their held their respective serves for Duckworth to win at 6-4.</span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">&nbsp;</span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">&ldquo;I have had a good run on Indian soil these two weeks. I may think of applying for citizenship,&rdquo; joked Duckworth after his victory. &ldquo;I am very happy to win here. It was a quality field and organized very efficiently. This win has given me the confidence to try and get to the top-50,&rdquo; said the winner.</span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">&nbsp;</span></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><strong><span style=\"font-family: Calibri,sans-serif;\">Results Singles final</span></strong></p>\r\n<p style=\"margin: 0in 0in 0.0001pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; font-size: 12pt; font-family: \'Times New Roman\',serif;\"><span style=\"font-family: Calibri,sans-serif;\">&nbsp;[4] James Duckworth (AUS) bt Benjamin Bonzi (FRA) 6-4, 6-4</span></p>', 'Admin', '2020-02-16', 'Bengaluru', 'India', '2020-02-16', '2020-03-16', '2020-02-16 11:52:56', '2020-02-16 17:53:02', 'A', '', 'Admin', 'James Duckworth of Australia with his prized possession'),
(179, 'Srishti Kiran won U-10 Titles in SAT Sports', 'Home', 's-k', 'R', 'uploadimage/Srishti Kiran SAT91394.jpg', '<p>Srishti Kiran won U-10 Titles in SAT Sports 24th Feb 2020</p>', 'Admin', '2020-02-24', 'Bengaluru', 'India', '2020-03-04', '2021-01-01', '2020-03-04 08:21:33', '2020-12-01 13:45:38', 'A', '', 'Admin', 'Srishti Kiran won U-10 Titles in SAT Sports'),
(181, 'Tarun Vetrivelan, Suhitha Maruri triumph in the AITA Talent Series tennis tournament held at KSLTA', 'Home', 't-s', 'A', 'uploadimage/Prize Distribution 93130.jpg', '<p>Top Seed and National Ranked 7 Suhitha Maruri of Karnataka stayed calm and stroked with ease in beating the unseeded National Ranked 526 Vanya Srivastav of Karnataka 6\\2,6\\1 in the Girls 16 and Under finals of the KSLTA-AITA Talent series 16 and under Tennis Tournament held at KSLTA Tennis Stadium cubbon Park. Vanya After loosing the first 3 games in the 1<sup>st</sup> set did try to come back by breaking Suhitha&rsquo;s Serve in the 4<sup>th</sup> game to make it 3\\2 but the strong and experienced Suhitha sail through with ease to close the set 6\\2 and the second set was a cake walk.</p>\r\n<p>The boys 16 and under finals was a nail-biting match packed with power and both the players gave run for each other to gain the National Ranking points. The 2<sup>nd</sup> seeded ranked 85 in the country Tarun vetrivelan arrested his opponent Mandeep Reddy Kudumala in a trilling 3 setter match which lasted 3hrs and 5min. Tarun&rsquo;s flamboyant attitude and aggressive backhand down the line and variation in strokes helped him beat his opponent in the score line of 6\\4,2\\6,6\\4.&nbsp; &nbsp;</p>\r\n<p>Mr. M D Dyaberi IAS (Retd) and Vice President KSLTA gave away the prizes during the prize distribution.</p>\r\n<p>Suhitha Maruri Said- &ldquo;I am excited to win the 1<sup>st</sup> tournament after the lock down and I look forward to play more events&rdquo;</p>\r\n<p>Tarun Vetrivelan Said- &ldquo;I thank the organizers for organising this event and giving us the opportunity to play and gain national ranking points&rdquo;&nbsp;&nbsp;</p>\r\n<p><strong><u>Results: </u></strong></p>\r\n<p><strong><em><u>Girls Under 16 Singles Finals</u></em></strong></p>\r\n<p>1) Suhitha Maruri (KAR) bt Vanya Srivastav (KAR) -6/2,6/1</p>\r\n<p><strong><em><u>Boys Under 16 Singles Finals</u></em></strong></p>\r\n<p>1) Tarun Vetrivelan (KAR) bt Mandeep Reddy Kudumal (KAR) &ndash; 6\\4,2\\6,6\\4</p>\r\n<p>&nbsp;</p>\r\n<p>KSLTA will host an AITA Talent series 14 and under event starting from 27<sup>th</sup> November 2020 and we request your kind support in encouraging the junior tennis players of the state.</p>', 'Admin', '2020-11-22', 'Bengaluru', 'India', '2020-11-23', '2020-12-23', '2020-11-23 11:07:12', '2020-11-23 11:09:39', 'A', '', 'Admin', 'Tarun Vetrivelan, Suhitha Maruri triumph in the AITA Talent Series tennis tournament held at KSLTA'),
(182, 'David, Sanjana bag Trophies', 'Home', 'd-s', 'R', 'uploadimage/Prize Distribution TS U-1478182.jpg', '<p>Jason Michael David and Sanjana Devineni clinch title at the KSLTA-AITA Junior Circuit Talent series 14 and Under tournament held at KSLTA Tennis Stadium in Cubbon park Bengaluru.<br /> <br /> The gloomy weather of cubbon park witnessed some blazing tennis and spectacular show by the 4 budding top 20 players of the country while they battled for the title of 14 and under KSLTA-AITA Talent series tournament.<br /> <br /> In the girls singles category Sanjana Devineni (seed2) and rank 18 stroked efficiently to break her opponents serve at 4-4 in the first set and capitalized the same till the end of the match by beating<br /> Amodini Vijay Naik 6\\4,7\\6(5).<br /> <br /> The boys singles match was the battle of consistency and both the players rallied neck to neck and eventually the top seed ranked 14 in the country Jason Michael David prevailed over 2nd Seeded&nbsp; Kriish Ajay Tyagi 6\\4,4\\6,6\\4.<br /> <br /> Ms Archana Venkataraman former national champion and eklavya awardee was present to&nbsp; give away prizes&nbsp; for the Winners and Runners up.<br /> <br /> &nbsp;Boys Singles Results<br /> 1.&nbsp; &nbsp; &nbsp; &nbsp;Jason Michael David (Seed 1) (Rank 14) bt Kriish Ajay Tyagi (Seed 2) (Rank 17) &ndash; 6\\4,4\\6,6\\4<br /> </p>\r\n<p>Girls Singles Results<br /> 2.&nbsp; &nbsp; &nbsp; &nbsp;Sanjana Devineni (Seed 2) (Rank 18) bt Amodini Vijay Naik (Seed 1) (Rank 10)- 6\\4,7\\6(5)</p>', 'admin', '2020-11-29', 'Bengauru', 'india', '2020-11-30', '2020-12-30', '2020-11-30 08:29:21', '2020-11-30 08:29:30', 'A', '', '', 'David, Sanjana bag Trophies'),
(183, 'Jason Michael David and Sanjana Devineni clinch title', 'Home', 'j-s', 'A', 'uploadimage/Prize Distribution TS U-1485234.jpg', '<p>Jason Michael David and Sanjana Devineni clinch title at the KSLTA-AITA Junior Circuit Talent series 14 and Under tournament held at KSLTA Tennis Stadium in Cubbon park Bengaluru.<br /> <br /> The gloomy weather of cubbon park witnessed some blazing tennis and spectacular show by the 4 budding top 20 players of the country while they battled for the title of 14 and under KSLTA-AITA Talent series tournament.<br /> <br /> In the girls singles category Sanjana Devineni (seed2) and rank 18 stroked efficiently to break her opponents serve at 4-4 in the first set and capitalized the same till the end of the match by beating<br /> Amodini Vijay Naik 6\\4,7\\6(5).<br /> <br /> The boys singles match was the battle of consistency and both the players rallied neck to neck and eventually the top seed ranked 14 in the country Jason Michael David prevailed over 2nd Seeded&nbsp; Kriish Ajay Tyagi 6\\4,4\\6,6\\4.<br /> <br /> Ms Archana Venkataraman former national champion and eklavya awardee was present to&nbsp; give away prizes&nbsp; for the Winners and Runners up.<br /> <br /> &nbsp;Boys Singles Results<br /> 1.&nbsp; &nbsp; &nbsp; &nbsp;Jason Michael David (Seed 1) (Rank 14) bt Kriish Ajay Tyagi (Seed 2) (Rank 17) &ndash; 6\\4,4\\6,6\\4<br /> </p>\r\n<p>Girls Singles Results<br /> 2.&nbsp; &nbsp; &nbsp; &nbsp;Sanjana Devineni (Seed 2) (Rank 18) bt Amodini Vijay Naik (Seed 1) (Rank 10)- 6\\4,7\\6(5)</p>', 'Admin', '2020-11-29', 'Bengaluru', 'India', '2020-12-01', '2021-01-01', '2020-12-01 13:44:34', '2020-12-01 13:44:45', 'A', '', 'Admin', 'Jason Michael David and Sanjana Devineni clinch title');

-- --------------------------------------------------------

--
-- Table structure for table `ksl_notification`
--

CREATE TABLE `ksl_notification` (
  `notify_id` int(20) NOT NULL,
  `notify_text` text NOT NULL,
  `notify_link` text NOT NULL,
  `notify_color` text NOT NULL,
  `notify_status` varchar(100) NOT NULL,
  `notify_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ksl_notification`
--

INSERT INTO `ksl_notification` (`notify_id`, `notify_text`, `notify_link`, `notify_color`, `notify_status`, `notify_timestamp`) VALUES
(1, 'U-14 BOYS DRAW', 'fileupload/boysu14drs.pdf ', 'Blue', 'Active', '2020-11-28 12:06:32'),
(2, 'U-14 GIRLS DRAW', 'fileupload/girlsu14drs.pdf', 'red', 'Active', '2020-11-28 12:05:58'),
(3, 'FRI ORDER OF PLAY', 'fileupload/Opfriday.pdf', 'Blue\r\n', 'Inactive', '2020-11-26 11:05:54'),
(4, 'ORDER OF PLAY, SUN', 'fileupload/oop29th.pdf', 'orange', 'Active', '2020-11-28 12:05:22');

-- --------------------------------------------------------

--
-- Table structure for table `ksl_page_contents`
--

CREATE TABLE `ksl_page_contents` (
  `page_content_id` bigint(20) NOT NULL,
  `gallery_id` bigint(20) DEFAULT NULL,
  `page_content_title` varchar(250) NOT NULL,
  `page_name` varchar(350) NOT NULL,
  `page_content_top_main_heading` varchar(1000) NOT NULL,
  `page_content_top_sub_heading` text CHARACTER SET utf8 NOT NULL,
  `page_content_heading` varchar(500) NOT NULL,
  `page_main_content` text CHARACTER SET utf8,
  `page_content_img` varchar(350) DEFAULT NULL,
  `page_content_portfolio_img` text CHARACTER SET utf8,
  `remove_content_img` tinyint(1) NOT NULL,
  `page_assign_sidebar` tinyint(1) NOT NULL,
  `page_content_created_at` datetime NOT NULL,
  `page_content_modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `page_content_status` enum('A','I') NOT NULL COMMENT 'A=Active, I=Inactive',
  `page_meta_name` text CHARACTER SET utf8 NOT NULL,
  `page_meta_content` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ksl_page_contents`
--

INSERT INTO `ksl_page_contents` (`page_content_id`, `gallery_id`, `page_content_title`, `page_name`, `page_content_top_main_heading`, `page_content_top_sub_heading`, `page_content_heading`, `page_main_content`, `page_content_img`, `page_content_portfolio_img`, `remove_content_img`, `page_assign_sidebar`, `page_content_created_at`, `page_content_modified_at`, `page_content_status`, `page_meta_name`, `page_meta_content`) VALUES
(3, 4, 'Bar & Restaurant - Karnataka State Lawn Tennis Association', 'barrestaurant', 'Bar & Restaurant', 'KSLTA FACILITIES', 'KSLTA BAR & FACILITIES', '', 'uploads/pageImg/ksltapage130932.jpg', '', 0, 0, '2030-11-01 00:00:00', '2016-10-22 18:09:32', 'A', 'Bar & Restaurant', 'Bar & Restaurant'),
(5, 6, 'KSLTA - Karnataka State Lawn Tennis Association', 'Guest', 'KSLTA FACILITIES', 'Guest Room', 'GUEST ROOMS TERMS', '<h3><strong><span style=\"color: #000000;\">GUEST ROOMS TERMS<span class=\"point-little\">.</span></span></strong></h3>', 'uploads/pageImg/ksltapage3920.jpg', 'uploads/pageImg/ksltapage824.jpg', 0, 0, '2030-11-01 00:00:00', '2016-10-21 15:06:45', 'A', 'GUEST ROOMS', 'GUEST ROOMS TERMS'),
(8, 9, 'Table Tennis & Billiards - Karnataka State Lawn Tennis Association', 'table_tennis', 'TABLE TENNIS  & BILLIARDS', 'KSLTA FACILITIES', 'TABLE TENNIS  & BILLIARDS', '', 'uploads/pageImg/ksltapage120459.jpg', '', 1, 1, '0000-00-00 00:00:00', '2016-10-24 17:05:31', 'A', 'kslta table tennis', 'kslta table tennis'),
(10, 0, 'CUBBON PARK & BANGALORE.', 'cubbonpark_bangalore', 'CUBBON PARK & BANGALORE', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', 'CUBBON PARK & BANGALORE.', '<p class=\"desc_news\">Cubbon Park is a landmark \'lung&rsquo; space of the Bangalore city, located within the heart of city. Lush greenery welcomes any visitor entering Cubbon Park through any of the approaches and our club house is located within the Cubbon Park.</p>\r\n<p class=\"desc_news\">The landscaping in the park creatively integrates natural rock outcrops with thickets of trees, massive bamboos, with grassy expanse and flowerbeds and the monuments within its limits, regulated by the Horticulture Department of the Government of Karnataka. Rich in plant wealth, many indigenous and exotic botanical species are found here. The park has about 68 genera and 96 species with a total of around 6000 plants/trees growing in its locale. Silver Oak, having the distinction of being the first oak introduced to Bangalore from Australia, is still found in Cubbon Park.</p>\r\n<p class=\"desc_news\">The predominantly green area of the park has many motorable roads, and the well laid out walking paths running through the park is frequented by early morning walkers and the naturalists who study plants in the tranquil natural environment.</p>\r\n<p class=\"desc_news\">Tourists visiting this Park in the city of Bangalore have nicknamed the city itself as \'Garden City\'. Though Kannada is the language spoken by the locals, because of Bangalore\'s cosmopolitan nature and demographic diverseness, one could comfortably converse in English, Hindi or any of the numerous Indian languages. Bangalore is best known for making people all over the world, at home.</p>\r\n<p class=\"desc_news\">While the Vidhana Soudha (the State Secretariat) is the prime attraction, one could also enjoy the marvels of the universe at the Nehru Planetarium; The Government Museum of Bangalore; the Visvesvaraya Technological and Industrial Museum or the LalBagh Botanical Gardens, which holds a number of flower shows, especially during the Republic Day, attracting several tourists. The Fort and Palace of Tipu Sultan, which have historical significance, are other important places to see in Bangalore\\ Some of the temples that have stood the test of time and have attracted thousands for their tranquility, antiquity and sanctity are the Nandi Bull Temple, the Venkataramanaswamy Temple, the Gavi Gangadhareshwara Cave Temple and the Someshwara temple, the last being the oldest in the city and more than a thousand years old.</p>\r\n<p class=\"desc_news\">With the advent of IT, Bangalore is now one of India&rsquo;s largest and most populated cities. With a diverse population of over 10 million, it is called the Silicon Valley of India being the proud host of many successful IT companies of the world &mdash; with Infosys and Wipro headquartered in the city. It is also the scientific hub of India and has within its precincts one of the oldest research universities &mdash; the world renowned Indian Institute of Science (llsc) and the Raman Research Institute (RRI). Bangalore is also home to Indian Institute of Management (Bangalore) (IIMB), National Institute of Mental Health and Neurosciences (NIMHANS) and numerous public sector heavy industries, technology companies, aerospace, telecommunications, and defence organisations, such as Bharat Electronics Limited (BEL), Hindustan Aeronautics Limited (HAL), National Aerospace Laboratories (NAL) and Indian Space Research Organisation (ISRO). Added to these are many multi-specialty hospitals. In short, it is a perfect amalgamation of modernization and development with a glorious history, presented in a backdrop of beautiful nature that makes Bangaloreans proud of their city.</p>', 'uploads/pageImg/ksltapage9180.jpg', '', 1, 0, '2030-11-01 00:00:00', '2016-10-24 17:36:28', 'A', 'description', 'The stadium was originally built in 1976 to stage the first ever Davis Cup in Bangalore. The entire stadium was renovated fully to International Standards in October 2000 for conducting the ATP Tour World Championships.'),
(11, 0, 'ABOUT KSLTA', 'aboutkslta', 'ABOUT KSLTA', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', 'ABOUT KSLTA', '<p class=\"desc_news\">KARNATAKA STATE LAWN TENNISASSOCIATION &amp; ITS CLUB HOUSE. The Karnataka State Lawn Tennis Association is one of its kinds in the country. Perhaps, it is the only State Tennis Association in the country, which has international class tennis facility with an arena having a seating capacity for more end 7000 spectators. What&rsquo;s more, it also boasts of clubhouse, which has a gymnasium, restaurant, a pool table &amp;swimming pool, facilities for indoor games, besides seven newly built air conditioned guest rooms.</p>\r\n<p class=\"desc_news\">The KSLTA has a hoary past, and goes back to 1930s, when Mysore State Lawn Tennis Association was established and the Association was registered in 1952. The Association shot into international prominence in 1970 when India-Australia tie was held in Bangalore. The historic event was held in Cubbon court, belonging to Bangalore Tennis Club. It was during that, time, the Mysore State Lawn Tennis Association, approached the Government to allot a land for the Association. With the generosity of the Bangalore Tennis Club the Cubbon Court was leased to the Association. In 1972, Bangaloe Club relinquished its hold on the Cubbon Park courts and State Association began to take steps to build a stadium.</p>\r\n<p class=\"desc_news\">The foundation stone for the Stadium in Cubbon Park was laid in 1972 by then Governor, Shri Dharam Vira and the Stadium was completed in 1976 and was inaugurated by then late chief Minister Mr. Devaraj Urs. The KSLTA ushered in a new millennium in 2000, when new set of office bearers, headed by the then Chief Minister, Shri. S.M. Krishna and as President and dynamic entrepreneur, Shri.C.S. Sunder Raju as secretary, and Shri. P.R.Ramaswamy as Joint Secretary, took charge of the Association. The new executive committee soon began implementing its Vision 2000 programme, with the aim of making KSLTA and Bangalore an international tennis destination. The Stadium was fully renovated at the cost Rs.2 crore with the sponsorship of Shri Vijay Mallya of the UB Group. It soon had four [04] synthetic courts, bucket seats in gallery for spectators; fully air conditioned lounge, change rooms, media room with internet and other facilities.</p>\r\n<p class=\"desc_news\">Besides catering to high end tour events, The KSLTA also took initiative to develop grassroots tennis, by having mini tennis clinics and various high performance tennis coaching to groom the young talent in the State. It also encourage youngsters, who perform well at the National and international events with cash awards. Besides upgrading facilities for tennis, KSLTA now has a full-fledged club house, with multi cuisine restaurant, bar, swimming pool, gym; facilities for indoor games and seven fully furnished A.C guest rooms. And no wonder, the KSLTA is now a much sought after destination not only for tennis, but for complete relaxation and enjoyable social gatherings.</p>', 'uploads/pageImg/ksltapage265.jpg', '', 1, 0, '2030-11-01 00:00:00', '2016-10-24 12:38:13', 'A', 'description', 'The stadium was originally built in 1976 to stage the first ever Davis Cup in Bangalore. The entire stadium was renovated fully to International Standards in October 2000 for conducting the ATP Tour World Championships.'),
(13, 0, 'commitee', 'commitee', 'COMMITTEE MEMBERS', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', 'COMMITTEE MEMBERS.', '<p>COMMITTEE MEMBERS.</p>', 'uploads/pageImg/ksltapage2288.jpg', '', 0, 0, '2016-09-29 16:29:43', '2016-10-20 16:52:50', 'A', 'COMMITTEE MEMBERS.', 'COMMITTEE MEMBERS.'),
(14, 0, 'Affilated Clubs - Karnataka State Lawn Tennis Association', 'affilated_clubs', 'Affilated Clubs', 'ALL INDIA ASSOCIATION', 'AFFILATED CLUBS.', '<div class=\"bulleted_list\"><dir id=\"info-company\" class=\"affclubs\">\r\n<div class=\"col-md-4\">\r\n<h4><strong>KERALA</strong></h4>\r\n<h5><strong>THE TRIVANDRUM TENNIS CLUB</strong></h5>\r\n<p style=\"line-height: 20px;\">Kowdiar P.o. Trivandrum</p>\r\n<p>0471-2722737 / 592 / 2310832</p>\r\n<p>0471-2727475</p>\r\n<p>ttctvm@gmail.com</p>\r\n<p>www.ttc.org</p>\r\n</div>\r\n<div class=\"col-md-4\">\r\n<h4><strong>KERALA</strong></h4>\r\n<h5><strong>LOTUS CLUB</strong></h5>\r\n<p style=\"line-height: 20px;\">Waarruam Road, Ernakulam, <br /> Cochin-682016</p>\r\n<p>04847-2352456 / 2366737</p>\r\n<p>lotusclubcoachin@yahoo.co.in</p>\r\n<p>www.lotusclub.in</p>\r\n</div>\r\n<div class=\"col-md-4\">\r\n<h4><strong>KERALA</strong></h4>\r\n<h5><strong>COCHIN SUBURBAN CLUB </strong></h5>\r\n<p style=\"line-height: 20px;\">Thrikkakara P.O, Cochin-682021</p>\r\n<p>0484-2575871 / 2576571</p>\r\n<p>cochinsuburbanclub1980@gmail.com</p>\r\n<p>www.cochinsuburbanclub.com</p>\r\n</div>\r\n</dir>\r\n<div class=\"clearfix\">&nbsp;</div>\r\n<dir id=\"info-company\" class=\"affclubs\">\r\n<div class=\"col-md-4\">\r\n<h4><strong>KARNATAKA</strong></h4>\r\n<h5><strong>THE HERITAGE CLUB</strong></h5>\r\n<p style=\"line-height: 20px;\">#816/274, \'C\' Block, Vijayanagar 3rd Stage,Mysuru</p>\r\n<p>0821-2412377</p>\r\n<p>heritageclubmys@gmail.com</p>\r\n<p><a href=\"http://www.heritageclubmys.in\">www.heritageclubmys.in</a></p>\r\n</div>\r\n<div class=\"col-md-4\">\r\n<h4><strong>KARNATAKA</strong></h4>\r\n<h5><strong>BHADRA SPORTS CLUB</strong></h5>\r\n<p style=\"line-height: 20px;\">Club Road, Balehonnur-577112, Karnataka</p>\r\n<p>08266-250405</p>\r\n<p>bhadraclubinfo@gmail.com</p>\r\n<p><a href=\"http://www.bhadrasportsclub.com\">www.bhadrasportsclub.com</a></p>\r\n</div>\r\n<div class=\"col-md-4\">\r\n<h4><strong>KARNATAKA</strong></h4>\r\n<h5><strong>PLANTERS CLUB</strong></h5>\r\n<p style=\"line-height: 20px;\">JPost Box No.7, SAKLESHPUR - 573134</p>\r\n<p>08173-230279 / 230158</p>\r\n<p>08173-230246</p>\r\n<p>plantersclubsakleshpur@rediffmail.com</p>\r\n<p>www.plantersclub.in</p>\r\n</div>\r\n</dir>\r\n<div class=\"clearfix\">&nbsp;</div>\r\n<dir id=\"info-company\" class=\"affclubs\">\r\n<div class=\"col-md-4\">\r\n<h4><strong>KARNATAKA</strong></h4>\r\n<h5><strong>TUMKUR CLUB</strong></h5>\r\n<p style=\"line-height: 20px;\">Gandhinagar, Tumkur-572102</p>\r\n<p>0816-2278366</p>\r\n<p>tumkurclub@gmail.com</p>\r\n<p>www.tumkurclub.in</p>\r\n</div>\r\n<div class=\"col-md-4\">\r\n<h4><strong>KARNATAKA</strong></h4>\r\n<h5><strong>BELUR PLANTER\'S CLUB</strong></h5>\r\n<p style=\"line-height: 20px;\">Post Box No-4, Belur - 573115, Hassan District</p>\r\n<p>9880872892 / 9448070104</p>\r\n</div>\r\n<div class=\"col-md-4\">\r\n<h4><strong>KARNATAKA</strong></h4>\r\n<h5><strong>COTTON COUNTY RESORTS &amp; ESTATES PVT LTD</strong></h5>\r\n<p style=\"line-height: 20px;\">Opp.To Airport, Gokul Road, Hubli-30</p>\r\n<p>0836-6563466 / 2335799</p>\r\n<p>0484-2381596</p>\r\n<p>cottoncountyclub@gmail.com</p>\r\n<p>www.cottoncountyclub.com</p>\r\n</div>\r\n</dir>\r\n<div class=\"clearfix\">&nbsp;</div>\r\n<dir id=\"info-company\" class=\"affclubs\">\r\n<div class=\"col-md-4\">\r\n<h4><strong>KARNATAKA</strong></h4>\r\n<h5><strong>MUDIGERE CLUB</strong></h5>\r\n<p style=\"line-height: 20px;\">Post Box No.28. Mudigere-577132, Chickamagalur District</p>\r\n<p>08263-220191</p>\r\n<p>mudigereclub@yahoo.com</p>\r\n</div>\r\n<div class=\"col-md-4\">\r\n<h4><strong>KARNATAKA</strong></h4>\r\n<h5><strong>THE PUTTUR CLUB</strong></h5>\r\n<p style=\"line-height: 20px;\">P.O.Darbe, Puttur D.K.574 202</p>\r\n<p>9980410423 / 9448277385</p>\r\n<p>putturclub@gmail.com</p>\r\n<p>www.theputturclub.com</p>\r\n</div>\r\n<div class=\"col-md-4\">\r\n<h4><strong>KARNATAKA</strong></h4>\r\n<h5><strong>MALNAD CLUB</strong></h5>\r\n<p style=\"line-height: 20px;\">Balebylu, Agumbe Road, Thirthahalli - 577432 Shivamogga Dist</p>\r\n<p>08181-228220 / 220220</p>\r\n<p>malnadclubth@gmail.com</p>\r\n<p>www.themalnadclub.com</p>\r\n</div>\r\n</dir>\r\n<div class=\"clearfix\">&nbsp;</div>\r\n<dir id=\"info-company\" class=\"affclubs\">\r\n<div class=\"col-md-4\">\r\n<h4><strong>MAHARASHTRA</strong></h4>\r\n<h5><strong>UNITED SERVICE CLUB</strong></h5>\r\n<p style=\"line-height: 20px;\">Robert Road, Near R C Church, Colaba, Mumbai - 400005</p>\r\n<p>022-22150881, 22151480</p>\r\n<p>022-22150881, 22160139</p>\r\n<p>admin@usclub.co.in</p>\r\n<p>www.usclub.co.in</p>\r\n</div>\r\n<div class=\"col-md-4\">\r\n<h4><strong>MAHARASHTRA</strong></h4>\r\n<h5><strong>ROYAL CONNAUGHT BOAT CLUB</strong></h5>\r\n<p style=\"line-height: 20px;\">7/8, Boat Club Road, Pune-411001</p>\r\n<p>020-26163512 / 13 / 14</p>\r\n<p>020-26163511</p>\r\n<p>info@boatclubpune.com</p>\r\n<p>www.boatclubpune.com</p>\r\n</div>\r\n<div class=\"col-md-4\">\r\n<h4><strong>RAJASTHAN</strong></h4>\r\n<h5><strong>JAISAL CLUB</strong></h5>\r\n<p style=\"line-height: 20px;\">Jethwai Road,Jaisalmer-345001</p>\r\n<p>02992-255555, 254999</p>\r\n<p>0291-2435349</p>\r\n<p>jaisalclub24x7@gmail.com</p>\r\n<p>www.jaisalclub.com</p>\r\n</div>\r\n</dir>\r\n<div class=\"clearfix\">&nbsp;</div>\r\n<dir id=\"info-company\" class=\"affclubs\">\r\n<div class=\"col-md-4\">\r\n<h4><strong>RAJASTHAN</strong></h4>\r\n<h5><strong>FIELD CLUB</strong></h5>\r\n<p style=\"line-height: 20px;\">Fatehpura, Udaipur-313001</p>\r\n<p>0294-24161499 / 2560105</p>\r\n<p>0294 - 2421312</p>\r\n<p>fieldclubindia@fieldclubindia.com</p>\r\n<p>www.fieldclubindia.com</p>\r\n</div>\r\n<div class=\"col-md-4\">\r\n<h4><strong>ANDHRA PRADESH</strong></h4>\r\n<h5><strong>I.B.P CENTURY CLUB</strong></h5>\r\n<p style=\"line-height: 20px;\">D.No.15-3-15, Maharanipeta, Visakhapatnam-530002</p>\r\n<p>0891-2562573 / 2567471</p>\r\n<p>0891-2714102</p>\r\n<p>secretary@ibpcenturyclub.in</p>\r\n<p>www.ibpcenturyclub.in</p>\r\n</div>\r\n<div class=\"col-md-4\">\r\n<h4><strong>TAMILNADU</strong></h4>\r\n<h5><strong>THE KERALA CLUB COIMBATORE</strong></h5>\r\n<p style=\"line-height: 20px;\">78-80,A.T.T.Colony, Coimbatore-641018</p>\r\n<p>2216231 / 2215178 / 2213642</p>\r\n<p>thekeralaclub@gmail.com</p>\r\n<p>www.tumkurclub.in</p>\r\n</div>\r\n</dir>\r\n<div class=\"clearfix\">&nbsp;</div>\r\n<dir id=\"info-company\" class=\"affclubs\">\r\n<div class=\"col-md-4\">\r\n<h4><strong>WEST BENGAL</strong></h4>\r\n<h5><strong>CALCUTTA ROWING CLUB</strong></h5>\r\n<p style=\"line-height: 20px;\">15, Rabindra Sarobar,Kolkata-700029</p>\r\n<p>2419-8915, 2419-8914</p>\r\n<p>hony.secy@calcuttarowingclub.com</p>\r\n<p>www.culcuttarowingclub.co.in</p>\r\n</div>\r\n</dir>\r\n<div class=\"clearfix\">&nbsp;</div>\r\n<p class=\"desc_news\">&nbsp;</p>\r\n<h4 class=\"desc_news\"><strong>Members of Affiliated Clubs Terms &amp; Conditions:</strong></h4>\r\n<p class=\"desc_news\">The Executive Committee may allow affiliation to any other club in India or abroad of comparable standards and facilities with the club on a reciprocal basis, subject to the following conditions.</p>\r\n<p><strong>a)</strong> Shall have the privilege to use the club facilities for Seven Days in a month</p>\r\n<p><strong>b)</strong> Affiliated members will be allowed to utilize our Bar / Restaurant / Tennis and Guest Rooms.</p>\r\n<p><strong>c)</strong> Affiliated club Members can bring his / her spouse and their children only</p>\r\n<p><strong>d)</strong> Guest fee excluded for affiliated members and their family on reciprocal basis</p>\r\n<p><strong>e)</strong> Member&rsquo;s Children below the age of 16 years need to pay guest fee</p>\r\n<p><strong>f)</strong> Have the privilege of using our residential rooms for a period not exceeding seven days in a month at a time, on payment of rent in advance; extension of stay shall be subject to the discreation of the Hon.Secretary</p>\r\n<p><strong>g)</strong> Affiliated club members, including spouse &amp; children are requested to produce introduction letter or Current validity ID from the respective clubs and of registering their details in the Guest Register at Reception Counter</p>\r\n<p><strong>h)</strong> Affiliated club members can bring 4 (Four) guest with them. This is allowed only on reciprocal basis. Such guests can utilize only Restaurant and Bar</p>\r\n<p><strong>i)</strong> They have to settle all their bills in cash before leaving the club</p>\r\n<p><strong>j)</strong> Infraction and violation of the rules and regulation of the club on the part of the members of the affiliated clubs will entail disciplinary action, in the first instance by the Hon.Secretary with due report to the affiliated club concerned, not excluding suspension of the use of facilities for such periods as may be deemed warranted</p>\r\n<p><strong>k)</strong> Members of affiliated club will have no powers of voting and serving as members of any committees of the club</p>\r\n</div>', 'uploads/pageImg/ksltapage122320.jpg', '', 0, 0, '2016-09-30 06:37:53', '2017-10-16 13:46:03', 'A', 'affilated-clubs', 'affilated-clubs'),
(15, 0, 'AITA', 'aita-tournament', 'KSLTA-TIMES OF KARNATAKA AITA WOMENS TOURNAMENT', '20TH AUGUST TO 26TH AUGUST 2016', 'KSLTA KARNATAKA TIMES AITA-WOMEN\'S SINGLES DRAW', '<h3 class=\"news-title n-match\">KSLTA KARNATAKA TIMES AITA-WOMEN\'S SINGLES DRAW</h3>', 'uploads/pageImg/ksltapage9991.jpg', '', 0, 0, '2016-09-30 06:45:48', '2016-10-22 08:00:54', 'A', 'aita', 'aita'),
(21, NULL, 'Contact Us', 'contactus', 'Contact US', 'Keep In Touch with US', '', '<div class=\"content-link col-md-6\">\r\n<div id=\"info-company\" class=\"top-score-title align-center\" style=\"margin: 50px 0px;\">\r\n<h3 style=\"margin-top: 0px; text-transform: capitalize; font-size: 24px;\">Karnataka State Lawn Tennis Association</h3>\r\n<p style=\"line-height: 20px;\">KSLTA Tennis Stadium Stadium, <br /> Cubbon Park , Bengaluru - 560001</p>\r\n<br />\r\n<p><i class=\"fa fa-phone\"></i> +91-80-22869797 / 22863636 / 22861010</p>\r\n<p><i class=\"fa fa-fax\"></i> Fax : 080-22860099</p>\r\n<h5>KSLTA : info@kslta.com</h5>\r\n<h5>Accounts : accounts@kslta.com&nbsp;&nbsp;</h5>\r\n<h5>Club House&nbsp; : clubhouse@kslta.com</h5>\r\n<h5>Guest Rooms : rooms@kslta.com</h5>\r\n<h4 style=\"margin-top: 30px; font-size: 24px;\"><strong>&nbsp;</strong></h4>\r\n</div>\r\n</div>\r\n<div class=\"content-link col-md-6\">\r\n<div id=\"info-company\" class=\"top-score-title align-center\" style=\"margin: 50px 0px;\"><br /><iframe width=\"100%\" height=\"450\" style=\"border: 0;\" src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15551.787107569862!2d77.594486!3d12.975256!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc91b74e54e0d400f!2sKarnataka+State+Lawn+Tennis+Association!5e0!3m2!1sen!2sin!4v1472971591340\" frameborder=\"0\" allowfullscreen=\"allowfullscreen\"></iframe></div>\r\n</div>', 'uploads/pageImg/ksltapage011616.jpg', NULL, 0, 0, '2016-10-25 00:56:12', '2019-11-19 14:11:39', 'A', 'dfgds', 'dfgdsf'),
(22, NULL, 'Bangalore Open 2017', 'BangaloreOpen2017', 'Bangalore Open 2017', 'US$ 100,000 event, 25-25 Nov, 2017', 'Fill the Below Form to Get Tickets', '<p><iframe width=\"760\" height=\"500\" src=\"https://docs.google.com/forms/d/e/1FAIpQLSeLf_JBdIuQ2JTgVD_O_nIJfWzo-0wyAIAlKhvrQexEwPQPWA/viewform?embedded=true\" frameborder=\"0\" marginheight=\"0\" marginwidth=\"0\">Loading...</iframe></p>', 'uploads/pageImg/ksltapage000439.jpg', NULL, 0, 0, '2017-11-17 00:04:39', '2017-11-17 06:06:41', 'A', '', ''),
(23, NULL, 'calander', 'calander', 'TOURNAMENT - CALENDAR', 'KARNATAKA', 'TOURNAMENT - CALENDAR', '', NULL, 'uploads/pageImg/ksltapage021401.jpg', 0, 1, '2017-12-06 02:14:01', '2019-07-26 09:07:19', 'A', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ksl_player_events`
--

CREATE TABLE `ksl_player_events` (
  `ue_autoid` bigint(20) NOT NULL,
  `ue_userid` varchar(50) NOT NULL,
  `ue_tournamentid` varchar(50) NOT NULL,
  `ue_eventid` varchar(50) NOT NULL,
  `ue_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ksl_player_events`
--

INSERT INTO `ksl_player_events` (`ue_autoid`, `ue_userid`, `ue_tournamentid`, `ue_eventid`, `ue_created`) VALUES
(3, '2', '1', '1', '2016-05-11 14:59:06'),
(4, '2', '1', '2', '2016-05-11 14:59:06'),
(5, '3', '1', '2', '2016-05-11 14:59:24'),
(6, '3', '1', '3', '2016-05-26 06:12:11'),
(7, '3', '1', '4', '2016-05-26 06:12:11'),
(8, '1', '3', '42', '2016-05-26 06:13:23'),
(9, '4', '1', '1', '2016-05-27 12:41:54'),
(10, '4', '1', '2', '2016-05-27 12:41:54'),
(11, '5', '1', '1', '2016-06-02 08:14:28'),
(12, '5', '1', '2', '2016-06-02 08:14:28'),
(13, '6', '8', '57', '2016-06-06 14:23:03'),
(14, '6', '8', '58', '2016-06-06 14:23:03'),
(15, '6', '8', '59', '2016-06-06 14:23:03'),
(16, '6', '8', '60', '2016-06-06 14:23:03'),
(17, '7', '13', '74', '2016-07-06 06:48:26'),
(18, '7', '13', '75', '2016-07-06 06:48:26'),
(19, '8', '13', '76', '2016-07-06 06:49:24'),
(20, '8', '13', '77', '2016-07-06 06:49:24'),
(21, '9', '13', '74', '2016-07-06 06:58:07'),
(22, '9', '13', '75', '2016-07-06 06:58:07'),
(23, '10', '13', '74', '2016-07-06 07:02:05'),
(24, '10', '13', '75', '2016-07-06 07:02:05'),
(25, '11', '13', '74', '2016-07-06 07:02:49'),
(26, '11', '13', '75', '2016-07-06 07:02:49'),
(27, '13', '10', '64', '2016-07-13 12:58:12'),
(28, '14', '1', '1', '2016-07-13 14:33:45'),
(29, '14', '1', '2', '2016-07-13 14:33:45'),
(30, '15', '4', '49', '2016-07-13 14:55:39'),
(31, '16', '1', '1', '2016-08-30 06:46:21'),
(32, '17', '1', '2', '2016-08-30 06:53:31'),
(33, '18', '1', '1', '2016-08-30 06:54:12');

-- --------------------------------------------------------

--
-- Table structure for table `ksl_player_register`
--

CREATE TABLE `ksl_player_register` (
  `reg_id` int(11) NOT NULL,
  `tourn_id` int(10) DEFAULT NULL,
  `reg_first_name` varchar(70) NOT NULL,
  `reg_last_name` varchar(70) NOT NULL,
  `reg_gender` varchar(100) NOT NULL,
  `reg_dob` date NOT NULL,
  `reg_email` varchar(70) NOT NULL,
  `reg_player_category` int(10) NOT NULL,
  `reg_player_id` varchar(15) NOT NULL,
  `reg_city` varchar(70) NOT NULL,
  `reg_mobile` varchar(15) NOT NULL,
  `registration_date` date NOT NULL,
  `reg_created_at` datetime NOT NULL,
  `reg_last_modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reg_status` enum('A','I') NOT NULL COMMENT 'A=Active, I=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ksl_player_register`
--

INSERT INTO `ksl_player_register` (`reg_id`, `tourn_id`, `reg_first_name`, `reg_last_name`, `reg_gender`, `reg_dob`, `reg_email`, `reg_player_category`, `reg_player_id`, `reg_city`, `reg_mobile`, `registration_date`, `reg_created_at`, `reg_last_modified_at`, `reg_status`) VALUES
(25, NULL, 'ARYA ', 'RAJATH RAJ', 'male', '2003-05-15', '', 1, '415295', 'Bangalore', '', '2017-03-10', '2017-03-10 01:29:07', '2017-03-10 07:29:07', 'A'),
(26, NULL, 'MANAS', 'GIRISH', 'male', '2002-06-27', '', 1, '419882', 'Bangalore', '', '2017-03-10', '2017-03-10 01:29:57', '2017-03-10 07:29:57', 'A'),
(27, NULL, 'YU ', 'KINOSHITA', 'male', '2002-02-13', '', 1, '419977', 'Bangalore', '', '2017-03-10', '2017-03-10 01:30:39', '2017-03-10 07:30:39', 'A'),
(28, NULL, 'AMEETESH ', 'SAXENA', 'male', '2004-03-09', '', 1, '423054', 'Bangalore', '', '2017-03-10', '2017-03-10 01:31:13', '2017-03-10 07:31:13', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `ksl_player_register_tbl`
--

CREATE TABLE `ksl_player_register_tbl` (
  `reg_id` int(11) NOT NULL,
  `tourn_id` int(11) NOT NULL,
  `reg_first_name` varchar(70) NOT NULL,
  `reg_last_name` varchar(70) NOT NULL,
  `reg_dob` date NOT NULL,
  `reg_email` varchar(70) NOT NULL,
  `reg_player_category` int(11) NOT NULL,
  `reg_player_id` int(11) NOT NULL,
  `reg_city` varchar(50) NOT NULL,
  `reg_mobile` varchar(15) NOT NULL,
  `registration_date` date NOT NULL,
  `reg_created_at` datetime NOT NULL,
  `reg_last_modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reg_status` enum('A','I') NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ksl_slider_banner`
--

CREATE TABLE `ksl_slider_banner` (
  `banner_id` int(11) NOT NULL,
  `banner_name` varchar(250) NOT NULL,
  `banner_url` varchar(750) NOT NULL,
  `banner_image` varchar(500) NOT NULL,
  `banner_order_flow` int(11) DEFAULT NULL,
  `banner_date` datetime NOT NULL,
  `banner_created_at` datetime NOT NULL,
  `banner_modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `banner_status` enum('A','I') NOT NULL COMMENT 'A= Active, I=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ksl_slider_banner`
--

INSERT INTO `ksl_slider_banner` (`banner_id`, `banner_name`, `banner_url`, `banner_image`, `banner_order_flow`, `banner_date`, `banner_created_at`, `banner_modified_at`, `banner_status`) VALUES
(74, 'RG-NS', '', 'uploadimage/sliderbanner/28547.jpg', NULL, '2018-05-03 07:08:00', '2018-05-03 07:08:00', '2018-05-03 12:08:00', 'A'),
(82, 'ATP Challenger-17', '', 'uploadimage/sliderbanner/55540.jpeg', NULL, '2018-08-28 08:59:05', '2018-08-28 08:59:05', '2018-08-28 13:59:05', 'A'),
(88, 'ATP Challenger-17', '', 'uploadimage/sliderbanner/Press Conf3220.jpg', NULL, '2018-08-28 09:02:46', '2018-08-28 09:02:46', '2018-08-28 14:02:46', 'A'),
(93, 'ATP Challenger-18', '', 'uploadimage/sliderbanner/Snapseed (1)3793.jpg', NULL, '2018-10-15 04:00:17', '2018-10-15 04:00:17', '2018-10-15 09:00:17', 'A'),
(95, 'ATP Challenger - 18', '', 'uploadimage/sliderbanner/Bengaluru Open 1561.jpg', NULL, '2018-11-13 09:57:58', '2018-11-13 09:57:58', '2018-11-13 15:57:58', 'A'),
(103, '	ATP Challenger-18', '', 'uploadimage/sliderbanner/Final6990.jpeg', NULL, '2018-11-21 06:10:54', '2018-11-21 06:10:54', '2018-11-21 12:10:54', 'A'),
(104, 'ATP Challenger-18', '', 'uploadimage/sliderbanner/Winner6637.jpg', NULL, '2018-11-21 06:13:57', '2018-11-21 06:13:57', '2018-11-21 12:13:57', 'A'),
(105, 'ATP Challenger-18', '', 'uploadimage/sliderbanner/Final4688.jpeg', NULL, '2018-11-21 06:14:13', '2018-11-21 06:14:13', '2018-11-21 12:14:13', 'A'),
(106, 'ATP Challenger-18', '', 'uploadimage/sliderbanner/Final16344.jpeg', NULL, '2018-11-21 06:14:30', '2018-11-21 06:14:30', '2018-11-21 12:14:30', 'A'),
(107, 'ATP Challenger-18', '', 'uploadimage/sliderbanner/Final24273.jpeg', NULL, '2018-11-21 06:14:43', '2018-11-21 06:14:43', '2018-11-21 12:14:43', 'A'),
(109, 'ATP Challenger-18', '', 'uploadimage/sliderbanner/Final45478.jpeg', NULL, '2018-11-21 06:15:35', '2018-11-21 06:15:35', '2018-11-21 12:15:35', 'A'),
(111, 'Tennis Clinic With Arantxa Sanchez', '', 'uploadimage/sliderbanner/RPG_40734501.jpg', NULL, '2019-05-18 00:20:54', '2019-05-18 00:20:54', '2019-05-18 05:20:54', 'A'),
(114, 'Bengaluru Open 2020', '', 'uploadimage/sliderbanner/Launch Ceremony4531.jpg', NULL, '2020-02-09 01:42:03', '2020-02-09 01:42:03', '2020-02-09 07:42:03', 'A'),
(115, 'Bengaluru Open 2020', '', 'uploadimage/sliderbanner/19636.jpg', NULL, '2020-02-16 11:43:48', '2020-02-16 11:43:48', '2020-02-16 17:43:48', 'A'),
(116, 'Bengaluru Open 2020', '', 'uploadimage/sliderbanner/29983.jpg', NULL, '2020-02-16 11:44:20', '2020-02-16 11:44:20', '2020-02-16 17:44:20', 'A'),
(117, 'Bengaluru Open 2020', '', 'uploadimage/sliderbanner/37788.jpg', NULL, '2020-02-16 11:44:36', '2020-02-16 11:44:36', '2020-02-16 17:44:36', 'A'),
(118, 'Bengaluru Open 2020', '', 'uploadimage/sliderbanner/47261.jpg', NULL, '2020-02-16 11:44:55', '2020-02-16 11:44:55', '2020-02-16 17:44:55', 'A'),
(119, 'Bengaluru Open 2020', '', 'uploadimage/sliderbanner/IMG_05901776.jpg', NULL, '2020-02-22 05:12:43', '2020-02-22 05:12:43', '2020-02-22 11:12:43', 'A'),
(120, 'Bengaluru Open 2020', '', 'uploadimage/sliderbanner/IMG_07889960.jpg', NULL, '2020-02-22 05:12:58', '2020-02-22 05:12:58', '2020-02-22 11:12:58', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `ksl_tournaments_event_matche`
--

CREATE TABLE `ksl_tournaments_event_matche` (
  `tem_id` bigint(20) NOT NULL,
  `tem_tournament_id` varchar(50) NOT NULL,
  `tem_event_id` varchar(50) NOT NULL,
  `tem_player_id` varchar(50) NOT NULL,
  `tem_player_reg_id` varchar(50) NOT NULL,
  `tem_location` varchar(255) NOT NULL,
  `tem_scheduled_time` varchar(255) NOT NULL,
  `tem_scheduled_date` date NOT NULL,
  `tem_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ksl_tournaments_event_matche`
--

INSERT INTO `ksl_tournaments_event_matche` (`tem_id`, `tem_tournament_id`, `tem_event_id`, `tem_player_id`, `tem_player_reg_id`, `tem_location`, `tem_scheduled_time`, `tem_scheduled_date`, `tem_timestamp`) VALUES
(1, '1', '1', '0', '5656ffffff ', 'location ', 'sdfsd 4 pm', '2016-08-18', '2016-07-05 09:52:15'),
(2, '1', '1', '2', 'test123 ', 'chennai ', '11 am', '2016-07-05', '2016-07-05 12:16:22'),
(3, '13', '74', '2', '1017 ', 'b1', '10 am', '2016-07-06', '2016-07-06 11:10:26'),
(4, '13', '74', '3', '1019 ', 'b1', '10 am', '2016-07-06', '2016-07-06 11:10:32'),
(5, '13', '74', '0', '1015 ', 'b0', '9 am', '2016-07-06', '2016-07-06 07:06:01'),
(6, '13', '74', '1', '1016 ', 'b0', ' 9 am', '2016-07-06', '2016-07-06 07:06:15'),
(7, '13', '75', '0', '1015 ', 'v1', '1 pm', '2016-07-08', '2016-07-06 07:18:26'),
(8, '13', '75', '1', '1016 ', 'v1', '1 pm', '2016-07-07', '2016-07-06 07:18:49'),
(9, '13', '75', '2', '1017 ', 'v2', '2 pm', '2016-07-09', '2016-07-06 07:19:10'),
(10, '13', '75', '3', '1019 ', 'v2', '1 pm', '2016-07-09', '2016-07-06 07:19:29'),
(11, '13', '74', '30', '1015 ', 't1', '4 pm', '2016-07-10', '2016-07-06 07:31:55'),
(12, '13', '74', '31', '1016 ', 't1', '5pm', '2016-07-10', '2016-07-06 07:32:22'),
(13, '13', '74', '32', '1016 ', 'u6', '8 pm', '2016-07-11', '2016-07-06 07:51:13'),
(14, '13', '74', '4', '1015 ', 'ss', '34', '2016-07-06', '2016-07-06 13:05:40'),
(15, '13', '74', '5', '1016 ', 'ss', '343', '2016-07-06', '2016-07-06 13:05:49'),
(16, '13', '74', '9', '5656ffffff ', 'sdf', 'sdf', '2016-09-26', '2016-09-26 11:32:09'),
(17, '92', '429', '0', '', 'Bengaluru', '9.00AM', '2017-01-23', '2017-01-22 10:41:40'),
(18, '92', '457', '0', '415295 ', 'kslta, Bangalore', '9:30 AM', '2017-01-21', '2017-03-10 07:37:57'),
(19, '92', '457', '1', '419882 ', 'kslta, Bangalore', '9:30 AM', '2017-01-21', '2017-03-10 13:07:55'),
(20, '92', '457', '3', '423054 ', 'kslta, bangalore', '9:30 AM', '2017-01-22', '2017-03-10 13:14:23'),
(21, '92', '457', '2', '419977 ', 'Kslta, Bangalore', '9:30 AM', '2017-01-22', '2017-03-10 07:35:30'),
(22, '92', '457', '4', '415295 ', 'kslta, Bangalore', '9:30 AM', '2017-01-23', '2017-03-10 07:41:19'),
(23, '92', '457', '5', '423054 ', 'kslta, Banglore', '9:30 AM', '2017-01-23', '2017-03-10 07:41:45');

-- --------------------------------------------------------

--
-- Table structure for table `ksl_tournaments_event_result`
--

CREATE TABLE `ksl_tournaments_event_result` (
  `ter_id` bigint(20) NOT NULL,
  `ter_tournament_id` varchar(50) NOT NULL,
  `ter_event_id` varchar(50) NOT NULL,
  `ter_player_id` varchar(50) NOT NULL,
  `ter_player_reg_id` varchar(50) NOT NULL,
  `ter_set` varchar(50) NOT NULL,
  `ter_result` bigint(20) NOT NULL,
  `ter_timestamp` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ksl_tournaments_event_result`
--

INSERT INTO `ksl_tournaments_event_result` (`ter_id`, `ter_tournament_id`, `ter_event_id`, `ter_player_id`, `ter_player_reg_id`, `ter_set`, `ter_result`, `ter_timestamp`) VALUES
(1, '1', '1', '0', '5656ffffff ', '6,7,2,45', 60, '2016-07-05'),
(2, '1', '1', '8', 'test123 ', '3,4', 7, '2016-07-05'),
(3, '1', '1', '2', 'test123 ', '3,4', 7, '2016-07-05'),
(4, '13', '74', '0', '1015 ', '6,3,2', 11, '2016-07-06'),
(5, '13', '74', '1', '1016 ', '2,4,3', 9, '2016-07-06'),
(6, '13', '74', '30', '1015 ', '2,3,4', 9, '2016-07-06'),
(7, '13', '74', '31', '1016 ', '3,5,6', 14, '2016-07-06'),
(8, '13', '74', '32', '1016 ', '3,4,5', 12, '2016-07-06'),
(9, '13', '74', '9', '5656ffffff ', '34', 34, '2016-09-26'),
(10, '92', '457', '0', '415295 ', '2,2,3', 7, '2017-03-10'),
(11, '92', '457', '1', '419882 ', '1,1,4', 6, '2017-03-10'),
(12, '92', '457', '2', '419977 ', '1,1,1', 3, '2017-03-10'),
(13, '92', '457', '3', '423054 ', '2,1,2', 5, '2017-03-10'),
(14, '92', '457', '4', '415295 ', '1,1,3', 5, '2017-03-10'),
(15, '92', '457', '5', '423054 ', '2,2,2', 6, '2017-03-10');

-- --------------------------------------------------------

--
-- Table structure for table `ksl_tournaments_tbl`
--

CREATE TABLE `ksl_tournaments_tbl` (
  `tourn_id` int(11) NOT NULL,
  `tourn_title` varchar(300) NOT NULL,
  `tourn_category` varchar(30) DEFAULT NULL,
  `tourn_sub_category` varchar(30) DEFAULT NULL,
  `tourn_venue` varchar(100) NOT NULL,
  `tourn_venue_address1` varchar(200) NOT NULL,
  `tourn_venue_address2` varchar(200) NOT NULL,
  `tourn_venue_city` varchar(100) NOT NULL,
  `tourn_from_date` date NOT NULL,
  `tourn_to_date` date NOT NULL,
  `tourn_last_submission_date` date NOT NULL,
  `tourn_withdrl_date` date NOT NULL,
  `tourn_description` varchar(1500) NOT NULL,
  `tourn_price_money` varchar(100) CHARACTER SET utf8 NOT NULL,
  `tourn_tournament_image` varchar(350) DEFAULT NULL,
  `tourn_register_status` enum('O','C') NOT NULL COMMENT 'O= Open, C=Close',
  `tourn_asso_title` varchar(200) NOT NULL,
  `tourn_asso_address1` varchar(200) DEFAULT NULL,
  `tourn_asso_address2` varchar(200) DEFAULT NULL,
  `tourn_asso_head` varchar(70) NOT NULL,
  `tourn_refree1` varchar(100) NOT NULL,
  `tourn_refree2` varchar(100) NOT NULL,
  `tourn_refree3` varchar(100) NOT NULL,
  `tourn_link` varchar(100) NOT NULL,
  `tourn_created_date` date NOT NULL,
  `tourn_lastmodified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tourn_status` enum('A','R') NOT NULL DEFAULT 'A' COMMENT 'A=Active, R=Removed',
  `sponser_name_of_organisation` varchar(200) DEFAULT NULL,
  `sponser_address1` varchar(300) DEFAULT NULL,
  `sponser_address2` varchar(300) DEFAULT NULL,
  `sponser_contact_person_name` varchar(100) DEFAULT NULL,
  `sponser_phone` varchar(20) DEFAULT NULL,
  `sponser_email` varchar(200) DEFAULT NULL,
  `tour_event_category` varchar(20) NOT NULL,
  `tour_event_subcategory` varchar(20) NOT NULL,
  `tourn_factsheet` varchar(750) DEFAULT NULL,
  `tourn_factsheet_uploaded_at` datetime DEFAULT NULL,
  `tourn_facesheet_status` enum('A','I') NOT NULL DEFAULT 'A' COMMENT 'A=Active, I= Inactive',
  `tourn_gallery_id` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ksl_tournaments_tbl`
--

INSERT INTO `ksl_tournaments_tbl` (`tourn_id`, `tourn_title`, `tourn_category`, `tourn_sub_category`, `tourn_venue`, `tourn_venue_address1`, `tourn_venue_address2`, `tourn_venue_city`, `tourn_from_date`, `tourn_to_date`, `tourn_last_submission_date`, `tourn_withdrl_date`, `tourn_description`, `tourn_price_money`, `tourn_tournament_image`, `tourn_register_status`, `tourn_asso_title`, `tourn_asso_address1`, `tourn_asso_address2`, `tourn_asso_head`, `tourn_refree1`, `tourn_refree2`, `tourn_refree3`, `tourn_link`, `tourn_created_date`, `tourn_lastmodified_date`, `tourn_status`, `sponser_name_of_organisation`, `sponser_address1`, `sponser_address2`, `sponser_contact_person_name`, `sponser_phone`, `sponser_email`, `tour_event_category`, `tour_event_subcategory`, `tourn_factsheet`, `tourn_factsheet_uploaded_at`, `tourn_facesheet_status`, `tourn_gallery_id`) VALUES
(23, 'Talent Series Boys & Girls U-14', NULL, NULL, 'Tennis Temple', 'Tennis Temple', '', 'Bangalore', '2016-04-04', '2016-04-08', '2016-04-02', '2016-04-03', 'Tallent Series Boys & Girls U-14', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2017-11-16 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', NULL, '0000-00-00 00:00:00', 'A', NULL),
(24, 'Mens & Womens', NULL, NULL, 'Energy Tennis Centre', 'Energy Tennis Centre\r\n', '', 'Bangalore', '2016-04-04', '2016-04-09', '2016-04-03', '2016-04-03', 'Mens & Womens', '', NULL, 'O', 'Energy Tennis Centre', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', NULL, '0000-00-00 00:00:00', 'A', NULL),
(25, 'Tallent Series Boys & Girls U-16', NULL, NULL, 'KSLTA', 'KSLTA, Cubbon Park, Bangalore', '', 'Bangalore', '2016-04-11', '2016-04-16', '2016-04-09', '2016-04-10', 'Tallent Series Boys & Girls U-16', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '12', NULL, '0000-00-00 00:00:00', 'A', NULL),
(26, 'Tallent Series Mens & Womens', NULL, NULL, 'Energy Tennis Centre', 'Energy Tennis Centre\r\n', '', 'Bangalore', '2016-04-11', '2016-04-16', '2016-04-09', '2016-04-10', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', NULL, '0000-00-00 00:00:00', 'A', NULL),
(27, 'Tallent Series Boys & Girls U-10', NULL, NULL, 'NYKE Sportz', 'NYKE Sportz\r\n', '', 'Bangalore', '2016-04-18', '2016-04-23', '2016-04-16', '2016-04-17', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '9', NULL, '0000-00-00 00:00:00', 'A', NULL),
(28, 'Tallent Series Boys & Girls U-14', NULL, NULL, 'NYKE Sportz', 'NYKE Sportz\r\n', '', 'Bangalore', '2016-04-18', '2016-04-23', '2016-04-16', '2016-04-17', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', NULL, '0000-00-00 00:00:00', 'A', NULL),
(29, 'Mens', NULL, NULL, 'Laureates', 'Laureates\r\n', '', 'Bangalore', '2016-04-18', '2016-04-23', '2016-04-16', '2016-04-17', '', '50,000', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '5', '13', NULL, '0000-00-00 00:00:00', 'A', NULL),
(30, 'Champion Series Boys & Girls U-18', NULL, NULL, 'Century Club', 'Century Club\r\n', '', 'Bangalore', '2016-04-25', '2016-04-30', '2016-04-23', '2016-04-24', '', '', NULL, 'O', 'Century Club', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '12', NULL, '0000-00-00 00:00:00', 'A', NULL),
(31, 'Tallent Series Boys & Girls U-12', NULL, NULL, 'Spagolo.Com', 'Spagolo.Com\r\n', '', 'Bangalore', '2016-04-25', '2016-04-30', '2016-04-23', '2016-04-24', '', '', NULL, 'O', 'Spagolo.Com', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', NULL, '0000-00-00 00:00:00', 'A', NULL),
(32, 'Womens', NULL, NULL, 'Laureates', 'Laureates\r\n', '', 'Bangalore', '2016-04-25', '2016-04-30', '2016-04-23', '2016-04-25', '', '50,000', NULL, 'O', 'Laureates', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', NULL, '0000-00-00 00:00:00', 'A', NULL),
(33, 'Tallent Series Boys & Girls U-14', NULL, NULL, 'Mysore Tennis Club', 'Mysore Tennis Club\r\n', '', 'Mysore', '2016-05-02', '2016-05-07', '2016-05-01', '2016-04-30', '', '', NULL, 'O', 'Mysore Tennis Club', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', NULL, '0000-00-00 00:00:00', 'A', NULL),
(34, 'Champion Series Boys & Girls U-12', NULL, NULL, 'Elite Tennis Academy', 'Elite Tennis Academy\r\n', '', 'Bangalore', '2016-05-09', '2016-05-14', '2016-05-07', '2016-05-08', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', NULL, '0000-00-00 00:00:00', 'A', NULL),
(35, 'Champion Series Boys & Girls U-18', NULL, NULL, 'KTPPA', 'KTPPA\r\n', '', 'Bangalore', '2016-05-09', '2016-05-14', '2016-05-07', '2016-05-08', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '12', NULL, '0000-00-00 00:00:00', 'A', NULL),
(36, 'Champion Series Boys & Girls U-16', NULL, NULL, 'Sports Cult', 'Sports Cult\r\n', '', 'Bangalore', '2016-05-16', '2016-05-21', '2016-05-14', '2016-05-15', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', NULL, '0000-00-00 00:00:00', 'A', NULL),
(37, 'Tallent Series Boys & Girls U-10', NULL, NULL, 'KSLTA', 'KSLTA, Cubbon Park, Bangalore', '', 'Bangalore', '2016-05-16', '2016-05-21', '2016-05-14', '2016-05-15', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '9', NULL, '0000-00-00 00:00:00', 'A', NULL),
(38, 'Tallent Series Boys & Girls U-16', NULL, NULL, 'Mysore Tennis Club', 'Mysore Tennis Club\r\n', '', 'Mysore', '2016-05-23', '2016-05-28', '2016-05-21', '2016-05-22', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', NULL, '0000-00-00 00:00:00', 'A', NULL),
(39, 'Tallent Series Boys & Girls U-10', NULL, NULL, 'KSLTA', 'KSLTA, Cubbon Park, Bangalore', '', 'Bangalore', '2016-05-23', '2016-05-28', '2016-05-21', '2016-05-22', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '9', NULL, '0000-00-00 00:00:00', 'A', NULL),
(40, 'Tallent Series Boys & Girls U-10', NULL, NULL, 'KSLTA', 'KSLTA, Cubbon Park, Bangalore', '', 'Bangalore', '2016-05-30', '2016-06-04', '2016-05-28', '2016-05-29', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '9', NULL, '0000-00-00 00:00:00', 'A', NULL),
(41, 'Tallent Series Boys & Girls U-14', NULL, NULL, 'KSLTA', 'KSLTA, Cubbon Park, Bangalore', '', 'Bangalore', '2016-06-06', '2016-06-11', '2016-06-04', '2016-06-05', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', NULL, '0000-00-00 00:00:00', 'A', NULL),
(42, 'Tallent Series Boys & Girls U-10', NULL, NULL, 'KSLTA', 'KSLTA, Cubbon Park, Bangalore', '', 'Bangalore', '2016-06-13', '2016-06-18', '2016-06-11', '2016-06-12', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '9', NULL, '0000-00-00 00:00:00', 'A', NULL),
(43, 'Champion Series Boys & Girls U-14', NULL, NULL, 'KTPPA', 'KTPPA', '', 'Bangalore', '2016-06-13', '2016-06-18', '2016-06-11', '2016-06-12', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', NULL, '0000-00-00 00:00:00', 'A', NULL),
(44, 'Talent Series Boys & Girls U-14', NULL, NULL, 'Tennis Temple', 'Tennis Temple', '', 'Bangalore', '2016-06-20', '2016-06-25', '2016-06-18', '2016-06-19', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2017-11-16 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', NULL, '0000-00-00 00:00:00', 'A', NULL),
(45, 'Champion Series Boys & Girls U-16', NULL, NULL, 'SAT Sports Pvt Ltd', 'SAT Sports Pvt Ltd\r\n', '', 'Bangalore', '2016-06-27', '2016-07-02', '2016-06-25', '2016-06-26', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', NULL, '0000-00-00 00:00:00', 'A', NULL),
(46, 'Tallent Series Boys & Girls U-12', NULL, NULL, 'KSLTA', 'KSLTA, Cubbon Park, Bangalore', '', 'Bangalore', '2016-07-04', '2016-07-09', '2016-07-02', '2016-07-03', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', NULL, '0000-00-00 00:00:00', 'A', NULL),
(47, 'Tallent Series Boys & Girls U-14', NULL, NULL, 'KSLTA', 'KSLTA, Cubbon Park, Bangalore', '', 'Bangalore', '2016-07-11', '2016-07-16', '2016-07-09', '2016-07-10', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', NULL, '0000-00-00 00:00:00', 'A', NULL),
(48, 'Tallent Series Boys & Girls U-16', NULL, NULL, 'KSLTA', 'KSLTA, Cubbon Park, Bangalore', '', 'Bangalore', '2016-07-18', '2016-07-23', '2016-07-16', '2016-07-17', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', NULL, '0000-00-00 00:00:00', 'A', NULL),
(49, 'Champion Series Boys & Girls U-18', NULL, NULL, 'Tennis Temple', 'Tennis Temple\r\n', '', 'Bangalore', '2016-07-25', '2016-07-30', '2016-07-23', '2016-07-24', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '12', NULL, '0000-00-00 00:00:00', 'A', NULL),
(50, 'Champion Series Boys & Girls U-14', NULL, NULL, 'SAT Sports Pvt Ltd', 'SAT Sports Pvt Ltd\r\n', '', 'Bangalore', '2016-08-01', '2016-08-06', '2016-07-30', '2016-07-31', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', NULL, '0000-00-00 00:00:00', 'A', NULL),
(51, 'Mens', NULL, NULL, 'The Bangalore City Institute', 'The Bangalore City Institute\r\n', '', 'Bangalore', '2016-08-01', '2016-08-06', '2016-07-30', '2016-07-31', '', '50,000', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '5', '13', NULL, '0000-00-00 00:00:00', 'A', NULL),
(52, 'Tallent Series Boys & Girls U-10', NULL, NULL, 'KSLTA', 'KSLTA, Cubbon Park, Bangalore', '', 'Bangalore', '2016-08-08', '2016-08-13', '2016-08-06', '2016-08-07', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '9', NULL, '0000-00-00 00:00:00', 'A', NULL),
(53, 'Tallent Series Boys & Girls U-16', NULL, NULL, 'Tennis Temple', 'Tennis Temple\r\n', '', 'Bangalore', '2016-08-15', '2016-08-20', '2016-08-13', '2016-08-14', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', NULL, '0000-00-00 00:00:00', 'A', NULL),
(54, 'Womens', NULL, NULL, 'KSLTA', 'KSLTA, Cubbon Park, Bangalore', '', 'Bangalore', '2016-08-22', '2016-08-27', '2016-08-20', '2016-08-21', '', '50,000', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '6', '14', NULL, '0000-00-00 00:00:00', 'A', NULL),
(55, 'Tallent Series Boys & Girls U-14', NULL, NULL, 'Topspin Tennis Academy', 'Topspin Tennis Academy\r\n', '', 'Bangalore', '2016-08-22', '2016-08-27', '2016-08-20', '2016-08-21', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', NULL, '0000-00-00 00:00:00', 'A', NULL),
(56, 'Mens', NULL, NULL, 'Tennis Temple', 'Tennis Temple\r\n', '', 'Bangalore', '2016-08-29', '2016-09-03', '2016-08-27', '2016-08-28', '', '50,000', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '5', '13', NULL, '0000-00-00 00:00:00', 'A', NULL),
(57, 'Tallent Series Boys & Girls U-16', NULL, NULL, 'KTPPA', 'KTPPA\r\n', '', 'Bangalore', '2016-08-29', '2016-09-03', '2016-08-27', '2016-08-28', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', NULL, '0000-00-00 00:00:00', 'A', NULL),
(58, 'Tallent Series Boys & Girls U-10', NULL, NULL, 'Noah Tennis Academy', 'Noah Tennis Academy\r\n', '', 'Bangalore', '2016-08-29', '2016-09-02', '2016-08-27', '2016-08-28', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '9', NULL, '0000-00-00 00:00:00', 'A', NULL),
(59, 'Tallent Series Boys & Girls U-14', NULL, NULL, 'Noah Tennis Academy', 'Noah Tennis Academy', '', 'Bangalore', '2016-09-05', '2016-09-10', '2016-09-03', '2016-09-04', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', NULL, '0000-00-00 00:00:00', 'A', NULL),
(60, 'Tallent Series Boys & Girls U-12', NULL, NULL, 'Tennis Temple', 'Tennis Temple\r\n', '', 'Bangalore', '2016-09-12', '2016-09-17', '2016-09-10', '2016-09-11', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', NULL, '0000-00-00 00:00:00', 'A', NULL),
(61, 'Tallent Series Boys & Girls U-16', NULL, NULL, 'Fortune Sports Academy', 'Fortune Sports Academy\r\n', '', 'Bangalore', '2016-09-19', '2016-09-24', '2016-09-17', '2016-09-18', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', NULL, '0000-00-00 00:00:00', 'A', NULL),
(62, 'Tallent Series Boys & Girls U-10', NULL, NULL, 'Noah Tennis Academy', 'Noah Tennis Academy\r\n', '', 'Bangalore', '2016-09-26', '2016-10-01', '2016-09-24', '2016-09-25', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '9', NULL, '0000-00-00 00:00:00', 'A', NULL),
(63, 'Tallent Series Boys & Girls U-14', NULL, NULL, 'Tenvic ', 'Tenvic \r\n', '', 'Bangalore', '2016-09-26', '2016-10-01', '2016-09-24', '2016-09-25', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', NULL, '0000-00-00 00:00:00', 'A', NULL),
(64, 'Champion Series Boys & Girls U-18', NULL, NULL, 'SAT Sports Pvt Ltd', 'SAT Sports Pvt Ltd\r\n', '', 'Bangalore', '2016-09-26', '2016-10-01', '2016-09-24', '2016-09-25', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '12', NULL, '0000-00-00 00:00:00', 'A', NULL),
(65, 'Champion Series Boys & Girls U-12 3', NULL, NULL, 'Tennis Temple', 'Tennis Temple\r\n', '', 'Bangalore', '2016-10-01', '2016-10-03', '2016-09-30', '2016-09-30', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/3rd,Oct.2016 Tennis Temple52318.doc', '2016-10-25 10:20:03', 'A', NULL),
(66, 'Tallent Series 7 Boys & Girls U-14 ', NULL, NULL, 'Sol Sports Academy', 'Sol Sports Academy\r\n', '', 'Bangalore', '2016-10-07', '2016-10-10', '2016-10-05', '2016-10-05', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/9th,Oct.2016 Sol Sports12508.doc', '2016-10-25 10:27:18', 'A', NULL),
(67, 'Tallent Series 3 Boys & Girls U-10', NULL, NULL, 'SAT Sports Pvt Ltd', 'SAT Sports Pvt Ltd\r\n', '', 'Bangalore', '2016-10-08', '2016-10-10', '2016-10-05', '2016-10-05', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '9', 'uploads/factsheets/10th.Oct.2016 SAT Sports52260.doc', '2016-10-25 10:28:57', 'A', NULL),
(68, 'National Series B & G U-16', NULL, NULL, 'KTPPA', 'KTPPA', '', 'Bangalore', '2016-10-14', '2016-10-17', '2016-10-12', '2016-10-12', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/16th,Oct.2016 KTPPA78539.doc', '2016-10-25 10:31:04', 'A', NULL),
(69, 'Tallent Series 3 Boys & Girls U-12', NULL, NULL, 'Zeeshan Ali Tennis Academy', 'Zeeshan Ali Tennis Academy', '', 'Bangalore', '2016-10-14', '2016-10-17', '2016-10-12', '2016-10-12', '', '', NULL, 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/17th,Oct.2016 Zeeshanali Tennis Academy98681.doc', '2016-10-25 10:36:22', 'A', NULL),
(70, 'Champion Series 7 Boys & Girls U-18', NULL, NULL, 'Topspin Tennis Academy', 'Topspin Tennis Academy', '', 'Bangalore', '2016-10-21', '2016-10-24', '2016-10-17', '2016-10-17', '', '', 'uploads/tournamemnt_img/ksltatournament4895.jpg', 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-26 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '12', 'uploads/factsheets/24th,Oct.2016 Topspin Tennis Academy25510.xls', '2016-10-25 10:39:57', 'A', NULL),
(71, 'Tallent Series 3 Boys & Girls U-14', NULL, NULL, 'Fortune Sports Academy', 'Fortune Sports Academy', '', 'Bangalore', '2016-10-21', '2016-10-24', '2016-10-17', '2016-10-17', '', '', 'uploads/tournamemnt_img/ksltatournament5664.jpg', 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-26 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/24th,Oct.2016 Fortune Sports Academy8273.doc', '2016-10-25 10:41:41', 'A', NULL),
(72, 'Tallent Series 3 Boys & Girls U-10', NULL, NULL, 'KSLTA', 'KSLTA, Cubbon Park, Bangalore', '', 'Bangalore', '2016-10-29', '2016-10-31', '2016-10-27', '2016-10-27', '', '', 'uploads/tournamemnt_img/ksltatournament3839.jpg', 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-26 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '9', 'uploads/factsheets/31st,Oct.2016 KSLTA4845.doc', '2016-10-25 11:40:37', 'A', NULL),
(73, 'Tallent Series 3 Boys & Girls U-16', NULL, NULL, 'Proteam Tennis Academy', 'Proteam Tennis Academy', '', 'Bangalore', '2016-10-29', '2016-10-31', '2016-10-27', '2016-10-27', '', '', 'uploads/tournamemnt_img/ksltatournament2928.jpg', 'O', '', '', '', '', '', '', '', '', '2016-10-25', '2016-10-26 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/31st,Oct.2016 Proteam Tennis77.doc', '2016-10-25 11:43:27', 'A', NULL),
(74, 'Champion Series 3 Boys & Girls U-14', NULL, NULL, 'Proteam Tennis Academy', 'Bangalore', '', 'Bangalore', '2016-11-07', '2016-11-11', '2016-11-04', '2016-11-04', '', '', 'uploads/tournamemnt_img/ksltatournament8739.jpg', 'O', '', '', '', '', '', '', '', '', '2016-11-17', '2016-11-16 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', NULL, '0000-00-00 00:00:00', 'A', NULL),
(75, 'Tallent Series 7 Boys & Girls U-14', NULL, NULL, 'Mysore Tennis Club', 'Mysore', '', 'Mysore', '2016-11-14', '2016-11-18', '2016-11-11', '2016-11-11', '', '', 'uploads/tournamemnt_img/ksltatournament6671.jpg', 'O', '', '', '', '', '', '', '', '', '2016-11-17', '2016-11-16 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', NULL, '0000-00-00 00:00:00', 'A', NULL),
(76, 'Tallent Series 7 Boys & Girls U-12', NULL, NULL, 'Noah Tennis Academy', 'Bangalore', '', 'Bangalore', '2016-11-21', '2016-11-25', '2016-11-18', '2016-11-18', '', '', 'uploads/tournamemnt_img/ksltatournament9820.jpg', 'O', '', '', '', '', '', '', '', '', '2016-11-17', '2016-11-16 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', NULL, '0000-00-00 00:00:00', 'A', NULL),
(77, 'Tallent Series 3 Boys & Girls U-16', NULL, NULL, 'Bellari Police Gymkhana', 'Bellari', '', 'Bellari', '2016-11-28', '2016-12-02', '2016-11-25', '2016-11-25', '', '', 'uploads/tournamemnt_img/ksltatournament9695.jpg', 'O', '', '', '', '', '', '', '', '', '2016-11-17', '2016-11-16 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', NULL, '0000-00-00 00:00:00', 'A', NULL),
(78, 'Tallent Series 3 Boys & Girls U-10', NULL, NULL, 'KSLTA', 'KSLTA, Cubbon Park, Bangalore', '', 'Bangalore', '2016-11-28', '2016-12-02', '2016-11-25', '2016-11-25', '', '', 'uploads/tournamemnt_img/ksltatournament3817.jpg', 'O', '', '', '', '', '', '', '', '', '2016-11-17', '2016-11-16 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '9', NULL, '0000-00-00 00:00:00', 'A', NULL),
(79, 'Talent Series 3 Boys & Girls U-16', NULL, NULL, 'SAT Sports Pvt Ltd', 'Bangalore', '', 'Bangalore', '2016-11-28', '2016-12-02', '2016-11-25', '2016-11-25', '', '', 'uploads/tournamemnt_img/ksltatournament3466.jpg', 'O', '', '', '', '', '', '', '', '', '2016-11-17', '2017-11-16 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', NULL, '0000-00-00 00:00:00', 'A', NULL),
(80, 'Championship Series 7 Boys & Girls U-12', NULL, NULL, 'Tennis Temple Tennis Academy', '#174,DASASHRAMA, NEAR PANDURANGA TEMPLE, 5TH BLOCK RAJAJINAGAR, , BENGALURU: 560010', '', 'Bangalore ', '2016-12-03', '2016-12-09', '2016-11-14', '2016-12-28', '', '', 'uploads/tournamemnt_img/ksltatournament9688.jpg', 'O', 'Tennis Temple Tennis Academy', '#174,DASASHRAMA, NEAR PANDURANGA TEMPLE', '5TH BLOCK RAJAJINAGAR, , BENGALURU: 560010', 'Karnataka state Lawn Tennis Association', 'Jeet Rajput', '', '', '', '2016-12-24', '2016-12-23 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/05th,Dec.2016 KTPPA74267.doc', '2016-12-24 05:09:41', 'A', NULL),
(81, 'Talent Series 3 Boys & Girls U-14', NULL, NULL, 'Mahila Seva Samaja', 'MAHILA SEVA SAMAJA, # 80 K R Road Shankarpuram BANGLORE -560004 ', '', 'Bangalore', '2016-12-10', '2016-12-12', '2016-12-11', '2016-12-11', '', '', NULL, 'O', 'Mahila Seva Samaja', 'MAHILA SEVA SAMAJA, # 80 K R Road Shankarpuram BANGLORE -560004 .', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2016-12-25', '2016-12-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/12th,Dec.2016 MSS13633.doc', '2016-12-25 02:08:59', 'A', NULL),
(82, 'Talent Series 3 Boys & Girls U-16', NULL, NULL, 'Mahila Seva Samaja', 'MAHILA SEVA SAMAJA, # 80 K R Road Shankarpuram BANGLORE -560004 .', '', 'Bangalore', '2016-12-17', '2016-12-19', '2016-12-16', '2016-12-16', '', '', NULL, 'O', 'Mahila Seva Samaja', 'MAHILA SEVA SAMAJA, # 80 K R Road Shankarpuram BANGLORE -560004 .', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2016-12-25', '2016-12-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/19th,Dec.2016 MSS3070.doc', '2016-12-25 02:11:50', 'A', NULL),
(83, 'Talent Series 3 Boys & Girls U-10', NULL, NULL, 'KSLTA', 'Cubbon Park\r\nBangalore-560001', '', 'Bangalore', '2016-12-17', '2016-12-19', '2016-12-16', '2016-12-16', '', '', NULL, 'O', 'K S L T A', 'Cubbon Park Bangalore-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2016-12-25', '2016-12-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/19th,Dec.2016 KSLTA67656.doc', '2016-12-25 02:15:27', 'A', NULL),
(84, 'Talent Series 3 Boys & Girls U-10', NULL, NULL, 'Tennis Temple Tennis Academy', '#174 ,DASASHRAMA INTERNATIONAL CENTRE, 5TH BLOCK, RAJAJINAGAR, BANGALORE 560010 ', '', 'Bangalore', '2016-12-24', '2016-12-25', '2016-12-23', '2016-12-23', '', '', NULL, 'O', 'Tennis Temple Tennis Academy', '#174 ,DASASHRAMA INTERNATIONAL CENTRE, 5TH BLOCK, RAJAJINAGAR, BANGALORE 560010 ', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2016-12-25', '2016-12-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', NULL, '0000-00-00 00:00:00', 'A', NULL),
(85, 'Talent Series 3 Boys & Girls U-14', NULL, NULL, 'Fortune Sports Academy', 'Premises of KrishnaPriya Convention Hall, Bangalore-Mysore Road, Kengeri, Bangalore-560 060. (Landmark:next to Kengeri Bus Stand/HP Petrol Bunk)', '', 'Bangalore', '2016-12-31', '2017-01-02', '2016-12-30', '2016-12-30', '', '', NULL, 'O', 'Fortune Sports Academy', 'KSLTA, Cubbon Park, Bangalore', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-01-03', '2017-01-02 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/2nd,Jan.2017 Fortune Sports Academy66235.doc', '2017-01-03 01:56:39', 'A', NULL),
(86, 'Championship Series 7 Boys & Girls U-16', NULL, NULL, 'BHARATH TENNIS EXPRESS', 'Mysuru Tennis Club [MTC]\r\nKrishna raj Boulevard Chamrajapura , MYSURU – 570 005\r\n', '', 'Mysuru', '2017-01-07', '2017-01-13', '2016-12-19', '2017-01-02', '', '', NULL, 'O', 'BHARATH TENNIS EXPRESS', 'KSLTA ,CUBBON PARK,BANGALORE 560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-01-03', '2017-01-02 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/9th,Jan.2017 Bharath Tennis Express85888.doc', '2017-01-03 02:00:43', 'A', NULL),
(87, 'Championship Series 3 Boys & Girls U-12', NULL, NULL, 'NOAH TENNIS ACADEMY', 'No .28 Jayanti Nagar ,Horamavu Mn Rd , Nxt to Vibgyor International School Bangalore - 560043', '', 'Bangalore', '2017-01-07', '2017-01-09', '2017-01-06', '2017-01-06', '', '', NULL, 'O', 'NOAH TENNIS ACADEMY ', 'CUBBON PARK,  BANGALORE-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-01-03', '2017-01-02 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/9th,Jan.2017  Noah Tennis Academy25167.doc', '2017-01-03 02:03:26', 'A', NULL),
(88, 'Men\'s 50K', NULL, NULL, 'Tennis Temple Tennis Academy', '– #174,DASASHRAMA,6TH BLOCK RAJAJINAGAR, OPP ST.ANN’S SCHOOL, BENGALURU:560010', '', 'Bangalore', '2017-01-07', '2017-01-13', '2016-12-19', '2017-01-02', '', '50000', NULL, 'O', 'Tennis Temple Tennis Academy', '#174,DASASHRAMA,6TH BLOCK RAJAJINAGAR, OPP ST.ANN’S SCHOOL, BENGALURU:560010', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-01-03', '2017-01-02 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/9th,Jan.2017 OSI Sports & Tennis Temple89361.doc', '2017-01-03 02:11:00', 'A', NULL),
(89, 'WOMEN’S 50K', NULL, NULL, 'Tennis Temple Tennis Academy', '#174,DASASHRAMA,6TH BLOCK RAJAJINAGAR, OPP ST.ANN’S SCHOOL, BENGALURU:560010', '', 'Bangalore', '2017-01-07', '2017-01-13', '2016-12-19', '2017-01-02', '', '50000', NULL, 'O', 'Tennis Temple Tennis Academy', '', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-01-03', '2017-01-02 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', 'uploads/factsheets/9th,Jan.2017 OSI Sports & Tennis Temple.49761.doc', '2017-01-03 02:14:53', 'A', NULL),
(90, 'Championship Series 7 Boys & Girls U-14', NULL, NULL, 'ANT ACADEMY', 'ANT Academy\r\nSurvey #91/3/A, Jakkur-Amruthahalli Main\r\nRoad, Opp. BGS Layout, Jakkur, Bangalore-\r\n560064.', '', 'Bangalore', '2017-01-14', '2017-01-20', '2016-12-28', '2017-01-09', '', '', NULL, 'O', 'ANT ACADEMY', 'Karnataka State Lawn Tennis Association.\r\nCubbon Park,\r\nBangalore 560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-01-03', '2017-01-02 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/16th,Jan.2017 ANT Academy95726.pdf', '2017-01-03 02:20:45', 'A', NULL),
(91, 'Talent Series 3 Boys & Girls U-10', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bangalore', '2017-01-14', '2017-01-16', '2017-01-13', '2017-01-13', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE – 560 001\r\n', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-01-03', '2017-01-02 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/16th,Jan.2017 SAT Sports Academy82434.doc', '2017-01-03 02:23:22', 'A', NULL),
(92, 'National Series Boys & Girls U-14 RTW', NULL, NULL, 'KSLTA', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION          [ KSLTA], CUBBON PARK,  BANGALORE – 560 001', '', 'Bangalore', '2017-01-21', '2017-01-27', '2017-01-02', '2017-01-16', '', '', 'uploads/tournamemnt_img/ksltatournament4805.jpg', 'O', 'K S L T A', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION          [ KSLTA], CUBBON PARK,  BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-01-03', '2017-01-27 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/23rd,Jan.2017 KSLTA80388.doc', '2017-01-03 02:26:43', 'A', NULL),
(93, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'Transform Tennis Academy', '16th Cross, 13th Main Road, \r\nVirat Nagar, Bommanahalli,\r\n Bangalore-560068', '', 'Bangalore', '2017-01-28', '2017-02-03', '2017-01-09', '2017-01-23', '', '', NULL, 'O', 'Transform Tennis Academy', 'CUBBON PARK,  BANGALORE-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-01-03', '2017-01-02 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/30th,Jan.2017 Transorn Tennis16508.doc', '2017-01-03 02:29:47', 'A', NULL),
(94, 'Talent Series 3 Boys & Girls U-10', NULL, NULL, 'BEL Officers Club', 'BEL Officers Club\r\n1, Vidyaranyapura Rd, Bengaluru,\r\n Karnataka 560013', '', 'Bangalore', '2017-01-28', '2017-01-30', '2017-01-27', '2017-01-27', '', '', NULL, 'O', 'BEL officers Club', '', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-01-26', '2017-01-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/30th,Jan.2017 BEL officers Club15989.doc', '2017-01-26 01:09:50', 'A', NULL),
(95, 'Championship Series 3 Boys & Girls U-12', NULL, NULL, 'Rohan Bopanna Tennis Academy', 'ROHAN BOPANNA TENNIS ACADEMY, \r\nYELAHANKA,          \r\n  B’LORE – 560 064', '', 'Bangalore', '2017-02-04', '2017-02-06', '2017-01-03', '2017-02-03', '', '', NULL, 'O', 'Rohan Bopanna Tennis Academy', 'Cubbon Park Bangalore-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-01-26', '2017-01-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/6th,Feb.2017 Rohan Bopanna Tennis Academy34570.doc', '2017-01-26 01:13:34', 'A', NULL),
(96, 'Talent Series 3 Boys & Girls U-16', NULL, NULL, 'Fortune Sports Academy', 'Premises of KrishnaPriya Convention Hall, \r\nBangalore-Mysore Road, Kengeri, \r\nBangalore-560 060. (Landmark:next to Kengeri Bus Stand/HP Petrol Bunk)', '', 'Bangalore', '2017-02-04', '2017-02-06', '2017-01-04', '2017-02-04', '', '', NULL, 'O', 'Fortune Sports Academy', '', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-01-26', '2017-01-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/6th,Feb.2017 Fortune Sports Academy11466.doc', '2017-01-26 01:16:16', 'A', NULL),
(97, 'Men\'s 50K', NULL, NULL, 'KSLTA', 'KARNATAKA STATE LAWN TENNIS ASSOCIACTION\r\nCUBBON PARK BANGALORE - 560001', '', 'Bangalore', '2017-02-06', '2017-02-10', '2017-01-16', '2017-01-30', '', '50000', NULL, 'O', 'K S L T A', ' CUBBON PARK \r\nBANGALORE - 560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-01-26', '2017-01-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/6th,Feb.2017 KSLTA29982.doc', '2017-01-26 01:20:00', 'A', NULL),
(98, 'Championship Series 3 Boys & Girls U-16', NULL, NULL, 'K T P P A', 'Sol Sports Academy, near Wipro, opposite Radha Reddy Layout, beside Mana Pristine Apartments, off Sarjapur Road, Bangalore- 560035', '', 'Bangalore', '2017-02-13', '2017-02-17', '2017-02-13', '2017-01-13', '', '', NULL, 'O', 'K T P P A', '', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-01-26', '2017-01-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/13th,Feb.2017 KTPPA69850.doc', '2017-01-26 01:22:26', 'A', NULL),
(99, 'Men\'s 50K', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bangalore', '2017-02-13', '2017-02-17', '2017-01-23', '2017-02-06', '', '50000', NULL, 'O', 'SAT SPORTS PVT LTD', '', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-01-26', '2017-01-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/13th,Feb.2017 SAT Sports77524.doc', '2017-01-26 01:25:11', 'A', NULL),
(100, 'WOMEN’S 50K', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bangalore', '2017-02-13', '2017-02-17', '2017-01-23', '2017-02-06', '', '50000', NULL, 'O', 'SAT SPORTS PVT LTD', '', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-01-26', '2017-01-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', 'uploads/factsheets/13th,Feb.2017 SAT Sports.73531.doc', '2017-01-26 01:27:53', 'A', NULL),
(101, 'Championship Series 7 Boys & Girls U-16', NULL, NULL, 'Sol Sports Academy', 'Sol Sports Academy, near Wipro, \r\nopposite Radha Reddy Layout, \r\nbeside Mana Pristine Apartments, \r\noff Sarjapur Road, Bangalore- 560035\r\n', '', 'Bangalore', '2017-02-20', '2017-02-24', '2017-01-30', '2017-02-13', '', '', NULL, 'O', 'Sol Sports Academy', '', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-01-26', '2017-01-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/20th,Feb.2017 Sol Sports Academy93002.doc', '2017-01-26 01:31:25', 'A', NULL),
(102, 'Talent Series 3 Boys & Girls U-10', NULL, NULL, 'Transform Tennis Academy', '16th Cross, 13th Main Road, \r\nVirat Nagar, Bommanahalli, \r\nBangalore-560068\r\n(Follow Google maps for location Transform Tennis academy) \r\n', '', 'Bangalore', '2017-02-25', '2017-02-27', '2017-01-25', '2017-01-25', '', '', NULL, 'O', 'Transform Tennis Academy', '', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-01-26', '2017-01-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/27th,Feb.2017 Transform Tennis76565.doc', '2017-01-26 01:34:27', 'A', NULL),
(103, 'Championship Series 7 Boys & Girls U-18', NULL, NULL, 'Sol Sports Academy', 'Sol Sports Academy, near Wipro, opposite Radha Reddy Layout, beside Mana Pristine Apartments, off Sarjapur Road, Bangalore- \r\n560035\r\n', '', 'Bangalore', '2017-02-27', '2017-03-03', '2017-02-06', '2017-01-20', '', '', NULL, 'O', 'Sol Sports Academy', '', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-01-26', '2017-01-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '12', 'uploads/factsheets/27th,Feb.2017 Sol Sports Academy93727.doc', '2017-01-26 01:37:21', 'A', NULL),
(104, 'SAT Sports Open', NULL, NULL, 'SAT SPORTS Pvt Ltd', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bengalore', '2016-12-05', '2016-12-09', '2016-11-14', '2016-11-28', '', '50000', NULL, 'O', 'SAT SPORTS Pvt Ltd', '', '', 'KSLTA', '', '', '', '', '2017-02-25', '2017-02-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', NULL, '0000-00-00 00:00:00', 'A', NULL),
(105, 'ITF Open Men\'s', NULL, NULL, 'KSLTA Tennis Stadium', 'Cubbon Park,Bangalore-560001', '', 'Bangalore', '2017-03-13', '2017-02-18', '2017-02-12', '2017-03-12', '', '$15000', 'uploads/tournamemnt_img/ksltatournament9111.jpg', 'O', 'KSLTA', '', '', 'KSLTA', '', '', '', '', '2017-02-25', '2017-07-26 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '15', 'uploads/factsheets/ITF Tennis - 13 March - 19 March 201751617.htm', '2017-02-25 04:00:27', 'A', NULL),
(106, 'Talent Series 3 Boys & Girls U-10', NULL, NULL, 'Sol Sports Academy', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bangalore', '2017-03-27', '2017-03-27', '2017-03-26', '2017-03-26', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-04-14', '2017-04-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/27th,Mar.2017 SAT Sports44980.doc', '2017-04-14 05:30:05', 'A', NULL),
(107, 'Talent Series 3 Boys & Girls U-14', NULL, NULL, 'Focus Tennis Academy', 'Focus Tennis Academy, Behind Gangotri School,Byraveshwara nagar,Rajivgandhingar mainRoad,4th.Cross,1st.Main, Sunkadakatte,Blore-560091', '', 'Bangalore', '2017-03-27', '2017-03-29', '2017-03-26', '2017-03-26', '', '', NULL, 'O', 'Focus Tennis Academy', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-04-14', '2017-04-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/27th,Mar.2017 Focus Tennis Academy69266.doc', '2017-04-14 05:32:45', 'A', NULL),
(108, 'Championship Series 3 Boys & Girls U-16', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bangalore', '2017-04-03', '2017-04-05', '2017-04-02', '2017-04-02', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'Cubbon Park, Bengaluru-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-04-14', '2017-04-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/3rd,Apr.2017 SAT Sports82369.doc', '2017-04-14 05:35:56', 'A', NULL),
(109, 'Men\'s 1 Lc ', NULL, NULL, 'Century Club', 'NO.1, SHESHADRI ROAD, B’LORE - 560 001{ LAND MARK: NEAR K.R. CIRCLE}', '', 'Bangalore', '2017-04-10', '2017-04-14', '2017-03-20', '2017-04-03', '', '100000', NULL, 'O', 'Century Club', '', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-04-14', '2017-04-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/10th Apr.2017 Century Club22910.doc', '2017-04-14 05:39:21', 'A', NULL),
(110, 'women\'s 1 Lc', NULL, NULL, 'Century Club', 'NO.1, SHESHADRI ROAD, B’LORE - 560 001{ LAND MARK: NEAR K.R. CIRCLE}', '', 'Bangalore', '2017-04-10', '2017-04-14', '2017-03-20', '2017-04-03', '', '100000', NULL, 'O', 'Century Club', 'Cubbon PArk, Bengaluru-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-04-14', '2017-04-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', 'uploads/factsheets/10th Apr.2017 Century Club21616.doc', '2017-04-14 05:41:59', 'A', NULL),
(111, 'Championship Series 3 Boys & Girls U-14', NULL, NULL, 'Rohan Bopanna Tennis Academy', '53, Venkatala Village, On the road to Canadian International School, Yelahanka, Bangalore 560064', '', 'Bangalore', '2017-04-10', '2017-04-12', '2017-04-09', '2017-04-09', '', '', NULL, 'O', 'Rohan Bopanna Tennis Academy', 'Cubbon Park, Bengaluru-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-04-14', '2017-04-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/10th Apr.2017 Rohan Bopanna Tennis Academy21608.doc', '2017-04-14 05:44:17', 'A', NULL),
(112, 'Talent Series 3 Boys & Girls U-12', NULL, NULL, 'MANASAROWAR PUSHKARINI VIDYASHRAMA ', 'NAGARAJ TENNIS CENTRE\r\nSJCE TENNIS COMPLEX, SRI JAYACHAMARAJENDRA COLLEGE OF ENGINEERING, MANASA GANGOTRI, MYSURU - 570008', '', 'Mysuru', '2017-04-17', '2017-04-19', '2017-04-16', '2017-04-16', '', '', NULL, 'O', 'NAGARAJ TENNIS CENTRE', 'Cubbon Park, Bengaluru-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-04-14', '2017-04-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/17th,Apr.2017 Manasarowar Pushkarini Vidyashrama72251.doc', '2017-04-14 05:47:09', 'A', NULL),
(113, 'Championship Series 7 Boys & Girls U-14', NULL, NULL, 'Topspin Tennis Academy', 'Palm Groove Estate, Near Sarakki Lake,\r\nBehind Puttenahalli Govt School,\r\nJ P Nagar,7th Phase,Bengaluru-', '', 'Bangalore', '2017-04-17', '2017-04-21', '2017-03-27', '2017-04-10', '', '', NULL, 'O', 'Topspin Tennis Academy', 'Cubbon PArk, Bengaluru-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-04-14', '2017-04-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/17th,Apr.2017 Topspin Tennis Academy59420.xls', '2017-04-14 05:51:19', 'A', NULL),
(114, 'Men\'s 50K', NULL, NULL, 'Elite Tennis Academy', '#51/6, Halanayyakanahalli\r\nOpp to Rainbow retreat Layout,\r\nNear to Nimritha Green County\r\nBangalore – 560035, Karnataka – INDIA\r\n', '', 'Bangalore', '2017-04-24', '2017-04-28', '2017-04-03', '2017-04-17', '', '50000', NULL, 'O', 'Elite Tennis Academy', 'Cubbon Park,Bengaluru-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-04-14', '2017-04-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/24th,Apr.2017 Elite Tennis Academy5872.doc', '2017-04-14 05:54:03', 'A', NULL),
(115, 'WOMEN’S 50K', NULL, NULL, 'Elite Tennis Academy', '#51/6, Halanayyakanahalli\r\nOpp to Rainbow retreat Layout,\r\nNear to Nimritha Green County\r\nBangalore – 560035, Karnataka – INDIA\r\n', '', 'Bangalore', '2017-04-24', '2017-04-28', '2017-04-03', '2017-04-17', '', '50000', NULL, 'O', 'Elite Tennis Academy', 'Cubbon Park, Bengaluru-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-04-14', '2017-04-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', 'uploads/factsheets/24th,Apr.2017 Elite Tennis Academy72376.doc', '2017-04-14 05:55:44', 'A', NULL),
(116, 'Talent Series 3 Boys & Girls U-12', NULL, NULL, 'SAT SPORTS PVT LTD', 'SAT Sports @ DHI Sports Centre\r\nInnovation Park, Arekere Gate,\r\nBannerghatta Road,\r\nVenugopal Reddy Layout. Arekere, Bangalore-76\r\n', '', 'Bangalore', '2017-04-24', '2017-04-27', '2017-04-23', '2017-04-23', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'Cubbon Park, Bengaluru-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-04-14', '2017-04-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/24th Apr.2017 SAT Sports34565.doc', '2017-04-14 05:58:44', 'A', NULL),
(118, 'Davis Cup India Vs Uzbekistan', NULL, NULL, 'KSLTA', 'Cubbon Park, Bangalore-560001', '', 'Bangalore', '2017-04-07', '2017-04-09', '2017-04-06', '2017-04-05', '', '', NULL, 'O', 'K S L T A', 'Cubbon Park, Bangalore-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-04-18', '2017-07-26 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '17', 'uploads/factsheets/Poster73420.jpg', '2017-07-27 03:43:25', 'A', NULL),
(119, 'Talent Series 3 Boys & Girls U-14', NULL, NULL, 'NAGARAJ TENNIS CENTRE', 'SJCE TENNIS COMPLEX, SRI JAYACHAMARAJENDRA COLLEGE OF ENGINEERING, MANASA GANGOTRI, MYSURU - 570008', '', 'Mysuru', '2017-05-01', '2017-05-03', '2017-04-28', '2017-04-28', '', '', NULL, 'O', 'INNER WHEEL CLUB OF MYSORE ', 'Cubbon Park, Bangaluru-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-04-30', '2017-04-29 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/1st,May.2017 Inner wheel Club Mysore78166.doc', '2017-04-30 03:35:20', 'A', NULL),
(120, 'Talent Series 3 Boys & Girls U-12', NULL, NULL, 'Mysore Tennis Club', 'Chamarajapura, Mysuru, Karnataka 570005', '', 'Mysuru', '2017-05-08', '2017-05-10', '2017-05-05', '2017-05-05', '', '', NULL, 'O', 'Mysore Tennis Club', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-04-30', '2017-04-29 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/8th,May.2017 Mysore Tennis Club27503.doc', '2017-04-30 03:37:13', 'A', NULL),
(121, 'Championship Series 3 Boys & Girls U-14', NULL, NULL, 'KSLTA', 'CUBBON PARK, BANGALORE – 560 001', '', 'Bangalore', '2017-05-15', '2017-05-17', '2017-05-12', '2017-05-12', '', '', NULL, 'O', 'CRS TRUST', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-04-30', '2017-04-29 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/15th,May.2017 CRS Trust294.doc', '2017-04-30 03:40:12', 'A', NULL),
(122, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'Namma Sportz Tennis Academy', 'SMVN School campus, Judicial Layout, Off Kanakapura Road, Thalaghattapura, Bangalore 560 062', '', 'Bangalore', '2017-05-22', '2017-05-23', '2017-05-19', '2017-05-19', '', '', NULL, 'O', 'Namma Sportz Tennis Academy', 'CUBBON PARK ,BANGALORE-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-04-30', '2017-05-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/22nd,May.2017 Namma Sports Tennis Academy7706.doc', '2017-04-30 03:42:37', 'A', NULL),
(123, 'Men\'s 1 Lc', NULL, NULL, 'MYSORE TENNIS CLUB  ', 'KRISHNARAJ BOLUEVARD, 2ND CROSS, CHAMARAJAPURAM, MYSORE-570 005', '', 'Mysuru', '2017-05-22', '2017-05-26', '2017-05-01', '2017-05-15', '', '100000', NULL, 'O', 'Mysore Tennis Club', 'CUBBON PARK, BANGALORE - 560001\r\n', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-04-30', '2017-04-29 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/22nd,May.2017 Mysore Tennis Club36052.doc', '2017-04-30 03:46:08', 'A', NULL),
(124, 'Talent Series 3 Boys & Girls U-12', NULL, NULL, 'True Bounce Tennis Academy', '-', '', 'Bangalore', '2017-05-29', '2017-05-31', '2017-05-26', '2017-05-26', '', '', NULL, 'O', 'True Bounce Tennis Academy', 'Cubbon Park, Bengaluru-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-04-30', '2017-04-30 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/TS(3) U 12 29-05-17 B\'LORE9966.doc', '2017-05-01 09:55:31', 'A', NULL),
(125, 'Championship Series3 Boys & Girls U-16', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bangalore', '2017-06-05', '2017-06-08', '2017-06-02', '2017-06-02', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-05-22', '2017-05-21 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/5th,Jun.2017 SAT Sports53771.doc', '2017-05-22 03:22:43', 'A', NULL),
(126, 'Championship Series3 Boys & Girls U-12', NULL, NULL, 'KSLTA', 'CUBBON PARK, BANGALORE – 560 001', '', 'Bangalore', '2017-06-05', '2017-06-02', '2017-06-02', '2017-06-02', '', '', NULL, 'O', 'CRS TRUST', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-05-22', '2017-05-21 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/5th,Jun.2017 CRS Trust13293.doc', '2017-05-22 03:27:11', 'A', NULL),
(127, 'Talent Series 3 Boys & Girls U-16', NULL, NULL, 'PROTEAM Tennis Academy', '25/3,21ST CROSS, KANAKANAGAR MAIN ROAD ,VISWANTH NAGENHALLI. R.T.NAGAR POST  BANGALORE -560032', '', 'Bangalore', '2017-06-12', '2017-06-14', '2017-06-09', '2017-06-09', '', '', NULL, 'O', 'PROTEAM Tennis Academy', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-05-22', '2017-05-21 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/12th,Jun.2017 Proteam Tennis Academy10578.doc', '2017-05-22 03:39:37', 'A', NULL),
(128, 'Talent Series 3 Boys & Girls U-10', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bangalore', '2017-06-19', '2017-06-21', '2017-06-16', '2017-06-16', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-05-22', '2017-05-21 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/19th.Jun.2017 SAT Sports45205.doc', '2017-05-22 04:03:25', 'A', NULL),
(129, 'Championship Series 3 Boys & Girls U-14', NULL, NULL, 'PROTEAM Tennis Academy', '25/3,21ST CROSS, KANAKANAGAR MAIN ROAD, VISWANTH NAGENHALLI. R.T.NAGAR POST  BANGALORE -560032', '', 'Bangalore', '2017-06-19', '2017-06-21', '2017-06-16', '2017-06-16', '', '', NULL, 'O', 'PROTEAM Tennis Academy', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-05-22', '2017-05-21 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/19th,Jun.2017 Proteam Tennis Academy87401.doc', '2017-05-22 04:08:53', 'A', NULL),
(130, 'Championship Series 3 Boys & Girls U-18', NULL, NULL, 'Sports Cult', '# 505, ITI Layout, Mangammapalya, near old ujala factory, vasapalya main road,\r\nBengaluru-560 068.', '', 'Bangalore', '2017-06-19', '2017-06-21', '2017-06-16', '2017-06-16', '', '', NULL, 'O', 'Sports Cult', 'CUBBON PARK, BENGALURU – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-05-22', '2017-05-21 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '12', 'uploads/factsheets/19th,Jun.2017 Sports Cult98093.doc', '2017-05-22 04:27:58', 'A', NULL);
INSERT INTO `ksl_tournaments_tbl` (`tourn_id`, `tourn_title`, `tourn_category`, `tourn_sub_category`, `tourn_venue`, `tourn_venue_address1`, `tourn_venue_address2`, `tourn_venue_city`, `tourn_from_date`, `tourn_to_date`, `tourn_last_submission_date`, `tourn_withdrl_date`, `tourn_description`, `tourn_price_money`, `tourn_tournament_image`, `tourn_register_status`, `tourn_asso_title`, `tourn_asso_address1`, `tourn_asso_address2`, `tourn_asso_head`, `tourn_refree1`, `tourn_refree2`, `tourn_refree3`, `tourn_link`, `tourn_created_date`, `tourn_lastmodified_date`, `tourn_status`, `sponser_name_of_organisation`, `sponser_address1`, `sponser_address2`, `sponser_contact_person_name`, `sponser_phone`, `sponser_email`, `tour_event_category`, `tour_event_subcategory`, `tourn_factsheet`, `tourn_factsheet_uploaded_at`, `tourn_facesheet_status`, `tourn_gallery_id`) VALUES
(131, 'Talent Series 3 Boys & Girls U-14', NULL, NULL, 'Sports Cult', '6th cross, Dhanalakshmi Layout, Sampige road, Kodigehalli ,  near Thindlu mainroad , Bengaluru-560 097.', '', 'Bangalore', '2017-06-26', '2017-06-28', '2017-06-23', '2017-06-23', '', '', NULL, 'O', 'Sports Cult', 'CUBBON PARK, BENGALURU – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-05-22', '2017-05-21 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/26th,Jun.2017 Sports Cult80987.doc', '2017-05-22 04:51:05', 'A', NULL),
(132, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'SPORTSCULT Tennis Academy', 'Beside pragathi School , opp state bank of mysore, kudlu main road, bommanahalli, bangalore.', '', 'Bangalore', '2017-07-17', '2017-07-21', '2017-06-26', '2017-07-10', '', '', NULL, 'O', 'Sports Cult', 'Cubbon Park, Bengaluru-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-06-20', '2017-06-19 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/17th,Jul.2017 Sports Cult24580.doc', '2017-06-20 06:47:27', 'A', NULL),
(133, 'Championship Series 3 Boys & Girls U-18', NULL, NULL, 'SPORTSCULT Tennis Academy', '# 505, ITI Layout, Mangammapalya, near old ujala factory, vasapalya main road,      Bengaluru-560 068.', '', 'Bangalore', '2017-07-24', '2017-07-26', '2017-07-21', '2017-07-21', '', '', NULL, 'O', 'Sports Cult', 'Cubbon Park. Bengaluru-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-06-20', '2017-06-19 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '12', 'uploads/factsheets/24th,Jul.2017 Sports Cult-Wilson44768.doc', '2017-06-20 06:52:36', 'A', NULL),
(134, 'Talent Series 3 Boys & Girls U-14', NULL, NULL, 'SPORTSCULT Tennis Academy', '6th cross, Dhanalakshmi Layout, Sampige road, Kodigehalli ,  near Thindlu mainroad , Bengaluru-560 097.', '', 'Bangalore', '2017-07-31', '2017-08-02', '2017-07-28', '2017-07-28', '', '', NULL, 'O', 'Sports Cult', 'Cubbon Park, Bengaluru-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-06-20', '2017-06-19 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/31st,Jul.2017 Sportscult & Sports Line64353.doc', '2017-06-20 07:28:02', 'A', NULL),
(135, 'Talent Series 3 Boys & Girls U-10', NULL, NULL, 'ANT ACADEMY', 'No 91/3A,Jakkur,Amruthalli\r\nMain Road, Bangalore-560064\r\n', '', 'Bangalore', '2017-08-07', '2017-08-09', '2017-08-04', '2017-08-04', '', '', 'uploads/tournamemnt_img/ksltatournament2462.doc', 'O', 'ANT ACADEMY', 'Cubbon Park, Bengaluru-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-07-17', '2017-07-17 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/7th Aug.2017 Ant Academy24097.doc', '2017-07-18 00:02:51', 'A', NULL),
(136, 'Men\'s 50K', NULL, NULL, 'SPORTSCULT Tennis Academy ', '# 505, Mangammanapalya road, near old ujalla factory, bommanahalli main road, HSR sector -2, Bangalore. Karnataka ', '', 'Bangalore', '2017-08-07', '2017-08-11', '2017-07-17', '2017-07-28', '', '50000', NULL, 'O', 'SPORTSCULT Tennis Academy ', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-07-18', '2017-07-17 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/7th,Aug.2017 Sports Cult96464.doc', '2017-07-18 00:12:26', 'A', NULL),
(137, 'WOMEN’S 50K', NULL, NULL, 'SPORTSCULT Tennis Academy', '# 505, Mangammanapalya road, near old ujalla factory, bommanahalli main road, HSR sector -2, Bangalore. Karnataka ', '', 'Bangalore', '2017-08-07', '2017-08-11', '2017-07-17', '2017-07-28', '', '50000', NULL, 'O', 'SPORTSCULT Tennis Academy', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-07-18', '2017-07-17 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', 'uploads/factsheets/7th,Aug.2017 Sports Cult92159.doc', '2017-07-18 00:16:13', 'A', NULL),
(138, 'Championship Series3 Boys & Girls U-12', NULL, NULL, 'NAGARAJ TENNIS CENTRE, MYSURU', 'SJCE TENNIS COMPLEX, SRI JAYACHAMARAJENDRA COLLEGE OF ENGINEERING, MANASA GANGOTRI, MYSURU – 570008', '', 'Mysuru', '2017-08-14', '2017-08-16', '2017-08-11', '2017-08-10', '', '', NULL, 'O', 'NAGARAJ TENNIS CENTRE, MYSURU	', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-07-18', '2017-07-17 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/14th,Aug.2017 Nagaraj Tennis Centre54196.doc', '2017-07-18 00:37:25', 'A', NULL),
(139, 'Talent Series 3 Boys & Girls U-12', NULL, NULL, 'POLICE GYMKHANA', 'PARVATI NAGAR, 2ND CROSS, BALLARI – 583 101, KARNATAKA', '', 'Bellari', '2017-08-21', '2017-08-23', '2017-08-17', '2017-08-18', '', '', NULL, 'O', 'POLICE GYMKHANA', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-07-18', '2017-07-17 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/21st,Aug.2017 Police Gymkhana65364.doc', '2017-07-18 00:41:39', 'A', NULL),
(140, 'Men\'s 1 Lc', NULL, NULL, 'NATIONAL SPORTS & GAMES DEVELOPMENT SOCIETY ', 'Noah Tennis Academy, N0 .28 Jayanti Nagar ,Horamavu Mn Rd , Next to Vibgyor International School Bangalore', '', 'Bangalore', '2017-08-21', '2017-08-25', '2017-07-31', '2017-08-11', '', '100000', NULL, 'O', 'NATIONAL SPORTS & GAMES DEVELOPMENT SOCIETY ', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-07-18', '2017-07-17 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/21st,Aug.2017 National Sports & Games Dev Society97725.doc', '2017-07-18 00:51:59', 'A', NULL),
(141, 'women\'s 1 Lc', NULL, NULL, 'NATIONAL SPORTS & GAMES DEVELOPMENT SOCIETY', 'Noah Tennis Academy, N0 .28 Jayanti Nagar ,Horamavu Mn Rd , Next to Vibgyor International School Bangalore', '', 'Bangalore', '2017-08-21', '2017-08-25', '2017-07-31', '2017-08-11', '', '100000', NULL, 'O', 'NATIONAL SPORTS & GAMES DEVELOPMENT SOCIETY', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-07-18', '2017-07-17 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', 'uploads/factsheets/21st,Aug.2017 National Sports & Games Dev Society40473.doc', '2017-07-18 00:56:02', 'A', NULL),
(142, 'Men\'s 50K', NULL, NULL, 'Noah Tennis Academy', 'Noah Tennis Academy, N0 .28 Jayanti Nagar ,Horamavu Mn Rd , Next to Vibgyor International School Bangalore', '', 'Bangalore', '2017-08-28', '2017-09-01', '2017-08-07', '2017-08-21', '', '50000', NULL, 'O', 'NOAH Tennis Academy ', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-07-18', '2017-07-17 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/28th,Aug.2017 Noah Tennis Academy72311.doc', '2017-07-18 01:17:07', 'A', NULL),
(143, 'WOMEN’S 50K', NULL, NULL, 'Noah Tennis Academy', 'Noah Tennis Academy, N0 .28 Jayanti Nagar ,Horamavu Mn Rd , Next to Vibgyor International School Bangalore', '', 'Bangalore', '2017-08-28', '2017-09-01', '2017-08-07', '2017-08-11', '', '50000', NULL, 'O', 'NOAH Tennis Academy', 'Cubbon Park, Bengaluru-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-07-18', '2017-07-17 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', 'uploads/factsheets/28th,Aug.2017 Noah Tennis Academy62366.doc', '2017-07-18 01:09:04', 'A', NULL),
(144, 'Talent Series 3 Boys & Girls U-10', NULL, NULL, 'Noah Tennis Academy', 'Dsc Sports Club, Near Alphine Eco Apartment, Doddanekundi Bangalore – 560037.\r\nTEL.PH: 9845812465. \r\n', '', 'Bangalore', '2017-08-28', '2017-08-30', '2017-08-26', '2017-08-25', '', '', NULL, 'O', 'NOAH Tennis Academy', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-08-07', '2017-08-06 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/28th,Aug.2017 Noah Tennis Academy42944.doc', '2017-08-07 00:13:53', 'A', NULL),
(145, 'Men\'s 50K', NULL, NULL, 'TOP SPIN TENNIS ACADEMY.', 'PALM GROOVE ESTATE NEAR SARAKKI LAKE, BEHIND PUTTENAHALLI GOVT SCHOOL, PUTTENAHALLI, JP NAGAR, 7TH PHASE, BANGALORE.', '', 'Bangalore', '2017-09-04', '2017-09-08', '2017-08-14', '2017-08-27', '', '50000', NULL, 'O', 'Topspin Tennis Academy', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-08-07', '2017-08-06 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/4th,Sep.2017 Top Spin Tennis Academy6500.doc', '2017-08-07 00:19:47', 'A', NULL),
(146, 'WOMEN’S 50K', NULL, NULL, 'TOP SPIN TENNIS ACADEMY.', 'PALM GROOVE ESTATE NEAR SARAKKI LAKE, BEHIND PUTTENAHALLI GOVT SCHOOL, PUTTENAHALLI, JP NAGAR, 7TH PHASE, BANGALORE', '', 'Bangalore', '2017-09-04', '2017-09-08', '2017-08-14', '2017-08-28', '', '50000', NULL, 'O', 'Topspin Tennis Academy', '', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-08-07', '2017-08-06 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', 'uploads/factsheets/4th,Sep.2017 Top Spin Tennis Academy35867.doc', '2017-08-07 00:21:31', 'A', NULL),
(147, 'Championship Series 7 Boys & Girls U-12', NULL, NULL, 'KSLTA', 'KSLTA ,CUBBON PARK , BENGALURU', '', 'Bangalore', '2017-09-04', '2017-09-08', '2017-08-14', '2017-08-28', '', '', NULL, 'O', 'K S L T A', 'KSLTA ,CUBBON PARK , BENGALURU', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-08-07', '2017-09-07 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/4th,Sep.2017 KSLTA90179.doc', '2017-08-07 00:25:33', 'A', NULL),
(148, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'Elite Tennis Academy', '#51/6, Halanayyakanahalli\r\nOpp to Rainbow retreat Layout,\r\nNear to Nimritha Green County\r\nBangalore – 560035, Karnataka – INDIA\r\n', '', 'Bangalore', '2017-09-11', '2017-09-15', '2017-08-21', '2016-09-04', '', '', NULL, 'O', 'Elite Tennis Academy', '', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-08-07', '2017-08-06 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/11th,Sep.2017 Elite Tennis Academy30903.doc', '2017-08-07 00:31:34', 'A', NULL),
(149, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'Elite Tennis Academy', '#51/6, Halanayyakanahalli\r\nOpp to Rainbow retreat Layout,\r\nNear to Nimritha Green County\r\nBangalore – 560035, Karnataka – INDIA\r\n', '', 'Bangalore', '2017-09-11', '2017-09-15', '2017-08-21', '2017-09-04', '', '', NULL, 'O', 'Elite Tennis Academy', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-08-07', '2017-08-06 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/11th,Sep.2017 Elite Tennis Academy53627.doc', '2017-08-07 00:33:45', 'A', NULL),
(150, 'Talent Series 3 Boys & Girls U-10', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bangalore', '2017-09-18', '2017-09-20', '2017-09-16', '2017-09-15', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-08-07', '2017-08-06 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/18th,Sep.2017 SAT Sports77014.doc', '2017-08-07 00:36:54', 'A', NULL),
(151, 'Championship Series 3 Boys & Girls U-18', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bangalore', '2017-09-25', '2017-09-27', '2017-09-22', '2017-09-22', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-08-07', '2017-08-06 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '12', 'uploads/factsheets/25th,Sep.2017 SAT Sports38008.doc', '2017-08-07 00:43:06', 'A', NULL),
(152, 'Talent Series 3 Boys & Girls U-10', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bangalore', '2017-10-09', '2017-10-10', '2017-10-07', '2017-10-07', '', '', 'uploads/tournamemnt_img/ksltatournament6271.doc', 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-09-11', '2017-09-10 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/9th,Oct.2017 SAT Sports84494.doc', '2017-09-11 08:00:29', 'A', NULL),
(153, 'Championship Series 3 Boys & Girls U-18', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bangalore', '2017-10-16', '2017-10-17', '2017-10-13', '2017-10-13', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-09-11', '2017-09-10 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '12', 'uploads/factsheets/16th,Oct.2017 SAT Sports Ltd31208.doc', '2017-09-11 08:05:25', 'A', NULL),
(154, 'Championship Series3 Boys & Girls U-16', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bangalore', '2017-10-30', '2017-10-31', '2017-10-27', '2017-10-27', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-09-11', '2017-09-10 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/30th,Oct.2017 SAT Sports37631.doc', '2017-09-11 08:15:41', 'A', NULL),
(155, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'Tennis Temple Academy', '176, 6th Cross Road, SSI Industrial Area, 5th Block, Rajaji Nagar, Bengaluru, Karnataka 560010.', '', 'Bangalore', '2017-10-23', '2017-10-27', '2017-10-02', '2017-10-16', '', '', NULL, 'O', 'Tennis Temple Academy', '', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-09-19', '2017-09-18 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/23rd,Oct.2017 Tennis Temple91126.doc', '2017-09-19 03:17:04', 'A', NULL),
(156, 'Talent Series 3 Boys & Girls U-16', NULL, NULL, 'Fortune Sports Academy', 'Premises of KrishnaPriya Convention Hall, Bangalore-Mysore Road, Kengeri, Bangalore-560 060. (Landmark:next to Kengeri Bus Stand/HP Petrol Bunk)', '', 'Bangalore', '2017-10-09', '2017-10-11', '2017-10-06', '2017-10-05', '', '', NULL, 'O', 'FORTUNE SPORTS ACADEMY', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-09-26', '2017-09-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/9th,Oct.2017 Fortune Sports Academy62247.doc', '2017-09-26 03:21:23', 'A', NULL),
(157, 'Talent Series 3 Boys & Girls U-14', NULL, NULL, 'Fortune Sports Academy', 'Premises of KrishnaPriya Convention Hall, Bangalore-Mysore Road, Kengeri, Bangalore-560 060. (Landmark:next to Kengeri Bus Stand/HP Petrol Bunk)', '', 'Bangalore', '2017-10-16', '2017-10-18', '2017-10-13', '2017-10-12', '', '', NULL, 'O', 'FORTUNE SPORTS ACADEMY', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-09-26', '2017-09-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/16th,Oct.2017 Fortune Sports Academy77337.doc', '2017-09-26 03:52:38', 'A', NULL),
(158, 'Talent Series 3 Boys & Girls U-12', NULL, NULL, 'Fortune Sports Academy', 'Premises of KrishnaPriya Convention Hall, Bangalore-Mysore Road, Kengeri, Bangalore-560 060. (Landmark:next to Kengeri Bus Stand/HP Petrol Bunk)', '', 'Bangalore', '2017-10-30', '2017-10-30', '2017-10-27', '2017-10-27', '', '', NULL, 'O', 'FORTUNE SPORTS ACADEMY', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-09-26', '2017-09-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/30th,Oct.2017 Fortune Sports Academy41736.doc', '2017-09-26 03:54:26', 'A', NULL),
(159, 'Championship Series 3 Boys & Girls U-12', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bangalore', '2017-11-06', '2017-11-08', '2017-11-03', '2017-11-03', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-10-25', '2017-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/6th Nov.2017 SAT Sports2259.doc', '2017-10-25 08:03:10', 'A', NULL),
(160, 'Men\'s 50K', NULL, NULL, 'TENNIS TEMPLE ACADEMY', '176, 6TH  Cross Road, SSI Industrial Area, 5TH  Block, Rajaji Nagar, Bengaluru – 560010, Karnataka', '', 'Bangalore', '2017-11-06', '2017-11-10', '2017-10-16', '2017-10-30', '', '50000', NULL, 'O', 'Tennis Temple Academy', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-10-25', '2017-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/6th Nov.2017 Tennis Temple17215.doc', '2017-10-25 08:06:29', 'A', NULL),
(161, 'WOMEN’S 50K', NULL, NULL, 'TENNIS TEMPLE ACADEMY', '176, 6TH  Cross Road, SSI Industrial Area, 5TH  Block, Rajaji Nagar, Bengaluru – 560010, Karnataka', '', 'Bangalore', '2017-11-06', '2017-11-10', '2017-10-16', '2017-10-30', '', '50000', NULL, 'O', 'Tennis Temple Academy', '', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-10-25', '2017-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', 'uploads/factsheets/6th Nov.2017 Tennis Temple40364.doc', '2017-10-25 08:08:25', 'A', NULL),
(162, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'Elite Tennis Academy', '#51/6, Halanayyakanahalli, Opp to Rainbow retreat Layout, Near to Nimritha Green County, Bangalore – 560035, Karnataka – INDIA', '', 'Bangalore', '2017-11-13', '2017-11-17', '2017-10-23', '2017-11-06', '', '', NULL, 'O', 'Elite Tennis Academy', '', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-10-25', '2017-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/13th Nov.2017 Elite Tennis Academy19156.doc', '2017-10-25 08:12:58', 'A', NULL),
(163, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'Elite Tennis Academy ', '#51/6, Halanayyakanahalli, Opp to Rainbow retreat Layout, Near to Nimritha Green County, Bangalore – 560035, Karnataka – INDIA', '', 'Bangalore', '2017-11-13', '2017-11-17', '2017-10-23', '2017-11-06', '', '', NULL, 'O', 'Elite Tennis Academy', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-10-25', '2017-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/13th Nov.2017 Elite Tennis Academy55101.doc', '2017-10-25 08:18:05', 'A', NULL),
(164, 'Men\'s 1 Lc', NULL, NULL, 'TOPSPIN TENNIS ACADEMY', 'PALM GROOVE ESTATE NEAR SARAKKI LAKE, BEHIND PUTTENAHALLI GOVT SCHOOL, PUTTENAHALLI, JP NAGAR, 7TH PHASE, BANGALORE.', '', 'Bangalore', '2017-11-13', '2017-11-17', '2017-10-23', '2017-11-06', '', '100000', NULL, 'O', 'Topspin Tennis Academy', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-10-25', '2017-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/13th Nov.2017 Topspin Tennis Academy44802.doc', '2017-10-25 08:22:26', 'A', NULL),
(165, 'women\'s 1 Lc', NULL, NULL, 'TOPSPIN TENNIS ACADEMY', 'PALM GROOVE ESTATE NEAR SARAKKI LAKE, BEHIND PUTTENAHALLI GOVT SCHOOL, PUTTENAHALLI, JP NAGAR, 7TH PHASE, BANGALORE', '', 'Bangalore', '2017-11-13', '2017-11-17', '2017-10-23', '2017-11-06', '', '100000', NULL, 'O', 'TOPSPIN TENNIS ACADEMY.', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-10-25', '2017-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', 'uploads/factsheets/13th Nov.2017 Topspin Tennis Academy58929.doc', '2017-10-25 08:24:52', 'A', NULL),
(166, 'Championship Series 7 Boys & Girls U-12', NULL, NULL, 'Topspin Tennis Academy - KTPPA', 'Palm Grove Estate, Near Sarakki Lake, Behind Government School, Puttenhalli, J.P.Nagar, 7th Phase, BENGALURU', '', 'Bangalore', '2017-11-20', '2017-11-24', '2017-10-30', '2017-11-13', '', '', NULL, 'O', 'KTPPA', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-10-25', '2017-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/20th Nov .2017 KTPPA77477.doc', '2017-10-25 08:29:20', 'A', NULL),
(167, 'Championship Series 3 Boys & Girls U-18', NULL, NULL, 'Proteam Tennis Academy ', '25/3,21ST CROSS, KANAKANAGAR MAIN ROAD, VISWANTH NAGENHALLI. R.T.NAGAR POST  BANGALORE -560032', '', 'Bangalore', '2017-11-20', '2017-11-22', '2017-11-17', '2017-11-17', '', '', NULL, 'O', 'PROTEAM Tennis Academy', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-10-25', '2017-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '12', 'uploads/factsheets/20th Nov.2017 Proteam Tennis Academy36335.doc', '2017-10-25 08:32:29', 'A', NULL),
(168, 'Championship Series 3 Boys & Girls U-14', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bangalore', '2017-11-27', '2017-11-29', '2017-11-24', '2017-11-24', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-10-25', '2017-10-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/27th Nov.2017 SAT Sports50565.doc', '2017-10-25 08:35:16', 'A', NULL),
(169, 'Championship Series 3 Boys & Girls U-18', NULL, NULL, 'Tennis Temple Academy', '176, 6th Cross Road, SSI Industrial Area, 5th Block, Rajaji Nagar, Bengaluru, Karnataka 560010', '', 'Bangalore', '2017-11-27', '2017-11-28', '2017-11-24', '2017-11-24', '', '', NULL, 'O', 'Tennis Temple Academy', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-12-21', '2017-12-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '12', 'uploads/factsheets/27th Nov.2017 Tennis Temple50046.doc', '2017-12-21 06:54:29', 'A', NULL),
(170, 'Championship Series 7 Boys & Girls U-14', NULL, NULL, 'TOPSPIN TENNIS ACADEMY', 'PALM GROOVE ESTATE NEAR SARAKKI LAKE, BEHIND PUTTENAHALLI GOVT SCHOOL, PUTTENAHALLI, JP NAGAR, 7TH PHASE, BANGALORE.', '', 'Bangalore', '2017-12-04', '2017-12-08', '2017-11-13', '2017-12-27', '', '', NULL, 'O', 'Topspin Tennis Academy', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-12-21', '2017-12-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/4th,Sep.2017 Top Spin Tennis Academy46044.doc', '2017-12-21 06:57:53', 'A', NULL),
(171, 'Championship Series3 Boys & Girls U-16', NULL, NULL, 'Proteam Tennis Academy', '25/3,21ST CROSS, KANAKANAGAR MAIN ROAD, VISWANTH NAGENHALLI. R.T.NAGAR POST  BANGALORE -560032', '', 'Bangalore', '2017-12-04', '2017-12-06', '2017-12-01', '2017-12-01', '', '', NULL, 'O', 'PROTEAM Tennis Academy', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-12-21', '2017-12-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/4th Dec.2017 Proteam Tennis Academy18314.doc', '2017-12-21 07:01:43', 'A', NULL),
(172, 'Talent Series 3 Boys & Girls U-12', NULL, NULL, 'MAHILA SEVA SAMAJA', '# 80 K R Road Shankar puram Bangalore -560004', '', 'Bangalore', '2017-12-11', '2017-12-13', '2017-12-08', '2017-12-08', '', '', NULL, 'O', 'Mahila Seva Samaja', 'Cubbon Park Bangalore 56001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-12-21', '2017-12-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/11th Dec.2017 Mahila Seva Samaja29439.doc', '2017-12-21 07:05:17', 'A', NULL),
(173, 'Talent Series 3 Boys & Girls U-14', NULL, NULL, 'MAHILA SEVA SAMAJA', '# 80 K R Road Shankar puram Bangalore -560004', '', 'Bangalore', '2017-12-18', '2017-12-20', '2017-12-15', '2017-12-15', '', '', NULL, 'O', 'Mahila Seva Samaja', '', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-12-21', '2017-12-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/18th Dec.2017 Mahila Seva Samaja24950.doc', '2017-12-21 07:09:44', 'A', NULL),
(174, 'Talent Series 3 Boys & Girls U-10', NULL, NULL, 'BEL OFFICERS CLUB', '1, Vidyaranyapura Rd Jala halli, Bengaluru, Karnataka 560013', '', 'Bangalore', '2017-12-18', '2017-12-20', '2017-12-15', '2017-12-15', '', '', NULL, 'O', 'BEL OFFICERS CLUB  ', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-12-21', '2017-12-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/18th Dec.2017 BEL Officer Club82775.doc', '2017-12-21 07:12:59', 'A', NULL),
(175, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'Proteam Tennis Academy', '25/3,21ST CROSS, KANAKANAGAR MAIN ROAD, VISWANTH NAGENHALLI. R.T.NAGAR POST  BANGALORE -560032', '', 'Bangalore', '2017-12-25', '2017-12-27', '2017-12-22', '2017-12-22', '', '', NULL, 'O', 'PROTEAM Tennis Academy', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-12-21', '2017-12-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/25th Dec.2017 Proteam tennis Academy35131.doc', '2017-12-21 07:16:59', 'A', NULL),
(176, 'Talent Series 3 Boys & Girls U-12', NULL, NULL, 'Core Sports Academy', '#3/4,DASANAPURA HOBLI, KACHOHALLIE , BENGALURU NORTH,BENGALURU-560091', '', 'Bangalore', '2017-12-25', '2017-12-27', '2017-12-22', '2017-12-22', '', '', NULL, 'O', 'Core Sports Academy', 'Cubbon Park Bangalore 560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-12-21', '2017-12-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/25th Dec.2017 Core Sports Academy3721.doc', '2017-12-21 07:20:59', 'A', NULL),
(177, 'Men\'s 50K', NULL, NULL, 'Namma Sportz Tennis Academy', 'SMVN School campus, Judicial Layout, Off Kanakapura Road, Thalaghattapura, Bangalore 560 062.', '', 'Bangalore', '2017-12-25', '2017-12-29', '2017-12-04', '2017-12-18', '', '50000', NULL, 'O', 'Namma Sportz Tennis Academy', 'KSLTA ,CUBBON PARK ,BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-12-21', '2017-12-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/25th Dec.2017 Namma Sports Tennis Academy18963.doc', '2017-12-21 07:25:30', 'A', NULL),
(178, 'Talent Series 3 Boys & Girls U-12', NULL, NULL, 'SPORTSCULT Tennis Academy', 'NO: 93, NEAR PRAGATHI SCHOOL, OPP TO STATE BANK OF MYSORE, KUDLU GATE, BOMMANAHALLI, BANGALORE.', '', 'Bangalore', '2018-01-01', '2018-01-03', '2017-12-29', '2017-12-29', '', '', NULL, 'O', 'SPORTSCULT Tennis Academy', 'Cubbon Park Bangalore 560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-12-21', '2017-12-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/1st Jan.2018 Sports Cult35956.doc', '2017-12-21 07:31:26', 'A', NULL),
(179, 'Championship Series 7 Boys & Girls U-18', NULL, NULL, 'Mysore Tennis Club ', 'Krishnaraj Boulevard,  2nd cross, Chamarajapuram, Mysore - 570005', '', 'Mysuru', '2018-01-08', '2018-01-12', '2017-12-18', '2018-01-01', '', '', NULL, 'O', 'Mysore Tennis Club', '', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-12-21', '2017-12-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '12', 'uploads/factsheets/8th,Jan.2018 KTPPA4458.doc', '2017-12-21 07:39:01', 'A', NULL),
(180, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'SPORTSCULT Tennis Academy', 'NO: 93, NEAR PRAGATHI SCHOOL, OPP TO STATE BANK OF MYSORE, KUDLU GATE, BOMMANAHALLI, BANGALORE.', '', 'Bangalore', '2018-01-08', '2018-01-12', '2017-12-18', '2018-01-01', '', '', NULL, 'O', 'SPORTSCULT Tennis Academy', 'KSLTA ,CUBBON PARK ,BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-12-21', '2017-12-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/8th,Jan.2018 Sports Cult58900.doc', '2017-12-21 08:17:40', 'A', NULL),
(181, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'ELITE TENNIS ACADEMY', 'ELITE TENNIS ACADEMY #51/6, Halanayyakanahalli- (Sarjapur Main Road – Road Opp Wipro Office ), Opp to Rainbow retreat Layout, Near to Nimritha Green County\r\nBangalore – 560035, Karnataka – INDIA\r\n', '', 'Bangalore', '2018-01-15', '2018-01-19', '2017-12-25', '2018-01-08', '', '', NULL, 'O', 'Elite Tennis Academy', 'CUBBON PARK, BANGALORE    ', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-12-21', '2017-12-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/15th Jan.2018 Elite Tennis Academy36129.doc', '2017-12-21 08:16:17', 'A', NULL),
(182, 'Talent Series 3 Boys & Girls U-18', NULL, NULL, 'ELITE TENNIS ACADEMY', 'ELITE TENNIS ACADEMY #51/6, Halanayyakanahalli- (Sarjapur Main Road – Road Opp Wipro Office ), Opp to Rainbow retreat Layout, Near to Nimritha Green County\r\nBangalore – 560035, Karnataka – INDIA\r\n', '', 'Bangalore', '2018-01-15', '2018-01-19', '2017-12-25', '2018-01-08', '', '', NULL, 'O', 'Elite Tennis Academy', 'CUBBON PARK, BANGALORE    ', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-12-21', '2017-12-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '12', 'uploads/factsheets/15th Jan.2018 Elite Tennis Academy87958.doc', '2017-12-21 08:15:58', 'A', NULL),
(183, 'Talent Series 3 Boys & Girls U-10', NULL, NULL, 'Fortune Sports Academy', 'Premises of Krishna Priya Convention Hall Campaus, Bangalore-Mysore Road, Kengeri, Bangalore-560 060. (Landmark:next to Kengeri Bus Stand/HP Petrol Bunk)', '', 'Bangalore', '2018-01-15', '2018-01-17', '2018-01-13', '2018-01-13', '', '', NULL, 'O', 'FORTUNE SPORTS ACADEMY', 'CUBBON PARK, BANGALORE    ', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-12-21', '2017-12-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/15th,jan.2018 Fortune Sports Academy94600.doc', '2017-12-21 08:13:32', 'A', NULL),
(184, 'Talent Series 3 Boys & Girls U-10', NULL, NULL, 'ELITE TENNIS ACADEMY', 'ELITE TENNIS ACADEMY – 4/5 VASANTHAPURA MAIN ROAD, OFF KANAKAPURA ROAD, BENGALURU, KARNATAKA-560061 ', '', 'Bangalore', '2018-01-22', '2018-01-24', '2018-01-19', '2018-01-19', '', '', NULL, 'O', 'Elite Tennis Academy', 'CUBBON PARK, BANGALORE    ', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-12-21', '2017-12-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/22nd,Jan.2018 Elite Tennis Academy11738.doc', '2017-12-21 08:12:39', 'A', NULL),
(185, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'FORTUNE SPORTS ACADEMY', 'FORTUNE SPORTS ACADEMY - Premises of Krishna Priya Convention Hall Campus, Bangalore-Mysore Road, Kengeri, Bangalore-560 060. (Landmark: next to Kengeri Bus Stand/HP Petrol Bunk)', '', 'Bangalore', '2018-01-22', '2018-01-26', '2018-01-01', '2018-01-15', '', '', NULL, 'O', 'FORTUNE SPORTS ACADEMY', 'CUBBON PARK, BANGALORE    ', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-12-21', '2017-12-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/22nd,Jan.2018 Fortune Sports98533.doc', '2017-12-21 08:11:43', 'A', NULL),
(186, 'Men\'s 1 Lc', NULL, NULL, 'TENNIS TEMPLE ACADEMY', '#174,DASASHRAMA, NEAR PANDURANGA TEMPLE, 5TH BLOCK RAJAJINAGAR, , BENGALURU: 560010', '', 'Bangalore', '2018-01-29', '2018-02-02', '2018-01-08', '2018-01-22', '', '100000', NULL, 'O', 'Tennis Temple Academy', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-12-21', '2017-12-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/29th Jan.2018 KTPPA51263.doc', '2017-12-21 08:11:10', 'A', NULL),
(187, 'women\'s 1 Lc', NULL, NULL, 'TENNIS TEMPLE ACADEMY', '#174,DASASHRAMA, NEAR PANDURANGA TEMPLE, 5TH BLOCK RAJAJINAGAR, , BENGALURU: 560010', '', 'Bangalore', '2018-01-29', '2018-02-02', '2018-01-08', '2018-01-22', '', '100000', NULL, 'O', 'Tennis Temple Academy', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-12-21', '2017-12-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '13', 'uploads/factsheets/29th Jan.2018 KTPPA92515.doc', '2017-12-21 08:10:36', 'A', NULL),
(188, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'TRUE BOUNCE TENNIS ACADEMY', 'TRUE BOUNCE TENNIS ACADEMY -   Seegehalli,(Whitefield- Hoskote Road) , Near HP Petrol Pump - Chrysalis High School, Bengaluru, Karnataka 560067 ', '', 'Bangalore', '2018-01-29', '2018-02-02', '2018-01-08', '2018-01-22', '', '', NULL, 'O', 'TRUE BOUNCE TENNIS ACADEMY', 'CUBBON PARK, BANGALORE    ', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-12-21', '2017-12-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/29th Jan.2018 True Bounce Tennis Academy40933.doc', '2017-12-21 08:10:01', 'A', NULL),
(189, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'TRUE BOUNCE TENNIS ACADEMY', 'TRUE BOUNCE TENNIS ACADEMY -   Seegehalli,(Whitefield- Hoskote Road) , Near HP Petrol Pump - Chrysalis High School, Bengaluru, Karnataka 560067 ', '', 'Bangalore', '2018-01-29', '2018-02-02', '2018-01-08', '2018-01-22', '', '', NULL, 'O', 'TRUE BOUNCE TENNIS ACADEMY', 'CUBBON PARK, BANGALORE    ', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2017-12-21', '2017-12-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/29th Jan.2018 True Bounce Tennis Academy50309.doc', '2017-12-21 08:09:38', 'A', NULL),
(190, 'ATF Junior Series without Hospitality ', NULL, NULL, 'JAIN GLOBAL CAMPUS Jain International Residential School', 'JAIN GLOBAL CAMPUS Jain International Residential School – Bangalore Jakkasandra Post , Kanakapu, City:BANGALORE\r\nPhone:91 080 27577005\r\nEmail:jirsaita@gmail.com\r\n', '', 'Bangalore', '2018-02-05', '2018-02-10', '2018-01-09', '2018-01-23', '', '', NULL, 'O', 'JAIN GLOBAL CAMPUS Jain International Residential School', 'Cubbon Park, Bengaluru-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-01-12', '2018-01-15 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/5th,Feb.2017 Jain Global22731.doc', '2018-01-12 23:21:28', 'A', NULL),
(191, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'TRUE BOUNCE TENNIS ACADEMY', 'Kaggadasapura,  Holy Cross School Rd, Malleshpalya(Near More Super Market) – Bengaluru- 560093', '', 'Bangalore', '2018-02-05', '2018-02-09', '2018-01-15', '2018-01-29', '', '', NULL, 'O', 'TRUE BOUNCE TENNIS ACADEMY', 'Cubbon Park, Bengaluru', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-01-12', '2018-01-11 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/5th,Feb.2017 True Bounce Tennis Academy29027.doc', '2018-01-12 23:29:55', 'A', NULL),
(192, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'TRUE BOUNCE TENNIS ACADEMY', 'Kaggadasapura,  Holy Cross School Rd, Malleshpalya(Near More Super Market) – Bengaluru- 560093', '', 'Bangalore', '2018-02-05', '2018-02-09', '2018-01-15', '2018-01-29', '', '', NULL, 'O', 'TRUE BOUNCE TENNIS ACADEMY', 'Cubbon Par, Bengaluru-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-01-12', '2018-01-11 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/5th,Feb.2017 True Bounce Tennis Academy73326.doc', '2018-01-12 23:34:33', 'A', NULL),
(193, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'Elite Tennis Academy', '#51/6, Halanayyakanahalli- (Sarjapur Main Road – Road Opp Wipro Office ), Opp to Rainbow retreat Layout, Near to Nimritha Green County\r\nBangalore – 560035, Karnataka – INDIA\r\n', '', 'Bangalore', '2018-02-12', '2018-02-16', '2018-01-22', '2018-02-05', '', '', NULL, 'O', 'Elite Tennis Academy', 'Cubbon Par, Bengalluru', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-01-12', '2018-01-11 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/12th,Feb.2017 Elite Tennis Academy19221.doc', '2018-01-12 23:44:33', 'A', NULL),
(194, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'ELITE TENNIS ACADEMY', '#51/6, Halanayyakanahalli- (Sarjapur Main Road – Road Opp Wipro Office ), Opp to Rainbow retreat Layout, Near to Nimritha Green County\r\nBangalore – 560035, Karnataka – INDIA\r\n', '', 'Bangalore', '2018-02-12', '2018-02-16', '2018-01-22', '2018-02-05', '', '', NULL, 'O', 'Elite Tennis Academy', 'Cubbon Park, Bengaluru-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-01-12', '2018-01-11 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/12th,Feb.2017 Elite Tennis Academy21750.doc', '2018-01-12 23:46:48', 'A', NULL),
(195, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'Transform Tennis Academy', 'Sri Tirumalaraya Swamy Temple Road, Thubarahalli extended road, Thubarahalli, Whitefield Main Road, Marathalli, Bangalore-560066', '', 'Bangalore', '2018-02-19', '2018-02-23', '2018-01-29', '2018-02-09', '', '', NULL, 'O', 'Transform Tennis Academy', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-01-12', '2018-01-11 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/19th Feb.2017 Transfform Tennis Academy66251.doc', '2018-01-12 23:53:27', 'A', NULL),
(196, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'Transform Tennis Academy', 'Sri Tirumalaraya Swamy Temple Road, Thubarahalli extended road, Thubarahalli, Whitefield Main Road, Marathalli, Bangalore-560066', '', 'Bangalore', '2018-02-19', '2018-02-23', '2018-01-29', '2018-02-09', '', '', NULL, 'O', 'Transform Tennis Academy', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-01-12', '2018-01-11 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/19th Feb.2017 Transfform Tennis Academy99785.doc', '2018-01-12 23:55:04', 'A', NULL),
(197, 'All India Inter State team Tennis Chamionship - 2018', NULL, NULL, 'Bhilai Tennis Club', 'Next to Bhilai Hotel (Bhilai Niwas), near Civic Centre, Bhilai ', '', 'Bhilai', '2018-02-19', '2018-02-24', '2018-01-19', '2018-01-28', '', '', NULL, 'O', 'Bhilai Tennis Club', 'Cubbon Park , Bengaluru-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-01-13', '2018-01-12 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', 'uploads/factsheets/INTERSTATE MENS BHILAI Fact Sheet69784.doc', '2018-01-13 00:03:43', 'A', NULL),
(198, 'Talent Series 3 Boys & Girls U-10', NULL, NULL, 'ELITE TENNIS ACADEMY', 'ELITE TENNIS ACADEMY – 4/5 VASANTHAPURA MAIN ROAD, OFF KANAKAPURA ROAD, BENGALURU, KARNATAKA-560061 ', '', 'Bangalore', '2018-02-19', '2018-02-21', '2018-02-09', '2018-02-09', '', '', NULL, 'O', 'Elite Tennis Academy', 'CUBBON PARK, BANGALORE    ', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-01-13', '2018-01-12 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/19th Feb.2017 Transfform Tennis Academy18978.doc', '2018-01-13 00:06:54', 'A', NULL),
(199, 'Championship Series 3 Boys & Girls U-18', NULL, NULL, 'SPORTS CULT TENNIS ACADEMY', '# 505, ITI LAYOUT, MANGAMMAPALYA, NEAR OLD UJALA FACTORY, VASAPALYA MAINROAD, BENGALURU-560 068', '', 'Bangalore', '2018-02-19', '2018-02-21', '2018-02-16', '2018-02-16', '', '', NULL, 'O', 'SPORTSCULT Tennis Academy', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-01-15', '2018-01-14 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '12', 'uploads/factsheets/19th Feb.2019 Sports Cult11865.doc', '2018-01-15 23:35:47', 'A', NULL),
(200, 'Men\'s 50K', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bangalore', '2018-02-26', '2018-03-02', '2018-02-05', '2018-02-17', '', '50000', NULL, 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-01-15', '2018-01-14 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/26th Feb.2018 SAT Sports28234.doc', '2018-01-15 23:42:47', 'A', NULL),
(201, 'WOMEN’S 50K', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bangalore', '2018-02-26', '2018-03-02', '2018-02-05', '2018-02-17', '', '50000', NULL, 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE – 560 001\r\n', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-01-16', '2018-01-15 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', 'uploads/factsheets/26th Feb.2018 SAT Sports58005.doc', '2018-01-16 00:04:05', 'A', NULL),
(202, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'ELITE TENNIS ACADEMY', ' #51/6, Halanayyakanahalli- (Sarjapur Main Road – Road Opp Wipro Office ), Opp to Rainbow retreat Layout, Near to Nimritha Green County\r\nBangalore – 560035\r\n', '', 'Bangalore', '2018-02-26', '2018-03-02', '2018-02-05', '2018-02-16', '', '', NULL, 'O', 'Elite Tennis Academy', 'CUBBON PARK, BANGALORE    ', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-01-16', '2018-01-15 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', NULL, '0000-00-00 00:00:00', 'A', NULL),
(203, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'ELITE TENNIS ACADEMY', '#51/6, Halanayyakanahalli- (Sarjapur Main Road – Road Opp Wipro Office ), Opp to Rainbow retreat Layout, Near to Nimritha Green County\r\nBangalore – 560035\r\n', '', 'Bangalore', '2018-02-26', '2018-03-02', '2018-02-05', '2018-02-16', '', '', NULL, 'O', 'Elite Tennis Academy', 'CUBBON PARK, BANGALORE    ', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-01-16', '2018-01-15 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/26th Feb.2018 Elite Tennis Academy34009.doc', '2018-01-16 00:21:59', 'A', NULL),
(204, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'BANGALORE TENNIS ACADEMY', '104, Chengamaraju Sapota Garden, Guru Raghavendra Nagar, Opp. RBI Colony, Vinayaka Vidyalaya , J.P Nagar 7th Phase, Eswara Layout, JP Nagar 7th Phase, JP Nagar, Bengaluru, Karnataka 560078', '', 'Bangalore', '2018-03-05', '2018-03-09', '2018-02-12', '2018-02-26', '', '', NULL, 'O', 'TENNIS360 & BANGALORE TENNIS ACADEMY', 'CUBBON PARK, BANGALORE    ', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-02-14', '2018-02-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/5th Mar.2018 Tennis 36055592.doc', '2018-02-14 04:28:40', 'A', NULL),
(205, 'Men\'s 50K', NULL, NULL, 'TENNIS TEMPLE ACADEMY', '#174,DASASHRAMA, NEAR PANDURANGA TEMPLE, 5TH BLOCK RAJAJINAGAR, , BENGALURU: 560010', '', 'Bangalore', '2018-03-12', '2018-03-16', '2018-02-19', '2018-03-05', '', '50000', NULL, 'O', 'TENNIS HIVE', '#174,DASASHRAMA, NEAR PANDURANGA TEMPLE, 5TH BLOCK RAJAJINAGAR, BENGALURU: 560010', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-02-14', '2018-02-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/12th Mar 2018 Tennis Hive78349.doc', '2018-02-14 04:41:39', 'A', NULL),
(206, 'WOMEN’S 50K', NULL, NULL, 'TENNIS TEMPLE ACADEMY', '#174,DASASHRAMA, NEAR PANDURANGA TEMPLE, 5TH BLOCK RAJAJINAGAR, , BENGALURU: 560010', '', 'Bangalore', '2018-03-12', '2018-03-16', '2018-02-19', '2018-03-05', '', '50000', NULL, 'O', 'TENNIS HIVE ', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-02-14', '2018-02-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', 'uploads/factsheets/12th Mar 2018 Tennis Hive83832.doc', '2018-02-14 04:41:11', 'A', NULL),
(207, 'Talent Series 3 Boys & Girls U-10', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bangalore', '2018-02-12', '2018-02-13', '2018-02-10', '2018-02-10', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-03-07', '2018-03-06 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/12th Feb.2018 SAT Sports34538.doc', '2018-03-07 06:00:51', 'A', NULL),
(208, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bangalore', '2018-04-02', '2018-04-06', '2018-03-12', '2018-03-26', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-03-16', '2018-03-15 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/2nd,Apr.2018 SAT Sports28766.doc', '2018-03-16 06:53:28', 'A', NULL),
(209, 'Talent Series 3 Boys & Girls U-16', NULL, NULL, 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', 'CUBBON PARK, BANGALORE – 560 001', '', 'Bangalore', '2018-04-09', '2018-04-10', '2018-04-06', '2018-04-06', '', '', NULL, 'O', 'CRS TRUST', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-03-16', '2018-03-15 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/9th Apr.2018 CRS Trust54458.doc', '2018-03-16 06:59:34', 'A', NULL),
(210, 'Championship Series 3 Boys & Girls U-12', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bangalore', '2018-04-16', '2018-04-18', '2018-04-13', '2018-04-13', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-03-16', '2018-03-15 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/16th,Apr.2018 SAT Sports66340.doc', '2018-03-16 07:15:50', 'A', NULL),
(211, 'Super Series U-16 Boys & Girls ', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bangalore', '2018-04-23', '2018-04-27', '2018-04-02', '2018-04-16', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE – 560 001\r\n', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-03-16', '2018-03-15 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', NULL, '0000-00-00 00:00:00', 'A', NULL),
(212, 'Men\'s 1 Lc', NULL, NULL, 'Century Club', 'No 1 , Seshadri Road ,Bangalore – 560001(LAND MARK: NEAR KR CIRCLE)', '', 'Bangalore', '2018-04-30', '2018-05-04', '2018-04-09', '2018-04-21', '', '100000', NULL, 'O', 'Century Club', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-03-16', '2018-03-15 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/30th Apr.2018 Century Club10290.doc', '2018-03-16 07:31:59', 'A', NULL),
(213, 'women\'s 1 Lc', NULL, NULL, 'Century Club', 'No 1 , Seshadri Road ,Bangalore – 560001(LAND MARK: NEAR KR CIRCLE)', '', 'Bangalore', '2018-04-30', '2018-05-04', '2018-04-09', '2018-04-21', '', '100000', NULL, 'O', 'Century Club', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-03-16', '2018-03-15 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', 'uploads/factsheets/30th Apr.2018 Century Club64163.doc', '2018-03-16 07:31:41', 'A', NULL),
(214, 'RENDEZ – VOUS A ROLAND – GARROS – NATIONAL SERIES', NULL, NULL, 'Bowring Institute', '19, St Marks Road, Bengaluru - 560001\r\nKarnataka\r\n', '', 'Bangalore', '2018-04-30', '2018-05-05', '2018-04-09', '2018-04-23', '', '', 'uploads/tournamemnt_img/ksltatournament8064.jpg', 'O', 'Bowring Institute', 'CUBBON PARK, BENGALURU – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-03-16', '2018-04-27 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '12', 'uploads/factsheets/30th Apr.2018 Bowring Institute96588.doc', '2018-03-16 07:37:52', 'A', NULL);
INSERT INTO `ksl_tournaments_tbl` (`tourn_id`, `tourn_title`, `tourn_category`, `tourn_sub_category`, `tourn_venue`, `tourn_venue_address1`, `tourn_venue_address2`, `tourn_venue_city`, `tourn_from_date`, `tourn_to_date`, `tourn_last_submission_date`, `tourn_withdrl_date`, `tourn_description`, `tourn_price_money`, `tourn_tournament_image`, `tourn_register_status`, `tourn_asso_title`, `tourn_asso_address1`, `tourn_asso_address2`, `tourn_asso_head`, `tourn_refree1`, `tourn_refree2`, `tourn_refree3`, `tourn_link`, `tourn_created_date`, `tourn_lastmodified_date`, `tourn_status`, `sponser_name_of_organisation`, `sponser_address1`, `sponser_address2`, `sponser_contact_person_name`, `sponser_phone`, `sponser_email`, `tour_event_category`, `tour_event_subcategory`, `tourn_factsheet`, `tourn_factsheet_uploaded_at`, `tourn_facesheet_status`, `tourn_gallery_id`) VALUES
(215, 'Talent Series 3 Boys & Girls U-10', NULL, NULL, 'FORTUNE SPORTS ACADEMY', 'FORTUNE SPORTS ACADEMY – Mysore Rd, Fort Kengeri, Kengeri Satellite Town, Bengaluru, Karnataka 560060 ', '', 'Bangalore', '2018-04-09', '2018-04-10', '2018-04-07', '2018-04-07', '', '', NULL, 'O', 'TENNIS360 & FORTUNE SPORTS TENNIS ', 'CUBBON PARK, BANGALORE  ', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-03-23', '2018-03-22 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/9th Apr.2018 Tennis 36094474.doc', '2018-03-23 02:48:57', 'A', NULL),
(216, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'Proteam Tennis Academy', '25/3,21ST CROSS, KANAKANAGAR MAIN ROAD, VISWANTH NAGENHALLI. R.T.NAGAR POST  BANGALORE -560032', '', 'Bangalore', '2018-04-30', '2018-05-04', '2018-04-09', '2018-04-23', '', '', NULL, 'O', 'PROTEAM Tennis Academy', 'CUBBON PARK,  BANGALORE-560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-03-23', '2018-03-22 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/30th Apr.2018 Proteam Tennis Academy72011.doc', '2018-03-23 02:56:10', 'A', NULL),
(217, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bangalore', '2018-05-07', '2018-05-11', '2018-04-16', '2018-04-30', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-04-17', '2018-04-16 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/7th May.2018 SAT Sports95864.doc', '2018-04-17 01:35:56', 'A', NULL),
(218, 'Championship Series 3 Boys & Girls U-18', NULL, NULL, 'Noah Tennis Academy ', 'Noah Tennis Academy, N0 .28 Jayanti Nagar, Horamavu Mn Rd, Next to Vibgyor International School Bangalore', '', 'Bengaluru', '2018-05-07', '2018-05-09', '2018-05-04', '2018-05-04', '', '', NULL, 'O', 'Noah Tennis Academy ', 'CUBBONPARK, BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-04-17', '2018-04-16 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '12', 'uploads/factsheets/7th May.2018 Tennis Hive Foundation32184.doc', '2018-04-17 01:43:18', 'A', NULL),
(219, 'Championship Series3 Boys & Girls U-16', NULL, NULL, 'PROTEAMTENNIS ACADEMY ', '25/3,21ST CROSS, KANAKANAGAR MAIN ROAD, VISWANTH NAGENHALLI. R.T.NAGAR POST  BANGALORE -560032', '', 'Bengaluru', '2018-05-14', '2018-05-16', '2018-05-11', '2018-05-11', '', '', NULL, 'O', 'PROTEAMTENNIS ACADEMY ', 'CUBBONPARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-04-17', '2018-04-16 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/14th May.2018 Proteam Tennis Academy15065.doc', '2018-04-17 01:50:42', 'A', NULL),
(220, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bengaluru', '2018-05-21', '2018-05-25', '2018-04-30', '2018-05-14', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-04-17', '2018-04-16 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/21st May.2018 SAT Sports17201.doc', '2018-04-17 02:01:18', 'A', NULL),
(221, 'Talent Series 3 Boys & Girls U-10', NULL, NULL, 'PROTEAMTENNIS ACADEMY ', '25/3,21ST CROSS, KANAKANAGAR MAIN ROAD, VISWANTH NAGENHALLI. R.T.NAGAR POST  BANGALORE -560032', '', 'Bengaluru', '2018-05-21', '2018-05-23', '2018-05-12', '2018-05-12', '', '', NULL, 'O', 'PROTEAMTENNIS ACADEMY', 'CUBBONPARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-04-17', '2018-04-16 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/21st May.2018n Proteam tennis Academy63706.doc', '2018-04-17 02:07:23', 'A', NULL),
(222, 'Championship Series 3 Boys & Girls U-18', NULL, NULL, 'NammaSportz Tennis Academy', 'SMVN School campus, Judicial Layout, Off Kanakapura Road, Thalaghattapura, Bangalore 560 062', '', 'Bengaluru', '2018-05-21', '2018-05-23', '2018-05-18', '2018-05-18', '', '', NULL, 'O', 'Namma Sportz Tennis Academy', 'KSLTA ,CUBBON PARK ,BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-04-17', '2018-04-16 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '12', 'uploads/factsheets/21st May.2018 Namma sportz Tennis Academy56785.doc', '2018-04-17 02:11:57', 'A', NULL),
(223, 'Men\'s 50K', NULL, NULL, 'DAVANGERE DISTRICT TENNIS ASSOCIATION', 'GOVT HIGH SCHOOL FIELD GROUND\r\nDAVANGERE- 577002\r\n', '', 'Davangere', '2018-05-21', '2018-05-25', '2018-04-23', '2018-05-14', '', '50000', NULL, 'O', 'DAVANGERE DISTRICT TENNIS ASSOCIATION', 'CUBBON PARK, BENGALURU.', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-04-17', '2018-04-16 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/21st May.2018 Davangere District Tennis Association30522.doc', '2018-04-17 02:28:32', 'A', NULL),
(224, 'WOMEN’S 50K', NULL, NULL, 'DAVANGERE DISTRICT TENNIS ASSOCIATION', 'GOVT HIGH SCHOOL FIELD GROUND\r\nDAVANGERE- 577002\r\n', '', 'Davangere', '2018-05-21', '2018-05-25', '2018-04-30', '2018-05-14', '', '50000', NULL, 'O', 'DAVANGERE DISTRICT TENNIS ASSOCIATION', 'CUBBON PARK, BENGALURU.', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-04-17', '2018-04-16 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', 'uploads/factsheets/21st May.2018 Davangere District Tennis Association41996.doc', '2018-04-17 02:28:48', 'A', NULL),
(225, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bengaluru', '2018-05-28', '2018-06-01', '2018-05-07', '2018-05-21', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-04-17', '2018-04-16 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/28th May.2018 SAT Sports27944.doc', '2018-04-17 02:35:54', 'A', NULL),
(226, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'ROHAN BOPANNA TENNIS ACADEMY', '53, Venkatala Village, On the road to Canadian International School, Yelahanka, Bangalore 560064', '', 'Bengaluru', '2018-05-28', '2018-06-01', '2018-05-07', '2018-05-21', '', '', NULL, 'O', 'ROHAN BOPANNA TENNIS ACADEMY', 'CUBBON PARK, BANGALORE    ', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-04-17', '2018-04-16 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/28th May.2018 RBTA65693.doc', '2018-04-17 02:46:48', 'A', NULL),
(227, 'Men\'s 50K', NULL, NULL, 'Noah Tennis Academy ', 'Noah Tennis Academy, N0 .28 Jayanti Nagar, Horamavu Mn Rd, Next to Vibgyor International School Bangalore', '', 'Bengaluru', '2018-05-28', '2018-06-01', '2018-05-07', '2018-05-21', '', '50000', NULL, 'O', 'PROFESSOINAL TENNIS HIVE FOUNDATION ', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-04-17', '2018-04-16 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', NULL, '0000-00-00 00:00:00', 'A', NULL),
(228, 'WOMEN’S 50K', NULL, NULL, 'Noah Tennis Academy', 'Noah Tennis Academy, N0 .28 Jayanti Nagar, Horamavu Mn Rd, Next to Vibgyor International School Bangalore', '', 'Bengaluru', '2018-05-28', '2018-06-01', '2018-05-07', '2018-05-21', '', '50000', NULL, 'O', 'PROFESSOINAL TENNIS HIVE FOUNDATION', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-04-17', '2018-04-16 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/28th May.2018 Tennis Hive74981.doc', '2018-04-17 03:00:22', 'A', NULL),
(229, 'Championship Series3 Boys & Girls U-16', NULL, NULL, 'CITY INSTITUTE', 'BESIDE DISTRICT LIBRARY\r\nCHITRADURGA—577501', '', 'Chitradurga', '2018-04-30', '2018-05-02', '2018-04-27', '2018-04-27', '', '', NULL, 'O', 'CITY INSTITUTE', 'CUBBON PARK, BENGALURU', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-05-03', '2018-05-02 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/30th Apr.2018 City Institute Chitradurga22958.doc', '2018-05-03 03:03:12', 'A', NULL),
(230, 'Championship Series 3 Boys & Girls U-18', NULL, NULL, 'SAT SPORTS PVT LTD', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bengaluru', '2018-06-04', '2018-06-06', '2018-06-01', '2018-06-01', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-05-19', '2018-05-18 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '12', 'uploads/factsheets/4th Jun.2018 SAT Sports22304.doc', '2018-05-19 02:08:35', 'A', NULL),
(231, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'PROTEAMTENNIS ACADEMY', '25/3,21ST CROSS, KANAKANAGAR MAIN ROAD, VISWANTH NAGENHALLI. R.T.NAGAR POST  BANGALORE -560032', '', 'Bengaluru', '2018-06-04', '2018-06-08', '2018-05-14', '2018-05-28', '', '', NULL, 'O', 'PROTEAMTENNIS ACADEMY', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-05-19', '2018-05-18 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/4th Jun.2018 Proteam Tennis academy50442.doc', '2018-05-19 02:13:21', 'A', NULL),
(232, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'SAT SPORTS Pvt Ltd', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bengaluru', '2018-06-11', '2018-06-15', '2018-05-21', '2018-06-04', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-05-19', '2018-05-18 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/11th Jun.2018 SAT Sports92783.doc', '2018-05-19 02:22:30', 'A', NULL),
(233, 'Talent Series 3 Boys & Girls U-10', NULL, NULL, 'SAT SPORTS Pvt Ltd', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bengaluru', '2018-06-18', '2018-06-20', '2018-06-16', '2018-06-16', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-05-19', '2018-05-18 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/18th Jun.2018 SAT Sports10645.doc', '2018-05-19 02:31:06', 'A', NULL),
(234, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'FORTUNE SPORTS ACADEMY', 'Mysore Rd, Fort Kengeri, Kengeri Satellite Town, Bengaluru, Karnataka 560060', '', 'Bengaluru', '2018-06-25', '2018-06-29', '2018-06-04', '2018-06-18', '', '', NULL, 'O', 'FORTUNE SPORTS ACADEMY', 'CUBBON PARK, BANGALORE    ', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-05-19', '2018-05-18 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/25th Jun.2018 Fortune Sports Academy20327.doc', '2018-05-19 02:34:50', 'A', NULL),
(235, 'Championship Series3 Boys & Girls U-16', NULL, NULL, 'Namma Sportz Tennis Academy', 'SMVN School campus, Judicial Layout, Off Kanakapura Road, Thalaghattapura, Bangalore 560 062', '', 'Bengaluru', '2018-06-25', '2018-06-27', '2018-06-22', '2018-06-22', '', '', NULL, 'O', 'Namma Sportz Tennis Academy', 'KSLTA ,CUBBON PARK ,BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-05-19', '2018-05-18 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/25th Jun.2018 Namma Sportz5012.doc', '2018-05-19 02:56:44', 'A', NULL),
(236, 'Talent Series 3 Boys & Girls U-10', NULL, NULL, 'Mahila Seva Samaja', '# 80 K R Road Shankar puram Bangalore -560004', '', 'Bengaluru', '2018-06-11', '2018-06-13', '2018-06-09', '2018-06-09', '', '', NULL, 'O', 'Mahila Seva Samaja', 'Cubbon Park Bangalore 56001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-06-21', '2018-06-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/11th Jun.2018 Mahila Seva Samaja76132.doc', '2018-06-21 04:50:03', 'A', NULL),
(237, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'Namma Sportz Tennis Academy', 'SMVN School campus, Judicial Layout, Off Kanakapura Road, Thalaghattapura, Bangalore 560 062', '', 'Bengaluru', '2018-07-16', '2018-07-20', '2018-06-25', '2018-07-09', '', '', NULL, 'O', 'Namma Sportz Tennis Academy', 'CUBBON PARK ,BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-06-21', '2018-06-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/16th,Jul.2018. Namma Tennis Academy99732.doc', '2018-06-21 04:54:59', 'A', NULL),
(238, 'Talent Series 3 Boys & Girls U-14', NULL, NULL, 'Tennis Temple Academy ', '– #174,dasashrama, near panduranga temple, 5TH block Rajajinagar, , Bengaluru: 560010', '', 'Bengaluru', '2018-07-09', '2018-07-10', '2018-07-06', '2018-07-06', '', '', NULL, 'O', 'Tennis Temple Academy ', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-07-03', '2018-07-02 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/9th,Jul.2018 Tennis Temple18020.doc', '2018-07-03 01:55:38', 'A', NULL),
(239, 'Talent Series 3 Boys & Girls U-12', NULL, NULL, 'Tennis Temple Academy', '#174,dasashrama, near panduranga temple, 5TH block Rajajinagar, , Bengaluru: 560010', '', 'Bengaluru', '2018-07-23', '2018-07-25', '2018-07-20', '2018-07-20', '', '', NULL, 'O', 'Tennis Temple Academy', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-07-03', '2018-07-02 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/23rd,Jul.2018 Tennis Temple41767.doc', '2018-07-03 02:00:08', 'A', NULL),
(240, 'Championship Series 3 Boys & Girls U-12', NULL, NULL, 'POLICE GYMKHANA', 'PARVATI NAGAR, 2ND CROSS, BALLARI – 583 101, KARNATAKA', '', 'Bellari', '2018-07-30', '2018-08-01', '2018-07-27', '2018-07-27', '', '', NULL, 'O', 'POLICE GYMKHANA', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-07-03', '2018-07-02 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/30th Jul.2018 Bellari85510.doc', '2018-07-03 02:07:11', 'A', NULL),
(241, 'Men\'s 1 Lc', NULL, NULL, 'SAT Sports Pvt Ltd.', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bengaluru', '2018-08-06', '2018-08-10', '2018-07-16', '2018-07-30', '', '100000', NULL, 'O', 'KTPPA', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-07-17', '2018-07-22 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/6th,Aug.2018 KTPPA78807.doc', '2018-07-17 04:49:59', 'A', NULL),
(242, 'women\'s 1 Lc', NULL, NULL, 'SAT Sports Pvt Ltd.', 'SY.NO: 9/4, PATEL RAMAIAH NAGAR, KOTHANUR, OFF HENNUR MAIN ROAD, (OPP. BPC PETROL BUNK,B’LORE  - 560 077)', '', 'Bengaluru', '2018-08-06', '2018-08-10', '2018-07-16', '2018-07-30', '', '100000', NULL, 'O', 'KTPPA', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-07-17', '2018-07-22 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', 'uploads/factsheets/6th,Aug.2018 KTPPA81643.doc', '2018-07-17 04:51:55', 'A', NULL),
(244, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'TENNIS TEMPLE ACADEMY', 'Venue – #174,dasashrama, near panduranga temple, 5TH block Rajajinagar, , Bengaluru: 560010', '', 'Bengaluru', '2018-08-13', '2018-08-17', '2018-07-23', '2018-08-06', '', '', NULL, 'O', 'Tennis Temple Academy', 'CUBBON PARK, BANGALORE    ', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-07-23', '2018-07-22 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/13th Aug.2018 Tennis Temple12319.doc', '2018-07-23 07:24:25', 'A', NULL),
(245, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'Namma Sportz Tennis Academy', 'SMVN School campus, Judicial Layout, Off Kanakapura Road, Thalaghattapura, Bangalore 560 062', '', 'Bengaluru', '2018-08-20', '2018-08-24', '2018-07-30', '2018-08-13', '', '', NULL, 'O', 'Namma Sportz Tennis Academy', 'KSLTA ,CUBBON PARK ,BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-07-23', '2018-07-22 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/20th Aug.2018 Namma Sportz Academy38229.doc', '2018-07-23 07:26:56', 'A', NULL),
(246, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'Mysore Tennis Club', 'VENUE – KRISHNARAJ BOULEVARD CHAMRAJAPURA, MYSURU – 57 0 005', '', 'Mysore', '2018-08-27', '2018-08-31', '2018-08-06', '2018-08-20', '', '', NULL, 'O', 'MYSORE TENNIS CLUB(MTC)', 'CUBBON PARK, BENGALURU – 560 001    ', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-07-23', '2018-07-22 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/27th Aug.2018 MTC29616.doc', '2018-07-23 07:37:44', 'A', NULL),
(247, 'Talent Series 3 Boys & Girls U-14', NULL, NULL, 'DHARWAD DSITRICT LAWN TENNIS ASSOCIATION', 'NEAR COURT CIRCLE, OPP SWIMMING POOL DHARWAD-8\r\nKARNATAKA \r\n', '', 'Dharwad', '2018-08-06', '2018-08-09', '2018-08-03', '2018-08-03', '', '', NULL, 'O', 'DHARWAD DSITRICT LAWN TENNIS ASSOCIATION', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-08-01', '2018-07-31 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/6th Aug.2018 DDLTA94498.doc', '2018-08-01 02:58:11', 'A', NULL),
(248, 'Championship Series 3 Boys & Girls U-18', NULL, NULL, 'NammaSportz Tennis Academy', 'SMVN School campus, Judicial Layout, Off Kanakapura Road, Thalaghattapura, Bangalore 560 062', '', 'Bengaluru', '2018-09-03', '2018-09-05', '2018-08-31', '2018-08-31', '', '', NULL, 'O', 'NammaSportz Tennis Academy', 'KSLTA ,CUBBON PARK ,BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-08-21', '2018-08-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '12', 'uploads/factsheets/3rd Sep.2018 Namma Sportz Academy37495.doc', '2018-08-21 02:53:27', 'A', NULL),
(249, 'Talent Series 3 Boys & Girls U-10', NULL, NULL, 'KANTEERAVA STADIUM', 'KANTEERAVA STADIUM(OPP MALLYA HOSPITAL), BENGALURU', '', 'Bengaluru', '2018-09-03', '2018-09-05', '2018-09-01', '2018-09-01', '', '', NULL, 'O', 'RMS', 'Cubbon Park Bangalore 560001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-08-21', '2018-08-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/3rd Sep.2018 RMS42863.doc', '2018-08-21 02:58:21', 'A', NULL),
(250, 'Championship Series 3 Boys & Girls U-12', NULL, NULL, 'Rio Sports Academy', 'N0 .192/2, Singena Agrahara Road (Electronic City Phase 2), Huskur Post, Bangalore 560 100', '', 'Bengaluru', '2018-09-24', '2018-09-26', '2018-09-21', '2018-09-21', '', '', NULL, 'O', 'PROFESSOINAL TENNIS HIVE FOUNDATION', 'CUBBONPARK, BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-08-21', '2018-08-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/24th Sep.2018 Professional Tennis Hive Foundation95251.doc', '2018-08-21 03:10:28', 'A', NULL),
(251, 'Talent Series 3 Boys & Girls U-10', NULL, NULL, 'KANTEERAVA STADIUM', '29TH  SEP – 18(SAT) / TIME: 9 AM – 10 AM\r\nVENUE: SRI. KANTEERAVA STADIUM(OPP MALLYA HOSPITAL), BENGALURU\r\n', '', 'Bengaluru', '2018-10-01', '2018-10-03', '2018-09-29', '2018-09-29', '', '', NULL, 'O', 'RMS', 'Cubbon Park Bangalore 56001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-09-19', '2018-09-18 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/1st Oct.2018 RMS97223.doc', '2018-09-19 03:03:05', 'A', NULL),
(252, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'Namma Sportz Tennis Academy', 'SMVN School campus, Judicial Layout, Off Kanakapura Road, Thalaghattapura, Bangalore 560 062', '', 'Bengaluru', '2018-10-08', '2018-10-12', '2018-09-17', '2018-10-01', '', '', NULL, 'O', 'Namma Sportz Tennis Academy', 'KSLTA ,CUBBON PARK ,BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-09-19', '2018-09-18 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/8th Oct.2018 Namma Sportz Academy92669.doc', '2018-09-19 03:17:31', 'A', NULL),
(253, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'Mysore Tennis Club', 'VENUE – KRISHNARAJ BOULEVARD CHAMRAJAPURA, MYSURU – 57 0 005', '', 'Mysore', '2018-10-15', '2018-10-19', '2018-09-24', '2018-10-08', '', '', NULL, 'O', 'MYSORE TENNIS CLUB(MTC)', 'CUBBON PARK, BENGALURU – 560001    ', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-09-19', '2018-09-18 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/15th Oct,2018 MTC51161.doc', '2018-09-19 04:18:08', 'A', NULL),
(254, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'Mysore Tennis Club', 'VENUE – KRISHNARAJ BOULEVARD CHAMRAJAPURA, MYSURU – 57 0 005', '', 'Mysore', '2018-10-15', '2018-10-19', '2018-09-24', '2018-10-08', '', '', NULL, 'O', 'MYSORE TENNIS CLUB(MTC)', 'CUBBON PARK, BENGALURU – 560 001    ', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-09-19', '2018-09-18 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/15th Oct,2018 MTC4122.doc', '2018-09-19 04:20:43', 'A', NULL),
(255, 'Championship Series 7 Boys & Girls U-14', NULL, NULL, 'Topspin Tennis Academy', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68', '', 'Bengaluru', '2018-10-22', '2018-10-26', '2018-10-01', '2018-10-15', '', '', NULL, 'O', 'Topspin Tennis Academy', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-09-19', '2018-09-18 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '3', '11', 'uploads/factsheets/22nd Oct,2018 Topspin3076.doc', '2018-09-19 07:19:59', 'A', NULL),
(256, 'Championship Series 7 Boys & Girls U-16', NULL, NULL, 'Topspin Tennis Academy', 'CUBBON PARK,  BANGALORE', '', 'Bengaluru', '2018-10-22', '2018-10-26', '2018-10-01', '2018-10-15', '', '', NULL, 'O', 'Topspin Tennis Academy', 'CUBBON PARK,  BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-09-19', '2018-09-18 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/22nd Oct,2018 Topspin50399.doc', '2018-09-19 07:17:16', 'A', NULL),
(257, 'Talent Series 3 Boys & Girls U-12', NULL, NULL, 'P.E.T. Tennis Stadium', 'P.E.T. Sports Complex, PES College of Engineering campus, Mandya-571401, Karnataka', '', 'Mandya', '2018-10-22', '2018-10-26', '2018-10-19', '2018-10-19', '', '', NULL, 'O', 'PET – Tenvic ', 'CUBBON PARK, BANGALORE – 560 001', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-09-19', '2018-09-18 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/22nd Oct.2018 PET-Tenvic87449.doc', '2018-09-19 07:01:21', 'A', NULL),
(258, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'Namma Sportz Tennis Academy', 'SMVN School campus, Judicial Layout, Off Kanakapura Road, Thalaghattapura, Bangalore 560 062', '', 'Bengaluru', '2018-10-29', '2018-11-02', '2018-10-08', '2018-10-22', '', '', NULL, 'O', 'Namma Sportz Tennis Academy', 'KSLTA ,CUBBON PARK ,BANGALORE', '', 'Karnataka state Lawn Tennis Association', '', '', '', '', '2018-09-19', '2018-09-18 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/29th Oct.2018 Namma Sportz Academy23893.doc', '2018-09-19 07:05:08', 'A', NULL),
(259, 'Bengaluru Open Wild Card Event Rs. 3 lac prize money All India Ranking event', NULL, NULL, 'KARNATAKA STATE LAWN TENNIS ASSOCIACTION ', 'CUBBON PARK \r\nBANGALORE - 560001\r\n', '', 'Bengaluru', '2018-10-22', '2018-10-27', '2018-10-08', '2018-10-15', '', '3,00,000', NULL, 'O', 'KSLTA', '', '', 'Karnataka State Lawn Tennis Association', '', '', '', '', '2018-10-08', '2018-10-07 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', NULL, '0000-00-00 00:00:00', 'A', NULL),
(260, 'ATP Challenger Bengaluru Open', NULL, NULL, 'KARNATAKA STATE LAWN TENNIS ASSOCIACTION', 'Cubbon Park\r\nBengaluru-560001', '', 'Bengaluru', '2018-11-12', '2018-11-17', '2018-10-29', '2018-11-05', '', '$1,50,000', 'uploads/tournamemnt_img/ksltatournament4811.jpg', 'O', 'KSLTA', '', '', 'Karnataka State Lawn Tennis Association', '', '', '', '', '2018-10-08', '2018-12-27 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '5', '13', NULL, '0000-00-00 00:00:00', 'A', NULL),
(261, 'Bengaluru Open-17 ATP Challenger', NULL, NULL, 'KSLTA', 'Cubbon Park Bengalur-560001', '', 'Bengaluru', '2017-11-20', '2017-11-25', '2017-11-17', '2017-11-17', '', '$1,00,000', 'uploads/tournamemnt_img/ksltatournament2890.jpg', 'O', 'Karnatala State Lawn Tennis Association', '', '', 'Karnatala State Lawn Tennis Association', '', '', '', '', '2018-10-13', '2018-10-12 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', NULL, '0000-00-00 00:00:00', 'A', NULL),
(262, 'AITA Champion Series3 U-18 ', NULL, NULL, 'Tennis Temple Academy ', 'Venue – #174,dasashrama, near panduranga temple, 5TH block Rajajinagar, , Bengaluru: 560010', '', 'Bengaluru', '2018-10-15', '2018-10-17', '2018-10-12', '2018-10-12', '', '', NULL, 'O', 'Tennis Temple', 'CUBBON PARK, BANGALORE – 560 001', '', ' KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-10-18', '2018-10-17 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '12', 'uploads/factsheets/15th Oct.2018 Tennis Temple69348.doc', '2018-10-18 05:28:26', 'A', NULL),
(263, 'Talent Series7 U-12 Boys & Girls', NULL, NULL, 'Tennis And More Sporting (Tams)', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066', '', 'Bengaluru', '2018-10-29', '2018-11-02', '2018-10-08', '2018-10-22', '', '', NULL, 'O', 'Tennis And More Sporting (Tams)', 'Cubbon Park Bangalore 56001', '', 'Karnataka State Lawn Tennis Association', '', '', '', '', '2018-10-18', '2018-10-17 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/29th Oct.2018 TAMS45335.doc', '2018-10-18 05:42:41', 'A', NULL),
(264, 'Talent Series7 U-12 Boys & Girls', NULL, NULL, 'Tennis And More Sporting (Tams)', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066', '', 'Bengaluru', '2018-11-05', '2018-11-09', '2018-10-15', '2018-10-29', '', '', NULL, 'O', 'Tennis And More Sporting ', '', '', 'Karnataka State Lawn Tennis Association', '', '', '', '', '2018-10-21', '2018-10-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/5th Nov,2018 TAMS57454.doc', '2018-10-21 04:23:17', 'A', NULL),
(265, 'Talent Series7 U-14 Boys & Girls', NULL, NULL, 'Tennis And More Sporting', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066', '', 'Bengaluru', '2018-11-05', '2018-11-09', '2018-10-15', '2018-10-29', '', '', NULL, 'O', 'Tennis And More Sporting ', 'Cubbon Park Bangalore 56001', '', 'Karnataka State Lawn Tennis Association', '', '', '', '', '2018-10-21', '2018-10-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/5th Nov,2018 TAMS17352.doc', '2018-10-21 04:26:40', 'A', NULL),
(266, 'Talent Series7 U-12 Boys & Girls', NULL, NULL, 'TOPSPIN TENNIS', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68', '', 'Bengaluru', '2018-11-12', '2018-11-16', '2018-10-22', '2018-10-05', '', '', NULL, 'O', 'Top Spin Tennis Academy', '', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-10-21', '2018-10-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/12th Nov,2018 Topspin70584.doc', '2018-10-21 04:32:57', 'A', NULL),
(267, 'Talent Series7 U-14 Boys & Girls', NULL, NULL, 'TOPSPIN TENNIS', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68', '', 'Bengaluru', '2018-11-12', '2018-11-16', '2018-10-22', '2018-11-05', '', '', NULL, 'O', 'TOPSPIN TENNIS', 'CUBBON PARK,  BANGALORE', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-10-21', '2018-10-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/12th Nov,2018 Topspin76533.doc', '2018-10-21 04:36:31', 'A', NULL),
(268, 'Championship Series3 U-16 Boys & Girls', NULL, NULL, 'NammaSportz Tennis Academy', 'SMVN School campus, Judicial Layout, Off Kanakapura Road, Thalaghattapura, Bangalore 560 062', '', 'Bengaluru', '2018-11-19', '2018-11-21', '2018-11-16', '2018-11-16', '', '', NULL, 'O', 'NammaSportz Tennis Academy', 'KSLTA ,CUBBON PARK ,BANGALORE', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-10-21', '2018-10-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/19th Nov,2018 Namma Sportz Academy58048.doc', '2018-10-21 04:42:49', 'A', NULL),
(269, 'Talent Series3 U-12 Boys & Girls', NULL, NULL, 'NVICTUS SPORTS ARENA', '#118, CHIKKABANAVARA, HE\r\nSARAGHATTA MAIN \r\nROAD, BENGALURU - 560090', '', 'Bengaluru', '2018-11-19', '2018-11-21', '2018-11-16', '2018-11-16', '', '', NULL, 'O', 'INVICTUS SPORTS ARENA', 'Cubbon Park, Bengaluru-560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-10-21', '2018-10-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/19th Nov.2018 Invictus6013.pdf', '2018-10-21 04:48:26', 'A', NULL),
(270, 'Talent Series3 U-10 Boys & Girls', NULL, NULL, 'GLOW TENNIS ACADEMY', 'NO. 22, SRI SATYA SAI NILAYA, CHINNAPPANAHALLI,  NEAR A.E.C.S LAYOUT, OPPOSITE TO EURO KIDS SCHOOL, BENGALURU ', '', 'Bengaluru', '2018-11-26', '2018-11-28', '2018-11-24', '2018-11-24', '', '', NULL, 'O', 'GLOW TENNIS ACADEMY', 'CUBBON PARK, BENGALURU - 560001 ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-10-21', '2018-10-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/26th Nov.2018 GTA87504.pdf', '2018-10-21 04:52:56', 'A', NULL),
(271, 'Talent Series3 U-10 Boys & Girls', NULL, NULL, 'Tennis And More Sporting ', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066', '', 'Bengaluru', '2018-10-29', '2018-10-31', '2018-10-27', '2018-10-27', '', '', NULL, 'O', 'Tennis And More Sporting (Tams)', 'Cubbon Park Bangalore 56001', '', 'Karnataka State Lawn Tennis Association', '', '', '', '', '2018-10-21', '2018-10-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/29th Oct,2018 TAMS36761.doc', '2018-10-21 04:58:23', 'A', NULL),
(272, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'Namma Sportz Tennis Academy', 'SMVN School campus, Judicial Layout, Off Kanakapura Road, Thalaghattapura, Bangalore 560 062\r\n\r\n', '', 'Bengaluru', '2018-12-03', '2018-12-07', '2018-11-12', '2018-11-26', '', '', NULL, 'O', 'Namma Sportz Tennis Academy', 'KSLTA ,CUBBON PARK ,BANGALORE', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-11-29', '2018-11-28 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/3rd Dec 2018, Namma sports Academy99620.doc', '2018-11-29 02:32:03', 'A', NULL),
(273, 'Talent Series3 U-10 Boys & Girls', NULL, NULL, 'BEL OFFICERS CLUB', '1, Vidyaranyapura Rd\r\nJala halli, Bengaluru-560013', '', 'Bengaluru', '2018-12-03', '2018-12-04', '2018-12-01', '2018-12-01', '', '', NULL, 'O', 'BEL OFFICERS CLUB', 'CUBBON PARK, BANGALORE \r\n–\r\n560 001', '', 'KARNATAKA  STATE  LAWN  TENNIS  ASSOCIATION', '', '', '', '', '2018-11-29', '2018-11-28 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/3rd Dec 2018, BEL Officer Club10826.pdf', '2018-11-29 02:32:22', 'A', NULL),
(274, 'Talent Series3 U-14 Boys & Girls', NULL, NULL, 'MAHILA SEVA SAMAJA', '# 80 K R Road Shankar puram Bangalore -560004', '', 'Bengaluru', '2018-12-10', '2018-12-12', '2018-12-07', '2018-12-07', '', '', NULL, 'O', 'MAHILA SEVA SAMAJA', 'Cubbon Park Bangalore 56001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-11-29', '2018-11-28 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/10th Dec 2018 MSS36002.doc', '2018-11-29 02:37:05', 'A', NULL),
(275, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'Namma Sportz Tennis Academy', 'SMVN School campus, Judicial Layout, Off Kanakapura Road, Thalaghattapura, Bangalore 560 062', '', 'Bengaluru', '2018-12-17', '2018-12-21', '2018-11-26', '2018-12-10', '', '', NULL, 'O', 'Namma Sportz Tennis Academy', 'KSLTA ,CUBBON PARK ,BANGALORE', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-11-29', '2018-11-28 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/17th Dec 2018 Namma Sports Academy33269.doc', '2018-11-29 02:42:16', 'A', NULL),
(276, 'Talent Series3 U-12 Boys & Girls', NULL, NULL, 'MAHILA SEVA SAMAJA', '# 80 K R Road Shankar puram Bangalore -560004', '', 'Bengaluru', '2018-12-17', '2018-12-18', '2018-12-14', '2018-12-14', '', '', NULL, 'O', 'MAHILA SEVA SAMAJA', 'Cubbon Park Bangalore 56001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-11-29', '2018-11-28 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/17th Dec 2018 MSS88546.doc', '2018-11-29 02:47:28', 'A', NULL),
(277, 'Men\'s 50K', NULL, NULL, 'TOPSPIN TENNIS.', 'SURVEY  #  14/3,  NARAYANANAGAR  1\r\nST\r\nBLOCK,  NEAR \r\nAPCO   CEMENTING   BLOCK   COORPORATE   OFFICE, \r\nDODDAKALSANDRA, \r\nUTTARAHALLI, OFF \r\nKANAKAPURA ROAD, BANGALORE 68', '', 'Bengaluru', '2018-12-17', '2018-12-21', '2018-11-26', '2018-12-10', '', '50,000', NULL, 'O', 'TOPSPIN TENNIS', 'CUBBON PARK\r\n, \r\nBANGALORE', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-11-29', '2018-11-28 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/17th Dec 2018 Tospin Tennis Academy25394.pdf', '2018-11-29 02:53:38', 'A', NULL),
(278, 'Women\'s 50K', NULL, NULL, 'TOP SPIN TENNIS', 'SURVEY  #  14/3,  NARAYANANAGAR  1\r\nST\r\nBLOCK,  NEAR \r\nAPCO   CEMENTING   BLOCK   COORPORATE   OFFICE, \r\nDODDAKALSANDRA, \r\nUTTARAHALLI, OFF \r\nKANAKAPURA ROAD, BANGALORE 68', '', 'Bengaluru', '2018-12-17', '2018-12-21', '2018-11-26', '2018-12-10', '', '50000', NULL, 'O', 'TOPSPIN TENNIS', 'CUBBON PARK ,BANGALORE', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-11-29', '2018-11-28 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '14', 'uploads/factsheets/17th Dec 2018 Tospin Tennis Academy74694.pdf', '2018-11-29 02:56:35', 'A', NULL),
(279, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'TOPSPIN TENNIS', 'SURVEY  #  14/3,  NARAYANANAGAR  1\r\nST BLOCK,  NEAR \r\nAPCO   CEMENTING   BLOCK   COORPORATE   OFFICE, \r\nDODDAKALSANDRA, \r\nUTTARAHALLI,OFF \r\nKANAKAPURA ROAD, BANGALORE 68', '', 'Bengaluru', '2018-12-24', '2018-12-28', '2018-12-03', '2018-11-17', '', '', NULL, 'O', 'TOPSPIN TENNIS', 'CUBBON PARK ,BANGALORE', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-11-29', '2018-11-28 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/24th Dec 2018 Topspin Tennis Academy48772.pdf', '2018-11-29 03:03:09', 'A', NULL),
(280, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'TOPSPIN TENNIS', 'SURVEY  #  14/3,  NARAYANANAGAR  1\r\nST\r\nBLOCK,  NEAR \r\nAPCO   CEMENTING   BLOCK   COORPORATE   OFFICE, \r\nDODDAKALSANDRA, \r\nUTTARAHALLI, OFF \r\nKANAKAPURA ROAD, BANGALORE 68', '', 'Bengaluru', '2018-12-24', '2018-12-28', '2018-12-03', '2018-12-17', '', '', NULL, 'O', 'TOPSPIN TENNIS', 'CUBBON PARK,BANGALORE', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-11-29', '2018-11-28 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/24th Dec 2018 Topspin Tennis Academy99058.pdf', '2018-11-29 03:05:40', 'A', NULL),
(281, 'Talent Series7 U-14 Boys & Girls', NULL, NULL, 'Transform Tennis Academy ', '16th Cross, 13th Main Road, Virat Nagar, Bommanahalli, Bangalore-560068 ', '', 'Bengaluru', '2018-11-26', '2018-11-30', '2018-11-05', '2018-11-19', '', '', NULL, 'O', 'Transform Tennis Academy ', 'CUBBON PARK,  BANGALORE ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2018-12-03', '2018-12-02 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/26th Nov.2018 Transform Tennis Academy73715.pdf', '2018-12-03 05:06:37', 'A', NULL),
(282, 'Talent Series7 U-16 Boys & Girls', NULL, NULL, 'Transform Tennis Academy', '16th Cross,  13th\r\nMain  Road,  Virat  Nagar, \r\nBommanahalli, Bangalore-560068', '', 'Bengaluru', '2018-11-26', '2018-11-30', '2018-11-05', '2018-11-19', '', '', NULL, 'O', 'Transform Tennis Academy', 'CUBBON PARK, BANGALORE', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIAT ION', '', '', '', '', '2018-12-03', '2018-12-02 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/26th Nov.2018 Transform Tennis Academy25400.pdf', '2018-12-03 05:33:08', 'A', NULL),
(283, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'MYSORE TENNIS CLUB(MTC)', 'KRISHNARAJ BOULEVARD CHAMRAJAPURA, MYSURU – 57 0 005', '', 'Mysore', '2019-01-07', '2019-01-11', '2018-12-17', '2018-12-31', '', '', NULL, 'O', 'MYSORE TENNIS CLUB(MTC)', 'CUBBON PARK, BENGALURU – 560 001    ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-12-24', '2018-12-23 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/07th Jan 2019 MTC38104.doc', '2018-12-24 04:40:15', 'A', NULL),
(284, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'MYSORE TENNIS CLUB(MTC)', 'KRISHNARAJ BOULEVARD CHAMRAJAPURA, MYSURU – 57 0 005', '', 'Mysore', '2019-01-07', '2019-01-11', '2018-12-17', '2018-12-31', '', '', NULL, 'O', 'MYSORE TENNIS CLUB(MTC)', 'CUBBON PARK, BENGALURU – 560 001    ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-12-24', '2018-12-23 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/07th Jan 2019 MTC29467.doc', '2018-12-24 04:42:45', 'A', NULL),
(285, 'Talent Series 3 Boys & Girls U-12', NULL, NULL, 'Sree Datta Sports Academy', '#505, ITI Layout, Hosapalya Main Road,\r\nMangammapalya,HSR layout, Bangalore\r\n68\r\n', '', 'Bengaluru', '2019-01-07', '2019-01-09', '2019-01-04', '2019-01-04', '', '', NULL, 'O', 'Sree Datta Sports Academy', 'CUBBON PARK, BENGALURU – 560 001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-12-24', '2018-12-23 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/07th Jan 2019 Sree Datta Sports Academy70929.pdf', '2018-12-24 04:45:53', 'A', NULL),
(286, 'Asia Tennis Federation U-14 Boys & Girls', NULL, NULL, 'Jain International Residential School ', 'JAIN GLOBAL CAMPUS Jain International Residential School – Bangalore Jakkasandra Post , Kanakapu\r\ntbc Ramanagara District, Bangalore, Karnataka', '', 'Bengaluru', '2019-01-14', '2019-01-19', '2018-10-14', '2018-12-18', '', '', NULL, 'O', 'Jain International Residential School', 'Cubbon Park, Bengaluuru-560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-12-24', '2018-12-23 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/14th Jan 2019 JIRS19029.doc', '2018-12-24 04:52:53', 'A', NULL),
(287, 'Men\'s 1 Lakh', NULL, NULL, 'Tennis Temple Academy', '#174,DASASHRAMA, NEAR PANDURANGA TEMPLE,\r\n5TH BLOCK RAJAJINAGAR, , BENGALURU: 560010', '', 'Bengaluru', '2019-01-14', '2019-01-18', '2018-12-24', '2019-01-07', '', '1,00,000', NULL, 'O', 'KTPPA – AITA Men’s 1 lakh', 'CUBBON PARK, BANGALORE\r\n', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-12-24', '2018-12-23 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/14th Jan 2019 KTPPA89958.pdf', '2018-12-24 05:00:51', 'A', NULL),
(288, 'Women’s 1Lakh', NULL, NULL, 'Tennis Temple Academy', '#174,DASASHRAMA, NEAR PANDURANGA TEMPLE,\r\n5TH BLOCK RAJAJINAGAR, , BENGALURU: 560010\r\n', '', 'Bengaluru', '2019-01-14', '2019-01-18', '2018-12-24', '2019-01-07', '', '1,00,000', NULL, 'O', 'KTPPA – Women\'s 1 Lakh', 'CUBBON PARK, BANGALORE\r\n', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-12-24', '2018-12-23 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', 'uploads/factsheets/14th Jan 2019 KTPPA34237.pdf', '2018-12-24 05:05:02', 'A', NULL),
(289, 'Champion Series 7 Boys & Girls U-16', NULL, NULL, 'TOPSPIN TENNIS.', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR\r\nAPCO CEMENTING BLOCK COORPORATE OFFICE,\r\nDODDAKALSANDRA, UTTARAHALLI, OFF\r\nKANAKAPURA ROAD, BANGALORE 68', '', 'Bengaluru', '2019-01-21', '2019-01-25', '2019-01-31', '2019-01-14', '', '', NULL, 'O', 'TOPSPIN TENNIS.', 'CUBBON PARK, BANGALORE\r\n', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-12-24', '2018-12-23 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/21st Jan 2019 Topspin Tennis4548.pdf', '2018-12-24 05:09:48', 'A', NULL),
(290, 'Champion Series 7 Boys & Girls U-18', NULL, NULL, 'TOPSPIN TENNIS.', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR\r\nAPCO CEMENTING BLOCK COORPORATE OFFICE,\r\nDODDAKALSANDRA, UTTARAHALLI, OFF\r\nKANAKAPURA ROAD, BANGALORE 68', '', 'Bengaluru', '2019-01-21', '2019-01-25', '2019-01-31', '2019-02-07', '', '', NULL, 'O', 'TOPSPIN TENNIS.', 'CUBBON PARK, BANGALORE', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-12-24', '2018-12-23 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '12', 'uploads/factsheets/21st Jan 2019 Topspin Tennis26529.pdf', '2018-12-24 05:12:12', 'A', NULL),
(291, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'FORTUNE TENNIS ACADEMY ', 'FORTUNE TENNIS ACADEMY, 469, Fort Kengeri, Kengeri,\r\nBengaluru, Karnataka 560060\r\n', '', 'Bengaluru', '2019-01-28', '2019-02-01', '2019-01-07', '2019-01-21', '', '', NULL, 'O', 'FORTUNE TENNIS ACADEMY ', 'CUBBON PARK, BENGALURU – 560 001 ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-12-24', '2018-12-23 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/28th Jan Fortune Tennis Academy15029.pdf', '2018-12-24 05:17:27', 'A', NULL),
(292, 'AITA Children U-10', NULL, NULL, 'BEL OFFICERS CLUB', '1, Vidyaranyapura Rd Jala halli,\r\nBengaluru, Karnataka 560013', '', 'Bengaluru', '2019-01-14', '2019-01-16', '2019-01-15', '2019-01-11', '', '', NULL, 'O', 'BEL OFFICERS CLUB', 'CUBBON PARK, BANGALORE – 560 001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2018-12-24', '2018-12-23 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/14th Jan 2019 BEL Officers Club13915.pdf', '2018-12-24 05:28:55', 'A', NULL),
(293, 'AITA Children U-10', NULL, NULL, 'Sree Datta Sports Academy', '#505, ITI Layout, Hosapalya Main Road,\r\nMangammapalya,HSR layout, Bangalore 68\r\n', '', 'Bengaluru', '2018-12-24', '2018-12-25', '2018-12-22', '2018-12-22', '', '', NULL, 'O', 'SREE DATTA SPORTS ACADEM', 'CUBBON PARK, BENGALURU – 560001\r\n', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-01-07', '2019-01-06 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/24th Dec 2018 Sree Datta Sports Academy25986.pdf', '2019-01-07 04:14:04', 'A', NULL),
(294, 'Champion Series 3 Boys & Girls U-18', NULL, NULL, 'ANT ACADEMY', 'ANT ACADEMY, Bengaluru\r\n20th ‘A’ Cross, MCECHS Layout Phase-1,\r\nVenkateshpura Layout, Sampigehalli,\r\nBengaluru-560077.\r\n', '', 'Bengaluru', '2018-12-31', '2019-01-01', '2018-12-28', '2018-12-28', '', '', NULL, 'O', 'ANT ACADEMY', 'CUBBON PARK, BANGALORE – 560 001\r\n', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-01-07', '2019-01-06 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '12', 'uploads/factsheets/31st Dec 2018 ANT Academy54750.pdf', '2019-01-07 04:29:18', 'A', NULL),
(295, 'Men\'s 1 Lakh', NULL, NULL, 'ANT TENNIS ACADEMY', '20th ‘A’  Cross,  MCECHS  Layout  Phase\r\n-1, Venkateshpura Layout, \r\nSampigehalli, Bengaluru-560077', '', 'Bengaluru', '2019-01-28', '2019-02-01', '2019-01-07', '2019-01-14', '', '100000', NULL, 'O', 'ANT TENNIS ACADEMY', '', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-01-25', '2019-01-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', NULL, '0000-00-00 00:00:00', 'A', NULL),
(296, 'Women\'s 1 Lakh', NULL, NULL, 'ANT TENNIS ACADEMY', '20th ‘A’  Cross,  MCECHS  Layout  Phase\r\n-1,Venkateshpura Layout,Sampigehalli, \r\nBengaluru -560077.', '', 'Bengaluru', '2019-01-28', '2019-02-01', '2019-01-07', '2019-01-14', '', '100000', NULL, 'O', 'ANT TENNIS ACADEMY', 'CUBBON PARK,  BANGALORE \r\n– 560 001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-01-25', '2019-01-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '14', 'uploads/factsheets/28th Jan ANT Academy60892.pdf', '2019-01-25 03:33:56', 'A', NULL),
(297, 'AITA Children U-10', NULL, NULL, 'TENNIS 360 ACADEMY', '2nd Cross \r\nLakeshore Garden, APC Layout\r\n, Doddabommasandra, \r\nBengaluru-560097', '', 'Bengaluru', '2019-01-28', '2019-01-29', '2019-01-26', '2019-01-26', '', '', NULL, 'O', 'TENNIS 360 ACADEMY', 'CUBBON PARK, BENGALURU', '', 'KARNATAKA STATE LAWN TENNIS  ASSOCIATION', '', '', '', '', '2019-01-25', '2019-01-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/28th Jan 2019 Tennis 360 Academy8406.pdf', '2019-01-25 04:10:17', 'A', NULL),
(298, 'Talent Series 3 Boys & Girls U-14', NULL, NULL, 'Sree Datta Sports  Academy', '#505, ITI Layout, Hosapalya Main Road,\r\nMangammapalya,HSR layout, Bangalore 68', '', 'Bengaluru', '2019-02-04', '2019-02-04', '2019-02-05', '2019-02-01', '', '', NULL, 'O', 'Sree Datta Sports  Academy', 'CUBBON PARK, BENGALURU –560 001\r\n', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-01-25', '2019-01-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/4th Feb 2019 Sree Datta Sports Academy46519.pdf', '2019-01-25 04:09:44', 'A', NULL),
(300, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'TOP SPIN TENNIS', 'SURVEY  #  14/3,  NARAYANANAGAR  1\r\nST BLOCK,  NEAR APCO   CEMENTING   BLOCK   COORPORATE   OFFICE, DODDAKALSANDRA, \r\nUTTARAHALLI,OFF KANAKAPURA ROAD, \r\nBANGALORE 68\r\n', '', 'Bengaluru', '2019-02-11', '2019-02-15', '2019-01-21', '2019-02-11', '', '', NULL, 'O', 'TOP SPIN TENNIS', 'CUBBON PARK , BANGALORE', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-01-25', '2019-01-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/11th Feb 2019 Topspin Tennis Academy65871.pdf', '2019-01-25 04:43:49', 'A', NULL),
(301, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'TOP SPIN TENNIS', 'SURVEY  #  14/3,  NARAYANANAGAR  1\r\nST\r\nBLOCK,  NEAR \r\nAPCO   CEMENTING   BLOCK   COORPORATE   OFFICE, \r\nDODDAKALSANDRA, \r\nUTTARAHALLI,OFF KANAKAPURA ROAD, \r\nBANGALORE 68', '', 'Bengaluru', '2019-02-11', '2019-02-15', '2019-01-21', '2019-02-04', '', '', NULL, 'O', 'TOP SPIN TENNIS', 'CUBBON PARK ,BANGALORE', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-01-25', '2019-01-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/11th Feb 2019 Topspin Tennis Academy8800.pdf', '2019-01-25 04:48:22', 'A', NULL),
(302, 'Men\'s 1 Lac', NULL, NULL, 'MYSORE TENNIS CLUB', 'KRISHNARAJ \r\nBOULEVARD \r\nCHAMARAJAPURA, MYSURU – 57 0 005', '', 'Mysore', '2019-02-11', '2019-02-15', '2019-01-21', '2019-01-04', '', '100000', NULL, 'O', 'MYSORE TENNIS CLUB', 'CUBBON PARK,  BANGALORE –560 001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-01-25', '2019-01-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/11th Feb 2019 MTC3120.pdf', '2019-01-25 05:26:45', 'A', NULL);
INSERT INTO `ksl_tournaments_tbl` (`tourn_id`, `tourn_title`, `tourn_category`, `tourn_sub_category`, `tourn_venue`, `tourn_venue_address1`, `tourn_venue_address2`, `tourn_venue_city`, `tourn_from_date`, `tourn_to_date`, `tourn_last_submission_date`, `tourn_withdrl_date`, `tourn_description`, `tourn_price_money`, `tourn_tournament_image`, `tourn_register_status`, `tourn_asso_title`, `tourn_asso_address1`, `tourn_asso_address2`, `tourn_asso_head`, `tourn_refree1`, `tourn_refree2`, `tourn_refree3`, `tourn_link`, `tourn_created_date`, `tourn_lastmodified_date`, `tourn_status`, `sponser_name_of_organisation`, `sponser_address1`, `sponser_address2`, `sponser_contact_person_name`, `sponser_phone`, `sponser_email`, `tour_event_category`, `tour_event_subcategory`, `tourn_factsheet`, `tourn_factsheet_uploaded_at`, `tourn_facesheet_status`, `tourn_gallery_id`) VALUES
(303, 'Champion Series 3 Boys & Girls U-18', NULL, NULL, 'SAT SPORTS PV T LTD', 'SAT Sports Pvt Ltd,\r\n@ D Cube Sports Club,\r\nSy. No -\r\n16, 17 & 18,\r\nSathanur Village\r\n, \r\nBangalore, 562149', '', 'Bengaluru', '2019-02-11', '2019-02-12', '2019-02-08', '2019-02-08', '', '', NULL, 'O', 'SAT SPORTS OPEN', 'CUBBON PARK, BANGALORE \r\n–560 001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-01-25', '2019-01-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '12', 'uploads/factsheets/11th Feb 219 SAT Sports 59493.pdf', '2019-01-25 05:44:01', 'A', NULL),
(304, 'Talent Series 3 Boys & Girls U-14', NULL, NULL, 'SREE DATTA SPORTS ACADEMY', '#505, ITI Layout, Hosapalya Main Road,\r\nMangammapalya,HSR layout, Bangalore 68', '', 'Bengaluru', '2019-02-18', '2019-02-19', '2019-02-15', '2019-02-15', '', '', NULL, 'O', 'Sree Datta Sports  Academy', 'CUBBON PARK, BENGALURU\r\n–560 001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-01-25', '2019-01-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/18th Feb 2019 Sree Datta Sports Academy3453.pdf', '2019-01-25 05:49:57', 'A', NULL),
(305, 'Championship Series 3 Boys & Girls U-18', NULL, NULL, 'FORTUNE TENNIS ACADEMY', 'FORTUNE TENNIS ACADEMY, \r\n469, Fort Kengeri, Kengeri, \r\nBengaluru, Karnataka 560060', '', 'Bengaluru', '2019-02-18', '2019-02-19', '2019-02-15', '2019-02-15', '', '', NULL, 'O', 'FORTUNE TENNIS ACADEMY', 'CUBBON PARK, BENGALURU \r\n–560 001    ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-01-25', '2019-01-29 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '12', 'uploads/factsheets/18th Feb 2019 Fortune Sports 81743.pdf', '2019-01-25 06:03:13', 'A', NULL),
(306, 'AITA Children U-10', NULL, NULL, 'SAT SPORTS OPEN', 'SAT Sports Pvt Ltd\r\n@ D Cube Sports Club,\r\nSy. No-16,\r\n17 & 18,Sathanur village, \r\nBangalore-562149', '', 'Bengaluru', '2019-02-18', '2019-02-19', '2019-02-16', '2019-02-16', '', '', NULL, 'O', 'SAT SPORTS PVT LTD', 'CUBBON PARK, BANGALORE \r\n–560 001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-01-25', '2019-01-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', NULL, '0000-00-00 00:00:00', 'A', NULL),
(307, 'AITA Children U-10', NULL, NULL, 'Tennis And More Sporting (Tams)', '35/5,    Sidhapura,    Marathalli\r\n-Whitefield Main  Road,  Next  to  D  Mart  Super  Market, \r\nBangalore 560066', '', 'Bengaluru', '2019-02-25', '2019-02-26', '2019-02-23', '2019-02-23', '', '', NULL, 'O', 'Tennis And More Sporting (Tams)', 'Cubbon Park Bangalore 56001', '', 'Karnataka State Lawn Tennis Association', '', '', '', '', '2019-01-25', '2019-01-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/25th Feb 2019 TAMS54840.pdf', '2019-01-25 06:13:28', 'A', NULL),
(308, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'Tennis And More Sporting (Tams)', '35/5,    Sidhapura,    Marathalli\r\n-Whitefield Main  Road,  Next  to  D  Mart  Super  Market, \r\nBangalore 560066', '', 'Bengaluru', '2019-02-25', '2019-03-01', '2019-02-04', '2019-01-18', '', '', NULL, 'O', 'Tennis And More Sporting (Tams)', 'Cubbon Park Bangalore 560001', '', 'Karnataka State Lawn Tennis Association', '', '', '', '', '2019-01-25', '2019-01-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/25th Feb 2019 TAMS U-1245488.pdf', '2019-01-25 06:16:18', 'A', NULL),
(309, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'FORTUNE TENNIS ACADEMY ', 'FORTUNE TENNIS ACADEMY, \r\n469, Fort Kengeri, \r\nKengeri, Bengaluru, Karnataka 560060', '', 'Bengaluru', '2019-02-25', '2019-03-01', '2019-02-04', '2019-02-18', '', '', NULL, 'O', 'FORTUNE TENNIS ACADEMY ', 'CUBBON PARK, BENGALURU \r\n–560001    ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-01-25', '2019-01-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/25th Feb 2019 Fortune45799.pdf', '2019-01-25 06:20:37', 'A', NULL),
(310, 'Men\'s 50 K', NULL, NULL, 'DISTRICT TENNIS ASSOCIATION', 'GOVT HIGH SCHOOL FIELD \r\nGROUND\r\nDAVANGERE-577002', '', 'Davangere', '2019-02-25', '2019-02-25', '2019-02-04', '2019-02-18', '', '50000', NULL, 'O', 'KTPPA', 'CUBBON PARK\r\n, BANGALORE', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-01-25', '2019-01-24 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/25th Feb 2019 KTPPA57844.pdf', '2019-01-25 06:25:25', 'A', NULL),
(311, 'AITA Children U-10', NULL, NULL, 'SAT SPORTS PVT LTD ', 'SAT Sports Pvt Ltd @ D Cube Sports Club, Sy. No-16, 17 & 18, Sathanur village,  Bangalore- 562149 ', '', 'Bengaluru', '2019-02-04', '2019-02-05', '2019-02-02', '2019-02-02', '', '', NULL, 'O', 'SAT SPORTS PVT LTD ', 'CUBBON PARK, BANGALORE – 560 001 ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-01-30', '2019-01-29 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/4th Feb 2019 SAT Sports53316.pdf', '2019-01-30 05:47:09', 'A', NULL),
(312, 'Talent Series 3 Boys & Girls U-12', NULL, NULL, 'POLICE GYMKHANA', 'PARVATI NAGAR, \r\n2 ND CROSS, BALLARI \r\n–583101,KARNATAKA', '', 'Ballari', '2019-01-21', '2019-01-22', '2019-01-18', '2019-01-18', '', '', NULL, 'O', 'POLICE GYMKHANA', 'CUBBON PARK, BANGALORE \r\n–560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-02-04', '2019-02-03 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/21st Jan 2019 Police Gymkhana40217.pdf', '2019-02-04 07:03:00', 'A', NULL),
(313, 'Championship Series 3 Boys & Girls U-14', NULL, NULL, 'TOP SPIN TENNIS ACADEMY', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68', '', 'Bengaluru', '2019-03-04', '2019-03-05', '2019-03-01', '2019-03-01', '', '', NULL, 'O', 'TOP SPIN TENNIS ACADEMY', 'CUBBON PARK,  BANGALORE', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-02-15', '2019-02-14 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/4th Mar.2019 Topspin Tennis89017.doc', '2019-02-15 02:25:45', 'A', NULL),
(314, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'Tennis And More Sporting (Tams)', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066', '', 'Bengaluru', '2019-03-18', '2019-03-22', '2019-02-25', '2020-03-11', '', 'Bengaluru', NULL, 'O', 'Tennis And More Sporting (Tams)', 'Cubbon Park Bangalore 56001', '', 'Karnataka State Lawn Tennis Association', '', '', '', '', '2019-02-15', '2019-02-14 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/18th Mar 2019 TAMS73149.doc', '2019-02-15 02:36:01', 'A', NULL),
(315, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'Tennis And More Sporting (Tams)', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066', '', 'Bengaluru', '2019-03-18', '2019-03-22', '2019-02-25', '2019-03-11', '', '', NULL, 'O', 'Tennis And More Sporting (Tams)', 'Cubbon Park Bangalore 56001', '', 'Karnataka State Lawn Tennis Association', '', '', '', '', '2019-02-15', '2019-02-14 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/18th Mar 2019 TAMS4750.doc', '2019-02-15 02:42:29', 'A', NULL),
(316, 'Men\'s 1 Lac', NULL, NULL, 'SAT Sports Pvt Ltd ', 'SAT Sports Pvt Ltd @ D Cube Sports Club, Sy. No-16, 17 & 18, Sathanur village,  Bangalore- 562149 ', '', 'Bengaluru', '2019-03-25', '2019-03-29', '2019-03-04', '2019-03-18', '', '1,00,000', NULL, 'O', 'SAT Sports Pvt Ltd ', 'CUBBON PARK, BANGALORE – 560 001 ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-02-15', '2019-02-14 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/25th Mar 2019 SAT Sports 47403.pdf', '2019-02-15 02:47:45', 'A', NULL),
(317, 'women\'s 1 Lac', NULL, NULL, 'SAT Sports Pvt Ltd ', 'SAT Sports Pvt Ltd @ D Cube Sports Club, Sy. No-16, 17 & 18, Sathanur village,  Bangalore- 562149 ', '', 'Bengaluru', '2019-03-25', '2019-03-29', '2019-03-04', '2019-03-18', '', '1,00,000', NULL, 'O', 'SAT Sports Pvt Ltd ', 'CUBBON PARK, BANGALORE – 560 001 ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-02-15', '2019-02-14 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', 'uploads/factsheets/25th Mar 2019 SAT Sports 13600.pdf', '2019-02-15 02:49:52', 'A', NULL),
(318, 'Men\'s 1 Lac', NULL, NULL, 'CENTURY CLUB', 'Venue –No  1  , Seshadri  Road,Bangalore –560001[NEAR  K.R. CIRCLE, Inside Cubbon Park, Bengaluru-560001', '', 'Bengaluru', '2019-04-29', '2019-05-03', '2019-04-08', '2019-04-22', '', '1,00,000', NULL, 'O', 'Century Club', 'KSLTA Tennis Stadium,\r\nCubbon Park\r\nBengaluru-560001', '', 'Karnataka State Lawn Tennis Association', '', '', '', '', '2019-02-25', '2019-03-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/29th Apr,2019 Century Club8399.pdf', '2019-03-26 05:00:53', 'A', NULL),
(320, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'ELITE TENNIS ACADEMY', '# 51/6, HalanayyakanahalliOpp to Rainbow retreat LayoutNear to Nimritha Green CountyBangalore –560035https://goo.gl/maps/nSjfaMsy22yE mail: elitetennistournament@gmail.com', '', 'Bengaluru', '2019-04-08', '2019-04-12', '2019-03-18', '2019-04-01', '', '', NULL, 'O', 'ELITE TENNIS ACADEMY', 'CUBBON PARK, BENGALURU–560 001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-03-23', '2019-03-22 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/8th Apr 2019 Elite Tennis Academy 37915.pdf', '2019-03-23 06:34:29', 'A', NULL),
(321, 'AITA Children U-10', NULL, NULL, 'Tennis And More Sporting (Tams)', '35/5,Sidhapura,    Marathalli-Whitefield Main  Road,  Next  to  D  Mart  Super  Market, Bangalore 560066', '', 'Bengaluru', '2019-04-15', '2019-04-16', '2019-04-13', '2019-04-13', '', '', NULL, 'O', 'Tennis And More Sporting (Tams)', 'Cubbon Park Bangalore 560001', '', 'Karnataka State Lawn Tennis Association', '', '', '', '', '2019-03-23', '2019-03-22 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/15th Apr 2019 TAMS2331.pdf', '2019-03-23 06:38:43', 'A', NULL),
(322, 'BOWRING INSTITUTE AITA TENNIS TOURNAMENT', NULL, NULL, 'BOWRING INSTITUTE', 'NO.19, ST.MARKS ROAD,\r\nBENGALURU –560 001', '', 'Bengaluru', '2019-04-22', '2019-04-26', '2019-04-01', '2019-04-15', '', '1,00,000', NULL, 'O', 'BOWRING INSTITUTE ', 'CUBBON PARK , BENGALURU –560 001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-03-23', '2019-03-22 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/22nd Apr 2019 Bowring Institute32494.pdf', '2019-03-23 06:51:21', 'A', NULL),
(323, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'Tennis And More Sporting (Tams)', '35/5,  Sidhapura,  Marathalli-Whitefield  Main Road,   Next   to   D   Mart   Super   Market, \r\nBangalore 560066', '', 'Bengaluru', '2019-04-22', '2019-04-26', '2019-04-01', '2019-04-15', '', '', NULL, 'O', 'Tennis And More Sporting (Tams', 'Cubbon Park Bangalore 560001', '', 'Karnataka State Lawn Tennis Association', '', '', '', '', '2019-03-26', '2019-03-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', NULL, '0000-00-00 00:00:00', 'A', NULL),
(324, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'MYSURU TENNIS CLUB[MTC]', 'KRISHNARAJ BOULEVARD CHAMARAJAPURA, \r\nMYSURU –570005', '', 'Mysore', '2019-04-29', '2019-05-03', '2019-04-08', '2019-04-22', '', '', NULL, 'O', 'MYSURU TENNIS CLUB[MTC]', 'CUBBON PARK, BENGALURU –560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-03-26', '2019-03-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', NULL, '0000-00-00 00:00:00', 'A', NULL),
(325, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'TOPSPIN TENNIS', 'SURVEY  #  14/3,  NARAYANANAGAR  1STBLOCK,  NEAR APCO   CEMENTING   BLOCK   COORPORATE   OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68', '', 'Bengaluru', '2019-04-29', '2019-05-03', '2019-04-08', '2019-04-22', '', '', NULL, 'O', 'TOPSPIN TENNIS', 'CUBBON PARK, BANGALORE-560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-03-26', '2019-03-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', NULL, '0000-00-00 00:00:00', 'A', NULL),
(326, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'FORTUNE TENNIS ACADEMY', 'FORTUNE TENNIS ACADEMY, 469, Fort Kengeri, Kengeri, Bengaluru, Karnataka 560060 ', '', 'Bengaluru', '2019-05-06', '2019-05-10', '2019-04-15', '2019-04-30', '', '', NULL, 'O', 'FORTUNE TENNIS ACADEMY  ADDRESS ', '', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-04-30', '2019-04-29 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/6th May 2019 Fortune Tennis Academy54991.pdf', '2019-04-30 05:38:44', 'A', NULL),
(327, 'Champion Series 7 Boys & Girls U-14', NULL, NULL, 'TOPSPIN TENNIS', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68 ', '', 'Bengaluru', '2019-06-03', '2019-06-07', '2019-05-13', '2019-05-27', '', '', NULL, 'O', 'TOPSPIN TENNIS', 'CUBBON PARK,  BANGALORE ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-05-11', '2019-05-10 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/3rd Jun 2019 Topspin Tennis Academy54289.pdf', '2019-05-11 04:25:18', 'A', NULL),
(328, 'Champion Series 7 Boys & Girls U-16', NULL, NULL, 'TOPSPIN TENNIS', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68 ', '', 'Bangaluru', '2019-06-03', '2019-06-07', '2019-05-13', '2019-05-27', '', '', NULL, 'O', 'TOPSPIN TENNI', 'CUBBON PARK,  BANGALORE ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-05-11', '2019-05-10 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/3rd Jun 2019 Topspin Tennis Academy68616.pdf', '2019-05-11 04:27:21', 'A', NULL),
(329, 'AITA Children U-10', NULL, NULL, 'TENNIS ARENA', 'Venue -6th  Cross  Rd, dead-end,  Prakruthi  Nagar, Kogilu, Bengaluru, Karnataka 560064', '', 'Bengaluru', '2019-05-27', '2019-05-28', '2019-05-25', '2019-05-25', '', '', NULL, 'O', 'Tennis Arena', 'CUBBON PARK, BANGALORE –560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-05-21', '2019-05-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/27th May 2019 Tennis Arena57389.pdf', '2019-05-21 06:23:32', 'A', NULL),
(330, 'AITA Children U-10', NULL, NULL, 'TENNIS360 ACADEMY', '2ndCross Lakeshore Garden, APC Layout, Doddabommasandra, Bengaluru, Karnataka 560097', '', 'Bengaluru', '2019-06-03', '2019-06-04', '2019-06-01', '2019-06-01', '', '', NULL, 'O', 'TENNIS360 ACADEMY', 'CUBBON PARK, BENGALUR', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-05-21', '2019-05-27 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/3rd Jun 2019 Tennis 360Academy20957.pdf', '2019-05-28 07:38:18', 'A', NULL),
(331, 'Champion Series 7 Boys & Girls U-14', NULL, NULL, 'MYSORE TENNIS CLUB (MTC) ', 'KRISHNARAJ BOULEVARD CHAMRAJAPURA, \r\nMYSURU –570005', '', 'Mysuru', '2019-06-10', '2019-06-11', '2019-05-20', '2019-06-03', '', '', NULL, 'O', 'MYSORE TENNIS CLUB(MTC) ', 'CUBBON PARK, BENGALURU – 560 001     ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-05-21', '2019-05-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/10th Jun 2019,MTC53572.pdf', '2019-05-21 06:36:59', 'A', NULL),
(332, 'Champion Series 7 Boys & Girls U-16', NULL, NULL, 'MYSORE TENNIS CLUB(MTC', 'KRISHNARAJ BOULEVARD CHAMRAJAPURA, \r\nMYSURU –570005', '', 'Mysuru', '2019-06-10', '2019-06-11', '2019-05-20', '2019-06-03', '', '', NULL, 'O', 'MYSORE TENNIS CLUB(MTC)', 'CUBBON PARK, BENGALURU –560 001   ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-05-21', '2019-05-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/10th Jun 2019,MTC1672.pdf', '2019-05-21 06:40:06', 'A', NULL),
(333, 'AITA Children U-10', NULL, NULL, 'SAT SPORTS PVT LTD ', 'SAT Sports Pvt Ltd @ D Cube Sports Club, Sy. No - 16, 17 & 18, Sathanur village,  Bangalore- 562149 ', '', 'Bengaluru', '2019-06-17', '2019-06-18', '2019-06-15', '2019-06-15', '', '', NULL, 'O', 'SAT SPORTS PVT LTD ', 'CUBBON PARK, BANGALORE – 560 001 ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-05-28', '2019-05-27 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/17th Jun 2019 SAT Sports 82149.pdf', '2019-05-28 07:01:44', 'A', NULL),
(334, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'Tennis And More Sporting (Tams) ', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066 ', '', 'Bengaluru', '2019-06-24', '2019-06-28', '2019-06-03', '2019-06-17', '', '', NULL, 'O', 'Tennis And More Sporting (Tams) ', 'Cubbon Park Bangalore 56001 ', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2019-05-28', '2019-05-27 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/24th Jun 2019 TAMS34896.pdf', '2019-05-28 07:18:44', 'A', NULL),
(335, 'Talent Series 7 Boys & Girls U-14	', NULL, NULL, 'Tennis And More Sporting (Tams) ', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066 ', '', 'Bengaluru', '2019-06-24', '2019-06-28', '2019-06-03', '2019-06-17', '', '', NULL, 'O', 'Tennis And More Sporting (Tams) ', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066 ', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2019-05-28', '2019-05-27 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/24th Jun 2019 TAMS2633.pdf', '2019-05-28 07:22:51', 'A', NULL),
(336, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'TOPSPIN TENNIS', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68 ', '', 'Bengaluru', '2019-07-08', '2019-07-12', '2019-06-17', '2019-07-01', '', '', NULL, 'O', 'TOPSPIN TENNIS. ', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68 ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-05-28', '2019-05-27 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/8th Jul 2019 Topspin Tennis Academy65903.pdf', '2019-05-28 07:31:54', 'A', NULL),
(337, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'TOPSPIN TENNIS', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68 ', '', 'Bengaluru', '2019-07-08', '2019-07-12', '2019-06-17', '2019-07-01', '', '', NULL, 'O', 'TOPSPIN TENNIS', 'CUBBON PARK,  BANGALORE ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-05-28', '2019-05-27 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/8th Jul 2019 Topspin Tennis Academy40762.pdf', '2019-05-28 07:33:59', 'A', NULL),
(338, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'SAT SPORTS Pvt Ltd ', 'SAT Sports Pvt Ltd, @ D Cube Sports Club, Sy. No -16, 17 & 18, Sathanur Village,  Bangalore-562149 ', '', 'Bengaluru', '2019-07-01', '2019-07-05', '2019-06-10', '2019-06-24', '', '', NULL, 'O', 'SAT SPORTS Pvt Ltd   ', 'CUBBON PARK, BANGALORE – 560 001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-06-10', '2019-06-09 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/1st Jul 2019 SAT Sports 32393.pdf', '2019-06-10 07:47:00', 'A', NULL),
(339, 'Championship Series 7 Boys & Girls U-14', NULL, NULL, 'MYSORE TENNIS CLUB(MTC) ', 'VENUE – KRISHNARAJ BOULEVARD CHAMRAJAPURA, MYSURU – 57 0 005 ', '', 'Mysore', '2019-07-15', '2019-07-19', '2019-06-24', '2019-07-08', '', '', NULL, 'O', 'MYSORE TENNIS CLUB(MTC) ', 'CUBBON PARK, BENGALURU – 560 001   ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-06-10', '2019-06-09 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/15th Jul 2019 MTC96203.pdf', '2019-06-10 07:52:20', 'A', NULL),
(340, 'Championship Series 7 Boys & Girls U-16', NULL, NULL, 'MYSORE TENNIS CLUB(MTC)', 'VENUE – KRISHNARAJ BOULEVARD CHAMRAJAPURA, MYSURU – 57 0 005 ', '', 'Mysore', '2019-07-15', '2019-07-19', '2019-06-24', '2019-07-08', '', '', NULL, 'O', 'MYSORE TENNIS CLUB(MTC', 'CUBBON PARK, BENGALURU – 560 001 ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-06-10', '2019-06-09 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/15th Jul 2019 MTC10800.pdf', '2019-06-10 07:56:09', 'A', NULL),
(342, 'AITA Children U-10', NULL, NULL, 'SAT SPORTS PVT LTD ', 'SAT Sports Pvt Ltd @ D Cube Sports Club, Sy. No-16, 17 & 18, Sathanur village,  Bangalore- 562149 ', '', 'Bengaluru', '2019-07-22', '2019-07-23', '2019-07-20', '2019-07-20', '', '', NULL, 'O', 'SAT SPORTS PVT LTD ', 'CUBBON PARK, BANGALORE – 560 001 ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-06-21', '2019-06-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/22nd Jul 2019 SAT23156.pdf', '2019-06-21 09:00:05', 'A', NULL),
(343, 'Champion Series 7 Boys & Girls U-12', NULL, NULL, 'Tennis And More Sporting (Tams) ', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066 ', '', 'Bengaluru', '2019-07-29', '2019-08-02', '2019-07-08', '2019-07-22', '', '', NULL, 'O', 'Tennis And More Sporting (Tams) ', 'Cubbon Park Bangalore 56001 ', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2019-06-21', '2019-06-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/29th Jul 2019 TAMS14093.pdf', '2019-06-21 09:07:48', 'A', NULL),
(344, 'Champion Series 7 Boys & Girls U-14', NULL, NULL, 'Tennis And More Sporting (Tams) ', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066 ', '', 'Bengaluru', '2019-07-29', '2019-08-02', '2019-07-08', '2019-07-22', '', '', NULL, 'O', 'Tennis And More Sporting (Tams) ', 'Cubbon Park Bangalore 56001 ', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2019-06-21', '2019-06-20 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/29th Jul 2019 TAMS77623.pdf', '2019-06-21 09:09:44', 'A', NULL),
(345, 'Champion Series 7 Boys & Girls U-18', NULL, NULL, 'SAT SPORTS PVT LTD ', 'SAT Sports Pvt Ltd, @ D Cube Sports Club, Sy. No -16, 17 & 18, Sathanur Village,  Bangalore, 562149 ', '', 'Bengaluru', '2019-08-05', '2019-08-09', '2019-07-15', '2019-07-29', '', '', NULL, 'O', 'SAT SPORTS PVT LTD ', 'CUBBON PARK, BANGALORE – 560 001 ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-06-21', '2019-07-17 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '12', 'uploads/factsheets/5th Aug 2019 SAT Sports18616.pdf', '2019-06-21 09:14:39', 'A', NULL),
(346, 'AITA Children U-10', NULL, NULL, 'SREE DATTA TENNIS ACADEMY, BENGALURU ', 'Sy.no :9\\5, kreedagramma , beside shiva temple , oppbharath petrol bunk, Kothanur, HennurMainroad, Bangalore-77. ', '', 'Bengaluru', '2019-07-08', '2019-07-09', '2019-07-06', '2019-07-06', '', '', NULL, 'O', 'SREE DATTA TENNIS ACADEMY', 'CUBBON PARK, BENGALURU - 560001 ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-06-29', '2019-06-28 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/8th Jul 2019 Sree Datta Sports Academy33992.pdf', '2019-06-29 04:41:53', 'A', NULL),
(347, 'AITA Children U-10', NULL, NULL, 'Tennis Arena ', ' 6th Cross Rd, deadend, Prakruthi Nagar, Kogilu, Bengaluru, Karnataka 560064 ', '', 'Bengaluru', '2019-07-15', '2019-07-16', '2019-07-13', '2019-07-13', '', '', NULL, 'O', 'Tennis Arena ', 'CUBBON PARK, BANGALORE – 560 001 ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-06-29', '2019-06-28 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/15th Jul 2019 Tennis Arena19408.pdf', '2019-06-29 04:46:35', 'A', NULL),
(348, 'Championship Series 7 Boys & Girls U-12', NULL, NULL, 'MYSORE TENNIS CLUB(MTC) ', 'KRISHNARAJ BOULEVARD CHAMRAJAPURA, \r\nMYSURU – 57 0 005 ', '', 'Mysore', '2019-08-12', '2019-08-16', '2019-07-22', '2019-08-05', '', '', NULL, 'O', 'MYSORE TENNIS CLUB(MTC)  ', 'CUBBON PARK, BENGALURU – 560 001     ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-07-08', '2019-07-07 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '10', 'uploads/factsheets/12th Aug 2019 MTC37115.pdf', '2019-07-08 03:48:15', 'A', NULL),
(349, 'Championship Series 7 Boys & Girls U-14', NULL, NULL, 'MYSORE TENNIS CLUB(MTC) ', 'KRISHNARAJ BOULEVARD CHAMRAJAPURA, \r\nMYSURU – 57 0 005 \r\n', '', 'Mysore', '2019-08-12', '2019-08-16', '2019-07-22', '2019-08-05', '', '', NULL, 'O', 'MYSORE TENNIS CLUB(MTC', 'CUBBON PARK, BENGALURU – 560 001     ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-07-08', '2019-07-07 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/12th Aug 2019 MTC42806.pdf', '2019-07-08 03:50:24', 'A', NULL),
(350, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'SAT SPORTS Pvt Ltd', 'SAT Sports Pvt Ltd, @ D Cube Sports Club, Sy. No -16, 17 & 18, Sathanur Village,  Bangalore, 562149 ', '', 'Bengaluru', '2019-08-19', '2019-08-23', '2019-07-29', '2019-08-12', '', '', NULL, 'O', 'SAT SPORTS Pvt Ltd   ', 'CUBBON PARK, BANGALORE – 560 001 ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-07-08', '2019-07-17 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/19th Aug 2019 SAT27463.pdf', '2019-07-08 03:55:33', 'A', NULL),
(351, 'AITA Children U-10', NULL, NULL, 'Tennis Arena  ', 'Venue - 6th Cross Rd, deadend, Prakruthi Nagar, Kogilu, Bengaluru, Karnataka 560064 ', '', 'Bengaluru', '2019-08-05', '2019-08-06', '2019-08-03', '2019-08-03', '', '', NULL, 'O', 'Tennis Arena ', 'CUBBON PARK, BANGALORE – 560 001 ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-07-18', '2019-07-17 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/5th Aug 2019 Tennis Arena66637.pdf', '2019-07-18 05:23:32', 'A', NULL),
(352, 'AITA Children U-10', NULL, NULL, 'Tennis And More Sporting (Tams) ', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066 ', '', 'Bengaluru', '2019-08-19', '2019-08-20', '2019-08-17', '2019-08-17', '', '', NULL, 'O', 'Tennis And More Sporting (Tams) ', 'Cubbon Park Bangalore 56001 ', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2019-07-18', '2019-07-17 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/19th Aug 2019 TAMS95034.pdf', '2019-07-18 05:33:26', 'A', NULL),
(353, 'Championship Series 7 Boys & Girls U-12', NULL, NULL, 'Tennis And More Sporting (Tams) ', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066 ', '', 'Bengaluru', '2019-08-19', '2019-08-23', '2019-07-29', '2019-08-12', '', '', NULL, 'O', 'Tennis And More Sporting (Tams) ', 'Cubbon Park Bangalore 56001 ', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2019-07-18', '2019-07-17 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/19th Aug 2019 TAMS.159152.pdf', '2019-07-18 07:56:11', 'A', NULL),
(354, 'AITA Children U-10', NULL, NULL, 'SAT SPORTS PVT LTD ', 'SAT Sports Pvt Ltd @ D Cube Sports Club, Sy. No-16, 17 & 18, Sathanur village,  Bangalore- 562149', '', 'Bengaluru', '2019-08-26', '2019-08-27', '2019-08-24', '2019-08-24', '', '', NULL, 'O', 'SAT SPORTS PVT LTD ', 'CUBBON PARK, BANGALORE – 560 001 ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-07-18', '2019-07-17 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/26th Aug 2019 SAT Sports 51429.pdf', '2019-07-18 07:59:53', 'A', NULL),
(355, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'ANT ACADEMY INTERNATIONAL, BANGALORE ', '20th ‘A’ Cross, MCECHS Layout Phase1, Venkateshpura Layout, Sampigehalli, Bengaluru560064. ', '', 'Bengaluru', '2019-08-26', '2019-08-30', '2019-08-05', '2019-08-12', '', '', NULL, 'O', 'ANT ACADEMY INTERNATIONAL, BANGALORE ', 'CUBBON PARK, BANGALORE – 560 001 ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-07-18', '2019-07-17 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/26th Aug 2019 Ant Tennis Academy32241.pdf', '2019-07-18 08:07:04', 'A', NULL),
(356, 'Championship Series 7 Boys & Girls U-14', NULL, NULL, 'FORTUNE TENNIS ACADEMY', 'FORTUNE TENNIS ACADEMY, 469, Fort Kengeri, Kengeri, Bengaluru, Karnataka 560060 ', '', 'Bengaluru', '2019-09-02', '2019-09-06', '2019-08-12', '2019-08-26', '', '', NULL, 'O', 'FORTUNE TENNIS ACADEMY ', 'CUBBON PARK, BENGALURU – 560 001     ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-07-18', '2019-07-17 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/2nd Sep 2019 Fortune Sports16202.pdf', '2019-07-18 08:10:31', 'A', NULL),
(357, 'National Series U-14 Boys & Girls', NULL, NULL, 'MYSORE TENNIS CLUB ', 'KRISHNARAJ BOULEVARD \r\nCHAMARAJAPURA, MYSURU- 57005 ', '', 'Mysuru', '2019-09-09', '2019-09-13', '2019-08-19', '2019-09-02', '', '', NULL, 'O', 'BHARATH TENNIS EXPRESS', 'CUBBON PARK, BANGALORE ', '', 'KARNATAKA STATE LAWN TENNIS ASSOSIATION ', '', '', '', '', '2019-07-30', '2019-07-29 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/9th Sep 2019 Bhart Tennis Express81320.pdf', '2019-07-30 07:58:42', 'A', NULL),
(358, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'TOPSPIN TENNIS. ', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68 ', '', 'Bengaluru', '2019-09-09', '2019-09-13', '2019-08-19', '2019-09-02', '', '', NULL, 'O', 'TOPSPIN TENNIS. ', 'CUBBON PARK,  BANGALORE ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-07-30', '2019-07-29 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/9th Sep 2019 Topspin Tennis Academy14523.pdf', '2019-07-30 08:06:01', 'A', NULL),
(359, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'TOPSPIN TENNIS', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68 ', '', 'Bengaluru', '2019-09-09', '2019-09-13', '2019-08-19', '2019-09-02', '', '', NULL, 'O', 'TOPSPIN TENNIS. ', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-07-30', '2019-07-29 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/9th Sep 2019 Topspin Tennis Academy35502.pdf', '2019-07-30 08:19:43', 'A', NULL),
(360, 'Men\'s 1 Lac', NULL, NULL, 'SREE DATTA TENNIS ACADEMY', 'SREE DATTA TENNIS ACADEMY, PATEL RAMAIAHA NAGAR, BESIDE SHIVA TEMPLE, OPP BHARATH PETROL BUNK, KOTHANOOR, HENNUR MAIN ROAD, BENGALURU – 560 077 \r\n \r\n ', '', 'Bengaluru', '2019-09-09', '2019-09-13', '2019-08-19', '2019-09-02', '', '1,00,000', NULL, 'O', 'SREE DATTA TENNIS ACADEMY', 'Cubbon Park, Banglaore-560001 ', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2019-08-14', '2019-08-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/9th Sep 2019 Sree Datta Tennis Academy60847.pdf', '2019-08-14 03:47:14', 'A', NULL),
(361, 'Championship Series 7 Boys & Girls U-12', NULL, NULL, 'Tennis And More Sporting (Tams) ', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066 ', '', 'Bengaluru', '2019-09-16', '2019-09-20', '2019-08-26', '2019-09-09', '', '', NULL, 'O', 'Tennis And More Sporting ( Tams )', 'Cubbon Park Bangalore 560001 ', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2019-08-14', '2019-08-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/16th Sep 2019 TAMS76179.pdf', '2019-08-14 03:57:49', 'A', NULL),
(362, 'Championship Series 7 Boys & Girls U-14', NULL, NULL, 'Tennis And More Sporting (Tams) ', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066 ', '', 'Bengaluru', '2019-09-16', '2019-09-20', '2019-08-26', '2019-09-09', '', '', NULL, 'O', 'Tennis And More Sporting (Tams)', 'Cubbon Park Bangalore 560001 ', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2019-08-14', '2019-08-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/16th Sep 2019 TAMS76433.pdf', '2019-08-14 04:00:48', 'A', NULL),
(363, 'Women\'s 1 Lac', NULL, NULL, 'The Bangalore City Institute ', '#8, Pampa Mahakavi Road, Opp. Makkala Koota, Bangalore – 560004 ', '', 'Bengaluru', '2019-09-16', '2019-09-20', '2019-08-26', '2019-09-09', '', '1,00,000', NULL, 'O', 'The Bangalore City Institute ', 'Cubbon Park, Banglaore-560001 ', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2019-08-14', '2019-08-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', 'uploads/factsheets/16th Sep 2019 The Bengaluru City Institute17695.pdf', '2019-08-14 04:04:50', 'A', NULL),
(364, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'SREE DATTA TENNIS ACADEMY ', 'SREE DATTA TENNIS ACADEMY, PATEL RAMAIAHA NAGAR, BESIDE SHIVA TEMPLE, OPP BHARATH PETROL BUNK, KOTHANOOR, HENNUR MAIN ROAD, BENGALURU – 560 077 ', '', 'Bengaluru', '2019-09-23', '2019-09-27', '2019-09-02', '2019-09-16', '', '', NULL, 'O', 'SREE DATTA TENNIS ACADEMY ', 'CUBBON PARK, BANGALORE -560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-08-14', '2019-08-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/23rd Sep 2019 Sree Datta Tennis Academy16644.pdf', '2019-08-14 04:39:53', 'A', NULL),
(365, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'SREE DATTA TENNIS ACADEMY ', 'SREE DATTA TENNIS ACADEMY, PATEL RAMAIAHA NAGAR, BESIDE SHIVA TEMPLE, OPP BHARATH PETROL BUNK, KOTHANOOR, HENNUR MAIN ROAD, BENGALURU – 560 077 ', '', 'Bengaluru', '2019-09-23', '2019-09-27', '2019-09-02', '2019-09-16', '', '', NULL, 'O', 'SREE DATTA TENNIS ACADEMY ', 'CUBBON PARK, BANGALORE -560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-08-14', '2019-08-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/23rd Sep 2019 Sree Datta Tennis Academy77865.pdf', '2019-08-14 04:42:08', 'A', NULL),
(366, 'Championship Series 7 Boys & Girls U-14', NULL, NULL, 'TOPSPIN TENNIS. ', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68 ', '', 'Bengaluru', '2019-09-30', '2019-10-04', '2019-09-09', '2019-09-23', '', '', NULL, 'O', 'TOPSPIN TENNIS. ', 'CUBBON PARK,  BANGALORE', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-08-14', '2019-08-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/30th Sep 2019 Topspin 78732.pdf', '2019-08-14 04:57:17', 'A', NULL),
(367, 'Championship Series 7 Boys & Girls U-16', NULL, NULL, 'TOPSPIN TENNIS. ', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68 ', '', 'Bengaluru', '2019-09-30', '2019-10-04', '2019-09-09', '2019-09-23', '', '', NULL, 'O', 'TOPSPIN TENNIS', 'CUBBON PARK,  BANGALORE-560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-08-14', '2019-08-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/30th Sep 2019 Topspin 61453.pdf', '2019-08-14 04:59:31', 'A', NULL),
(368, 'AITA Children U-10', NULL, NULL, 'NOAH TENNIS ACADEMY, BENGALURU', '# 25, JAYANTHINAGAR, HORAMAVU, BENGALURU –560 016NEXT TO: VIBGYOR SCHOOL', '', 'Bengaluru', '2019-09-09', '2019-09-10', '2019-09-07', '2019-09-07', '', '', NULL, 'O', 'QUEENS SPORTS PVT LTD', '# 25, JAYANTHINAGAR, HORAMAVU, BENGALURU –560 016NEXT TO: VIBGYOR SCHOOL', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-08-28', '2019-08-27 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/9th Sep 2019 Queens Sports Pvt Ltd 71455.pdf', '2019-08-28 07:08:24', 'A', NULL),
(369, 'Championship Series 7 Boys & Girls U-14', NULL, NULL, 'ROHAN BOPANNA TENNIS ACADEMY[RBTA] AT SPORTS SCHOOL ', '92 VADERAHALLI,19TH MILE ,KANAKAPURA ROAD, BENGALURU 562112 ', '', 'Bengaluru', '2019-10-07', '2019-10-11', '2019-09-16', '2019-09-30', '', '', NULL, 'O', 'ROHAN BOPANNA TENNIS ACADEMY[RBTA] AT SPORTS SCHOOL ', 'Cubbon Park Bangalore 56001 ', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2019-08-28', '2019-08-27 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/7th Oct 2019 RBTA21530.pdf', '2019-08-28 07:19:49', 'A', NULL),
(370, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'Transform Tennis Academy ', '16th Cross, 13th Main Road, Virat Nagar, Bommanahalli, Bangalore-560068 ', '', 'Bengaluru', '2019-10-14', '2019-10-18', '2019-09-23', '2019-10-07', '', '', NULL, 'O', 'Transform Tennis Academy ', 'CUBBON PARK,  BANGALORE ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-08-28', '2019-08-27 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/14th Oct 2019 Transform Tennis 99980.pdf', '2019-08-28 07:27:16', 'A', NULL),
(371, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'Transform Tennis Academy ', '16th Cross, 13th Main Road, Virat Nagar, Bommanahalli, Bangalore-560068 ', '', 'Bengaluru', '2019-10-14', '2019-10-18', '2019-09-23', '2019-10-07', '', '', NULL, 'O', 'Transform Tennis Academy ', 'CUBBON PARK,  BANGALORE ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-08-28', '2019-08-27 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/14th Oct 2019 Transform Tennis 45330.pdf', '2019-08-28 07:29:52', 'A', NULL),
(372, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'Transform Tennis Academy ', 'Sri Tirumalaraya Swamy Temple Road, Thubarahalli extended road, Thubarahalli, Whitefield Main Road, Marathalli, Bangalore-560066 ', '', 'Bengaluru', '2019-10-21', '2019-10-25', '2019-09-30', '2019-10-14', '', '', NULL, 'O', 'Transform Tennis Academy ', 'CUBBON PARK,  BANGALORE -560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-08-28', '2019-08-27 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/21st Oct 2019 Transform Tennis 7738.pdf', '2019-08-28 07:34:39', 'A', NULL),
(373, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'Transform Tennis Academy ', 'Sri Tirumalaraya Swamy Temple Road, Thubarahalli extended road, Thubarahalli, Whitefield Main Road, Marathalli, Bangalore-560066 ', '', 'Bengaluru', '2019-10-21', '2019-10-25', '2019-09-30', '2019-10-14', '', '', NULL, 'O', 'Transform Tennis Academy ', 'CUBBON PARK,  BANGALORE -560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-08-28', '2019-08-27 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/21st Oct 2019 Transform Tennis 17869.pdf', '2019-08-28 07:36:56', 'A', NULL),
(374, 'Men\'s 1 Lac', NULL, NULL, 'BOWRING INSTITUTE', 'BOWRING INSTITUTE, NO. 19, ST.MARK’S ROAD, B’LORE-1 ', '', 'Bengaluru', '2019-10-21', '2019-10-25', '2019-09-30', '2019-10-14', '', '1,00,000', NULL, 'O', 'BOWRING INSTITUTE ', 'CUBBON PARK, BANGALORE – 560 001 ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-09-05', '2019-09-04 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/21st Oct 2019 Bowring Institute 6776.pdf', '2019-09-05 08:49:47', 'A', NULL),
(375, 'Championship Series 7 Boys & Girls U-16', NULL, NULL, 'TOPSPINTENNIS', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68 ', '', 'Bengaluru', '2019-10-28', '2019-11-01', '2019-10-07', '2019-10-21', '', '', NULL, 'O', 'TOPSPINTENNIS. ', 'CUBBON PARK,  BANGALORE ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-09-14', '2019-09-22 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', 'uploads/factsheets/28th Oct 2019 Topspin Tennis Academy42989.pdf', '2019-09-23 05:18:02', 'A', NULL),
(376, 'Championship Series 7 Boys & Girls U-18', NULL, NULL, 'TOPSPINTENNIS', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68 ', '', 'Bengaluru', '2019-10-28', '2019-11-01', '2019-10-07', '2019-10-21', '', '', NULL, 'O', 'TOPSPIN TENNIS. ', 'CUBBON PARK,  BANGALORE-560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-09-23', '2019-09-22 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '12', 'uploads/factsheets/28th Oct 2019 Topspin Tennis Academy51530.pdf', '2019-09-23 05:22:13', 'A', NULL),
(377, 'Championship Series 7 Boys & Girls U-12', NULL, NULL, 'Tennis And More Sporting (Tams) ', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066 ', '', 'Bengaluru', '2019-11-11', '2019-11-15', '2019-10-21', '2019-11-04', '', '', NULL, 'O', 'Tennis And More Sporting (Tams) ', 'Cubbon Park Bangalore 560001 ', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2019-09-24', '2019-09-23 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/11th Nov 2019 TAMS70615.pdf', '2019-09-24 09:11:26', 'A', NULL),
(378, 'Championship Series 7 Boys & Girls U-14', NULL, NULL, 'Tennis And More Sporting (Tams) ', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066 ', '', 'Bengaluru', '2019-11-11', '2019-11-15', '2019-10-21', '2019-11-04', '', '', NULL, 'O', 'Tennis And More Sporting (Tams) ', 'Cubbon Park Bangalore 560001 ', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2019-09-24', '2019-09-23 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/11th Nov 2019 TAMS36100.pdf', '2019-09-24 09:15:03', 'A', NULL),
(379, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'TOPSPIN TENNIS', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68 ', '', 'Bengaluru', '2019-11-18', '2019-11-22', '2019-10-28', '2019-11-11', '', '', NULL, 'O', 'TOPSPIN TENNIS', 'CUBBON PARK,  BANGALORE -560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-09-24', '2019-09-23 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/18th Nov 2019 Topspin 4607.pdf', '2019-09-24 09:20:26', 'A', NULL),
(380, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'TOPSPIN TENNIS', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68 ', '', 'Bengaluru', '2019-11-18', '2019-11-22', '2019-10-28', '2019-11-11', '', '', NULL, 'O', 'TOPSPIN TENNIS', 'CUBBON PARK,  BANGALORE-560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-09-24', '2019-09-23 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/18th Nov 2019 Topspin 89353.pdf', '2019-09-24 09:22:14', 'A', NULL),
(381, 'Championship Series 7 Boys & Girls U-14', NULL, NULL, 'Tennis And More Sporting (Tams) ', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066 ', '', 'Bengaluru', '2019-11-25', '2019-11-29', '2019-11-04', '2019-11-18', '', '', NULL, 'O', 'Tennis And More Sporting (Tams) ', 'Cubbon Park Bangalore 560001 ', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2019-09-24', '2019-09-23 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '1', '29', 'uploads/factsheets/25th Nov 2019 TAMS75516.pdf', '2019-09-24 09:26:35', 'A', NULL),
(382, 'Championship Series 7 Boys & Girls U-16', NULL, NULL, 'Tennis And More Sporting (Tams)', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066 ', '', 'Bengaluru', '2019-11-25', '2019-11-29', '2019-11-04', '2019-11-18', '', '', NULL, 'O', 'Tennis And More Sporting (Tams) ', 'Cubbon Park Bangalore 560001 ', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2019-09-24', '2019-09-23 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/25th Nov 2019 TAMS74013.pdf', '2019-09-24 09:28:40', 'A', NULL),
(383, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'Transform Tennis Academy ', '16th Cross, 13th Main Road, Virat Nagar, Bommanahalli, Bangalore-560068 ', '', 'Bengaluru', '2019-11-04', '2019-11-08', '2019-10-14', '2019-10-28', '', '', NULL, 'O', 'Transform Tennis Academy ', 'CUBBON PARK,  BANGALORE ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-10-10', '2019-10-09 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', NULL, '0000-00-00 00:00:00', 'A', NULL),
(384, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'Transform Tennis Academy ', '16th Cross, 13th Main Road, Virat Nagar, Bommanahalli, Bangalore-560068 ', '', 'Bengaluru', '2019-11-04', '2019-11-08', '2019-10-14', '2019-10-28', '', '', NULL, 'O', 'Transform Tennis Academy ', 'CUBBON PARK,  BANGALORE ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-10-10', '2019-10-09 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/4th Nov 2019 Transform Tennis Academy90473.pdf', '2019-10-10 03:11:20', 'A', NULL),
(385, 'AITA Children U-10', NULL, NULL, 'Transform Tennis Academy  ', 'Sri Tirumalaraya Swamy Temple Road, Thubarahalli extended road, Thubarahalli, Whitefield Main Road, Marathalli, Bangalore560066 ', '', 'Bengaluru', '2019-11-18', '2019-11-19', '2019-11-16', '2019-11-16', '', '', NULL, 'O', 'Transform Tennis Academy ', 'CUBBON PARK,  BANGALORE ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-10-10', '2019-10-09 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/18th Nov 2019 Transform 38841.pdf', '2019-10-10 04:07:53', 'A', NULL),
(386, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'MAHILA SEVA SAMAJA ', '# 80 K R Road Shankar puram, Bengaluru -560004 ', '', 'Bengaluru', '2019-12-09', '2019-12-13', '2019-11-18', '2019-12-02', '', '', NULL, 'O', 'MAHILA SEVA SAMAJA ', 'Cubbon Park Bangaluru -560001 \r\n  ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-10-10', '2019-10-09 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/9th Dec 2019 MSS92192.pdf', '2019-10-10 04:14:30', 'A', NULL),
(387, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'MAHILA SEVA SAMAJA ', '# 80 K R Road Shankar puram, Bengaluru -560004 ', '', 'Bengaluru', '2019-12-16', '2019-12-20', '2019-11-25', '2019-12-09', '', '', NULL, 'O', 'MAHILA SEVA SAMAJA ', 'Cubbon Park Bangaluru -560001 \r\n', '', '# 80 K R Road Shankar puram, Bengaluru -560004', '', '', '', '', '2019-10-10', '2019-10-09 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/16th Dec 2019 MSS6449.pdf', '2019-10-10 04:18:52', 'A', NULL),
(388, 'ASIAN TENNIS TOUR Women\'s US$ 3000', NULL, NULL, 'Mysore Tennis Club ', 'MTC, Krishnaraj Boulevard, Chamarajpura, Mysuru, karnataka , India ', '', 'Bengaluru', '2019-11-11', '2019-11-15', '2019-10-28', '2019-11-04', '', 'US $ 3000', NULL, 'O', 'Mysore Tennis Club ', 'Cubbon Park, Bengaluru-560001', '', 'KSLTA', '', '', '', '', '2019-10-26', '2019-10-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '6', '14', 'uploads/factsheets/11th Nov 2019 MTC 90784.pdf', '2019-10-26 04:28:05', 'A', NULL);
INSERT INTO `ksl_tournaments_tbl` (`tourn_id`, `tourn_title`, `tourn_category`, `tourn_sub_category`, `tourn_venue`, `tourn_venue_address1`, `tourn_venue_address2`, `tourn_venue_city`, `tourn_from_date`, `tourn_to_date`, `tourn_last_submission_date`, `tourn_withdrl_date`, `tourn_description`, `tourn_price_money`, `tourn_tournament_image`, `tourn_register_status`, `tourn_asso_title`, `tourn_asso_address1`, `tourn_asso_address2`, `tourn_asso_head`, `tourn_refree1`, `tourn_refree2`, `tourn_refree3`, `tourn_link`, `tourn_created_date`, `tourn_lastmodified_date`, `tourn_status`, `sponser_name_of_organisation`, `sponser_address1`, `sponser_address2`, `sponser_contact_person_name`, `sponser_phone`, `sponser_email`, `tour_event_category`, `tour_event_subcategory`, `tourn_factsheet`, `tourn_factsheet_uploaded_at`, `tourn_facesheet_status`, `tourn_gallery_id`) VALUES
(389, 'Champion Series 7 Boys & Girls U-14', NULL, NULL, 'Tennis And More Sporting (Tams) ', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066 ', '', 'Bengaluru', '2019-12-23', '2019-12-27', '2019-12-02', '2019-10-16', '', '', NULL, 'O', 'Tennis And More Sporting (Tams) ', 'Cubbon Park Bangalore 560001 ', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2019-10-26', '2019-10-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/23rd Dec 2019 TAMS24347.pdf', '2019-10-26 04:38:59', 'A', NULL),
(390, 'Champion Series 7 Boys & Girls U-16', NULL, NULL, 'Tennis And More Sporting (Tams) ', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066 ', '', 'Bengaluru', '2019-12-23', '2019-12-27', '2019-12-02', '2019-12-16', '', '', NULL, 'O', 'Tennis And More Sporting (Tams) ', 'Cubbon Park Bangalore 560001 ', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2019-10-26', '2019-10-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/23rd Dec 2019 TAMS97111.pdf', '2019-10-26 04:41:07', 'A', NULL),
(391, 'Championship Series 7 Boys & Girls U-14', NULL, NULL, 'TOPSPIN TENNIS. ', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68 ', '', 'Bengaluru', '2019-12-02', '2019-12-06', '2019-11-11', '2019-12-25', '', '', NULL, 'O', 'TOPSPIN TENNIS.', 'CUBBON PARK,  BENGALURU-560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-11-07', '2019-11-06 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', NULL, '0000-00-00 00:00:00', 'A', NULL),
(392, 'Championship Series 7 Boys & Girls U-16', NULL, NULL, 'TOPSPIN TENNIS.', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68 ', '', 'Bengaluru', '2019-12-02', '2019-12-06', '2019-11-11', '2019-11-25', '', '', NULL, 'O', 'TOPSPIN TENNIS', 'CUBBON PARK,  BENGALURU-560001 ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-11-07', '2019-11-06 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/2nd Dec 2019 Topspin Tennis 47305.pdf', '2019-11-07 07:40:00', 'A', NULL),
(393, 'Asian Tennis Federation U-14 Boys & Girls ', NULL, NULL, 'Tams Tennis Academy', '5/5, Marathalli-Varthur Rd, Sidhapura\r\nBangalore-560037', '', 'Bengaluru', '2020-01-13', '2020-01-18', '2019-12-17', '2019-12-31', '', '', NULL, 'O', 'Tams Tennis Academy', 'Cubbon Park, Bengaluru-560001', '', 'Karnataka State Lawn Tennis Association', '', '', '', '', '2019-12-04', '2019-12-03 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', 'uploads/factsheets/13th Jan 2020 TAMS74007.pdf', '2019-12-04 07:24:21', 'A', NULL),
(394, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'TOPSPIN TENNIS', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68 ', '', 'Bengaluru', '2020-01-13', '2020-01-17', '2019-12-23', '2020-01-06', '', '', NULL, 'O', 'TOPSPIN TENNIS', 'CUBBON PARK,  BANGALORE ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-12-04', '2019-12-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/13th Jan 2020 Topspin Tennis 72388.pdf', '2019-12-04 07:31:01', 'A', NULL),
(395, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'TOPSPIN TENNIS', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68 ', '', 'Bengaluru', '2020-01-13', '2020-01-17', '2019-12-23', '2020-01-06', '', '', NULL, 'O', 'TOPSPIN TENNIS', 'CUBBON PARK,  BANGALORE -560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-12-04', '2019-12-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/13th Jan 2020 Topspin Tennis 68600.pdf', '2019-12-04 07:33:07', 'A', NULL),
(396, 'Championship Series 7 Boys & Girls U-14', NULL, NULL, 'TOPSPIN TENNIS', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68 ', '', 'Bengaluru', '2020-01-27', '2020-01-31', '2020-01-06', '2020-01-20', '', '', NULL, 'O', 'TOPSPIN TENNIS', 'CUBBON PARK,  BANGALORE -560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-12-04', '2019-12-03 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/27th Jan 2020 Topspin 96505.pdf', '2019-12-04 07:37:41', 'A', NULL),
(397, 'Championship Series 7 Boys & Girls U-16', NULL, NULL, 'TOPSPIN TENNIS', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68 ', '', 'Bengaluru', '2020-01-27', '2020-01-31', '2020-01-06', '2020-01-20', '', '', NULL, 'O', 'TOPSPIN TENNIS', 'CUBBON PARK,  BANGALORE-560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-12-04', '2019-12-03 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/27th Jan 2020 Topspin 48053.pdf', '2019-12-04 07:40:12', 'A', NULL),
(398, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'TOPSPIN TENNIS', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68', '', 'Bengaluru', '2020-02-10', '2020-02-14', '2020-01-20', '2020-02-03', '', '', NULL, 'O', 'TOPSPIN TENNIS', 'CUBBON PARK,  Bengaluru-560001 ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-12-14', '2019-12-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/10th Feb 2020 Tospin Tennis85459.pdf', '2019-12-14 01:20:03', 'A', NULL),
(399, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'TOPSPIN TENNIS', 'SURVEY # 14/3, NARAYANANAGAR 1ST BLOCK, NEAR APCO CEMENTING BLOCK COORPORATE OFFICE, DODDAKALSANDRA, UTTARAHALLI, OFF KANAKAPURA ROAD, BANGALORE 68 ', '', 'Bengaluru', '2020-02-10', '2020-02-14', '2020-01-20', '2020-02-03', '', '', NULL, 'O', 'TOPSPIN TENNIS', 'CUBBON PARK,  BANGALORE -560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2019-12-14', '2019-12-13 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/10th Feb 2020 Tospin Tennis78116.pdf', '2019-12-14 01:21:58', 'A', NULL),
(400, 'Talent Series 7 Boys & Girls U-12', NULL, NULL, 'ASHPIRE TENNIS ACADEMY', 'LAVENDER LANE, SANGAM ROAD, Opp. SALARPURIA SATTVA GOLD SUMMIT, NEXT TO DECATHLON, KOTHANUR,  BANGALORE 77', '', 'Bengaluru', '2020-01-20', '2020-01-24', '2019-12-30', '2020-01-13', '', '', NULL, 'O', 'ASHPIRE TENNIS ACADEMY ', 'CUBBON PARK,  BANGALORE -560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2019-12-26', '2019-12-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/20th Jan 2020 Ashpire Tennis Academy63978.pdf', '2019-12-26 07:39:16', 'A', NULL),
(401, 'Men\'s 1 Lac', NULL, NULL, 'Rohan Bopanna Tennis Academy ', '92 Vaderahalli,19th mile, Kanakapura road, Bengaluru 562112 ', '', 'Bengaluru', '2020-01-27', '2020-01-31', '2020-01-06', '2020-01-20', '', '1,00,000', NULL, 'O', 'Rohan Bopanna Tennis Academy', 'Cubbon Park Bangalore 560001 ', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2019-12-26', '2019-12-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/27th Jan 2020 RBTA39758.pdf', '2019-12-26 07:46:51', 'A', NULL),
(402, 'women\'s 1 Lac', NULL, NULL, 'Rohan Bopanna Tennis Academy ', '92 Vaderahalli,19th mile, Kanakapura road, Bengaluru 562112 ', '', 'Bengaluru', '2020-01-27', '2020-01-31', '2020-01-06', '2020-01-20', '', '1,00,000', NULL, 'O', 'Rohan Bopanna Tennis Academy ', 'Cubbon Park Bangalore 560001 ', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2019-12-26', '2019-12-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', 'uploads/factsheets/27th Jan 2020 RBTA96130.pdf', '2019-12-26 07:49:20', 'A', NULL),
(403, 'Championship Series 7 Boys & Girls U-12', NULL, NULL, 'Rohan Bopanna Tennis Academy ', '92 Vaderahalli,19th mile, Kanakapura road, Bengaluru 562112 ', '', 'Bengaluru', '2020-02-03', '2020-02-07', '2020-01-13', '2020-01-27', '', '', NULL, 'O', 'Rohan Bopanna Tennis Academy ', 'Cubbon Park Bangalore 56001 ', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2019-12-26', '2019-12-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/3rd Feb 2020 RBTA 29865.pdf', '2019-12-26 07:52:44', 'A', NULL),
(404, 'ATP Challenger Bengaluru Open-2020', NULL, NULL, 'KSLTA', 'Cubbon Park\r\nBengaluru-560001', '', 'Bengaluru', '2020-02-08', '2020-02-16', '2020-01-26', '2020-02-01', '', '', NULL, 'O', 'KSLTA', 'Cubbon Park ', '', 'KSLTA', '', '', '', '', '2019-12-26', '2019-12-25 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '5', '13', NULL, '0000-00-00 00:00:00', 'A', NULL),
(405, 'Men\'s 1 Lac KSLTA', NULL, NULL, 'KSLTA', 'CUBBON PARK, BENGALURU - 560001 ', '', 'Bengaluru', '2020-04-20', '2020-04-25', '2020-03-30', '2020-04-13', '', '1,00,000', NULL, 'O', 'KSLTA ', 'CUBBON PARK,  BANGALORE -560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2020-01-08', '2020-01-07 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '5', '13', 'uploads/factsheets/20th Apr 2020 KSLTA51298.pdf', '2020-01-08 07:14:57', 'A', NULL),
(406, 'Men\'s 1 Lac', NULL, NULL, 'Rohan Bopanna Tennis Academy at The Sports Schoo', '92 Vaderahalli,19th mile, Kanakapura road, Bengaluru 562112 ', '', 'Bengaluru', '2020-02-10', '2020-02-14', '2020-01-20', '2020-02-03', '', '1,00,000', NULL, 'O', 'Rohan Bopanna Tennis Academy ', 'Cubbon Park Bangalore 560001 ', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2020-01-29', '2020-02-21 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/10th Feb 2020 RBTA 82750.pdf', '2020-02-22 06:01:45', 'A', NULL),
(407, 'women\'s 1 Lac', NULL, NULL, 'Rohan Bopanna Tennis Academy at The Sports Schoo', '92 Vaderahalli,19th mile, Kanakapura road, Bengaluru 562112 ', '', 'Bengaluru', '2020-02-10', '2020-02-14', '2020-01-20', '2020-02-03', '', '1,00,000', NULL, 'O', 'Rohan Bopanna Tennis Academy ', 'Cubbon Park Bangalore 560001 ', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2020-01-29', '2020-02-21 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '8', '14', 'uploads/factsheets/10th Feb 2020 RBTA 90215.pdf', '2020-02-22 06:01:05', 'A', NULL),
(408, 'AITA Children U-10', NULL, NULL, 'PADUKONE -DRAVID  CENTRE FOR SPORTS EXCELLENCE ', 'Survey No 336, Bettahalasura, Jala Hobli Yelahanka Taluk, Bengaluru- 562157, India ', '', 'Bengaluru', '2020-02-17', '2020-02-19', '2020-02-15', '2020-02-15', '', '', NULL, 'O', 'PADUKONE -DRAVID  CENTRE FOR SPORTS EXCELLENCE ', 'CUBBON PARK BENGALURU ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2020-02-22', '2020-02-21 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/17th Feb 2020 Dravid-Padukone Sports Excellence36575.pdf', '2020-02-22 05:21:51', 'A', NULL),
(409, 'AITA Children U-10', NULL, NULL, 'SAT SPORTS PVT LTD ', 'SAT Sports Pvt Ltd , @ D Cube Sports Club, Sy. No-16, 17 & 18, Sathanur village,  Bangalore- 562149', '', 'Bengaluru', '2020-02-24', '2020-02-26', '2020-02-22', '2020-02-22', '', '', NULL, 'O', 'SAT SPORTS PVT LT', 'CUBBON PARK, BANGALORE – 560 001 ', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2020-02-22', '2020-02-21 18:30:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', 'uploads/factsheets/24th Feb 2020 SAT Sports 84611.pdf', '2020-02-22 05:25:07', 'A', NULL),
(410, 'Championship Series 7 Boys & Girls U-16', NULL, NULL, 'Tennis And More Sporting (Tams) ', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066', '', 'Bengaluru', '2020-03-16', '2020-03-20', '2020-02-24', '2020-03-09', '', '', NULL, 'O', 'Tennis And More Sporting (Tams) ', 'Cubbon Park Bangalore 560001 ', '', 'Karnataka State Lawn Tennis Association', '', '', '', '', '2020-02-22', '2020-10-20 22:00:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', '', '2020-02-29 02:39:34', 'I', NULL),
(411, 'Championship Series 7 Boys & Girls U-18', NULL, NULL, 'Tennis And More Sporting (Tams)', '35/5, Sidhapura, Marathalli-Whitefield Main Road, Next to D Mart Super Market, Bangalore 560066 ', '', 'Bengaluru', '2020-03-16', '2020-03-20', '2020-02-24', '2020-03-06', '', '', NULL, 'O', 'Tennis And More Sporting (Tams) ', 'Cubbon Park Bangalore 560001', '', 'Karnataka State Lawn Tennis Association', '', '', '', '', '2020-02-22', '2020-10-20 22:00:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '12', '', '2020-02-29 02:39:21', 'I', NULL),
(412, 'AITA Children U-10', NULL, NULL, 'SAT SPORTS PVT LTD ', 'SAT Sports Pvt Ltd , @ D Cube Sports Club, Sy. No-16, 17 & 18, Sathanur village,  Bangalore- 562149 ', '', 'Bengaluru', '2020-03-23', '2020-03-25', '2020-03-21', '2020-03-21', '', '', NULL, 'O', 'SAT SPORTS PVT LTD ', 'CUBBON PARK, BANGALORE – 560 001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION', '', '', '', '', '2020-02-22', '2020-10-20 22:00:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '9', '', '2020-02-22 05:39:21', 'I', NULL),
(413, 'Talent Series 7 Boys & Girls U-16', NULL, NULL, 'Rohan Bopanna Tennis Academy ', '92 Vaderahalli,19th mile, Kanakapura road, Bengaluru 562112 ', '', 'Bengaluru', '2020-03-30', '2020-04-03', '2020-03-09', '2020-02-23', '', '', NULL, 'O', 'Rohan Bopanna Tennis Academy ', 'Cubbon Park Bangalore 56000', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2020-02-29', '2020-10-20 22:00:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '29', '', '2020-02-29 02:06:23', 'I', NULL),
(414, 'Super Series U-14 Boys & Girls ', NULL, NULL, 'KARNATAKA STATE LAWN TENNIS ASSOCIATION[KSLTA] ', 'CUBBON PARK, BENGALURU – 560 001 ', '', 'Bengaluru', '2020-04-06', '2020-04-10', '2020-03-16', '2020-03-30', '', '', NULL, 'O', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION[KSLTA]', 'CUBBON PARK, BENGALURU – 560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION[KSLTA]', '', '', '', '', '2020-02-29', '2020-10-20 22:00:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', '', '2020-02-29 02:17:40', 'I', NULL),
(415, 'Talent Series 7 Boys & Girls U-14', NULL, NULL, 'ASHPIRE TENNIS ACADEMY ', 'LAVENDER LANE, SANGAM ROAD,  Opp. SALARPURIA SATTVA GOLD SUMMIT, NEXT TO DECATHLON, HENNUR MAIN ROAD, KOTHANUR,   BANGALORE 77 ', '', 'Bengaluru', '2020-04-13', '2020-04-17', '2020-03-23', '2020-04-06', '', '', NULL, 'O', 'ASHPIRE TENNIS ACADEMY ', 'CUBBON PARK,  BANGALORE-560001', '', 'KARNATAKA STATE LAWN TENNIS ASSOCIATION ', '', '', '', '', '2020-02-29', '2020-10-20 22:00:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '4', '11', '', '2020-02-29 02:35:14', 'I', NULL),
(420, 'Talent Series Boys & Girls U-16', NULL, NULL, 'Karnataka State Lawn Tennis Association ', 'Cubbon Park \r\nBengaluru-560001', '', 'Bengaluru', '2020-11-16', '2020-11-20', '2020-11-19', '2020-11-19', '', '', NULL, 'O', 'KSLTA-AITA Talent Series 16 and Under ', 'KSLTA Tennis Stadium, Cubbon Park, Bengaluru 560001', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2020-11-11', '2020-11-30 23:00:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/16th Nov 2020 KSLTA39644.docx', '2020-11-11 10:38:38', 'A', NULL),
(421, 'Talent Series Boys & Girls U-14', NULL, NULL, 'Karnataka State Lawn Tennis Association ', 'KSLTA Tennis Stadium, Cubbon Park, Bengaluru 560001', '', 'Bengaluru', '2020-11-23', '2020-11-25', '2020-11-26', '2020-11-26', '', '', NULL, 'O', 'KSLTA-AITA Talent Series 14 and Under ', 'KSLTA Tennis Stadium, Cubbon Park, Bengaluru 560001', '', 'Karnataka State Lawn Tennis Association ', '', '', '', '', '2020-11-11', '2020-11-30 23:00:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/23rd Nov 2020 KSLTA29114.doc', '2020-11-11 10:40:47', 'A', NULL),
(422, 'Talent Series Boys & Girls U-12', NULL, NULL, 'Transform Tennis Academy, HENNUR', 'Sy.No 9/4, Patel Ramaiah Nagar, Raika road, off,\r\nHennur Main Rd, Kothanur, Bengaluru,\r\nKarnataka 560077\r\n', '', 'Bengaluru', '2020-11-30', '2020-12-02', '2020-11-27', '2020-11-27', '', '', NULL, 'O', 'Transform Tennis-AITA Talent Series 12 and Under', 'KSLTA Tennis Stadium, Cubbon Park, Bengaluru\r\n560001\r\n', '', 'Karnataka State Lawn Tennis Association', '', '', '', '', '2020-11-24', '2020-11-30 23:00:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '10', 'uploads/factsheets/30th Nov 2020 Transform Tennis99880.pdf', '2020-11-24 13:35:07', 'A', NULL),
(423, 'Championship Series Boys & Girls U-14', NULL, NULL, 'Centre for Integrated Sports Learning(CISL)', 'Centre for Integrated Sports Learning,\r\nSy. No. 217, near Manito Apartment\r\nDoddajala village and post, Jala Hobli\r\nBengaluru – 562157 ', '', 'Bengaluru', '2020-12-14', '2020-11-16', '2020-12-11', '2020-12-11', '', '', NULL, 'O', 'CISL-AITA Championship Series 14 and Under', 'KSLTA Tennis Stadium, Cubbon Park, \r\nBengaluru-560001', '', 'Karnataka State Lawn Tennis Association', '', '', '', '', '2020-11-25', '2020-11-30 23:00:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '11', 'uploads/factsheets/14th Dec 2020 CISL68704.pdf', '2020-11-25 12:40:05', 'A', NULL),
(424, 'Men\'s 1 Lac', NULL, NULL, 'Centre for Integrated Sports Learning(CISL)', 'Centre for Integrated Sports Learning,\r\nSy. No. 217, near Manito Apartment\r\nDoddajala village and post, Jala Hobli\r\nBangalore – 562157', '', 'Bengaluru', '2020-12-21', '2020-12-25', '2020-11-30', '2020-12-14', '', '1,00,000', NULL, 'O', 'CISL AITA MEN\'S 1 LAC TENNIS TOURNAMENT', 'KSLTA Tennis Stadium, Cubbon Park, \r\nBengaluru-560001\r\n', '', 'Karnataka State Lawn Tennis Association', '', '', '', '', '2020-12-01', '2020-11-30 23:00:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '7', '13', 'uploads/factsheets/21st Dec CISL51507.pdf', '2020-12-01 13:29:50', 'A', NULL),
(425, 'Talent Series Boys & Girls U-16', NULL, NULL, 'Transform Tennis Academy,BOMMANAHALLI', '16 Cross Road, 13th Main Rd, Virat Nagar,\r\nBommanahalli, Bengaluru, Karnataka 560068', '', 'Bengaluru', '2020-12-07', '2020-12-07', '2020-12-09', '2020-12-04', '', '', NULL, 'O', 'Transform Tennis-AITA Talent Series 16 and Under', 'KSLTA Tennis Stadium, Cubbon Park,\r\nBengaluru-560001', '', 'Karnataka State Lawn Tennis Association', '', '', '', '', '2020-12-01', '2020-11-30 23:00:00', 'A', NULL, NULL, NULL, NULL, NULL, NULL, '2', '29', 'uploads/factsheets/7th Dec 2020 Transform Tennis80678.pdf', '2020-12-01 13:40:06', 'A', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ksl_tournament_gallery`
--

CREATE TABLE `ksl_tournament_gallery` (
  `tourn_gallery_id` bigint(20) NOT NULL,
  `tourn_id` bigint(20) NOT NULL,
  `gallery_id` bigint(20) NOT NULL,
  `tourn_gallery_date` date NOT NULL,
  `tourn_gallery_created_at` datetime NOT NULL,
  `tourn_gallery_modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tourn_gallery_status` enum('A','I') NOT NULL COMMENT 'A=Active, I=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ksl_tournament_gallery`
--

INSERT INTO `ksl_tournament_gallery` (`tourn_gallery_id`, `tourn_id`, `gallery_id`, `tourn_gallery_date`, `tourn_gallery_created_at`, `tourn_gallery_modified_at`, `tourn_gallery_status`) VALUES
(1, 2, 10, '2016-06-16', '2016-06-16 10:42:05', '2016-06-16 08:42:05', 'A'),
(2, 2, 11, '2016-06-16', '2016-06-16 10:42:05', '2016-06-16 08:42:05', 'A'),
(3, 2, 12, '2016-06-16', '2016-06-16 10:42:05', '2016-06-16 08:42:05', 'A'),
(4, 2, 13, '2016-06-16', '2016-06-16 10:42:05', '2016-06-16 08:42:05', 'A'),
(5, 2, 14, '2016-06-16', '2016-06-16 10:42:05', '2016-06-16 08:42:05', 'A'),
(6, 2, 15, '2016-06-16', '2016-06-16 10:42:05', '2016-06-16 08:42:05', 'A'),
(7, 12, 14, '2016-06-16', '2016-06-16 12:41:03', '2016-06-16 10:41:03', 'A'),
(8, 12, 15, '2016-06-16', '2016-06-16 12:41:03', '2016-06-16 10:41:03', 'A'),
(9, 12, 16, '2016-06-16', '2016-06-16 12:41:03', '2016-06-16 10:41:03', 'A'),
(10, 12, 16, '2016-06-16', '2016-06-16 12:47:18', '2016-06-16 10:47:18', 'A'),
(11, 12, 15, '2016-06-16', '2016-06-16 13:26:33', '2016-06-16 11:26:33', 'A'),
(12, 12, 16, '2016-06-16', '2016-06-16 13:26:33', '2016-06-16 11:26:33', 'A'),
(13, 8, 2, '2016-06-16', '2016-06-16 13:43:11', '2016-06-16 11:43:11', 'A'),
(14, 8, 3, '2016-06-16', '2016-06-16 13:43:11', '2016-06-16 11:43:11', 'A'),
(15, 8, 2, '2016-06-16', '2016-06-16 13:43:32', '2016-06-16 11:43:32', 'A'),
(16, 8, 3, '2016-06-16', '2016-06-16 13:43:32', '2016-06-16 11:43:32', 'A'),
(17, 6, 2, '2016-06-16', '2016-06-16 13:44:41', '2016-06-16 11:44:41', 'A'),
(18, 7, 2, '2016-07-26', '2016-07-26 15:19:58', '2016-07-26 13:19:58', 'A'),
(19, 7, 3, '2016-07-26', '2016-07-26 15:19:58', '2016-07-26 13:19:58', 'A'),
(20, 18, 3, '2016-10-21', '2016-10-21 00:39:47', '2016-10-21 05:39:47', 'A'),
(21, 18, 11, '2016-10-21', '2016-10-21 00:39:47', '2016-10-21 05:39:47', 'A'),
(22, 18, 12, '2016-10-21', '2016-10-21 00:39:47', '2016-10-21 05:39:47', 'A'),
(23, 18, 13, '2016-10-21', '2016-10-21 00:39:47', '2016-10-21 05:39:47', 'A'),
(31, 19, 19, '2016-10-22', '2016-10-22 03:27:36', '2016-10-22 08:27:36', 'A'),
(34, 20, 3, '2016-10-25', '2016-10-25 02:13:17', '2016-10-25 07:13:17', 'A'),
(44, 92, 29, '2017-02-11', '2017-02-11 04:17:27', '2017-02-11 10:17:27', 'A'),
(45, 97, 24, '2017-02-16', '2017-02-16 00:14:09', '2017-02-16 06:14:09', 'A'),
(46, 117, 30, '2017-04-18', '2017-04-18 05:07:57', '2017-04-18 10:07:57', 'A'),
(47, 105, 30, '2017-04-18', '2017-04-18 05:09:52', '2017-04-18 10:09:52', 'A'),
(50, 118, 10, '2017-07-27', '2017-07-27 03:29:59', '2017-07-27 08:29:59', 'A'),
(51, 118, 11, '2017-07-27', '2017-07-27 03:29:59', '2017-07-27 08:29:59', 'A'),
(52, 118, 12, '2017-07-27', '2017-07-27 03:29:59', '2017-07-27 08:29:59', 'A'),
(53, 118, 13, '2017-07-27', '2017-07-27 03:29:59', '2017-07-27 08:29:59', 'A'),
(54, 118, 14, '2017-07-27', '2017-07-27 03:29:59', '2017-07-27 08:29:59', 'A'),
(55, 118, 15, '2017-07-27', '2017-07-27 03:29:59', '2017-07-27 08:29:59', 'A'),
(56, 118, 31, '2017-07-27', '2017-07-27 03:29:59', '2017-07-27 08:29:59', 'A'),
(57, 147, 32, '2017-09-08', '2017-09-08 03:25:01', '2017-09-08 08:25:01', 'A'),
(58, 261, 33, '2018-10-13', '2018-10-13 01:55:40', '2018-10-13 06:55:40', 'A'),
(59, 260, 34, '2018-12-28', '2018-12-28 03:43:00', '2018-12-28 09:43:00', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `ksl_tournament_playerlists`
--

CREATE TABLE `ksl_tournament_playerlists` (
  `playerlists_id` bigint(20) NOT NULL,
  `tourn_id` bigint(20) NOT NULL,
  `event_id` bigint(20) NOT NULL,
  `playerlists_filename` varchar(1000) NOT NULL,
  `playerlists_filepath` text NOT NULL,
  `playerlists_created_at` datetime NOT NULL,
  `playerlists_modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `playerlists_active_status` enum('A','I') NOT NULL COMMENT 'A=Active, I=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ksl_tournament_playerlists`
--

INSERT INTO `ksl_tournament_playerlists` (`playerlists_id`, `tourn_id`, `event_id`, `playerlists_filename`, `playerlists_filepath`, `playerlists_created_at`, `playerlists_modified_at`, `playerlists_active_status`) VALUES
(5, 81, 382, 'MSS U-14 Boys_playerkslta_36.pdf', 'uploads/playerlists/MSS U-14 Boys_playerkslta_36.pdf', '2016-12-25 02:26:40', '2016-12-25 08:26:40', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `ksl_tournament_schedule`
--

CREATE TABLE `ksl_tournament_schedule` (
  `ts_id` int(11) NOT NULL,
  `ts_tournament_id` bigint(20) NOT NULL,
  `ts_event_id` bigint(20) NOT NULL,
  `ts_tournament_result` text NOT NULL,
  `ts_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ksl_tournament_schedule`
--

INSERT INTO `ksl_tournament_schedule` (`ts_id`, `ts_tournament_id`, `ts_event_id`, `ts_tournament_result`, `ts_date_time`) VALUES
(1, 1, 1, '{\"teams\":[[{\"flag\":\"<i class=\\\"player_id_css\\\">5656ffffff <\\/i>\",\"name\":\"first name sf333 \"},{\"flag\":\"<i class=\\\"player_id_css\\\">test123 <\\/i>\",\"name\":\"first name name \"}],[{\"flag\":\"<i class=\\\"player_id_css\\\">test123 <\\/i>\",\"name\":\"first name last name \"},{\"flag\":\"<i class=\\\"player_id_css\\\">5656ffffff <\\/i>\",\"name\":\"first name sf333 \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}]],\"results\":[[[[\"18\",\"5\"],[\"3\",{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}]],[[\"\",\"\"],[\"\",\"\"]],[[\"\",\"\"]]]]}', '2016-08-23 14:19:18'),
(2, 13, 74, '{\"teams\":[[{\"flag\":\"<i class=\\\"player_id_css\\\">1015 <\\/i>\",\"name\":\"Boy1 name \"},{\"flag\":\"<i class=\\\"player_id_css\\\">1016 <\\/i>\",\"name\":\"Boy2 name \"}],[{\"flag\":\"<i class=\\\"player_id_css\\\">1017 <\\/i>\",\"name\":\"Boy3 last name \"},{\"flag\":\"<i class=\\\"player_id_css\\\">1019 <\\/i>\",\"name\":\"Boy4 sf333 \"}],[{\"flag\":\"<i class=\\\"player_id_css\\\">1015 <\\/i>\",\"name\":\"Boy1 name \"},{\"flag\":\"<i class=\\\"player_id_css\\\">1016 <\\/i>\",\"name\":\"Boy2 name \"}],[{\"flag\":\"<i class=\\\"player_id_css\\\">1015 <\\/i>\",\"name\":\"Boy1 name \"},{\"flag\":\"<i class=\\\"player_id_css\\\"> <\\/i>\",\"name\":\"test3 3434 \"}],[{\"flag\":\" \",\"name\":\"\"},{\"flag\":\"<i class=\\\"player_id_css\\\">5656ffffff <\\/i>\",\"name\":\"first name sf333 \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"flag\":\"<i class=\\\"player_id_css\\\">1015 <\\/i>\",\"name\":\"Boy1 name \"},{\"flag\":\"<i class=\\\"player_id_css\\\">1016 <\\/i>\",\"name\":\"Boy2 name \"}]],\"results\":[[[[\"11\",\"9\"],[\"2\",\"3\"],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[\"9\",\"14\"]],[[\"12\",\"22\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"]],[[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"]],[[\"\",\"\"],[\"\",\"\"]],[[\"\",\"\"]]]]}', '2016-09-26 11:26:40'),
(3, 13, 75, '{\"teams\":[[{\"flag\":\"<i class=\\\"player_id_css\\\">1015 <\\/i>\",\"name\":\"Boy1 name \"},{\"flag\":\"<i class=\\\"player_id_css\\\">1016 <\\/i>\",\"name\":\"Boy2 name \"}],[{\"flag\":\"<i class=\\\"player_id_css\\\">1017 <\\/i>\",\"name\":\"Boy3 last name \"},{\"flag\":\"<i class=\\\"player_id_css\\\">1019 <\\/i>\",\"name\":\"Boy4 sf333 \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}]],\"results\":[[[[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}]],[[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"]],[[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"]],[[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"]],[[\"\",\"\"],[\"\",\"\"]],[[\"\",\"\"]]]]}', '2016-07-06 07:18:07'),
(4, 14, 81, '{\"teams\":[[{\"flag\":\" \",\"name\":\"t\"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}]],\"results\":[[[[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}]],[[\"\",\"\"]]]]}', '2016-09-26 11:12:12'),
(5, 92, 429, '{\"teams\":[[{\"flag\":\" \",\"name\":\"\"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}]],\"results\":[[[[\"0\",{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}]],[[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"]],[[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"]],[[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"]],[[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"]],[[\"\",\"\"],[\"\",\"\"]],[[\"\",\"\"]]]]}', '2017-01-22 10:35:55'),
(6, 97, 439, '{\"teams\":[[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}]],\"results\":[[[[\"1\",\"0\"],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}]],[[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"]],[[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"]],[[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"]],[[\"\",\"\"],[\"\",\"\"]],[[\"\",\"\"]]]]}', '2017-02-10 08:30:46'),
(8, 105, 465, '{\"teams\":[[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}]],\"results\":[[[[\"0\",\"0\"],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}],[{\"name\":\" \",\"flag\":\" \"},{\"name\":\" \",\"flag\":\" \"}]],[[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"]],[[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"]],[[\"\",\"\"],[\"\",\"\"],[\"\",\"\"],[\"\",\"\"]],[[\"\",\"\"],[\"\",\"\"]],[[\"\",\"\"]]]]}', '2017-02-25 10:06:58'),
(12, 92, 457, '{\"teams\":[[{\"flag\":\"<i class=\\\"player_id_css\\\">415295 <\\/i>\",\"name\":\"ARYA  RAJATH RAJ \"},{\"flag\":\"<i class=\\\"player_id_css\\\">419882 <\\/i>\",\"name\":\"MANAS GIRISH \"}],[{\"flag\":\"<i class=\\\"player_id_css\\\">419977 <\\/i>\",\"name\":\"YU  KINOSHITA \"},{\"flag\":\"<i class=\\\"player_id_css\\\">423054 <\\/i>\",\"name\":\"AMEETESH  SAXENA \"}]],\"results\":[[[[\"7\",\"6\"],[\"5\",\"6\"]],[[\"\",\"\"]]]]}', '2017-03-10 13:09:44');

-- --------------------------------------------------------

--
-- Table structure for table `main_menu`
--

CREATE TABLE `main_menu` (
  `menu_id` bigint(20) NOT NULL,
  `menu_name` varchar(1000) NOT NULL,
  `menu_order_no` smallint(6) NOT NULL,
  `menu_link` text NOT NULL,
  `menu_front_view_status` enum('P','A','N') NOT NULL DEFAULT 'P' COMMENT 'P=Pending, A=Active, N=None',
  `menu_created_at` datetime NOT NULL,
  `menu_modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `menu_active_status` enum('A','I') NOT NULL DEFAULT 'A' COMMENT 'A=Active, I=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_menu`
--

INSERT INTO `main_menu` (`menu_id`, `menu_name`, `menu_order_no`, `menu_link`, `menu_front_view_status`, `menu_created_at`, `menu_modified_at`, `menu_active_status`) VALUES
(1, 'Home', 1, 'Home', 'P', '2016-09-26 13:11:57', '2016-09-26 11:11:57', 'A'),
(2, 'About', 2, 'About', 'P', '2016-09-26 13:12:20', '2016-09-26 11:12:20', 'A'),
(3, 'Coaching', 3, '', 'P', '2018-03-07 07:54:13', '2018-03-07 13:54:13', 'A'),
(4, 'Calendar', 4, 'calander', 'P', '2018-08-28 00:48:31', '2018-08-28 05:48:31', 'A'),
(6, 'Facilities', 6, '', 'P', '2017-07-27 00:23:48', '2017-07-27 05:23:48', 'A'),
(7, 'Contact', 8, 'contactus', 'P', '2017-02-11 03:16:25', '2017-02-11 09:16:25', 'A'),
(8, 'Affilated Clubs', 7, 'affilated_clubs', 'P', '2016-10-24 12:24:37', '2016-10-24 17:24:37', 'A'),
(10, 'News', 5, 'news', 'P', '2016-10-25 05:33:53', '2016-10-25 10:33:53', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `main_sub_menu`
--

CREATE TABLE `main_sub_menu` (
  `sub_menu_id` bigint(20) NOT NULL,
  `menu_id` bigint(20) NOT NULL,
  `sub_menu_name` varchar(1000) NOT NULL,
  `sub_menu_order_no` smallint(6) NOT NULL,
  `sub_menu_link` text NOT NULL,
  `sub_menu_visible_status` enum('P','A','N') NOT NULL DEFAULT 'P' COMMENT 'P=Pending, A=Active, N=None',
  `sub_menu_created_at` datetime NOT NULL,
  `sub_menu_modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sub_menu_status` enum('A','I') NOT NULL COMMENT 'A=Active, I=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_sub_menu`
--

INSERT INTO `main_sub_menu` (`sub_menu_id`, `menu_id`, `sub_menu_name`, `sub_menu_order_no`, `sub_menu_link`, `sub_menu_visible_status`, `sub_menu_created_at`, `sub_menu_modified_at`, `sub_menu_status`) VALUES
(1, 2, 'KSLTA', 1, 'aboutkslta', 'P', '2016-10-22 02:36:48', '2016-10-22 07:36:48', 'A'),
(2, 2, 'commitee', 3, 'commitee', 'P', '2016-10-22 02:26:30', '2016-10-22 07:26:30', 'A'),
(3, 3, 'Regular Coaching', 1, 'coaching/regular', 'P', '2016-10-22 02:26:40', '2016-10-22 07:26:40', 'A'),
(4, 3, 'Summer Coaching  - Swimming', 3, 'summer_coaching_camp_swimming', 'P', '2019-03-14 04:49:50', '2019-03-14 09:49:50', 'A'),
(6, 5, 'Air Asia Open', 1, 'tournament/18', 'P', '2016-09-26 13:21:14', '2016-10-21 14:39:25', 'A'),
(7, 5, 'RTNMC FEB 2016', 2, 'rtnmc', 'P', '2016-09-26 13:21:52', '2016-09-26 11:21:52', 'A'),
(8, 6, 'Bar & Restaurant', 1, 'barrestaurant', 'P', '2016-10-22 02:39:39', '2016-10-22 07:39:39', 'A'),
(9, 6, 'Party Halls', 2, 'partyhall', 'P', '2016-10-24 06:06:43', '2016-10-24 11:06:43', 'A'),
(10, 6, 'Guest Room', 3, 'guestroom', 'P', '2016-10-24 06:07:10', '2016-10-24 11:07:10', 'A'),
(11, 6, 'Swimming Pool', 4, 'swimming_pool', 'P', '2017-07-22 05:16:08', '2017-07-22 10:16:08', 'A'),
(13, 6, 'Table Tennis & Billiards', 7, 'table_tennis', 'P', '2017-07-22 05:28:59', '2017-07-22 10:28:59', 'A'),
(14, 2, 'cubbon-park-bangalore', 2, 'cubbonpark_bangalore', 'P', '2016-10-24 12:36:55', '2016-10-24 17:36:55', 'A'),
(18, 6, 'Tennis', 5, 'tennis_timing_charges', 'P', '2016-10-24 07:08:59', '2016-10-24 12:08:59', 'A'),
(19, 6, 'HEALTH CLUB', 6, 'healthclub', 'P', '2017-07-27 03:03:09', '2017-07-27 08:03:09', 'A'),
(21, 12, 'Tennis Camp', 1, 'Tennissummercamp', 'P', '2017-03-08 02:05:27', '2017-03-08 08:05:27', 'A'),
(22, 3, 'Summer Coaching - Tennis', 2, 'summer_coaching_camp_tennis', 'P', '2019-03-14 05:14:04', '2019-03-14 10:14:04', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1459344856),
('m130524_201442_init', 1459344861);

-- --------------------------------------------------------

--
-- Table structure for table `rights`
--

CREATE TABLE `rights` (
  `id` int(11) NOT NULL,
  `rights_list` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `state_id` int(11) NOT NULL,
  `state_name` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`state_id`, `state_name`) VALUES
(1, 'Andaman and Nicobar Island'),
(2, 'Andhra Pradesh'),
(3, 'Arunachal Pradesh'),
(4, 'Assam'),
(5, 'Bihar'),
(6, 'Chandigarh'),
(7, 'Chhattisgarh'),
(8, 'Dadra and Nagar Haveli'),
(9, 'Daman and Diu'),
(10, 'Delhi'),
(11, 'Goa'),
(12, 'Gujarat'),
(13, 'Haryana'),
(14, 'Himachal Pradesh'),
(15, 'Jammu and Kashmir'),
(16, 'Jharkhand'),
(17, 'Karnataka'),
(18, 'Kerala'),
(19, 'Lakshadweep'),
(20, 'Madhya Pradesh'),
(21, 'Maharashtra'),
(22, 'Manipur'),
(23, 'Meghalaya'),
(24, 'Mizoram'),
(25, 'Nagaland'),
(26, 'Odisha'),
(27, 'Puducherry'),
(28, 'Punjab'),
(29, 'Rajasthan'),
(30, 'Sikkim'),
(31, 'Tamil Nadu'),
(32, 'Telangana'),
(33, 'Tripura'),
(34, 'Uttar Pradesh'),
(35, 'Uttarakhand'),
(36, 'West Bengal');

-- --------------------------------------------------------

--
-- Table structure for table `tournament_schedule`
--

CREATE TABLE `tournament_schedule` (
  `id` bigint(20) NOT NULL,
  `tour_name` varchar(255) NOT NULL,
  `tour_url` varchar(100) NOT NULL,
  `order_no` bigint(12) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tournament_schedule`
--

INSERT INTO `tournament_schedule` (`id`, `tour_name`, `tour_url`, `order_no`, `created_at`) VALUES
(1, 'ATP Challenger Bengaluru Open-2020', '404', 1, '2019-12-26 08:12:31'),
(3, 'Asian Tennis Federation U-14 Boys & Girls ', '393', 3, '2019-12-26 08:12:27'),
(4, 'Men\'s 1 Lac KSLTA', '405', 2, '2020-01-08 07:01:34');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `user_type` enum('A','U','P') COLLATE utf8_unicode_ci NOT NULL COMMENT 'A=Admin, U=User, P=Player',
  `city` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `rights` text COLLATE utf8_unicode_ci,
  `status_flag` enum('A','I') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A' COMMENT 'A=Active, I=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `first_name`, `last_name`, `dob`, `user_type`, `city`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `rights`, `status_flag`) VALUES
(1, 'admin', 'Admin', 'KSLA', '2016-04-06', 'A', 'Bangalore', '8nAQquRZCfOFOdOngHc2lEhhJ5brco1t', '$2y$13$04h5Rr1SHyTvNkgGM0ot6OMWvkv9eHRZjVO..ZYRfZvQM3wQHa6tS', NULL, 'vinothviv@gmail.com', 10, 1459345080, 1459345080, '', 'A'),
(10, 'kslatest', 'Mis', 'M', '0000-00-00', 'A', 'test', '5qGRX7cC7N1esSWr0H_GCYKpEtXcSKvs', 'testtest', NULL, 'test@test.comm', 10, 1460549473, 1460549473, '', 'A'),
(17, 'balaji', 'Balaji', 'udayakumar', '2016-03-08', 'A', 'chennai', 'TunQ0xFcteadcr0IzGKytZSeocS_qqTT', '$2y$13$D2OHX9KQ2Dk3N.aa/Qi5X.MisXshwiFE9EkIWjPBxBXxw0/WK9yuO', NULL, 'balaji@yahoo.com', 10, 1460555753, 1460555753, '', 'A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminusers`
--
ALTER TABLE `adminusers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- Indexes for table `affilatedclubs`
--
ALTER TABLE `affilatedclubs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dropdown_management`
--
ALTER TABLE `dropdown_management`
  ADD PRIMARY KEY (`dropdown_id`);

--
-- Indexes for table `filesupload`
--
ALTER TABLE `filesupload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `footer_menu`
--
ALTER TABLE `footer_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guestroombooking`
--
ALTER TABLE `guestroombooking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `headlines`
--
ALTER TABLE `headlines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homepage_news`
--
ALTER TABLE `homepage_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homepage_scroll_news`
--
ALTER TABLE `homepage_scroll_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_facilities`
--
ALTER TABLE `home_facilities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_notification`
--
ALTER TABLE `home_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ksl_coaching_pages`
--
ALTER TABLE `ksl_coaching_pages`
  ADD PRIMARY KEY (`coaching_id`);

--
-- Indexes for table `ksl_event`
--
ALTER TABLE `ksl_event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ksl_event_drawsheet`
--
ALTER TABLE `ksl_event_drawsheet`
  ADD PRIMARY KEY (`draw_id`);

--
-- Indexes for table `ksl_event_matches`
--
ALTER TABLE `ksl_event_matches`
  ADD PRIMARY KEY (`em_id`);

--
-- Indexes for table `ksl_event_tbl`
--
ALTER TABLE `ksl_event_tbl`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `ksl_gallery_image_tbl`
--
ALTER TABLE `ksl_gallery_image_tbl`
  ADD PRIMARY KEY (`gal_image_id`);

--
-- Indexes for table `ksl_gallery_tbl`
--
ALTER TABLE `ksl_gallery_tbl`
  ADD PRIMARY KEY (`gallery_id`);

--
-- Indexes for table `ksl_gallery_title`
--
ALTER TABLE `ksl_gallery_title`
  ADD PRIMARY KEY (`tit_id`);

--
-- Indexes for table `ksl_images_view`
--
ALTER TABLE `ksl_images_view`
  ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `ksl_newsletter_online`
--
ALTER TABLE `ksl_newsletter_online`
  ADD PRIMARY KEY (`newsletter_id`);

--
-- Indexes for table `ksl_news_tbl`
--
ALTER TABLE `ksl_news_tbl`
  ADD PRIMARY KEY (`news_id`);

--
-- Indexes for table `ksl_notification`
--
ALTER TABLE `ksl_notification`
  ADD PRIMARY KEY (`notify_id`);

--
-- Indexes for table `ksl_page_contents`
--
ALTER TABLE `ksl_page_contents`
  ADD PRIMARY KEY (`page_content_id`);

--
-- Indexes for table `ksl_player_events`
--
ALTER TABLE `ksl_player_events`
  ADD PRIMARY KEY (`ue_autoid`);

--
-- Indexes for table `ksl_player_register`
--
ALTER TABLE `ksl_player_register`
  ADD PRIMARY KEY (`reg_id`);

--
-- Indexes for table `ksl_player_register_tbl`
--
ALTER TABLE `ksl_player_register_tbl`
  ADD PRIMARY KEY (`reg_id`);

--
-- Indexes for table `ksl_slider_banner`
--
ALTER TABLE `ksl_slider_banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `ksl_tournaments_event_matche`
--
ALTER TABLE `ksl_tournaments_event_matche`
  ADD PRIMARY KEY (`tem_id`);

--
-- Indexes for table `ksl_tournaments_event_result`
--
ALTER TABLE `ksl_tournaments_event_result`
  ADD PRIMARY KEY (`ter_id`);

--
-- Indexes for table `ksl_tournaments_tbl`
--
ALTER TABLE `ksl_tournaments_tbl`
  ADD PRIMARY KEY (`tourn_id`);

--
-- Indexes for table `ksl_tournament_gallery`
--
ALTER TABLE `ksl_tournament_gallery`
  ADD PRIMARY KEY (`tourn_gallery_id`);

--
-- Indexes for table `ksl_tournament_playerlists`
--
ALTER TABLE `ksl_tournament_playerlists`
  ADD PRIMARY KEY (`playerlists_id`);

--
-- Indexes for table `ksl_tournament_schedule`
--
ALTER TABLE `ksl_tournament_schedule`
  ADD PRIMARY KEY (`ts_id`);

--
-- Indexes for table `main_menu`
--
ALTER TABLE `main_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `main_sub_menu`
--
ALTER TABLE `main_sub_menu`
  ADD PRIMARY KEY (`sub_menu_id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `rights`
--
ALTER TABLE `rights`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `tournament_schedule`
--
ALTER TABLE `tournament_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adminusers`
--
ALTER TABLE `adminusers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `affilatedclubs`
--
ALTER TABLE `affilatedclubs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `dropdown_management`
--
ALTER TABLE `dropdown_management`
  MODIFY `dropdown_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `filesupload`
--
ALTER TABLE `filesupload`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=299;

--
-- AUTO_INCREMENT for table `footer_menu`
--
ALTER TABLE `footer_menu`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `guestroombooking`
--
ALTER TABLE `guestroombooking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `headlines`
--
ALTER TABLE `headlines`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `homepage_news`
--
ALTER TABLE `homepage_news`
  MODIFY `id` bigint(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `home_facilities`
--
ALTER TABLE `home_facilities`
  MODIFY `id` bigint(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `home_notification`
--
ALTER TABLE `home_notification`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ksl_coaching_pages`
--
ALTER TABLE `ksl_coaching_pages`
  MODIFY `coaching_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `ksl_event`
--
ALTER TABLE `ksl_event`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ksl_event_drawsheet`
--
ALTER TABLE `ksl_event_drawsheet`
  MODIFY `draw_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `ksl_event_matches`
--
ALTER TABLE `ksl_event_matches`
  MODIFY `em_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `ksl_event_tbl`
--
ALTER TABLE `ksl_event_tbl`
  MODIFY `event_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1481;

--
-- AUTO_INCREMENT for table `ksl_gallery_image_tbl`
--
ALTER TABLE `ksl_gallery_image_tbl`
  MODIFY `gal_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=394;

--
-- AUTO_INCREMENT for table `ksl_gallery_tbl`
--
ALTER TABLE `ksl_gallery_tbl`
  MODIFY `gallery_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `ksl_gallery_title`
--
ALTER TABLE `ksl_gallery_title`
  MODIFY `tit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ksl_images_view`
--
ALTER TABLE `ksl_images_view`
  MODIFY `image_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `ksl_newsletter_online`
--
ALTER TABLE `ksl_newsletter_online`
  MODIFY `newsletter_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ksl_news_tbl`
--
ALTER TABLE `ksl_news_tbl`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184;

--
-- AUTO_INCREMENT for table `ksl_notification`
--
ALTER TABLE `ksl_notification`
  MODIFY `notify_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ksl_page_contents`
--
ALTER TABLE `ksl_page_contents`
  MODIFY `page_content_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `ksl_player_events`
--
ALTER TABLE `ksl_player_events`
  MODIFY `ue_autoid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `ksl_player_register`
--
ALTER TABLE `ksl_player_register`
  MODIFY `reg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `ksl_player_register_tbl`
--
ALTER TABLE `ksl_player_register_tbl`
  MODIFY `reg_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ksl_slider_banner`
--
ALTER TABLE `ksl_slider_banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `ksl_tournaments_event_matche`
--
ALTER TABLE `ksl_tournaments_event_matche`
  MODIFY `tem_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `ksl_tournaments_event_result`
--
ALTER TABLE `ksl_tournaments_event_result`
  MODIFY `ter_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `ksl_tournaments_tbl`
--
ALTER TABLE `ksl_tournaments_tbl`
  MODIFY `tourn_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=426;

--
-- AUTO_INCREMENT for table `ksl_tournament_gallery`
--
ALTER TABLE `ksl_tournament_gallery`
  MODIFY `tourn_gallery_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `ksl_tournament_playerlists`
--
ALTER TABLE `ksl_tournament_playerlists`
  MODIFY `playerlists_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ksl_tournament_schedule`
--
ALTER TABLE `ksl_tournament_schedule`
  MODIFY `ts_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `main_menu`
--
ALTER TABLE `main_menu`
  MODIFY `menu_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `main_sub_menu`
--
ALTER TABLE `main_sub_menu`
  MODIFY `sub_menu_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `rights`
--
ALTER TABLE `rights`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `tournament_schedule`
--
ALTER TABLE `tournament_schedule`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
