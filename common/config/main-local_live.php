<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost:3307;dbname=kslta_cms',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
         'db2' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost:3307;dbname=kslta_admin',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
    ],
];
