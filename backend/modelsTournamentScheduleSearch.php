<?php

namespace backend;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TournamentSchedule;

/**
 * modelsTournamentScheduleSearch represents the model behind the search form about `backend\models\TournamentSchedule`.
 */
class modelsTournamentScheduleSearch extends TournamentSchedule
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ts_id', 'ts_tournament_id', 'ts_event_id', 'ts_player_id', 'ts_player_order'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TournamentSchedule::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ts_id' => $this->ts_id,
            'ts_tournament_id' => $this->ts_tournament_id,
            'ts_event_id' => $this->ts_event_id,
            'ts_player_id' => $this->ts_player_id,
            'ts_player_order' => $this->ts_player_order,
        ]);

        return $dataProvider;
    }
}
