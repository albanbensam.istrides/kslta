<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Filesupload */

$this->title = 'Add Files';

$this->params['breadcrumbs'][] = ['label' => 'Filesuploads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filesupload-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
