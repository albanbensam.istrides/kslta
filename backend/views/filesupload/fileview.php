<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="col-md-12">
  
    <ul class="timeline">
       
        <li>
         <div class="timeline-item">

                
               
                <div class="timeline-body">
                	<div>
                	<label>File Name:</label>
                	
               <?=  strip_tags($model->filename);	?>
               </div>
               <div>
                	<label>File Link:</label>
               <?=  strip_tags($model->filelink);	?>
               </div>
    			
                </div>
                <div class="timeline-footer">
                                       
					<a class="btn btn-default btn-xs"  data-pjax="0" data-method="post" data-confirm="Are you sure you want to delete this item?" aria-label="Delete" title="Delete" href="<?php echo Yii::$app->homeUrl;  ?>?r=filesupload%2Fdelete&id= <?= $model->id;  ?>" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></a>
                    <a class="btn btn-warning btn-xs" href="<?= Yii::$app->urlManager->createUrl(['filesupload/update', 'id' => $model->id]); ?>" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>  
				</div>
			</div>
</div>
