<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FilesuploadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Media file Upload';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filesupload-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(' Add New File', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
       /* 'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(Html::encode($model->id), ['view', 'id' => $model->id]);
        },*/
        'itemView'=>'fileview',
    ]) ?>
</div>
