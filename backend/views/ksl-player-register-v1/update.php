<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\KslPlayerRegisterV1 */

$this->title = 'Update Ksl Player Register V1: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ksl Player Register V1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ksl-player-register-v1-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
