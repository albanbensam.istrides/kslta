<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\KslPlayerRegisterV1 */

$this->title = "Player Name - ".$model->first_name." ".$model->last_name;
//$this->params['breadcrumbs'][] = ['label' => 'Ksl Player Register V1s', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box-body">
    <div class="box box-primary cgridoverlap">
                <div class="box-header with-border">

     <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
 
    
    </div><!-- /.box-header -->
                  <div class="box-body">

    <p>
        <?= Html::a('Reject', ['updatestatus', 'id' => $model->id,'status'=>'Reject'], ['class' => 'btn btn-danger']) ?>
        <?= Html::a('Hold', ['updatestatus', 'id' => $model->id,'status'=>'Hold'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Approve', ['updatestatus', 'id' => $model->id,'status'=>'Approved'], ['class' => 'btn btn-success']) ?>
        
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'first_name',
            'last_name',
            'dob',
            'place_of_birth',
            'father_name',
            'mother_name',
            'guardian_details',
            'address:ntext',
            'contact_no',
            'email_id:email',
            'active_status',
            'created_at',
           // 'updated_at',
        ],
    ]) ?>

</div>
</div>
</div>