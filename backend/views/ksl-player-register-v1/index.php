<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\KslPlayerRegisterV1Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Player Register';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ksl-tournaments-tbl-index">
    <div class="box-body">
    <div class="box box-primary cgridoverlap">
                <div class="box-header with-border">

    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

    
    </div><!-- /.box-header -->
                  <div class="box-body">
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'first_name',
            'last_name',
            'dob',
            'place_of_birth',
             'father_name',
            // 'mother_name',
            // 'guardian_details',
             'address:ntext',
             'contact_no',
             'email_id:email',
             'active_status',
            // 'created_at',
            // 'updated_at',

            //['class' => 'yii\grid\ActionColumn'],

            ['class' => 'yii\grid\ActionColumn',

              'header'=> 'Action',
               'template'=>'{view}',
                 'headerOptions' => ['style' => 'width:150px;color:#337ab7;'],
                'buttons'=>[
                  'view' => function ($url, $model, $key) {
                       $url = Url::to(['ksl-player-register-v1/viewdata', 'id' => ($model -> id)]);     
                       return Html::a('View', $url, ['class' => 'btn btn-xs btn-success']);
                    }, 
              ]
             ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
</div>
</div>
</div>
</div>
