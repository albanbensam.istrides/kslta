<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\KslPlayerRegisterV1 */

$this->title = 'Create Ksl Player Register V1';
$this->params['breadcrumbs'][] = ['label' => 'Ksl Player Register V1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-player-register-v1-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
