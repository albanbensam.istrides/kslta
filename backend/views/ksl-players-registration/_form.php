<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\DropdownManagement;
use backend\models\KslTournamentsTbl;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\KslPlayersRegistration */
/* @var $form yii\widgets\ActiveForm */
?>

                  <div class="box-body">
<div class="ksl-players-registration-form">
	
<div class='col-lg-6'>
    <?php $form = ActiveForm::begin(); ?>
    
                <?= Html::activeDropDownList($model, 'tourn_id',
      ArrayHelper::map(KslTournamentsTbl::find()->all(), 'tourn_id', 'tourn_title'), ['class' => 'form-control']) ?>


    <?= $form->field($model, 'player_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tourn_id')->textInput() ?>

    <?= $form->field($model, 'player_category')->textInput() ?>

    <?= $form->field($model, 'register_id')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-lg-6'>
    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mobile')->textInput() ?>

    <?= $form->field($model, 'registration_date')->textInput(['class' => 'datepick form-control']) ?>

    <?= $form->field($model, 'tournament_date')->textInput(['class' => 'datepick form-control']) ?>


    <div class="box-footer form-group pull-right">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
</div>
</div>
<script>
	$('.datepick').datepicker({
		
		 autoclose: true
		
	});
	
</script>