<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\KslPlayersRegistration */

$this->title = 'Update Players Registration:';
//$model->reg_id $model->reg_id
$this->params['breadcrumbs'][] = ['label' => 'Players Registrations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'View', 'url' => ['view', 'id' => $model->reg_id]];
$this->params['breadcrumbs'][] = 'Update';

?>
<div class="ksl-players-registration-update">

 
   
<div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div><!-- /.box-header -->
                <!-- form start -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>