<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\KslPlayersRegistration */

$this->title = 'Create Players Registration';
$this->params['breadcrumbs'][] = ['label' => 'Players Registrations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-players-registration-create">

   
<div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
             
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
</div>
