<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\KslPlayersRegistrationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

 use kartik\datecontrol\Module;
use kartik\datecontrol\DateControl;

$this->title = 'Players Registrations';
$this->params['breadcrumbs'][] = $this->title;
?>
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/datatables/dataTables.bootstrap.css">

<script src="<?php echo Url::base(); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>


<div class="ksl-players-registration-index">
<div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div><!-- /.box-header -->
                  <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> Add Player', ['create'], ['class' => 'btn btn-primary btn-md pull-right ']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'reg_id',
            'player_name',
            'tourn_id',
            'player_category',
            'register_id',
            // 'city',
            // 'mobile',
            // 'registration_date',
            // 'tournament_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div></div></div>
 <script>
    
       $('table').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });  
     
    
    </script>
