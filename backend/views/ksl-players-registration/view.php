<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\KslPlayersRegistration */

$this->title = 'View Players Registration';
//$model->reg_id;
$this->params['breadcrumbs'][] = ['label' => 'Ksl Players Registrations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-players-registration-view">
<div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
<!--    <h1><?= Html::encode($this->title) ?></h1>-->
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->reg_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->reg_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'reg_id',
            'player_name',
            'tourn_id',
            'player_category',
            'register_id',
            'city',
            'mobile',
            'registration_date',
            'tournament_date',
        ],
    ]) ?>

</div>
</div>