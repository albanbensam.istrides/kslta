<?php $menu_data_array=array(); 
		  $menu_data_array[0]=array('one','Dashboard',Yii::$app->homeUrl,'<i class="fa fa-dashboard"></i>','index');
		  
		  $menu_data_array[1]=array('more','Users','-','<i class="fa fa-user"></i>','rights');
		  $menu_data_array[1]['sub'][0]=array('Rights Management',Yii::$app->homeUrl.'?r=rights','<i class="fa fa-fw fa-check-circle-o"></i>','rights','');
		  $menu_data_array[1]['sub'][1]=array('User Management',Yii::$app->homeUrl.'?r=userdetails','<i class="fa fa-fw fa-street-view"></i>','rights','');
		  $menu_data_array[2]=array('one','Dropdown Management',Yii::$app->homeUrl.'?r=dropdown-management','<i class="fa fa-fw fa-sliders"></i>','dropdown-management');

		  $menu_data_array[3]=array('more','Menu Management','-','<i class="fa fa-fw fa-list"></i>','main-menu');
		  $menu_data_array[3]['sub'][0]=array('Add Main Menu',Yii::$app->homeUrl.'?r=main-menu/create','<i class="fa fa-fw fa-plus-square"></i>','main-menu','create');
		  $menu_data_array[3]['sub'][1]=array('Main Menu Manage',Yii::$app->homeUrl.'?r=main-menu','<i class="fa fa-fw fa-list-ol"></i>','main-menu','index');
		   $menu_data_array[3]['sub'][2]=array('Add Sub Menu',Yii::$app->homeUrl.'?r=sub-menu/create','<i class="fa fa-fw fa-plus-square"></i>','sub-menu','create');
		  $menu_data_array[3]['sub'][3]=array('Sub Menu Manage',Yii::$app->homeUrl.'?r=sub-menu','<i class="fa fa-fw fa-align-left"></i>','sub-menu','sub-menu');


		  $menu_data_array[4]=array('more','General Pages Control','-','<i class="fa fa-fw fa-file-o"></i>','page-contents');
		  $menu_data_array[4]['sub'][0]=array('Add Page',Yii::$app->homeUrl.'?r=page-contents/create','<i class="fa fa-fw fa-plus-square"></i>','page-contents','create');
		  $menu_data_array[4]['sub'][1]=array('Manage Pages',Yii::$app->homeUrl.'?r=page-contents','<i class="fa fa-fw fa-file-text-o"></i>','page-contents','index');
		  


		  $menu_data_array[5]=array('more','Tabs Pages Control','-','<i class="fa fa-fw fa-user-secret"></i>','coaching-pages');
		  $menu_data_array[5]['sub'][0]=array('Add Coaching Page',Yii::$app->homeUrl.'?r=coaching-pages/create','<i class="fa fa-fw fa-plus-square"></i>','coaching-pages','create');
		  $menu_data_array[5]['sub'][1]=array('Manage Coaching Pages',Yii::$app->homeUrl.'?r=coaching-pages','<i class="fa fa-fw fa-file-text-o"></i>','coaching-pages','index');
		 
		$menu_data_array[6]=array('one','Image Control',Yii::$app->homeUrl.'?r=images-view','<i class="fa fa-fw fa-clone"></i>','images-view');
		   
		  
		  $menu_data_array[7]=array('more','KSLTA News Control','#','<i class="fa fa-fw fa-newspaper-o"></i>','ksl-news-tbl');
		   $menu_data_array[7]['sub'][0]=array('Add News',Yii::$app->homeUrl.'?r=ksl-news-tbl/create','<i class="fa fa-fw fa-calendar-plus-o"></i>','ksl-news-tbl','create');
		  $menu_data_array[7]['sub'][1]=array('Approved List',Yii::$app->homeUrl.'?r=ksl-news-tbl','<i class="fa fa-fw fa-check-square-o"></i>','ksl-news-tbl','index');
		  $menu_data_array[7]['sub'][2]=array('Un-approved List',Yii::$app->homeUrl.'?r=ksl-news-tbl/newsunapproved','<i class="fa fa-fw fa-exclamation-triangle"></i>','ksl-news-tbl','newsunapproved');


		  $menu_data_array[8]['sub'][3]=array('Rejected List',Yii::$app->homeUrl.'?r=ksl-news-tbl/newsrecjected','<i class="fa fa-fw fa-ban"></i>','ksl-news-tbl','ksl-news-tbl','newsrecjected');
		  $menu_data_array[8]=array('more','Gallery','#','<i class="fa fa-fw fa-photo"></i>','ksl-gallery-model');
		  $menu_data_array[8]['sub'][0]=array(' Add New Gallery',Yii::$app->homeUrl.'?r=ksl-gallery-model%2Fcreate','<i class="fa fa-fw fa-object-ungroup"></i>','ksl-gallery-model','create');
		  $menu_data_array[8]['sub'][1]=array(' Gallery Management',Yii::$app->homeUrl.'?r=ksl-gallery-model','<i class="fa fa-fw fa-file-photo-o"></i>','ksl-gallery-model','index');
		  $menu_data_array[8]['sub'][2]=array(' Home Page Gallery',Yii::$app->homeUrl.'?r=gallery-page%2Fcreate&id=3','<i class="fa fa-fw fa-file-photo-o"></i>','gallery-page','create');
		  $menu_data_array[8]['sub'][3]=array(' Home Page Gallery Title',Yii::$app->homeUrl.'?r=ksl-gallery-title%2Fview&id=1','<i class="fa fa-fw fa-file-photo-o"></i>','ksl-gallery-title','view');


		  $menu_data_array[9]=array('more','Tournament','#','<i class="fa fa-fw fa-trophy"></i>','ksl-tournaments-tbl');
		  $menu_data_array[9]['sub'][0]=array('Tournament Master',Yii::$app->homeUrl.'?r=ksl-tournaments-tbl','<i class="fa fa-fw fa-tags"></i>','ksl-tournaments-tbl','index');
		  $menu_data_array[9]['sub'][1]=array('Tournament News',Yii::$app->homeUrl.'?r=ksl-tournaments-tbl/tournamentnews','<i class="fa fa-fw fa-list-alt"></i>','ksl-tournaments-tbl','tournamentnews');


		  $menu_data_array[10]=array('one','Player\'s registration',Yii::$app->homeUrl.'?r=player-register','<i class="fa fa-fw fa-user-plus"></i>','players-registration');
		  
		  $menu_data_array[11]=array('one','Tournament schedule',Yii::$app->homeUrl.'?r=tournament-schedule','<i class="fa fa-fw fa-user-plus"></i>','tournament-schedule'); 
		  
		  $menu_data_array[12]=array('one','Slider Banners',Yii::$app->homeUrl.'?r=slider-banner','<i class="fa fa-fw fa-clone"></i>','slider-banner');
		  $menu_data_array[13]=array('one','Notification',Yii::$app->homeUrl.'?r=notification','<i class="fa fa-fw fa-calendar-check-o"></i>','');
		 
		 
		  $menu_data_array[14]=array('one','Guest Room Booking',Yii::$app->homeUrl.'?r=guestroombooking','<i class="fa fa-fw fa-file-o"></i>','');
		 
		 $menu_data_array[15]=array('one','Affilated Clubs',Yii::$app->homeUrl.'?r=affilatedclubs','<i class="fa fa-fw fa-file-o"></i>','');
		 
		 $menu_data_array[16]=array('one','Media Upload',Yii::$app->homeUrl.'?r=filesupload','<i class="fa fa-fw fa-sliders"></i>','filesupload');
		 
		 
		//  $menu_data_array[17]=array('one','Scrolling News',Yii::$app->homeUrl.'?r=homepage-news','<i class="fa fa-fw fa-sliders"></i>','homepage-news');
		  
		  
		  //  $menu_data_array[18]=array('one','Home Notification',Yii::$app->homeUrl.'?r=home-notification/index','<i class="fa fa-fw fa-bell"></i>','home-notification');
			
			  $menu_data_array[19]=array('one','Home Headlines',Yii::$app->homeUrl.'?r=headlines/index','<i class="fa fa-fw fa-sliders"></i>','headlines');
			  
			//  $menu_data_array[20]=array('one','Home Events',Yii::$app->homeUrl.'?r=event/index','<i class="fa fa-calendar" aria-hidden="true"></i>','event');
			  
			  $menu_data_array[21]=array('one','Home Facilities',Yii::$app->homeUrl.'?r=home-facilities/index','<i class="fa fa-object-ungroup" aria-hidden="true"></i>','home-facilities');
		 	
			$menu_data_array[22]=array('one','Home Tournament',Yii::$app->homeUrl.'?r=tournament/index','<i class="fa fa-calendar" aria-hidden="true"></i>','tournament');
			
			$menu_data_array[23]=array('one','Footer Menu',Yii::$app->homeUrl.'?r=footer-menu/index','<i class="fa fa-clone" aria-hidden="true"></i>','footer-menu');
			
						  $menu_data_array[24]=array('one','Academy Register',Yii::$app->homeUrl.'?r=ksl-academy/index','<i class="fa fa-object-ungroup" aria-hidden="true"></i>','home-facilities');
		 	
			$menu_data_array[25]=array('one','Coaches Register',Yii::$app->homeUrl.'?r=ksl-coaches-reg/index','<i class="fa fa-calendar" aria-hidden="true"></i>','tournament');
			
			$menu_data_array[26]=array('one','Player Register',Yii::$app->homeUrl.'?r=ksl-player-register-v1/index','<i class="fa fa-clone" aria-hidden="true"></i>','footer-menu');
		 
		
		  
		  // $menu_data_array[12]=array('one','Ranking','','<i class="fa fa-fw fa-cubes"></i> ','');
		  // $menu_data_array[13]=array('one','Online Forms','','<i class="fa fa-fw fa-globe"></i>','');
		  // $menu_data_array[14]=array('one','Coaching','','<i class="fa fa-fw fa-user-secret"></i>','');
		  // $menu_data_array[15]=array('one','Courts','','<i class="fa fa-fw fa-sort-numeric-asc"></i>','');
		  // $menu_data_array[16]=array('one','Approval','','<i class="fa fa-fw fa-check-square-o"></i>','');
		  // $menu_data_array[17]=array('one','Board Members','','<i class="fa fa-fw fa-users"></i>','');
		  // $menu_data_array[18]=array('one','Configuration','','<i class="fa fa-fw fa-cog"></i>',''); 
		 
		  
		  $html_menu_out='';		  
		  $controler_url_id=Yii::$app->controller->id;
		  $active_url_id=Yii::$app->controller->action->id;
		 // echo $controler_url_id."-".$active_url_id;die;
		  foreach($menu_data_array as $one_ig=>$one_menus){
		  	if(count($one_menus)>0){		  	
				if($one_menus[0]=='more'){
					$isselct='';
					if($controler_url_id==$one_menus[4]){$isselct='active';}
					 $html_menu_out.='<li class="treeview '.$isselct.'"><a href="#">'.$one_menus[3].' <span>'.$one_menus[1].'</span><i class="fa fa-angle-left pull-right"></i></a>';
					 $html_menu_out.='<ul class="treeview-menu">';					 
					foreach($one_menus['sub'] as $one_submenus){
						$isactive='';
						if($active_url_id==$one_submenus[4]){$isactive='class="active"';}
						 $html_menu_out.='<li '.$isactive.'><a href="'.$one_submenus[1].'">'.$one_submenus[2].''.$one_submenus[0].'</a></li>';
					}
					$html_menu_out.='</ul></li>';
				}elseif($one_menus[0]=='one'){
					$isselct='';
					if($controler_url_id==$one_menus[4]){$isselct='active';}
					 $html_menu_out.='<li class="treeview '.$isselct.'"> 
		              <a href="'.$one_menus[2].'">'.$one_menus[3].' <span>'.$one_menus[1].'</span></a></li>';
				}
				
		  	}
		  }
		   ?>
<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel 
          <div class="user-panel">
            <div class="pull-left image">
              <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>Alexander Pierce</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>-->
          <!-- search form 
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->          
          <ul class="sidebar-menu">
          	<?php echo $html_menu_out;  ?>           
           
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>