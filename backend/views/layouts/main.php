<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\DashboardAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\helpers\Url;
use yii\bootstrap\Model;


DashboardAsset::register($this);
 
   $session = Yii::$app->session;
 
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" type="image/png" href="<?php echo Url::to('@web/frontend/web/img/favicon.png');  ?>" />
    
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>

<!-- Grid view overflow style -->
 <style>
 .cgridoverlap{
      overflow-x: scroll;
 }
</style>

<div class="wrapper">
     <header class="main-header">
        <!-- Logo -->
        <a href="<?= Yii::$app->homeUrl ?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><img src="uploadimage/custom_images/favicon.png" width="45px" height="45px"> </span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><img src="uploadimage/custom_images/favicon.png" width="45px" height="45px"> <b>KSLTA</b> CMS</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">              
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo  Yii::$app->user->identity->username;  ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                    <p>
                     <?php echo  Yii::$app->user->identity->username;  ?>
                      
                    </p>
                  </li>
                  <!-- Menu Body -->
                    
                    
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href='<?php echo Yii::$app->homeUrl."?r=userdetails/view&id=".$session['kslta_id']; ?>' class="btn btn-default btn-flat"><i class="fa fa-fw fa-user"></i> Profile</a>
                    </div>
                    <div class="pull-right">                  
					<?php  if (Yii::$app->user->isGuest) {
					       echo  ['label' => 'Sign out', 'url' => ['/site/login']];
					    } else {
					        echo '<a>'
					            . Html::beginForm(['/site/logout'], 'post')
					            . Html::submitButton(
					                '<i class="fa fa-fw fa-sign-out"></i> Logout',
					                ['class' => 'btn btn-default btn-flat']
					            )
					            . Html::endForm()
					            . '</a>';
					    } ?>
                    </div>
                  </li>
                </ul>
              </li>
            
            </ul>
          </div>
        </nav>
      </header>
       
    <!-- Left side column. contains the logo and sidebar -->      
 	<?php  echo $this->render('left_menu.php'); ?>
      <!-- Content Wrapper. Contains page content -->
     
     <div class="content-wrapper">
     	<section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
          	<div class="col-md-12">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
        
    </div>
    </div>
    </section>
    
    </div>
     <?php $this->beginContent('@backend/views/layouts/fooder.php'); ?>
<?php $this->endContent(); ?>
</div>
<!--
	<footer class="footer">
	
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>-->

<?php 

 $this->endBody() ?>
 </body>
</html>
<?php $this->endPage() ?>
