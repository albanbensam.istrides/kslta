<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\PageContents */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="<?php echo Url::base(); ?>/plugins/tinymce/tinymce.min.js" type="text/javascript"></script>
<style>

  #blah{
  display: inline-block;
 
  width: 121px;
  height: 100px;
  background-repeat: no-repeat;
  background-position: center center;
  background-size: cover;
 

  }
  #imgInp{
    float: left;
  }

</style>
<section class="content">
<!-- Info boxes -->
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="page-content-form">
                    <div class="box-header with-border">
                     <h3 class="box-title"><?= $model->isNewRecord ? '<i class="fa fa-fw fa-sticky-note-o"></i>' : '<i class="fa fa-fw fa-sticky-note"></i>' ?> <?= Html::encode($this->title) ?></h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">

    <?php $form = ActiveForm::begin(
                           [
                           //'enableClientValidation' => false, 
                           'options' => ['enctype' => 'multipart/form-data']
                            ]); ?>

    <div class="col-md-12">
        <div class="col-md-6">
            <div class="form-group">
                <?= $form->field($model, 'page_content_title')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

         <div class="col-md-6">
            <div class="form-group">
                <?= $form->field($model, 'page_name')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
       
    </div>
     <div class="col-md-12">
        <div class="col-md-6">
            <div class="form-group">
                <?= $form->field($model, 'page_content_top_main_heading')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

         <div class="col-md-6">
            <div class="form-group">
                <?= $form->field($model, 'page_content_top_sub_heading')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
       
    </div>

    <div class="col-md-12">
        <div class="col-md-6">
            <div class="form-group">
                  <?= $form->field($model, 'page_content_heading')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

         <div class="col-md-4">
            <div class="form-group">
                <?= $form->field($model, 'page_content_img')->fileInput(['id'=> 'imgInp', 'class' => 'btn btn-primary']) ?>

            </div>
        </div>

        <div class="col-md-2">
           <img id="blah" class="profile-user-img img-responsive img-circle" src=<?php echo $model->isNewRecord ? "control_images/Photo-icon.png" : $model->page_content_img;  ?> />
           <br/><span style="margin-left:27px;">Image View</span>
        </div>
       
    </div>
    <div class="col-md-12">
    <div class="col-md-4">
            <div class="form-group">
                <?= $form->field($model, 'page_content_portfolio_img')->fileInput(['id'=> 'contentbanner', 'class' => 'btn btn-primary']) ?>

            </div>
        </div>
         <div class="col-md-2">
           <img id="blah" class="profile-user-img img-responsive img-circle" src=<?php echo $model->isNewRecord ? "control_images/Photo-icon.png" : $model->page_content_portfolio_img;  ?> />
           <br/><span style="margin-left:27px;">Image View</span>
        </div>
                 <div class="col-md-6">
          <?= $form->field($model, 'page_assign_sidebar')->checkbox(); ?>
          <?= $form->field($model, 'remove_content_img')->checkbox(); ?>
        </div>
        </div>

<div class="col-md-12">
        <div class="col-md-12">
            <div class="form-group" name = "PageContents[page_main_content]">
    <?= $form->field($model, 'page_main_content')->textarea(['name' => 'PageContents[page_main_content]', 'id'=>'pagecontents-page_main_content', 'class' => 'form-control contentEditor']) ?>
    </div>
        </div>
       
    </div>
<div class="col-md-12">
        <div class="col-md-6">
            <div class="form-group">
                <?= $form->field($model, 'page_meta_name')->textarea(['rows' => 3]) ?>
            </div>
        </div>

         <div class="col-md-6">
            <div class="form-group">
            <?= $form->field($model, 'page_meta_content')->textarea(['rows' => 3]) ?>
            </div>
        </div>
       
    </div>

  
    
    </div><!-- /.box-body -->
    <div class="box-footer">
                        <div class="form-group pull-right">
                            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
</div>
</div>
</div>
</div>
</section>





<script>
    /* Onchange Image View Start */
        function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).ready(function(){
        $("#imgInp").change(function(){
            readURL(this);
        });
    });

    /* Onchange Image View End */



    tinymce.init({
        selector: '.contentEditor',
        height: 250,
        theme: 'modern',
        valid_elements : '*[*]',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ],
        content_css: [
            '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            '//www.tinymce.com/css/codepen.min.css'
        ],
        setup: function(ed){
            ed.on("blur", function () {
                $("#" + ed.id).val(tinyMCE.activeEditor.getContent());
            });
        }
    }); 
</script>