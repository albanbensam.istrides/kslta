<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PageContentsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-contents-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'page_content_id') ?>

    <?= $form->field($model, 'page_content_title') ?>

    <?= $form->field($model, 'page_name') ?>

    <?= $form->field($model, 'page_content_heading') ?>

    <?= $form->field($model, 'page_main_content') ?>

    <?php // echo $form->field($model, 'page_content_img') ?>

    <?php // echo $form->field($model, 'page_content_created_at') ?>

    <?php // echo $form->field($model, 'page_content_modified_at') ?>

    <?php // echo $form->field($model, 'page_content_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
