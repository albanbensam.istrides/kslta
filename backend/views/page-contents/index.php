<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\PageContentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Page Contents';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .mygridcustom{ 
        height: 530px;
        overflow: scroll;

    }

    .timeline::before {
        background: #ECF0F5 none repeat scroll 0 0;
        border-radius: 2px;
        bottom: 0;
        content: "";
        left: -7px;
        margin: 0;
        position: absolute;
        top: 0;
        width: 4px;
    }

    .modal-header > h3{
        margin-top: 1px;
        margin-bottom: -6px;

    }
</style>
<div class="page-contents-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <p>
        <?php // Html::a('Create Page Contents', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
 -->
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        // 'itemView' => function ($model, $key, $index, $widget) {
        //     return Html::a(Html::encode($model->page_content_id), ['view', 'id' => $model->page_content_id]);
        // },
        'itemView' => 'pagecontentlisting',
    ]) ?>

</div>


 
  <?php 
            Modal::begin([
                    'header' => '<h2>Gallery Select</h2>',
                    'id' => 'modal', 
                    'size' => 'modal-md',

                ]);

            echo "<div id='modalContent'></div>";
            Modal::end();

     ?>
<script>
    
    // Configure/customize these variables.
    var showChar = 250;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Show more >";
    var lesstext = "Show less";


    $('.more').each(function () {
        var content = $(this).html();

        if (content.length > showChar) {

            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);

            var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';


            $(this).html(html);
        }

    });

    $(".morelink").click(function () {
        if ($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });



    $(document).ready(function(){
    $('.gallerydata').click(function(){

        $('#modal').modal('show').find('#modalContent').load($(this).attr('value'));
    });

    $('td > img').css('cursor','pointer');

    

        /* When Model is in Page the User Profile option will not come for avoid that bug the below code has been resolved it. */
        $('.dropdown-toggle').click(function(){
             $('.navbar-nav > .user ').toggleClass("open");
            $(".navbar-nav > .user > .dropdown-toggle").attr("aria-expanded","true");
        });
   
});



</script>