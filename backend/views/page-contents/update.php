<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PageContents */

$this->title = 'Update Page Contents';
$this->params['breadcrumbs'][] = ['label' => 'Page Contents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-contents-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
