<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\KslGalleryModel;
/* @var $this yii\web\View */
/* @var $model backend\models\SliderBanner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gallery-form">

    <?php $form = ActiveForm::begin(['id' => 'gallerybanner', 
    								//'options' => ['enctype' => 'multipart/form-data']
    								]); ?>


    <?php  $dataList=ArrayHelper::map(KslGalleryModel::find()->asArray()->all(), 'gallery_id', 'gallery_title'); ?>
   <?=$form->field($model, 'gallery_id')->dropDownList($dataList, 
         ['prompt' => '---Select---', 'class' => 'form-control']) ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
