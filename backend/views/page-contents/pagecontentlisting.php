<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<style>
	
	.morecontent span {
    display: none;
}
.morelink {
    display: block;
}
</style>
<div class="col-md-12">
    <!-- The time line -->
    <ul class="timeline">
        <!-- timeline time label -->
       
        <!-- /.timeline-label -->
        <!-- timeline item -->
        <li>
<!--             <i class="fa fa-newspaper-o bg-blue"></i> -->            <div class="timeline-item">
<!--                 <span class="time"><i class="fa fa-clock-o"></i> 12:05</span> -->
                <h3 class="timeline-header"><a href="<?= Yii::$app->urlManager->createUrl(['page-contents/view', 'id' => $model->page_content_id]); ?>"><?= $model->page_name; ?></a> </h3>
                <div class="timeline-body">
                	
    			<span class="more">
                    <?=  strip_tags($model->page_main_content); ?>
                 </span>
                </div>
                <div class="timeline-footer">
<!--                     <a class="btn btn-primary btn-xs newsreadmore" data-toggle="tooltip" title="Read more"><i class="fa fa-ellipsis-h"></i></a> -->                    
					<a class="btn btn-default btn-xs"  data-pjax="0" data-method="post" data-confirm="Are you sure you want to delete this item?" aria-label="Delete" title="Delete" href="<?php echo Yii::$app->homeUrl;  ?>?r=page-contents%2Fdelete&id= <?= $model->page_content_id;  ?>" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></a>
                    <a class="btn btn-warning btn-xs" href="<?= Yii::$app->urlManager->createUrl(['page-contents/update', 'id' => $model->page_content_id]); ?>" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
<!--                    <a data-pjax="0" data-method="post" data-confirm="Are you sure you want to delete this item?" aria-label="Delete" title="Delete" href="Url::toRoute('ksl-news-tbl/delete'.$model->news_id);  ">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>-->
                    <label> Posted by :</label> Admin
<!-- <a class="btn btn-danger btn-xs" data-toggle="tooltip" title="Rejected"><i class="fa fa-ban"></i></a>
 <a class="btn btn-success btn-xs" data-toggle="tooltip" title="Approved"><i class="fa fa-check"></i></a>-->
                   <div class="pull-right footerdata">

                     <label>
                          
                    
                        
                        <label class='unblock newsstatuschange'>
                         
                        <?= Html::button('<i class="fa fa-fw fa-image"></i> Page Gallery Manager', ['value' => Url::to(['page-contents/gallerymanager', 'id' => $model->page_content_id] ), 'class' => 'btn btn-block btn-primary btn-xs gallerydata', 'id' => $model->page_content_id]) ?>
                        </label>  
                    </div>
                </div>
            </div>
        </li>
        <!-- END timeline item -->
        <!-- timeline item -->
    </ul>
</div><!-- /.col -->


