<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\GuestroombookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Guestroombookings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guestroombooking-index">

    <div class="user-index">
    <div class="box-body">
    <div class="box box-primary cgridoverlap">
                <div class="box-header with-border">

    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div><!-- /.box-header -->
                  <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> Add Guestroombooking', ['create'], ['class' => 'btn btn-primary btn-md pull-right']) ?>

    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'name',
             'mobile',
            'checkindate',
            'checkoutdate',
            'adultcount',
            'childrencount',
            
           /* ['attribute' => 'status', 'format' => 'raw', 'value' => function($data) {
				if($data ->status=="Request"){
				return 'Requested';
				}elseif($data ->status=="Approval"){
				return 'Approved';				
				}else{
					return $data ->status;
				}
			}, ],*/
			
            // 'email:email',
             'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

</div>
</div>
</div>
</div>
