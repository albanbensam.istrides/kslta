<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Guestroombooking */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Guestroombookings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guestroombooking-view">
<!--
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>-->
    
<div class="main-menu-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'checkindate',
            'checkoutdate',
            'adultcount',
            'childrencount',
            'name',
            'mobile',
            'email:email',
            'status',
        ],
    ]) ?>

</div>
</div>
