<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Guestroombooking */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="guestroombooking-form">
	
	<section class="content">
<!-- Info boxes -->
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="page-content-form">
                    <div class="box-header with-border">
                     <h3 class="box-title"><?= $model->isNewRecord ? '<i class="fa fa-fw fa-sticky-note-o"></i>' : '<i class="fa fa-fw fa-sticky-note"></i>' ?> <?= Html::encode($this->title) ?></h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">

    <?php $form = ActiveForm::begin(); ?>

<div class="col-md-12">
	<div class="col-md-6">
   		 <?= $form->field($model, 'checkindate')->textInput(['id'=>'checkindate']) ?>
   		</div>
	<div class="col-md-6">
    	<?= $form->field($model, 'checkoutdate')->textInput(['id'=>'checkoutdate']) ?>
	</div>
</div>
<div class="col-md-12">
	<div class="col-md-6">
    	 <?php echo $form->field($model, 'adultcount')->dropDownList(['' => 'Select','1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5']); ?>   
</div>
	<div class="col-md-6">
    	 <?php echo $form->field($model, 'childrencount')->dropDownList(['' => 'Select','1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5']); ?>  

    	
	</div>
</div>
<div class="col-md-12">
	<div class="col-md-6">
    	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
</div>
	<div class="col-md-6">
    	<?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>
	</div>
</div>
<div class="col-md-12">
	<div class="col-md-6">
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
</div>
	<div class="col-md-6">
  	 
  	 <?php echo $form->field($model, 'status')->dropDownList(['' => 'Select','Canceled' => 'Cancel', 'Approvaled' => 'Approval']); ?>   

	</div>
</div>

    <div class="form-group pull-right">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

</div>
</div>
</div>
</div>
</section> 
<script>
	$("#checkindate").datepicker();
	$("#checkoutdate").datepicker();
           
       
</script>
