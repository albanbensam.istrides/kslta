<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Guestroombooking */

$this->title = 'Add Guestroombooking';
$this->params['breadcrumbs'][] = ['label' => 'Guestroombookings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guestroombooking-create">

  

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
