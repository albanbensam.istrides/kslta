<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Guestroombooking */

$this->title = 'Update Guestroombooking: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Guestroombookings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="guestroombooking-update">

   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
