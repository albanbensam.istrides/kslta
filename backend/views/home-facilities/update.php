<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\HomeFacilities */

$this->title = 'Update Home Facilities: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Home Facilities', 'url' => ['index']]; 
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="home-facilities-update">

  

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
