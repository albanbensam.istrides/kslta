<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\HomeFacilities */

$this->title = 'Create Home Facilities';
$this->params['breadcrumbs'][] = ['label' => 'Home Facilities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-facilities-create">

    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
