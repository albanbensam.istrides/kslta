<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\HomeFacilitiesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Home Facilities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="affilatedclubs-index">

    <div class="user-index">
    <div class="box-body">
    <div class="box box-primary cgridoverlap">
                <div class="box-header with-border">

    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div><!-- /.box-header -->
                  <div class="box-body"> 
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Add Facilities', ['create'], ['class' => 'btn btn-info']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

             
            'facility_name',
            'facility_url:url',
            'order_no',
            [
	            'attribute' => 'image_upload',
	            'format' => 'html',	            
	            'value' => function ($data) {
	                return Html::img($data->image_upload,
	                    ['width' => '60px']);
	            },
	        ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
</div>
</div>
</div>
</div>