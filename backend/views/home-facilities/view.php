<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\HomeFacilities */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Home Facilities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-facilities-view">

    <h1><?= Html::encode($this->title) ?></h1>

     

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'id',
            'facility_name',
            'facility_url:url',
            'order_no',
            
        ],
    ]) ?>

</div>
