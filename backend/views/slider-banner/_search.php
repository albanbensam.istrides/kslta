<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SliderBannerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="slider-banner-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'banner_id') ?>

    <?= $form->field($model, 'banner_name') ?>

    <?= $form->field($model, 'banner_url') ?>

    <?= $form->field($model, 'banner_image') ?>

    <?= $form->field($model, 'banner_order_flow') ?>

    <?php // echo $form->field($model, 'banner_date') ?>

    <?php // echo $form->field($model, 'banner_created_at') ?>

    <?php // echo $form->field($model, 'banner_modified_at') ?>

    <?php // echo $form->field($model, 'banner_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
