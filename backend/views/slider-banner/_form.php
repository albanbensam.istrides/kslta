<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SliderBanner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="slider-banner-form">

    <?php $form = ActiveForm::begin(['id' => 'sliderbanner', 
    								'options' => ['enctype' => 'multipart/form-data']
    								]); ?>

    <?= $form->field($model, 'banner_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'banner_url')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'banner_image')->fileInput(['id' => 'image']) ?>

    <?php // $form->field($model, 'banner_order_flow')->dropDownList([ '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5' ], ['prompt' => 'Select Order']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
