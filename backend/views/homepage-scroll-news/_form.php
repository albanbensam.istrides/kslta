<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\HomepageScrollNews */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="homepage-scroll-news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'sort_order')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'news')->textInput(['maxlength' => true]) ?>

    <?php //echo $form->field($model, 'status')->dropDownList([ 'A' => 'A', 'I' => 'I', ], ['prompt' => '']) ?>

    <?php //echo $form->field($model, 'created_at')->textInput() ?>

    <?php echo $form->field($model, 'created_by')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
