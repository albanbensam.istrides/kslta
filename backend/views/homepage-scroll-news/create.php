<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\HomepageScrollNews */

$this->title = 'Create Homepage Scroll News';
$this->params['breadcrumbs'][] = ['label' => 'Homepage Scroll News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="homepage-scroll-news-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
