<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\DropdownManagement */

$this->title = 'Add Dropdown';
$this->params['breadcrumbs'][] = ['label' => 'Dropdown Managements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dropdown-management-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
