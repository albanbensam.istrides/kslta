<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\DropdownManagementSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dropdown-management-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'dropdown_id') ?>

    <?= $form->field($model, 'dropdown_key') ?>

    <?= $form->field($model, 'dropdown_value') ?>

    <?= $form->field($model, 'dropdown_order') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
