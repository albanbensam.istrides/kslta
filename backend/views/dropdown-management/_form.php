<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\DropdownManagement;

/* @var $this yii\web\View */
/* @var $model backend\models\NasaCollaborations */
/* @var $form yii\widgets\ActiveForm */
?>
<section class="content">
<!-- Info boxes -->
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
            <div class="box-header with-border">
                     <h3 class="box-title"><?= $model->isNewRecord ? '<i class="fa fa-fw fa-calendar-plus-o"></i>' : '<i class="fa fa-fw fa-edit"></i>' ?>  <?= Html::encode($this->title) ?></h3>
              </div><!-- /.box-header -->
<div class="nasa-collaborations-form">
<div class="box-body">
    <?php $form = ActiveForm::begin(); ?>
			    <div class="col-md-12">
                        <div class="form-group col-md-6">
			    <?= $form->field($model, 'dropdown_key')->textInput(['maxlength' => true, 'placeholder' => 'Dropdown list Key']) ?>
			    	</div>
			    </div>

			    <div class="col-md-12">
                        <div class="form-group col-md-6">

			    <?= $form->field($model, 'dropdown_value')->textInput(['maxlength' => true, 'placeholder' => 'Dropdown list Value']) ?>
			    	</div>
			    </div>

			    <div class="col-md-12">
                        <div class="form-group col-md-6">

			    <?= $form->field($model, 'dropdown_order')->textInput(['maxlength' => true, 'placeholder' => 'Dropdown Order']) ?>
			    	</div>
			    </div>
			    <div class="box-footer pull-right form-group">
			        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			    </div>

			    <?php ActiveForm::end(); ?>


</div>

</div>
</div>
</div>
</div>
</section>
