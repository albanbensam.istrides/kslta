<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\DropdownManagement */

$this->title = $model->dropdown_id;
$this->params['breadcrumbs'][] = ['label' => 'Dropdown Managements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dropdown-management-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->dropdown_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->dropdown_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'dropdown_id',
            'dropdown_key',
            'dropdown_value',
            'dropdown_order',
        ],
    ]) ?>

</div>
