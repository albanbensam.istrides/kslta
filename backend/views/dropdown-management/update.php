<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\DropdownManagement */

$this->title = 'Update Dropdown';
$this->params['breadcrumbs'][] = ['label' => 'Dropdown Managements', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dropdown-management-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
