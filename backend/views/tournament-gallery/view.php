<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\TournamentGallery */

$this->title = $model->tourn_gallery_id;
$this->params['breadcrumbs'][] = ['label' => 'Tournament Galleries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tournament-gallery-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->tourn_gallery_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->tourn_gallery_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tourn_gallery_id',
            'tourn_id',
            'gallery_id',
            'tourn_gallery_date',
            'tourn_gallery_created_at',
            'tourn_gallery_modified_at',
            'tourn_gallery_status',
        ],
    ]) ?>

</div>
