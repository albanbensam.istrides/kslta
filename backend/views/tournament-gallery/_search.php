<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TournamentGallerySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tournament-gallery-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'tourn_gallery_id') ?>

    <?= $form->field($model, 'tourn_id') ?>

    <?= $form->field($model, 'gallery_id') ?>

    <?= $form->field($model, 'tourn_gallery_date') ?>

    <?= $form->field($model, 'tourn_gallery_created_at') ?>

    <?php // echo $form->field($model, 'tourn_gallery_modified_at') ?>

    <?php // echo $form->field($model, 'tourn_gallery_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
