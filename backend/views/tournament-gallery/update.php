<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TournamentGallery */

$this->title = 'Update Tournament Gallery: ' . ' ' . $model->tourn_gallery_id;
$this->params['breadcrumbs'][] = ['label' => 'Tournament Galleries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tourn_gallery_id, 'url' => ['view', 'id' => $model->tourn_gallery_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tournament-gallery-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
