<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TournamentGallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tournament Galleries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tournament-gallery-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tournament Gallery', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'tourn_gallery_id',
            'tourn_id',
            'gallery_id',
            'tourn_gallery_date',
            'tourn_gallery_created_at',
            // 'tourn_gallery_modified_at',
            // 'tourn_gallery_status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
