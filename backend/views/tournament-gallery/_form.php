<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\KslGalleryModel;
use backend\models\TournamentGallery;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model backend\models\TournamentGallery */
/* @var $form yii\widgets\ActiveForm */

?>
<script src="<?php echo Url::base(); ?>/plugins/multiselect/jquery.sumoselect.js"></script>
<link href="<?php echo Url::base(); ?>/plugins/multiselect/sumoselect.css" rel="stylesheet" />
<div class="tournament-gallery-form">

    <?php $form = ActiveForm::begin(['id' => 'mytourn-form']); ?>


    <?=  $form->field($model, 'tourn_id')->hiddenInput()->label(false); ?>

     <?= $form->field($model, 'tournTitle')->textInput(['disabled' => true ]) ?>
     
   <?php  $event_names = ArrayHelper::map(KslGalleryModel::find()->asArray()->all(), 'gallery_id', 'gallery_title');

    ?>

    <?php

        $selected_event = ArrayHelper::getColumn(TournamentGallery::find()->where(['tourn_id' => $model->tourn_id])->all(), 'gallery_id', 'tourn_id'); 
        $model->gallery_id=$selected_event;
     ?>

    <div class="form-group col-md-12" style="margin-left: -12px;">
                        <script>$(".groups_eg_g").SumoSelect({selectAll:true, search:false });</script>
                        <label>Gallery</label><br/>
                        <?php echo $form->field($model, 'gallery_id')->dropDownList($event_names,['multiple'=>'multiple','size'=>'1','class'=>'groups_eg_g form-control'])->label(false);  
            
          ?>
    </div>

    
    
    <div class="modal-footer form-group">
          <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div>


<script type="text/javascript">
    $(".groups_eg_g").SumoSelect({selectAll:true, search:false });
</script>