<div class="col-md-12">
            <!-- The time line -->
            <ul class="timeline">
                <!-- timeline time label -->
                <li class="time-label">
                    <span class="bg-red">
                        	<?= $model->news_created_date; ?> 
                    </span>
                </li>
                <!-- /.timeline-label -->
                <!-- timeline item -->
                <li>
                    <i class="fa fa-newspaper-o bg-blue"></i>
                    <div class="timeline-item">
                        <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
                        <h3 class="timeline-header"><a href="#"><?= $model->news_title; ?></a> </h3>
                        <div class="timeline-body">
                          <?= $model->news_editor_content; ?>
                        </div>
                        <div class="timeline-footer">
                            <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Read more"><i class="fa fa-ellipsis-h"></i></a>
                            <a class="btn btn-default btn-xs" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></a>
                            <a class="btn btn-warning btn-xs" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
                            <label> Posted by:</label><?= $model->news_posted_by; ?>
        <!-- <a class="btn btn-danger btn-xs" data-toggle="tooltip" title="Rejected"><i class="fa fa-ban"></i></a>
         <a class="btn btn-success btn-xs" data-toggle="tooltip" title="Approved"><i class="fa fa-check"></i></a>-->
                            <div class="pull-right"><label class='unblock'><i class="fa fa-check"></i> Approved </label>  
                            </div>
                        </div>
                    </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                </ul>
        </div><!-- /.col -->
