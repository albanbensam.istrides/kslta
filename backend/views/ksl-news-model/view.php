<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\KslNewsModel */

$this->title = $model->news_id;
$this->params['breadcrumbs'][] = ['label' => 'Ksl News Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-news-model-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->news_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->news_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'news_id',
            'news_title',
            'news_display_tag',
            'news_link_address',
            'news_link_status',
            'news_content_image',
            'news_editor_content:ntext',
            'news_posted_by',
            'news_from_date',
            'news_to_date',
            'news_created_date',
            'news_lastupdated_date',
            'news_flat',
        ],
    ]) ?>

</div>
