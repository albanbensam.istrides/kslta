<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\KslNewsModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'News List';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .mygridcustom{ 
        height: 600px;
        overflow: scroll;
    
    }
</style>
<div class="ksl-news-model-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> Add', ['create'], ['class' => 'btn btn-primary btn-md pull-right ']) ?>
    </p>

    <div class="col-md-12 mygridcustom">
        <!-- The time line -->
        <ul class="timeline">
            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'itemOptions' => ['class' => 'item'],
                /* 'itemView' => function ($model, $key, $index, $widget) {
                  return Html::a(Html::encode($model->news_id), ['view', 'id' => $model->news_id]).$model->news_title;
                  }, */
                'itemView' => '_newsview',
            ])
            ?>
        </ul>
    </div><!-- /.col -->
</div>
