<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\KslNewsTbl */

$this->title = 'Add News';
$this->params['breadcrumbs'][] = ['label' => 'News List', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-news-tbl-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
