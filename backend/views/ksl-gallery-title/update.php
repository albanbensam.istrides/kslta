<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\KslGalleryTitle */

$this->title = 'Update Ksl Gallery Title: ' . $model->tit_id;
$this->title = 'Update Gallery Title ';
$this->params['breadcrumbs'][] = ['label' => 'Ksl Gallery Titles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tit_id, 'url' => ['view', 'id' => $model->tit_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ksl-gallery-title-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
