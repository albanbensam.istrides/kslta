<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\KslGalleryTitle */

$this->title = 'Create Ksl Gallery Title';
$this->params['breadcrumbs'][] = ['label' => 'Ksl Gallery Titles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-gallery-title-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
