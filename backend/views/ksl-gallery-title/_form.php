<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\KslGalleryTitle */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ksl-gallery-title-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tit_value')->textarea(['rows' => 6]) ?>

    <?php //$form->field($model, 'tit_timestamp')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
