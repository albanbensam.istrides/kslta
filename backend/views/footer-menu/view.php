<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\FooterMenu */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Footer Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="footer-menu-view">

    <h1><?= Html::encode($this->title) ?></h1>

    

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            
            'menu_name',
            'menu_link',
            'order_number',
             
        ],
    ]) ?>

</div>
