<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PlayerEventsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="player-events-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ue_autoid') ?>

    <?= $form->field($model, 'ue_userid') ?>

    <?= $form->field($model, 'ue_tournamentid') ?>

    <?= $form->field($model, 'ue_eventid') ?>

    <?= $form->field($model, 'ue_created') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
