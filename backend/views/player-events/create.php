<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\PlayerEvents */

$this->title = 'Create Player Events';
$this->params['breadcrumbs'][] = ['label' => 'Player Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="player-events-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
