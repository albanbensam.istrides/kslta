<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\PlayerEvents */

$this->title = $model->ue_autoid;
$this->params['breadcrumbs'][] = ['label' => 'Player Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="player-events-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ue_autoid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ue_autoid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ue_autoid',
            'ue_userid',
            'ue_tournamentid',
            'ue_eventid',
            'ue_created',
        ],
    ]) ?>

</div>
