<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PlayerEvents */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="player-events-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ue_userid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ue_tournamentid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ue_eventid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ue_created')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
