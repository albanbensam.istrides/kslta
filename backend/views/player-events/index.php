<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Player Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="player-events-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Player Events', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ue_autoid',
            'ue_userid',
            'ue_tournamentid',
            'ue_eventid',
            'ue_created',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
