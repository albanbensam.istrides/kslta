<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PlayerEvents */

$this->title = 'Update Player Events: ' . ' ' . $model->ue_autoid;
$this->params['breadcrumbs'][] = ['label' => 'Player Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ue_autoid, 'url' => ['view', 'id' => $model->ue_autoid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="player-events-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
