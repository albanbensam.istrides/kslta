<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\KslNotification */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ksl-notification-form">


 
    <?php $form = ActiveForm::begin(); ?>
<div class="box-body">                

				 <div class="col-md-6">
					 <div class="form-group">
					<?= $form->field($model, 'notify_text')->textarea(['rows' => 2]) ?>
					 </div>
				 </div>
				 
				<div class="col-md-6">
					 <div class="form-group">
					<?= $form->field($model, 'notify_link')->textarea(['rows' => 2]) ?>
					 </div> 
				 </div>
				 
				 <div class="col-md-6">
					 <div class="form-group">
					<?= $form->field($model, 'notify_color')->textarea(['rows' => 1]) ?>
					 </div>
				 </div>
				 
				<div class="col-md-6">
					 <div class="form-group">
					<?= $form->field($model, 'notify_status')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ], ['prompt' => 'select here']) ?>
					 </div>
				</div>

					 
				 <!--   <?= $form->field($model, 'notify_timestamp')->textInput() ?>-->
</div>
 
 

                       
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
	

    <?php ActiveForm::end(); ?>

</div>
