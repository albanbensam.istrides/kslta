<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\KslNotification */

$this->title = $model->notify_id;
$this->params['breadcrumbs'][] = ['label' => 'Ksl Notifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-notification-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->notify_id], ['class' => 'btn btn-primary']) ?>
      <!--  <?= Html::a('Delete', ['delete', 'id' => $model->notify_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>-->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'notify_id',
            'notify_text:ntext',
            'notify_link:ntext',
            'notify_color:ntext',
            'notify_status',
            //'notify_timestamp',
			
			
        ],
		
    ]) ?>

</div>
