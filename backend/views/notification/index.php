<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductSolutionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notifications';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/datatables/dataTables.bootstrap.css">
<script src="<?php echo Url::base(); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>


<div class="user-index">
    <div class="box-body">
    <div class="box box-primary cgridoverlap">
                <div class="box-header with-border">

    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div><!-- /.box-header -->
                  <div class="box-body">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   <?php Pjax::begin(); ?>
           <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'notify_id',
            'notify_text:ntext',
            'notify_link:ntext',
            'notify_color:ntext',
            'notify_status',
            // 'notify_timestamp',

            //['class' => 'yii\grid\ActionColumn'],
      ['class' => 'yii\grid\ActionColumn',
              'header'=> 'Actions',
              'template'=>'{view}{update}'
                       ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
</div>
</div>
</div>

