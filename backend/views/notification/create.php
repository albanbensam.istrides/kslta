<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\KslNotification */

$this->title = 'Create Ksl Notification';
$this->params['breadcrumbs'][] = ['label' => 'Ksl Notifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-notification-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
