<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Userdetails */

$this->title = 'User Details - View';
$this->params['breadcrumbs'][] = ['label' => 'Userdetails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userdetails-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>


    <div class="col-md-12">

              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="dist/img/user2-160x160.jpg" alt="User profile picture">
                  <h3 class="profile-username text-center"><?= $model->first_name .' '. $model->last_name  ?></h3>
                  <p class="text-muted text-center"><?php echo $model->user_type == 'A' ? 'Admin' : "Others"; ?></p>

                  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <b>KSLTA User Name</b> <a class="pull-right"><?= $model->username ?></a>
                    </li>
                    <li class="list-group-item">
                      <b>Date of Birth</b> <a class="pull-right"><?= $model->dob ?></a>
                    </li>
                    <li class="list-group-item">
                      <b>City</b> <a class="pull-right"><?= $model->city ?></a>
                    </li>
                    <li class="list-group-item">
                      <b>Email id</b> <a class="pull-right"><?= $model->email ?></a>
                    </li>
                    <li class="list-group-item">
                      <b>Status</b> <a class="pull-right"><?= $model->status_flag == 'A'? 'Active' : 'Inactive' ?></a>
                    </li>
                  </ul>

                </div><!-- /.box-body -->
              </div><!-- /.box -->

              
            </div><!-- /.col -->

    
</div>
