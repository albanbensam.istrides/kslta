<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Userdetails */
/* @var $form yii\widgets\ActiveForm */
?>
<section class="content">
<!-- Info boxes -->
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
             <div class="box-header with-border">
                     <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div><!-- /.box-header -->
<div class="userdetails-form">
<div class="box-body">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12">

    <div class="form-group col-md-3">
    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true, 'placeholder' => 'First Name']) ?>
    </div>
        <div class="form-group col-md-3">

	<?= $form->field($model, 'last_name')->textInput(['maxlength' => true, 'placeholder' => 'Last Name']) ?>
	</div>
	    <div class="form-group col-md-3">

	<?= $form->field($model, 'dob')->textInput(['class' => 'form-control datepicker', 'placeholder' => 'DD-MM-YYYY', 'bootstrap-datepicker data-date-autoclose' => "true", 'data-date-end-date' => "today", 'data-required' => "true", 'data-provide' => "datepicker", 'data-date-format' => "dd-mm-yyyy"]) ?>
	</div>
	    <div class="form-group col-md-3">

	<?= $form->field($model, 'city')->textInput(['maxlength' => true, 'placeholder' => 'City']) ?>
	</div>
	</div>

	 <div class="col-md-12">
	    <div class="form-group col-md-4">

    <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'placeholder' => 'Login Username']) ?>
    </div>
    	    <div class="form-group col-md-4">

    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Email id']) ?>
    </div>


    <div class="form-group col-md-4">
    <?php 
    if($model->isNewRecord){
        echo $form->field($model, 'password_hash')->passwordInput(['placeholder' => 'Password']); 
    }
    ?>
    </div>
    </div>

        <div class="box-footer pull-right">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    </div>
    <?php ActiveForm::end(); ?>
    </div>

</div>
</div>
</div>
</div>
</section>



<script type="text/javascript">
    $(".datepicker").datepicker();
    </script>