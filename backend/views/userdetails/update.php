<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Userdetails */

$this->title = 'Update User Details';
$this->params['breadcrumbs'][] = ['label' => 'Userdetails', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="userdetails-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
