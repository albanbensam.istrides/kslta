<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TournamentEventModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tournament-event-model-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tourn_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'event_category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'event_subcategory')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
