<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TournamentEventModel */

$this->title = 'Create Tournament Event Model';
$this->params['breadcrumbs'][] = ['label' => 'Tournament Event Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tournament-event-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
