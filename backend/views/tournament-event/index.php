<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tournament Event Models';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tournament-event-model-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Tournament Event Model', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'event_id',
            'tourn_id',
            'event_category',
            'event_subcategory',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
