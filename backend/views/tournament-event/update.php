<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TournamentEventModel */

$this->title = 'Update Tournament Event Model: ' . ' ' . $model->event_id;
$this->params['breadcrumbs'][] = ['label' => 'Tournament Event Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->event_id, 'url' => ['view', 'id' => $model->event_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tournament-event-model-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
