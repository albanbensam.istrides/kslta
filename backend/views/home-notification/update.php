<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\HomeNotification */

$this->title = 'Update Home Notification: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Home Notifications', 'url' => ['index']];
 
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="home-notification-update">
 
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
