<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\HomeNotification */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Home Notifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-notification-view">
 

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
        //    'id',
            'notification_name',
            'notification_url:url',
            'order_no',
        ],
    ]) ?>

</div>
