<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\HomeNotification */

$this->title = 'Create Home Notification';
$this->params['breadcrumbs'][] = ['label' => 'Home Notifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-notification-create">

     

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
