<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TournamentPlayerListSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tournament-player-list-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'playerlists_id') ?>

    <?= $form->field($model, 'tourn_id') ?>

    <?= $form->field($model, 'event_id') ?>

    <?= $form->field($model, 'playerlists_filename') ?>

    <?= $form->field($model, 'playerlists_filepath') ?>

    <?php // echo $form->field($model, 'playerlists_created_at') ?>

    <?php // echo $form->field($model, 'playerlists_modified_at') ?>

    <?php // echo $form->field($model, 'playerlists_active_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
