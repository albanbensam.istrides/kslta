<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TournamentPlayerListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="tournament-player-list-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'playerlists_id',
          //  'tourn_id',
           // 'event_id',
            'playerlists_filename',
             [
                'attribute' => 'playerlists_filepath',
                'format' => 'html',             
                'value' => function ($data) {
                    return Html::img($data->playerlists_filepath,
                        ['width' => '60px']);
                },
            ],
           // 'playerlists_filepath:ntext',
            // 'playerlists_created_at',
            // 'playerlists_modified_at',
            // 'playerlists_active_status',

             ['class' => 'yii\grid\ActionColumn',
               'header'=> 'Actions',
               'template'=>'{delete}',
                            'buttons'=>[
                              'view' => function ($url, $model, $key) {
                                   
                                   // return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
                                   return Html::button('<i class="glyphicon glyphicon-eye-open"></i>', ['value' => $url, 'class' => 'btn btn-primary btn-xs view view gridbtncustom modalView', 'data-toggle'=>'tooltip', 'title' =>'View' ]);
                              }, 
                            /*   'update' => function ($url, $model) {     
                                return Html::button('<span class="fa fa-edit"></span>', [
                                        'value' => $url,
                                        'name' => 'Category Update',
                                        'title' => Yii::t('yii', 'update'),
                                        'class' => 'btn btn-warning btn-xs gridbtncustom updatedata',
                                        'data-toggle'=> 'tooltip',
                                ]);                                
            
                              }, */
                              'update' => function ($url, $model, $key) {
                                        $options = array_merge([
                                            'class' => 'btn btn-warning btn-xs update gridbtncustom',
                                            'data-toggle'=>'tooltip',
                                            'title' => Yii::t('yii', 'Update'),
                                            'aria-label' => Yii::t('yii', 'Update'),
                                            'data-pjax' => '0',
                                        ]);
                                        return Html::a('<span class="fa fa-edit"></span>', $url, $options);
                                    },
                                // 'delete' => function ($url, $model, $key) {
                                       
                                //        // return Html::a('<span class="fa fa-trash"></span>', $url, $options);
                                //         return Html::button('<i class="fa fa-trash"></i>', ['value' => $url, 'class' => 'btn btn-default btn-xs delete gridbtncustom modalDelete', 'data-toggle'=>'tooltip', 'title' =>'Delete' ]);
                                //     },
                          ] ],
        ],
    ]); ?>

</div>
