<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\TournamentPlayerList */

$this->title = $model->playerlists_id;
$this->params['breadcrumbs'][] = ['label' => 'Tournament Player Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tournament-player-list-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->playerlists_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->playerlists_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'playerlists_id',
            'tourn_id',
            'event_id',
            'playerlists_filename',
            'playerlists_filepath:ntext',
            'playerlists_created_at',
            'playerlists_modified_at',
            'playerlists_active_status',
        ],
    ]) ?>

</div>
