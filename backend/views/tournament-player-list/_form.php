<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TournamentPlayerList */
/* @var $form yii\widgets\ActiveForm */

$torunIdData = $_GET['tourn_id'];
$eventIdData = $_GET['event_id'];

?>
<style>
    #blah{
    margin-top: -75px;
    margin-right: 0px;
  }
  </style>
<div class="tournament-player-list-form">

    <?php $form = ActiveForm::begin([
        
        'id' => 'playerlist-form', 
        'options' => ['enctype' => 'multipart/form-data']

        ]); ?>

    <?= $form->field($model, 'tourn_id')->hiddenInput(['value'=> $torunIdData])->label(false) ?>

    <?= $form->field($model, 'event_id')->hiddenInput(['value'=> $eventIdData])->label(false) ?>

   
     <div class="col-md-12">
           <div class="form-group col-md-6">
                                                
             <?= $form->field($model, 'playerlists_filepath')->fileInput(['id'=> 'imgInp', 'class' => 'btn btn-primary']) ?>
            <img id="blah" class="profile-user-img img-responsive img-circle" src=<?php echo $model->isNewRecord ? "control_images/question.png" : $model->playerlists_filepath;  ?> />

           </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<i class="fa fa-fw fa-save"></i> SAVE' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
     /* Onchange Image View Start */
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]);
            }
        }
        
        $("#imgInp").change(function(){
            readURL(this);
        });
    /* Onchange Image View End */

</script>