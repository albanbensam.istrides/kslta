<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TournamentPlayerList */

$this->title = 'Update Tournament Player List';
$this->params['breadcrumbs'][] = ['label' => 'Tournament Player Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tournament-player-list-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
