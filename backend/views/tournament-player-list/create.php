<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TournamentPlayerList */

$this->title = 'Add Tournament Player List';
$this->params['breadcrumbs'][] = ['label' => 'Tournament Player Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tournament-player-list-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
