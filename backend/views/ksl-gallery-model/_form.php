<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\DropdownManagement;
use yii\helpers\Url;
use backend\models\KslTournamentsTbl;
/* @var $this yii\web\View */
/* @var $model backend\models\KslGalleryModel */
/* @var $form yii\widgets\ActiveForm */
?>
<?php

$form = ActiveForm::begin([
            'id' => 'news-active-form',
            'options' => ['enctype' => 'multipart/form-data'],
        ]);
?>
<section class="content">
<!-- Info boxes -->
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                     <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div><!-- /.box-header -->
            <div class="gallery-form">

<div class="box-body">

<div class="row">

    <div class="col-xs-12">
        <!-- text input -->        
        <div class="col-xs-6">                               
            <div class="form-group">
<?= $form->field($model, 'gallery_title')->textInput(['class' => 'form-control', 'placeholder' => 'Gallery Title']) ?>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="form-group">
                <?php $list1 = ArrayHelper::map(DropdownManagement::find()->where(['dropdown_key' => 'page_display_tag'])->orderBy('dropdown_order')->all(), 'dropdown_id', 'dropdown_value'); ?>
<?= $form->field($model, 'gallery_display_tag')->dropDownList($list1, ['prompt' => 'Select']) ?>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="control-group">	
<?= $form->field($model, 'gallery_link_address')->fileInput() ?>
            </div> <!-- /control-group -->
        </div>
        <div class="col-xs-6">
            <div class="form-group">
<?= $form->field($model, 'gallery_content_image')->textarea([ 'class' => 'form-control contentEditor']) ?>
            </div>       
        </div>
        <div class="col-xs-6"> 
            <div class="form-group">
<?= $form->field($model, 'gallery_posted_by')->textInput(['class' => 'form-control', 'placeholder' => 'Gallery Title']) ?>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="form-group">
<?= $form->field($model, 'gallery_picture_courtesy')->textInput(['class' => 'form-control', 'placeholder' => 'Gallery Title']) ?>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="form-group">
<?= $form->field($model, 'gallery_from_date')->textInput(['maxlength' => true, 'class' => 'form-control datepicker', 'placeholder' => 'DD-MM-YYYY', 'bootstrap-datepicker data-date-autoclose' => "true", 'data-required' => "true", 'data-provide' => "datepicker", 'data-date-format' => "dd-mm-yyyy"]) ?>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="form-group">
<?= $form->field($model, 'gallery_to_date')->textInput(['maxlength' => true, 'class' => 'form-control datepicker', 'placeholder' => 'DD-MM-YYYY', 'bootstrap-datepicker data-date-autoclose' => "true", 'data-required' => "true", 'data-provide' => "datepicker", 'data-date-format' => "dd-mm-yyyy"]) ?>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="form-group">
<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
<?php ActiveForm::end(); ?>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </section>
    