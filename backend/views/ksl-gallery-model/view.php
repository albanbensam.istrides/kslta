<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\KslGalleryModel */

$this->title = $model->gallery_id;
$this->params['breadcrumbs'][] = ['label' => 'Ksl Gallery Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-gallery-model-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->gallery_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->gallery_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'gallery_id',
            'gallery_title',
            'gallery_display_tag',
            'gallery_link_address',
            'gallery_link_status',
            'gallery_content_image',
            'gallery_posted_by',
            'gallery_picture_courtesy',
            'gallery_from_date',
            'gallery_to_date',
            'gallery_created_date',
            'gallery_lastupdated_date',
            'gallery_flat',
        ],
    ]) ?>

</div>
