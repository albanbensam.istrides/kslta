<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\KslGalleryModel */

$this->title = 'Add New Gallery';
$this->params['breadcrumbs'][] = ['label' => 'Gallery', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-gallery-model-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
