<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use backend\models\DropdownManagement;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductSolutionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gallery';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/datatables/dataTables.bootstrap.css">
<script src="<?php echo Url::base(); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>


<div class="gallery-index">
    <div class="box-body">
    <div class="box box-primary cgridoverlap">
                <div class="box-header with-border">

    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

    
    </div><!-- /.box-header -->
                  <div class="box-body">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('<i class="fa fa-plus"></i> Add Gallery', ['create'], ['class' => 'btn btn-primary btn-md pull-right ']) ?>
    </p>
   <?php Pjax::begin(); ?>
         <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

	            'gallery_id',
	            'gallery_title',
	            [
	            'attribute' => 'gallery_display_tag',
	            'format' => 'html',	            
	            'value' => function ($data) {
	            	$list1=ArrayHelper::map(DropdownManagement::find()->where(['dropdown_id'=>$data->gallery_display_tag])->all(), 'dropdown_id','dropdown_value');
					return $list1[$data->gallery_display_tag];
	            },
	        ],
	            [
	            'attribute' => 'gallery_link_address',
	            'format' => 'html',	            
	            'value' => function ($data) {
	                return Html::img($data->gallery_link_address,
	                    ['width' => '60px']);
	            },
	        ],
            'gallery_link_status',
            // 'gallery_content_image',
            // 'gallery_posted_by',
            // 'gallery_picture_courtesy',
            // 'gallery_from_date',
            // 'gallery_to_date',
            // 'gallery_created_date',
            // 'gallery_lastupdated_date',
            // 'gallery_flat',
			['class' => 'yii\grid\ActionColumn','template' => '{my_button}', 
		'buttons' => [
		    'my_button' => function ($url, $model, $key) {
		        return Html::a('Manage Page', ['gallery-page/create/', 'id'=>$model->gallery_id],$options = ['class' => 'btn btn-info btn-sm']);
		    },
		]],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
</div>
</div>
</div>

