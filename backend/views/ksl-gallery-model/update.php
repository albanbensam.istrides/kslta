<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\KslGalleryModel */

$this->title = 'Update Gallery: ' . ' ' . $model->gallery_id;
$this->params['breadcrumbs'][] = ['label' => 'Gallery', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->gallery_id, 'url' => ['view', 'id' => $model->gallery_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ksl-gallery-model-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
