<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\KslGalleryModelSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ksl-gallery-model-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'gallery_id') ?>

    <?= $form->field($model, 'gallery_title') ?>

    <?= $form->field($model, 'gallery_display_tag') ?>

    <?= $form->field($model, 'gallery_link_address') ?>

    <?= $form->field($model, 'gallery_link_status') ?>

    <?php // echo $form->field($model, 'gallery_content_image') ?>

    <?php // echo $form->field($model, 'gallery_posted_by') ?>

    <?php // echo $form->field($model, 'gallery_picture_courtesy') ?>

    <?php // echo $form->field($model, 'gallery_from_date') ?>

    <?php // echo $form->field($model, 'gallery_to_date') ?>

    <?php // echo $form->field($model, 'gallery_created_date') ?>

    <?php // echo $form->field($model, 'gallery_lastupdated_date') ?>

    <?php // echo $form->field($model, 'gallery_flat') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
