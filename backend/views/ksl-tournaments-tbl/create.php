<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\KslTournamentsTbl */

$this->title = 'Add Tournaments';
$this->params['breadcrumbs'][] = ['label' => ' Tournaments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-tournaments-tbl-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
