<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\DropdownManagement;
use backend\models\TournamentEventModel;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\KslTournaments */
/* @var $form yii\widgets\ActiveForm */
?>

<link href="<?php echo Url::base(); ?>/plugins/jQuery-Form-Wizard-Plugin-Smart-Wizard/styles/demo_style.css" rel="stylesheet" type="text/css">
<link href="<?php echo Url::base(); ?>/plugins/jQuery-Form-Wizard-Plugin-Smart-Wizard/styles/smart_wizard.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo Url::base(); ?>/plugins/jQuery-Form-Wizard-Plugin-Smart-Wizard/js/jquery.smartWizard.js"></script>
<style>

  #blah{
  display: inline-block;
  margin-top: -25px;
  width: 50px;
  height: 50px;
  border-radius: 50%;
  background-repeat: no-repeat;
  background-position: center center;
  background-size: cover;
  background-image: url('uploadimage/custom_images/yourLogo_icon.png');

  }
  #imgInp{
    float: left;
  }

</style>
<!--  <section class="content-header">
        <h1>
            Add Tournament

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Tournament</li>
            <li class="active">Add Tournament</li>
        </ol>
    </section>-->

<div class="box box-info">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <!-- Content Header (Page header) -->
    <div class="box-header with-border">
                     <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div><!-- /.box-header -->
    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <!-- left column -->
            <div class="col-md-12 stepwise-tbl">
                <!-- general form elements -->

                <table align="center" border="0" cellpadding="0" cellspacing="0">
                    <tr><td>

                            <div id="wizard2" class="swMain">
                                <ul>
                                    <li><a href="#step-1">
                                            <span class="stepDesc">
                                                Tournament Details
                                            </span>
                                        </a></li>
                                    <li><a href="#step-2">
                                            <span class="stepDesc">                                            
                                               Venue Details
                                            </span>
                                        </a></li>
                                    <li><a href="#step-3">                                            
                                            <span class="stepDesc">
                                              Association Details
                                            </span>                   
                                        </a></li>
                                    <li><a href="#step-4">                                           
                                            <span class="stepDesc">
                                                Event Details
                                            </span>                   
                                        </a></li>
                                </ul>

                                <div id="step-1">	                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?= $form->field($model, 'tourn_title')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'Tournament Title']) ?>
                                            </div>
                                            <div class="form-group">
                                                <?= $form->field($model, 'tourn_from_date')->textInput(['class' => 'form-control datepicker', 'placeholder' => 'DD-MM-YYYY', 'bootstrap-datepicker data-date-autoclose' => "true", 'data-required' => "true", 'data-provide' => "datepicker", 'data-date-format' => "dd-mm-yyyy"]) ?>

                                            </div>
                                            <div class="form-group">
                                                <?= $form->field($model, 'tourn_to_date')->textInput(['class' => 'form-control datepicker', 'placeholder' => 'DD-MM-YYYY', 'bootstrap-datepicker data-date-autoclose' => "true", 'data-required' => "true", 'data-provide' => "datepicker", 'data-date-format' => "dd-mm-yyyy"]) ?>

                                            </div>
                                            <div class="form-group">
                                                <?= $form->field($model, 'tourn_last_submission_date')->textInput(['class' => 'form-control datepicker', 'placeholder' => 'DD-MM-YYYY', 'bootstrap-datepicker data-date-autoclose' => "true", 'data-required' => "true", 'data-provide' => "datepicker", 'data-date-format' => "dd-mm-yyyy"]) ?>

                                            </div>
                                            <div class="form-group">
                                            
                                               <?= $form->field($model, 'tourn_tournament_image')->fileInput(['id'=> 'imgInp', 'class' => 'btn btn-primary']) ?>
                                               <img id="blah" src=<?php echo $model->isNewRecord ? "#" : $model->tourn_tournament_image;  ?> width="50px" height="50px"/>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?= $form->field($model, 'tourn_withdrl_date')->textInput(['class' => 'form-control datepicker', 'placeholder' => 'DD-MM-YYYY', 'bootstrap-datepicker data-date-autoclose' => "true", 'data-required' => "true", 'data-provide' => "datepicker", 'data-date-format' => "dd-mm-yyyy"]) ?>


                                            </div>
                                            <div class="form-group">
                                                <?= $form->field($model, 'tourn_description')->textArea(['maxlength' => true, 'class' => 'form-control', 'rows' => '5']) ?>


                                            </div>
                                            <div class="form-group">
                                                <?= $form->field($model, 'tourn_price_money')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'Tournament Price Money']) ?>
                                            </div>

                                            <!--                 <div class="form-group">
                                                               <label for="exampleInputFile">File input</label>
                                                               <input type="file" id="exampleInputFile">
                                                               <p class="help-block">Example block-level help text here.</p>
                                                             </div>
                                                             <div class="checkbox">
                                                               <label>
                                                                 <input type="checkbox"> Check me out
                                                               </label>
                                                             </div>
                                            -->                  
                                        </div>
                                    </div>


                                </div>
                                <div id="step-2">                                   	
                                    <div class="row">
                                        <div class="col-md-6">
                                          <?php /*  <div class="form-group">
                                                <?= $form->field($model, 'tourn_category')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'Category']) ?>
                                            </div>
                                            <div class="form-group">
                                                <?= $form->field($model, 'tourn_sub_category')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'Sub-Category']) ?>
                                            </div>*/?>
                                            <div class="form-group">
                                                <?= $form->field($model, 'tourn_venue')->textInput([['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'Venue', 'placeholder' => 'Tournament Venue']]) ?>

                                            </div> 
                                            <div class="form-group">
                                                <?= $form->field($model, 'tourn_venue_address1')->textArea(['maxlength' => true, 'class' => 'form-control', 'rows' => '4']) ?>

                                            </div>

                                        </div>
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <?= $form->field($model, 'tourn_venue_address2')->textArea(['maxlength' => true, 'class' => 'form-control', 'rows' => '4']) ?>

                                            </div>
                                            <div class="form-group">
                                                <?= $form->field($model, 'tourn_venue_city')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'City']) ?>

                                            </div>

                                        </div>
                                    </div>         
                                </div>     







                                <div id="step-3">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?= $form->field($model, 'tourn_asso_title')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'Associate Title']) ?>
                                            </div>
                                            <div class="form-group">
                                                <?= $form->field($model, 'tourn_asso_head')->textInput(['maxlength' => true, 'class' => 'form-control']) ?>

                                            </div>
                                            <div class="form-group">
                                                <?= $form->field($model, 'tourn_asso_address1')->textArea(['maxlength' => true, 'class' => 'form-control', 'rows' => '2']) ?>

                                            </div>
                                            <div class="form-group">
                                                <?= $form->field($model, 'tourn_asso_address2')->textArea(['maxlength' => true, 'class' => 'form-control', 'rows' => '2']) ?>
                                            </div>
                                            
                                        </div>
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <?= $form->field($model, 'tourn_refree1')->textInput(['maxlength' => true, 'class' => 'form-control']) ?>

                                            </div>
                                            <div class="form-group">
                                                <?= $form->field($model, 'tourn_refree2')->textInput(['maxlength' => true, 'class' => 'form-control']) ?>

                                            </div>

                                            <div class="form-group">
                                                <?= $form->field($model, 'tourn_refree3')->textInput(['maxlength' => true, 'class' => 'form-control']) ?>

                                            </div>                                           
                                            <div class="form-group">
                                                <?= $form->field($model, 'tourn_link')->textInput(['maxlength' => true, 'class' => 'form-control']) ?>
											 </div>

                                        </div>
                                    </div>			          
                                </div>

                                <div id="step-4">
                                    <div class="row">

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?php $list1 = ArrayHelper::map(DropdownManagement::find()->where(['dropdown_key' => 'tourmament_event_category'])->orderBy('dropdown_order')->all(), 'dropdown_id', 'dropdown_value'); ?>
                                                <?= $form->field($model, 'tour_event_category')->dropDownList($list1, ['prompt' => '---Select---', 'class' => 'form-control']) ?>
                                            </div>
                                            </div>
                                             <div class="col-md-4">
                                            <div class="form-group">
                                                <?php $list1 = ArrayHelper::map(DropdownManagement::find()->where(['dropdown_key' => 'tourmament_event_subcategory'])->orderBy('dropdown_order')->all(), 'dropdown_id', 'dropdown_value'); ?>
                                                <?= $form->field($model, 'tour_event_subcategory')->dropDownList($list1, ['prompt' => '---Select---', 'class' => 'form-control']) ?>
                                            </div>
                                              </div>
                                             <div class="col-md-1">
                                             <div class="form-group">
                                             	<br>
                                                   <img class="add_click" src="dist/img/add.png">
                                            </div>
                                            </div>
                                             <div class="col-md-12">
                                            <div class="form-group">                                    
                                                <?php
                                                $event_all_val='';
												$id_db_all=array();  
												$html_str='';  
                                                if(!$model->isNewRecord){
                                                	$event_all_val='';
													$TournamentEvenlist = TournamentEventModel::find()->where(['tourn_id' => $model->tourn_id])->all();
													foreach($TournamentEvenlist as $one_event){
														$id_db_all[]=$one_event->event_category;
														$id_db_all[]=$one_event->event_subcategory;																												
													}
													if(count($id_db_all)>0){
														/*$studentQuery = DropdownManagement::Find();
														$studentQuery->FilterWhere(array('in', 'dropdown_id', $id_db_all));
														var_dump($studentQuery->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
														*/
													$event_names = ArrayHelper::map(DropdownManagement::find()->FilterWhere(array('in', 'dropdown_id', $id_db_all))->all(), 'dropdown_id', 'dropdown_value');
													}
													foreach($TournamentEvenlist as $one_event){
														$one_category_txt=$one_subcategory_txt='';
														$event_all_val.=$one_event->event_category.'_'.$one_event->event_subcategory.'~';
														$html_ids=$one_event->event_category.'_'.$one_event->event_subcategory;
														$one_category_txt='';
														if(isset($event_names[$one_event->event_category])){
															$one_category_txt=$event_names[$one_event->event_category];
														}
														$one_subcategory_txt='';
														if(isset($event_names[$one_event->event_subcategory])){
															$one_subcategory_txt=$event_names[$one_event->event_subcategory];
														}
														$html_str.='<div class="rmd_'.$html_ids.'">'.$one_category_txt.' '.$one_subcategory_txt.' <img class="remove_click" data_rmd="'.$html_ids.'" src="dist/img/remove.png"></div>';														
													}
                                                }  ?>
                                                 <div id="selected_events"><?php echo $html_str;  ?></div>
                                                <?= Html::hiddenInput('event_all_key',$event_all_val ,['id'=>'event_all_key']); ?>
                                            </div>
                                            <?php /*
                                             <div class="form-group">
                                                   <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                                            </div> */?>                                        </div>
                                       
                                        <div class="col-md-6">
                                           
                                        </div>
                                          
                                    </div>
                                </div>
                            </div>
                            <!-- End SmartWizard Content 1 -->

                        </td></tr>
                </table>  
            </div><!--/.col (left) -->

        </div>
        <!-- right column -->
    </section><!-- /.content -->
  
    <?php ActiveForm::end(); ?>  

</div><!-- /.content-wrapper -->

<script type="text/javascript">
   
    $(document).ready(function(){
    	// Smart Wizard     	
  		$('#wizard2').smartWizard({transitionEffect:'slideleft',onLeaveStep:leaveAStepCallback,onFinish:onFinishCallback,enableFinishButton:true});

      function leaveAStepCallback(obj){
        var step_num= obj.attr('rel');
        return validateSteps(step_num);
      }
      
      function onFinishCallback(){
       if(validateAllSteps()){
        $('#w0').submit();
       }
      }
            
		});
	   
    function validateAllSteps(){
       var isStepValid = true;
       
       if(validateStep1() == false){
         isStepValid = false;
         $('#wizard2').smartWizard('setError',{stepnum:1,iserror:true});         
       }else{
         $('#wizard2').smartWizard('setError',{stepnum:1,iserror:false});
       }
       
       if(validateStep2() == false){
         isStepValid = false;
         $('#wizard2').smartWizard('setError',{stepnum:3,iserror:true});         
       }else{
         $('#wizard2').smartWizard('setError',{stepnum:3,iserror:false});
       }
       
       if(!isStepValid){
          $('#wizard2').smartWizard('showMessage','Please correct the errors in the steps and continue');
       }
              
       return isStepValid;
    } 	
		
		
		function validateSteps(step){
		  var isStepValid = true;
      // validate step 1
      if(step == 1){
        if(validateStep1() == false ){
          isStepValid = false; 
         // $('#wizard2').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
        // $('#wizard2').smartWizard('setError',{stepnum:step,iserror:true});         
        }else{
         // $('#wizard2').smartWizard('hideMessage');
        //  $('#wizard2').smartWizard('setError',{stepnum:step,iserror:false});
        }
      }
      
      // validate step3
      if(step == 2){
        if(validateStep2() == false ){
          isStepValid = false; 
         // $('#wizard2').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
        //  $('#wizard2').smartWizard('setError',{stepnum:step,iserror:true});         
        }else{
         // $('#wizard2').smartWizard('hideMessage');
         // $('#wizard2').smartWizard('setError',{stepnum:step,iserror:false});
        }
      }
      
      if(step == 3){
        if(validateStep2() == false ){
          isStepValid = false; 
         // $('#wizard2').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
        //  $('#wizard2').smartWizard('setError',{stepnum:step,iserror:true});         
        }else{
         // $('#wizard2').smartWizard('hideMessage');
         // $('#wizard2').smartWizard('setError',{stepnum:step,iserror:false});
        }
      }
      
      if(step == 4){
        if(validateStep2() == false ){
          isStepValid = false; 
         // $('#wizard2').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
        //  $('#wizard2').smartWizard('setError',{stepnum:step,iserror:true});         
        }else{
         // $('#wizard2').smartWizard('hideMessage');
         // $('#wizard2').smartWizard('setError',{stepnum:step,iserror:false});
        }
      }
      
      return isStepValid;
    }
		
		function validateStep1(){
       var isValid = true; 
       // Validate Username     
       var un = $('#ksltournamentstbl-tourn_title').val();
       if(!un && un.length <= 0){
       	 $('#w0').yiiActiveForm('updateAttribute', 'ksltournamentstbl-tourn_title', ["Tournament Title cannot be blank."]);
         isValid = false;       
       }
		var un = $('#ksltournamentstbl-tourn_from_date').val();
       if(!un && un.length <= 0){
       	 $('#w0').yiiActiveForm('updateAttribute', 'ksltournamentstbl-tourn_from_date', ["From Date cannot be blank."]);
         isValid = false;       
       }
       var un = $('#ksltournamentstbl-tourn_to_date').val();
       if(!un && un.length <= 0){
       	 $('#w0').yiiActiveForm('updateAttribute', 'ksltournamentstbl-tourn_to_date', ["To Date cannot be blank."]);
         isValid = false;       
       }
       var un = $('#ksltournamentstbl-tourn_last_submission_date').val();
       if(!un && un.length <= 0){
       	 $('#w0').yiiActiveForm('updateAttribute', 'ksltournamentstbl-tourn_last_submission_date', ["Tournament Last Submission Date cannot be blank."]);
         isValid = false;       
       }
       var un = $('#ksltournamentstbl-tourn_withdrl_date').val();
       if(!un && un.length <= 0){
       	 $('#w0').yiiActiveForm('updateAttribute', 'ksltournamentstbl-tourn_withdrl_date', ["Tournament Withdrl Date cannot be blank.."]);
         isValid = false;       
       }
       
       return isValid;
    }
    function validateStep2(){
      var isValid = true;    
      var un = $('#ksltournamentstbl-tourn_venue').val();
       if(!un && un.length <= 0){
       	 $('#w0').yiiActiveForm('updateAttribute', 'ksltournamentstbl-tourn_venue', ["Venue cannot be blank.."]);
         isValid = false;       
       }
        var un = $('#ksltournamentstbl-tourn_venue_address1').val();
       if(!un && un.length <= 0){
       	 $('#w0').yiiActiveForm('updateAttribute', 'ksltournamentstbl-tourn_venue_address1', ["Tournament Venue Address1 cannot be blank.."]);
         isValid = false;       
       }
       var un = $('#ksltournamentstbl-tourn_venue_city').val();
       if(!un && un.length <= 0){
       	 $('#w0').yiiActiveForm('updateAttribute', 'ksltournamentstbl-tourn_venue_city', ["Tournament City cannot be blank.."]);
         isValid = false;       
       }
      return isValid;
    }
    
    function validateStep3(){
      var isValid = true;    
      //validate email  email
      
      return isValid;
    }
    
    function validateStep4(){
      var isValid = true;    
      //validate email  email
  
      return isValid;
    }
    
    // Email Validation
    function isValidEmailAddress(emailAddress) {
      var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
      return pattern.test(emailAddress);
    } 
		
		
</script>
<script type="text/javascript">
    $(".datepicker").datepicker();
   
    $(document).ready(function () {

    /* Onchange Image View Start */
        function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imgInp").change(function(){
        readURL(this);
    });

    /* Onchange Image View End */
        
        //  Wizard 1  	
        $('#wizard1').smartWizard({
            transitionEffect: 'fade',
            onFinish: onFinishCallback,
            onLeaveStep: leaveAStepCallback,
        });
        function leaveAStepCallback(obj, context) {
            // To check and enable finish button if needed
            if (context.fromStep >= 2) {
                $('#wizard1').smartWizard('enableFinish', true);
            }
            return true;
        }
        //  Wizard 2
        $('#wizard2').smartWizard({transitionEffect: 'slide', onFinish: onFinishCallback});
        function onFinishCallback() {
           //alert('If you finish click create/update');
        }
    });
    
    
    $(document).ready(function(){
	var id = 2,max = 8,ct=1,append_data;
	//document.getElementById('users_list_div1');
	/*If the add icon was clicked*/

	$("body").on("click",".add_click", function(e){
		var event_category=$('#ksltournamentstbl-tour_event_category').val();
		var event_subcategory=$('#ksltournamentstbl-tour_event_subcategory').val();		
		if(event_category!="" && event_subcategory!=""){
			var category_val=$('#ksltournamentstbl-tour_event_category option:selected').text();
			var subcategory_val=$('#ksltournamentstbl-tour_event_subcategory option:selected').text();
			var category_ids=$('#ksltournamentstbl-tour_event_category option:selected').val();
			var subcategory_ids=$('#ksltournamentstbl-tour_event_subcategory option:selected').val();
			var html_ids=category_ids+'_'+subcategory_ids;				
			var event_ids=$('#event_all_key').val();
			var event_id_array = event_ids.split("~");
			if(jQuery.inArray( html_ids, event_id_array)<0){				
				var html_txt='<div class="rmd_'+html_ids+'">'+category_val+' '+subcategory_val+' <img class="remove_click" data_rmd="'+html_ids+'" src="dist/img/remove.png"></div>';
				$('#selected_events').append(html_txt);
				html_ids+='~';
				var old_ids=$('#event_all_key').val();
				$('#event_all_key').val(old_ids+html_ids);
			}
		}

		
	});	
	$("body").on("click",".remove_click", function(e){
		var rmd_id=$(this).attr('data_rmd');
		if(rmd_id!=""){
			var event_ids2=$('#event_all_key').val();
			var rmd_divid='rmd_'+rmd_id;			
			rmd_id+='~';
			event_ids2=event_ids2.replace(rmd_id, "");			
			$('#event_all_key').val(event_ids2);
			$( "."+rmd_divid).remove();	
		} 
	});
	
});

</script>  
