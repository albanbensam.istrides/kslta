<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\KslTournamentsTbl */

$this->title = 'Update Tournaments';
$this->params['breadcrumbs'][] = ['label' => 'Tournaments', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ksl-tournaments-tbl-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
