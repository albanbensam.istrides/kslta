<?php



use yii\helpers\Html;

use yii\widgets\ActiveForm;

use yii\grid\GridView;

use yii\widgets\Pjax;

use yii\helpers\Url;

use yii\bootstrap\Modal;

/* @var $this yii\web\View */

/* @var $searchModel backend\models\ProductSolutionSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */



$this->title = 'Tournaments';

$this->params['breadcrumbs'][] = $this->title;

?>



<link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/datatables/dataTables.bootstrap.css">

<script src="<?php echo Url::base(); ?>/plugins/datatables/jquery.dataTables.min.js"></script>

<script src="<?php echo Url::base(); ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>





<div class="ksl-tournaments-tbl-index">

    <div class="box-body">

    <div class="box box-primary cgridoverlap">

                <div class="box-header with-border">



    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>



    

    </div><!-- /.box-header -->

                  <div class="box-body">



    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>

        <?= Html::a('<i class="fa fa-plus"></i> Add Tournaments', ['create'], ['class' => 'btn btn-primary btn-md pull-right ']) ?>

    </p>

   <?php Pjax::begin(); ?>

          <?= GridView::widget([

        'dataProvider' => $dataProvider,

        'filterModel' => $searchModel,

        'columns' => [

            ['class' => 'yii\grid\SerialColumn'],



          //  'tourn_id',

            'tourn_title',

           // 'tourn_category',

           // 'tourn_sub_category',

            'tourn_venue',

            // 'tourn_venue_address1',

            // 'tourn_venue_address2',

            // 'tourn_venue_city',

             'tourn_from_date',

             'tourn_to_date',

             'tourn_last_submission_date',

            // 'tourn_withdrl_date',

            // 'tourn_description',

            // 'tourn_asso_title',

            // 'tourn_asso_address1',
            // 'tourn_asso_address2',

            // 'tourn_asso_head',

            // 'tourn_refree1',

            // 'tourn_refree2',

            // 'tourn_refree3',

            // 'tourn_link',

            // 'tourn_created_date',

            // 'tourn_lastmodified_date',

            // 'tourn_status',

            // 'sponser_name_of_organisation',

            // 'sponser_address1',

            // 'sponser_address2',

            // 'sponser_contact_person_name',

            // 'sponser_phone',

            // 'sponser_email:email',

            // 'tour_event_category',

            // 'tour_event_subcategory',

             ['class' => 'yii\grid\ActionColumn','template' => '{tournnews}', 'header' => 'Factsheet',

        'buttons' => [

            'tournnews' => function ($url, $model, $key) {

                return Html::a('<i class="fa fa-fw fa-file-text-o"></i> News List', ['ksl-news-tbl/tournamentnewslisting/', 'id'=>$model->tourn_id],$options = ['class' => 'btn btn-info btn-xs', 'data-toggle'=>'tooltip', 'aria-label'=>'News List View', 'data-original-title'=>'News List View', ]);

            },

        ]],

        ['class' => 'yii\grid\ActionColumn','template' => '{factsheet}', 'header' => 'News',

        'buttons' => [

            'factsheet' => function ($url, $model, $key) {

                return Html::a('<i class="fa fa-fw fa-plus"></i> News', ['ksl-news-tbl/create/', 'tid'=>$model->tourn_id],$options = ['class' => 'btn btn-success btn-xs', 'data-toggle'=>'tooltip', 'aria-label'=>'Add News', 'data-original-title'=>'Add News', ]);

            },

        ]],

 

            ['class' => 'yii\grid\ActionColumn','header' => 'Action',],

        ],

    ]); ?>

    <?php Pjax::end(); ?>

    <?php 

	$form = ActiveForm::begin(['action' => ['tournament-schedule/drawsheetupload'],'id' => 'tournamend_galleryadd', 'options' => ['enctype' => 'multipart/form-data']]);

    Modal::begin(['header' => '<h3>Select Gallery </h3>', 'id' => 'modalgalleryselect', 'size' => 'modal-md', ]);

	echo "<div id='modalgalleryselect_result'>"; 

	echo "<div id='galleryselectdiv'></div>";

	echo  $form->field($model, 'galleryselect')->dropDownList(array(), ['prompt' => '--Select--', 'class' => 'form-control']);

    echo "</div>";

	echo "<div class='modal-footer'>";

	echo Html::button('<i class="fa fa-fw fa-save"></i> Save', ['class' => 'btn btn-primary','id' =>'gallery_save_id' ]);

	echo "<input id='tournament_id_gallery' name='tournament_id_gallery' type='hidden'>";

	echo "<button class='btn' data-dismiss='modal' aria-hidden='true'><i class='fa fa-fw fa-ban'></i> Close</button></div>";

	echo "</div>";

	Modal::end();

	ActiveForm::end(); ?>

</div>

</div>

</div>

</div>

<script>

	$(document).on('click', '.tm_gallerydata:button', function () {

		var btn_id=(this.id);

		var tournament_id = $('#'+btn_id).val();  

		$('#tournament_id_gallery').val(tournament_id);

		var PageUrl = '<?php echo Yii::$app->homeUrl . '?r=ksl-tournaments-tbl/galleryselect'; ?>';

             $.ajax({

                 url: PageUrl,

                 type: 'POST',                 

                 success: function (result) {

                 	$('#ksltournamentstbl-galleryselect').empty();

                 	$('#ksltournamentstbl-galleryselect').html(result);

                 	$('#modalgalleryselect').modal('show').find('#modalgalleryselect_result').load();

                 },

                 error: function (error) {

                     alert(error.responseText);

                 }

             });

	    	

	});

	$(document).on('click', '#gallery_save_id:button', function () {

		var gallery_id=$('#ksltournamentstbl-galleryselect').val();

		var tournament_id = $('#tournament_id_gallery').val();

		if(gallery_id!=""){

		var PageUrl = '<?php echo Yii::$app->homeUrl . '?r=ksl-tournaments-tbl/galleryselectsave'; ?>';

             $.ajax({

                 url: PageUrl,

                 type: 'POST',

                 data : { 'tournament_id' : tournament_id,'gallery_id':gallery_id},

                 success: function (result) {

                 	alert("Updated Successfully!");

                 	$('#modalgalleryselect').modal('hide').find('#modalgalleryselect_result').load();

                 },

                 error: function (error) {

                     alert(error.responseText);

                 }

             });

            }else{

            	alert("Please Select the Gallery!");

            }

	    	

	});





    $(document).ready(function(){



        /* When Model is in Page the User Profile option will not come for avoid that bug the below code has been resolved it. */

        $('.dropdown-toggle').click(function(){

             $('.navbar-nav > .user ').toggleClass("open");

            $(".navbar-nav > .user > .dropdown-toggle").attr("aria-expanded","true");

        });



        

        // Below Pagination class click for reload the page for ajax loading issue

        $('.pagination').click(function() {

            location.reload();

        });

    });

</script>

