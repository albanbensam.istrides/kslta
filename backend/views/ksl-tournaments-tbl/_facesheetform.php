<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $model backend\models\SliderBanner */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Facesheet Upload';
$this->params['breadcrumbs'][] = ['label' => 'Tournament', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
<!-- Info boxes -->
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="banner-form">
                    <div class="box-header with-border">
                     <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
<div class="facesheet-upload-form">

    <?php $form = ActiveForm::begin(['id' => 'faceheet', 
    								'options' => ['enctype' => 'multipart/form-data']
    								]); ?>

    <?= $form->field($model, 'tourn_factsheet')->fileInput(['id' => 'image']) ?>

 <div class="box-footer">
                        <div class="form-group pull-left">
                           
        <?= Html::submitButton($model->isNewRecord ? 'Create' : '<i class="fa fa-fw fa-upload"></i> Upload', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	   					</div>
                        <?php if($model->tourn_factsheet != "") { ?>
                        <div class="pull-right">                            
                            <?= Html::a('<i class="fa fa-fw fa-download"></i> Download',['downloadfile', 'file'=>$model->tourn_factsheet], $options = ['class' => 'btn btn-success btn-sm downloadfactsheet', 'target'=>"_blank" ])  ?>
							<?= Html::button('<i class="fa fa-fw fa-trash"></i> Delete', ['class' => 'btn btn-danger btn-sm actionchange',  'id'=>$model->tourn_id, 'value'=>$model->tourn_factsheet]) ?>
                        </div>
                        <?php } ?>
                    </div>
    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
</div>
</div>
</div>
</section>

<!--Modal Start -->
<?php 
    Modal::begin([
            'header' => '<h3>Confirmation</h3>',
            'id' => 'alertmodel',
            'size'=> 'modal-md',
        ]);

    echo "<div id='alertmodalContent'>
            <h4>Are you sure do you want to Delete ?</h4>
            <div class='modal-footer'>
                <input type='hidden' class='data1'>
                <button class='btn btn-primary newsChangeStatusConfirm'><i class='fa fa-fw fa-check-square-o'></i> Yes</button>
                <button class='btn' data-dismiss='modal' aria-hidden='true'><i class='fa fa-fw fa-ban'></i> No</button>

            </div> 
        </div>";

    Modal::end();
?>
<!--Modal End -->
<script>
$(document).ready(function(){
  //  alert();
});
$(document).on('click', '.actionchange', function () {
        var id = $(this).attr('id');
        $('.modal-footer > input').val(id);
        $('#alertmodel').modal('show').find('#alertmodalContent').load();

            
    });



    $(document).on('click', '.newsChangeStatusConfirm', function () {
        var id = $('.data1').attr('value');
            var PageUrl = '<?php echo Yii::$app->homeUrl . '?r=ksl-tournaments-tbl/visibleoperation&id='; ?>' + id;
            $.ajax({
                url: PageUrl,
                type: 'POST',
                success: function (result) {
                    if (result == "S")
                    {
                            window.location.href = '<?php echo Yii::$app->homeUrl . '?r=ksl-tournaments-tbl'; ?>';
                    }

                },
                error: function (error) {
                    //alert(error.responseText);
                }
            });
    });
</script>