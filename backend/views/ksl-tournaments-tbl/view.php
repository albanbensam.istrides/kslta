<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\KslTournamentsTbl */

$this->title = "Tournaments View";
$this->params['breadcrumbs'][] = ['label' => 'Tournaments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-tournaments-tbl-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->tourn_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->tourn_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php /* DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'tourn_id',
            'tourn_title',
            'tourn_category',
            'tourn_sub_category',
            'tourn_venue',
            'tourn_venue_address1',
            'tourn_venue_address2',
            'tourn_venue_city',
            'tourn_from_date',
            'tourn_to_date',
            'tourn_last_submission_date',
            'tourn_withdrl_date',
            'tourn_description',
            'tourn_asso_title',
            'tourn_asso_address1',
            'tourn_asso_address2',
            'tourn_asso_head',
            'tourn_refree1',
            'tourn_refree2',
            'tourn_refree3',
            'tourn_link',
            //'tourn_created_date',
            //'tourn_lastmodified_date',
            //'tourn_status',
            //'sponser_name_of_organisation',
            //'sponser_address1',
            //'sponser_address2',
            //'sponser_contact_person_name',
            //'sponser_phone',
            //'sponser_email:email',
            'tour_event_category',
            'tour_event_subcategory',
        ],
    ]) */ ?>

</div>


<section class="invoice">
          <!-- title row -->
          <div class="row">
            <div class="col-xs-12">
              <h2 class="page-header">
                <i class="fa fa-fw fa-trophy"></i> <?= $model->tourn_title ?>
                <small class="pull-right">Date: <?= $model->tourn_created_date ?></small>
              </h2>
            </div><!-- /.col -->
          </div>
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
              Tournaments Venue Address
              <address>
                <strong><?= $model->tourn_venue ?></strong><br>
                <?= $model->tourn_venue_address1 ?><br>
                <?= $model->tourn_venue_address2 ?><br>
                City: <?= $model->tourn_venue_city ?><br>
              </address>
            </div><!-- /.col -->
            <div class="col-sm-4 invoice-col">
              Association Address Details
              <address>
                <strong><?= $model->tourn_asso_title ?></strong><br>
                <?= $model->tourn_asso_address1 ?><br>
                <?= $model->tourn_asso_address2 ?><br>
                Head:  <?= $model->tourn_asso_head ?><br>
<!--                Email: john.doe@example.com-->
              </address>
            </div><!-- /.col -->
            <div class="col-sm-4 invoice-col">
              <b>Refree Details</b><br>
              <br>
              <b>referee 1:</b><?= $model->tourn_refree1 ?><br>
              <b>referee 2:</b> <?= $model->tourn_refree2 ?><br>
              <b>referee 3:</b> <?= $model->tourn_refree3 ?><br>
              <b>Link Address</b> <?= $model->tourn_link ?><br>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <!-- Table row -->
          <div class="row">
            <div class="col-xs-12 table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Tournaments From Date</th>
                    <th>Tournaments To Date</th>
                    <th>Tournaments Last Submission</th>
                    <th>Tournaments Withdraw</th>
                    <th>Description</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?= $model->tourn_from_date ?></td>
                    <td><?= $model->tourn_to_date ?></td>
                    <td><?= $model->tourn_last_submission_date ?></td>
                    <td><?= $model->tourn_withdrl_date ?></td>
                    <td><?= $model->tourn_description ?></td>
                  </tr>
<!--                  <tr>
                    <td>Need for Speed IV</td>
                    <td>247-925-726</td>
                    <td>Wes Anderson umami biodiesel</td>
                    <td>$50.00</td>
                  </tr>
                  <tr>
                    <td>Monsters DVD</td>
                    <td>735-845-642</td>
                    <td>Terry Richardson helvetica tousled street art master</td>
                    <td>$10.70</td>
                  </tr>
                  <tr>
                    <td>Grown Ups Blue Ray</td>
                    <td>422-568-642</td>
                    <td>Tousled lomo letterpress</td>
                    <td>$25.99</td>
                  </tr>-->
                </tbody>
              </table>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">
             <p class="lead">Tournaments Event Category</p>
              <div class="table-responsive">
                <table class="table">
                  <tr>
                    <th style="width:50%">Category:</th>
                    <td><?= $model->tourn_description==1?"Boys":"Girls" ?></td>
                  </tr>
                  
                </table>
              </div>
            </div><!-- /.col -->
            <div class="col-xs-6">
              <p class="lead">Tournaments Event Sub-Category</p>
              <div class="table-responsive">
                <table class="table">
                  <tr>
                    <th style="width:50%">Sub-Category:</th>
                    <td><?= $model->tour_event_subcategory==1?"Under 12":"Under 14" ?></td>
                  </tr>
                  
                </table>
              </div>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <!-- this row will not appear when printing -->
<!--          <div class="row no-print">
            <div class="col-xs-12">
              <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
              <button class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment</button>
              <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
            </div>
          </div>-->
        </section><!-- /.content -->
