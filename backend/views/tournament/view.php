<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Tournament */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tournaments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tournament-view">

     

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'id',
            'tour_name',
           //'tour_url',
           'order_no',
        ],
    ]) ?>

</div>
