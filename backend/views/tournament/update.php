<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Tournament */

$this->title = 'Update Tournament: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tournaments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tournament-update">
 

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
