<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\models\KslTournamentsTbl;
use backend\models\DropdownManagement;
use backend\models\PageContents;
use backend\models\CoachingPages;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model backend\models\Tournament */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="affilatedclubs-form">
<section class="content">
<!-- Info boxes -->
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="page-content-form">
                    <div class="box-header with-border">
                     <h3 class="box-title"><?= $model->isNewRecord ? '<i class="fa fa-fw fa-sticky-note-o"></i>' : '<i class="fa fa-fw fa-sticky-note"></i>' ?> <?= Html::encode($this->title) ?></h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-12">
		<div class="col-md-6">
			<?php $tName = ArrayHelper::map(KslTournamentsTbl::find()->orderBy(['tourn_id'=>'SORT_DESC'])->all(), 'tourn_title', 'tourn_title'); ?>
    <?php  echo $form->field($model, 'tour_name')->dropDownList($tName, 
                     ['prompt'=>'Choose',
                        //'style'=>'width:75%',
                      'onchange'=>'
          $.get( "'.Url::toRoute('tournament/lists').'", { id: $(this).val() })
         .done(function( data ) { $( "#tournament-tour_url" ).val( data ); } );'
]);  ?>
		</div>
			<div class="col-md-6">
						<!-- <?php
            $dataList=ArrayHelper::map(PageContents::find()->asArray()->all(), 'page_name', 'page_content_title');
			$dataList2=ArrayHelper::map(CoachingPages::find()->asArray()->all(), 'coaching_page_name', 'coaching_page_title');
			$output_data=array();			 
			$output_data['news']='News';
			foreach($dataList as $page_name =>$page_content_title){
				$output_data[$page_name]=$page_content_title;
			}
			foreach($dataList2 as $coaching_page_name =>$coaching_page_title){
				$output_data["".$coaching_page_name]=$coaching_page_title;
			}
            ?>
            <?= $form->field($model, 'tour_url')->dropDownList($output_data, 
                    ['prompt'=>'-----Select-----']) ?>
    			 -->
    			   <?= $form->field($model, 'order_no')->textInput(['maxlength' => true]) ?>
    			    <?= $form->field($model, 'tour_url')->hiddenInput()->label(false) ?>
                   
			
			</div>
 		</div>
	<div class="col-md-12">
		<div class="col-md-6"> 
   
    </div>
	<div class="col-md-6"> 
    <div class="form-group pull-right">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>
    <?php ActiveForm::end(); ?>

</div>
</div>

</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section> 