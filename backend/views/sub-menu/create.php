<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\SubMenu */

$this->title = 'Add Sub Menu';
$this->params['breadcrumbs'][] = ['label' => 'Sub Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-menu-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
