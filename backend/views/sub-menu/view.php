<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\SubMenu */

$this->title = $model->sub_menu_id;
$this->params['breadcrumbs'][] = ['label' => 'Sub Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-menu-view">


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'sub_menu_id',
           // 'menu_id',
            'sub_menu_name',
            'sub_menu_order_no',
            'sub_menu_link:ntext',
           // 'sub_menu_visible_status',
            'sub_menu_created_at',
            //'sub_menu_modified_at',
            //'sub_menu_status',
        ],
    ]) ?>

</div>
