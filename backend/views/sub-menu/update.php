<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\SubMenu */

$this->title = 'Update Sub Menu: ' . ' ' . $model->sub_menu_name;
$this->params['breadcrumbs'][] = ['label' => 'Sub Menus', 'url' => ['index']];

$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sub-menu-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
