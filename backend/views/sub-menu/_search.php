<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SubMenuSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-menu-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'sub_menu_id') ?>

    <?= $form->field($model, 'menu_id') ?>

    <?= $form->field($model, 'sub_menu_name') ?>

    <?= $form->field($model, 'sub_menu_order_no') ?>

    <?= $form->field($model, 'sub_menu_link') ?>

    <?php // echo $form->field($model, 'sub_menu_visible_status') ?>

    <?php // echo $form->field($model, 'sub_menu_created_at') ?>

    <?php // echo $form->field($model, 'sub_menu_modified_at') ?>

    <?php // echo $form->field($model, 'sub_menu_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
