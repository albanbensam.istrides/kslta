<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\MainMenu;
use backend\models\PageContents;
use backend\models\CoachingPages;
/* @var $this yii\web\View */
/* @var $model backend\models\NasaCollaborations */
/* @var $form yii\widgets\ActiveForm */
?>
<section class="content">
<!-- Info boxes -->
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
            <div class="box-header with-border">
                     <h3 class="box-title"><?= $model->isNewRecord ? '<i class="fa fa-fw fa-calendar-plus-o"></i>' : '<i class="fa fa-fw fa-edit"></i>' ?>  <?= Html::encode($this->title) ?></h3>
              </div><!-- /.box-header -->
<div class="nasa-collaborations-form">
<div class="box-body">
    <?php $form = ActiveForm::begin(['id' => 'statusuploadsave']); ?>


     <div class="col-md-12">
     <div class="form-group col-md-6">
        <?php
            $dataList=ArrayHelper::map(MainMenu::find()->asArray()->all(), 'menu_id', 'menu_name');
            ?>
            <?= $form->field($model, 'menu_id')->dropDownList($dataList, 
                    ['prompt'=>'-----Select a Main Menu-----']) ?>
        </div>
      </div>

    <div class="col-md-12">
        <div class="form-group col-md-6">
         <?= $form->field($model, 'sub_menu_name')->textInput(['maxlength' => true]) ?>
        </div>
   </div>

   <div class="col-md-12">
       <div class="form-group col-md-6">
          <?= $form->field($model, 'sub_menu_order_no')->textInput(['maxlength' => true]) ?>
        </div>
   </div>

    <div class="col-md-12">
       <div class="form-group col-md-6">
             <?php
            $dataList=ArrayHelper::map(PageContents::find()->asArray()->all(), 'page_name', 'page_content_title');
			$dataList2=ArrayHelper::map(CoachingPages::find()->asArray()->all(), 'coaching_page_name', 'coaching_page_title');
			$output_data=array();			
			/*$output_data['about-kslta']='ABOUT KSLTA';
			$output_data['barrestaurant']='KSLTA BAR & FACILITIES';
			$output_data['coaching/party_hall']='PARTY HALL FACILITIES';
			$output_data['guestroom']='GUEST ROOMS TERMS';
			$output_data['swimmingpool']='SWIMMING CLASS TIMING';
			$output_data['gym']='GYM';
			$output_data['tennis-billiards']='TABLE TENNIS & BILLIARDS';
			$output_data['affilated-clubs']='AFFILATED CLUBS';
			$output_data['commitee']='COMMITTEE MEMBERS';
			$output_data['cubbonparkbangalore']='CUBBON PARK & BANGALORE';*/
			//$output_data['contact']='CONTACT US';
			$output_data['news']='News';
			foreach($dataList as $page_name =>$page_content_title){
				$output_data[$page_name]=$page_content_title;
			}
			foreach($dataList2 as $coaching_page_name =>$coaching_page_title){
				$output_data["".$coaching_page_name]=$coaching_page_title;
			}
            ?>
            <?= $form->field($model, 'sub_menu_link')->dropDownList($output_data, 
                    ['prompt'=>'-----Select a Main Menu-----']) ?>
                    
                    
       </div>
    </div>
<div class="box-footer pull-right">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<i class="fa fa-fw fa-save"></i> Save' : '<i class="fa fa-fw fa-edit"></i> Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>
    <?php ActiveForm::end(); ?>

</div>

</div>
</div>
</div>
</div>
</section>
