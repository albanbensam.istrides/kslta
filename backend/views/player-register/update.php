<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PlayerRegister */

$this->title = 'Update Player';
$this->params['breadcrumbs'][] = ['label' => 'Player Registers', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="player-register-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
