<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PlayerRegisterSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="player-register-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'reg_id') ?>

    <?= $form->field($model, 'tourn_id') ?>

    <?= $form->field($model, 'reg_first_name') ?>

    <?= $form->field($model, 'reg_last_name') ?>

    <?= $form->field($model, 'reg_dob') ?>

    <?php // echo $form->field($model, 'reg_email') ?>

    <?php // echo $form->field($model, 'reg_player_category') ?>

    <?php // echo $form->field($model, 'reg_player_id') ?>

    <?php // echo $form->field($model, 'reg_city') ?>

    <?php // echo $form->field($model, 'reg_mobile') ?>

    <?php // echo $form->field($model, 'registration_date') ?>

    <?php // echo $form->field($model, 'reg_created_at') ?>

    <?php // echo $form->field($model, 'reg_last_modified_at') ?>

    <?php // echo $form->field($model, 'reg_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
