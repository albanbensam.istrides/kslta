<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductSolutionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Player Register';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/datatables/dataTables.bootstrap.css">
<script src="<?php echo Url::base(); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>


<div class="player-registeration-index">
    <div class="box-body">
    <div class="box box-primary cgridoverlap">
                <div class="box-header with-border">

    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

    
    </div><!-- /.box-header -->
                  <div class="box-body">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('<i class="fa fa-plus"></i> Add Player', ['create'], ['class' => 'btn btn-primary btn-md pull-right ']) ?>
    </p>
   <?php Pjax::begin(); ?>
         <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'reg_id',
            'reg_player_id',
            /*[
              'attribute'=>'tourn_id',
              'label'=>'Tournament Title',
              'format'=>'text',//raw, html
              'content'=>function($data){
                  return $data->getParentName();
                  }
            ],*/
            //'tourn_id',
            'reg_first_name',
            'reg_last_name',
			'reg_gender',
            'reg_dob',
             'reg_email:email',
            // 'reg_player_category',
            // 'reg_player_id',
            // 'reg_city',
            // 'reg_mobile',
            // 'registration_date',
            // 'reg_created_at',
            // 'reg_last_modified_at',
            // 'reg_status',
            ['class' => 'yii\grid\ActionColumn'],
            /*['class' => 'yii\grid\ActionColumn',
                          'template'=>'{create}{view}{delete}',
                            'buttons'=>[
                              'create' => function ($url, $model) {     
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                        'title' => Yii::t('yii', 'Create'),
                                        'class' => 'btn btn-primary btn-xs gridbtncustom',
                                        'data-toggle'=> 'tooltip',
                                        'title' => 'Create'
                                ]);                                
            
                              }, 
                                'view' => function ($url, $model) {     
                                return Html::a('<span class="fa fa-edit"></span>', $url, [
                                        'title' => Yii::t('yii', 'Create'),
                                        'class' => 'btn btn-warning btn-xs gridbtncustom',
                                        'data-toggle'=> 'tooltip',
                                        'title' => 'View'
                                ]);                                
            
                              }, 
                              'delete' => function ($url, $model) {     
                                return Html::a('<span class="fa fa-trash"></span>', $url, [
                                        'title' => Yii::t('yii', 'Create'),
                                        'class' => 'btn btn-default btn-xs gridbtncustom',
                                        'data-toggle'=> 'tooltip',
                                        'title' => 'Delete'
                                ]);                                
            
                              }


                          ]                            
                            ], */

        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
</div>
</div>
</div>

