<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\PlayerRegister */

$this->title = 'Player Registeration';
$this->params['breadcrumbs'][] = ['label' => 'Player Registers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="player-register-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
