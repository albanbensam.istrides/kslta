<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\PlayerRegister */

$this->title = '';
$this->params['breadcrumbs'][] = ['label' => 'Player Registers', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Player Details';
?>

<div class="box box-primary">
    <div class="box-header with-border">
    <h3 class="box-title">Player Details</h3>
    </div>
    <div class="box-body">
<div class="player-register-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->reg_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->reg_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'reg_id',
            'tourn_id',
            'reg_first_name',
            'reg_last_name',
			'reg_gender',
            'reg_dob',
            'reg_email:email',
            'reg_player_category',
            'reg_player_id',
            'reg_city',
            'reg_mobile',
            'registration_date',
            'reg_created_at',
            'reg_last_modified_at',
            'reg_status',
        ],
    ]) ?>

</div>
</div>
</div>
