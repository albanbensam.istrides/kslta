<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\DropdownManagement;
use backend\models\KslTournamentsTbl;
use backend\models\TournamentEventModel;
use backend\models\PlayerEvents;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\PlayerRegister */
/* @var $form yii\widgets\ActiveForm */

?>
<script src="<?php echo Url::base(); ?>/plugins/multiselect/jquery_1.9.0.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/multiselect/jquery.sumoselect.js"></script>
<link href="<?php echo Url::base(); ?>/plugins/multiselect/sumoselect.css" rel="stylesheet" />
	
<section class="content">
<!-- Info boxes -->
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                     <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div><!-- /.box-header -->
            <div class="player-register-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
    	<!-- <div class="col-md-12">
                        <div class="form-group col-md-6">
                            <?php $tournList=ArrayHelper::map(KslTournamentsTbl::find()->asArray()->all(), 'tourn_id', 'tourn_title'); ?>
                       		<?php 
                       		 echo $form->field($model, 'tourn_id')->dropDownList(
							                $tournList,
							                [
							                    'prompt'=>'Select Tournament',
							                    'class' => 'form-control',
							                    'onchange'=>'
							                        $.get( "'.Url::toRoute('eventvalue').'", { id: $(this).val() } )
							                            .done(function( data ) {
							                                $( "#'.Html::getInputId($model, 'tourn_event_id').'" ).html( data );															
															$(".groups_eg_g").SumoSelect({selectAll:true, search:false });
							                            }
							                        );'
							                ]
							                
							        );		
		$selected_event = ArrayHelper::map(PlayerEvents::find()->where(['ue_tournamentid' => $model->tourn_id,'ue_userid'=>$model->reg_id])->all(), 'ue_autoid', 'ue_eventid');							
		$user_event=array();
		$rows=TournamentEventModel::find()->where(['tourn_id' => $model->tourn_id])->all();
		$id_db_cat=array();
		if(count($rows)>0){
        	$id_db_all=array();
        	foreach($rows as $one_event){
				$id_db_all[]=$one_event->event_category;
				$id_db_all[]=$one_event->event_subcategory;
				$id_db_cat[$one_event->event_category][$one_event->event_id]=$one_event->event_subcategory;																									
			}
			
			if(count($id_db_all)>0){
				$event_names = ArrayHelper::map(DropdownManagement::find()->FilterWhere(array('in', 'dropdown_id', $id_db_all))->all(), 'dropdown_id', 'dropdown_value');
			}
		}
			foreach($id_db_cat as $one_categoryid=>$one_subcategory){
				$one_category_txt='';
				if(isset($event_names[$one_categoryid])){
					$one_category_txt=$event_names[$one_categoryid];
				}	
						foreach($one_subcategory as $event_id=>$one_event){
						$one_subcategory_txt='';
						if(isset($event_names[$one_event])){
							$one_subcategory_txt=$event_names[$one_event];
						}
						$user_event[$one_category_txt][$event_id]=$one_category_txt.' '.$one_subcategory_txt;		              
		            }				
			}
			$model->tourn_event_id=$selected_event;
							?>
						</div>
						<div class="form-group col-md-6">
						<script>$(".groups_eg_g").SumoSelect({selectAll:true, search:false });</script>
						<label>Event List</label><br/>
						<?php echo $form->field($model, 'tourn_event_id')->dropDownList($user_event,['multiple'=>'multiple','size'=>'1','class'=>'groups_eg_g form-control'])->label(false);  
			
          ?>
						</div>
					</div>-->
					<div class="col-md-12">
                        <div class="form-group col-md-6">
                                <?= $form->field($model, 'reg_first_name')->textInput(['maxlength' => true, 'placeholder' => 'First Name']) ?>

                        </div>
                        <div class="form-group col-md-6">
                                <?= $form->field($model, 'reg_last_name')->textInput(['maxlength' => true, 'placeholder' => 'Last Name']) ?>

                        </div>
					</div>
					<div class="col-md-12">
                        <div class="form-group col-md-6">
                         <?= $form->field($model, 'reg_dob')->textInput(['class' => 'form-control datepicker', 'placeholder' => 'DD-MM-YYYY', 'bootstrap-datepicker data-date-autoclose' => "true", 'data-date-end-date' => "today", 'data-required' => "true", 'data-provide' => "datepicker", 'data-date-format' => "dd-mm-yyyy"]) ?>
                            
                        </div>
                        <div class="form-group col-md-6">
                            <?= $form->field($model, 'reg_email')->textInput(['maxlength' => true, 'placeholder' => 'Email id']) ?>
                        </div>
						</div>
						<div class="col-md-12">
                        <div class="form-group col-md-6">
                            <?php $list1 = ArrayHelper::map(DropdownManagement::find()->where(['dropdown_key' => 'tourmament_event_category'])->orderBy('dropdown_order')->all(), 'dropdown_id', 'dropdown_value'); ?>
                            <?= $form->field($model, 'reg_player_category')->dropDownList($list1, ['prompt' => 'Select']) ?>
                        </div>
                        <div class="form-group col-md-6">
                            <?= $form->field($model, 'reg_player_id')->textInput(['maxlength' => true, 'placeholder' => 'Player Unique Id']) ?>
                        </div>
						</div>
					<div class="col-md-12">

                        <div class="form-group col-md-6">
                            <?= $form->field($model, 'reg_city')->textInput(['maxlength' => true, 'placeholder' => 'City']) ?>
                        </div>
                        <div class="form-group col-md-6">
                            <?= $form->field($model, 'reg_mobile')->textInput(['maxlength' => true, 'placeholder' => 'Mobile Number']) ?>
                        </div>
						</div>
						<div class="col-md-12">

                        <div class="form-group col-md-6">
                            <?= $form->field($model, 'reg_gender')->dropDownList(['male' => 'Male', 'female' => 'Female', ],['prompt'=>'Select...']) ?>
                        </div>
                        
						</div>
					

                        <div class="box-footer pull-right">
                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? 'Register' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>
                    </div>
  

    <?php ActiveForm::end(); ?>

</div>
</div></div></div></div>
</section>



<script type="text/javascript">
   // $(".datepicker").datepicker();
    $(".groups_eg_g").SumoSelect({selectAll:true, search:false });
    </script>