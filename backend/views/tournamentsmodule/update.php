<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Tournamentsmodule */

$this->title = 'Update Tournamentsmodule: ' . ' ' . $model->tourn_id;
$this->params['breadcrumbs'][] = ['label' => 'Tournamentsmodules', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tourn_id, 'url' => ['view', 'id' => $model->tourn_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tournamentsmodule-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
