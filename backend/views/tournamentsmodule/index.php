<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TournamentsmoduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tournamentsmodules';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tournamentsmodule-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tournamentsmodule', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'tourn_id',
            'tourn_title',
            'tourn_category',
            'tourn_sub_category',
            'tourn_venue',
            // 'tourn_venue_address1',
            // 'tourn_venue_address2',
            // 'tourn_venue_city',
            // 'tourn_from_date',
            // 'tourn_to_date',
            // 'tourn_last_submission_date',
            // 'tourn_withdrl_date',
            // 'tourn_description',
            // 'tourn_asso_title',
            // 'tourn_asso_address1',
            // 'tourn_asso_address2',
            // 'tourn_asso_head',
            // 'tourn_refree1',
            // 'tourn_refree2',
            // 'tourn_refree3',
            // 'tourn_link',
            // 'tourn_created_date',
            // 'tourn_lastmodified_date',
            // 'tourn_status',
            // 'sponser_name_of_organisation',
            // 'sponser_address1',
            // 'sponser_address2',
            // 'sponser_contact_person_name',
            // 'sponser_phone',
            // 'sponser_email:email',
            // 'tour_event_category',
            // 'tour_event_subcategory',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
