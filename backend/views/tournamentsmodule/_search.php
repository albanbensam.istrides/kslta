<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TournamentsmoduleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tournamentsmodule-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'tourn_id') ?>

    <?= $form->field($model, 'tourn_title') ?>

    <?= $form->field($model, 'tourn_category') ?>

    <?= $form->field($model, 'tourn_sub_category') ?>

    <?= $form->field($model, 'tourn_venue') ?>

    <?php // echo $form->field($model, 'tourn_venue_address1') ?>

    <?php // echo $form->field($model, 'tourn_venue_address2') ?>

    <?php // echo $form->field($model, 'tourn_venue_city') ?>

    <?php // echo $form->field($model, 'tourn_from_date') ?>

    <?php // echo $form->field($model, 'tourn_to_date') ?>

    <?php // echo $form->field($model, 'tourn_last_submission_date') ?>

    <?php // echo $form->field($model, 'tourn_withdrl_date') ?>

    <?php // echo $form->field($model, 'tourn_description') ?>

    <?php // echo $form->field($model, 'tourn_asso_title') ?>

    <?php // echo $form->field($model, 'tourn_asso_address1') ?>

    <?php // echo $form->field($model, 'tourn_asso_address2') ?>

    <?php // echo $form->field($model, 'tourn_asso_head') ?>

    <?php // echo $form->field($model, 'tourn_refree1') ?>

    <?php // echo $form->field($model, 'tourn_refree2') ?>

    <?php // echo $form->field($model, 'tourn_refree3') ?>

    <?php // echo $form->field($model, 'tourn_link') ?>

    <?php // echo $form->field($model, 'tourn_created_date') ?>

    <?php // echo $form->field($model, 'tourn_lastmodified_date') ?>

    <?php // echo $form->field($model, 'tourn_status') ?>

    <?php // echo $form->field($model, 'sponser_name_of_organisation') ?>

    <?php // echo $form->field($model, 'sponser_address1') ?>

    <?php // echo $form->field($model, 'sponser_address2') ?>

    <?php // echo $form->field($model, 'sponser_contact_person_name') ?>

    <?php // echo $form->field($model, 'sponser_phone') ?>

    <?php // echo $form->field($model, 'sponser_email') ?>

    <?php // echo $form->field($model, 'tour_event_category') ?>

    <?php // echo $form->field($model, 'tour_event_subcategory') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
