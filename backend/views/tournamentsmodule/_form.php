<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Tournamentsmodule */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tournamentsmodule-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tourn_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tourn_category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tourn_sub_category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tourn_venue')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tourn_venue_address1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tourn_venue_address2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tourn_venue_city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tourn_from_date')->textInput() ?>

    <?= $form->field($model, 'tourn_to_date')->textInput() ?>

    <?= $form->field($model, 'tourn_last_submission_date')->textInput() ?>

    <?= $form->field($model, 'tourn_withdrl_date')->textInput() ?>

    <?= $form->field($model, 'tourn_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tourn_asso_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tourn_asso_address1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tourn_asso_address2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tourn_asso_head')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tourn_refree1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tourn_refree2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tourn_refree3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tourn_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tourn_created_date')->textInput() ?>

    <?= $form->field($model, 'tourn_lastmodified_date')->textInput() ?>

    <?= $form->field($model, 'tourn_status')->dropDownList([ 'A' => 'A', 'R' => 'R', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'sponser_name_of_organisation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sponser_address1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sponser_address2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sponser_contact_person_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sponser_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sponser_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tour_event_category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tour_event_subcategory')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
