<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Tournamentsmodule */

$this->title = 'Create Tournamentsmodule';
$this->params['breadcrumbs'][] = ['label' => 'Tournamentsmodules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tournamentsmodule-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
