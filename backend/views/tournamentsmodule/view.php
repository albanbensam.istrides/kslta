<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Tournamentsmodule */

$this->title = $model->tourn_id;
$this->params['breadcrumbs'][] = ['label' => 'Tournamentsmodules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tournamentsmodule-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->tourn_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->tourn_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tourn_id',
            'tourn_title',
            'tourn_category',
            'tourn_sub_category',
            'tourn_venue',
            'tourn_venue_address1',
            'tourn_venue_address2',
            'tourn_venue_city',
            'tourn_from_date',
            'tourn_to_date',
            'tourn_last_submission_date',
            'tourn_withdrl_date',
            'tourn_description',
            'tourn_asso_title',
            'tourn_asso_address1',
            'tourn_asso_address2',
            'tourn_asso_head',
            'tourn_refree1',
            'tourn_refree2',
            'tourn_refree3',
            'tourn_link',
            'tourn_created_date',
            'tourn_lastmodified_date',
            'tourn_status',
            'sponser_name_of_organisation',
            'sponser_address1',
            'sponser_address2',
            'sponser_contact_person_name',
            'sponser_phone',
            'sponser_email:email',
            'tour_event_category',
            'tour_event_subcategory',
        ],
    ]) ?>

</div>
