<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\GalleryPageModel;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model backend\models\GalleryPageModel */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="<?php echo Url::base(); ?>/plugins/jQueryUI/jquery-ui.js"></script>
<div style="margin-left: 0px;" >
							
<ul id="my_list_img">
<?php 
$request = Yii::$app->request;
$gallery_id = $request->get('id', 1); 
$gallery_img = GalleryPageModel::find()->where(['gal_image_rootid' => $gallery_id])->orderBy('gal_image_order')->all();
foreach($gallery_img as $one_img){
	
echo '<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3" id="ss_'.$one_img->gal_image_id.'">
						<li class="ui-state-default imgContent" id="dd_'.$one_img->gal_image_id.'">
						<i class="fa fa-times-circle" style="font-size:17px;" id="'.$one_img->gal_image_id.'" name=""galleryimage/gallery_pages/'.$one_img->gal_image_path.'""></i></a>
						<img width="100%" height="200" name="galleryimage/gallery_pages/'.$one_img->gal_image_path.'"  src="galleryimage/gallery_pages/'.$one_img->gal_image_path.'">'.ltrim($gallery_id."_",$one_img->gal_image_path).'</li></div>';
						
}
						 ?>
<!--<li class="ui-state-default"><img width="250" height="200"  src="">
	</li>
-->

</ul>

</div>
<script>
						
							 	
							 	
								$(function() {
									jQuery("#my_list_img").sortable({
    	 							placeholder: "ui-state-default",
    								update: function( event, ui ) {
        								var sorted = $( "#my_list_img" ).sortable( "serialize");
        								$.ajax({
										  type: "POST",
										  url: '<?php echo Yii::$app->homeUrl . '?r=gallery-page/updateimgorder'; ?>',
										  data: sorted,
										  success: function(ret){
										  //alert(ret);
										  //alert("Success.");
										  }
										  
										});
    									}
        
    								});
									$("#sortable").disableSelection();
								});
								
							</script>
							
<style>
	.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
		background: none;
		border: none;
		font-weight: normal;
		list-style: outside none none;
		float: left;
		width: 24%;
		box-shadow: 10px 10px 5px #888;
		min-width: 250px;
	}
	/* prograss bar */

#output{
	padding: 5px;
	font-size: 12px;
}
#progressbox {
	border: 1px solid #CAF2FF;
	padding: 1px; 
	position:relative;
	width:400px;
	border-radius: 3px;
	margin: 10px;
	display:none;
	text-align:left;
}
#progressbar {
	height:20px;
	border-radius: 3px;
	background-color: #CAF2FF;
	width:1%;
}
#statustxt {
	top:3px;
	left:50%;
	position:absolute;
	display:inline-block;
	color: #FFFFFF;
}
</style>