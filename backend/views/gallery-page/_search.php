<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\GalleryPageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gallery-page-model-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'gal_image_id') ?>

    <?= $form->field($model, 'gal_image_rootid') ?>

    <?= $form->field($model, 'gal_image_path') ?>

    <?= $form->field($model, 'gal_image_order') ?>

    <?= $form->field($model, 'gal_image_created') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
