<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\GalleryPageModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gallery-page-model-form">

    <?php $form = ActiveForm::begin([
            'id' => 'news-active-form',
            'options' => ['enctype' => 'multipart/form-data'],
        ]); ?>	    
	<?= $form->field($model, 'gallery_multi_images[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>
	

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Upload' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
