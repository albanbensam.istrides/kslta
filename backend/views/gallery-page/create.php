<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model backend\models\GalleryPageModel */

$this->title = 'Gallery Page';
$this->params['breadcrumbs'][] = ['label' => 'Gallery', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="gallery-page-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
 <?= $this->render('_imageview', [
        'model' => $model,
    ]) ?>
<div class="tab-pane" id="tab_2">


<!--Image View Modal Start -->
<?php 
    Modal::begin([
            'header' => '<h3>Content Image View</h3>',
            'id' => 'alertmodel',
            'size'=> 'modal-md',
        ]);

    echo "<div id='alertmodalContent'>
            <img id='bannerimg' src='' width='566' height='350'/>
            <div class='modal-footer'>
                <input type='hidden' class='data1'>
               <a class='btn btn-success btn-sm downloadfactsheet' href='' target='_blank'><i class='fa fa-fw fa-download'></i> Download</a>

                <button class='btn' data-dismiss='modal' aria-hidden='true'><i class='fa fa-fw fa-close'></i> Close</button>
            </div> 
        </div>";

    Modal::end();
?>
<!--Image View Modal End -->





<!--Confirmation Modal Start -->
<?php 
    Modal::begin([
            'header' => '<h3>Confirmation</h3>',
            'id' => 'alertmodel1',
            'size'=> 'modal-md',
        ]);

    echo "<div id='alertmodalContent1'>
            <h4>Are you sure do you want to <span style='color:red'>DELETE</span> this item?</h4>
            <div class='modal-footer'>
                <input type='hidden' class='data1'>
                <button class='btn btn-primary newsChangeStatusConfirm1'><i class='fa fa-fw fa-check-square-o'></i> Yes</button>
                <button class='btn' data-dismiss='modal' aria-hidden='true'><i class='fa fa-fw fa-ban'></i> No</button>

            </div> 
        </div>";

    Modal::end();
?>
<!--Confirmation Modal End -->




<script type="text/javascript">
$(document).ready(function(){
	//$('.imgContent > img').css('cursor','pointer');
       $('.imgContent > i').css('cursor', 'pointer');
     
});


/* Image Model View */
  $(document).on('click', '.imgContent > img', function(){
            
         var url = $(this).attr('src');
         var imgUrl = '<?php echo Url::base(); ?>' + '/' + url;
         var contentData2 = $(this).attr('name');
         $('#bannerimg').attr('src',imgUrl);
        $('.downloadfactsheet').attr('href', contentData2);
         $('#alertmodel').modal('show').find('#alertmodalContent').load();
    
});
/* Image Model view End */

$(document).on('click', '.imgContent > i', function () {
        var id = $(this).attr('id');
        
        $('.modal-footer > input').val(id);
         
        $('#alertmodel1').modal('show').find('#alertmodalContent1').load();
    });

$(document).on('click', '.newsChangeStatusConfirm1', function () {
        var id = $('.data1').attr('value');
        var contentId = '&content=' + <?php echo $_GET['id']; ?>;
            var PageUrl = '<?php echo Yii::$app->homeUrl . '?r=gallery-page/deletedata&id='; ?>' + id + contentId ;
            $.ajax({
                url: PageUrl,
                type: 'POST',
                success: function (result) {
                    if (result == "S")
                    {
                        try {
                               location.reload();

                        } catch (e) {
                                location.reload();

                        }
                        return false;
                    }

                },
                error: function (error) {
                    alert(error.responseText);
                }
            });
    });
    </script>