<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\GalleryPageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gallery Page Models';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-page-model-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Gallery Page Model', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'gal_image_id',
            'gal_image_rootid',
            'gal_image_path',
            'gal_image_order',
            'gal_image_created',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
