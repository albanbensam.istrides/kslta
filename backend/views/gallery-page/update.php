<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\GalleryPageModel */

$this->title = 'Update Gallery Page Model: ' . ' ' . $model->gal_image_id;
$this->params['breadcrumbs'][] = ['label' => 'Gallery Page Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->gal_image_id, 'url' => ['view', 'id' => $model->gal_image_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="gallery-page-model-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
