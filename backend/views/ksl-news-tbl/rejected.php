<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\bootstrap\Modal;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\KslNewsTblSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rejected News List';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .mygridcustom{ 
        height: 530px;
        overflow: scroll;

    }

    .timeline::before {
        background: #ECF0F5 none repeat scroll 0 0;
        border-radius: 2px;
        bottom: 0;
        content: "";
        left: -7px;
        margin: 0;
        position: absolute;
        top: 0;
        width: 4px;
    }

    .modal-header > h3{
        margin-top: 1px;
        margin-bottom: -6px;

    }

   /* .ksltaheading{
    background: rgba(60, 141, 188, 0.86);
    border:1px solid #aaa;
    margin:10px;
    padding:4px;
    width:330px;
    
    -webkit-border-radius:7px; 
        -moz-border-radius:7px; 
    }*/
</style>
<div class="ksl-news-tbl-index">

    <h2 class="ksltaheading"><i class="fa fa-fw fa-ban"></i> <?= Html::encode($this->title) ?></h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> Add News', ['create'], ['class' => 'btn btn-primary btn-md pull-right ']) ?>
    </p>
    <div class="col-md-12 mygridcustom">
        <!-- The time line -->
        <ul class="timeline">

            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'id' => 'news-list-data',
                'itemOptions' => ['class' => 'item'],
                /* 'itemView' => function ($model, $key, $index, $widget) {
                  return Html::a(Html::encode($model->news_id), ['view', 'id' => $model->news_id]).$model->news_title;
                  }, */
                'itemView' => '_newsview',
            ])
            ?>

        </ul>
    </div><!-- /.col -->


</div>


<!--Modal Start -->
<?php 
    Modal::begin([
            'header' => '<h3>Confirmation</h3>',
            'id' => 'modal',
            'size'=> 'modal-md',
        ]);

    echo "<div id='modalContent'>
            <h4>Are you sure do you want to unapprove ?</h4>
            <div class='modal-footer'>
                <input type='hidden' class='data1'>
                <button class='btn btn-primary newsChangeStatusConfirm'><i class='fa fa-fw fa-check-square-o'></i> Yes</button>
                <button class='btn' data-dismiss='modal' aria-hidden='true'><i class='fa fa-fw fa-ban'></i> No</button>
            </div> 
        </div>";

    Modal::end();
?>
<!--Modal End -->

<script>

    // Configure/customize these variables.
    var showChar = 250;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Show more >";
    var lesstext = "Show less";


    $('.more').each(function () {
        var content = $(this).html();

        if (content.length > showChar) {

            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);

            var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';


            $(this).html(html);
        }

    });

    $(".morelink").click(function () {
        if ($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });

    /* Change Status Ajax request functionality */
    $(document).on('click', '.newsstatuschange > label', function () {
        var txt;
        var id = $(this).attr('name');
        $('.modal-footer > input').val(id);
        $('#modal').modal('show').find('#modalContent').load();
    });

    $(document).on('click', '.newsChangeStatusConfirm', function () {
        var id = $('.data1').attr('value');
            var PageUrl = '<?php echo Yii::$app->homeUrl . '?r=ksl-news-tbl/activatenews&id='; ?>' + id;
            $.ajax({
                url: PageUrl,
                type: 'POST',
                success: function (result) {
                    if (result == "S")
                    {
                        try {
                            $.fn.yiiGridView.update('#news-list-data');
                        } catch (e) {
                            window.location.href = window.location.href;
                        }
                        return false;
                    }

                },
                error: function (error) {
                    alert(error.responseText);
                }
            });
    });


    $(document).ready(function(){

        /* When Model is in Page the User Profile option will not come for avoid that bug the below code has been resolved it. */
        $('.dropdown-toggle').click(function(){
             $('.navbar-nav > .user ').toggleClass("open");
            $(".navbar-nav > .user > .dropdown-toggle").attr("aria-expanded","true");
        });
       
    });
</script>



