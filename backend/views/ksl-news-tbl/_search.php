<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\KslNewsTblSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ksl-news-tbl-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'globalSearch') ?>

    <?php // echo $form->field($model, 'news_content_image') ?>

    <?php // echo $form->field($model, 'news_editor_content') ?>

    <?php // echo $form->field($model, 'news_posted_by') ?>

    <?php // echo $form->field($model, 'news_from_date') ?>

    <?php // echo $form->field($model, 'news_to_date') ?>

    <?php // echo $form->field($model, 'news_created_date') ?>

    <?php // echo $form->field($model, 'news_lastupdated_date') ?>

    <?php // echo $form->field($model, 'news_flat') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
