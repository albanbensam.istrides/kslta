

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\KslNewsTbl */
/* @var $form yii\widgets\ActiveForm */

?>
<script src="<?php echo Url::base(); ?>/plugins/tinymce/tinymce.min.js" type="text/javascript"></script>

<section class="content">
    <?php
    $form = ActiveForm::begin([
                'id' => 'news-active-form',
                'options' => ['enctype' => 'multipart/form-data'],
    ]);
    ?>
    <?php 
    if($model->news_tournament_id!=""){
    	echo $form->field($model, 'news_tournament_id')->hiddenInput(['id' => 'news_tournament_id','value'=>$model->news_tournament_id])->label(false);
	} ?>
    <!-- Info boxes -->
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">

                     <h3 class="box-title">
                      <?php if($model->dummy_tourn_title == '') { ?>
                        <?= $model->isNewRecord ? '<i class="fa fa-fw fa-sticky-note-o"></i>' : '<i class="fa fa-fw fa-sticky-note"></i>' ?> <?= Html::encode($this->title) ?></h3>

                        <?php } else { ?>
                            <?= $model->isNewRecord ? '<i class="fa fa-fw fa-sticky-note-o"></i>' : '<i class="fa fa-fw fa-sticky-note"></i>' ?> <?= 'Add Tournament News For <b>'.$model->dummy_tourn_title.'</b>' ?></h3>

                        <?php } ?>

                        <p>
                        <?php echo isset($_GET['tid']) ? Html::a('<i class="fa fa-fw fa-mail-reply"></i> Go Back', ['ksl-tournaments-tbl/tournamentnews'], ['class' => 'btn btn-primary btn-md pull-right ']) : '' ?>
                        </p>
                    </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" class="ksl-news-tbl-form">
                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= $form->field($model, 'news_title')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'News Title']) ?>
                            </div>
                            <div class="form-group">
                                <?= $form->field($model, 'news_link_address')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'www.example.com']) ?>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <?= $form->field($model, 'news_display_tag')->dropDownList([ 'Home' => 'Home', 'Others' => 'Others'], ['prompt' => '--Select--', 'class' => 'form-control']) ?>
                            </div>
<!--                            <div class="form-group">
                                $form->field($model, 'news_link_status')->radioList(array('A' => 'Active', 'I' => 'Inactive')); 
                            </div>-->
                        </div>

                        <div class='col-md-12'>
                            <div class="form-group">
                                <?= $form->field($model, 'news_content_image')->fileInput(['id' => 'image']) ?>
                                <p class="help-block">possible to choose one images.</p>
                            </div>
                            <div class="form-group">
                                <?= $form->field($model, 'news_editor_content')->textarea(['name' => 'news_editor_content', 'class' => 'form-control contentEditor']) ?>
                            </div>
                        </div><!-- /.box-body -->

                        <div class='col-md-6'>
                            <div class="form-group">
                                <?= $form->field($model, 'news_located_city')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'City']) ?>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <?= $form->field($model, 'news_located_country')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'Country']) ?>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <?= $form->field($model, 'news_posted_by')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'Author Name']) ?>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <?= $form->field($model, 'news_posted_at')->textInput(['maxlength' => true, 'class' => 'form-control datepicker', 'placeholder' => 'DD-MM-YYYY', 'bootstrap-datepicker data-date-autoclose' => "true", 'data-date-end-date' => "today", 'data-required' => "true", 'data-provide' => "datepicker", 'data-date-format' => "dd-mm-yyyy"]) ?>
                            </div>
                        </div>
						<div class='col-md-6'>
                            <div class="form-group">
                                <?= $form->field($model, 'news_meta_name')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'Meta']) ?>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <?= $form->field($model, 'news_meta_content')->textInput(['maxlength' => true, 'class' => 'form-control ' , 'placeholder' => 'Content']) ?>
                            </div>
                        </div>
						
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>
                    </div>
                </form>
            </div><!-- /.box -->
        </div><!--/.col (left) -->
    </div>
    <!-- right column -->
    <?php ActiveForm::end(); ?>
</section><!-- /.content -->

<script>
    $(".datepicker").datepicker();
    
    /* Image Upload validation */
    $("#image").change(function () {
        var val = $(this).val();
        switch (val.substring(val.lastIndexOf('.') + 1).toLowerCase()) {
            case 'gif':
            case 'jpg':
            case 'png':
                break;
            default:
                $(this).val('');
                // error message here
                alert("File is Not an image! Please upload a image file.");
                break;
        }
    });
    
    
    //  tinymce.init({ selector:'.contentEditor' });
    tinymce.init({
        selector: '.contentEditor',
        height: 250,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ],
        content_css: [
            '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    }); 

</script>
