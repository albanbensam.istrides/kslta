<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\KslNewsTbl */

//$this->title = $model->news_title;
$this->params['breadcrumbs'][] = ['label' => 'News List', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-news-tbl-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fa fa-edit"></i> Update', ['update', 'id' => $model->news_id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('<i class="fa fa-trash"></i> Delete', ['delete', 'id' => $model->news_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>





    <div class="row">
        <div class="col-md-6">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="<?= Yii::$app->getUrlManager()->getBaseUrl() . '/' . $model->news_content_image ?>" alt="User profile picture">
                    <h3 class="profile-username text-center"><?= $model->news_title ?></h3>
                    <p class="text-muted text-center"><?= $model->news_display_tag ?></p>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Link Address</b> <a class="pull-right"><?= $model->news_link_address ?></a>
                        </li>
                        <li class="list-group-item">
                            <b>News Status</b> <a class="pull-right"><?php echo $model->news_link_status == 'A' ? 'Active' : 'Inactive'; ?></a>
                        </li>
                        <li class="list-group-item">
                            <b>Posted By</b> <a class="pull-right"><?= $model->news_posted_by ?></a>
                        </li>
                        <li class="list-group-item">
                            <b>Form Date</b> <a class="pull-right"><?= $model->news_from_date ?></a>
                        </li>
                        <li class="list-group-item">
                            <b>To Date</b> <a class="pull-right"><?= $model->news_to_date ?></a>
                        </li>
                    </ul>

                </div><!-- /.box-body -->
            </div><!-- /.box -->

            <!-- About Me Box -->

        </div><!-- /.col -->
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">News Content</h3>
                </div><!-- /.box-header -->
                <div class="box-body" style="height: 400px; overflow: scroll;">
                    <?=
                    "by  " . $model->news_posted_by . "  " .
                    $model->news_editor_content . "  " .
                    '<i class="fa fa-calendar"></i>' . "  " . date('d F, Y', strtotime($model->news_posted_at)) . "  -" .
                    $model->news_located_city . "  "
                   // isset($model->news_located_country)? ", ".$model->news_located_country : " "
                    ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->

</div>
