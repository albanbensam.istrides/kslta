<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\KslNewsTbl */

$this->title = 'News List Update: ' . ' ' . $model->news_title;
$this->params['breadcrumbs'][] = ['label' => 'News List', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->news_id, 'url' => ['view', 'id' => $model->news_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ksl-news-tbl-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
