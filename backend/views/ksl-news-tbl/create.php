<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\KslNewsTbl */

if($model->news_tournament_id!=""){
	$this->title = 'Add Tournament News';
}else{
	$this->title = 'Add News';
}
$this->params['breadcrumbs'][] = ['label' => 'News List', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-news-tbl-create">

 
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
