<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\modelsTournamentScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this -> title = 'Tournament Schedules';
$this -> params['breadcrumbs'][] = $this -> title;


$return_btu="";
	

?>
<script src="<?php echo Url::base(); ?>/plugins/bracket/dist/jquery.bracket.min.js"></script>
<link href="<?php echo Url::base(); ?>/plugins/bracket/dist/jquery.bracket.min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/bracket/dist/jquery-ui.css">
<script src="<?php echo Url::base(); ?>/plugins/bracket/dist/jquery-ui.js"></script>
<div class="user-index">
    <div class="box-body">
    <div class="box box-primary cgridoverlap">
                <div class="box-header with-border">

    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div><!-- /.box-header -->
                  <div class="box-body">
        <!-- left column -->
        <div class="col-md-12">
   
     
<?= $this -> render('_tournamentschedule', ['model' => $model ]) ?>
<?php $form = ActiveForm::begin(['id' => 'tournamend_schedule1']); ?>
    <?php 
    
    if($tournament_details){
    
    if ($tournament_details->tourn_title!="") { ?>   
    <div class="col-md-12">
    	<div class="col-md-6">
    		<div class="col-md-3">
    		<label>Tournament :</label> 
    		</div>
    		<div class="col-md-9">
    		 <?=$tournament_details -> tourn_title ?>    		
    		 <input id='tournament_id' name='tournament_id' type='hidden' value="<?=$tournament_details -> tourn_id ?>">
    		 <?php $torunKsltaId = $tournament_details->tourn_id; ?>
    		</div>	
    	</div>
    </div>
    <?php } } ?>    
    <?= GridView::widget(['dataProvider' => $dataProvider, 'columns' => [['class' => 'yii\grid\SerialColumn'], ['attribute' => 'Event', 'value' => function($model) use ($event_names_all) {
			return $event_names_all[$model -> event_id];
		}],/*[
		 	'attribute' => 'ue_userid',
        	'label' => 'No of Player\'s',
        	'enableSorting' =>false
		],*/
		['class' => 'yii\grid\ActionColumn', 'template' => '{noofmatches}', 'header' => 'No. of Matches', 'buttons' => ['noofmatches' => function($url, $model, $key) use ($event_matches_no) {
			$return_text='<div class="col-md-3">'.$event_matches_no[$model -> event_id].'</div><div class="col-md-6">'.Html::button('<span class="fa fa-edit"></span>', ['class' => 'btn btn-warning btn-xs tm_event_players', 'id' => "btn_evevt" . $model -> event_id, 'value' => $model -> event_id,'data_no_matches'=>$event_matches_no[$model -> event_id]]).'</div>';
			return $return_text;
		}, ]],
		['class' => 'yii\grid\ActionColumn', 'template' => '{drawsheetupload}', 'header' => 'Draw Sheet', 'buttons' => ['drawsheetupload' => function($url, $model, $key) use ($event_matches_no) {
				 
			if(isset($event_matches_no[$model -> event_id])){
				$return_btu=Html::button('<i class="fa fa-fw fa-upload"></i> Upload', ['class' => 'btn btn-info btn-xs tm_drawsheetupload', 'id' => "btn_draw" . $model -> event_id, 'value' => $model -> event_id]);
				//$return_btu.=" ".Html::button('<i class="glyphicon glyphicon-eye-open"></i>', ['class' => 'btn btn-primary btn-xs tm_drawsheetview', 'id' => "btn_" . $model -> event_id, 'value' => $model -> event_id]);
				$return_btu.=" ".Html::button('<i class="glyphicon glyphicon-eye-open"></i>', ['class' => 'btn btn-primary btn-xs tm_drawsheetview', 'id' => "btn_drawview" . $model -> event_id, 'value' => $model -> event_id]);
				return $return_btu;	
			}else{
				return '';
			}
		}, ]],

		['class' => 'yii\grid\ActionColumn', 'template' => '{tournplayerlist}{playerlistview}', 'header' => 'ORDER OF PLAY Upload', 'buttons' => [
			
		'tournplayerlist' => function($url, $model, $key) {
			$return_btu="";
            $return_btu.=" ".Html::button('<i class="fa fa-fw fa-users"></i> Player Lists', ['value' => Url::to(['tournament-player-list/create', 'event_id' => $model->event_id, 'tourn_id' => $_POST['TournamentSchedule']['ts_tournament_id']] ), 'class' => 'btn bg-purple btn-xs tournplayerlistadd', 'data-toggle'=>'tooltip', 'aria-label'=>'Player List Add', 'data-original-title'=>'Player List Add']);
                return $return_btu; 
        }, 

        'playerlistview' => function($url, $model, $key) {
			$return_btu="";
            $return_btu.=" ".Html::button('<i class="glyphicon glyphicon-eye-open"></i>', ['value' => Url::to(['tournament-player-list/index'] ), 'class' => 'btn bg-purple btn-xs tournplayerlistadd', 'data-toggle'=>'tooltip', 'aria-label'=>'View File Lists', 'data-original-title'=>'View File Lists']);
                return $return_btu; 
        }, 


        ]],



		['class' => 'yii\grid\ActionColumn', 'template' => '{scheduleupdate}', 'header' => 'Action', 'buttons' => ['scheduleupdate' => function($url, $model, $key) use ($event_matches_no){
			if(isset($event_matches_no[$model -> event_id])){
				return Html::button('<i class="fa fa-fw fa-table"></i> Schedule', ['class' => 'btn btn-success btn-xs tm_event_update', 'id' => "btn_schedule" . $model -> event_id, 'value' => $model -> event_id]);	
			}else{
				return '';
			}
		}, ]],
		 ], ]);
		 
		/* ['attribute' => 'No. of Players', 'value' => 'event_id'], ['class' => 'yii\grid\ActionColumn', 'template' => '{factsheet}', 'header' => 'No. of Matches', 'buttons' => ['factsheet' => function($url, $model, $key) {
			$nomatches = array(2 => 2, 4 => 4, 8 => 8, 16 => 16, 32 => 32, 46 => 64);
			return Html::dropDownList("btn_" . $model -> event_id, '', $nomatches, ['prompt' => 'Select', 'class' => '']);
		}, ]],*/
	 ?>
      <?php ActiveForm::end(); ?>
</div>
</div>
</div>
</div>

<!-- Tournament Model  -->
	  <?php 
            Modal::begin([
                    'header' => '<h2 class="modalgalleryheader"></h2>',
                    'id' => 'modalgallery', 
                    'size' => 'modal-md',

                ]);

            echo "<div id='modalContent'></div>";
            Modal::end();

     ?>
<!-- Tournament Model End -->


<!--Modal Start -->
<?php
$out_mode="";
	Modal::begin(['header' => '<h3>Event Update </h3>', 'id' => 'modalEventupdate', 'size' => 'modal-md', ]);
  echo Html::button('<i class="fa fa-fw fa-table"></i> Clear All', ['class' => 'btn btn-success btn-xs clearbtn', 'id' => "clear"]);
	echo "<div id='modalContent_result'>
            <div class='tournament_chart' style='position:relative;height:480px;overflow:auto;'>
</div><input id='event_id_schedule' name='event_id_schedule' type='hidden'>
        </div>";

	Modal::end();
	
	Modal::begin(['header' => '<h3>Event Draw Sheet Upload</h3>', 'id' => 'modaldrawsheetupload', 'size' => 'modal-md', ]);
	$form = ActiveForm::begin(['action' => ['tournament-schedule/drawsheetupload'],'id' => 'drawsheetupload', 'options' => ['enctype' => 'multipart/form-data']]);
	echo '<div id="modalDrawsheetupload_result"><div class="col-md-12"><div class="col-md-3"></div><div class="col-md-4">'; 
 	echo $form->field($model, 'event_drawsheet')->fileInput(['id' => 'image']);	
    echo '</div></div>';
	echo "<div class='modal-footer'>";
	if($tournament_details){
	$model->drawsheet_tournament_id=$tournament_details -> tourn_id;
	}
	echo $form->field($model, 'drawsheet_tournament_id')->hiddenInput(['id' => 'drawsheet_tournament_id'])->label(false);
	echo $form->field($model, 'drawsheet_eventid')->hiddenInput(['id' => 'drawsheet_eventid'])->label(false);
	echo Html::submitButton('<i class="fa fa-fw fa-upload"></i> Upload', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
	echo "<button class='btn' data-dismiss='modal' aria-hidden='true'><i class='fa fa-fw fa-ban'></i> Close</button></div>";
    echo '</div></div>';
	
	echo $out_mode;
	
	ActiveForm::end();
	Modal::end();
	
	Modal::begin(['header' => '<h3>Event Draw Sheet Upload</h3>', 'id' => 'modaldrawsheetview', 'size' => 'modal-md', ]);
	echo "<div id='modaldrawsheetview_result'>"; 
	echo '<div class="box">      
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody  id="drawsheet_result_table">        
              </tbody></table>
            </div>
          </div>';
    echo "</div>";
	Modal::end();
	
	
	Modal::begin(['header' => '<h3>Event Schedule </h3>', 'id' => 'modalNoplayer', 'size' => 'modal-md', ]);
	$out_mode='<div id="modalNoplayer_result"><div class="col-md-12"><div class="col-md-3">Selct Number of Matches</div><div class="col-md-4">';
    $nomatches = array(2 => 2, 4 => 4, 8 => 8, 16 => 16, 32 => 32, 64 => 64, 128 => 128);
	$out_mode.=Html::dropDownList("btn_no_of_player", '', $nomatches, ['id'=>'btn_no_of_player','prompt' => 'Select', 'class' => '']);   
    $out_mode.='</div></div>';
	$out_mode.="<div class='modal-footer'><input id='event_id_matches' name='event_id_matches' type='hidden'><a class='btn btn-primary eventNoofMatches' href='#' ><i class='fa fa-fw fa-check-square-o'></i> Save</a>
                <button class='btn' data-dismiss='modal' aria-hidden='true'><i class='fa fa-fw fa-ban'></i> Close</button></div>";
    $out_mode.='</div>';
	echo $out_mode;
	Modal::end();
	
	$action_buttons="";
	$action_buttons1="";
	Modal::begin(['header' => '<h3>Report Scores </h3>', 'id' => 'modalResultupdate', 'size' => 'modal-md', ]);
	$form = ActiveForm::begin(['action' => ['tournament-schedule/save_playerresult'],'id' => 'scoreupdate', 'options' => ['enctype' => 'multipart/form-data']]);
	$out_mode='<div id="modalResultupdate_div"><div class="col-md-12">';
	/*$location_btn=Html::input('text', 'matchlocation', '', ['class' => 'form-control','id'=>'matchlocation'] );
	$scheduledtime_btn=Html::input('text', 'scheduledtime', '', ['class' => 'form-control','id'=>'scheduledtime']);
	$scheduleddate_btn=Html::input('text', 'scheduleddate1', '', ['id'=>'scheduleddate1','class' => 'form-control datepicker', 'placeholder' => 'DD-MM-YYYY']);
	*//*$score_set='';
	for($rr=1;$rr<=6;$rr++){
		$score_set.='<div class="col-md-2"><label>Set '.$rr.'</label>'.Html::input('number', 'set_val[]', $user->name, ['class' => 'form-control'] )."</div>";
	}*/
	$action_buttons.="<div class='modal-footer col-md-12'><input id='event_id_matches' name='event_id_matches' type='hidden'><a class='btn btn-primary scoreMatchdetails' href='#' ><i class='fa fa-fw fa-check-square-o'></i> Save</a>
                <button class='btn' data-dismiss='modal' aria-hidden='true'><i class='fa fa-fw fa-ban'></i> Close</button></div>";
    $action_buttons1.="<input id='edit_id_score' name='edit_id_score' type='hidden'><div class='modal-footer col-md-12'><input id='event_id_matches' name='event_id_matches' type='hidden'><a class='btn btn-primary scoreMatchdetailsresult' href='#' ><i class='fa fa-fw fa-check-square-o'></i> Save</a>
                <button class='btn' data-dismiss='modal' aria-hidden='true'><i class='fa fa-fw fa-ban'></i> Close</button></div>";
	$out_mode.='<div class="nav-tabs-custom">
                <ul class="nav nav-tabs pull-right">
                  <li class="pull-left"><a data-toggle="tab" href="#matche_details">Match Details</a></li>
                  <li class="pull-left active"><a data-toggle="tab" href="#report_scrores">Report Scores</a></li>
                </ul>
                <div class="tab-content no-padding">
                  <div style="position: relative; height: 225px;" id="matche_details" class="chart tab-pane box">
                  <div id="matchdetails_div">
                 </div>'.$action_buttons.'
                  </div>
                  <div style="position: relative; height: 200px;" id="report_scrores" class="chart tab-pane box active">
	                  <div class="col-md-12 box-body">
	                  	<div class="form-group"><label>Enter Scores</label></div><div class="col-md-12 box-body" id="set_div_html"></div>
	                  </div>'.$action_buttons1.'
                  </div>
                </div>
              </div>';   
    $out_mode.='</div>';
    $out_mode.="<div class='modal-footer'></div>";
	ActiveForm::end();
    $out_mode.='</div>';
	echo $out_mode;
	Modal::end();
?>				
 
<!--Modal End -->
<style>
	.modal-dialog {
		width: 920px !important;
	}
</style>
<script>
 
//'bootstrap-datepicker data-date-autoclose' => "true", 'data-required' => "true", 'data-provide' => "datepicker", 'data-date-format' => "dd-mm-yyyy"
//var autoCompleteData = 
 //  {"teams":[["1234567890","Player 4"],["Player 2","Player 3"],["Player 1","Player 2"],["p",""]],"results":[[[["8/2","8/1"],[1,0],[1,2],[null,null]],[[0,1],[null,null]],[[null,null],[null,null]]]]}
  // {"teams":[["1234567890","Player 4"],["Player 2","Player 3"]],"results":[[[["8/2","8/1"],[1,0],[1,2],[null,null]],[[0,1],[null,null]],[[null,null],[null,null]]]]}
var  autoCompleteData={"teams":[],"results":[]};
/* Data for autocomplete */
var acData = [];
 
 /* Change Status Ajax request functionality */
	$(document).on('click', '.tm_event_update:button', function () {  
		 var btn_id=(this.id);
		 $('#event_id_schedule').val($('#'+btn_id).val());
         var event_id = $('#event_id_schedule').val();
         var tournament_id = $('#tournament_id').val(); 
         
          var PageUrl = '<?php echo Yii::$app->homeUrl . '?r=tournament-schedule/tournamentschedule'; ?>';
             $.ajax({
                 url: PageUrl,
                 type: 'POST',
                 dataType: 'json',
                 data : { 'event_id' : event_id, 'tournament_id' : tournament_id},
                 success: function (result) {
                 	//alert(result); 
                 	   $('#modalEventupdate').modal('show').find('#modalContent_result').load();
                 	 	autoCompleteData=result; 
                 	 	 var PageUrl_1 = '<?php echo Yii::$app->homeUrl . '?r=tournament-schedule/eventplayers'; ?>';
			             $.ajax({
			                 url: PageUrl_1,
			                 type: 'POST',
			                 dataType: 'json',
			                 data : { 'event_id' : event_id, 'tournament_id' : tournament_id},
			                 success: function (result) {  
			                 	//alert(result); 
									acData = result;	
									$('.tournament_chart').bracket({
									skipConsolationRound: true,
							     	init: autoCompleteData,
							      	save: saveFn, /* without save() labels are disabled */
							      	decorator: {edit: edit_fn,
                 								render: render_fn}})											
			                 },
			                 error: function (error) {
			                    // alert(error.responseText);
			                 }
			             });
							  
                   /* if (result=="Success")
                     { 
                  		 $('#tournamend_schedule1').submit();
                    }*/

                 },
                 error: function (error) {
                     alert(error.responseText);
                 }
             });
	 
	});	
	$(document).on('click', '.tm_drawsheetupload:button', function () {
		var btn_id=(this.id);
		$('#drawsheet_eventid').val($('#'+btn_id).val());
	    $('#modaldrawsheetupload').modal('show').find('#modalDrawsheetupload_result').load();		
	});
	$(document).on('click', '.tm_drawsheetview:button', function () {
		var btn_id=(this.id);
		var event_id = $('#'+btn_id).val();
        var tournament_id = $('#tournament_id').val();
		var PageUrl = '<?php echo Yii::$app->homeUrl . '?r=tournament-schedule/drawsheetview'; ?>';
             $.ajax({
                 url: PageUrl,
                 type: 'POST',
                 data : { 'event_id' : event_id, 'tournament_id' : tournament_id},
                 success: function (result) {  	
                 	//alert(result);
                 	$('#drawsheet_result_table').html(result);
                 	$('#modaldrawsheetview').modal('show').find('#modaldrawsheetview_result').load();	
                 },
                 error: function (error) {
                     alert(error.responseText);
                 }
             });
	    	
	});
	
	/* Change Status Ajax request functionality */
	$(document).on('click', '.tm_event_players:button', function () {
		var btn_id=(this.id);
		$('#event_id_matches').val($('#'+btn_id).val());
		$('#btn_no_of_player').val($('#'+btn_id).attr('data_no_matches'));
	    $('#modalNoplayer').modal('show').find('#modalNoplayer_result').load();		
	});    
   
/* If you call doneCb([value], true), the next edit will be automatically 
   activated. This works only in the first round. */
function acEditFn(container, data, doneCb) {
  var input = $('<input type="text">')
  input.val(data)
  input.autocomplete({ source: acData })
  input.blur(function() { doneCb(input.val()) })
  input.keyup(function(e) { if ((e.keyCode||e.which)===13) input.blur() })
  container.html(input)
  input.focus()
}
 
function acRenderFn(container, data, score) {
 /* var fields = data.split(':')
  if (fields.length != 2)
    container.append('--')
  else*/
    container.append(data)
}
 
 function saveFn(data, userData) {
  var event_id = $('#event_id_schedule').val();
  var tournament_id = $('#tournament_id').val();
  var json = JSON.stringify(data);//alert(json);
  var PageUrl_2 = '<?php echo Yii::$app->homeUrl . '?r=tournament-schedule/eventresultsave'; ?>';
			             $.ajax({
			                 url: PageUrl_2,
			                 type: 'POST',
			                 dataType: 'json',
			                 data : { 'event_id' : event_id, 'tournament_id' : tournament_id, 'tournament_result' :data},
			                 success: function (result) {			                 	 
				                  	// alert(result);				                  		 
				                  	 $("#btn_schedule"+result).click();								
			                 },
			                 error: function (error) {
			                     alert(error.responseText);
			                 }
			             });
  /* You probably want to do something like this
  jQuery.ajax("rest/"+userData, {contentType: 'application/json',
                                dataType: 'json',
                                type: 'post',
                                data: json})
  */
}
function edit_fn(container, data, doneCb) {
  var input = $('<input type="text">')
  input.val(data.name)
  input.autocomplete({ source: acData })
  container.html(input)
  input.focus()
  input.blur(function() { doneCb({flag: data.flag, name: input.val()},false) })
}
function render_fn(container, data, score) {
  if (!data.flag || !data.name)
    return
  //container.append('<img src="site/png/'+data.flag+'.png" /> ').append(data.name)
  container.append(data.flag).append(data.name)
}
$(function() {
    $('.tournament_chart').bracket({
  	  skipConsolationRound: true,
      init: autoCompleteData,
      save: saveFn, /* without save() labels are disabled */
      decorator: {edit: acEditFn,
                  render: acRenderFn}})
  })
  //tools to enable ++ -- de
   //$('.doubleElimination').hide();
   
$(document).on('click', '.eventNoofMatches', function () {
        	 var event_id = $('#event_id_matches').val();
        	 var no_of_matches = $('#btn_no_of_player').val();
        	  var tournament_id = $('#tournament_id').val();
			if(no_of_matches!=""){
             var PageUrl = '<?php echo Yii::$app->homeUrl . '?r=tournament-schedule/noofmatches'; ?>';
             $.ajax({
                 url: PageUrl,
                 type: 'POST',
                 data : { 'event_id' : event_id, 'no_of_matches' : no_of_matches , 'tournament_id' : tournament_id},
                 success: function (result) {      	
                    if (result=="Success")
                     { 
                  		 $('#tournamend_schedule1').submit();
                    }
                 },
                 error: function (error) {
                     alert(error.responseText);
                 }
             });
             }else{
             	alert("Please select Number of Matches!");
             }
     });
 $(document).on('click', '.edit_player_result', function () {
		var btn_id=(this.id);//alert(btn_id);	
		$('#edit_id_score').val($('#'+btn_id).attr('data-resultid'));
		 var event_id = $('#event_id_schedule').val();
         var edit_id_score = $('#edit_id_score').val();
         var tournament_id = $('#tournament_id').val();	
		if(edit_id_score>=0){
             var PageUrl = '<?php echo Yii::$app->homeUrl . '?r=tournament-schedule/setdetails'; ?>';
             $.ajax({
                 url: PageUrl,
                 type: 'POST',
                 data : { 'event_id' : event_id, 'edit_id_score' : edit_id_score , 'tournament_id' : tournament_id},
                 success: function (result) { 
                 	//alert(result);               
                 	var tmp1=result.split("~~");   	
                 	 $('#set_div_html').html(tmp1[0]);
                 	 $('#matchdetails_div').html(tmp1[1]); 
                 	 //data picker
                 	 $('#scheduleddate1').daterangepicker({
							format : 'DD-MM-YYYY',
							singleDatePicker : true
						}, function(start, end, label) {
							console.log(start.toISOString(), end.toISOString(), label);
						});
                 },
                 error: function (error) {
                     alert(error.responseText);
                 }
             });
             }
	    $('#modalResultupdate').modal('show').find('#modalResultupdate_div').load();		
	});
$(document).on('click', '.scoreMatchdetailsresult', function () {
        	 var event_id = $('#event_id_schedule').val();
        	 var edit_id_score = $('#edit_id_score').val();
        	  var tournament_id = $('#tournament_id').val();
        	  var player_reg_id=($('#plyerid_'+edit_id_score+' .player_id_css').html());
        	  	var set_value = [];
		$("input[name='set_val[]']").each(function() {
				if($(this).val()!=''){
			    set_value.push($(this).val());
			   }
		});
			if(edit_id_score>=0){
             var PageUrl = '<?php echo Yii::$app->homeUrl . '?r=tournament-schedule/scorematchdetails'; ?>';
             $.ajax({
                 url: PageUrl,
                 type: 'POST',
                 data : { 'event_id' : event_id, 'edit_id_score' : edit_id_score , 'tournament_id' : tournament_id,'set_value':set_value,'player_reg_id':player_reg_id},
                 success: function (result) { 
                 	//alert(result); 
                 	$('#result_sum'+edit_id_score).html(result);
                 	alert("Updated Successfully!");
                 },
                 error: function (error) {
                    // alert(error.responseText);
                 }
             });
             }
     });
$(document).on('click', '.scoreMatchdetails', function () {
        	 var event_id = $('#event_id_schedule').val();
        	 var edit_id_score = $('#edit_id_score').val();
        	var tournament_id = $('#tournament_id').val();
        	var player_reg_id=($('#plyerid_'+edit_id_score+' .player_id_css').html());
			var matchlocation=$('#matchlocation').val();
			var scheduledtime=$('#scheduledtime').val();
			var scheduleddate1=$('#scheduleddate1').val();
			if(edit_id_score>=0){
             var PageUrl = '<?php echo Yii::$app->homeUrl . '?r=tournament-schedule/schediledetails'; ?>';
             $.ajax({
                 url: PageUrl,
                 type: 'POST',
                 data : { 'event_id' : event_id, 'edit_id_score' : edit_id_score , 'tournament_id' : tournament_id,'matchlocation':matchlocation,'scheduledtime':scheduledtime,'scheduleddate1':scheduleddate1,'player_reg_id':player_reg_id},
                 success: function (result) { 
                 	//alert(result); 
                 	alert("Updated Successfully!");
                 },
                 error: function (error) {
                     alert(error.responseText);
                 }
             });
             }
     });

    $(document).ready(function(){

        /* When Model is in Page the User Profile option will not come for avoid that bug the below code has been resolved it. */
        $('.dropdown-toggle').click(function(){
             $('.navbar-nav > .user ').toggleClass("open");
            $(".navbar-nav > .user > .dropdown-toggle").attr("aria-expanded","true");
        });

         $('.tournplayerlistadd').click(function(){
         	$('.modalgalleryheader').text('Tournament Player List Image');
         	$("#modalContent").html('<center><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><center>');
        $('#modalgallery').modal('show').find('#modalContent').load($(this).attr('value'));
         });
       
    });
	$(document).on('click', '.clearbtn', function () {  
		 
		 
         var event_id = $('#event_id_schedule').val();
         var tournament_id = $('#tournament_id').val(); 
         
          var PageUrl = '<?php echo Yii::$app->homeUrl . '?r=tournament-schedule/tournamentclear'; ?>';
             $.ajax({
                 url: PageUrl,
                 type: 'POST',
                 dataType: 'json',
                 data : { 'event_id' : event_id, 'tournament_id' : tournament_id},
                 success: function (result) {
                 	//alert(result); 
                 	   $('#modalEventupdate').modal('show').find('#modalContent_result').load();
                 	 	autoCompleteData=result; 
                 	 	 var PageUrl_1 = '<?php echo Yii::$app->homeUrl . '?r=tournament-schedule/eventplayers'; ?>';
			             $.ajax({
			                 url: PageUrl_1,
			                 type: 'POST',
			                 dataType: 'json',
			                 data : { 'event_id' : event_id, 'tournament_id' : tournament_id},
			                 success: function (result) {  
			                 	//alert(result); 
									acData = result;	
									$('.tournament_chart').bracket({
									skipConsolationRound: true,
							     	init: autoCompleteData,
							      	save: saveFn, /* without save() labels are disabled */
							      	decorator: {edit: edit_fn,
                 								render: render_fn}})											
			                 },
			                 error: function (error) {
			                    // alert(error.responseText);
			                 }
			             });
							  
                   /* if (result=="Success")
                     { 
                  		 $('#tournamend_schedule1').submit();
                    }*/

                 },
                 error: function (error) {
                     alert(error.responseText);
                 }
             });
	 
	});	
</script>
