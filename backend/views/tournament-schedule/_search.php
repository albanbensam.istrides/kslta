<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modelsTournamentScheduleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tournament-schedule-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ts_id') ?>

    <?= $form->field($model, 'ts_tournament_id') ?>

    <?= $form->field($model, 'ts_event_id') ?>

    <?= $form->field($model, 'ts_player_id') ?>

    <?= $form->field($model, 'ts_player_order') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
