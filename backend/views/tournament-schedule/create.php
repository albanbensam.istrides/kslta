<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TournamentSchedule */

$this->title = 'Create Tournament Schedule';
$this->params['breadcrumbs'][] = ['label' => 'Tournament Schedules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tournament-schedule-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
