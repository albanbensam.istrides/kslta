<?php 

use yii\helpers\Html;

use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;

use backend\models\DropdownManagement;

use backend\models\KslTournamentsTbl;

use backend\models\TournamentEventModel;

use backend\models\PlayerEvents;

use yii\helpers\Url;

?>

<?php $form = ActiveForm::begin(); ?>

<div class="form-group col-md-12">

<div class="form-group col-md-6">

                            <?php $tournList=ArrayHelper::map(KslTournamentsTbl::find()->asArray()->all(), 'tourn_id', 'tourn_title'); ?>

                       		<?php 

                       		 echo $form->field($model, 'ts_tournament_id')->dropDownList(

							                $tournList,

							                [

							                    'prompt'=>'Select Tournament',

							                    'class' => 'form-control',

							                    'onchange'=>'this.form.submit();'

							                  /*  'onchange'=>'

							                        $.get( "'.Url::toRoute('eventvalue').'", { id: $(this).val() } )

							                            .done(function( data ) {

							                                $( "#'.Html::getInputId($model, 'ts_event_id').'" ).html( data );														

																						                            }

							                        );'*/

							                ]

							                

						        );		

		//$selected_event = ArrayHelper::map(PlayerEvents::find()->where(['ue_tournamentid' => $model->ts_tournament_id,'ue_userid'=>$model->ts_player_id])->all(), 'ue_autoid', 'ue_eventid');							

		/*$user_event=array();

		$rows=TournamentEventModel::find()->where(['tourn_id' => $model->ts_tournament_id])->all();

		$id_db_cat=array();

		if(count($rows)>0){

        	$id_db_all=array();

        	foreach($rows as $one_event){

				$id_db_all[]=$one_event->event_category;

				$id_db_all[]=$one_event->event_subcategory;

				$id_db_cat[$one_event->event_category][$one_event->event_id]=$one_event->event_subcategory;																									

			}

			

			if(count($id_db_all)>0){

				$event_names = ArrayHelper::map(DropdownManagement::find()->FilterWhere(array('in', 'dropdown_id', $id_db_all))->all(), 'dropdown_id', 'dropdown_value');

			}

		}

			foreach($id_db_cat as $one_categoryid=>$one_subcategory){

				$one_category_txt='';

				if(isset($event_names[$one_categoryid])){

					$one_category_txt=$event_names[$one_categoryid];

				}	

						foreach($one_subcategory as $event_id=>$one_event){

						$one_subcategory_txt='';

						if(isset($event_names[$one_event])){

							$one_subcategory_txt=$event_names[$one_event];

						}

						$user_event[$one_category_txt][$event_id]=$one_category_txt.' '.$one_subcategory_txt;		              

		            }				

			}

			//$model->ts_event_id=$selected_event;

							?>

							</div>

						<div class="form-group col-md-6">

						<?php echo $form->field($model, 'ts_event_id')->dropDownList($user_event,

						[

								'class'=>' form-control',

								'prompt'=>'Select Tournament',

			                    'onchange'=>'this.form.submit();'

						]

						)  */

			

          ?>

		</div>

		</div>

		<?php 

		/*foreach($playerevents['playerevents'] as $onerec){

			 if(isset($playerevents['playernames'][$onerec->ue_userid])){

			 	echo $playerevents['playernames'][$onerec->ue_userid]."<br>";

			 }

		}*/ ?>

		<?php // echo $playerevents->ue_autoid; ?>

 <?php ActiveForm::end(); ?>

 