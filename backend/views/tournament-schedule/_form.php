<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\DropdownManagement;
use backend\models\PageContents;
use backend\models\CoachingPages;
/* @var $this yii\web\View */
/* @var $model backend\models\TournamentSchedule */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tournament-schedule-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ts_tournament_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ts_event_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ts_player_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ts_player_order')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
