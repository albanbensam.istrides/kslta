<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\KslAcademyRegSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Academy Register';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ksl-tournaments-tbl-index">
    <div class="box-body">
    <div class="box box-primary cgridoverlap">
                <div class="box-header with-border">

    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

    
    </div><!-- /.box-header -->
                  <div class="box-body">
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'academy_name:ntext',
            'owner',
            'contact_number',
            'email_id:email',
            'address:ntext',
           'no_of_courts_clay',
             'no_of_courts_hard',
             'no_of_courts_others',
             'no_of_courts_flood',
             'no_of_coaches',
            // 'office',
            // 'pro_shop',
            // 'cafeteria',
            // 'toilet',
            // 'room',
            // 'gym',
            // 'warm_up',
            // 'water',
            // 'other_information',
            // 'remarks:ntext',
             'active_status',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn',

              'header'=> 'Action',
               'template'=>'{view}',
                 'headerOptions' => ['style' => 'width:150px;color:#337ab7;'],
                'buttons'=>[
                  'view' => function ($url, $model, $key) {
                       $url = Url::to(['ksl-academy/viewdata', 'id' => ($model -> id)]);     
                       return Html::a('View', $url, ['class' => 'btn btn-xs btn-success']);
                    }, 
              ]
             ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
</div>
</div>
</div>
</div>
