<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\KslAcademyReg */

$this->title = "Academy Name - ". $model->academy_name;
//$this->params['breadcrumbs'][] = ['label' => 'Ksl Academy Regsmm', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box-body">
    <div class="box box-primary cgridoverlap">
                <div class="box-header with-border">

     <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
 
    
    </div><!-- /.box-header -->
                  <div class="box-body">

    <p>
        <?= Html::a('Reject', ['updatestatus', 'id' => $model->id,'status'=>'Reject'], ['class' => 'btn btn-danger']) ?>
        <?= Html::a('Hold', ['updatestatus', 'id' => $model->id,'status'=>'Hold'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Approve', ['updatestatus', 'id' => $model->id,'status'=>'Approved'], ['class' => 'btn btn-success']) ?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'academy_name:ntext',
            'owner',
            'contact_number',
            'email_id:email',
            'address:ntext',
            'no_of_courts_clay',
            'no_of_courts_hard',
            'no_of_courts_others',
            'no_of_courts_flood',
            'no_of_coaches',
            'office',
            'pro_shop',
            'cafeteria',
            'toilet',
            'room',
            'gym',
            'warm_up',
            'water',
            'other_information',
            'remarks:ntext',
            'active_status',
            'created_at',
            //'updated_at',
        ],
    ]) ?>

</div>
</div>
</div>