<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\KslAcademyReg */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ksl-academy-reg-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'academy_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'owner')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contact_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'no_of_courts_clay')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_of_courts_hard')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_of_courts_others')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_of_courts_flood')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_of_coaches')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'office')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pro_shop')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cafeteria')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'toilet')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'room')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gym')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'warm_up')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'water')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'other_information')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'remarks')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'active_status')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
