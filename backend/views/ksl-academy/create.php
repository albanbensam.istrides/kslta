<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\KslAcademyReg */

$this->title = 'Create Ksl Academy Reg';
$this->params['breadcrumbs'][] = ['label' => 'Ksl Academy Regs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-academy-reg-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
