<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Headlines */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Headlines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="headlines-view">

 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          //  'id',
            'headlines_name',
           // 'headline_link',
            'order_no',
        ],
    ]) ?>

</div>
