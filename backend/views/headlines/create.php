<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Headlines */

$this->title = 'Create Headlines';
$this->params['breadcrumbs'][] = ['label' => 'Headlines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="headlines-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
