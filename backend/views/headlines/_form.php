<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\DropdownManagement;
use backend\models\PageContents;
use backend\models\CoachingPages;
use backend\models\KslNewsTbl;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Headlines */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="affilatedclubs-form">
<section class="content">
<!-- Info boxes -->
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="page-content-form">
                    <div class="box-header with-border">
                     <h3 class="box-title"><?= $model->isNewRecord ? '<i class="fa fa-fw fa-sticky-note-o"></i>' : '<i class="fa fa-fw fa-sticky-note"></i>' ?> <?= Html::encode($this->title) ?></h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12">
    	<div class="col-md-6">
    			<?php
          $dataList=ArrayHelper::map(KslNewsTbl::find()->asArray()->all(), 'news_title', 'news_title');
            ?>
            <?php
            echo $form->field($model, 'headlines_name')->dropDownList($dataList, 
                     ['prompt'=>'Choose',
                        //'style'=>'width:75%',
                      'onchange'=>'
          $.get( "'.Url::toRoute('headlines/lists').'", { id: $(this).val() })
         .done(function( data ) { $( "#headlines-headline_link" ).val( data ); } );'
]); 
             ?>
      
    		<!-- <?= $form->field($model, 'headlines_name')->dropDownList($dataList, 
                    ['prompt'=>'-----Select a Main Menu-----']) ?> -->
    		</div>
    		<div class="col-md-6">
    				 
            <?= $form->field($model, 'headline_link')->hiddenInput()->label(false) ?>
    			
    	</div>
    </div>
      <div class="col-md-12">
      	<div class="col-md-6">
    		<?= $form->field($model, 'order_no')->textInput(['maxlength' => true]) ?>
    		</div>
    		<div class="col-md-6">
    <div class="form-group pull-right">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    </div>
	</div>
    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
</div>
</div>
</div>
</section> 