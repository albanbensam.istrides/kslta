<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Headlines */

$this->title = 'Update Headlines: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Headlines', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="headlines-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
