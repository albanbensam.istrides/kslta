<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\KslCoachesReg */

$this->title = 'Create Ksl Coaches Reg';
$this->params['breadcrumbs'][] = ['label' => 'Ksl Coaches Regs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-coaches-reg-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
