<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\KslCoachesReg */

$this->title = "Coach Name - ". $model->name;
//$this->params['breadcrumbs'][] = ['label' => 'Ksl Coaches Regs', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box-body">
    <div class="box box-primary cgridoverlap">
                <div class="box-header with-border">

     <h3 class="box-title"><?//= Html::encode($this->title) ?></h3>
 
    
    </div><!-- /.box-header -->
                  <div class="box-body">

    <p>
        <?= Html::a('Reject', ['updatestatus', 'id' => $model->id,'status'=>'Reject'], ['class' => 'btn btn-danger']) ?>
        <?= Html::a('Hold', ['updatestatus', 'id' => $model->id,'status'=>'Hold'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Approve', ['updatestatus', 'id' => $model->id,'status'=>'Approved'], ['class' => 'btn btn-success']) ?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            'dob',
            'gender',
            'p_address:ntext',
            'contact_no',
            'mob_no',
            'email_id:email',
            'e_qual',
            'tennis_qual',
            'certificate:ntext',
            'remarks:ntext',
         
        ],
    ]) ?>

</div>
</div>
</div>