<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\KslCoachesRegSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Coaches Register';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/datatables/dataTables.bootstrap.css">
<script src="<?php echo Url::base(); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>


<div class="ksl-tournaments-tbl-index">
    <div class="box-body">
    <div class="box box-primary cgridoverlap">
                <div class="box-header with-border">

    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

    
    </div><!-- /.box-header -->
                  <div class="box-body">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            'dob',
            'gender',
            'p_address:ntext',
            //'contact_no',
             'mob_no',
             'email_id:email',
            // 'e_qual',
             'tennis_qual',
            // 'certificate:ntext',
            // 'remarks:ntext',
             'active_status',
            // 'created_at',
            // 'updated_at',

           // ['class' => 'yii\grid\ActionColumn'],
            ['class' => 'yii\grid\ActionColumn',

              'header'=> 'Action',
               'template'=>'{view}',
                 'headerOptions' => ['style' => 'width:150px;color:#337ab7;'],
                'buttons'=>[
                  'view' => function ($url, $model, $key) {
                       $url = Url::to(['ksl-coaches-reg/viewdata', 'id' => ($model -> id)]);     
                       return Html::a('View', $url, ['class' => 'btn btn-xs btn-success']);
                    }, 
              ]
             ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
</div>
</div>
</div>
</div>
