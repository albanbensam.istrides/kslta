<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\NewsletterOnlineSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="newsletter-online-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'newsletter_id') ?>

    <?= $form->field($model, 'newsletter_name') ?>

    <?= $form->field($model, 'newsletter_email') ?>

    <?= $form->field($model, 'newsletter_mobile') ?>

    <?= $form->field($model, 'newsletter_email_view_status') ?>

    <?php // echo $form->field($model, 'newsletter_created_at') ?>

    <?php // echo $form->field($model, 'newsletter_modified_at') ?>

    <?php // echo $form->field($model, 'newsletter_active_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
