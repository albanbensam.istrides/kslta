<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\NewsletterOnline */

$this->title = 'Update Newsletter Online: ' . $model->newsletter_id;
$this->params['breadcrumbs'][] = ['label' => 'Newsletter Onlines', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->newsletter_id, 'url' => ['view', 'id' => $model->newsletter_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="newsletter-online-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
