<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\NewsletterOnline */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="newsletter-online-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'newsletter_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'newsletter_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'newsletter_mobile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'newsletter_email_view_status')->dropDownList([ 'P' => 'P', 'V' => 'V', 'N' => 'N', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'newsletter_created_at')->textInput() ?>

    <?= $form->field($model, 'newsletter_modified_at')->textInput() ?>

    <?= $form->field($model, 'newsletter_active_status')->dropDownList([ 'A' => 'A', 'I' => 'I', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
