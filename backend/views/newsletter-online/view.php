<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\NewsletterOnline */

$this->title = $model->newsletter_id;
$this->params['breadcrumbs'][] = ['label' => 'Newsletter Onlines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newsletter-online-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->newsletter_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->newsletter_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'newsletter_id',
            'newsletter_name',
            'newsletter_email:email',
            'newsletter_mobile',
            'newsletter_email_view_status:email',
            'newsletter_created_at',
            'newsletter_modified_at',
            'newsletter_active_status',
        ],
    ]) ?>

</div>
