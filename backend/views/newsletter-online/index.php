<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\NewsletterOnlineSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Newsletter Onlines';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newsletter-online-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Newsletter Online', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'newsletter_id',
            'newsletter_name',
            'newsletter_email:email',
            'newsletter_mobile',
            'newsletter_email_view_status:email',
            // 'newsletter_created_at',
            // 'newsletter_modified_at',
            // 'newsletter_active_status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
