<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductSolutionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Menu Master';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/datatables/dataTables.bootstrap.css">
<script src="<?php echo Url::base(); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>


<div class="user-index">
    <div class="box-body">
    <div class="box box-primary cgridoverlap">
                <div class="box-header with-border">

    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div><!-- /.box-header -->
                  <div class="box-body">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('<i class="fa fa-plus"></i> Add Menu', ['create'], ['class' => 'btn btn-primary btn-md pull-right ']) ?>
    </p>

   <?php Pjax::begin(); ?>
           <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
           // 'menu_id',
            'menu_name',
            'menu_order_no',
          //  'menu_link:ntext',
           // 'menu_front_view_status',
            // 'menu_created_at',
            // 'menu_modified_at',
            // 'menu_active_status',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
</div>
</div>
</div>

