<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MainMenuSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="main-menu-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'menu_id') ?>

    <?= $form->field($model, 'menu_name') ?>

    <?= $form->field($model, 'menu_order_no') ?>

    <?= $form->field($model, 'menu_link') ?>

    <?= $form->field($model, 'menu_front_view_status') ?>

    <?php // echo $form->field($model, 'menu_created_at') ?>

    <?php // echo $form->field($model, 'menu_modified_at') ?>

    <?php // echo $form->field($model, 'menu_active_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
