<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\MainMenu */

$this->title = $model->menu_id;
$this->params['breadcrumbs'][] = ['label' => 'Main Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-menu-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          //  'menu_id',
            'menu_name',
            'menu_order_no',
            'menu_link:ntext',
          //  'menu_front_view_status',
            'menu_created_at',
            //'menu_modified_at',
            //'menu_active_status',
        ],
    ]) ?>

</div>
