<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\MainMenu */

$this->title = 'Update Main Menu: ' . ' ' . $model->menu_name;
$this->params['breadcrumbs'][] = ['label' => 'Main Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="main-menu-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
