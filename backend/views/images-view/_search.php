<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ImagesViewSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="images-view-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'image_id') ?>

    <?= $form->field($model, 'image_visible_page') ?>

    <?= $form->field($model, 'image_title') ?>

    <?= $form->field($model, 'image_over_title') ?>

    <?= $form->field($model, 'image_top_title') ?>

    <?php // echo $form->field($model, 'image_sub_title') ?>

    <?php // echo $form->field($model, 'image_file_location') ?>

    <?php // echo $form->field($model, 'image_file_name') ?>

    <?php // echo $form->field($model, 'image_type') ?>

    <?php // echo $form->field($model, 'image_visible_status') ?>

    <?php // echo $form->field($model, 'image_modified_at') ?>

    <?php // echo $form->field($model, 'image_delete_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
