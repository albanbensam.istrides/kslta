<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ImagesView */

$this->title = 'Update Images View: ' . $model->image_visible_page;
$this->params['breadcrumbs'][] = ['label' => 'Images Views', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="images-view-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
