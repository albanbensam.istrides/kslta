<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ImagesView */

$this->title = 'Create Images View';
$this->params['breadcrumbs'][] = ['label' => 'Images Views', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="images-view-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
