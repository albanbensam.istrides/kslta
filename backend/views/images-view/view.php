<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\ImagesView */

$this->title = $model->image_id;
$this->params['breadcrumbs'][] = ['label' => 'Images Views', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="images-view-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->image_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->image_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'image_id',
            'image_visible_page',
            'image_title',
            'image_over_title',
            'image_top_title',
            'image_sub_title:ntext',
            'image_file_location:ntext',
            'image_file_name',
            'image_type',
            'image_visible_status',
            'image_modified_at',
            'image_delete_status',
        ],
    ]) ?>

</div>
