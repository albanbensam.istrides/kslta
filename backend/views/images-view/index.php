<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductSolutionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'STATIC IMAGE CONTENT MANAGE';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/datatables/dataTables.bootstrap.css">
<script src="<?php echo Url::base(); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
  .gridbtncustom{margin-right: 3px;}
</style>

<div class="slider-banner-index">
    <div class="box-body">
    <div class="box box-primary cgridoverlap">
                <div class="box-header with-border">

    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

    
    </div><!-- /.box-header -->
                  <div class="box-body">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::button('<i class="fa fa-plus"></i> Add IMAGE', ['value' => Url::to('index.php?r=images-view/create'), 'class' => 'btn btn-primary btn-md pull-right ', 'id' => 'modalCreate']) ?>
       
       
        <?= Html::button(' Multiple Delete',  ['class' => 'btn btn-info ', 'id' => 'MyButton']) ?>
     
    </p>
   <?php Pjax::begin(); ?>
          <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
        ['class' => 'yii\grid\CheckboxColumn'],
            ['class' => 'yii\grid\SerialColumn'],    //to display the check box 
          //  'banner_id',
            // 'banner_name',
            // 'banner_url:url',
            // [
            //     'attribute' => 'banner_image',
            //     'format' => 'html',             
            //     'value' => function ($data) {
            //         return Html::img($data->banner_image,
            //             ['width' => '60px']);
            //     },
            // ],
            //'banner_order_flow',
            // 'banner_date',
            // 'banner_created_at',
            // 'banner_modified_at',
            // 'banner_status',

            //'image_id',
           // 'image_visible_page',
            'image_title',
            'image_over_title',
            'image_top_title',
            // 'image_sub_title:ntext',
            [
                'attribute' => 'image_file_location',
                'format' => 'html',             
                'value' => function ($data) {
                    return Html::img($data->image_file_location,
                        ['width' => '60px']);
                },
            ],
           //  'image_file_location',
            // 'image_file_name',
            // 'image_type',
            // 'image_visible_status',
            // 'image_modified_at',
            // 'image_delete_status',

            //['class' => 'yii\grid\ActionColumn'],
             ['class' => 'yii\grid\ActionColumn',
                          'template'=>'{update}{delete}',
                            'buttons'=>[
                              'view' => function ($url, $model) {     
                                return Html::button('<span class="glyphicon glyphicon-eye-open"></span>', [
                                        'value' => $url,
                                        'name' => 'Category View',
                                        'title' => Yii::t('yii', 'view'),
                                        'class' => 'btn btn-primary btn-xs gridbtncustom viewdata',
                                        'data-toggle'=> 'tooltip',
                                ]);                                
            
                              }, 
                                'update' => function ($url, $model) {     
                                return Html::button('<span class="fa fa-edit"></span>', [
                                        'value' => $url,
                                        'name' => 'Category Update',
                                        'title' => Yii::t('yii', 'update'),
                                        'class' => 'btn btn-warning btn-xs gridbtncustom updatedata',
                                        'data-toggle'=> 'tooltip',
                                ]);                                
            
                              }, 
                              


                          ]                            
                            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
</div>
</div>
</div>



  <?php 
            Modal::begin([
                    'header' => '<h2>IMAGE VIEW CONTROL</h2>',
                    'id' => 'modal', 
                    'size' => 'modal-md',

                ]);

            echo "<div id='modalContent'></div>";
            Modal::end();

     ?>
     <!--Modal Start -->
<?php 
    Modal::begin([
            'header' => '<h3>IMAGE VIEW CONTROL</h3>',
            'id' => 'alertmodel',
            'size'=> 'modal-md',
        ]);

    echo "<div id='alertmodalContent'>
           
            <img id='bannerimg' src='' width='550' height='350'/>
            <div class='modal-footer'>
                <input type='hidden' class='data1'>
                
                <button class='btn' data-dismiss='modal' aria-hidden='true'><i class='fa fa-fw fa-close'></i> Close</button>

            </div> 
        </div>";

    Modal::end();
?>
<!--Modal End -->

<script>
$(document).ready(function(){
    $('#modalCreate, .updatedata').click(function(){
        $("#modalContent").html('<center><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><center>');
        $('#modal').modal('show').find('#modalContent').load($(this).attr('value'));
    });

    $('td > img').css('cursor','pointer');



     /* When Model is in Page the User Profile option will not come for avoid that bug the below code has been resolved it. */
        $('.dropdown-toggle').click(function(){
             $('.navbar-nav > .user ').toggleClass("open");
            $(".navbar-nav > .user > .dropdown-toggle").attr("aria-expanded","true");
        });
       
});

$(document).on('click', 'td > img', function(){
            
         var url = $(this).attr('src');
         var imgUrl = '<?php echo Url::base(); ?>' + '/' + url;

         $('#bannerimg').attr('src',imgUrl);
        $("#alertmodalContent").html('<center><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><center>');
         $('#alertmodel').modal('show').find('#alertmodalContent').load();
        
});



$(document).ready(function(){
    $('#MyButton').click(function(){

        var HotId = $('#w1').yiiGridView('getSelectedRows');
          $.ajax({
            type: 'POST',
            url : 'index.php?r=images-view/multipledelete',
            data : {row_id: HotId},
            success : function() {
              $(this).closest('tr').remove(); //or whatever html you use for displaying rows
            }
        });

    });
    });


    
</script>