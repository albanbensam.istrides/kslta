<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\DropdownManagement;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model backend\models\ImagesView */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    #blah{
    margin-top: -75px;
    margin-right: 0px;
  }
  </style>
<div class="images-view-form">

     <?php $form = ActiveForm::begin(['id' => 'imagecontrol', 
                                    'options' => ['enctype' => 'multipart/form-data']
                                    ]); ?>

     <div class="col-md-12">
             <?= $form->field($model, 'image_file_location')->fileInput(['id'=> 'imgInp', 'class' => 'btn btn-primary']) ?>
            <img id="blah" class="profile-user-img img-responsive img-circle" src=<?php echo $model->isNewRecord ? "control_images/Photo-icon.png" : $model->image_file_location;  ?> />
    </div>


    <?=
$form->field($model, 'image_visible_page')
     ->dropDownList(
            ArrayHelper::map(DropdownManagement::find()->where(['dropdown_key' => 'page_display_tag'])->orderBy(['dropdown_order' => 'SORT_ASC'])->all(), 'dropdown_id', 'dropdown_value')
            , ['prompt' => '---Select Image Page View---'])
?>

    <?= $form->field($model, 'image_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image_over_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image_top_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image_sub_title')->textarea(['rows' => 2]) ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
     /* Onchange Image View Start */
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]);
            }
        }
        
        $("#imgInp").change(function(){
            readURL(this);
        });
    /* Onchange Image View End */

</script>