<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\DropdownManagement;
use backend\models\PageContents;
use backend\models\CoachingPages;
/* @var $this yii\web\View */
/* @var $model backend\models\Event */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="affilatedclubs-form">
<section class="content">
<!-- Info boxes -->
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="page-content-form">
                    <div class="box-header with-border">
                     <h3 class="box-title"><?= $model->isNewRecord ? '<i class="fa fa-fw fa-sticky-note-o"></i>' : '<i class="fa fa-fw fa-sticky-note"></i>' ?> <?= Html::encode($this->title) ?></h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
 <div class="col-md-12">
    	<div class="col-md-6">
    <?= $form->field($model, 'event_title')->textInput(['maxlength' => true]) ?>
		</div>
		 
    	<div class="col-md-6">
    <?= $form->field($model, 'event_header')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
 
 <div class="col-md-12">
    	<div class="col-md-6">
    <?= $form->field($model, 'event_name')->textInput(['maxlength' => true]) ?>
		</div>
		 
    	<div class="col-md-6">
    		<?php
            $dataList=ArrayHelper::map(PageContents::find()->asArray()->all(), 'page_name', 'page_content_title');
			$dataList2=ArrayHelper::map(CoachingPages::find()->asArray()->all(), 'coaching_page_name', 'coaching_page_title');
			$output_data=array();			 
			$output_data['news']='News';
			foreach($dataList as $page_name =>$page_content_title){
				$output_data[$page_name]=$page_content_title;
			}
			foreach($dataList2 as $coaching_page_name =>$coaching_page_title){
				$output_data["".$coaching_page_name]=$coaching_page_title;
			}
            ?>
            <?= $form->field($model, 'event_url')->dropDownList($output_data, 
                    ['prompt'=>'-----Select -----']) ?>
    
		</div>
	</div>
		 <div class="col-md-12">
    	<div class="col-md-6">
    <?= $form->field($model, 'event_img')->fileInput(['maxlength' => true]) ?>
	</div>
	<div class="col-md-6">
    <?= $form->field($model, 'order_no')->TextInput(['maxlength' => true]) ?>
	</div>
	</div>
	<div class="col-md-12">
    	<div class="col-md-6">
    </div>
    <div class="col-md-6">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    </div>
    
</div>
</div>
    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
</div>
</div>
</div>
</section> 
</div>