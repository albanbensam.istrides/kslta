<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CoachingPagesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coaching-pages-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'coaching_id') ?>

    <?= $form->field($model, 'gallery_id') ?>

    <?= $form->field($model, 'coaching_page_title') ?>

    <?= $form->field($model, 'coaching_page_name') ?>

    <?= $form->field($model, 'coaching_tab_one_title') ?>

    <?php // echo $form->field($model, 'coaching_tab_one_content') ?>

    <?php // echo $form->field($model, 'coaching_tab_two_title') ?>

    <?php // echo $form->field($model, 'coaching_tab_two_content_1') ?>

    <?php // echo $form->field($model, 'coaching_tab_two_content_2') ?>

    <?php // echo $form->field($model, 'coaching_tab_three_title') ?>

    <?php // echo $form->field($model, 'coaching_file_upload') ?>

    <?php // echo $form->field($model, 'coaching_created_at') ?>

    <?php // echo $form->field($model, 'coaching_modified_at') ?>

    <?php // echo $form->field($model, 'coaching_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
