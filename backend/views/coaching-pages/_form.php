<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\KslGalleryModel;
use backend\models\DropdownManagement;
use backend\models\TournamentEventModel;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\KslTournaments */
/* @var $form yii\widgets\ActiveForm */
?>

<link href="<?php echo Url::base(); ?>/plugins/jQuery-Form-Wizard-Plugin-Smart-Wizard/styles/demo_style.css" rel="stylesheet" type="text/css">
<link href="<?php echo Url::base(); ?>/plugins/jQuery-Form-Wizard-Plugin-Smart-Wizard/styles/smart_wizard.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo Url::base(); ?>/plugins/jQuery-Form-Wizard-Plugin-Smart-Wizard/js/jquery.smartWizard.js"></script>

<script src="<?php echo Url::base(); ?>/plugins/tinymce/tinymce.min.js" type="text/javascript"></script>


<style>
.stepContainer{
  height: 550px !important;
}
</style>

<!--  <section class="content-header">
        <h1>
            Add Tournament

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Tournament</li>
            <li class="active">Add Tournament</li>
        </ol>
    </section>-->



<div class="box box-info">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <!-- Content Header (Page header) -->
    <div class="box-header with-border">
                     <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div><!-- /.box-header -->
    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <!-- left column -->
            <div class="col-md-12 stepwise-tbl">
                <!-- general form elements -->

                <table align="center" border="0" cellpadding="0" cellspacing="0">
                    <tr><td>

                            <div id="wizard2" class="swMain">
                                <ul>
                                    <li><a href="#step-1">
                                            <span class="stepDesc">
                                                Page Details
                                            </span>
                                        </a></li>
                                    <li><a href="#step-2">
                                            <span class="stepDesc">                                            
                                             Tab One
                                            </span>
                                        </a></li>
                                    <li><a href="#step-3">                                            
                                            <span class="stepDesc">
                                              Tab Two
                                            </span>                   
                                        </a></li>
                                    <li><a href="#step-4">                                           
                                            <span class="stepDesc">
                                                Tab Three
                                            </span>                   
                                        </a></li>
                                </ul>

                                <div id="step-1">                                       
                                    <div class="row">
                                        
                                            
                                            <div class="col-md-6">
                                                <?= $form->field($model, 'coaching_page_title')->textInput(['maxlength' => true]) ?>

                                            </div>
                                            <div class="col-md-6">
                                                <?= $form->field($model, 'coaching_page_name')->textInput(['maxlength' => true]) ?>

                                            </div>
     
                                           <div class="col-md-6">
                                            <?=
                                                $form->field($model, 'gallery_id')
                                                     ->dropDownList(
                                                            ArrayHelper::map(KslGalleryModel::find()->all(), 'gallery_id', 'gallery_title'), ['prompt' => 'Select Gallery']
                                                            )
                                                ?>
                                                
                                            </div>

                                        <div class="col-md-6">
                                                <div class="form-group">
                                                    <?= $form->field($model, 'coaching_top_main_heading')->textInput(['maxlength' => true]) ?>
                                                </div>
                                            </div>

                                             <div class="col-md-6">
                                                <div class="form-group">
                                                    <?= $form->field($model, 'coaching_top_sub_heading')->textInput(['maxlength' => true]) ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <?= $form->field($model, 'coaching_content_heading')->textInput(['maxlength' => true]) ?>
                                                </div>
                                            </div>
                                      <div class="col-md-4">
                                            <div class="form-group">
                                                <?= $form->field($model, 'coaching_top_background_img')->fileInput(['id'=> 'imgInp', 'class' => 'btn btn-primary']) ?>

                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                           <img id="blah" class="profile-user-img img-responsive img-circle" src=<?php echo $model->isNewRecord ? "control_images/Photo-icon.png" : $model->coaching_top_background_img;  ?> />
                                           <br/><span style="margin-left:27px;">Image View</span>
                                        </div>
										 <div class="col-md-4">
                                            <div class="form-group">
                                                <?= $form->field($model, 'content_portfolio_img')->fileInput(['id'=> 'portimg', 'class' => 'btn btn-primary']) ?>

                                            </div>
                                        </div>

                                      <div class="col-md-2">
                                           <img id="blah" class="profile-user-img img-responsive img-circle" src=<?php echo $model->isNewRecord ? "control_images/Photo-icon.png" : $model->content_portfolio_img;  ?> />
                                           <br/><span style="margin-left:27px;">Image View</span>
                                        </div>
										
										
										
                                        <div class="col-md-3">
                                          <div class="form-group">
                                              <?= $form->field($model, 'coaching_meta_tag_name')->textarea(['rows' => 3]) ?>
                                          </div>
                                      </div>

                                       <div class="col-md-3">
                                          <div class="form-group">
                                          <?= $form->field($model, 'coaching_meta_tag_description')->textarea(['rows' => 3]) ?>
                                          </div>
                                      </div>
                                     

                                        
                                   
                                    <div class="col-md-6">
                                          <div class="form-group">
                                            <?= $form->field($model, 'remove_content_img')->checkbox(); ?>
                                          </div>
                                      </div>
                                       <div class="col-md-6">
                                          <div class="form-group">
                                            <?= $form->field($model, 'coaching_assign_sidebar')->checkbox(); ?>
                                          </div>
                                      </div>
                                </div>
                                </div>


                                <div id="step-2">                                       
                                    <div class="row">
                                        <div class="col-md-12">
                                         
                                            <div class="form-group">
                                                <?= $form->field($model, 'coaching_tab_one_title')->textInput(['maxlength' => true]) ?>

                                            </div> 
                                             <div class="form-group" name = "CoachingPages[coaching_tab_one_content]">
                                                <?= $form->field($model, 'coaching_tab_one_content')->textarea(['name' => 'CoachingPages[coaching_tab_one_content]', 'id'=>'coachingpages-coaching_tab_one_content', 'class' => 'form-control contentEditor']) ?>
                                            </div>
                                        </div>
                                       
                                    </div>         
                                </div>     


                                <div id="step-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <?= $form->field($model, 'coaching_tab_two_title')->textInput(['maxlength' => true]) ?>

                                            </div>

                                             <div class="form-group" name = "CoachingPages[coaching_tab_two_content_1]">
                                                <?= $form->field($model, 'coaching_tab_two_content_1')->textarea(['name' => 'CoachingPages[coaching_tab_two_content_1]', 'id'=>'coachingpages-coaching_tab_two_content_1', 'class' => 'form-control contentEditor']) ?>
                                            </div>

                                            <!-- <div class="form-group" name = "CoachingPages[coaching_tab_two_content_2]">
                                                <?//= $form->field($model, 'coaching_tab_two_content_2')->textarea(['name' => 'CoachingPages[coaching_tab_two_content_2]', 'id'=>'coachingpages-coaching_tab_two_content_2', 'class' => 'form-control contentEditor']) ?>
                                            </div>-->
                                          
                                            
                                        </div>
                                        
                                    </div>                    
                                </div>

                                <div id="step-4">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <?= $form->field($model, 'coaching_tab_three_title')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="form-group">
                                            <?= $form->field($model, 'coaching_file_upload')->fileInput(['id'=> 'imgInp', 'class' => 'btn btn-default']) ?>
                                            </div>
                                             <div class="form-group" name = "CoachingPages[coaching_tab_three_content_1]">
                                                <?= $form->field($model, 'coaching_tab_three_content_1')->textarea(['name' => 'CoachingPages[coaching_tab_three_content_1]', 'id'=>'coachingpages-coaching_tab_three_content_1', 'class' => 'form-control contentEditor']) ?>
                                            </div>


                                            <?php if($model->isNewRecord == false) { ?>
                                              <div> 
                                                 <a class='btn btn-success btn-sm downloadfactsheet' href=<?= $model->coaching_file_upload ?> target='_blank'><i class='fa fa-fw fa-download'></i> Download the Exits File</a>
                                              </div>

                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End SmartWizard Content 1 -->

                        </td></tr>
                </table>  
            </div><!--/.col (left) -->

        </div>
        <!-- right column -->
    </section><!-- /.content -->
  
    <?php ActiveForm::end(); ?>  

</div><!-- /.content-wrapper -->

<script type="text/javascript">
   
    $(document).ready(function(){
        // Smart Wizard         
        $('#wizard2').smartWizard({transitionEffect:'slideleft',onLeaveStep:leaveAStepCallback,onFinish:onFinishCallback,enableFinishButton:true});

      function leaveAStepCallback(obj){
        var step_num= obj.attr('rel');
        return validateSteps(step_num);
      }
      
      function onFinishCallback(){
       if(validateAllSteps()){
        $('#w0').submit();
       }
      }
            
        });
       
    function validateAllSteps(){
       var isStepValid = true;
       
       if(validateStep1() == false){
         isStepValid = false;
         $('#wizard2').smartWizard('setError',{stepnum:1,iserror:true});         
       }else{
         $('#wizard2').smartWizard('setError',{stepnum:1,iserror:false});
       }
       
       if(validateStep2() == false){
         isStepValid = false;
         $('#wizard2').smartWizard('setError',{stepnum:3,iserror:true});         
       }else{
         $('#wizard2').smartWizard('setError',{stepnum:3,iserror:false});
       }
       
       if(!isStepValid){
          $('#wizard2').smartWizard('showMessage','Please correct the errors in the steps and continue');
       }
              
       return isStepValid;
    }   
        
        
        function validateSteps(step){
          var isStepValid = true;
      // validate step 1
      if(step == 1){
        if(validateStep1() == false ){
          isStepValid = false; 
         // $('#wizard2').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
        // $('#wizard2').smartWizard('setError',{stepnum:step,iserror:true});         
        }else{
         // $('#wizard2').smartWizard('hideMessage');
        //  $('#wizard2').smartWizard('setError',{stepnum:step,iserror:false});
        }
      }
      
      // validate step3
      if(step == 2){
        if(validateStep2() == false ){
          isStepValid = false; 
         // $('#wizard2').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
        //  $('#wizard2').smartWizard('setError',{stepnum:step,iserror:true});         
        }else{
         // $('#wizard2').smartWizard('hideMessage');
         // $('#wizard2').smartWizard('setError',{stepnum:step,iserror:false});
        }
      }
      
      if(step == 3){
        if(validateStep2() == false ){
          isStepValid = false; 
         // $('#wizard2').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
        //  $('#wizard2').smartWizard('setError',{stepnum:step,iserror:true});         
        }else{
         // $('#wizard2').smartWizard('hideMessage');
         // $('#wizard2').smartWizard('setError',{stepnum:step,iserror:false});
        }
      }
      
      if(step == 4){
        if(validateStep2() == false ){
          isStepValid = false; 
         // $('#wizard2').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
        //  $('#wizard2').smartWizard('setError',{stepnum:step,iserror:true});         
        }else{
         // $('#wizard2').smartWizard('hideMessage');
         // $('#wizard2').smartWizard('setError',{stepnum:step,iserror:false});
        }
      }
      
      return isStepValid;
    }
        
        function validateStep1(){
       var isValid = true; 
      
       // var un = $('#coachingpages-coaching_page_title').val();
       // if(!un && un.length <= 0){
       //   $('#w0').yiiActiveForm('updateAttribute', 'coachingpages-coaching_page_title', ["Title cannot be blank."]);
       //   isValid = false;       
       // }
       

       // var un = $('#coachingpages-coaching_page_name').val();
       // if(!un && un.length <= 0){
       //   $('#w0').yiiActiveForm('updateAttribute', 'coachingpages-coaching_page_name', ["Page Name cannot be blank."]);
       //   isValid = false;       
       // }


       // var un = $('#coachingpages-gallery_id').val();
       // if(!un && un.length <= 0){
       //   $('#w0').yiiActiveForm('updateAttribute', 'coachingpages-gallery_id', ["Galley cannot be blank."]);
       //   isValid = false;       
       // }
       
       return isValid;
    }
    function validateStep2(){
      var isValid = true;    
      // var un = $('#coachingpages-coaching_tab_one_title').val();
      //  if(!un && un.length <= 0){
      //    $('#w0').yiiActiveForm('updateAttribute', 'coachingpages-coaching_tab_one_title', ["Tab One Title cannot be blank."]);
      //    isValid = false;       
      //  }
      return isValid;
    }
    
    function validateStep3(){
      var isValid = true;    
      //validate email  email
      // var un = $('#coachingpages-coaching_tab_two_title').val();
      //  if(!un && un.length <= 0){
      //    $('#w0').yiiActiveForm('updateAttribute', 'coachingpages-coaching_tab_two_title', ["Tab Two Title cannot be blank."]);
      //    isValid = false;       
      //  }
      return isValid;
    }
    
    function validateStep4(){
      var isValid = true;    
      
      // var un = $('#coachingpages-coaching_tab_three_title').val();
      //  if(!un && un.length <= 0){
      //    $('#w0').yiiActiveForm('updateAttribute', 'coachingpages-coaching_tab_three_title', ["Tab Three Title cannot be blank."]);
      //    isValid = false;       
      //  }
  
      return isValid;
    }
    
        
</script>
<script type="text/javascript">
    $(".datepicker").datepicker();
    

      tinymce.init({
        selector: '.contentEditor',
        height: 250,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ],
        content_css: [
            '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    }); 


    $(document).ready(function () {

    /* Onchange Image View Start */
        function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
		
    }
    
    $("#imgInp").change(function(){
        readURL(this);
    });
	

    /* Onchange Image View End */
        
        //  Wizard 1    
        $('#wizard1').smartWizard({
            transitionEffect: 'fade',
            onFinish: onFinishCallback,
            onLeaveStep: leaveAStepCallback,
        });
        function leaveAStepCallback(obj, context) {
            // To check and enable finish button if needed
            if (context.fromStep >= 2) {
                $('#wizard1').smartWizard('enableFinish', true);
            }
            return true;
        }
        //  Wizard 2
        $('#wizard2').smartWizard({transitionEffect: 'slide', onFinish: onFinishCallback});
        function onFinishCallback() {
           //alert('If you finish click create/update');
        }
    });
    

</script>  
