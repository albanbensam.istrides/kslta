<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CoachingPages */

$this->title = 'Update Coaching Pages: ' . ' ' . $model->coaching_id;
$this->params['breadcrumbs'][] = ['label' => 'Coaching Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->coaching_id, 'url' => ['view', 'id' => $model->coaching_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="coaching-pages-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
