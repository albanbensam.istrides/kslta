<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<style>
	
	.morecontent span {
    display: none;
}
.morelink {
    display: block;
}
</style>
<div class="col-md-12">
    <!-- The time line -->
    <ul class="timeline">
        <!-- timeline time label -->
       
        <!-- /.timeline-label -->
        <!-- timeline item -->
        <li>
<!--             <i class="fa fa-newspaper-o bg-blue"></i> -->            <div class="timeline-item">
<!--                 <span class="time"><i class="fa fa-clock-o"></i> 12:05</span> -->
                <h3 class="timeline-header"><a href="<?= Yii::$app->urlManager->createUrl(['coaching-pages/view', 'id' => $model->coaching_id]); ?>"><?= $model->coaching_page_title; ?></a> </h3>
                <div class="timeline-body">
                	
    			<span class="more">
                    <?=  strip_tags($model->coaching_page_name); ?>
                 </span>
                </div>
                <div class="timeline-footer">
<!--                     <a class="btn btn-primary btn-xs newsreadmore" data-toggle="tooltip" title="Read more"><i class="fa fa-ellipsis-h"></i></a> -->                    
					<a class="btn btn-default btn-xs"  data-pjax="0" data-method="post" data-confirm="Are you sure you want to delete this item?" aria-label="Delete" title="Delete" href="<?php echo Yii::$app->homeUrl;  ?>?r=coaching-pages%2Fdelete&id= <?= $model->coaching_id;  ?>" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></a>
                    <a class="btn btn-warning btn-xs" href="<?= Yii::$app->urlManager->createUrl(['coaching-pages/update', 'id' => $model->coaching_id]); ?>" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
<!--                    <a data-pjax="0" data-method="post" data-confirm="Are you sure you want to delete this item?" aria-label="Delete" title="Delete" href="Url::toRoute('ksl-news-tbl/delete'.$model->news_id);  ">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>-->
                    <label> Posted by :</label> Admin
<!-- <a class="btn btn-danger btn-xs" data-toggle="tooltip" title="Rejected"><i class="fa fa-ban"></i></a>
 <a class="btn btn-success btn-xs" data-toggle="tooltip" title="Approved"><i class="fa fa-check"></i></a>-->
                   <div class="pull-right footerdata">
                        Modified at <i class="fa fa-fw fa-clock-o"></i> <?php echo date("d-m-Y h:i:s",strtotime($model->coaching_modified_at));  ?>
                     <label>
                          
                    
                        
                       
                    </div>
                </div>
            </div>
        </li>
        <!-- END timeline item -->
        <!-- timeline item -->
    </ul>
</div><!-- /.col -->


