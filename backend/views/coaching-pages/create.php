<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CoachingPages */

$this->title = 'Add Coaching Pages';
$this->params['breadcrumbs'][] = ['label' => 'Coaching Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coaching-pages-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
