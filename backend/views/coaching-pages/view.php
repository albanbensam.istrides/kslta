<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\CoachingPages */
$this->title = 'View Coaching Pages';
$this->params['breadcrumbs'][] = ['label' => 'Coaching Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coaching-pages-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <p class="pull-right">
        <?= Html::a('Update', ['update', 'id' => $model->coaching_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->coaching_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          //  'coaching_id',
            'gallery.gallery_title',
            [
                'attribute'=>'photo',
                'value'=>$model->gallery->gallery_link_address,
                'format' => ['image',['width'=>'300','height'=>'300']],
            ],
            'coaching_page_title',
            'coaching_page_name',
            'coaching_tab_one_title',
            'coaching_tab_one_content:ntext',
            'coaching_tab_two_title',
            'coaching_tab_two_content_1:ntext',
            'coaching_tab_two_content_2:ntext',
            'coaching_tab_three_title',
            'coaching_file_upload',
            'coaching_created_at',
            'coaching_modified_at',
          // 'coaching_status',
        ],
    ]) ?>

</div>
