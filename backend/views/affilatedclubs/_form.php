<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\States;

/* @var $this yii\web\View */
/* @var $model backend\models\Affilatedclubs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="affilatedclubs-form">
<section class="content">
<!-- Info boxes -->
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="page-content-form">
                    <div class="box-header with-border">
                     <h3 class="box-title"><?= $model->isNewRecord ? '<i class="fa fa-fw fa-sticky-note-o"></i>' : '<i class="fa fa-fw fa-sticky-note"></i>' ?> <?= Html::encode($this->title) ?></h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
    <?php $form = ActiveForm::begin(); ?>
<div class="col-md-12">
	<div class="col-md-6">
		 <?php
   				 $d=States::find()->all();
    			$a= ArrayHelper::map($d ,'state_id','state_name'); ?>
    <?= $form->field($model, 'state_id')->dropDownList($a,['prompt'=>'select state...']); ?>
    
    
	</div>
	<div class="col-md-6">
		 <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
  
</div>
</div>
<div class="col-md-12">
	<div class="col-md-6">
		  <?= $form->field($model, 'address1')->textarea(['maxlength' => true]) ?>
   
   </div>
<div class="col-md-6">
	 <?= $form->field($model, 'address2')->textarea(['maxlength' => true]) ?>
   
</div>
</div>
<div class="col-md-12">
	<div class="col-md-6">
		 <?= $form->field($model, 'mobile')->textInput() ?>
  
   </div>
<div class="col-md-6">
	  <?= $form->field($model, 'phone')->textInput() ?>
    
</div>
</div>
<div class="col-md-12">
	<div class="col-md-6">
		<?= $form->field($model, 'fax')->textInput(['maxlength' => true]) ?>
   
   </div>
<div class="col-md-6">
	 <?= $form->field($model, 'email')->textInput(['maxlength' => true,'type' => 'email',]) ?>
   
</div>
</div>
<div class="col-md-12">
	<div class="col-md-6">
 <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>
 </div>
 <div class="col-md-6">
    <div class="form-group pull-right">
        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div></div>
    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
</div>
</div>
</section> 
