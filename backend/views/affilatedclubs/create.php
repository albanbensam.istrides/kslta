<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Affilatedclubs */

$this->title = 'Add Affilatedclubs';
$this->params['breadcrumbs'][] = ['label' => 'Affilatedclubs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="affilatedclubs-create">

   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
