<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Affilatedclubs */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Affilatedclubs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="affilatedclubs-view">

    

   

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'id',
            'state_name',
            'name',
            'address1',
            'address2',
            'mobile',
            'phone',
            'fax',
            'email:email',
            'website',
        ],
    ]) ?>

</div>
