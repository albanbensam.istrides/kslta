<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Affilatedclubs */

$this->title = 'Update Affilatedclubs: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Affilatedclubs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="affilatedclubs-update">

   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
