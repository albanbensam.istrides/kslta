<?php

namespace backend\controllers;

use Yii;
use backend\models\CoachingPages;
use backend\models\CoachingPagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
/**
 * CoachingPagesController implements the CRUD actions for CoachingPages model.
 */
class CoachingPagesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
          'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],

                // ...
            ],
        ],


        ];
    }

    /* Avoid Bad Requrest 400 Error  */
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Lists all CoachingPages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CoachingPagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CoachingPages model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CoachingPages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CoachingPages();
        
        if ($model->load(Yii::$app->request->post())) {
     
        if($_POST['CoachingPages']['coaching_page_title'] != ''){
        $model->coaching_page_title = $_POST['CoachingPages']['coaching_page_title'];
        }else{
            $model->coaching_page_title = '';
        }

        if($_POST['CoachingPages']['coaching_page_name'] != ''){
        $model->coaching_page_name = $_POST['CoachingPages']['coaching_page_name'];
        }else{
            $model->coaching_page_name = '';
        }

        if($_POST['CoachingPages']['gallery_id'] != ''){
        $model->gallery_id = $_POST['CoachingPages']['gallery_id'];
        }else{
            $model->gallery_id = '';
        }

        if($_POST['CoachingPages']['coaching_top_main_heading'] != ''){
        $model->coaching_top_main_heading = $_POST['CoachingPages']['coaching_top_main_heading'];
        }else{
            $model->coaching_top_main_heading = '';
        }
        $model->coaching_assign_sidebar = $_POST['CoachingPages']['coaching_assign_sidebar'];

        if($_POST['CoachingPages']['coaching_top_sub_heading'] != ''){
        $model->coaching_top_sub_heading = $_POST['CoachingPages']['coaching_top_sub_heading'];
        }else{
            $model->coaching_top_sub_heading = '';
        }

         $model->coaching_content_heading = $_POST['CoachingPages']['coaching_content_heading'];

        if($_POST['CoachingPages']['coaching_meta_tag_name'] != ''){
        $model->coaching_meta_tag_name = $_POST['CoachingPages']['coaching_meta_tag_name'];
        }else{
            $model->coaching_meta_tag_name = '';
        }
        
        if($_POST['CoachingPages']['coaching_meta_tag_description'] != ''){
        $model->coaching_meta_tag_description = $_POST['CoachingPages']['coaching_meta_tag_description'];
        }else{
            $model->coaching_meta_tag_description = '';
        }


        if($_POST['CoachingPages']['coaching_tab_one_title'] != ''){
        $model->coaching_tab_one_title = $_POST['CoachingPages']['coaching_tab_one_title'];
        }else{
            $model->coaching_tab_one_title = '';
        }

        if($_POST['CoachingPages']['coaching_tab_one_content'] != ''){
        $model->coaching_tab_one_content = $_POST['CoachingPages']['coaching_tab_one_content'];
        }else{
            $model->coaching_tab_one_content = '';
        }

        if($_POST['CoachingPages']['coaching_tab_two_title'] != ''){
        $model->coaching_tab_two_title = $_POST['CoachingPages']['coaching_tab_two_title'];
        }else{
            $model->coaching_tab_two_title = '';
        }

        if($_POST['CoachingPages']['coaching_tab_two_content_1'] != ''){
        $model->coaching_tab_two_content_1 = $_POST['CoachingPages']['coaching_tab_two_content_1'];
        }else{
            $model->coaching_tab_two_content_1 = '';
        }

          $model->coaching_tab_two_content_2 = '';

        if($_POST['CoachingPages']['coaching_tab_three_title'] != ''){
        $model->coaching_tab_three_title = $_POST['CoachingPages']['coaching_tab_three_title'];
        }else{
            $model->coaching_tab_three_title = '';
        }

            if ($_FILES['CoachingPages']['error']['coaching_file_upload'] == 0) {
                    $rand = rand(0, 99999); // random number generation for unique image 
                    $model->file = UploadedFile::getInstance($model, 'coaching_file_upload');
                    $image_name = 'uploads/coaching/' . $model->file->basename . $rand.'_KSLTA' . "." . $model->file->extension;
                    $model->file->saveAs($image_name);
                    $model->coaching_file_upload = $image_name;
                }
        if($_POST['CoachingPages']['coaching_tab_three_content_1'] != ''){
        $model->coaching_tab_three_content_1 = $_POST['CoachingPages']['coaching_tab_three_content_1'];
        }else{
            $model->coaching_tab_three_content_1 = '';
        }

                if ($_FILES['CoachingPages']['error']['coaching_top_background_img'] == 0) {
                    $rand = rand(0, 99999); // random number generation for unique image 
                    $model->file = UploadedFile::getInstance($model, 'coaching_top_background_img');
                    $image_name = 'uploads/bannerimages/' . $model->file->basename . $rand.'_KSLTA' . "." . $model->file->extension;
                    $model->file->saveAs($image_name);
                    $model->coaching_top_background_img = $image_name;
                }
                if($_POST['CoachingPages']['remove_content_img'] == 1)
                {
                 $model->content_portfolio_img='';
                }
              else
                {
        if ($_FILES['CoachingPages']['error']['content_portfolio_img'] == 0) {
                    $rand = date("His"); // random number generation for unique image 
                    $model->file = UploadedFile::getInstance($model, 'content_portfolio_img');
                    $image_name = 'uploads/bannerimages/' . $model->file->basename . $rand.'_KSLTA' . "." . $model->file->extension;
                    $model->file->saveAs($image_name);
                    $model->content_portfolio_img = $image_name;
                }
            }
            $model->remove_content_img = $_POST['CoachingPages']['remove_content_img'];

                 if ($model->save()) {
                    Yii::$app->getSession()->setFlash('success', 'Coaching Deatils has been added.');
                    return $this->redirect(['index']);
                } else {
                    Yii::$app->getSession()->setFlash('error', 'Something went wrong !');
                }
            return $this->redirect(['index']);
        } 


        return $this->render('create', [
                'model' => $model,
            ]);
    }

    /**
     * Updates an existing CoachingPages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
     
        if($_POST['CoachingPages']['coaching_page_title'] != ''){
        $model->coaching_page_title = $_POST['CoachingPages']['coaching_page_title'];
        }else{
            $model->coaching_page_title = '';
        }

        if($_POST['CoachingPages']['coaching_page_name'] != ''){
        $model->coaching_page_name = $_POST['CoachingPages']['coaching_page_name'];
        }else{
            $model->coaching_page_name = '';
        }

        if($_POST['CoachingPages']['gallery_id'] != ''){
        $model->gallery_id = $_POST['CoachingPages']['gallery_id'];
        }else{
            $model->gallery_id = '';
        }
         $model->coaching_assign_sidebar = $_POST['CoachingPages']['coaching_assign_sidebar'];

        if($_POST['CoachingPages']['coaching_top_main_heading'] != ''){
        $model->coaching_top_main_heading = $_POST['CoachingPages']['coaching_top_main_heading'];
        }else{
            $model->coaching_top_main_heading = '';
        }

        if($_POST['CoachingPages']['coaching_top_sub_heading'] != ''){
        $model->coaching_top_sub_heading = $_POST['CoachingPages']['coaching_top_sub_heading'];
        }else{
            $model->coaching_top_sub_heading = '';
        }
        $model->coaching_content_heading = $_POST['CoachingPages']['coaching_content_heading'];

        if($_POST['CoachingPages']['coaching_meta_tag_name'] != ''){
        $model->coaching_meta_tag_name = $_POST['CoachingPages']['coaching_meta_tag_name'];
        }else{
            $model->coaching_meta_tag_name = '';
        }
        
        if($_POST['CoachingPages']['coaching_meta_tag_description'] != ''){
        $model->coaching_meta_tag_description = $_POST['CoachingPages']['coaching_meta_tag_description'];
        }else{
            $model->coaching_meta_tag_description = '';
        }


        if($_POST['CoachingPages']['coaching_tab_one_title'] != ''){
        $model->coaching_tab_one_title = $_POST['CoachingPages']['coaching_tab_one_title'];
        }else{
            $model->coaching_tab_one_title = '';
        }

        if($_POST['CoachingPages']['coaching_tab_one_content'] != ''){
        $model->coaching_tab_one_content = $_POST['CoachingPages']['coaching_tab_one_content'];
        }else{
            $model->coaching_tab_one_content = '';
        }

        if($_POST['CoachingPages']['coaching_tab_two_title'] != ''){
        $model->coaching_tab_two_title = $_POST['CoachingPages']['coaching_tab_two_title'];
        }else{
            $model->coaching_tab_two_title = '';
        }

        if($_POST['CoachingPages']['coaching_tab_two_content_1'] != ''){
        $model->coaching_tab_two_content_1 = $_POST['CoachingPages']['coaching_tab_two_content_1'];
        }else{
            $model->coaching_tab_two_content_1 = '';
        }

       $model->coaching_tab_two_content_2 = '';

        if($_POST['CoachingPages']['coaching_tab_three_title'] != ''){
        $model->coaching_tab_three_title = $_POST['CoachingPages']['coaching_tab_three_title'];
        }else{
            $model->coaching_tab_three_title = '';
        }

            if ($_FILES['CoachingPages']['error']['coaching_file_upload'] == 0) {
                    $rand = date("His"); // random number generation for unique image 
                    $model->file = UploadedFile::getInstance($model, 'coaching_file_upload');
                    $image_name = 'uploads/coaching/' . $model->file->basename . $rand.'_KSLTA' . "." . $model->file->extension;
                    $model->file->saveAs($image_name);
                    $model->coaching_file_upload = $image_name;
                }
				 if($_POST['CoachingPages']['coaching_tab_three_content_1'] != ''){
        $model->coaching_tab_three_content_1 = $_POST['CoachingPages']['coaching_tab_three_content_1'];
        }else{
            $model->coaching_tab_three_content_1 = '';
        }


                if ($_FILES['CoachingPages']['error']['coaching_top_background_img'] == 0) {
                    $rand = date("His"); // random number generation for unique image 
                    $model->file = UploadedFile::getInstance($model, 'coaching_top_background_img');
                    $image_name = 'uploads/bannerimages/' . $model->file->basename . $rand.'_KSLTA' . "." . $model->file->extension;
                    $model->file->saveAs($image_name);
                    $model->coaching_top_background_img = $image_name;
                }

 		if($_POST['CoachingPages']['remove_content_img'] == 1)
                {
                
                 $model->content_portfolio_img='';
                }
              else
                {
        if ($_FILES['CoachingPages']['error']['content_portfolio_img'] == 0) {
                    $rand = date("His"); // random number generation for unique image 
                    $model->file = UploadedFile::getInstance($model, 'content_portfolio_img');
                    $image_name = 'uploads/bannerimages/' . $model->file->basename . $rand.'_KSLTA' . "." . $model->file->extension;
                    $model->file->saveAs($image_name);
                    $model->content_portfolio_img = $image_name;
                }
            }
 $model->remove_content_img = $_POST['CoachingPages']['remove_content_img'];
  
                 if ($model->save()) {
                
                    Yii::$app->getSession()->setFlash('success', 'Coaching Deatils has been added.');
                    return $this->redirect(['index']);
                } else {
                    Yii::$app->getSession()->setFlash('error', 'Something went wrong !');
                }
            return $this->redirect(['index']);
        } 

        return $this->render('update', [
                'model' => $model,
            ]);
    }

    /**
     * Deletes an existing CoachingPages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CoachingPages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CoachingPages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CoachingPages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
