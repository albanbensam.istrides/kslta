<?php

namespace backend\controllers;

use Yii;
use backend\models\DropdownManagement;
use backend\models\DropdownManagementSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * DropdownManagementController implements the CRUD actions for DropdownManagement model.
 */
class DropdownManagementController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
          'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],

                // ...
            ],
        ],


        ];
    }

    /**
     * Lists all DropdownManagement models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DropdownManagementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function beforeAction($action) {
    $this->enableCsrfValidation = false;
    return parent::beforeAction($action);
}

    /**
     * Displays a single DropdownManagement model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DropdownManagement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DropdownManagement();

        if ($model->load(Yii::$app->request->post())) {

            if($model->save()){
                 Yii::$app->getSession()->setFlash('success', 'DropDownList has been added success.');
                 return $this->redirect(['index']);
            }else{
                 Yii::$app->getSession()->setFlash('error', 'Something went wrong !');
                 return $this->redirect(['index']);
            }
           
        } 

         return $this->render('create', [
                'model' => $model,
            ]);
    }

    /**
     * Updates an existing DropdownManagement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            if($model->save()){
                 Yii::$app->getSession()->setFlash('success', $model->dropdown_value.' Sub Category has been updated.');
                 return $this->redirect(['index']);
            }else{
                 Yii::$app->getSession()->setFlash('error', 'Something went wrong !');
                 return $this->redirect(['index']);
            }
           
        } 

         return $this->render('update', [
                'model' => $model,
            ]);
    }

    /**
     * Deletes an existing DropdownManagement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DropdownManagement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return DropdownManagement the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DropdownManagement::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }	
}
