<?php

namespace backend\controllers;

use Yii;
use backend\models\TournamentPlayerList;
use backend\models\TournamentPlayerListSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
/**
 * TournamentPlayerListController implements the CRUD actions for TournamentPlayerList model.
 */
class TournamentPlayerListController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
          'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],

                // ...
            ],
        ],


        ];
    }

     public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Lists all TournamentPlayerList models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TournamentPlayerListSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TournamentPlayerList model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TournamentPlayerList model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TournamentPlayerList();
     
        if ($model->load(Yii::$app->request->post())) {

            $model->tourn_id = $_POST['TournamentPlayerList']['tourn_id'];
            $model->event_id = $_POST['TournamentPlayerList']['event_id'];


            if($_FILES['TournamentPlayerList']['error']['playerlists_filepath']==0)
            {
                    $rand = rand(0, 999); // random number generation for unique image save
                    $model->file = UploadedFile::getInstance($model, 'playerlists_filepath');
                    $image_name = 'uploads/playerlists/' . $model->file->basename .'_playerkslta_'. $rand . "." . $model->file->extension;
                    $model->file->saveAs($image_name);

                    $model->playerlists_filename = $model->file->basename .'_playerkslta_'. $rand . "." . $model->file->extension;
                    $model->playerlists_filepath = $image_name;
            }

            $model->playerlists_created_at = date("Y-m-d H:i:s");

            if($model->save()){
                 Yii::$app->getSession()->setFlash('success', 'Player List has been added.');
                 return $this->redirect(['tournament-schedule/index']);
            }else{
                 Yii::$app->getSession()->setFlash('error', 'Something went wrong !');
                 return $this->redirect(['tournament-schedule/index']);
            }
            
        } 

         return $this->renderAjax('create', [
                'model' => $model,
            ]);
    }

    /**
     * Updates an existing TournamentPlayerList model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $model->tourn_id = $_POST['TournamentPlayerList']['tourn_id'];
            $model->event_id = $_POST['TournamentPlayerList']['event_id'];


            if($_FILES['TournamentPlayerList']['error']['playerlists_filepath']==0)
            {
                    $rand = rand(0, 999); // random number generation for unique image save
                    $model->file = UploadedFile::getInstance($model, 'playerlists_filepath');
                    $image_name = 'uploads/playerlists/' . $model->file->basename .'_playerkslta_'. $rand . "." . $model->file->extension;
                    $model->file->saveAs($image_name);

                    $model->playerlists_filename = $model->file->basename .'_playerkslta_'. $rand . "." . $model->file->extension;
                    $model->playerlists_filepath = $image_name;
            }

            $model->playerlists_created_at = date("Y-m-d H:i:s");

            if($model->save()){
                 Yii::$app->getSession()->setFlash('success', $model->playerlists_filename.' Player List has been Updated.');
                 return $this->redirect(['tournament-schedule/index']);
            }else{
                 Yii::$app->getSession()->setFlash('error', 'Something went wrong !');
                 return $this->redirect(['tournament-schedule/index']);
            }
            
        } 

         return $this->renderAjax('update', [
                'model' => $model,
            ]);
    }

    /**
     * Deletes an existing TournamentPlayerList model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        Yii::$app->getSession()->setFlash('error', 'The Record has been Deleted!');
        return $this->redirect(['tournament-schedule/index']);
    }

    /**
     * Finds the TournamentPlayerList model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TournamentPlayerList the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TournamentPlayerList::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
