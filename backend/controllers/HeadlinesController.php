<?php

namespace backend\controllers;

use Yii;
use backend\models\Headlines;
use backend\models\HeadlinesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\KslNewsTbl;

/**
 * HeadlinesController implements the CRUD actions for Headlines model.
 */
class HeadlinesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Headlines models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HeadlinesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Headlines model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Headlines model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Headlines();
		 
        if ($model->load(Yii::$app->request->post())) {
        	$model->order_no=$_POST['Headlines']['order_no'];
			$model->created_date = date('Y-m-d, H:m:s');
			 if($model->save()){
			 	return $this->redirect(['index']);
			 }
            
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Headlines model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
 
       if ($model->load(Yii::$app->request->post())) {
       	 
        $model->order_no=$_POST['Headlines']['order_no'];
			$model->created_date = date('Y-m-d H:m:s');
			 if($model->save()){
			 	 
			 	return $this->redirect(['index']);
			 }
            
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Headlines model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Headlines model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Headlines the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Headlines::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	
	 public function actionLists()
    {
    		if(isset($_GET['id'])){
    		//	echo $_GET['id']; 
				
				 $dataList=KslNewsTbl::find()->where(['news_title'=>$_GET['id']])->one();
				 
				 return $dataList->news_id;
    		}
    	
     
			 	
			 }
   
}
