<?php

namespace backend\controllers;

use Yii;
use backend\models\HomeFacilities;
use backend\models\HomeFacilitiesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
/**
 * HomeFacilitiesController implements the CRUD actions for HomeFacilities model.
 */
class HomeFacilitiesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all HomeFacilities models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HomeFacilitiesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single HomeFacilities model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new HomeFacilities model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new HomeFacilities();

       if ($model->load(Yii::$app->request->post())) {
       
        	if (isset($_FILES['HomeFacilities']['name']['image_upload']) && !empty($_FILES['HomeFacilities']['name']['image_upload'])) {
                    $rand = rand(0, 99999); // random number generation for unique image save
                  //  $model->scenario = 'imageUploaded';
                    $model->file = UploadedFile::getInstance($model, 'image_upload');
                    $image_name = 'uploads/facility/' . $model->file->basename . $rand . "." . $model->file->extension;
                    $model->file->saveAs($image_name);
                    $model->image_upload = $image_name;
                }
			$model->order_no=$_POST['HomeFacilities']['order_no'];
			$model->facility_content=$_POST['HomeFacilities']['facility_content'];
			$model->created_at = date('Y-m-d H:m:s');
			 
			if($model->save()){
			//echo "<pre>";	print_r($model->getErrors()); die;
				 return $this->redirect(['index']);
			}
           
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing HomeFacilities model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

       if ($model->load(Yii::$app->request->post())) {
       
        	if (isset($_FILES['HomeFacilities']['name']['image_upload']) && !empty($_FILES['HomeFacilities']['name']['image_upload'])) {
                    $rand = rand(0, 99999); // random number generation for unique image save
                  //  $model->scenario = 'imageUploaded';
                    $model->file = UploadedFile::getInstance($model, 'image_upload');
                    $image_name = 'uploads/facility/' . $model->file->basename . $rand . "." . $model->file->extension;
                    $model->file->saveAs($image_name);
                    $model->image_upload = $image_name;
                }
			$model->order_no=$_POST['HomeFacilities']['order_no'];
			$model->facility_content=$_POST['HomeFacilities']['facility_content'];
			$model->created_at = date('Y-m-d H:m:s');
			 
			if($model->save()){
			//echo "<pre>";	print_r($model->getErrors()); die;
				 return $this->redirect(['index']);
			}
           
        }  else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing HomeFacilities model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the HomeFacilities model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return HomeFacilities the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HomeFacilities::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
