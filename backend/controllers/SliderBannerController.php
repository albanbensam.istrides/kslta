<?php

namespace backend\controllers;

use Yii;
use backend\models\SliderBanner;
use backend\models\SliderBannerSearch;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
/**
 * SliderBannerController implements the CRUD actions for SliderBanner model.
 */
class SliderBannerController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
          'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],

                // ...
            ],
        ],


        ];
    }



    /* Avoid Bad Requrest 400 Error  */
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Lists all SliderBanner models.
     * @return mixed
     */
	public function actionMultipledelete()
    {
		
        $pk = Yii::$app->request->post('row_id');

        foreach ($pk as $key => $value) 
        {
            $sql = "DELETE FROM ksl_slider_banner WHERE banner_id = $value";
            $query = Yii::$app->db->createCommand($sql)->execute();
        }

        return $this->redirect(['index']);

    }
    public function actionIndex()
    {
        $searchModel = new SliderBannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SliderBanner model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SliderBanner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SliderBanner();

        if ($model->load(Yii::$app->request->post())) {
             $model->banner_date = date("Y-m-d H:i:s");
            $model->banner_status = "A";
            if (Yii::$app->request->isPost) {
                //Below condition is for whether the file is available or empty
                if (isset($_FILES['SliderBanner']['name']['banner_image']) && !empty($_FILES['SliderBanner']['name']['banner_image'])) {
                    $rand = rand(0, 9999); // random number generation for unique image save
                    $model->scenario = 'imageUploaded';
                    $model->file = UploadedFile::getInstance($model, 'banner_image');
                    $image_name = 'uploadimage/sliderbanner/' . $model->file->basename . $rand . "." . $model->file->extension;
                    $model->file->saveAs($image_name);
                    $model->banner_image = $image_name;
                }
                if ($model->save()) {
                    Yii::$app->getSession()->setFlash('success', 'Banner has been added.');
                    return $this->redirect(['index']);
                } else {
                    Yii::$app->getSession()->setFlash('error', 'Something went wrong !');
                }
//                if ($model->upload()) {
//                    return;
//                }
            }
        } 


         return $this->renderAjax('_form', [
                'model' => $model,
            ]);
    }

    /**
     * Updates an existing SliderBanner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
             $model->banner_date = date("Y-m-d H:i:s");
            $model->banner_status = "A";
            if (Yii::$app->request->isPost) {
                //Below condition is for whether the file is available or empty

                if ($_FILES['SliderBanner']['name']['banner_image'] != '') {
                    $rand = rand(0, 9999); // random number generation for unique image save
                    $model->scenario = 'imageUploaded';
                    $model->file = UploadedFile::getInstance($model, 'banner_image');
                    $image_name = 'uploadimage/sliderbanner/' . $model->file->basename . $rand . "." . $model->file->extension;
                    $model->file->saveAs($image_name);
                    $model->banner_image = $image_name;
                }
                if ($model->save()) {
                    Yii::$app->getSession()->setFlash('success', 'Banner has been Updated.');
                    return $this->redirect(['index']);
                } else {
                    Yii::$app->getSession()->setFlash('error', 'Something went wrong !');
                }
//                if ($model->upload()) {
//                    return;
//                }
            }
        } 

       return $this->renderAjax('_form', [
                'model' => $model,
            ]);
    }

    /**
     * Deletes an existing SliderBanner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SliderBanner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SliderBanner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SliderBanner::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
