<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use backend\models\ImagesView;
use backend\models\ImagesViewSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ImagesViewController implements the CRUD actions for ImagesView model.
 */
class ImagesViewController extends Controller
{
   public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
          'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],

                // ...
            ],
        ],


        ];
    }


     /* Avoid Bad Requrest 400 Error  */
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Lists all ImagesView models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ImagesViewSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ImagesView model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ImagesView model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ImagesView();

        if ($model->load(Yii::$app->request->post())) {
          

             if($_POST['ImagesView']['image_visible_page'] != ''){
                $model->image_visible_page = $_POST['ImagesView']['image_visible_page'];
                }

             if($_POST['ImagesView']['image_title'] != ''){
                $model->image_title = $_POST['ImagesView']['image_title'];
                }

             if($_POST['ImagesView']['image_over_title'] != ''){
                $model->image_over_title = $_POST['ImagesView']['image_over_title'];
                }

             if($_POST['ImagesView']['image_top_title'] != ''){
                $model->image_top_title = $_POST['ImagesView']['image_top_title'];
                }

             if($_POST['ImagesView']['image_sub_title'] != ''){
                $model->image_sub_title = $_POST['ImagesView']['image_sub_title'];
                }


            if($_FILES['ImagesView']['error']['image_file_location']==0)
            {
                    $rand = rand(0, 99); // random number generation for unique image save
                    $model->file = UploadedFile::getInstance($model, 'image_file_location');
                    $image_name = 'uploads/pages_images/' . $model->file->basename .'_kslta_'. $rand . "." . $model->file->extension;
                    $model->file->saveAs($image_name);

                    $model->image_file_name = $model->file->basename .'_kslta_'. $rand . "." . $model->file->extension;
                    $model->image_type = $model->file->extension;

                    $model->image_file_location = $image_name;
            }

            if($model->save()){
                 Yii::$app->getSession()->setFlash('success', 'Image has been added success.');
                 return $this->redirect(['index']);
            }else{
                 Yii::$app->getSession()->setFlash('error', 'Something went wrong !');
                 return $this->redirect(['index']);
            }
           
        } 

         return $this->renderAjax('create', [
                'model' => $model,
            ]);
    }

    /**
     * Updates an existing ImagesView model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
          

             if($_POST['ImagesView']['image_visible_page'] != ''){
                $model->image_visible_page = $_POST['ImagesView']['image_visible_page'];
                }

             if($_POST['ImagesView']['image_title'] != ''){
                $model->image_title = $_POST['ImagesView']['image_title'];
                }

             if($_POST['ImagesView']['image_over_title'] != ''){
                $model->image_over_title = $_POST['ImagesView']['image_over_title'];
                }

             if($_POST['ImagesView']['image_top_title'] != ''){
                $model->image_top_title = $_POST['ImagesView']['image_top_title'];
                }

             if($_POST['ImagesView']['image_sub_title'] != ''){
                $model->image_sub_title = $_POST['ImagesView']['image_sub_title'];
                }


            if($_FILES['ImagesView']['error']['image_file_location']==0)
            {
                    $rand = rand(0, 99); // random number generation for unique image save
                    $model->file = UploadedFile::getInstance($model, 'image_file_location');
                    $image_name = 'uploads/pages_images/' . $model->file->basename .'_kslta_'. $rand . "." . $model->file->extension;
                    $model->file->saveAs($image_name);

                    $model->image_file_name = $model->file->basename .'_kslta_'. $rand . "." . $model->file->extension;
                    $model->image_type = $model->file->extension;

                    $model->image_file_location = $image_name;
            }

            if($model->save()){
                 Yii::$app->getSession()->setFlash('success', 'Image has been added success.');
                 return $this->redirect(['index']);
            }else{
                 Yii::$app->getSession()->setFlash('error', 'Something went wrong !');
                 return $this->redirect(['index']);
            }
           
        } 

         return $this->renderAjax('update', [
                'model' => $model,
            ]);

    }

    /**
     * Deletes an existing ImagesView model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ImagesView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return ImagesView the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ImagesView::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionMultipledelete()
    {
        
        $pk = Yii::$app->request->post('row_id');

        foreach ($pk as $key => $value) 
        {
            $sql = "DELETE FROM ksl_slider_banner WHERE banner_id = $value";
            $query = Yii::$app->db->createCommand($sql)->execute();
        }

        return $this->redirect(['index']);

    }
}
