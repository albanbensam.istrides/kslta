<?php

namespace backend\controllers;

use Yii;
use backend\models\PageContents;
use backend\models\PageContentsSearch;
use backend\models\KslGalleryModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
/**
 * PageContentsController implements the CRUD actions for PageContents model.
 */
class PageContentsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
          'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],

                // ...
            ],
        ],


        ];
    }

     public function beforeAction($action) {
    $this->enableCsrfValidation = false;
    return parent::beforeAction($action);
}

    /**
     * Lists all PageContents models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PageContentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PageContents model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PageContents model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PageContents();
        
        if ($model->load(Yii::$app->request->post())) {

           
            $model->page_content_top_main_heading = $_POST['PageContents']['page_content_top_main_heading'];
            
            $model->page_content_top_sub_heading = $_POST['PageContents']['page_content_top_sub_heading'];

            $model->page_meta_name = $_POST['PageContents']['page_meta_name'];
        $model->page_content_heading = $_POST['PageContents']['page_content_heading'];
            $model->page_assign_sidebar = $_POST['PageContents']['page_assign_sidebar'];
             $model->remove_content_img = $_POST['PageContents']['remove_content_img'];

            $model->page_meta_content = $_POST['PageContents']['page_meta_content'];
 			if($_POST['PageContents']['page_main_content'] != ''){
                    $model->page_main_content = $_POST['PageContents']['page_main_content'];
                }
            //Below condition is for whether the file is available or empty
                if ($_FILES['PageContents']['error']['page_content_img'] == 0) {
                    $rand = date("His"); // random number generation for unique image save
                    $model->file = UploadedFile::getInstance($model, 'page_content_img');
                    $image_name = 'uploads/pageImg/'.'ksltapage'.$rand.".".$model->file->extension;
                    $model->file->saveAs($image_name);
                    $model->page_content_img = $image_name;
                }
                

                if($_POST['PageContents']['remove_content_img'] ==1){
                 $model->page_content_portfolio_img='';

                }
                else{
      if ($_FILES['PageContents']['error']['page_content_portfolio_img'] == 0) {
                    $rand = date("His"); // random number generation for unique image save
                    $model->file = UploadedFile::getInstance($model, 'page_content_portfolio_img');
                    $image_name = 'uploads/pageImg/'.'ksltapage'.$rand.".".$model->file->extension;
                    $model->file->saveAs($image_name);
                    $model->page_content_portfolio_img = $image_name;
                }

                } 
                  

           if ($model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Page has been added.');
            return $this->redirect(['index']);
           }else{
                Yii::$app->getSession()->setFlash('error', 'Something went wrong !');
                return $this->redirect(['index']);
            }            
        } 


        return $this->render('create', [
                'model' => $model,
            ]);
    }

    /**
     * Updates an existing PageContents model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    { 
        $model = $this->findModel($id); 
         if ($model->load(Yii::$app->request->post())) {
         	//echo"<pre>";  print_r($_POST); die;
            //Below condition is for whether the file is available or empty
               //print_r($_FILES);
                if($_POST['PageContents']['page_meta_name'] != ''){
                    $model->page_meta_name = $_POST['PageContents']['page_meta_name'];
                }

                if($_POST['PageContents']['page_meta_content'] != ''){
                    $model->page_meta_content = $_POST['PageContents']['page_meta_content'];
                }
 $model->page_content_heading = $_POST['PageContents']['page_content_heading'];
						 $model->page_main_content = $_POST['PageContents']['page_main_content'];
                          $model->remove_content_img = $_POST['PageContents']['remove_content_img'];
               
$model->page_assign_sidebar = $_POST['PageContents']['page_assign_sidebar'];

                if ($_FILES['PageContents']['error']['page_content_img'] == 0) {
                    $rand = date("His"); // random number generation for unique image save
                    $model->file = UploadedFile::getInstance($model, 'page_content_img');
                    $image_name = 'uploads/pageImg/'.'ksltapage'.$rand.".".$model->file->extension;
                    $model->file->saveAs($image_name);
                    $model->page_content_img = $image_name;
                }
                  if($_POST['PageContents']['remove_content_img'] ==1){
                 $model->page_content_portfolio_img='';

                }
                else{
      if ($_FILES['PageContents']['error']['page_content_portfolio_img'] == 0) {
                    $rand = date("His"); // random number generation for unique image save
                    $model->file = UploadedFile::getInstance($model, 'page_content_portfolio_img');
                    $image_name = 'uploads/pageImg/'.'ksltapage'.$rand.".".$model->file->extension;
                    $model->file->saveAs($image_name);
                    $model->page_content_portfolio_img = $image_name;
                }

                } 
 
           if ($model->save()) {
            Yii::$app->getSession()->setFlash('success', $model->page_name.' Page has been Updated.');
            return $this->redirect(['index']);
           }else{
                Yii::$app->getSession()->setFlash('error', 'Something went wrong !');
                return $this->redirect(['index']);
            }           
        } 


        return $this->render('update', [
                'model' => $model,
            ]);
    }

    /**
     * Deletes an existing PageContents model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PageContents model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PageContents the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PageContents::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /**
     * Gallery manager an existing PageContents model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionGallerymanager($id)
    {

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            if($_POST['PageContents']['gallery_id'] != ''){
                $model->gallery_id = $_POST['PageContents']['gallery_id'];
                if( $model->save()){
                Yii::$app->getSession()->setFlash('success', 'Gallery has been added.');
                return $this->redirect(['index']);
               }
            }else{
                 Yii::$app->getSession()->setFlash('error', 'Something went wrong.');
                 return $this->redirect(['index']);
            }
        } 

        return $this->renderAjax('gallerymanager', [
                'model' => $model,
            ]);
    }
}
