<?php

namespace backend\controllers;

use Yii;
use backend\models\KslNewsTbl;
use backend\models\KslNewsTblSearch;
use backend\models\KslTournamentsTbl;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Url;
/**
 * KslNewsTblController implements the CRUD actions for KslNewsTbl model.
 */
class KslNewsTblController extends Controller {

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
          'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],

                // ...
            ],
        ],


        ];
    }

    /* Avoid Bad Requrest 400 Error  */
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    /**
     * Lists all KslNewsTbl models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new KslNewsTblSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KslNewsTbl model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new KslNewsTbl model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new KslNewsTbl();
       $tournament_id='';
       $tourn_name = '';
      

       if(isset($_GET['tid'])){
		 	$tournament_id=$_GET['tid'];
            $tournModel = KslTournamentsTbl::find()->where(['tourn_id' => $tournament_id])->one();
            $tourn_name = $tournModel->tourn_title;
		}
		
		
        if ($model->load(Yii::$app->request->post())) {
			//var_dump($_POST['news_meta_name']);
             $model->news_editor_content = $_POST['news_editor_content'];
             $model->news_link_status = "U";
             $model->news_located_city = $_POST['KslNewsTbl']['news_located_city'];
             $model->news_located_country = $_POST['KslNewsTbl']['news_located_country'];
             $model->news_tournament_id = isset($_POST['KslNewsTbl']['news_tournament_id']) ? $_POST['KslNewsTbl']['news_tournament_id'] : '';
			// $model->news_meta_name = $_POST['news_meta_name'];
			// $model->news_meta_content = $_POST['news_meta_content'];
            if (Yii::$app->request->isPost) {
                //Below condition is for whether the file is available or empty
                if (isset($_FILES['KslNewsTbl']['name']['news_content_image']) && !empty($_FILES['KslNewsTbl']['name']['news_content_image'])) {
                    $rand = rand(0, 99999); // random number generation for unique image save
                    $model->scenario = 'imageUploaded';
                    $model->file = UploadedFile::getInstance($model, 'news_content_image');
                    $image_name = 'uploadimage/' . $model->file->basename . $rand . "." . $model->file->extension;
                    $model->file->saveAs($image_name);
                    $model->news_content_image = $image_name;
                }

                if ($model->save()) {                    
					if($_POST['KslNewsTbl']['news_tournament_id']!=""){						;
						Yii::$app->getSession()->setFlash('success', 'Tournament News has been added success.');
						return $this->redirect(Url::to(['ksl-news-tbl/newsunapproved']));
					}else{
						Yii::$app->getSession()->setFlash('success', 'News has been added success.');
                    	return $this->redirect(['index']);
					}
                } else {die;
                    Yii::$app->getSession()->setFlash('error', 'Something went wrong !');
                }
//                if ($model->upload()) {
//                    return;
//                }
            }
        } else {
        	$model->news_tournament_id=$tournament_id;
            $model->dummy_tourn_title = $tourn_name;
            return $this->render('create', [
                        'model' => $model,

            ]);
        }
    }

    /**
     * Updates an existing KslNewsTbl model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if (isset($_REQUEST['news_editor_content'])) {
            $model->news_editor_content = $_REQUEST['news_editor_content'];
        }
        if (isset($_REQUEST['KslNewsTbl']['news_located_country'])) {
            $model->news_located_country = $_REQUEST['KslNewsTbl']['news_located_country'];
        }
         if (isset($_REQUEST['KslNewsTbl']['news_located_city'])) {
            $model->news_located_city = $_REQUEST['KslNewsTbl']['news_located_city'];
        }

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isPost) {
                //Below condition is for whether the file is available or empty

                if ($_FILES['KslNewsTbl']['name']['news_content_image'] != '') {
                    $rand = rand(0, 99999); // random number generation for unique image save
                    //$model->scenario = 'imageUploaded';
                    $model->file = UploadedFile::getInstance($model, 'news_content_image');
                         $image_name = 'uploadimage/' . $model->file->basename . $rand . "." . $model->file->extension;
                         $model->file->saveAs($image_name);
                         $model->news_content_image = $image_name;
                }
                if ($model->save()) {
                    Yii::$app->getSession()->setFlash('success', 'News has been Updated success.');
                    return $this->redirect(['index']);
                } else {
                    Yii::$app->getSession()->setFlash('error', 'Something went wrong !');
                }
            }
            // return $this->redirect(['view', 'id' => $model->news_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing KslNewsTbl model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the KslNewsTbl model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KslNewsTbl the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = KslNewsTbl::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
   
    public function actionNewsunapproved() {
        $searchModel = new KslNewsTblSearch();
        $dataProvider = $searchModel->unapprovedsearch(Yii::$app->request->queryParams);

        return $this->render('unapproved', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionNewsrecjected() {
        $searchModel = new KslNewsTblSearch();
        $dataProvider = $searchModel->rejectsearch(Yii::$app->request->queryParams);

        return $this->render('rejected', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }
    
     public function actionTournamentnewslisting($id) {

        $searchModel = new KslNewsTblSearch();
        
       
        $dataProvider = $searchModel->tournamentsearch(Yii::$app->request->queryParams);
        $searchModel->news_tournament_id = $id;
        return $this->render('tournnews', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    
        ]);
    }

    /* This below action for to change news status */
    public function actionActivatenews() {
        if (isset($_REQUEST['id'])) {
            
            $news_model = $this->findModel($_REQUEST['id']);
            $news_model->news_located_city = $news_model->news_located_city;
            if ($news_model->news_link_status == 'A') {
                $news_model->news_link_status = 'R';
            } elseif($news_model->news_link_status == 'U'){
            	$news_model->news_link_status = 'A';
            }elseif($news_model->news_link_status == 'R')
             $news_model->news_link_status = 'U';
            
            if ($news_model->save()) {
                echo 'S';
            }
            else{
                echo '<pre>';
                print_r($news_model->getErrors());
                exit();
                
            }
        }
    }

}
