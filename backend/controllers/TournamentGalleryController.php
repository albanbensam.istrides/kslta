<?php

namespace backend\controllers;

use Yii;
use backend\models\TournamentGallery;
use backend\models\TournamentGallerySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * TournamentGalleryController implements the CRUD actions for TournamentGallery model.
 */
class TournamentGalleryController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
          'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],

                // ...
            ],
        ],


        ];
    }

     public function beforeAction($action) {
    $this->enableCsrfValidation = false;
    return parent::beforeAction($action);
}

    /**
     * Lists all TournamentGallery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TournamentGallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TournamentGallery model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TournamentGallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new TournamentGallery();
        $tourn_title = '';
        $tourn_iddata = 0;
        $galleryArray = array();
        $galleryMultipleData = array();


        if(isset($_GET['name'])){
            $tourn_title = $_GET['name'];
            $tourn_iddata = $_GET['id'];
        }


        if (isset($_POST['TournamentGallery'])) {
        	if($_POST['TournamentGallery']['tourn_id']){
				$model_del = TournamentGallery::deleteAll(['tourn_id'=>$_POST['TournamentGallery']['tourn_id']]);
			}
            $galleryArray = $_POST['TournamentGallery']['gallery_id'];			
            foreach ($galleryArray as $value) {
                $model = new TournamentGallery();
                $model->tourn_id = $_POST['TournamentGallery']['tourn_id'];
                $model->gallery_id =  $value;
                $model->save();
            }
            Yii::$app->getSession()->setFlash('success', 'Tournament Gallery List has been added.');
            return $this->redirect(Url::to(['ksl-tournaments-tbl/index']));
            
        } else {
            $model->tournTitle=$tourn_title;
            $model->tourn_id = $tourn_iddata;
            return $this->renderAjax('_form', [
                'model' => $model,
               
            ]);
        }
    }

    /**
     * Updates an existing TournamentGallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->tourn_gallery_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TournamentGallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TournamentGallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TournamentGallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TournamentGallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
