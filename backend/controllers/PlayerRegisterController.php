<?php

namespace backend\controllers;

use Yii;
use backend\models\PlayerRegister;
use backend\models\PlayerRegisterSearch;
use yii\web\Controller;
use yii\filters\AccessControl;

use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\TournamentEventModel;
use yii\helpers\ArrayHelper;
use backend\models\DropdownManagement;
use backend\models\PlayerEvents;
/**
 * PlayerRegisterController implements the CRUD actions for PlayerRegister model.
 */
class PlayerRegisterController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
          'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],

                // ...
            ],
        ],


        ];
    }


     public function beforeAction($action) {
    $this->enableCsrfValidation = false;
    return parent::beforeAction($action);
}

    /**
     * Lists all PlayerRegister models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PlayerRegisterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PlayerRegister model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PlayerRegister model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PlayerRegister();
		if ($model->load(Yii::$app->request->post())){
        $myStr = rand(0, 9999);
		$playerregister_post=Yii::$app->request->post('PlayerRegister');		
        if(isset($playerregister_post['reg_player_id'])){
            $model->reg_player_id =  $playerregister_post['reg_player_id'];
        }
        else{
            $model->reg_player_id = date("dmy").$myStr ; //random registeration id generation
        }	
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        		//echo $model->reg_id;die;
        		if(count($playerregister_post['tourn_event_id'])>0){
					foreach($playerregister_post['tourn_event_id'] as $one_event){
								if($one_event!=""){
								$event_dbdata[]=['ue_userid'=> $model->reg_id,'ue_tournamentid'=>$playerregister_post['tourn_id'],'ue_eventid'=>$one_event];
								}
							}	
							$modelplayerevent = new PlayerEvents();		
							$modelplayerevent->inset_playerevent_data($event_dbdata);
				}
            Yii::$app->getSession()->setFlash('success', 'Player details has been created successfully.');
            return $this->redirect(['index']);
        } 
        }else {
            //Yii::$app->getSession()->setFlash('error', 'Something went wrong !');
            
        }

        
        return $this->render('create', [
                'model' => $model,
            ]);
    }

    /**
     * Updates an existing PlayerRegister model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);        
//print_r(Yii::$app->request->post());
     /*   $myStr = rand(0, 9999);		
        if(isset($_REQUEST['PlayerRegister']['reg_player_id'])){
           /$model->reg_player_id = $_REQUEST['PlayerRegister']['reg_player_id'];
        }
        else{
            $model->reg_player_id = date("dmy").$myStr ; //random registeration id generation
        }*/
$all_event_post=$_REQUEST['PlayerRegister']['tourn_event_id'];
$tourn_id=$_REQUEST['PlayerRegister']['tourn_id'];
        	if(count($all_event_post)>0){
        		$selected_event = ArrayHelper::map(PlayerEvents::find()->where(['ue_tournamentid' => $model->tourn_id,'ue_userid'=>$model->reg_id])->all(), 'ue_autoid', 'ue_eventid');
				$all_db_event=array();
				foreach($all_event_post as $one_event){		
								if($one_event!=""){								
									if(!in_array($one_event, $selected_event)){
										$event_dbdata[]=['ue_userid'=> $model->reg_id,'ue_tournamentid'=>$_REQUEST['PlayerRegister']['tourn_id'],'ue_eventid'=>$one_event];										
									}else{
										$all_db_event[]=$one_event;
									}
								}
							}							
							$modelplayerevent = new PlayerEvents();		
							$modelplayerevent->inset_playerevent_data($event_dbdata);
							foreach($selected_event as $onedb_key=>$one_dbevent){
								if(!in_array($one_dbevent, $all_db_event)){
									 $modelplayerevent->delete_playerevent_data($onedb_key);
								}
							}							
				}
			if(Yii::$app->request->post('PlayerRegister')['reg_player_id']!=""){
				$model->reg_player_id =  Yii::$app->request->post('PlayerRegister')['reg_player_id'];
			}
				
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Player details has been updated successfully.');
            return $this->redirect(['index']);
        } else {
           Yii::$app->getSession()->setFlash('error', 'Something went wrong !');
        }

        return $this->render('update', [
                'model' => $model,
            ]);
    }

    /**
     * Deletes an existing PlayerRegister model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PlayerRegister model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PlayerRegister the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PlayerRegister::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
public function actionEventvalue($id){
	$rows = TournamentEventModel::find()->where(['tourn_id' => $id])->all(); 
        //echo '<option>Select Event</option>'; 
        if(count($rows)>0){
        	$id_db_all=array();
			$id_db_cat=array();
        	foreach($rows as $one_event){
				$id_db_all[]=$one_event->event_category;
				$id_db_all[]=$one_event->event_subcategory;
				$id_db_cat[$one_event->event_category][$one_event->event_id]=$one_event->event_subcategory;																									
			}
			if(count($id_db_all)>0){
				$event_names = ArrayHelper::map(DropdownManagement::find()->FilterWhere(array('in', 'dropdown_id', $id_db_all))->all(), 'dropdown_id', 'dropdown_value');
			}
			foreach($id_db_cat as $one_categoryid=>$one_subcategory){
				$one_category_txt='';
				if(isset($event_names[$one_categoryid])){
					$one_category_txt=$event_names[$one_categoryid];
				}
				echo '<optgroup label="'.$one_category_txt.'">';
						foreach($one_subcategory as $event_id=>$one_event){
						$one_subcategory_txt='';
						if(isset($event_names[$one_event])){
							$one_subcategory_txt=$event_names[$one_event];
						}
		                echo '<option value="'.$event_id.'">'.$one_category_txt.' '.$one_subcategory_txt.'</option>';
		            }
				echo '</optgroup>';
			}			
        }
 
    }
}
