<?php

namespace backend\controllers;

use Yii;
use backend\models\MainMenu;
use backend\models\MainMenuSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * MainMenuController implements the CRUD actions for MainMenu model.
 */
class MainMenuController extends Controller
{
   public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
          'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],

                // ...
            ],
        ],


        ];
    }


     /* Avoid Bad Requrest 400 Error  */
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Lists all MainMenu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MainMenuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MainMenu model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

   /**
     * Creates a new SubMenu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MainMenu();

        if ($model->load(Yii::$app->request->post())) {

            if($model->save()){
                 Yii::$app->getSession()->setFlash('success', 'Sub Menu has been added success.');
                 return $this->redirect(['index']);
            }else{
                 Yii::$app->getSession()->setFlash('error', 'Something went wrong !');
                 return $this->redirect(['index']);
            }
           
        } 

         return $this->render('create', [
                'model' => $model,
            ]);
    }

    /**
     * Updates an existing SubMenu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

         if ($model->load(Yii::$app->request->post())) {

            if($model->save()){
                 Yii::$app->getSession()->setFlash('success', $model->menu_name.' Sub Menu has been updated.');
                 return $this->redirect(['index']);
            }else{
                 Yii::$app->getSession()->setFlash('error', 'Something went wrong !');
                 return $this->redirect(['index']);
            }
           
        } 

         return $this->render('update', [
                'model' => $model,
            ]);
    }

    /**
     * Deletes an existing MainMenu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MainMenu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return MainMenu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MainMenu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
