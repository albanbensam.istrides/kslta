<?php

namespace backend\controllers;

use Yii;
use backend\models\KslTournamentsTbl;
use backend\models\KslTournamentsTblSearch;
use backend\models\TournamentEventModel;
use backend\models\KslGalleryModel;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * KslTournamentsTblController implements the CRUD actions for KslTournamentsTbl model.
 */
class KslTournamentsTblController extends Controller {

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
          'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],

                // ...
            ],
        ],


        ];
    }


    public function actionIndex() {
        $searchModel = new KslTournamentsTblSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$model = new KslTournamentsTbl();;
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model, 
        ]);
    }


     /**
     * Lists all KslTournamentsTbl models.
     * @return mixed
     */
    public function actionTournamentnews() {
        $searchModel = new KslTournamentsTblSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new KslTournamentsTbl();;
        return $this->render('tournnewsindex', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model, 
        ]);
    }

    /**
     * Displays a single KslTournamentsTbl model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new KslTournamentsTbl model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new KslTournamentsTbl();
       
        if (isset($_REQUEST['KslTournamentsTbl'])) {
		$modelevent = new TournamentEventModel();

        if(isset($_POST['KslTournamentsTbl']['tourn_price_money'])){
            $model->tourn_price_money = $_POST['KslTournamentsTbl']['tourn_price_money'];
        }
            if ($model->load(Yii::$app->request->post())) {
                //Below condition is for whether the file is available or empty
                if (isset($_FILES['KslTournamentsTbl']['name']['tourn_tournament_image']) && !empty($_FILES['KslTournamentsTbl']['name']['tourn_tournament_image'])) {
                    $rand = rand(0, 9999); // random number generation for unique image save
                   // $model->scenario = 'imageUploaded';
                    $model->file = UploadedFile::getInstance($model, 'tourn_tournament_image');
                    $image_name = 'uploads/tournamemnt_img/'.'ksltatournament'.$rand.".".$model->file->extension;
                    $model->file->saveAs($image_name);
                    $model->tourn_tournament_image = $image_name;
                }

                $model->save();


            	$event_post_id=Yii::$app->request->post('event_all_key');
				$event_post_id=rtrim($event_post_id,"~");
				$event_id_array=explode('~',$event_post_id);
				$event_dbdata=array();
				if(count($event_id_array)>0){
					foreach($event_id_array as $one_event){
						if($one_event!=""){
						$ts_event=explode('_',$one_event);
						$event_dbdata[]=['tourn_id'=>$model->tourn_id,'event_category'=>$ts_event[0],'event_subcategory'=>$ts_event[1]];
						}
					}			
					$modelevent->upload_event_data($event_dbdata);
				}
						/*$modelevent->tourn_id=$model->id;
				$modelevent->event_category='2';
				$modelevent->event_subcategory='3';
				$modelevent->save();
				die;*/
                //return $this->redirect(['view', 'id' => $model->tourn_id]);
                Yii::$app->getSession()->setFlash('success', 'Torunament detail has been added success.');
                return $this->redirect(['index']);
            } else {
                Yii::$app->getSession()->setFlash('error', 'Something went wrong !');
            }
        }
        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing KslTournamentsTbl model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if(isset($_POST['KslTournamentsTbl']['tourn_price_money'])){
            $model->tourn_price_money = $_POST['KslTournamentsTbl']['tourn_price_money'];
        }
        
        //Below condition is for whether the file is available or empty
                if ($_FILES['KslTournamentsTbl']['name']['tourn_tournament_image'] != '') {
                    $rand = rand(0, 9999); // random number generation for unique image save
                   // $model->scenario = 'imageUploaded';
                    $model->file = UploadedFile::getInstance($model, 'tourn_tournament_image');
                    $image_name = 'uploads/tournamemnt_img/'.'ksltatournament'.$rand.".".$model->file->extension;
                    $model->file->saveAs($image_name);
                    $model->tourn_tournament_image = $image_name;
                }

        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        	$modelevent = new TournamentEventModel();
        	$event_post_id=Yii::$app->request->post('event_all_key');
				$event_post_id=rtrim($event_post_id,"~");
				$event_id_array=explode('~',$event_post_id);
				$event_dbdata=array();
				if(count($event_id_array)>0){
					foreach($event_id_array as $one_event){
						if($one_event!=""){
						$ts_event=explode('_',$one_event);
						$event_dbdata[]=['tourn_id'=>$id,'event_category'=>$ts_event[0],'event_subcategory'=>$ts_event[1]];
						}
					}
					$modelevent->update_event_data($id,$event_dbdata);
				}
            Yii::$app->getSession()->setFlash('success', 'Torunament detail Updated.');
            return $this->redirect(['index']);
            //return $this->redirect(['view', 'id' => $model->tourn_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing KslTournamentsTbl model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the KslTournamentsTbl model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KslTournamentsTbl the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = KslTournamentsTbl::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
         return parent::beforeAction($action);
    }


    /**
     *Factsheet Upload
     */
    public function actionFactsheetupload()
    {
        $this->enableCsrfValidation = false;
       
        if(isset($_GET['id'])){
            $id = $_GET['id'];
            $model = $this->findModel($id);
            //Below condition is for whether the file is available or empty
                if ($_FILES['KslTournamentsTbl']['error']['tourn_factsheet']==0 && count($_FILES)>0) {
                    $rand = rand(0, 99999); 
                    $model->file = UploadedFile::getInstance($model, 'tourn_factsheet');
 
                    $data_name = 'uploads/factsheets/' . $model->file->basename . $rand . "." . $model->file->extension;
                  
                    $model->file->saveAs($data_name);
                    $model->tourn_factsheet = $data_name;

                    $model->tourn_factsheet_uploaded_at = date("Y-m-d H:i:s");
                    $model->tourn_facesheet_status = 'A';
                    if($model->save()){
                 Yii::$app->getSession()->setFlash('success', 'Facsheet has been Uploaded.');
                return $this->redirect(['index']);
                    }else{
                         echo '<pre>';
                         print_r($model->getErrors());
                         exit();
                         Yii::$app->getSession()->setFlash('error', 'Something went wrong!');
                       die; return $this->redirect(['index']);
                    }
                }
                elseif($_FILES['KslTournamentsTbl']['error']['tourn_factsheet']==""){
                     return $this->render('_facesheetform', [
                        'model' => $model,
                     ]);
                }
            
        }
    }

    public function actionVisibleoperation(){
        $this->enableCsrfValidation = false;
        if(isset($_GET['id'])){
            $tourn_model = $this->findModel($_GET['id']);
            if ($tourn_model->tourn_facesheet_status == 'A') {
                $tourn_model->tourn_factsheet = "";
                $tourn_model->tourn_facesheet_status = 'I';
            }else{
                $tourn_model->tourn_facesheet_status = 'A';
            }   
            

            if ($tourn_model->save()) {

                Yii::$app->getSession()->setFlash('success', 'Facsheet has been Deleted.');
                return $this->redirect(['index']);
                echo "S";
            }
            else{
                echo '<pre>';
                print_r($tourn_model->getErrors());
                exit();
                
            }
        }
    }

    //Download File Fuctionality
     public function actionDownloadfile(){
       // ini_set('max_execution_time', 5*60); // 5 minutes
        $name= $_GET['file'];
       // $myPath = $_SERVER['SERVER_NAME'].$name;
        if (file_exists($name)) {
             return \Yii::$app->response->sendFile($name);
        }
    }

public function actionGalleryselect(){
	
	 $gallery_details=ArrayHelper::toArray(KslGalleryModel::find()->all());
			echo '<option value="">Select</option>';
		  foreach ($gallery_details as $key => $value) {
			echo '<option value="'.$value['gallery_id'].'">'.$value['gallery_title'].'</option>';
		  }
		
	}
public function actionGalleryselectsave(){
	if(Yii::$app->request->post('tournament_id')!=''){
		$tournament_id=Yii::$app->request->post('tournament_id');
		}
		$gallery_id='';
		if(Yii::$app->request->post('gallery_id')!=''){
			$gallery_id=Yii::$app->request->post('gallery_id');
		}
		if($tournament_id!="" && $gallery_id!=""){
			$model_tournament = KslTournamentsTbl::find()->where(['tourn_id'=>$tournament_id])->one();				
		  	$model_tournament->tourn_gallery_id=$gallery_id;
			$model_tournament->save();		
		}	
	}

}
