<?php

namespace backend\controllers;

use Yii;
use backend\models\Tournament;
use backend\models\TournamentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\KslTournamentsTbl;

/**
 * TournamentController implements the CRUD actions for Tournament model.
 */
class TournamentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tournament models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TournamentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tournament model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tournament model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tournament();
		//echo "<pre>";print_r($_POST); die;
        if ($model->load(Yii::$app->request->post())) { 
				 
				$app= $_POST['Tournament']['tour_name'];
				// echo "<pre>";print_r($app); die;
				 
				$model->tour_name = $app;
			$model->tour_url= $_POST['Tournament']['tour_url'];
			$model->order_no= $_POST['Tournament']['order_no'];
			
        	$model->created_at = date('Y-m-d H:m:s');
			//echo "<pre>";print_r($model); die;
			 if($model->save()){
			 	
				return $this->redirect(['index']);
			 } 
            
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Tournament model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) { 
				 
				$app= $_POST['Tournament']['tour_name']; 
				 
				$model->tour_name = $app;
			$model->tour_url= $_POST['Tournament']['tour_url'];
			$model->order_no= $_POST['Tournament']['order_no'];
			
        	$model->created_at = date('Y-m-d H:m:s'); 
			 if($model->save()){
			 	
				return $this->redirect(['index']);
			 } 
            
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Tournament model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
 public function actionLists()
    {
    		if(isset($_GET['id'])){
    		//	echo $_GET['id']; 
				//use backend\models\KslTournamentsTbl;
				 $dataList=KslTournamentsTbl::find()->where(['tourn_title'=>$_GET['id']])->one();
				 
				 return $dataList->tourn_id;
    		}
    	
     
			 	
			 }
    /**
     * Finds the Tournament model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Tournament the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tournament::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
