<?php

namespace backend\controllers;

use Yii;
use backend\models\KslPlayerRegisterV1;
use backend\models\KslPlayerRegisterV1Search;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KslPlayerRegisterV1Controller implements the CRUD actions for KslPlayerRegisterV1 model.
 */
class KslPlayerRegisterV1Controller extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all KslPlayerRegisterV1 models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KslPlayerRegisterV1Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KslPlayerRegisterV1 model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Displays a single KslCoachesReg model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewdata($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Updates an existing KslCoachesReg model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdatestatus($id,$status)
    {
        $model = $this->findModel($id);
        $model->active_status = $status;
        $model->save();
        return $this->redirect(['index']);
    }

    /**
     * Creates a new KslPlayerRegisterV1 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KslPlayerRegisterV1();

        if ($model->load(Yii::$app->request->post())) 
        {
            foreach (Yii::$app->request->post('KslPlayerRegisterV1') as $key => $value) {
                $model->$key = $value;    
            }
            $model->created_at = date('Y-m-d H:i:s');
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } 
        else 
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing KslPlayerRegisterV1 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) 
        {
            foreach (Yii::$app->request->post('KslPlayerRegisterV1') as $key => $value) {
                $model->$key = $value;    
            }
            $model->created_at = date('Y-m-d H:i:s');
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing KslPlayerRegisterV1 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the KslPlayerRegisterV1 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KslPlayerRegisterV1 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = KslPlayerRegisterV1::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
