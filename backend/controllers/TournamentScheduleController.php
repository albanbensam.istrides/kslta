<?php

namespace backend\controllers;

use Yii;
use backend\models\TournamentSchedule;
use backend\modelsTournamentScheduleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\TournamentEventModel;
use yii\helpers\ArrayHelper;
use backend\models\DropdownManagement;
use backend\models\PlayerEvents;
use backend\models\PlayerEventsSearch;
use backend\models\TournamentEventModelSearch;
use backend\models\PlayerRegister;
use backend\models\KslTournamentsTbl;
use backend\models\EventMatches;
use yii\helpers\Url;
use yii\web\UploadedFile;
use backend\models\Drawsheet;
use backend\models\TournamentsEventResult;
use backend\models\TournamentsEventMatche;
use yii\helpers\Html;
/**
 * TournamentScheduleController implements the CRUD actions for TournamentSchedule model.
 */
class TournamentScheduleController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
          'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],

                // ...
            ],
        ],


        ];
    }

public function beforeAction($action) {
    $this->enableCsrfValidation = false;
    return parent::beforeAction($action);
}
    /**
     * Lists all TournamentSchedule models.
     * @return mixed
     */
    public function actionIndex()
    {
 		$model = new TournamentSchedule();
		$searchModel = new TournamentEventModelSearch();
		$tournament_details=array();
		$tournament_id='';		
		if(isset(Yii::$app->request->post('TournamentSchedule')['ts_tournament_id'])){
			$tournament_id=Yii::$app->request->post('TournamentSchedule')['ts_tournament_id'];
		}elseif(Yii::$app->request->post('tournament_id')!=""){
			$tournament_id=Yii::$app->request->post('tournament_id');
		}elseif(isset($_GET['tournamentid'])){
			$tournament_id=$_GET['tournamentid'];
		}
		if($tournament_id!=""){
			$model->ts_tournament_id=$tournament_id;
			$searchModel->tourn_id=$tournament_id;			
			$tournament_details_db=KslTournamentsTbl::find()->where(['tourn_id'=>$model->ts_tournament_id])->one();
			$tournament_details=$tournament_details_db;		
		}else{
			$searchModel->tourn_id=0;
		}
	    $db_events_cs = TournamentEventModel::find()->where(['tourn_id' => $model->ts_tournament_id])->all();    
		$db_events_matches = EventMatches::find()->where(['em_tournament_id' => $model->ts_tournament_id])->all();		
		$id_db_cat="";
       if(count($db_events_cs)>0){
        	$id_db_all=array();
			$id_db_cat=array();
        	foreach($db_events_cs as $one_event){
				$id_db_all[]=$one_event->event_category;
				$id_db_all[]=$one_event->event_subcategory;
				$id_db_cat[$one_event->event_id]=$one_event->event_subcategory;																									
			}
			if(count($id_db_all)>0){
				$event_names = ArrayHelper::map(DropdownManagement::find()->FilterWhere(array('in', 'dropdown_id', $id_db_all))->all(), 'dropdown_id', 'dropdown_value');
			}
			
		foreach($db_events_cs as $one_event){
			$category_name="";
			$subcategory_name="";
			if(isset($event_names[$one_event->event_category])){
				$category_name=$event_names[$one_event->event_category];			
			}
			if(isset($event_names[$one_event->event_subcategory])){
				$subcategory_name=$event_names[$one_event->event_subcategory];			
			}
			$id_db_cat[$one_event->event_id]=$category_name." ".$subcategory_name;	
		}
		}else{
			$searchModel->tourn_id=0;
		}
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);			
		$event_names_all=$id_db_cat;
		$event_matches_no=array();
		foreach($db_events_matches as $one_matches){
			$event_matches_no[$one_matches->em_event_id]=$one_matches->em_no_of_matches;
		}	
		return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
            'event_names_all'=>$event_names_all,
            'tournament_details'=>$tournament_details,
            'event_matches_no'=>$event_matches_no,
        ]);
    }
public function actionDrawsheetupload(){
 	$model = new TournamentSchedule();
	$tournament_id='';
	if(isset(Yii::$app->request->post('TournamentSchedule')['drawsheet_tournament_id'])){
	 	$tournament_id=Yii::$app->request->post('TournamentSchedule')['drawsheet_tournament_id'];
	}
	$event_id='';
	if(isset(Yii::$app->request->post('TournamentSchedule')['drawsheet_eventid'])){
		$event_id=Yii::$app->request->post('TournamentSchedule')['drawsheet_eventid'];
	}
	if($tournament_id!="" && $event_id!=""){			
			 if (isset($_FILES['TournamentSchedule']['name']['event_drawsheet']) && !empty($_FILES['TournamentSchedule']['name']['event_drawsheet'])) {
                    $rand = rand(0, 99999); // random number generation for unique image save
                    //$model->scenario = 'factsheetdata';
                    $model->file = UploadedFile::getInstance($model, 'event_drawsheet');					
                    $data_path = 'uploads/drawsheets/' . $model->file->basename . $rand . "." . $model->file->extension;			
					$data_name = $model->file->basename . "." . $model->file->extension;			
                    $model->file->saveAs($data_path);
										
					$drawmodel=new Drawsheet();
					$drawmodel->draw_tournamentid=$tournament_id;
					$drawmodel->draw_eventid=$event_id;
                    $drawmodel->draw_filepath = $data_path;
					$drawmodel->draw_filename = $data_name;
                    $drawmodel->draw_datetime = date("Y-m-d H:i:s");					
                    if($drawmodel->save()){
                		 Yii::$app->getSession()->setFlash('success', 'Deaw Sheet has been Uploaded.');
              			 return $this->redirect(['index','tournamentid'=>$tournament_id]);
                    }else{
                         Yii::$app->getSession()->setFlash('error', 'Something went wrong!');
                         return $this->redirect(['index','tournamentid'=>$tournament_id]);
                    }
                }else{
                     return $this->render('_facesheetform', [
                        'model' => $model,
                     ]);
                }
		}
}
    /**
     * Displays a single TournamentSchedule model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TournamentSchedule model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TournamentSchedule();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ts_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TournamentSchedule model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ts_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TournamentSchedule model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TournamentSchedule model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TournamentSchedule the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TournamentSchedule::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionEventvalue($id){
	$rows = TournamentEventModel::find()->where(['tourn_id' => $id])->all(); 
        echo '<option>Select Event</option>'; 
        if(count($rows)>0){
        	$id_db_all=array();
			$id_db_cat=array();
        	foreach($rows as $one_event){
				$id_db_all[]=$one_event->event_category;
				$id_db_all[]=$one_event->event_subcategory;
				$id_db_cat[$one_event->event_category][$one_event->event_id]=$one_event->event_subcategory;																									
			}
			if(count($id_db_all)>0){
				$event_names = ArrayHelper::map(DropdownManagement::find()->FilterWhere(array('in', 'dropdown_id', $id_db_all))->all(), 'dropdown_id', 'dropdown_value');
			}
			foreach($id_db_cat as $one_categoryid=>$one_subcategory){
				$one_category_txt='';
				if(isset($event_names[$one_categoryid])){
					$one_category_txt=$event_names[$one_categoryid];
				}
				echo '<optgroup label="'.$one_category_txt.'">';
						foreach($one_subcategory as $event_id=>$one_event){
						$one_subcategory_txt='';
						if(isset($event_names[$one_event])){
							$one_subcategory_txt=$event_names[$one_event];
						}
		                echo '<option value="'.$event_id.'">'.$one_category_txt.' '.$one_subcategory_txt.'</option>';
		            }
				echo '</optgroup>';
			}			
        }
 
    }

public function actionNoofmatches(){
	$tournament_id='';
	if(Yii::$app->request->post('tournament_id')!=''){
		$tournament_id=Yii::$app->request->post('tournament_id');
	}
	$event_id='';
	if(Yii::$app->request->post('event_id')!=''){
		$event_id=Yii::$app->request->post('event_id');
	}	
	$no_of_matches='';
	if(Yii::$app->request->post('no_of_matches')!=''){
		$no_of_matches=Yii::$app->request->post('no_of_matches');
	}
	if($tournament_id!="" && $event_id!="" && $no_of_matches!=""){	 	  
		  $event_matches_db=ArrayHelper::map(EventMatches::find()->where(['em_tournament_id'=>$tournament_id,'em_event_id'=>$event_id])->all(), 'em_tournament_id', 'em_event_id');
		  if(count($event_matches_db)>0){
			$model_matches = EventMatches::find()->where(['em_tournament_id'=>$tournament_id,'em_event_id'=>$event_id])->one();	
			/*$model_matches->em_tournament_id=$tournament_id;
		 	$model_matches->em_event_id=$event_id;*/
		  	$model_matches->em_no_of_matches=$no_of_matches;
			$model_matches->save();		  
		  }else{
		  	$model_matches = new EventMatches();
		  	$model_matches->em_tournament_id=$tournament_id;
		 	$model_matches->em_event_id=$event_id;
		  	$model_matches->em_no_of_matches=$no_of_matches;
			$model_matches->save();
		  }
		  	return "Success";
		}else{
				return "Error";
	}
	}
	public function actionTournamentschedule(){
		if(Yii::$app->request->post('tournament_id')!=''){
		$tournament_id=Yii::$app->request->post('tournament_id');
		}
		$event_id='';
		if(Yii::$app->request->post('event_id')!=''){
			$event_id=Yii::$app->request->post('event_id');
		}	
		if($tournament_id!="" && $event_id!=""){
			$event_result_db=ArrayHelper::map(TournamentSchedule::find()->where(['ts_tournament_id'=>$tournament_id,'ts_event_id'=>$event_id])->all(),'ts_tournament_id','ts_tournament_result');
			if($event_result_db[$tournament_id]!=""){
				$returnjson_data=$event_result_db[$tournament_id];
			}else{
				$event_matches=EventMatches::find()->where(['em_tournament_id'=>$tournament_id,'em_event_id'=>$event_id])->one();
				$no_event_matches=$event_matches->em_no_of_matches;
				$tournament_value=array();
				for($ct=0;$ct<$no_event_matches;$ct++){
					$players_data=array();
					$test1['name']=" ";
					$test1['flag']=" ";
					$players_data[0]=$test1;
					$players_data[1]=$test1;
					$tournament_teams[$ct]=$players_data;
					$tournament_results[$ct]=$players_data;
				}
				$tournament_value['teams']=$tournament_teams;
				$tournament_value['results']=$tournament_results;
				$returnjson_data=json_encode($tournament_value);
			}
			//$returnjson_data='{"teams":[["Player 1","Player 4"],["Player 2","Player 3"],["Player 1","Player 2"],["Player 2","Player 3"],["Player 2","Player 4"],["Player 3","Player 3"],["1234567890","Player 4"],["1234567890","Player 3"]],"results":[[[[2,0],[1,0],[1,2],[1,2],[3,34],[3,4],[5,3],[5,4]],[[2,1],[1,3],[2,23],[45,545]],[[55,3],[45,655]],[[88,999],[null,null]]]]}';
			echo $returnjson_data;
		}	 
	}

	public function actionTournamentclear(){
		if(Yii::$app->request->post('tournament_id')!=''){
		$tournament_id=Yii::$app->request->post('tournament_id');
		}
		$event_id='';
		if(Yii::$app->request->post('event_id')!=''){
			$event_id=Yii::$app->request->post('event_id');
		}	
		if($tournament_id!="" && $event_id!=""){
			
			$event_result_db=ArrayHelper::map(TournamentSchedule::find()->where(['ts_tournament_id'=>$tournament_id,'ts_event_id'=>$event_id])->all(),'ts_tournament_id','ts_tournament_result');
			if($event_result_db[$tournament_id]!=""){
				$tournamentdetails = TournamentSchedule::find()->where(['ts_tournament_id'=>$tournament_id,'ts_event_id'=>$event_id])->all();
				foreach($tournamentdetails as $tournament) {
					$tournament->delete();
				}

				 
			
				$event_matches=EventMatches::find()->where(['em_tournament_id'=>$tournament_id,'em_event_id'=>$event_id])->one();
				$no_event_matches=$event_matches->em_no_of_matches;
				$tournament_value=array();
				for($ct=0;$ct<$no_event_matches;$ct++){
					$players_data=array();
					$test1['name']=" ";
					$test1['flag']=" ";
					$players_data[0]=$test1;
					$players_data[1]=$test1;
					$tournament_teams[$ct]=$players_data;
					$tournament_results[$ct]=$players_data;
				}
				$tournament_value['teams']=$tournament_teams;
				$tournament_value['results']=$tournament_results;
				$returnjson_data=json_encode($tournament_value);
			}
			//$returnjson_data='{"teams":[["Player 1","Player 4"],["Player 2","Player 3"],["Player 1","Player 2"],["Player 2","Player 3"],["Player 2","Player 4"],["Player 3","Player 3"],["1234567890","Player 4"],["1234567890","Player 3"]],"results":[[[[2,0],[1,0],[1,2],[1,2],[3,34],[3,4],[5,3],[5,4]],[[2,1],[1,3],[2,23],[45,545]],[[55,3],[45,655]],[[88,999],[null,null]]]]}';
			echo $returnjson_data;
		}	 
	}

	public function actionEventplayers(){
		if(Yii::$app->request->post('tournament_id')!=''){
		$tournament_id=Yii::$app->request->post('tournament_id');
		}
		$event_id='';
		if(Yii::$app->request->post('event_id')!=''){
			$event_id=Yii::$app->request->post('event_id');
		}	
		$player_data=array();
		if($tournament_id!="" && $event_id!=""){
			$query = new \yii\db\Query;
		    $query->select('*')
		            ->from('ksl_player_register g1')
		            ->leftJoin('ksl_player_events s1', 's1.ue_userid = g1.reg_id') 
					//->where(['s1.ue_tournamentid'=>$tournament_id,'s1.ue_eventid'=>$event_id])
					->groupby('g1.reg_id')
		            ->limit($Limit);
		    $command = $query->createCommand();
		    $resp = $command->queryAll();
			if(count($resp)>0){
				foreach($resp as $one_user){
					$player_data[]=$one_user['reg_first_name']." ".$one_user['reg_last_name']." (".$one_user['reg_player_id'].")";				
					//$one_user['reg_player_id'];
					//$one_user['reg_id'];
				}
			}		
		}
		echo json_encode($player_data);
 		//echo '["vi3", "Player 2", "Player 3", "Player 4"]';
	}
	
	public function actionEventresultsave(){
		//print_r(Yii::$app->request->post());
		if(Yii::$app->request->post('tournament_id')!=''){
		$tournament_id=Yii::$app->request->post('tournament_id');
		}
		$event_id='';
		if(Yii::$app->request->post('event_id')!=''){
			$event_id=Yii::$app->request->post('event_id');
		}	
		$tournament_result=array();
		if(count(Yii::$app->request->post('tournament_result'))>0){
			$tournament_result=Yii::$app->request->post('tournament_result');
		}
		//print_r($tournament_result);
		foreach($tournament_result['teams'] as $key_1=>$one_termset){
			foreach($one_termset as $key_2=>$one_player){
				//echo $one_player['name'].$one_player['flag']."<br>";
				$name_str=$tournament_result['teams'][$key_1][$key_2]['name'];
				if($name_str!=""){
				$name_str1=explode("(",$name_str);
					if($name_str1[0]!="" && $name_str1[1]!=""){
						$tournament_result['teams'][$key_1][$key_2]['name']=$name_str1[0];
						$tournament_result['teams'][$key_1][$key_2]['flag']='<i class="player_id_css">'.trim(str_replace(")", "", $name_str1[1]))." </i>";
					}
				}
			}
		}
		if($tournament_id!="" && $event_id!="" && count($tournament_result)>0){
			//echo json_encode($tournament_result);
		  $event_result_db=ArrayHelper::map(TournamentSchedule::find()->where(['ts_tournament_id'=>$tournament_id,'ts_event_id'=>$event_id])->all(),'ts_tournament_id', 'ts_event_id');
		  if(count($event_result_db)>0){
			$model_matches = TournamentSchedule::find()->where(['ts_tournament_id'=>$tournament_id,'ts_event_id'=>$event_id])->one();				
		  	$model_matches->ts_tournament_result=json_encode($tournament_result);
			$model_matches->save();		  
		  }else{
		  	$model_matches = new TournamentSchedule();
		  	$model_matches->ts_tournament_id=$tournament_id;
		 	$model_matches->ts_event_id=$event_id;
		  	$model_matches->ts_tournament_result=json_encode($tournament_result);
			$model_matches->save();
		  }
			
		}	
		echo $event_id;	
	}
public function actionDrawsheetview(){
	if(Yii::$app->request->post('tournament_id')!=''){
		$tournament_id=Yii::$app->request->post('tournament_id');
		}
		$event_id='';
		if(Yii::$app->request->post('event_id')!=''){
			$event_id=Yii::$app->request->post('event_id');
		}	
		$result_string='<tr><th>Sl. No.</th><th>Download</th><th>Date</th><th>Action</th></tr>';
		$result_string1='<tr><td>No results found.</td></tr>';
		if($tournament_id!="" && $event_id!=""){
			$drawmodel=new Drawsheet();
			$event_result_db=Drawsheet::find()->where(['draw_tournamentid'=>$tournament_id,'draw_eventid'=>$event_id])->all();
			$ih=1;			
			foreach($event_result_db as $one_draw_file){			
		    	$result_string2.='<tr><td>'.$ih++.'</td>
		                  <td><a data-pjax="0" aria-label="download" data-toggle="tooltip" title="" href="'.Url::to('@web/'.$one_draw_file->draw_filepath).'" class="btn btn-info btn-xs update" data-original-title="Download"><span class="fa fa-download"></span></a> '.$one_draw_file->draw_filename.'</td>
		                  <td>'.date("d/m/Y H:i", strtotime($one_draw_file->draw_datetime)).'</td>
		                  <td><a data-pjax="0" aria-label="delete_file" data-toggle="tooltip" title="" href="'.Url::to(['tournament-schedule/delete_drawsheet', 'drawfileid' => $one_draw_file->draw_id,'tournametid' => $one_draw_file->draw_tournamentid]).'" class="btn btn-danger btn-xs update" data-original-title="Delete"><span class="fa fa-trash"></span></a></td>
		                </tr>';
			}
			if($result_string2!=""){
				$result_string1=$result_string2;
			}
		}
		echo $result_string.$result_string1;
}
public function actionScorematchdetails(){
	if(Yii::$app->request->post('tournament_id')!=''){
		$tournament_id=Yii::$app->request->post('tournament_id');
		}
		$event_id='';
		if(Yii::$app->request->post('event_id')!=''){
			$event_id=Yii::$app->request->post('event_id');
		}	
		$edit_id_score=array();
		if(count(Yii::$app->request->post('edit_id_score'))!=''){
			$edit_id_score=Yii::$app->request->post('edit_id_score');
		}
		$set_value=array();
		if(count(Yii::$app->request->post('set_value'))!=''){
			$set_value=Yii::$app->request->post('set_value');
		}
		$player_reg_id=array();
		if(count(Yii::$app->request->post('player_reg_id'))!=''){
			$player_reg_id=Yii::$app->request->post('player_reg_id');			
		}
		if($tournament_id!="" && $event_id!="" && $edit_id_score!=""){					
			$event_result_db=ArrayHelper::toArray(TournamentsEventResult::find()->where(['ter_tournament_id'=>$tournament_id,'ter_event_id'=>$event_id,'ter_player_id'=>$edit_id_score])->all());
		 $totla_set_val=0;
		 if(count($event_result_db)>0){
			$model_matches = TournamentsEventResult::find()->where(['ter_tournament_id'=>$tournament_id,'ter_event_id'=>$event_id,'ter_player_id'=>$edit_id_score])->one();				
		  	$model_matches->ter_set=implode(',',$set_value);
			$totla_set_val=array_sum($set_value);
			$model_matches->ter_result=$totla_set_val;
			$model_matches->ter_player_reg_id=$player_reg_id;
			$model_matches->save();		  
		  }else{
		  	$model_matches = new TournamentsEventResult();
		  	$model_matches->ter_tournament_id=$tournament_id;
		 	$model_matches->ter_event_id=$event_id;
			$model_matches->ter_player_id=$edit_id_score;
			$model_matches->ter_set=implode(',',$set_value);
			$totla_set_val=array_sum($set_value);
			$model_matches->ter_result=$totla_set_val;
			$model_matches->ter_player_reg_id=$player_reg_id;
			$model_matches->save(); 	
		  }	
		  return $totla_set_val;		
		  	//return "Success";
		}else{
			//	return "Error";
		}
}
public function actionSetdetails(){
	if(Yii::$app->request->post('tournament_id')!=''){
		$tournament_id=Yii::$app->request->post('tournament_id');
		}
		$event_id='';
		if(Yii::$app->request->post('event_id')!=''){
			$event_id=Yii::$app->request->post('event_id');
		}	
		$edit_id_score=array();
		if(count(Yii::$app->request->post('edit_id_score'))!=''){
			$edit_id_score=Yii::$app->request->post('edit_id_score');
		}
		$set_value=array();
		if(count(Yii::$app->request->post('set_value'))!=''){
			$set_value=Yii::$app->request->post('set_value');
		}
		if($tournament_id!="" && $event_id!="" && $edit_id_score!=""){		
			$score_set='';	
		 $event_result_db=ArrayHelper::toArray(TournamentsEventResult::find()->where(['ter_tournament_id'=>$tournament_id,'ter_event_id'=>$event_id,'ter_player_id'=>$edit_id_score])->all());		 
		 if(count($event_result_db)>0){
		 	$model_matches = TournamentsEventResult::find()->where(['ter_tournament_id'=>$tournament_id,'ter_event_id'=>$event_id,'ter_player_id'=>$edit_id_score])->one();				
		  	$set_array=explode(",",$model_matches->ter_set);
			for($rr=1;$rr<=6;$rr++){
				$score_set.='<div class="col-md-2"><label>Set '.$rr.'</label>'.Html::input('number', 'set_val[]', $set_array[($rr-1)] ,['class' => 'form-control'] )."</div>";
			}
		  }else{
			for($rr=1;$rr<=6;$rr++){
				$score_set.='<div class="col-md-2"><label>Set '.$rr.'</label>'.Html::input('number', 'set_val[]', '', ['class' => 'form-control'] )."</div>";
			}
		  }
		  $event_result_db1=ArrayHelper::toArray(TournamentsEventMatche::find()->where(['tem_tournament_id'=>$tournament_id,'tem_event_id'=>$event_id,'tem_player_id'=>$edit_id_score])->all());
		  $match_details='';
		  $matchlocation_db='';
		  $scheduledtime_db ='';	
		  $scheduleddate1_db='';
		  if(count($event_result_db1)>0){
		 	$model_matches1 = TournamentsEventMatche::find()->where(['tem_tournament_id'=>$tournament_id,'tem_event_id'=>$event_id,'tem_player_id'=>$edit_id_score])->one();
			$matchlocation_db=$model_matches1->tem_location;
		  	$scheduledtime_db =$model_matches1->tem_scheduled_time;	
		  	$scheduleddate1_db=date("d-m-Y",strtotime($model_matches1->tem_scheduled_date));
		  }
		  $location_btn=Html::input('text', 'matchlocation', $matchlocation_db, ['class' => 'form-control','id'=>'matchlocation'] );
		  $scheduledtime_btn=Html::input('text', 'scheduledtime', $scheduledtime_db, ['class' => 'form-control','id'=>'scheduledtime']);
		  $scheduleddate_btn=Html::input('text', 'scheduleddate1', $scheduleddate1_db, ['id'=>'scheduleddate1','class' => 'form-control datepicker', 'placeholder' => 'DD-MM-YYYY']);
		  $match_details='<div class="col-md-6 box-body">
                  		<div class="form-group"><label>Location</label>'.$location_btn.'</div>
                 		<div class="form-group"><label>Scheduled Date</label>'.$scheduleddate_btn.'</div>                	  
                  </div>
                  	<div class="col-md-6 box-body">
                  	<div class="form-group"><label>Scheduled Time</label>'.$scheduledtime_btn.'</div>
                  	</div>';
		  echo $score_set."~~".$match_details;		  	
		}else{
			
		}
}

public function actionSchediledetails(){
	if(Yii::$app->request->post('tournament_id')!=''){
		$tournament_id=Yii::$app->request->post('tournament_id');
		}
		$event_id='';
		if(Yii::$app->request->post('event_id')!=''){
			$event_id=Yii::$app->request->post('event_id');
		}	
		$edit_id_score=array();
		if(count(Yii::$app->request->post('edit_id_score'))!=''){
			$edit_id_score=Yii::$app->request->post('edit_id_score');
		}
		$set_value=array();
		if(count(Yii::$app->request->post('set_value'))!=''){
			$set_value=Yii::$app->request->post('set_value');
		}
		$matchlocation=array();
		if(count(Yii::$app->request->post('matchlocation'))!=''){
			$matchlocation=Yii::$app->request->post('matchlocation');
		}
		$scheduledtime=array();
		if(count(Yii::$app->request->post('scheduledtime'))!=''){
			$scheduledtime=Yii::$app->request->post('scheduledtime');
		}
		$scheduleddate=array();
		if(count(Yii::$app->request->post('scheduleddate1'))!=''){
			$scheduleddate1=Yii::$app->request->post('scheduleddate1');
			$tm=explode("-",$scheduleddate1);
			$scheduleddate=$tm[2]."-".$tm[1]."-".$tm[0];
		}
		$player_reg_id='';
		if(count(Yii::$app->request->post('player_reg_id'))!=''){
			$player_reg_id=Yii::$app->request->post('player_reg_id');			
		}
		if($tournament_id!="" && $event_id!="" && $edit_id_score!=""){			
			$event_result_db=ArrayHelper::toArray(TournamentsEventMatche::find()->where(['tem_tournament_id'=>$tournament_id,'tem_event_id'=>$event_id,'tem_player_id'=>$edit_id_score])->all());
		//echo count($event_result_db);
		 if(count($event_result_db)>0){
			$model_matches = TournamentsEventMatche::find()->where(['tem_tournament_id'=>$tournament_id,'tem_event_id'=>$event_id,'tem_player_id'=>$edit_id_score])->one();				
		  	$model_matches->tem_location=$matchlocation;
			$model_matches->tem_scheduled_time=$scheduledtime;
			$model_matches->tem_scheduled_date=$scheduleddate;
			$model_matches->tem_player_reg_id=$player_reg_id;
			$model_matches->save();		  
		  }else{
		  	$model_matches = new TournamentsEventMatche();
		  	$model_matches->tem_tournament_id=$tournament_id;
		 	$model_matches->tem_event_id=$event_id;
			$model_matches->tem_player_id=$edit_id_score;
			$model_matches->tem_location=$matchlocation;
			$model_matches->tem_scheduled_time=$scheduledtime;
			$model_matches->tem_scheduled_date=$scheduleddate;
			$model_matches->tem_player_reg_id=$player_reg_id;
			$model_matches->save();
		  }		
		  	//return "Success";
		}else{
			//	return "Error";
		}	
}
public function actionDelete_drawsheet($drawfileid='',$tournametid='')
{
		if($drawfileid!=""){
			Drawsheet::deleteall('draw_id='.$drawfileid);
		}
        return $this->redirect(['index','tournamentid'=>$tournametid]);
}
}
