<?php

namespace backend\controllers;

use Yii;
use backend\models\GalleryPageModel;
use backend\models\GalleryPageSearch;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
/**
 * GalleryPageController implements the CRUD actions for GalleryPageModel model.
 */
class GalleryPageController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
          'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],

                // ...
            ],
        ],


        ];
    }


     public function beforeAction($action) {
    $this->enableCsrfValidation = false;
    return parent::beforeAction($action);
}

    /**
     * Lists all GalleryPageModel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GalleryPageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single GalleryPageModel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new GalleryPageModel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new GalleryPageModel();
		$file_array=array();
		if (Yii::$app->request->isPost) {  		
                //Below condition is for whether the file is available or empty                
                if (count($_FILES['GalleryPageModel']['name']['gallery_multi_images'])>0) {
                	$gallery_id=$id;
                	$model->gallery_multi_images = UploadedFile::getInstances($model, 'gallery_multi_images');
	                   // $image_name = 'galleryimage/gallery_pages' . $model->file->basename . $rand . "." . $model->file->extension;
	                    $model->upload_img($gallery_id);
					foreach($_FILES['GalleryPageModel']['name']['gallery_multi_images'] as $key_file=>$onefile){
						$img_path=$gallery_id.'_'.$_FILES['GalleryPageModel']['name']['gallery_multi_images'][$key_file];
						$img_type=".".$_FILES['GalleryPageModel']['type']['gallery_multi_images'][$key_file];
						$file_array[]=['gal_image_rootid'=>$gallery_id,'gal_image_path'=>$img_path,'gal_image_type'=>$img_type,'gal_image_order'=>$key_file];
					
					}					
					 $model->upload_files($file_array);
				}
			return $this->redirect(['', 'id' => $gallery_id]);
		}else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->gal_image_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }*/
    }

    /**
     * Updates an existing GalleryPageModel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->gal_image_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing GalleryPageModel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeletedata()
    {

        $id = $_GET['id']; // Deleting image id
        $contentid = $_GET['content']; //Content redirect id
        //$model = $this->findModel($id);
        $this->findModel($id)->delete();
            echo 'S';
        // $locate_delete_file =  $_SERVER['SERVER_NAME'].Url::base().'/galleryimage/gallery_pages/'.$model->gal_image_path;

        // if (!unlink($locate_delete_file))
        //   {
        //   echo ("Error deleting");
        //   }
        // else
        //   {
        //     $this->findModel($id)->delete();
        //     echo 'S';
        //   }

        
    }

    /**
     * Finds the GalleryPageModel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GalleryPageModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GalleryPageModel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        
		}
    }
    
	public function actionUpdateimgorder() {
        if (Yii::$app->request->isPost) {
        	$model = new GalleryPageModel();
        	$request = Yii::$app->request;
			$order_list = $request->post('ss');print_r($order_list);
			$model->update_order_images($order_list);
		}else{
			//echo "error";
		}
    }
}
