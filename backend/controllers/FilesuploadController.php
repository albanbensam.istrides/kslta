<?php

namespace backend\controllers;

use Yii;
use backend\models\Filesupload;
use backend\models\FilesuploadSearch;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * FilesuploadController implements the CRUD actions for Filesupload model.
 */
class FilesuploadController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Filesupload models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FilesuploadSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Filesupload model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Filesupload model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
   /* public function actionCreate()
    {
        $model = new Filesupload();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }*/
    
    
    public function actionCreate() {

        $model = new Filesupload();

          if(Yii::$app->request->isPost) {
         
                //Below condition is for whether the file is available or empty
                if (isset($_FILES['Filesupload']['name']['filelink']) && !empty($_FILES['Filesupload']['name']['filelink'])) {
                		
                   // $rand = rand(0, 99999); // random number generation for unique image save
                    $model->file = UploadedFile::getInstance($model, 'filelink');
                    $file_name = '../../fileupload/' . $model->file->basename . "." . $model->file->extension;
				    $filename = $model->file->basename;
                    $model->file->saveAs($file_name);
					$filelink = Url::to(''.'fileupload/'.$model->file->basename . "." . $model->file->extension);
					//echo $filelink = $model->file->basename . $rand . "." . $model->file->extension;exit;
				// $thumbFile = 'imageupload/thumb/'. $model->file->basename . $rand . "." . $model->file->extension;
			      // $thumbimage = Image::thumbnail( $image_name, 200, 200)->save($thumbFile, ['quality' => 80]);
			     
				   $model->filename = $filename;
                   $model->filelink = $filelink;
				   $model->created_at = date('Y-m-d');
					$model->save();
				}
				if($model->save()){
					
                return $this->redirect(['index']);
				}
				else {
					echo "<pre>";
					print_r($model->getErrors());exit;
				}
                
            }
		  else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    

    /**
     * Updates an existing Filesupload model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
     
        public function actionUpdate($id)
    {
        $model = $this->findModel($id);
         
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isPost) {
                //Below condition is for whether the file is available or empty

                if ($_FILES['Filesupload']['name']['filelink'] != '') {
                    $rand = rand(0, 99999); // random number generation for unique image save
                    //$model->scenario = 'imageUploaded';
                    $model->file = UploadedFile::getInstance($model, 'filelink');
					     $file_name = '../../fileupload/' . $model->file->basename .  "." . $model->file->extension;
						 $filename = $model->file->basename;
                   		 $model->file->saveAs($file_name);  
						 $filelink = Url::to(''.'fileupload/'.$model->file->basename . "." . $model->file->extension);
						 
				  		 $model->filename = $filename;
                   		 $model->filelink = $filelink;
				         $model->updated_at = date('Y-m-d');
					     $model->save();
                }
                if ($model->save()) {
                    Yii::$app->getSession()->setFlash('success', 'Image has been Updated successfully.');
                    return $this->redirect(['index']);
                } else {
                    Yii::$app->getSession()->setFlash('error', 'Something went wrong !');
                }
            }
            // return $this->redirect(['view', 'id' => $model->news_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }
  /*  public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }*/

    /**
     * Deletes an existing Filesupload model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Filesupload model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Filesupload the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Filesupload::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
