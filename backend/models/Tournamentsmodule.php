<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ksl_tournaments".
 *
 * @property integer $tourn_id
 * @property string $tourn_title
 * @property string $tourn_category
 * @property string $tourn_sub_category
 * @property string $tourn_venue
 * @property string $tourn_venue_address1
 * @property string $tourn_venue_address2
 * @property string $tourn_venue_city
 * @property string $tourn_from_date
 * @property string $tourn_to_date
 * @property string $tourn_last_submission_date
 * @property string $tourn_withdrl_date
 * @property string $tourn_description
 * @property string $tourn_asso_title
 * @property string $tourn_asso_address1
 * @property string $tourn_asso_address2
 * @property string $tourn_asso_head
 * @property string $tourn_refree1
 * @property string $tourn_refree2
 * @property string $tourn_refree3
 * @property string $tourn_link
 * @property string $tourn_created_date
 * @property string $tourn_lastmodified_date
 * @property string $tourn_status
 * @property string $sponser_name_of_organisation
 * @property string $sponser_address1
 * @property string $sponser_address2
 * @property string $sponser_contact_person_name
 * @property string $sponser_phone
 * @property string $sponser_email
 * @property string $tour_event_category
 * @property string $tour_event_subcategory
 */
class Tournamentsmodule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ksl_tournaments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tourn_title', 'tourn_category', 'tourn_sub_category', 'tourn_venue', 'tourn_venue_address1', 'tourn_venue_address2', 'tourn_venue_city', 'tourn_from_date', 'tourn_to_date', 'tourn_last_submission_date', 'tourn_withdrl_date', 'tourn_description', 'tourn_asso_title', 'tourn_asso_address1', 'tourn_asso_address2', 'tourn_asso_head', 'tourn_refree1', 'tourn_refree2', 'tourn_refree3', 'tourn_link', 'tourn_created_date', 'sponser_name_of_organisation', 'sponser_address1', 'sponser_address2', 'sponser_contact_person_name', 'sponser_phone', 'sponser_email', 'tour_event_category', 'tour_event_subcategory'], 'required'],
            [['tourn_from_date', 'tourn_to_date', 'tourn_last_submission_date', 'tourn_withdrl_date', 'tourn_created_date', 'tourn_lastmodified_date'], 'safe'],
            [['tourn_status'], 'string'],
            [['tourn_title', 'sponser_address1', 'sponser_address2'], 'string', 'max' => 300],
            [['tourn_category', 'tourn_sub_category'], 'string', 'max' => 30],
            [['tourn_venue', 'tourn_venue_city', 'tourn_refree1', 'tourn_refree2', 'tourn_refree3', 'tourn_link', 'sponser_contact_person_name'], 'string', 'max' => 100],
            [['tourn_venue_address1', 'tourn_venue_address2', 'tourn_asso_title', 'tourn_asso_address1', 'tourn_asso_address2', 'sponser_name_of_organisation', 'sponser_email'], 'string', 'max' => 200],
            [['tourn_description'], 'string', 'max' => 1500],
            [['tourn_asso_head'], 'string', 'max' => 70],
            [['sponser_phone', 'tour_event_category', 'tour_event_subcategory'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tourn_id' => 'Tourn ID',
            'tourn_title' => 'Tourn Title',
            'tourn_category' => 'Tourn Category',
            'tourn_sub_category' => 'Tourn Sub Category',
            'tourn_venue' => 'Tourn Venue',
            'tourn_venue_address1' => 'Tourn Venue Address1',
            'tourn_venue_address2' => 'Tourn Venue Address2',
            'tourn_venue_city' => 'Tourn Venue City',
            'tourn_from_date' => 'Tourn From Date',
            'tourn_to_date' => 'Tourn To Date',
            'tourn_last_submission_date' => 'Tourn Last Submission Date',
            'tourn_withdrl_date' => 'Tourn Withdrl Date',
            'tourn_description' => 'Tourn Description',
            'tourn_asso_title' => 'Tourn Asso Title',
            'tourn_asso_address1' => 'Tourn Asso Address1',
            'tourn_asso_address2' => 'Tourn Asso Address2',
            'tourn_asso_head' => 'Tourn Asso Head',
            'tourn_refree1' => 'Tourn Refree1',
            'tourn_refree2' => 'Tourn Refree2',
            'tourn_refree3' => 'Tourn Refree3',
            'tourn_link' => 'Tourn Link',
            'tourn_created_date' => 'Tourn Created Date',
            'tourn_lastmodified_date' => 'Tourn Lastmodified Date',
            'tourn_status' => 'Tourn Status',
            'sponser_name_of_organisation' => 'Sponser Name Of Organisation',
            'sponser_address1' => 'Sponser Address1',
            'sponser_address2' => 'Sponser Address2',
            'sponser_contact_person_name' => 'Sponser Contact Person Name',
            'sponser_phone' => 'Sponser Phone',
            'sponser_email' => 'Sponser Email',
            'tour_event_category' => 'Tour Event Category',
            'tour_event_subcategory' => 'Tour Event Subcategory',
        ];
    }
}
