<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ksl_tournament_playerlists".
 *
 * @property string $playerlists_id
 * @property string $tourn_id
 * @property string $event_id
 * @property string $playerlists_filename
 * @property string $playerlists_filepath
 * @property string $playerlists_created_at
 * @property string $playerlists_modified_at
 * @property string $playerlists_active_status
 */
class TournamentPlayerList extends \yii\db\ActiveRecord
{



    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ksl_tournament_playerlists';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['playerlists_filepath'], 'required'],
            // [['tourn_id', 'event_id'], 'integer'],
            // [['playerlists_filepath', 'playerlists_active_status'], 'string'],
            // [['playerlists_created_at', 'playerlists_modified_at'], 'safe'],
            // [['playerlists_filename'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'playerlists_id' => 'Playerlists ID',
            'tourn_id' => 'Tourn ID',
            'event_id' => 'Event ID',
            'playerlists_filename' => 'Playerlists Filename',
            'playerlists_filepath' => 'PLAYER LIST IMAGE UPLOAD',
            'playerlists_created_at' => 'Created At',
            'playerlists_modified_at' => 'Modified At',
            'playerlists_active_status' => 'Active Status',
        ];
    }
}
