<?php

namespace backend\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\Html;
/**
 * This is the model class for table "ksl_gallery_tbl".
 *
 * @property integer $gallery_id
 * @property string $gallery_title
 * @property string $gallery_display_tag
 * @property string $gallery_link_address
 * @property string $gallery_link_status
 * @property string $gallery_content_image
 * @property string $gallery_posted_by
 * @property string $gallery_picture_courtesy
 * @property string $gallery_from_date
 * @property string $gallery_to_date
 * @property string $gallery_created_date
 * @property string $gallery_lastupdated_date
 * @property string $gallery_flat
 */
class KslGalleryModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
      public $file;
    public static function tableName()
    {
        return 'ksl_gallery_tbl';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gallery_title', 'gallery_display_tag','gallery_posted_by'], 'required'],           
            [['gallery_from_date', 'gallery_to_date', 'gallery_created_date', 'gallery_lastupdated_date'], 'safe'],
            [['gallery_title', 'gallery_link_address'], 'string', 'max' => 500],
            [['gallery_display_tag'], 'string', 'max' => 100],
            [['gallery_content_image'], 'string', 'max' => 250],
            [['gallery_link_address', 'gallery_link_address'], 'string', 'max' => 500],
            [['file'], 'file'],
            [['gallery_posted_by', 'gallery_picture_courtesy'], 'string', 'max' => 200]            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'gallery_id' => 'Gallery ID',
            'gallery_title' => 'Gallery Title',
            'gallery_display_tag' => 'Display Page',
            'gallery_link_address' => 'Cover Image',
            'gallery_content_image' => 'Short Description',
            'gallery_posted_by' => 'Gallery Posted By',
            'gallery_picture_courtesy' => 'Gallery Picture Courtesy',
            'gallery_from_date' => 'Gallery From Date',
            'gallery_to_date' => 'Gallery To Date',
            'gallery_created_date' => 'Gallery Created Date',
            'gallery_lastupdated_date' => 'Gallery Lastupdated Date',
            'gallery_flat' => 'Gallery Flat',
        ];
    }



     /**
     * @inheritdoc
     */
    public function beforeSave($insert) {

        if ($this->getIsNewRecord()) {
            $this->gallery_created_date = date("Y-m-d H:i:s");
           
        }
        $this->gallery_from_date = date('Y-m-d', strtotime($this->gallery_from_date));
        $this->gallery_to_date = date('Y-m-d', strtotime($this->gallery_to_date));
       
        if (parent::beforeSave($insert)) {
            // Place your custom code here

            return true;
        } else {
            return false;
        }
    }
    
    
     public function afterFind() {
       //  $this->gallery_from_date = date('d-m-Y', strtotime($this->gallery_from_date));
       // $this->gallery_to_date = date('d-m-Y', strtotime($this->gallery_to_date));
       
       
        parent::afterFind();
        
    }
}
