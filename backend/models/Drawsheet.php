<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ksl_event_drawsheet".
 *
 * @property string $draw_id
 * @property string $draw_tournamentid
 * @property string $draw_eventid
 * @property string $draw_filename
 * @property string $draw_datetime
 * @property string $draw_created
 */
class Drawsheet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ksl_event_drawsheet';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['draw_tournamentid', 'draw_eventid', 'draw_filepath'], 'required'],
            [['draw_tournamentid', 'draw_eventid'], 'integer'],
            [['draw_filepath'], 'string'],
            [['draw_filename'], 'string'],
            [['draw_datetime', 'draw_created'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'draw_id' => 'Draw ID',
            'draw_tournamentid' => 'Draw Tournamentid',
            'draw_eventid' => 'Draw Eventid',
            'draw_filepath' => 'Draw Filename',
            'draw_filename' => 'Draw Filename',
            'draw_datetime' => 'Draw Datetime',
            'draw_created' => 'Draw Created',
        ];
    }
}
