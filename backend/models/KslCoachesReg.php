<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ksl_coaches_reg".
 *
 * @property integer $id
 * @property string $name
 * @property string $dob
 * @property string $gender
 * @property string $p_address
 * @property string $contact_no
 * @property string $mob_no
 * @property string $email_id
 * @property string $e_qual
 * @property string $tennis_qual
 * @property string $certificate
 * @property string $remarks
 * @property string $active_status
 * @property string $created_at
 * @property string $updated_at
 */
class KslCoachesReg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ksl_coaches_reg';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dob', 'created_at', 'updated_at'], 'safe'],
            [['p_address', 'certificate', 'remarks'], 'string'],
            [['email_id'], 'required'],
            [['name', 'email_id', 'e_qual', 'tennis_qual'], 'string', 'max' => 255],
            [['gender', 'contact_no', 'mob_no', 'active_status'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'dob' => 'Date of Birth',
            'gender' => 'Gender',
            'p_address' => 'Permanent Address',
            'contact_no' => 'Contact Details',
            'mob_no' => 'Mobile No',
            'email_id' => 'Email ID',
            'e_qual' => 'Educational Qualification',
            'tennis_qual' => 'Tennis Qualification',
            'certificate' => 'Certificate',
            'remarks' => 'Remarks',
            'active_status' => 'Active Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
