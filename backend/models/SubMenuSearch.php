<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\SubMenu;

/**
 * SubMenuSearch represents the model behind the search form about `backend\models\SubMenu`.
 */
class SubMenuSearch extends SubMenu
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sub_menu_id', 'menu_id', 'sub_menu_order_no'], 'integer'],
            [['sub_menu_name', 'sub_menu_link', 'sub_menu_visible_status', 'sub_menu_created_at', 'menuname'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SubMenu::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'sub_menu_id' => $this->sub_menu_id,
            'menu_id' => $this->menu_id,
            'sub_menu_order_no' => $this->sub_menu_order_no,
            'sub_menu_created_at' => $this->sub_menu_created_at,
            'sub_menu_modified_at' => $this->sub_menu_modified_at,
        ]);

        $query->andFilterWhere(['like', 'sub_menu_name', $this->sub_menu_name])
            ->andFilterWhere(['like', 'sub_menu_link', $this->sub_menu_link])
            ->andFilterWhere(['like', 'sub_menu_visible_status', $this->sub_menu_visible_status])
            ->andFilterWhere(['like', 'sub_menu_status', $this->sub_menu_status]);

        return $dataProvider;
    }
}
