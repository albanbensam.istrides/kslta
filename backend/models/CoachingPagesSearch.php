<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CoachingPages;

/**
 * CoachingPagesSearch represents the model behind the search form about `backend\models\CoachingPages`.
 */
class CoachingPagesSearch extends CoachingPages
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coaching_id', 'gallery_id'], 'integer'],
            [['coaching_page_title', 'coaching_page_name', 'coaching_tab_one_title', 'coaching_tab_one_content', 'coaching_tab_two_title', 'coaching_tab_two_content_1', 'coaching_tab_two_content_2', 'coaching_tab_three_title', 'coaching_file_upload', 'coaching_created_at', 'coaching_modified_at', 'coaching_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CoachingPages::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'coaching_id' => $this->coaching_id,
            'gallery_id' => $this->gallery_id,
            'coaching_created_at' => $this->coaching_created_at,
            'coaching_modified_at' => $this->coaching_modified_at,
        ]);

        $query->andFilterWhere(['like', 'coaching_page_title', $this->coaching_page_title])
            ->andFilterWhere(['like', 'coaching_page_name', $this->coaching_page_name])
            ->andFilterWhere(['like', 'coaching_tab_one_title', $this->coaching_tab_one_title])
            ->andFilterWhere(['like', 'coaching_tab_one_content', $this->coaching_tab_one_content])
            ->andFilterWhere(['like', 'coaching_tab_two_title', $this->coaching_tab_two_title])
            ->andFilterWhere(['like', 'coaching_tab_two_content_1', $this->coaching_tab_two_content_1])
            ->andFilterWhere(['like', 'coaching_tab_two_content_2', $this->coaching_tab_two_content_2])
            ->andFilterWhere(['like', 'coaching_tab_three_title', $this->coaching_tab_three_title])
            ->andFilterWhere(['like', 'coaching_file_upload', $this->coaching_file_upload])
            ->andFilterWhere(['like', 'coaching_status', $this->coaching_status]);

        return $dataProvider;
    }
}
