<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "dropdown_management".
 *
 * @property string $dropdown_id
 * @property string $dropdown_key
 * @property string $dropdown_value
 * @property string $dropdown_order
 */
class DropdownManagement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dropdown_management';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dropdown_key', 'dropdown_value', 'dropdown_order'], 'required'],
            [['dropdown_order'], 'integer'],
            [['dropdown_key'], 'string', 'max' => 50],
            [['dropdown_value'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dropdown_id' => 'Dropdown ID',
            'dropdown_key' => 'Dropdown Key',
            'dropdown_value' => 'Dropdown Value',
            'dropdown_order' => 'Dropdown Order',
        ];
    }
}
