<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ksl_slider_banner".
 *
 * @property integer $banner_id
 * @property string $banner_name
 * @property string $banner_url
 * @property string $banner_image
 * @property integer $banner_order_flow
 * @property string $banner_date
 * @property string $banner_created_at
 * @property string $banner_modified_at
 * @property string $banner_status
 */
class SliderBanner extends \yii\db\ActiveRecord
{

    //custom property for file upload
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ksl_slider_banner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['banner_name', 'banner_image'], 'required'],
            [['banner_image'], 'file', 'extensions'=>'jpg, gif, png', 'on' => 'imageUploaded'],
            [['banner_order_flow'], 'integer'],
            [['banner_date', 'banner_created_at', 'banner_modified_at'], 'safe'],
            [['banner_status'], 'string'],
            [['banner_name'], 'string', 'max' => 250],
            [['banner_url'], 'string', 'max' => 750],
            [['banner_image'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'banner_id' => 'Banner ID',
            'banner_name' => 'TITLE',
            'banner_url' => 'URL',
            'banner_image' => 'BANNER IMAGE',
            'banner_order_flow' => 'ORDER FLOW',
            'banner_date' => 'DATE',
            'banner_created_at' => 'Banner Created At',
            'banner_modified_at' => 'Banner Modified At',
            'banner_status' => 'Banner Status',
        ];
    }


    /**
     * @this function will work before save() has to call
     */
    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->banner_created_at = date("Y-m-d H:i:s");
        }
        if (parent::beforeSave($insert)) {
            // Place your custom code here
           return true;
        } else {
            return false;
        }
    }
    
    
    /**
     * @this function will work after getting the data from db and changes view commenly
     */
     public function afterFind() {
         //$this->reg_dob = date('d-m-Y', strtotime($this->reg_dob));
       // $this->banner_date = date('d-m-Y H:i:s', strtotime($this->banner_date));
       // $this->banner_url = $this->banner_url == '' ? 'Not Available' : $this->banner_url;
       
        parent::afterFind();
        
    }
}
