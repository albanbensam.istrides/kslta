<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TournamentEventModel;

/**
 * TournamentEventModelSearch represents the model behind the search form about `backend\models\TournamentEventModel`.
 */
class TournamentEventModelSearch extends TournamentEventModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'tourn_id', 'event_category', 'event_subcategory'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TournamentEventModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'event_id' => $this->event_id,
            'tourn_id' => $this->tourn_id,
            'event_category' => $this->event_category,
            'event_subcategory' => $this->event_subcategory,
        ]);

        return $dataProvider;
    }
}
