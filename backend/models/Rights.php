<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "rights".
 *
 * @property integer $id
 * @property string $rights_list
 * @property integer $user_id
 * @property string $created_date
 */
class Rights extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rights';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rights_list', 'user_id'], 'required'],
            [['rights_list'], 'string'],
            [['user_id'], 'integer'],
           
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rights_list' => 'Rights List',
            'user_id' => 'User ID',
         
        ];
    }
}
