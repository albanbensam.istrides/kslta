<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\DropdownManagement;

/**
 * DropdownManagementSearch represents the model behind the search form about `backend\models\DropdownManagement`.
 */
class DropdownManagementSearch extends DropdownManagement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dropdown_id', 'dropdown_order'], 'integer'],
            [['dropdown_key', 'dropdown_value'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DropdownManagement::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'dropdown_id' => $this->dropdown_id,
            'dropdown_order' => $this->dropdown_order,
        ]);

        $query->andFilterWhere(['like', 'dropdown_key', $this->dropdown_key])
            ->andFilterWhere(['like', 'dropdown_value', $this->dropdown_value]);

        return $dataProvider;
    }
}
