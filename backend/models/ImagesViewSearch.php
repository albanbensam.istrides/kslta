<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ImagesView;

/**
 * ImagesViewSearch represents the model behind the search form about `backend\models\ImagesView`.
 */
class ImagesViewSearch extends ImagesView
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image_id'], 'integer'],
            [['image_visible_page', 'image_title', 'image_over_title', 'image_top_title', 'image_sub_title', 'image_file_location', 'image_file_name', 'image_type', 'image_visible_status', 'image_modified_at', 'image_delete_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ImagesView::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'image_id' => $this->image_id,
            'image_modified_at' => $this->image_modified_at,
        ]);

        $query->andFilterWhere(['like', 'image_visible_page', $this->image_visible_page])
            ->andFilterWhere(['like', 'image_title', $this->image_title])
            ->andFilterWhere(['like', 'image_over_title', $this->image_over_title])
            ->andFilterWhere(['like', 'image_top_title', $this->image_top_title])
            ->andFilterWhere(['like', 'image_sub_title', $this->image_sub_title])
            ->andFilterWhere(['like', 'image_file_location', $this->image_file_location])
            ->andFilterWhere(['like', 'image_file_name', $this->image_file_name])
            ->andFilterWhere(['like', 'image_type', $this->image_type])
            ->andFilterWhere(['like', 'image_visible_status', $this->image_visible_status])
            ->andFilterWhere(['like', 'image_delete_status', $this->image_delete_status]);

        return $dataProvider;
    }
}
