<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "homepage_scroll_news".
 *
 * @property string $id
 * @property string $news
 * @property string $status
 * @property string $created_at
 * @property string $created_by
 */
class HomepageScrollNews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'homepage_scroll_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'news'], 'required'],
            [['id'], 'integer'],
            [['status'], 'string'],
            [['created_at','sort_order','status', 'created_at', 'created_by'], 'safe'],
            [['news', 'created_by'], 'string', 'max' => 2000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'news' => 'News',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'sort_order' => 'Sort Order',
        ];
    }
}
