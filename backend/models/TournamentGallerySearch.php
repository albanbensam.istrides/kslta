<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TournamentGallery;

/**
 * TournamentGallerySearch represents the model behind the search form about `backend\models\TournamentGallery`.
 */
class TournamentGallerySearch extends TournamentGallery
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tourn_gallery_id', 'tourn_id', 'gallery_id'], 'integer'],
            [['tourn_gallery_date', 'tourn_gallery_created_at', 'tourn_gallery_modified_at', 'tourn_gallery_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TournamentGallery::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'tourn_gallery_id' => $this->tourn_gallery_id,
            'tourn_id' => $this->tourn_id,
            'gallery_id' => $this->gallery_id,
            'tourn_gallery_date' => $this->tourn_gallery_date,
            'tourn_gallery_created_at' => $this->tourn_gallery_created_at,
            'tourn_gallery_modified_at' => $this->tourn_gallery_modified_at,
        ]);

        $query->andFilterWhere(['like', 'tourn_gallery_status', $this->tourn_gallery_status]);

        return $dataProvider;
    }
}
