<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "main_menu".
 *
 * @property string $menu_id
 * @property string $menu_name
 * @property integer $menu_order_no
 * @property string $menu_link
 * @property string $menu_front_view_status
 * @property string $menu_created_at
 * @property string $menu_modified_at
 * @property string $menu_active_status
 */
class MainMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'main_menu';
    }

    //public $sub_menu_name;
    //public $sub_menu_order_no;
    //public $sub_menu_link;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_name', 'menu_order_no'], 'required'],
            [['menu_order_no'], 'integer'],
            [['menu_link', 'menu_front_view_status', 'menu_active_status'], 'string'],
            [['menu_created_at', 'menu_modified_at'], 'safe'],
            [['menu_name'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'menu_id' => 'Menu ID',
            'menu_name' => 'Menu Name',
            'menu_order_no' => 'Menu Order No',
            'menu_link' => 'Menu Link',
            'menu_front_view_status' => 'Menu Front View Status',
            'menu_created_at' => 'Menu Created At',
            'menu_modified_at' => 'Menu Modified At',
            'menu_active_status' => 'Menu Active Status',
        ];
    }


     /**
     * @this function will work before save() has to call
     */
    public function beforeSave($insert) {

        if ($this->isNewRecord) {
           
        }

         $this->menu_created_at = date("Y-m-d H:i:s");
       
        if (parent::beforeSave($insert)) {
            // Place your custom code here
           return true;
        } else {
            return false;
        }
    }
    
    
    /**
     * @this function will work after getting the data from db and changes view commenly
     */
     public function afterFind() {
        // $this->menu_created_at = date('d-m-Y H:i:s', strtotime($this->menu_created_at));
        
        parent::afterFind();
    }
}
