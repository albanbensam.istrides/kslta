<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "home_facilities".
 *
 * @property string $id
 * @property string $facility_name
 * @property string $facility_url
 * @property string $order_no
 * @property string $created_at
 */
class HomeFacilities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
     public $file;
    public static function tableName()
    {
        return 'home_facilities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[  'facility_name', 'facility_url', 'order_no', ], 'required'],
            [['id', 'order_no'], 'integer'],
            [['created_at'], 'safe'],
             
               [['image_upload'], 'file',  'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'facility_name' => 'Facility Name',
            'facility_url' => 'Facility Url',
            'order_no' => 'Order No',
            'created_at' => 'Created At',
        ];
    }
}
