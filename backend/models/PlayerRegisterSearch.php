<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PlayerRegister;

/**
 * PlayerRegisterSearch represents the model behind the search form about `backend\models\PlayerRegister`.
 */
class PlayerRegisterSearch extends PlayerRegister
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reg_id', 'tourn_id', 'reg_player_category'], 'integer'],
            [['reg_first_name', 'reg_gender','reg_last_name', 'reg_dob', 'reg_email', 'reg_player_id', 'reg_city', 'reg_mobile', 'registration_date', 'reg_created_at', 'reg_last_modified_at', 'reg_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PlayerRegister::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['reg_id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'reg_id' => $this->reg_id,
            'tourn_id' => $this->tourn_id,
            'reg_dob' => $this->reg_dob,
            'reg_player_category' => $this->reg_player_category,
            'registration_date' => $this->registration_date,
            'reg_created_at' => $this->reg_created_at,
            'reg_last_modified_at' => $this->reg_last_modified_at,
        ]);

        $query->andFilterWhere(['like', 'reg_first_name', $this->reg_first_name])
            ->andFilterWhere(['like', 'reg_last_name', $this->reg_last_name])
            ->andFilterWhere(['like', 'reg_email', $this->reg_email])
            ->andFilterWhere(['like', 'reg_player_id', $this->reg_player_id])
            ->andFilterWhere(['like', 'reg_city', $this->reg_city])
            ->andFilterWhere(['like', 'reg_mobile', $this->reg_mobile])
            ->andFilterWhere(['like', 'reg_status', $this->reg_status])
			->andFilterWhere(['like','reg_gender',$this->reg_gender]);

        return $dataProvider;
    }
}
