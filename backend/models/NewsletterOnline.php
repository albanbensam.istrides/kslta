<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ksl_newsletter_online".
 *
 * @property string $newsletter_id
 * @property string $newsletter_name
 * @property string $newsletter_email
 * @property string $newsletter_mobile
 * @property string $newsletter_email_view_status
 * @property string $newsletter_created_at
 * @property string $newsletter_modified_at
 * @property string $newsletter_active_status
 */
class NewsletterOnline extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ksl_newsletter_online';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['newsletter_name', 'newsletter_email', 'newsletter_mobile', 'newsletter_created_at', 'newsletter_active_status'], 'required'],
            [['newsletter_email_view_status', 'newsletter_active_status'], 'string'],
            [['newsletter_created_at', 'newsletter_modified_at'], 'safe'],
            [['newsletter_name', 'newsletter_email'], 'string', 'max' => 500],
            [['newsletter_mobile'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'newsletter_id' => 'Newsletter ID',
            'newsletter_name' => 'Newsletter Name',
            'newsletter_email' => 'Newsletter Email',
            'newsletter_mobile' => 'Newsletter Mobile',
            'newsletter_email_view_status' => 'Newsletter Email View Status',
            'newsletter_created_at' => 'Newsletter Created At',
            'newsletter_modified_at' => 'Newsletter Modified At',
            'newsletter_active_status' => 'Newsletter Active Status',
        ];
    }
}
