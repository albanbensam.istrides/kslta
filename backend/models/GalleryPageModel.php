<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ksl_gallery_image_tbl".
 *
 * @property integer $gal_image_id
 * @property string $gal_image_rootid
 * @property string $gal_image_path
 * @property string $gal_image_order
 * @property string $gal_image_created
 */
class GalleryPageModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
   
    public static function tableName()
    {
        return 'ksl_gallery_image_tbl';
    }

    /**
     * @inheritdoc
     */    
	 public $gallery_multi_images;
    public function rules()
    {
        return [
            [['gal_image_rootid', 'gal_image_path'], 'required'],
            [['gal_image_order'], 'integer'],
            [['gal_image_created'], 'safe'],
            [['gal_image_rootid'], 'string', 'max' => 100],
            [['gal_image_path'], 'string', 'max' => 300],
                    
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [            
            'gal_image_path' => 'Gal Image Path',
        ];
    }
	public function upload_img($gallery_id='')
    {
        if ($gallery_id!='') {
            foreach ($this->gallery_multi_images as $file) {
                $file->saveAs('galleryimage/gallery_pages/' .$gallery_id.'_' .$file->baseName . '.' . $file->extension);
            }
            return true;       
        }else {
            return false;
        }
    }
	public function upload_files($bulkInsertArray=array())
    {
		if(count($bulkInsertArray)>0){
	    $columnNameArray=['gal_image_rootid','gal_image_path','gal_image_type','gal_image_order'];
	    // below line insert all your record and return number of rows inserted
	    $insertCount = Yii::$app->db->createCommand()
	                   ->batchInsert(
	                         'ksl_gallery_image_tbl', $columnNameArray, $bulkInsertArray
	                     )
	                   ->execute();
	
		}
	}
	public function update_order_images($order_list){		
		if(count($order_list)>0){
			$ing=0;
			foreach($order_list as $oneorder=>$one_img_id){
	         Yii::$app->db->createCommand()
		            ->update('ksl_gallery_image_tbl', ['gal_image_order' => $ing++], 'gal_image_id = '.$one_img_id)
		            ->execute();					
			}
		}
		
	}
}
