<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ksl_page_contents".
 *
 * @property string $page_content_id
 * @property string $gallery_id
 * @property string $page_content_title
 * @property string $page_name
 * @property string $page_content_top_main_heading
 * @property string $page_content_top_sub_heading
 * @property string $page_content_heading
 * @property string $page_main_content
 * @property string $page_content_img
 * @property string $page_content_created_at
 * @property string $page_content_modified_at
 * @property string $page_content_status
 * @property string $page_meta_name
 * @property string $page_meta_content
 */
class PageContents extends \yii\db\ActiveRecord
{


    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ksl_page_contents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_content_title', 'page_name', 'page_content_top_main_heading', 'page_content_top_sub_heading'], 'required'],//, 'page_main_content'
            // [['gallery_id'], 'integer'],
            // [['page_content_top_sub_heading', 'page_main_content', 'page_content_status', 'page_meta_name', 'page_meta_content'], 'string'],
            // [['page_content_created_at', 'page_content_modified_at'], 'safe'],
            // [['page_content_title'], 'string', 'max' => 250],
            // [['page_name', 'page_content_img'], 'string', 'max' => 350],
            // [['page_content_top_main_heading'], 'string', 'max' => 1000],
            // [['page_content_heading'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'page_content_id' => 'Page Content ID',
            'page_content_title' => 'TITLE',
            'page_name' => 'PAGE NAME',
            'page_content_heading' => 'CONTENT HEADING',
            'page_main_content' => 'MAIN CONTENT',
            'page_content_img' => 'BREADCRUMB IMAGE',
            'page_content_portfolio_img' => 'Content Banner Image',
            'remove_content_img'=>'Remove Page Content Banner Image',
            'page_content_top_main_heading' => 'BREADCRUMB MAIN TITLE',
            'page_content_top_sub_heading' => 'BREADCRUMB SUB TITLE',
            'page_content_created_at' => 'Page Content Created At',
            'page_content_modified_at' => 'Page Content Modified At',
            'page_content_status' => 'Page Content Status',
            'page_meta_name'=>'Meta',
            'page_meta_content'=>'Content'
            
        ];
    }


      /**
     * @this function will work before save() has to call
     */
    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->page_content_created_at = date("Y-m-d H:i:s");
        }
        if (parent::beforeSave($insert)) {
            // Place your custom code here
           return true;
        } else {
            return false;
        }
    }
    
    
    /**
     * @this function will work after getting the data from db and changes view commenly
     */
     public function afterFind() {
       
        //$this->page_content_created_at = date('d-m-Y H:i:s', strtotime($this->page_content_created_at));
      
       
        parent::afterFind();
        
    }
}
