<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "home_notification".
 *
 * @property string $id
 * @property string $notification_name
 * @property string $notification_url
 * @property string $created_date
 */
class HomeNotification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'home_notification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notification_name', 'notification_url'], 'required'],
            [['created_date'], 'safe'],
            [['order_no'], 'integer'],
           // [['notification_name', 'notification_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'notification_name' => 'Notification Name',
            'notification_url' => 'Notification Url',
            'created_date' => 'Created Date',
        ];
    }
}
