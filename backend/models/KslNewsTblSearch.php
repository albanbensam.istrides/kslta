<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\KslNewsTbl;

/**
 * KslNewsTblSearch represents the model behind the search form about `backend\models\KslNewsTbl`.
 */
class KslNewsTblSearch extends KslNewsTbl {

    public $globalSearch;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['news_id'], 'integer'],
            [['news_title', 'globalSearch', 'news_display_tag', 'news_link_address', 'news_link_status', 'news_content_image', 'news_editor_content', 'news_posted_by', 'news_posted_at', 'news_located_city', 'news_located_country', 'news_from_date', 'news_to_date', 'news_created_date', 'news_lastupdated_date', 'news_flat'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     * Below search for Active list view
     */
    public function search($params) {
        $query = KslNewsTbl::find();
        $query->where('news_link_status ="A"');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['news_id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        /* $query->andFilterWhere([
            'news_id' => $this->news_id,
            'news_from_date' => $this->news_from_date,
            'news_to_date' => $this->news_to_date,
            'news_created_date' => $this->news_created_date,
            'news_lastupdated_date' => $this->news_lastupdated_date,
        ]); */

        $query->orFilterWhere(['like', 'news_title', $this->globalSearch])
                ->orFilterWhere(['like', 'news_display_tag', $this->globalSearch])
                ->orFilterWhere(['like', 'news_posted_by', $this->globalSearch]);
                
        return $dataProvider;
    }
    
    
    //Below search for unapproved list view
     public function unapprovedsearch($params) {
        $query = KslNewsTbl::find();
        $query->where('news_link_status ="U"');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['news_id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'news_id' => $this->news_id,
            'news_from_date' => $this->news_from_date,
            'news_to_date' => $this->news_to_date,
            'news_created_date' => $this->news_created_date,
            'news_lastupdated_date' => $this->news_lastupdated_date,
        ]);

        $query->andFilterWhere(['like', 'news_title', $this->news_title])
                ->andFilterWhere(['like', 'news_display_tag', $this->news_display_tag])
                ->andFilterWhere(['like', 'news_link_address', $this->news_link_address])
                ->andFilterWhere(['like', 'news_link_status', $this->news_link_status])
                ->andFilterWhere(['like', 'news_content_image', $this->news_content_image])
                ->andFilterWhere(['like', 'news_editor_content', $this->news_editor_content])
                ->andFilterWhere(['like', 'news_posted_by', $this->news_posted_by])
                ->andFilterWhere(['like', 'news_flat', $this->news_flat]);

        return $dataProvider;
    }
    
    
    //Below search for rejected list view
     public function rejectsearch($params) {
        $query = KslNewsTbl::find();
        $query->where('news_link_status ="R"');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['news_id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'news_id' => $this->news_id,
            'news_from_date' => $this->news_from_date,
            'news_to_date' => $this->news_to_date,
            'news_created_date' => $this->news_created_date,
            'news_lastupdated_date' => $this->news_lastupdated_date,
        ]);

        $query->andFilterWhere(['like', 'news_title', $this->news_title])
                ->andFilterWhere(['like', 'news_display_tag', $this->news_display_tag])
                ->andFilterWhere(['like', 'news_link_address', $this->news_link_address])
                ->andFilterWhere(['like', 'news_link_status', $this->news_link_status])
                ->andFilterWhere(['like', 'news_content_image', $this->news_content_image])
                ->andFilterWhere(['like', 'news_editor_content', $this->news_editor_content])
                ->andFilterWhere(['like', 'news_posted_by', $this->news_posted_by])
                ->andFilterWhere(['like', 'news_flat', $this->news_flat]);

        return $dataProvider;
    }



    public function tournamentsearch($params) {
        $query = KslNewsTbl::find();
        $query->where('news_tournament_id ='.$params['id']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['news_id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
     
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        /* $query->andFilterWhere([
            'news_id' => $this->news_id,
            'news_from_date' => $this->news_from_date,
            'news_to_date' => $this->news_to_date,
            'news_created_date' => $this->news_created_date,
            'news_lastupdated_date' => $this->news_lastupdated_date,
        ]); */

        $query->orFilterWhere(['like', 'news_title', $this->globalSearch])

                ->orFilterWhere(['like', 'news_display_tag', $this->globalSearch])
                ->orFilterWhere(['like', 'news_posted_by', $this->globalSearch]);
                
        return $dataProvider;
    }
public function tournamentsearchfrontend($params,$id) {
        $query = KslNewsTbl::find();
        $query->where('news_tournament_id ='.$id);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['news_id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
     
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        /* $query->andFilterWhere([
            'news_id' => $this->news_id,
            'news_from_date' => $this->news_from_date,
            'news_to_date' => $this->news_to_date,
            'news_created_date' => $this->news_created_date,
            'news_lastupdated_date' => $this->news_lastupdated_date,
        ]); */

        $query->orFilterWhere(['like', 'news_title', $this->globalSearch])

                ->orFilterWhere(['like', 'news_display_tag', $this->globalSearch])
                ->orFilterWhere(['like', 'news_posted_by', $this->globalSearch]);
                
        return $dataProvider;
    }

}
