<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ksl_images_view".
 *
 * @property string $image_id
 * @property string $image_visible_page
 * @property string $image_title
 * @property string $image_over_title
 * @property string $image_top_title
 * @property string $image_sub_title
 * @property string $image_file_location
 * @property string $image_file_name
 * @property string $image_type
 * @property string $image_visible_status
 * @property string $image_modified_at
 * @property string $image_delete_status
 */
class ImagesView extends \yii\db\ActiveRecord
{


    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ksl_images_view';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image_file_location'], 'required'],
            // [['image_sub_title', 'image_file_location', 'image_visible_status', 'image_delete_status'], 'string'],
            // [['image_modified_at'], 'safe'],
            // [['image_visible_page', 'image_over_title'], 'string', 'max' => 500],
            // [['image_title', 'image_top_title', 'image_file_name'], 'string', 'max' => 1000],
            // [['image_type'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'image_id' => 'Image ID',
            'image_visible_page' => 'Visbile Page Name',
            'image_title' => 'Image Title',
            'image_over_title' => 'Image Over Title',
            'image_top_title' => 'Image Top Title',
            'image_sub_title' => 'Image Sub Title',
            'image_file_location' => 'IMAGE UPLOAD',
            'image_file_name' => 'Image File Name',
            'image_type' => 'Image Type',
            'image_visible_status' => 'Image Visible Status',
            'image_modified_at' => 'Image Modified At',
            'image_delete_status' => 'Image Delete Status',
        ];
    }

     /**
     * @inheritdoc
     */
    public function beforeSave($insert) {

        if ($this->getIsNewRecord()) {
            
        }
     //  $this->reg_created_at = date("Y-m-d H:i:s");
        if (parent::beforeSave($insert)) {
            // Place your custom code here
           return true;
        } else {
            return false;
        }
    }
    
    
     public function afterFind() {
       // $this->image_modified_at = date('d-m-Y', strtotime($this->image_modified_at));
        parent::afterFind();
        
    }
}
