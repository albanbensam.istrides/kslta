<?php

namespace backend\models;

use Yii;
use backend\models\KslTournamentsTbl;

/**
 * This is the model class for table "ksl_player_register".
 *
 * @property integer $reg_id
 * @property integer $tourn_id
 * @property string $reg_first_name
 * @property string $reg_last_name
 * @property string $reg_dob
 * @property string $reg_email
 * @property integer $reg_player_category
 * @property string $reg_player_id
 * @property string $reg_city
 * @property string $reg_mobile
 * @property string $registration_date
 * @property string $reg_created_at
 * @property string $reg_last_modified_at
 * @property string $reg_status
 */
class PlayerRegister extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */public $tourn_event_id;
    public static function tableName()
    {
        return 'ksl_player_register';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'reg_first_name', 'reg_last_name', 'reg_player_category', 'reg_city','reg_gender'], 'required'],
            ['reg_email', 'email'],
            [['tourn_id', 'reg_player_category'], 'integer'],
            [['reg_dob', 'registration_date', 'reg_created_at', 'reg_last_modified_at','reg_gender','tourn_id','tourn_event_id'], 'safe'],
            [['reg_status'], 'string'],
            [['reg_first_name', 'reg_last_name', 'reg_email', 'reg_city'], 'string', 'max' => 70],
            [['reg_mobile'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'reg_id' => 'Reg ID',
            'tourn_id' => 'Tournament Title',
            'reg_first_name' => 'First Name',
            'reg_last_name' => 'Last Name',
            'reg_dob' => 'Date of birth',
            'reg_email' => 'Email id',
            'reg_player_category' => 'Player Category',
            'reg_player_id' => 'Player ID',
            'reg_city' => 'City',
            'reg_mobile' => 'Mobile',
            'registration_date' => 'Registration Date',
            'reg_created_at' => 'Reg Created At',
            'reg_last_modified_at' => 'Reg Last Modified At',
            'reg_status' => 'Reg Status',
            'tourn_event_id'=>'Event List',
			'reg_gender'=>'Gender'
        ];
    }


    public function getParent(){
        return $this->hasOne(KslTournamentsTbl::className(), ['tourn_id' => 'tourn_id']);
    }

    public function getParentName(){
        $model=$this->parent;
        return $model?$model->tourn_title:'';
    }


    /**
     * @inheritdoc
     */
    public function beforeSave($insert) {

        if ($this->getIsNewRecord()) {
            $this->registration_date = date("Y-m-d");
            $this->reg_created_at = date("Y-m-d H:i:s");
        }
        $this->reg_dob = date('Y-m-d', strtotime($this->reg_dob));
        if (parent::beforeSave($insert)) {
            // Place your custom code here
           return true;
        } else {
            return false;
        }
    }
    
    
     public function afterFind() {
       //  $this->reg_dob = date('d-m-Y', strtotime($this->reg_dob));
       // $this->registration_date = date('d-m-Y', strtotime($this->registration_date));
        
       
        parent::afterFind();
        
    }
}
