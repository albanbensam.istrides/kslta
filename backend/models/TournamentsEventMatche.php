<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ksl_tournaments_event_matche".
 *
 * @property string $tem_id
 * @property string $tem_tournament_id
 * @property string $tem_event_id
 * @property string $tem_player_id
 * @property string $tem_location
 * @property string $tem_scheduled_time
 * @property string $tem_scheduled_date
 * @property string $tem_timestamp
 */
class TournamentsEventMatche extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ksl_tournaments_event_matche';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tem_tournament_id', 'tem_event_id', 'tem_player_id'], 'required'], //, 'tem_location', 'tem_scheduled_time', 'tem_scheduled_date'
            [['tem_scheduled_date', 'tem_timestamp'], 'safe'],
            [['tem_tournament_id', 'tem_event_id', 'tem_player_id'], 'string', 'max' => 50],
            [['tem_location', 'tem_scheduled_time'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tem_id' => 'Tem ID',
            'tem_tournament_id' => 'Tem Tournament ID',
            'tem_event_id' => 'Tem Event ID',
            'tem_player_id' => 'Tem Player ID',
            'tem_location' => 'Tem Location',
            'tem_scheduled_time' => 'Tem Scheduled Time',
            'tem_scheduled_date' => 'Tem Scheduled Date',
            'tem_timestamp' => 'Tem Timestamp',
        ];
    }
}
