<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Tournamentsmodule;

/**
 * TournamentsmoduleSearch represents the model behind the search form about `backend\models\Tournamentsmodule`.
 */
class TournamentsmoduleSearch extends Tournamentsmodule
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tourn_id'], 'integer'],
            [['tourn_title', 'tourn_category', 'tourn_sub_category', 'tourn_venue', 'tourn_venue_address1', 'tourn_venue_address2', 'tourn_venue_city', 'tourn_from_date', 'tourn_to_date', 'tourn_last_submission_date', 'tourn_withdrl_date', 'tourn_description', 'tourn_asso_title', 'tourn_asso_address1', 'tourn_asso_address2', 'tourn_asso_head', 'tourn_refree1', 'tourn_refree2', 'tourn_refree3', 'tourn_link', 'tourn_created_date', 'tourn_lastmodified_date', 'tourn_status', 'sponser_name_of_organisation', 'sponser_address1', 'sponser_address2', 'sponser_contact_person_name', 'sponser_phone', 'sponser_email', 'tour_event_category', 'tour_event_subcategory'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tournamentsmodule::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'tourn_id' => $this->tourn_id,
            'tourn_from_date' => $this->tourn_from_date,
            'tourn_to_date' => $this->tourn_to_date,
            'tourn_last_submission_date' => $this->tourn_last_submission_date,
            'tourn_withdrl_date' => $this->tourn_withdrl_date,
            'tourn_created_date' => $this->tourn_created_date,
            'tourn_lastmodified_date' => $this->tourn_lastmodified_date,
        ]);

        $query->andFilterWhere(['like', 'tourn_title', $this->tourn_title])
            ->andFilterWhere(['like', 'tourn_category', $this->tourn_category])
            ->andFilterWhere(['like', 'tourn_sub_category', $this->tourn_sub_category])
            ->andFilterWhere(['like', 'tourn_venue', $this->tourn_venue])
            ->andFilterWhere(['like', 'tourn_venue_address1', $this->tourn_venue_address1])
            ->andFilterWhere(['like', 'tourn_venue_address2', $this->tourn_venue_address2])
            ->andFilterWhere(['like', 'tourn_venue_city', $this->tourn_venue_city])
            ->andFilterWhere(['like', 'tourn_description', $this->tourn_description])
            ->andFilterWhere(['like', 'tourn_asso_title', $this->tourn_asso_title])
            ->andFilterWhere(['like', 'tourn_asso_address1', $this->tourn_asso_address1])
            ->andFilterWhere(['like', 'tourn_asso_address2', $this->tourn_asso_address2])
            ->andFilterWhere(['like', 'tourn_asso_head', $this->tourn_asso_head])
            ->andFilterWhere(['like', 'tourn_refree1', $this->tourn_refree1])
            ->andFilterWhere(['like', 'tourn_refree2', $this->tourn_refree2])
            ->andFilterWhere(['like', 'tourn_refree3', $this->tourn_refree3])
            ->andFilterWhere(['like', 'tourn_link', $this->tourn_link])
            ->andFilterWhere(['like', 'tourn_status', $this->tourn_status])
            ->andFilterWhere(['like', 'sponser_name_of_organisation', $this->sponser_name_of_organisation])
            ->andFilterWhere(['like', 'sponser_address1', $this->sponser_address1])
            ->andFilterWhere(['like', 'sponser_address2', $this->sponser_address2])
            ->andFilterWhere(['like', 'sponser_contact_person_name', $this->sponser_contact_person_name])
            ->andFilterWhere(['like', 'sponser_phone', $this->sponser_phone])
            ->andFilterWhere(['like', 'sponser_email', $this->sponser_email])
            ->andFilterWhere(['like', 'tour_event_category', $this->tour_event_category])
            ->andFilterWhere(['like', 'tour_event_subcategory', $this->tour_event_subcategory]);

        return $dataProvider;
    }
}
