<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\NewsletterOnline;

/**
 * NewsletterOnlineSearch represents the model behind the search form about `backend\models\NewsletterOnline`.
 */
class NewsletterOnlineSearch extends NewsletterOnline
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['newsletter_id'], 'integer'],
            [['newsletter_name', 'newsletter_email', 'newsletter_mobile', 'newsletter_email_view_status', 'newsletter_created_at', 'newsletter_modified_at', 'newsletter_active_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NewsletterOnline::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'newsletter_id' => $this->newsletter_id,
            'newsletter_created_at' => $this->newsletter_created_at,
            'newsletter_modified_at' => $this->newsletter_modified_at,
        ]);

        $query->andFilterWhere(['like', 'newsletter_name', $this->newsletter_name])
            ->andFilterWhere(['like', 'newsletter_email', $this->newsletter_email])
            ->andFilterWhere(['like', 'newsletter_mobile', $this->newsletter_mobile])
            ->andFilterWhere(['like', 'newsletter_email_view_status', $this->newsletter_email_view_status])
            ->andFilterWhere(['like', 'newsletter_active_status', $this->newsletter_active_status]);

        return $dataProvider;
    }
}
