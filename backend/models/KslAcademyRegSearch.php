<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\KslAcademyReg;

/**
 * KslAcademyRegSearch represents the model behind the search form about `backend\models\KslAcademyReg`.
 */
class KslAcademyRegSearch extends KslAcademyReg
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['academy_name', 'owner', 'contact_number', 'email_id', 'address', 'no_of_courts_clay', 'no_of_courts_hard', 'no_of_courts_others', 'no_of_courts_flood', 'no_of_coaches', 'office', 'pro_shop', 'cafeteria', 'toilet', 'room', 'gym', 'warm_up', 'water', 'other_information', 'remarks', 'active_status', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KslAcademyReg::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'academy_name', $this->academy_name])
            ->andFilterWhere(['like', 'owner', $this->owner])
            ->andFilterWhere(['like', 'contact_number', $this->contact_number])
            ->andFilterWhere(['like', 'email_id', $this->email_id])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'no_of_courts_clay', $this->no_of_courts_clay])
            ->andFilterWhere(['like', 'no_of_courts_hard', $this->no_of_courts_hard])
            ->andFilterWhere(['like', 'no_of_courts_others', $this->no_of_courts_others])
            ->andFilterWhere(['like', 'no_of_courts_flood', $this->no_of_courts_flood])
            ->andFilterWhere(['like', 'no_of_coaches', $this->no_of_coaches])
            ->andFilterWhere(['like', 'office', $this->office])
            ->andFilterWhere(['like', 'pro_shop', $this->pro_shop])
            ->andFilterWhere(['like', 'cafeteria', $this->cafeteria])
            ->andFilterWhere(['like', 'toilet', $this->toilet])
            ->andFilterWhere(['like', 'room', $this->room])
            ->andFilterWhere(['like', 'gym', $this->gym])
            ->andFilterWhere(['like', 'warm_up', $this->warm_up])
            ->andFilterWhere(['like', 'water', $this->water])
            ->andFilterWhere(['like', 'other_information', $this->other_information])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'active_status', $this->active_status]);

        return $dataProvider;
    }
}
