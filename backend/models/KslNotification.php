<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ksl_notification".
 *
 * @property integer $notify_id
 * @property string $notify_text
 * @property string $notify_link
 * @property string $notify_color
 * @property string $notify_status
 * @property string $notify_timestamp
 */
class KslNotification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ksl_notification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notify_text', 'notify_link', 'notify_color', 'notify_status'], 'required'],
            [['notify_text', 'notify_link', 'notify_color', 'notify_status'], 'string'],
            [['notify_timestamp'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'notify_id' => 'Notify ID',
            'notify_text' => 'Notify Text',
            'notify_link' => 'Notify Link',
            'notify_color' => 'Notify Color',
            'notify_status' => 'Notify Status',
            'notify_timestamp' => 'Notify Timestamp',
        ];
    }
}
