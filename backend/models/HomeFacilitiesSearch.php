<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\HomeFacilities;

/**
 * HomeFacilitiesSearch represents the model behind the search form about `backend\models\HomeFacilities`.
 */
class HomeFacilitiesSearch extends HomeFacilities
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_no'], 'integer'],
            [['facility_name', 'facility_url', 'created_at','facility_content','order_no'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HomeFacilities::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'order_no' => $this->order_no,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'facility_name', $this->facility_name])
            ->andFilterWhere(['like', 'facility_url', $this->facility_url])
			->andFilterWhere(['like', 'facility_content', $this->facility_content])
			->andFilterWhere(['like', 'order_no', $this->order_no]);

        return $dataProvider;
    }
}
