<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PlayerEvents;

/**
 * PlayerEventsSearch represents the model behind the search form about `backend\models\PlayerEvents`.
 */
class PlayerEventsSearch extends PlayerEvents
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ue_autoid'], 'integer'],
            [['ue_userid', 'ue_tournamentid', 'ue_eventid', 'ue_created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PlayerEvents::find()->groupBy(['ue_eventid'])->select('count(*) as `ue_userid`,`ue_eventid`,`ue_tournamentid`');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ue_autoid' => $this->ue_autoid,
            'ue_created' => $this->ue_created,
        ]);

        $query->andFilterWhere(['like', 'ue_userid', $this->ue_userid])
            ->andFilterWhere(['like', 'ue_tournamentid', $this->ue_tournamentid])
            ->andFilterWhere(['like', 'ue_eventid', $this->ue_eventid]);

        return $dataProvider;
    }
}
