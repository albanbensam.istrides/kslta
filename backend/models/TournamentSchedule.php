<?php

namespace backend\models;
use backend\models\TournamentEventModel;
use Yii;

/**
 * This is the model class for table "ksl_tournament_schedule".
 *
 * @property integer $ts_id
 * @property string $ts_tournament_id
 * @property string $ts_event_id
 * @property string $ts_player_id
 * @property string $ts_player_order
 */
class TournamentSchedule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
     public $tornamentschedule_div;
	 public $event_drawsheet;
	 public $drawsheet_tournament_id;
	 public $drawsheet_eventid;
	 public $file;
    public static function tableName()
    {
        return 'ksl_tournament_schedule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ts_tournament_id', 'ts_event_id', 'ts_tournament_result'], 'required'],
           // [['ts_tournament_id', 'ts_event_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ts_id' => 'ID',
            'ts_tournament_id' => 'Tournament',
            'ts_event_id' => 'Event',
            'ts_tournament_result' => 'Result',
        ];
    }
	

}
