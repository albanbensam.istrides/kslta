<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ksl_player_events".
 *
 * @property string $ue_autoid
 * @property string $ue_userid
 * @property string $ue_tournamentid
 * @property string $ue_eventid
 * @property string $ue_created
 */
class PlayerEvents extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ksl_player_events';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ue_userid', 'ue_tournamentid', 'ue_eventid'], 'required'],
            [['ue_created'], 'safe'],
            [['ue_userid', 'ue_tournamentid', 'ue_eventid'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ue_autoid' => 'Ue Autoid',
            'ue_userid' => 'Ue Userid',
            'ue_tournamentid' => 'Ue Tournamentid',
            'ue_eventid' => 'Ue Eventid',
            'ue_created' => 'Ue Created',
        ];
    }
	public function inset_playerevent_data($bulkInsertArray=array())
    {
		if(count($bulkInsertArray)>0){
	    $columnNameArray=['ue_userid','ue_tournamentid','ue_eventid'];
	    // below line insert all your record and return number of rows inserted
	    $insertCount = Yii::$app->db->createCommand()
	                   ->batchInsert(
	                         'ksl_player_events', $columnNameArray, $bulkInsertArray
	                     )
	                   ->execute();
	
		}
	}
	
	public function delete_playerevent_data($deleteid='')
    {
    	if($deleteid!=""){
			$delconnection = Yii::$app->db->createCommand()
	            ->delete('ksl_player_events', 'ue_autoid ='.$deleteid)
	            ->execute();
    	}
	}
}
