<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tournament_schedule".
 *
 * @property string $id
 * @property string $tour_name
 * @property string $created_at
 */
class Tournament extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tournament_schedule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          //  [['tour_name'], 'required'],
            [['tour_name','created_at'], 'safe'],
             [['order_no'], 'integer'],
            //[['tour_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tour_name' => 'Tournament Name',
            'created_at' => 'Created At',
        ];
    }
}
