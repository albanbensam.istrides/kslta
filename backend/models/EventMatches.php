<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ksl_event_matches".
 *
 * @property integer $em_id
 * @property string $em_event_id
 * @property string $em_no_of_matches
 * @property string $em_created
 */
class EventMatches extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ksl_event_matches';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['em_event_id', 'em_no_of_matches'], 'required'],
            [['em_no_of_matches'], 'integer'],
            [['em_created'], 'safe'],
            [['em_event_id'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'em_id' => 'Em ID',
            'em_event_id' => 'Em Event ID',
            'em_no_of_matches' => 'Em No Of Matches',
            'em_created' => 'Em Created',
        ];
    }
}
