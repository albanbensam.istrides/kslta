<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TournamentSchedule;

/**
 * TournamentScheduleSearch represents the model behind the search form about `backend\models\TournamentSchedule`.
 */
class TournamentScheduleSearch extends TournamentSchedule
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ts_id', 'ts_tournament_id', 'ts_event_id'], 'integer'],
            [['ts_tournament_result', 'ts_date_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TournamentSchedule::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ts_id' => $this->ts_id,
            'ts_tournament_id' => $this->ts_tournament_id,
            'ts_event_id' => $this->ts_event_id,
            'ts_date_time' => $this->ts_date_time,
        ]);

        $query->andFilterWhere(['like', 'ts_tournament_result', $this->ts_tournament_result]);

        return $dataProvider;
    }
}
