<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PageContents;

/**
 * PageContentsSearch represents the model behind the search form about `backend\models\PageContents`.
 */
class PageContentsSearch extends PageContents
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_content_id'], 'integer'],
            [['page_content_title', 'page_name', 'page_content_heading', 'page_main_content', 'page_content_img', 'page_content_created_at', 'page_content_modified_at', 'page_content_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PageContents::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['page_content_id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'page_content_id' => $this->page_content_id,
            'page_content_created_at' => $this->page_content_created_at,
            'page_content_modified_at' => $this->page_content_modified_at,
        ]);

        $query->andFilterWhere(['like', 'page_content_title', $this->page_content_title])
            ->andFilterWhere(['like', 'page_name', $this->page_name])
            ->andFilterWhere(['like', 'page_content_heading', $this->page_content_heading])
            ->andFilterWhere(['like', 'page_main_content', $this->page_main_content])
            ->andFilterWhere(['like', 'page_content_img', $this->page_content_img])
            ->andFilterWhere(['like', 'page_content_status', $this->page_content_status]);

        return $dataProvider;
    }
}
