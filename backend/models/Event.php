<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ksl_event".
 *
 * @property string $id
 * @property string $event_name
 * @property string $event_url
 * @property string $event_img
 * @property string $created_at
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public static function tableName()
    {
        return 'ksl_event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_name', 'event_url','event_title', 'event_header'], 'required'],
            [['created_at'], 'safe'],
             [['order_no'], 'integer'],
          //  [['event_name', 'event_url'], 'string', 'max' => 255],
          //  [['event_img'], 'string', 'max' => 100],
            [['event_img'], 'file',  'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_name' => 'Event Name',
            'event_url' => 'Event Url',
            'event_img' => 'Event Img',
            'created_at' => 'Created At',
        ];
    }
}
