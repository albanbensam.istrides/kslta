<?php

namespace backend\models;

use yii\web\UploadedFile;
use Yii;

/**
 * This is the model class for table "filesupload".
 *
 * @property integer $id
 * @property string $filename
 * @property string $filelink
 * @property string $created_at
 * @property string $updated_at
 */
class Filesupload extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
     
     public $file;
	  public $imageFile;
    public static function tableName()
    {
        return 'filesupload';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['filename', 'filelink', 'created_at'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['filename'], 'string', 'max' => 100],
            [['filelink'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'filename' => 'File name',
            'filelink' => 'File link',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    public function upload() {
       if ($this->validate()) {
          $this->image->saveAs(Url::to('@web/backend/web/content_images/'). $this->image->baseName . '.' . $this->image->extension);
		  //$this->news_content_image->saveAs('http://192.168.1.52/2016/ksltacms/backend/web/content_images/' . $this->news_content_image->baseName . '.' . $this->news_content_image->extension);
           return true;
       } else {
           return false;
       }
   }
}
