<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ksl_event_tbl".
 *
 * @property string $event_id
 * @property string $tourn_id
 * @property string $event_category
 * @property string $event_subcategory
 */
class TournamentEventModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ksl_event_tbl';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tourn_id', 'event_category', 'event_subcategory'], 'required'],
            [['tourn_id', 'event_category', 'event_subcategory'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'event_id' => 'Event ID',
            'tourn_id' => 'Tourn ID',
            'event_category' => 'Event Category',
            'event_subcategory' => 'Event Subcategory',
        ];
    }
	public function upload_event_data($bulkInsertArray=array())
    {
		if(count($bulkInsertArray)>0){
	    $columnNameArray=['tourn_id','event_category','event_subcategory'];
	    // below line insert all your record and return number of rows inserted
	    $insertCount = Yii::$app->db->createCommand()
	                   ->batchInsert(
	                         'ksl_event_tbl', $columnNameArray, $bulkInsertArray
	                     )
	                   ->execute();
	
		}
	}
	public function update_event_data($tourm_id,$event_list=array())
    {
		if(count($event_list)>0){
			Yii::$app->db->createCommand()
		            ->delete('ksl_event_tbl', 'tourn_id = '.$tourm_id)
		            ->execute();
			$this->upload_event_data($event_list);			
		}
	}
}
