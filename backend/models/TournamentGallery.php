<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ksl_tournament_gallery".
 *
 * @property string $tourn_gallery_id
 * @property string $tourn_id
 * @property string $gallery_id
 * @property string $tourn_gallery_date
 * @property string $tourn_gallery_created_at
 * @property string $tourn_gallery_modified_at
 * @property string $tourn_gallery_status
 */
class TournamentGallery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ksl_tournament_gallery';
    }

    public $tournTitle;
    public $gallerymultiple;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gallery_id'], 'required'],
            [['tourn_id', 'gallery_id'], 'integer'],
            [['tourn_gallery_date', 'tourn_gallery_created_at', 'tourn_gallery_modified_at'], 'safe'],
            [['tourn_gallery_status'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tourn_gallery_id' => 'Tourn Gallery ID',
            'tourn_id' => 'Tournament Name',
            'gallery_id' => 'Gallery Name',
            'tourn_gallery_date' => 'Gallery Date',
            'tourn_gallery_created_at' => 'Tourn Gallery Created At',
            'tourn_gallery_modified_at' => 'Tourn Gallery Modified At',
            'tourn_gallery_status' => 'Tourn Gallery Status',
        ];
    }


     /**
     * @inheritdoc
     */
    public function beforeSave($insert) {

        if ($this->isNewRecord) {
            $this->tourn_gallery_created_at = date("Y-m-d H:i:s");
        }
        $this->tourn_gallery_date = date("Y-m-d");
       

        if (parent::beforeSave($insert)) {
            // Place your custom code here

            return true;
        } else {
            return false;
        }
    }
}
