<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "homepage_news".
 *
 * @property string $id
 * @property string $news
 * @property string $created_by
 * @property string $sort_order
 * @property string $created_at
 */
class HomepageNews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'homepage_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'news'], 'required'],
            [['id', 'sort_order'], 'integer'],
            [['created_at','sort_order','id', 'news', 'created_by'], 'safe'],
            [['news', 'created_by'], 'string', 'max' => 2000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'news' => 'News',
            'created_by' => 'Created By',
            'sort_order' => 'Sort Order',
            'created_at' => 'Created At',
        ];
    }
}
