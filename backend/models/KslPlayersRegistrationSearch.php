<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\KslPlayersRegistration;

/**
 * KslPlayersRegistrationSearch represents the model behind the search form about `backend\models\KslPlayersRegistration`.
 */
class KslPlayersRegistrationSearch extends KslPlayersRegistration
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reg_id', 'tourn_id', 'player_category', 'mobile'], 'integer'],
            [['player_name', 'register_id', 'city', 'registration_date', 'tournament_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KslPlayersRegistration::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'reg_id' => $this->reg_id,
            'tourn_id' => $this->tourn_id,
            'player_category' => $this->player_category,
            'mobile' => $this->mobile,
            'registration_date' => $this->registration_date,
            'tournament_date' => $this->tournament_date,
        ]);

        $query->andFilterWhere(['like', 'player_name', $this->player_name])
            ->andFilterWhere(['like', 'register_id', $this->register_id])
            ->andFilterWhere(['like', 'city', $this->city]);

        return $dataProvider;
    }
}
