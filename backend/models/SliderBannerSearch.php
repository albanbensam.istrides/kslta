<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\SliderBanner;

/**
 * SliderBannerSearch represents the model behind the search form about `backend\models\SliderBanner`.
 */
class SliderBannerSearch extends SliderBanner
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['banner_id', 'banner_order_flow'], 'integer'],
            [['banner_name', 'banner_url', 'banner_image', 'banner_date', 'banner_created_at', 'banner_modified_at', 'banner_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SliderBanner::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['banner_id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'banner_id' => $this->banner_id,
            'banner_order_flow' => $this->banner_order_flow,
            'banner_date' => $this->banner_date,
            'banner_created_at' => $this->banner_created_at,
            'banner_modified_at' => $this->banner_modified_at,
        ]);

        $query->andFilterWhere(['like', 'banner_name', $this->banner_name])
            ->andFilterWhere(['like', 'banner_url', $this->banner_url])
            ->andFilterWhere(['like', 'banner_image', $this->banner_image])
            ->andFilterWhere(['like', 'banner_status', $this->banner_status]);

        return $dataProvider;
    }
}
