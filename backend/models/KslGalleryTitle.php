<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ksl_gallery_title".
 *
 * @property integer $tit_id
 * @property string $tit_value
 * @property string $tit_timestamp
 */
class KslGalleryTitle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ksl_gallery_title';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          //  [['tit_value'], 'required'],
            [['tit_value'], 'string'],
            [['tit_timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tit_id' => 'Tit ID',
            'tit_value' => 'Gallery Title',
            'tit_timestamp' => 'Tit Timestamp',
        ];
    }
}
