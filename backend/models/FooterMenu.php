<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "footer_menu".
 *
 * @property string $id
 * @property string $menu_name
 * @property string $menu_link
 * @property string $order_number
 * @property integer $created
 */
class FooterMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'footer_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_name', 'menu_link', 'order_number'], 'required'],
            [['order_number', 'created'], 'safe'],
             [['order_number'], 'integer'],
            //[['menu_name', 'menu_link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_name' => 'Menu Name',
            'menu_link' => 'Menu Link',
            'order_number' => 'Order Number',
            'created' => 'Created',
        ];
    }
}
