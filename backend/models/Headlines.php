<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "headlines".
 *
 * @property string $id
 * @property string $headlines_name
 * @property string $headline_link
 * @property string $created_date
 */
class Headlines extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'headlines';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'headlines_name', 'headline_link'], 'required'],
            [['id','order_no'], 'integer'],
            [['created_date'], 'safe'],
            //[['headlines_name'], 'string', 'max' => 255],
           // [['headline_link'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'headlines_name' => 'Headlines Name',
            'headline_link' => 'Headline Link',
            'created_date' => 'Created Date',
        ];
    }
}
