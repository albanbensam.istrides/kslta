<?php

namespace backend\models;

use Yii;
use yii\helpers\Url;
/**
 * This is the model class for table "ksl_news_tbl".
 *
 * @property integer $news_id
 * @property string $news_title
 * @property string $news_display_tag
 * @property string $news_link_address
 * @property string $news_link_status
 * @property string $news_content_image
 * @property string $news_editor_content
 * @property string $news_posted_by
 * @property string $news_from_date
 * @property string $news_to_date
 * @property string $news_created_date
 * @property string $news_lastupdated_date
 * @property string $news_flat
 */
class KslNewsModel extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'ksl_news_tbl';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['news_title', 'news_display_tag', 'news_link_address', 'news_posted_by'], 'required'],
            //[['news_content_image'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
            [['news_link_status', 'news_editor_content', 'news_flat'], 'string'],
            [['news_from_date', 'news_to_date', 'news_created_date', 'news_lastupdated_date'], 'safe'],
            [['news_title', 'news_link_address'], 'string', 'max' => 500],
            [['news_display_tag'], 'string', 'max' => 100],
            [['news_content_image'], 'string', 'max' => 250],
            [['news_posted_by'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'news_id' => 'News ID',
            'news_title' => 'News Title',
            'news_display_tag' => 'Display Tag',
            'news_link_address' => 'Link Address',
            'news_link_status' => 'Link Status',
            'news_content_image' => 'Content Image',
            'news_editor_content' => 'Editor Content',
            'news_posted_by' => 'Posted By',
            'news_from_date' => 'News From Date',
            'news_to_date' => 'News To Date',
            'news_created_date' => 'News Created Date',
            'news_lastupdated_date' => 'News Lastupdated Date',
            'news_flat' => 'News Flat',
        ];
    }

     /**
     * @inheritdoc
     */
    public function beforeSave($insert) {

        if ($this->isNewRecord) {
            $this->news_created_date = date("Y-m-d H:i:s");
            $this->news_from_date = date("Y-m-d");
        }
        $this->news_posted_at = date("Y-m-d");
        $this->news_to_date = date("Y-m-d", strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . "+1 month"));

        if (parent::beforeSave($insert)) {
            // Place your custom code here

            return true;
        } else {
            return false;
        }
    }

}
