<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TournamentPlayerList;

/**
 * TournamentPlayerListSearch represents the model behind the search form about `backend\models\TournamentPlayerList`.
 */
class TournamentPlayerListSearch extends TournamentPlayerList
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['playerlists_id', 'tourn_id', 'event_id'], 'integer'],
            [['playerlists_filename', 'playerlists_filepath', 'playerlists_created_at', 'playerlists_modified_at', 'playerlists_active_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TournamentPlayerList::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'playerlists_id' => $this->playerlists_id,
            'tourn_id' => $this->tourn_id,
            'event_id' => $this->event_id,
            'playerlists_created_at' => $this->playerlists_created_at,
            'playerlists_modified_at' => $this->playerlists_modified_at,
        ]);

        $query->andFilterWhere(['like', 'playerlists_filename', $this->playerlists_filename])
            ->andFilterWhere(['like', 'playerlists_filepath', $this->playerlists_filepath])
            ->andFilterWhere(['like', 'playerlists_active_status', $this->playerlists_active_status]);

        return $dataProvider;
    }
}
