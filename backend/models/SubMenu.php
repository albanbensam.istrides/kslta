<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "main_sub_menu".
 *
 * @property string $sub_menu_id
 * @property string $menu_id
 * @property string $sub_menu_name
 * @property integer $sub_menu_order_no
 * @property string $sub_menu_link
 * @property string $sub_menu_visible_status
 * @property string $sub_menu_created_at
 * @property string $sub_menu_modified_at
 * @property string $sub_menu_status
 */
class SubMenu extends \yii\db\ActiveRecord
{


    public $menuname;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'main_sub_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_id', 'sub_menu_name', 'sub_menu_order_no', 'sub_menu_link'], 'required'],
            [['menu_id', 'sub_menu_order_no'], 'integer'],
            [['sub_menu_link', 'sub_menu_visible_status', 'sub_menu_status'], 'string'],
            [['sub_menu_created_at', 'sub_menu_modified_at'], 'safe'],
            [['sub_menu_name'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sub_menu_id' => 'Sub Menu ID',
            'menu_id' => 'Menu Name',
            'sub_menu_name' => 'Sub Menu Name',
            'sub_menu_order_no' => 'Sub Menu Order No',
            'sub_menu_link' => 'Sub Menu Link',
            'sub_menu_visible_status' => 'Sub Menu Visible Status',
            'sub_menu_created_at' => 'Sub Menu Created At',
            'sub_menu_modified_at' => 'Sub Menu Modified At',
            'sub_menu_status' => 'Sub Menu Status',
            'menuname' => 'Menu Name',
        ];
    }


       /**
     * @this function will work before save() has to call
     */
    public function beforeSave($insert) {

        if ($this->isNewRecord) {
           
        }

         $this->sub_menu_created_at = date("Y-m-d H:i:s");
       
        if (parent::beforeSave($insert)) {
            // Place your custom code here
           return true;
        } else {
            return false;
        }
    }
    
    
    /**
     * @this function will work after getting the data from db and changes view commenly
     */
     public function afterFind() {
        // $this->sub_menu_created_at = date('d-m-Y H:i:s', strtotime($this->sub_menu_created_at));
        $this->menuname = $this->menu->menu_name;
        parent::afterFind();
    }

     /**
     * return Relation with Product tbl
     */
    public function getMenu()
    {
        return $this->hasOne(MainMenu::className(), ['menu_id' => 'menu_id']);
    }



}
