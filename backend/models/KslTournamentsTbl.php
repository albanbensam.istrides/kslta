<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ksl_tournaments_tbl".
 *
 * @property integer $tourn_id
 * @property string $tourn_title
 * @property string $tourn_category
 * @property string $tourn_sub_category
 * @property string $tourn_venue
 * @property string $tourn_venue_address1
 * @property string $tourn_venue_address2
 * @property string $tourn_venue_city
 * @property string $tourn_from_date
 * @property string $tourn_to_date
 * @property string $tourn_last_submission_date
 * @property string $tourn_withdrl_date
 * @property string $tourn_description
 * @property string $tourn_asso_title
 * @property string $tourn_asso_address1
 * @property string $tourn_asso_address2
 * @property string $tourn_asso_head
 * @property string $tourn_refree1
 * @property string $tourn_refree2
 * @property string $tourn_refree3
 * @property string $tourn_link
 * @property string $tourn_created_date
 * @property string $tourn_lastmodified_date
 * @property string $tourn_status
 * @property string $sponser_name_of_organisation
 * @property string $sponser_address1
 * @property string $sponser_address2
 * @property string $sponser_contact_person_name
 * @property string $sponser_phone
 * @property string $sponser_email
 * @property string $tour_event_category
 * @property string $tour_event_subcategory
 */
class KslTournamentsTbl extends \yii\db\ActiveRecord
{



     //custom property for file upload its temporarily save the file name
    public $file;


    /**
     * @inheritdoc
     */
     public $galleryselect;
    public static function tableName()
    {
        return 'ksl_tournaments_tbl';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tourn_title', 'tourn_venue', 'tourn_venue_address1', 'tourn_venue_city', 'tourn_from_date', 'tourn_to_date', 'tourn_last_submission_date', 'tourn_withdrl_date'], 'required'],
            [['tourn_from_date', 'tourn_to_date', 'tourn_last_submission_date', 'tourn_withdrl_date', 'tourn_created_date', 'tourn_lastmodified_date'], 'safe'],
            [['tourn_status'], 'string'],
            [['tourn_title', 'sponser_address1', 'sponser_address2'], 'string', 'max' => 300],
            [['tourn_category', 'tourn_sub_category'], 'string', 'max' => 30],
            [['tourn_venue', 'tourn_venue_city', 'tourn_refree1', 'tourn_refree2', 'tourn_refree3', 'tourn_link', 'sponser_contact_person_name'], 'string', 'max' => 100],
            [['tourn_venue_address1', 'tourn_venue_address2', 'tourn_asso_title', 'tourn_asso_address1', 'tourn_asso_address2', 'sponser_name_of_organisation', 'sponser_email'], 'string', 'max' => 200],
            [['tourn_description'], 'string', 'max' => 1500],
            [['tourn_asso_head'], 'string', 'max' => 70],
            [['sponser_phone', 'tour_event_category', 'tour_event_subcategory'], 'string', 'max' => 20],

            // [['tourn_factsheet'], 'file', 'skipOnEmpty' => false, 'extensions' => 'pdf, doc', 'on'=>'factsheetdata'],
            // [['tourn_factsheet'], 'required', 'on' => 'factsheetdata'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tourn_id' => 'Tourn ID',
            'tourn_title' => 'Tournament  Title',
            'tourn_category' => 'Category',
            'tourn_sub_category' => 'Sub-Category',
            'tourn_venue' => 'Venue',
            'tourn_venue_address1' => 'Tournament Venue Address1',
            'tourn_venue_address2' => 'Tournament Venue Address2',
            'tourn_venue_city' => 'Tournament City',
            'tourn_from_date' => 'From Date',
            'tourn_to_date' => 'To Date',
            'tourn_last_submission_date' => 'Last Submission Date',
            'tourn_withdrl_date' => 'Tournament Withdraw Date',
            'tourn_description' => 'Tournament Description',
            'tourn_asso_title' => 'Association Title',
            'tourn_asso_address1' => 'Association Address Line1',
            'tourn_asso_address2' => 'Association Address Line2',
            'tourn_asso_head' => 'Tournament Association Head',
            'tourn_refree1' => 'Tournament Refree1',
            'tourn_refree2' => 'Tournament Refree2',
            'tourn_refree3' => 'Tournament Refree3',
            'tourn_link' => 'Link Address',
            'tourn_created_date' => 'Tournament Created Date',
            'tourn_lastmodified_date' => 'Tourn Lastmodified Date',
            'tourn_status' => 'Tourn Status',
            'sponser_name_of_organisation' => 'Sponser Name Of Organisation',
            'sponser_address1' => 'Sponser Address1',
            'sponser_address2' => 'Sponser Address2',
            'sponser_contact_person_name' => 'Sponser Contact Person Name',
            'sponser_phone' => 'Contact Number',
            'sponser_email' => 'Sponser Email',
            'tour_event_category' => 'Tournament Event Category',
            'tour_event_subcategory' => 'Tournament Event Subcategory',
            'tourn_price_money' => 'Price Money',
            'tourn_tournament_image' => 'Tournament Image',
        ];
    }
    
    
    
    
     /**
     * @inheritdoc
     */
    public function beforeSave($insert) {

        if ($this->getIsNewRecord()) {
            $this->tourn_created_date = date("Y-m-d H:i:s");
            $this->tourn_lastmodified_date = date("Y-m-d");
            $this->tourn_status = "A";
        }
        $this->tourn_from_date = date('Y-m-d', strtotime($this->tourn_from_date));
        $this->tourn_to_date = date('Y-m-d', strtotime($this->tourn_to_date));
        $this->tourn_last_submission_date = date('Y-m-d', strtotime($this->tourn_last_submission_date));
        $this->tourn_withdrl_date = date('Y-m-d', strtotime($this->tourn_withdrl_date));
        $this->tourn_lastmodified_date = date("Y-m-d");
        $this->tourn_status = "A";
        if (parent::beforeSave($insert)) {
            // Place your custom code here

            return true;
        } else {
            return false;
        }
    }
    
    
     public function afterFind() {
      /*   $this->tourn_from_date = date('d-m-Y', strtotime($this->tourn_from_date));
        $this->tourn_to_date = date('d-m-Y', strtotime($this->tourn_to_date));
        $this->tourn_last_submission_date = date('d-m-Y', strtotime($this->tourn_last_submission_date));
        $this->tourn_withdrl_date = date('d-m-Y', strtotime($this->tourn_withdrl_date));
        $this->tourn_lastmodified_date = date("d-m-Y");*/
       
        parent::afterFind();
        
    }
	/* public function getDatesFromRange($start, $end, $format = 'Y-m-d') {
    $array = array();
    $interval = new \DateInterval('P1D');

    $realEnd = new \DateTime($end);
    $realEnd->add($interval);

    $period = new \DatePeriod(new \DateTime($start), $interval, $realEnd);

    foreach($period as $date) { 
        $array[] = $date->format($format); 
    }

    return $array;
}*/
}
