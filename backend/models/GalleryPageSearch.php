<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\GalleryPageModel;

/**
 * GalleryPageSearch represents the model behind the search form about `backend\models\GalleryPageModel`.
 */
class GalleryPageSearch extends GalleryPageModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gal_image_id', 'gal_image_order'], 'integer'],
            [['gal_image_rootid', 'gal_image_path', 'gal_image_created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GalleryPageModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'gal_image_id' => $this->gal_image_id,
            'gal_image_order' => $this->gal_image_order,
            'gal_image_created' => $this->gal_image_created,
        ]);

        $query->andFilterWhere(['like', 'gal_image_rootid', $this->gal_image_rootid])
            ->andFilterWhere(['like', 'gal_image_path', $this->gal_image_path]);

        return $dataProvider;
    }
}
