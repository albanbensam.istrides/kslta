<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\KslNewsModel;

/**
 * KslNewsModelSearch represents the model behind the search form about `backend\models\KslNewsModel`.
 */
class KslNewsModelSearch extends KslNewsModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_id'], 'integer'],
            [['news_title', 'news_display_tag', 'news_link_address', 'news_link_status', 'news_content_image', 'news_editor_content', 'news_posted_by', 'news_from_date', 'news_to_date', 'news_created_date', 'news_lastupdated_date', 'news_flat'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KslNewsModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['news_id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'news_id' => $this->news_id,
            'news_from_date' => $this->news_from_date,
            'news_to_date' => $this->news_to_date,
            'news_created_date' => $this->news_created_date,
            'news_lastupdated_date' => $this->news_lastupdated_date,
        ]);

        $query->andFilterWhere(['like', 'news_title', $this->news_title])
            ->andFilterWhere(['like', 'news_display_tag', $this->news_display_tag])
            ->andFilterWhere(['like', 'news_link_address', $this->news_link_address])
            ->andFilterWhere(['like', 'news_link_status', $this->news_link_status])
            ->andFilterWhere(['like', 'news_content_image', $this->news_content_image])
            ->andFilterWhere(['like', 'news_editor_content', $this->news_editor_content])
            ->andFilterWhere(['like', 'news_posted_by', $this->news_posted_by])
            ->andFilterWhere(['like', 'news_flat', $this->news_flat]);

        return $dataProvider;
    }
}
