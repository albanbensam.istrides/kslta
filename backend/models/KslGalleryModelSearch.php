<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\KslGalleryModel;

/**
 * KslGalleryModelSearch represents the model behind the search form about `backend\models\KslGalleryModel`.
 */
class KslGalleryModelSearch extends KslGalleryModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gallery_id'], 'integer'],
            [['gallery_title', 'gallery_display_tag', 'gallery_link_address', 'gallery_link_status', 'gallery_content_image', 'gallery_posted_by', 'gallery_picture_courtesy', 'gallery_from_date', 'gallery_to_date', 'gallery_created_date', 'gallery_lastupdated_date', 'gallery_flat'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KslGalleryModel::find()->where('gallery_id != 3');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['gallery_id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'gallery_id' => $this->gallery_id,
            'gallery_from_date' => $this->gallery_from_date,
            'gallery_to_date' => $this->gallery_to_date,
            'gallery_created_date' => $this->gallery_created_date,
            'gallery_lastupdated_date' => $this->gallery_lastupdated_date,
        ]);

        $query->andFilterWhere(['like', 'gallery_title', $this->gallery_title])
            ->andFilterWhere(['like', 'gallery_display_tag', $this->gallery_display_tag])
            ->andFilterWhere(['like', 'gallery_link_address', $this->gallery_link_address])
            ->andFilterWhere(['like', 'gallery_link_status', $this->gallery_link_status])
            ->andFilterWhere(['like', 'gallery_content_image', $this->gallery_content_image])
            ->andFilterWhere(['like', 'gallery_posted_by', $this->gallery_posted_by])
            ->andFilterWhere(['like', 'gallery_picture_courtesy', $this->gallery_picture_courtesy])
            ->andFilterWhere(['like', 'gallery_flat', $this->gallery_flat]);

        return $dataProvider;
    }
}
