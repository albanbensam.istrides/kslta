<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ksl_coaching_pages".
 *
 * @property string $coaching_id
 * @property string $gallery_id
 * @property string $coaching_top_main_heading
 * @property string $coaching_top_sub_heading
 * @property string $coaching_top_background_img
 * @property string $coaching_page_title
 * @property string $coaching_page_name
 * @property string $coaching_tab_one_title
 * @property string $coaching_tab_one_content
 * @property string $coaching_tab_two_title
 * @property string $coaching_tab_two_content_1
 * @property string $coaching_tab_two_content_2
 * @property string $coaching_tab_three_title
 * @property string $coaching_file_upload
 * @property string $coaching_meta_tag_name
 * @property string $coaching_meta_tag_description
 * @property string $coaching_created_at
 * @property string $coaching_modified_at
 * @property string $coaching_status
 */
class CoachingPages extends \yii\db\ActiveRecord
{


    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ksl_coaching_pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coaching_page_title', 'coaching_page_name', 'coaching_tab_one_title'], 'required'],
            [['gallery_id'], 'integer'],
            
           // [['coaching_top_background_img'], 'file', 'extensions'=>'jpg, gif, png, svg'],
            // [['coaching_tab_one_content', 'coaching_tab_two_content_1', 'coaching_tab_two_content_2', 'coaching_status'], 'string'],
             [['coaching_created_at', 'coaching_modified_at'], 'safe'],
             //[['coaching_file_upload'], 'file', 'extensions' => 'pdf'], 
            // [['coaching_page_title', 'coaching_tab_one_title', 'coaching_tab_two_title', 'coaching_tab_three_title', 'coaching_file_upload'], 'string', 'max' => 500],
            // [['coaching_page_name'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
         return [
            'coaching_id' => 'Coaching ID',
            'gallery_id' => 'Gallery Name',
            'coaching_page_title' => 'Page Title',
            'coaching_page_name' => 'Page Name',
            'coaching_tab_one_title' => 'Tab One Title',
            'coaching_tab_one_content' => 'Tab One Content',
            'coaching_tab_two_title' => 'Tab Two Title',
            'coaching_tab_two_content_1' => 'Tab Two Content 1',
            'coaching_tab_two_content_2' => 'Tab Two Content 2',
        'coaching_top_main_heading'=>'BreadCrumb Title',
        'coaching_top_sub_heading'=>'BreadCrumb Sub Title',
        'coaching_top_background_img'=>'BreadCrumb Background Image',
            'content_portfolio_img'=>'Content Portfolio Image',
            'coaching_tab_three_title' => 'Tab Three Title',
            'coaching_file_upload' => 'File Upload',
             'coaching_tab_three_content_1' => 'Tab Three Content 1',
            'coaching_created_at' => 'Created At',
            'coaching_modified_at' => 'Coaching Modified At',
            'coaching_status' => 'Coaching Status',
            'remove_content_img'=>'Remove Content Porfolio Image',
        ];
    }


      /**
     * @this function will work before save() has to call
     */
    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->coaching_created_at = date("Y-m-d H:i:s");
             $this->coaching_created_at = date("Y-m-d H:i:s");
        }
        
        if (parent::beforeSave($insert)) {
            // Place your custom code here
           return true;
        } else {
            return false;
        }
    }
    
    
    /**
     * @this function will work after getting the data from db and changes view commenly
     */
     public function afterFind() {
       // $this->reg_dob = date('d-m-Y', strtotime($this->reg_dob));
     //   $this->coaching_modified_at = date('d-m-Y H:i:s', strtotime($this->coaching_modified_at));

        parent::afterFind();
        
    }


    /**
     * return Relation with KslGalleryModel
     */
    public function getGallery()
    {
        return $this->hasOne(KslGalleryModel::className(), ['gallery_id' => 'gallery_id']);
    }
}
