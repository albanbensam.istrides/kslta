<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MainMenu;

/**
 * MainMenuSearch represents the model behind the search form about `backend\models\MainMenu`.
 */
class MainMenuSearch extends MainMenu
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_id', 'menu_order_no'], 'integer'],
            [['menu_name', 'menu_link', 'menu_front_view_status', 'menu_created_at', 'menu_modified_at', 'menu_active_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MainMenu::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'menu_id' => $this->menu_id,
            'menu_order_no' => $this->menu_order_no,
            'menu_created_at' => $this->menu_created_at,
            'menu_modified_at' => $this->menu_modified_at,
        ]);

        $query->andFilterWhere(['like', 'menu_name', $this->menu_name])
            ->andFilterWhere(['like', 'menu_link', $this->menu_link])
            ->andFilterWhere(['like', 'menu_front_view_status', $this->menu_front_view_status])
            ->andFilterWhere(['like', 'menu_active_status', $this->menu_active_status]);

        return $dataProvider;
    }
}
