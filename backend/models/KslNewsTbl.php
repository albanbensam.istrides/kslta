<?php

namespace backend\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\Html;

/**
 * This is the model class for table "ksl_news_tbl".
 *
 * @property integer $news_id
 * @property string $news_title
 * @property string $news_display_tag
 * @property string $news_link_address
 * @property string $news_link_status
 * @property string $news_content_image
 * @property string $news_editor_content
 * @property string $news_posted_by
 * @property string $news_from_date
 * @property string $news_to_date
 * @property string $news_created_date
 * @property string $news_lastupdated_date
 * @property string $news_flat
 */
class KslNewsTbl extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
     public $file;
     public $dummy_tourn_title;
    public static function tableName() {
        return 'ksl_news_tbl';
    }
    public $imageFile;
	//public $news_meta_name;
	//public $news_meta_content;


    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['news_title', 'news_display_tag', 'news_link_address', 'news_posted_by', 'news_posted_at'], 'required'],
            [['news_content_image'], 'file', 'extensions'=>'jpg, gif, png', 'on' => 'imageUploaded'],
           // [['news_content_image'], 'file', 'extensions'=>'jpg, gif, png'],
            [['news_link_status', 'news_editor_content', 'news_flat'], 'string'],
            [['news_from_date', 'news_to_date', 'news_created_date', 'news_lastupdated_date','news_meta_name','news_meta_content'], 'safe'],
            [['news_title', 'news_link_address'], 'string', 'max' => 500],
            [['news_display_tag'], 'string', 'max' => 100],
            [['file'], 'file'],
            [['news_posted_by'], 'string', 'max' => 150],
            [['news_tournament_id'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'news_id' => 'News ID',
            'news_title' => 'News Title',
            'news_display_tag' => 'Display Tag',
            'news_link_address' => 'Link Address',
            'news_link_status' => 'Link Status',
            'news_content_image' => 'News Content Image',
            'news_editor_content' => 'Editor Content',
            'news_posted_by' => 'Posted By',
            'news_posted_at' => 'Date of Posting',
            'news_located_city' => 'City',
            'news_located_country' => 'Country',
            'news_from_date' => 'News From Date',
            'news_to_date' => 'News To Date',
            'news_created_date' => 'News Created Date',
            'news_lastupdated_date' => 'News Lastupdated Date',
            'news_flat' => 'News Flat',
            'news_tournament_id' => 'Tournament ID',
			'news_meta_name'=>'Meta',
			'news_meta_content'=>'Content'
        ];
    }

   public function upload() {
       if ($this->validate()) {
           $this->news_content_image->saveAs(Url::to('@web/backend/web/content_images/'). $this->news_content_image->baseName . '.' . $this->news_content_image->extension);
           return true;
       } else {
           return false;
       }
   }



    /**
     * @inheritdoc
     */
    public function beforeSave($insert) {

        if ($this->getIsNewRecord()) {
            $this->news_created_date = date("Y-m-d H:i:s");
            $this->news_from_date = date("Y-m-d");
        }
        $this->news_to_date = date("Y-m-d", strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . "+1 month"));
        $this->news_posted_at = date('Y-m-d', strtotime($this->news_posted_at));
        if (parent::beforeSave($insert)) {
            // Place your custom code here

            return true;
        } else {
            return false;
        }
    }
    
    public function afterFind() {
       // $this->news_posted_at = date("d-m-Y", strtotime($this->news_posted_at));
        parent::afterFind();
        
    }
    
    public static function status_name($status, $id) {
        $status_val = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        $Active = '<i class="fa fa-plus"></i>';
        $atert_info = '';
        switch ($status) {
        	case "A":
                $status_name = 'Reject';
                $status_title = 'Change the status to Reject';
                $atert_info = 'Reject News';
                $Active = '<span class="btn btn-block btn-danger btn-xs"><i class="fa fa-fw fa-ban"></i>Move to Reject<span>';
                break;
            case "U":
                $status_name = 'active';
                $status_title = 'Active News';
                $atert_info = 'Activate news';
                $Active = '<span class="btn btn-block btn-success btn-xs"><i class="fa fa-fw fa-check-square-o"></i>Move to Approve<span>';
                break;
            case "R":
                $status_name = 'unapprove';
                $status_title = 'Change the Pending status into Active';
                $atert_info = 'Are you sure you want to change status Pending into Active';
                $Active = '<span class="btn btn-block btn-warning btn-xs"><i class="fa fa-fw fa-reply-all"></i> Move to Unapprove<span>';
                break;
            default :
                $status_name = 'Activate';
                $status_title = 'Change the InActive status into Active';
                $atert_info = 'Are you sure you want to change status InActive into Active';
                break; //<i class="material-icons">
        }
        $status_val = Html::label($Active, array('id' => $id), ['class' => 'md-fab md-fab-small newstatus', 'name' => $id, 'title' => $status_title, 'data-alert' => $atert_info, "data-uk-tooltip" => "{pos:'top',cls:'long-text'}"]);  
        return $status_val;
    }

}
