<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "affilatedclubs".
 *
 * @property integer $id
 * @property integer $state_id
 * @property string $name
 * @property string $address1
 * @property string $address2
 * @property integer $mobile
 * @property integer $phone
 * @property string $fax
 * @property string $email
 * @property string $website
 */
class Affilatedclubs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'affilatedclubs';
    }

    /**
     * @inheritdoc
     */
      public $state_name;
    public function rules()
    {
        return [
           // [['state_id', 'name', 'address1', 'address2', 'mobile', 'phone', 'fax', 'email', 'website'], 'required'],
             [['state_id', 'name'], 'required'],
            [['state_id', 'mobile', 'phone'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['address1', 'address2', 'fax', 'email'], 'string', 'max' => 100],
            [['website'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'state_id' => 'State Name',
            'name' => 'Name',
            'address1' => 'Address1',
            'address2' => 'Address2',
            'mobile' => 'Mobile',
            'phone' => 'Phone',
            'fax' => 'Fax',
            'email' => 'Email',
            'website' => 'Website',
        ];
    }

 public function afterFind() {
        
       
         $this->state_name= $this->state['state_name'];        
         parent::afterFind();
    }
 public function getState()
    {
        return $this->hasOne(States::className(), ['state_id' => 'state_id']);
    }
}
