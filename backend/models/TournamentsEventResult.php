<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ksl_tournaments_event_result".
 *
 * @property string $ter_id
 * @property string $ter_tournament_id
 * @property string $ter_event_id
 * @property string $ter_player_id
 * @property string $ter_set
 * @property string $ter_result
 * @property string $ter_timestamp
 */
class TournamentsEventResult extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ksl_tournaments_event_result';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ter_tournament_id', 'ter_event_id', 'ter_player_id', 'ter_set', 'ter_result'], 'required'],
            [['ter_result'], 'integer'],
            [['ter_timestamp'], 'safe'],
            [['ter_tournament_id', 'ter_event_id', 'ter_player_id', 'ter_set'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ter_id' => 'Ter ID',
            'ter_tournament_id' => 'Ter Tournament ID',
            'ter_event_id' => 'Ter Event ID',
            'ter_player_id' => 'Ter Player ID',
            'ter_set' => 'Ter Set',
            'ter_result' => 'Ter Result',
            'ter_timestamp' => 'Ter Timestamp',
        ];
    }
}
