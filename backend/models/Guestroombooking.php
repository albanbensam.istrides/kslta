<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "guestroombooking".
 *
 * @property integer $id
 * @property string $checkindate
 * @property string $checkoutdate
 * @property integer $adultcount
 * @property integer $childrencount
 * @property string $name
 * @property string $mobile
 * @property string $email
 * @property string $status
 */
class Guestroombooking extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'guestroombooking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['checkindate', 'checkoutdate', 'adultcount', 'childrencount', 'name', 'mobile', 'email', 'status'], 'required'],
            [['checkindate', 'checkoutdate'], 'safe'],
            [['adultcount', 'childrencount'], 'integer'],
            [['name', 'mobile', 'email'], 'string', 'max' => 50],
            [['status'], 'string', 'max' => 25],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'checkindate' => 'Check In Date',
            'checkoutdate' => 'Check Out Date',
            'adultcount' => 'Adult Count',
            'childrencount' => 'Children Count',
            'name' => 'Name',
            'mobile' => 'Mobile',
            'email' => 'Email',
            'status' => 'Status',
        ];
    }
	
	
	  public function beforeSave($insert) {

        if ($this->isNewRecord) {
           
        }

         $this->checkindate = date("Y-m-d");
		  $this->checkoutdate = date("Y-m-d");
       
        if (parent::beforeSave($insert)) {
            // Place your custom code here
           return true;
        } else {
            return false;
        }		
		
    }
	  
	   public function afterFind() {
        // $this->menu_created_at = date('d-m-Y H:i:s', strtotime($this->menu_created_at));
          $this->checkindate = date("d-m-Y");
		  $this->checkoutdate = date("d-m-Y");
       
        parent::afterFind();
    }
}
