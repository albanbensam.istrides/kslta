<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ksl_players_registration".
 *
 * @property integer $reg_id
 * @property string $player_name
 * @property integer $tourn_id
 * @property integer $player_category
 * @property string $register_id
 * @property string $city
 * @property integer $mobile
 * @property string $registration_date
 * @property string $tournament_date
 */
class KslPlayersRegistration extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ksl_players_registration';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['player_name', 'tourn_id', 'player_category', 'register_id', 'city', 'mobile', 'registration_date', 'tournament_date'], 'required'],
            [['tourn_id', 'player_category', 'mobile'], 'integer'],
            [['registration_date', 'tournament_date'], 'safe'],
            [['player_name'], 'string', 'max' => 200],
            [['register_id'], 'string', 'max' => 100],
            [['city'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'reg_id' => 'Reg ID',
            'player_name' => 'Player Name',
            'tourn_id' => 'Tourn ID',
            'player_category' => 'Player Category',
            'register_id' => 'Register ID',
            'city' => 'City',
            'mobile' => 'Mobile',
            'registration_date' => 'Registration Date',
            'tournament_date' => 'Tournament Date',
        ];
    }
}
