<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
       
      'urlManager' => [
      
          //'baseUrl' => '/',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
              'calander' => 'tournament-schedule/calander',
              
            /*  'contact' => 'tournament-schedule/contact',
              'about-kslta' => 'tournament-schedule/aboutkslta',
              'bar-restaurant' => 'tournament-schedule/barrestaurant',
              'guest-room' => 'tournament-schedule/guestroom',
              'swimming-pool' => 'tournament-schedule/swimmingpool',
              'gym' => 'tournament-schedule/gym',
              'tennis-billiards' => 'tournament-schedule/tennisbilliards',*/
              'draw' => 'tournament-schedule/drawsheet',
              'news' => 'tournament-schedule/newsall',              
              'tournament/<v:\w+>' => 'tournament-schedule/tournament',             
              'newsview/<v:\w+>' => 'tournament-schedule/newsview',              
              '/galleryview'=>'tournament-schedule/galleryview',
              'tourn-gallery-show' => 'tournament-schedule/tournamentgalleryview',
              'coaching'=> 'tournament-schedule/coaching',
              'coaching/<type:\w+>'=> 'tournament-schedule/coaching',
              'stadium1'=> 'tournament-schedule/stadium',
              'daily-schedule'=> 'tournament-schedule/dailyschedule',
              'playerregister'=> 'tournament-schedule/playerregister',
              'eventvaluelist'=> 'player-save/eventvaluelist',
              'guestformsubmit'=>  'player-save/guestform',
              'partyhall'=>  'tournament-schedule/partyhall',
              'gall'=>  'tournament-schedule/gall',

             //API URL Start
             'apilogin'=>  'kslta-api-login/apilogin',
             'uploadinvoice'=>  'kslta-service/upload-member-invoice',
             'uploadpayment'=>  'kslta-service/upload-member-payment',
             //API URL End

              /* NEW PAGES START */
              //'cubbon-park-bangalore' => 'new-pages/cubbonparkbangalore',
              //'commitee' => 'new-pages/commitee',
              //'affilated-clubs' => 'new-pages/affilatedclubs',
              'times-of-karnataka-aita-womens-tournament' => 'new-pages/aita',
              //'/dev/<v:\w+>' => 'tournament-schedule/index',
        '/<v:\w+>' => 'tournament-schedule/index',
              /* NEW PAGES END */
              'academy-reg'=>  'ksl-academy/create',
              'player-reg'=> 'ksl-player-register-v1/create',
              'coaches-reg'=> 'ksl-coaches-reg/create',
              
            ],
        ],
        
        
    ],
    'params' => $params,
];
