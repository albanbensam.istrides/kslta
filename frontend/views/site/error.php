<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;
$this->title = $name;
?>
<style>
    .alert {
     padding: 0px !important;
    margin-bottom: 0px !important;
    border: 0px !important;
    border-radius: 4px;
}
</style>
<div class="site-error" style="margin-top:70px;">

    
    <div align="center">
        <h1><?= Html::encode($this->title) ?></h1>
        
        <img src="<?= Url::to('@web/backend/web/dist/img/empty-search.svg') ?>" alt="Nothing found" width="260">
    </div>
    <div class="alert alert-danger" align="center" style="margin-top: -70px;">

       <h3> <?= nl2br(Html::encode($message)) ?> </h3>
    </div>

    <p align="center" style="font-size: 20px;line-height: 43px;">
        The above error occurred while the Web server was processing your request. <br/>
        Please contact us if you think this is a server error. Thank you.
    </p>
   

</div>
