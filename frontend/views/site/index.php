<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\models\KslNewsTbl;
use backend\models\SliderBanner;
use backend\models\GalleryPageModel;
use backend\models\ImagesView;
use backend\models\KslGalleryTitle;
use backend\models\HomepageNews;
use backend\models\TournamentsEventResult;
use backend\models\PlayerRegister;
use backend\models\Headlines;
use backend\models\KslTournamentsTbl;
use backend\models\Tournament;
use backend\models\Event;
use backend\models\HomeFacilities;
use backend\models\KslGalleryModel;

/* @var $this yii\web\View */

$this -> title = "Home";
?>
<?= Html::csrfMetaTags() ?>
<?php $scrollnews = HomepageNews::find()->where(['status'=>'A'])->all();
$response='';
$contentdata22="";
$footerstring_cutting="";
$subContent="";
$setvaluh="";
$setvaluj="";
$setvalul="";
$setvalun="";
$setvalup="";
$setvalur="";
if($scrollnews) {
foreach($scrollnews as $scroll) {
  $news['regular-title'] = $scroll->news;
  $news['regular-body'] = "";
  $news['tags'] = [];
  $scrollnewsdet[] = $news;

}

$response = json_encode($scrollnewsdet);

}


?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://www.facebook.com/rsrc.php/v3ipwU4/ym/l/en_US/f_MPpVOF3lH.js"></script>
        <script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
        <script src="<?php echo Url::to('@web/frontend/web/lightGallery/dist/js/lightgallery-all.min.js');?>"></script> 
        <script src="<?php echo Url::to('@web/frontend/web/lightGallery/lib/jquery.mousewheel.min.js');?>"></script>
         <script src=" <?php echo Url::to('@web/frontend/web/lightGallery/dist/js/jquery.colorbox.js');?>"></script>
 
       
 
 <link href="frontend/web/lightGallery/dist/css/lightgallery.css" rel="stylesheet">
 <link href="frontend/web/lightGallery/dist/css/colorbox.css" rel="stylesheet">
 <script>
      $(document).ready(function(){
        //Examples of how to assign the Colorbox event to elements
        $(".group1").colorbox({rel:'group1'});
        $(".group2").colorbox({rel:'group2', transition:"fade"});
        $(".group3").colorbox({rel:'group3', transition:"none", width:"75%", height:"75%"});
        $(".group4").colorbox({rel:'group4', slideshow:true});
        $(".ajax").colorbox();
        $(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
        $(".vimeo").colorbox({iframe:true, innerWidth:500, innerHeight:409});
        $(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
        $(".inline").colorbox({inline:true, width:"50%"});
        $(".callbacks").colorbox({
          onOpen:function(){ alert('onOpen: colorbox is about to open'); },
          onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
          onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
          onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
          onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
        });

        $('.non-retina').colorbox({rel:'group5', transition:'none'})
        $('.retina').colorbox({rel:'group5', transition:'none', retinaImage:true, retinaUrl:true});
        
        //Example of preserving a JavaScript event for inline calls.
        $("#click").click(function(){ 
          $('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
          return false;
        });
      });
    </script>
 <style type="text/css">
          .lg-outer .lg-thumb {
        padding: 10px 0;
        height: 75% !important;
        /*margin-left: 13% !important;*/
        margin-bottom: -5px;
      }
      .highlight-button-dark:hover{
        background-color:#999 !important;
        color:#fff !important;
      }
            #lg-download{
              display: none !important;
            }
            .demo-gallery > ul {
              margin-bottom: 0;
            }
            a img:hover {
         opacity: 1;
      }
            .demo-gallery > ul > li {
                float: left;
                margin-bottom: 15px;
                margin-right: 20px;
                width: 200px;
            }
            .demo-gallery > ul > li a {
              border: 3px solid #FFF;
              border-radius: 3px;
              display: block;
              overflow: hidden;
              position: relative;
              float: left;
            }
            .demo-gallery > ul > li a > img {
              -webkit-transition: -webkit-transform 0.15s ease 0s;
              -moz-transition: -moz-transform 0.15s ease 0s;
              -o-transition: -o-transform 0.15s ease 0s;
              transition: transform 0.15s ease 0s;
              -webkit-transform: scale3d(1, 1, 1);
              transform: scale3d(1, 1, 1);
              height: 100%;
              width: 100%;
            }
            .demo-gallery > ul > li a:hover > img {
              -webkit-transform: scale3d(1.1, 1.1, 1.1);
              transform: scale3d(1.1, 1.1, 1.1);
            }
            .demo-gallery > ul > li a:hover .demo-gallery-poster > img {
              opacity: 1;
            }
            .demo-gallery > ul > li a .demo-gallery-poster {
              background-color: rgba(0, 0, 0, 0.1);
              bottom: 0;
              left: 0;
              position: absolute;
              right: 0;
              top: 0;
              -webkit-transition: background-color 0.15s ease 0s;
              -o-transition: background-color 0.15s ease 0s;
              transition: background-color 0.15s ease 0s;
            }
            .demo-gallery > ul > li a .demo-gallery-poster > img {
              left: 50%;
              margin-left: -10px;
              margin-top: -10px;
              opacity: 0;
              position: absolute;
              top: 50%;
              -webkit-transition: opacity 0.3s ease 0s;
              -o-transition: opacity 0.3s ease 0s;
              transition: opacity 0.3s ease 0s;
            }
            .demo-gallery > ul > li a:hover .demo-gallery-poster {
              background-color: rgba(0, 0, 0, 0.5);
            }
            .demo-gallery .justified-gallery > a > img {
              -webkit-transition: -webkit-transform 0.15s ease 0s;
              -moz-transition: -moz-transform 0.15s ease 0s;
              -o-transition: -o-transform 0.15s ease 0s;
              transition: transform 0.15s ease 0s;
              -webkit-transform: scale3d(1, 1, 1);
              transform: scale3d(1, 1, 1);
              height: 100%;
              width: 100%;
            }
            .demo-gallery .justified-gallery > a:hover > img {
              -webkit-transform: scale3d(1.1, 1.1, 1.1);
              transform: scale3d(1.1, 1.1, 1.1);
            }
            .demo-gallery .justified-gallery > a:hover .demo-gallery-poster > img {
              opacity: 1;
            }
            .demo-gallery .justified-gallery > a .demo-gallery-poster {
              background-color: rgba(0, 0, 0, 0.1);
              bottom: 0;
              left: 0;
              position: absolute;
              right: 0;
              top: 0;
              -webkit-transition: background-color 0.15s ease 0s;
              -o-transition: background-color 0.15s ease 0s;
              transition: background-color 0.15s ease 0s;
            }
            .demo-gallery .justified-gallery > a .demo-gallery-poster > img {
              left: 50%;
              margin-left: -10px;
              margin-top: -10px;
              opacity: 0;
              position: absolute;
              top: 50%;
              -webkit-transition: opacity 0.3s ease 0s;
              -o-transition: opacity 0.3s ease 0s;
              transition: opacity 0.3s ease 0s;
            }
            .demo-gallery .justified-gallery > a:hover .demo-gallery-poster {
              background-color: rgba(0, 0, 0, 0.5);
            }
            .demo-gallery .video .demo-gallery-poster img {
              height: 48px;
              margin-left: -24px;
              margin-top: -24px;
              opacity: 0.8;
              width: 48px;
            }
            .demo-gallery.dark > ul > li a {
              border: 3px solid #04070a;
            }
            .home .demo-gallery {
              padding-bottom: 80px;
            }
             </style>
             
           
             
<section id="single_news" class="container secondary-page">
  <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
      <img src="fileupload/sponsor.jpg" />
      
      
      
  <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12" style="margin-top: 40px;">
     
      <section class="slider">
                    <div id="slider" class="flexslider flexslider-attachments">
                        <ul class="slides"> 
               <?php 
                  $slider_list=SliderBanner::find()->where(['banner_status' => 'A'])->orderBy('banner_id desc')->limit(10)->all(); 
                 foreach ($slider_list as $sliderValue) {
                     $banner_location = Url::to('@web/backend/web/'.$sliderValue->banner_image);


                     $slider_data = '<li data-thumb="'.$banner_location.'"> <img src="'.$banner_location.'" alt=""/></li>';
                     echo $slider_data;
                 }
          ?>                                       
                         </ul>
                     </div>
              </section>
       </a>
      <br clear="all" /> 
          <?php 

            $topnews_list=KslNewsTbl::find()->where(['news_link_status' => 'A'])->andWhere(['news_flat' => 'A'])->orderBy(['news_posted_at'=> SORT_DESC],['news_lastupdated_date'=> SORT_DESC])->limit(8)->all(); 
      //$topnews_list=KslNewsTbl::find()->orderBy(['news_lastupdated_date'=> SORT_DESC])->limit(10)->all();
            $sideContentListing = '';
            $centerContentMainListing = '';
            $centerContentBottom = '';
            $count = 0 ;


            foreach ($topnews_list as $mainvalue) {
                
         /* if($count <= 3){
            $img_url='';
                  if($mainvalue->news_content_image!=""){
                       $img_url = Url::to('@web/backend/web/'.$mainvalue->news_content_image);
            }
                                       $string_cutting = ucfirst($mainvalue->news_editor_content);
                        if (strlen($string_cutting) > 65) {
                                 // truncate string
                                    $stringCut = substr($string_cutting, 0, 105);
                  $footerstringCut = substr($string_cutting, 0, 30);
                                // make sure it ends in a word so assassinate doesn't become ass...
                                $string_cutting = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a href="/this/story"></a>'; 
                 $footerstring_cutting = substr($footerstringCut, 0, strrpos($footerstringCut, ' ')); 
                            }
            $in_tag=array('<u>','</u>','<p>','</p>','<strong>','</strong>','<em>','</em>');
            $string_cutting=str_replace($in_tag, '', $string_cutting);
            $string_cutting=trim($string_cutting);
                    $contentdata22 .= '<div class="data-news-pg">';
                    if($img_url!=''){
           $contentdata22 .= '<a href="'.Url::toRoute(['/newsview/'.$mainvalue->news_id]).'" style="text-decoration: none;"><img class="img-djoko" src="'.$img_url.'"  alt="'.$mainvalue->news_title.'" /></a>';
          }
                                        $contentdata22 .= '<p class="news-dd">
                                      '.$mainvalue->news_located_city.", ".date('d F, Y', strtotime($mainvalue->news_posted_at)).'
                                            </p>
            <a href="'.Url::toRoute(['/newsview/'.$mainvalue->news_id]).'" style="text-decoration: none;"><h3>'.$mainvalue->news_title.'</h3></a>
            <p>
                '.$string_cutting.'        
            </p>
            <div style="border-top: 1px solid #e3e3e3;"></div>
        </div>
        <br clear="all" />
        <div>
           

        </div>';
    $footerdata='';
    $footerdata=' <li><img class="img-djoko" src="'.$img_url.'"  alt="'.$mainvalue->news_title.'" /><a href="href="'.Url::toRoute(['/newsview/'.$mainvalue->news_id]).'""> <p>
                '.$footerstring_cutting.'        
            </p></a></li>';

                  
                }*/

                if($count <= 3){
                  $img_url_top='';
                  if($mainvalue->news_content_image!=""){
                            $img_url_top = Url::to('@web/backend/web/'.$mainvalue->news_content_image);
          }
                    $string_cutting_data = ucfirst($mainvalue->news_editor_content);

                     if (strlen($string_cutting_data) > 65) {
                                 // truncate string
                                    $stringCuted = substr($string_cutting_data, 0, 80);
                                // make sure it ends in a word so assassinate doesn't become ass...
                                $string_cutting_data = substr($stringCuted, 0, strrpos($stringCuted, ' ')).'... <a href="/this/story"></a>'; 
                            }
            
                 $subContent .= '<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 data-news-pg" style="padding-left: 0px; min-height:416px;">';
          if($img_url_top!=""){
                    $subContent .= '<a href="'.Url::toRoute(['/newsview/'.$mainvalue->news_id]).'" style="text-decoration: none;"><img class="img-djoko" style="width: 282px; height: 200px;" src="'.$img_url_top.'"  alt="'.$mainvalue->news_title.'" /></a>';
          }
          $string_cutting_data=str_replace('<a>', '', $string_cutting_data);
          $string_cutting_data=str_replace('</a>', '', $string_cutting_data);
          $string_cutting_data=str_replace('<p>', '', $string_cutting_data);
          $string_cutting_data=str_replace('</p>', '', $string_cutting_data);
          $string_cutting_data=str_replace('<a href="/this/story">', '', $string_cutting_data);
          $string_cutting_data=htmlspecialchars($string_cutting_data);          
          $subContent .= '<p class="news-dd">'.$mainvalue->news_located_city.", ".date('d F, Y', strtotime($mainvalue->news_posted_at)).'</p>
         <div style="min-height:140px;"><a href="'.Url::toRoute(['/newsview/'.$mainvalue->news_id]).'"  style="text-decoration: none;"><h3>'.$mainvalue->news_title.'</h3></a>
                 <p style="margin-top: 15px;">'.$string_cutting_data.'</p></div>
                <div style="border-top: 1px solid #e3e3e3;margin-bottom:0px;"></div>';
            $subContent .=' </div>';
            // if($count%2==0)
            // {
              // $subContent.='<div class="clearfix"></div>';
            // }
           
                }

                if($count > 3 && $count <= 7){
                    $img_url_top = Url::to('@web/backend/web/'.$mainvalue->news_content_image);
                    $string_cutting_data = ucfirst($mainvalue->news_editor_content);

                     if (strlen($string_cutting_data) > 85) {
                                 // truncate string
                                    $stringCuted = substr($string_cutting_data, 0, 80);
                                // make sure it ends in a word so assassinate doesn't become ass...
                                $string_cutting_data = substr($stringCuted, 0, strrpos($stringCuted, ' ')).'... <a href="/this/story"></a>'; 
                            }


                        $centerContentBottom .= '<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 data-news-pg" style="padding-left: 0px;min-height:110px;">
            <div style="min-height:60px;"><p style="margin-bottom: 5px; color: #0D4C93; margin-left: 0px;">'.$mainvalue->news_located_city.", ".date('d F, Y', strtotime($mainvalue->news_posted_at)).'</p>
               <a href="'.Url::toRoute(['/newsview/'.$mainvalue->news_id]).'" style="text-decoration: none;"><h3>'.$mainvalue->news_title.'</h3></a></div>
                
                                     <div style="border-top: 1px solid #e3e3e3;margin-bottom: 0px;"></div>
                                
                            </div>';
              // if($count%2==0)
            // {
              // $centerContentBottom.='<div class="clearfix"></div>';
            // }
                }

            $count++;
               
            }

            /* Side Content News */
             echo  $subContent;
               echo $centerContentBottom;     
            ?>
     
    
  </div>
  <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12" style="margin-top: 40px;">
    <?php $model=HomeFacilities::find()->orderBy(['order_no'=>'SORT_ASC'])->limit(4)->all();
          
          foreach ($model as $value) {
            $link_data = Url::to('@web/backend/web/'.$value->image_upload);
            echo '<div style=" height: 245px;width: 280px;" align="center"><a href="'.Url::toRoute(['/'.$value->facility_url]).'"><img style="" src="'.$link_data.'"  alt="Facilities" style="margin-bottom: 20px;"/><span"><b  style="float: left;   margin-top:20px;" align="center">'.strtoupper($value->facility_name).'</b></span></a></div><hr />';
          
            }?> 
    

  </div>
<div class="col-md-3 col-xs-12 home-page general-results" style="margin-top: 40px;">
  <div class="main data-news-pg">
<?php 
    $imgModelSpa = ImagesView::find()->where(['image_sub_title' => 'FourFountain'])->one();
  if($imgModelSpa){
    $spaImage = Url::to('@web/backend/web/').$imgModelSpa->image_file_location;
    }
?>
<?php 
    $imgModelSpa1 = ImagesView::find()->where(['image_sub_title' => 'Fountain'])->one();
  
  if($imgModelSpa1){
    $spaImage1 = Url::to('@web/backend/web/').$imgModelSpa1->image_file_location;
  }
?>
<?php 
    $imgModelSpa2 = ImagesView::find()->where(['image_sub_title' => 'Fountains'])->one();
  if($imgModelSpa2){
    $spaImage2 = Url::to('@web/backend/web/').$imgModelSpa2->image_file_location;
  }
?>

 
<?php if(count($imgModelSpa)>0) {?>
    <ul class="portfolio group albumContainer isotope" style="position: relative; overflow: hidden; height: 427px;">
        <li style="width: 100%; border: none; position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);" class="item block cat2 isotope-item">
            <a href="<?php echo $spaImage;  ?>" data-gal="prettyPhoto[portfolio]"><img src="<?php echo $spaImage;  ?>" alt="Four Fountain" style="opacity: 1;"></a>
        </li>
    </ul>
<?php } ?>
<?php if(count($imgModelSpa1)>0) {?>
    <ul class="portfolio group albumContainer isotope" style="position: relative; overflow: hidden; height: 427px;">
        <li style="width: 100%; border: none; position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);" class="item block cat2 isotope-item">
            <a href="<?php echo $spaImage1;  ?>" data-gal="prettyPhoto[portfolio]"><img src="<?php echo $spaImage1;  ?>" alt="Fountain" style="opacity: 1;"></a>
        </li>
    </ul>
<?php } ?>
<?php if(count($imgModelSpa2)>0) {?>
    <ul class="portfolio group albumContainer isotope" style="position: relative; overflow: hidden; height: 427px;">
        <li style="width: 100%; border: none; position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);" class="item block cat2 isotope-item">
            <a href="<?php echo $spaImage2;  ?>" data-gal="prettyPhoto[portfolio]"><img src="<?php echo $spaImage2;  ?>" alt="Fountains" style="opacity: 1;"></a>
        </li>
    </ul>
 <?php } ?>
    <div class="tabs1"><div class="info"style="color: #2673cf ;  text-transform: uppercase;"><b> Key Series</b></div> 
  
            <ul>
          <?php $model=Tournament::find()->orderBy(['order_no'=>'SORT_ASC'])->limit(6)->all();
          
          foreach ($model as $value) {
            echo '<li class="newul forline" style="list-style: none!important; font-size:13px; margin: 0px 0px 0px 0px;border-bottom: 1px solid #ededed!important;
   padding: 11px 0px 8px 0px;"><a href="'.Url::toRoute(['tournament/'.$value->tour_url]).'" target="_blank" ><img src="frontend/web/images/i.png" width="20px" height="20px" />&nbsp;&nbsp;'.$value->tour_name.'</a></li>';
          }
          
           ?>
            
            </ul>
      </div>
      
      
      <div class="tabs2"><br><br>
        <div class="info"style="color: #2673cf ;  text-transform: uppercase;"><b>Top Headlines</b></div>
          <ul >
             <?php
             $model=Headlines::find()->orderBy(['order_no'=>'SORT_ASC'])->limit(5)->all();  
             foreach ($model as $value) {
               if (strlen($value->headlines_name) > 40) {
                                 // truncate string
                                    $stringCuted = substr($value->headlines_name, 0, 35);
                                // make sure it ends in a word so assassinate doesn't become ass...
                                $string_cutting_data = substr($stringCuted, 0, strrpos($stringCuted, ' ')).'..'; 
                            }else{
                              $string_cutting_data =$value->headlines_name;
                            }
               echo  '<a href="newsview/'.$value->headline_link.'"><li class="newul"style="line-height: 24px;font-size:13px; margin: 0px 0px 0px 18px;border-bottom: 1px solid #ededed!important;
   padding: 11px 0px 8px 0px;">'.$string_cutting_data.'</li>';
             }       
              ?> 
             
            </ul>
      </div>
    
      
      <a class="third" href="" style="text-decoration: none;"><img class="img-djoko" src="frontend/web/img/add.jpg" alt="" /></a>
       
      <div class="tabs3"> 
        
        
      <div class="info" style="float:left; margin-top: 15px;">Follow Kslta</div> 
            <ul style="float:left; margin-top: 15px; width: 100%;">
            <li class="newul li"style="list-style: none!important; margin: 0px 0px 0px 0px;border-bottom: 1px solid #ededed!important;padding: 11px 0px 8px 0px;"><a href="https://www.facebook.com/pages/The-KSLTA/348263411921591" target="_blank"><img src="frontend/web/images/dlink.png"/> &nbsp;&nbsp;Facebook<img src="frontend/web/images/arrow.png"/ style="width: 13px; height: 13px; float: right;"></a></li> 
                         <li class="newul li"style="list-style: none!important;    margin: 0px 0px 0px 0px;border-bottom: 1px solid #ededed!important;padding: 12px 0px 8px 0px;"><a href="https://twitter.com/hashtag/kslta" target="_blank"><img src="frontend/web/images/twitter.png"/>&nbsp; &nbsp;Twitter<img src="frontend/web/images/arrow.png"/ style="width: 13px; height: 13px; float: right;"></a></li> 
                            <li class="newul li"style="list-style: none!important;    margin: 0px 0px 0px 0px;border-bottom: 1px solid #ededed!important;padding: 12px 0px 8px 0px;"><a href="http://www.instapu.com/tag/kslta" target="_blank"><img src="frontend/web/images/instagram.png"/>&nbsp; &nbsp;Instagram<img src="frontend/web/images/arrow.png"/ style="width: 13px; height: 13px; float: right;"></a></li> 
                      
            </ul>
      </div>
    </div>
  </div>
  
  </div>
  <div class="col-md-12">
  <div class="col-md-9 col-lg-9 col-sm-12 col-xs-12" style="margin-top: 0px;">
  <div class="slider">
    <div class="toper" style="padding-top: 10px">
      
             <span style="font-size: 16px;padding: 0px 0px 10px 0px; float: left;"><b>Featured Gallery</b></span>
                    
    </div>
    <div class="slidernew">
  
   <script src="frontend/web/js/jssor.slider.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_SlideshowTransitions = [
              {$Duration:1200,x:0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:-0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:-0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:-0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:-0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,$Cols:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$Rows:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$Cols:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:-0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,$Rows:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:-0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$SlideOut:true,$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,$Delay:20,$Clip:3,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,$Delay:20,$Clip:3,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,$Delay:20,$Clip:12,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,$Delay:20,$Clip:12,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2}
            ];

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Cols: 5,
                $SpacingX: 5,
                $SpacingY: 5,
                $Align: 390
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 980;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <style>
        /* jssor slider loading skin spin css */
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from {
                transform: rotate(0deg);
            }

            to {
                transform: rotate(360deg);
            }
        }


        .jssora106 {display:block;position:absolute;cursor:pointer;}
        .jssora106 .c {fill:#fff;opacity:.3;}
        .jssora106 .a {fill:none;stroke:#000;stroke-width:350;stroke-miterlimit:10;}
        .jssora106:hover .c {opacity:.5;}
        .jssora106:hover .a {opacity:.8;}
        .jssora106.jssora106dn .c {opacity:.2;}
        .jssora106.jssora106dn .a {opacity:1;}
        .jssora106.jssora106ds {opacity:.3;pointer-events:none;}

        .jssort101 .p {position: absolute;top:0;left:0;box-sizing:border-box;background:#000;}
        .jssort101 .p .cv {position:relative;top:0;left:0;width:100%;height:100%;border:2px solid #000;box-sizing:border-box;z-index:1;}
        .jssort101 .a {fill:none;stroke:#fff;stroke-width:400;stroke-miterlimit:10;visibility:hidden;}
        .jssort101 .p:hover .cv, .jssort101 .p.pdn .cv {border:none;border-color:transparent;}
        .jssort101 .p:hover{padding:2px;}
        .jssort101 .p:hover .cv {background-color:rgba(0,0,0,6);opacity:.35;}
        .jssort101 .p:hover.pdn{padding:0;}
        .jssort101 .p:hover.pdn .cv {border:2px solid #fff;background:none;opacity:.35;}
        .jssort101 .pav .cv {border-color:#fff;opacity:.35;}
        .jssort101 .pav .a, .jssort101 .p:hover .a {visibility:visible;}
        .jssort101 .t {position:absolute;top:0;left:0;width:100%;height:100%;border:none;opacity:.6;}
        .jssort101 .pav .t, .jssort101 .p:hover .t{opacity:1;}
    </style>
    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:700px;height:480px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="<?php echo Url::to('@web/frontend/web/svg/loading/static-svg/spin.svg'); ?>" />
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:700px;height:380px;overflow:hidden;">
            <?php
             $home_gallery = GalleryPageModel::find()->where(['gal_image_rootid' => 3])->orderBy('gal_image_order desc')->limit(10)->all(); 

        
      foreach ($home_gallery as $gallery_imgvalue) {
        $link_data = Url::to('@web/backend/web/galleryimage/gallery_pages/'.$gallery_imgvalue->gal_image_path);
        echo '<div>
                <img data-u="image" src="'.$link_data.'" />
                <img data-u="thumb" src="'.$link_data.'" />
            </div>';
      }
            
             ?>
            
            
             
            
            <a data-u="any" href="https://www.jssor.com" style="display:none">javascript slider</a>
        </div>
        <!-- Thumbnail Navigator -->
        <div data-u="thumbnavigator" class="jssort101" style="position:absolute;left:0px;bottom:0px;width:700px;height:100px;background-color:#000;" data-autocenter="1" data-scale-bottom="0.75">
            <div data-u="slides">
                <div data-u="prototype" class="p" style="width:190px;height:84px;">
                    <div data-u="thumbnailtemplate" class="t"></div>
                    <svg viewBox="0 0 16000 16000" class="cv">
                        <circle class="a" cx="8000" cy="8000" r="3238.1"></circle>
                        <line class="a" x1="6190.5" y1="8000" x2="9809.5" y2="8000"></line>
                        <line class="a" x1="8000" y1="9809.5" x2="8000" y2="6190.5"></line>
                    </svg>
                </div>
            </div>
        </div>
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora106" style="width:55px;height:55px;top:162px;left:30px;" data-scale="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                <polyline class="a" points="7930.4,5495.7 5426.1,8000 7930.4,10504.3 "></polyline>
                <line class="a" x1="10573.9" y1="8000" x2="5426.1" y2="8000"></line>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora106" style="width:55px;height:55px;top:162px;right:30px;" data-scale="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                <polyline class="a" points="8069.6,5495.7 10573.9,8000 8069.6,10504.3 "></polyline>
                <line class="a" x1="5426.1" y1="8000" x2="10573.9" y2="8000"></line>
            </svg>
        </div>
    </div>
    <script type="text/javascript">jssor_1_slider_init();</script></div>
  </div>
  </div>
  
  <div class="col-md-3 col-xs-12 home-page general-results" style="margin-top: 40px;">
    </div>
  
  </div>
  
   
  <div class="col-md-9 col-lg-9 col-sm-12 col-xs-12" style="margin-top: 40px;">
    <div class="gallerydown" style="padding: 7px 8px 10px 0px;">
    <div class="toper" style="padding-top: 10px; height: 40px;" >
      
             <span style="font-size: 16px;padding: 0px 0px 0px 15px;"><b>Galleries</b></span>
      
            <a href="<?php echo Url::toRoute(['/galleryview']); ?>" class="button button1"style="float: right; padding: 5px; border-radius: 4px;  background: #2673cf;">
               <i class="fa fa-camera" aria-hidden="true" style="font-size: 16px"></i>&nbsp;&nbsp;<span class="gallery" style="color: #fff">More Gallery</span></a>
  
    </div>
    
    <div class="toper2" style="padding-top: 14px">
         <body>
     
    <?php $home_gallery = GalleryPageModel::find()->groupBy('gal_image_rootid')->limit(4)->all(); ?>
        
        
        <?php  
        $i=1;
        $class='';
        foreach ($home_gallery as  $value) {
          $homegallery1 = KslGalleryModel::find()->where(['gallery_id'=>$value->gal_image_rootid])->all();
          foreach ($homegallery1 as $key ) {
            $title=$key->gallery_title;
          }
          
        //  $image_data = Url::to('@web/backend/web/galleryimage/gallery_pages/'.$value->gal_image_path);
           $image_='<img class="gallery_data" style="width: 280px; height: 156px;" src='.Url::to('@web/backend/web/galleryimage/gallery_pages/'.$value->gal_image_path).' alt='.$value->gal_image_rootid.' />';
          
           $home_gallery1 = GalleryPageModel::find()->where(['gal_image_rootid'=>$value->gal_image_rootid])->all();
          $k=1;
          foreach ($home_gallery1 as $values) {
             $image_data = Url::to('@web/backend/web/galleryimage/gallery_pages/'.$values->gal_image_path);
          if($k==1){
            $class= 'test123';
            ?>
            <div class="col-md-3 hovereffect" style="padding-right: 0px;" ><a class="group<?php echo $i;?>"  href="<?php echo $image_data;?>" ><?php echo $image_; ?><div class="overlay">
                 <i class="fa fa-camera" aria-hidden="true"></i>
                  <p style="padding-top: 5px;"><?php echo $title;?></p>
               </div></a> </div>
      <?php } 
          else{?>
           
          <p style="margin: 0px 0px 0px 0px;"><a class="group<?php echo $i;?> <?php echo $class;?>"  href="<?php echo $image_data;?>">
                  
              
          
          
          </a></p>
          
          
          <?php
          }
          
          ++$k;
          
          }?>
          
          <script>
      $(document).ready(function(){
                
        $(".group"+<?php echo $i; ?>).colorbox({rel:'group'+<?php echo $i; ?>, transition:"fade"});
        
      });
      </script>
          
        <?php 
        ++$i;
        } 
        ?>
  
    
    
    <!-- <h2>Fade Transition</h2>
    <p><a class="group2" href="frontend/web/img/add.jpg" title="Me and my grandfather on the Ohoopee">Grouped Photo 1</a></p>
    <p><a class="group2" href="frontend/web/img/add.jpg" title="On the Ohoopee as a child">Grouped Photo 2</a></p>
    <p><a class="group2" href="frontend/web/img/add.jpg" title="On the Ohoopee as an adult">Grouped Photo 3</a></p>
     -->
    
     
    </body>
      <!--<body class="home" id="demobg">

        <div class="demo-gallery">
            <ul id="lightgallery" class="list-unstyled row">
              
              <?php $home_gallery = GalleryPageModel::find()->groupBy('gal_image_rootid')->limit(4)->all(); ?>
        
        
        <?php  
        foreach ($home_gallery as  $value) {
           $image_data = Url::to('@web/backend/web/galleryimage/gallery_pages/'.$value->gal_image_path);
        echo ' <div class="col-md-3 col-lg-3 col-xs-6" style="float:left;">
      <a class="btn"  data-content=""  data-config="" href="#">
              <img src="'.$image_data.'" data-src="'.$image_data.'" data-id="'.$value->gal_image_rootid.'" class="img-responsive test class_thump_image" alt="" >
      
      </a>
      </div>';
        } 
           ?>
            
            </ul>
        </div>
        
        <script type="text/javascript">
       $(document).ready(function(){
           $('#lightgallery').lightGallery(); 
           
        });
        </script>
       
       
    </body>
    -->
      
      <?php
       
           /* $home_gallery = GalleryPageModel::find()->groupBy('gal_image_rootid')->orderBy('gal_image_rootid desc')->limit(4)->all(); 
      
      foreach ($home_gallery as $gallery_imgvalue) { 
        
         $image_data='<img class="gallery_data" style="width: 280px; height: 156px;" src='.Url::to('@web/backend/web/galleryimage/gallery_pages/'.$gallery_imgvalue->gal_image_path).' alt='.$gallery_imgvalue->gal_image_rootid.' />';
      echo '<div class="col-md-3 col-xs-12 hovereffect" style="padding: 7px 0px 10px 15px;" > 
                    <div class="mart">';
      $gallery = GalleryPageModel::find()->where(['gal_image_rootid'=>$gallery_imgvalue->gal_image_rootid])->all();   
        foreach ($gallery as $value) {
             $img = $value->gal_image_path;
            $link_data = Url::to('@web/backend/web/galleryimage/gallery_pages/'.$img);  
            echo '
             <a href="'.$link_data.'" rel="prettyPhoto[portfolio]">'. $image_data.'<div class="overlay">
                 <i class="fa fa-camera" aria-hidden="true"></i>
                  <p style="padding-top: 5px;"></p>
            </div></a> 
            </div> 
             <div> 
            
            ';
      }
        echo ' </div>
          </div>';
      }  
         
        */
      
      //}
    /*  }*/
      
       ?> 
      
        <!-- <ul class="gallery clearfix">
        <li><a href="frontend/web/img/about.jpg" rel="prettyPhoto[portfolio]"" title="How is the description on that one? How is the description on that one? How is the description on that one? "><img src="frontend/web/img/about.jpg" width="60" height="60" alt="This is a pretty long title" /></a></li>
        <li><a href="frontend/web/img/about.jpg" rel="prettyPhoto[portfolio]" title="Description on a single line."><img src="frontend/web/img/about.jpg" width="60" height="60" alt="" /></a></li>
        <li><a href="frontend/web/img/about.jpg" rel="prettyPhoto[portfolio]"><img src="frontend/web/img/about.jpg" width="60" height="60" alt="" /></a></li>
        <li><a href="frontend/web/img/about.jpg" rel="prettyPhoto[portfolio]"><img src="frontend/web/img/add.jpg" width="60" height="60" alt="" /></a></li>
        <li><a href="frontend/web/img/about.jpg" rel="prettyPhoto[portfolio]"><img src="frontend/web/img/add2.jpg" width="60" height="60" alt="" /></a></li>
      </ul> -->
      </div>  
       <p><a href="#" onclick="$.prettyPhoto.open(api_gallery,api_titles,api_descriptions); return false"></a></p>
 <link rel="stylesheet" href="frontend/web/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
     
    
    <style type="text/css" media="screen">
      * { margin: 0; padding: 0; }
      .pp_social{
        display:none !important;
      }
       
      
    </style>
    
    <script type="text/javascript" charset="utf-8">
  $(window).load(function() {
    $('.flexslider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: true,
    asNavFor: '#slider'
  });
  });
</script>
<script>
     $(document).on('click','.test',function(e){
           
      var attr = $(this).attr('data-id');
       $('.demo-gallery').empty();
    $.ajax({
          type:"POST",
          url:'<?php echo Yii::$app -> homeUrl;  ?>gall?id='+attr,
           
          success: function(response){
            //alert(response);
            
            $(".demo-gallery").prepend("<ul id='lightgallery' class='list-unstyled row' style='display: block'>"+response+"</ul>");
            
            $('#lightgallery').lightGallery(); 
            
                  $("#demobg"). removeAttr("style");
           $(document).on('contextmenu', 'img', function() {
      return false;
  })
  
        }
         
    });
    
  
       
        });
  
</script>
  <script>
    $(function () {
    "use strict";
    $(".portfolio a").hover(function () {
        $(this).children("img").animate({ opacity: 0.55 }, "fast");
    }, function () {
        $(this).children("img").animate({ opacity: 1.0 }, "slow");
    });

    $("a[rel^='prettyPhoto[portfolio]']").prettyPhoto({
        animation_speed: 'fast', /* fast/slow/normal */
        slideshow: 3000, /* false OR interval time in ms */
        autoplay_slideshow: true, /* true/false */
        opacity: 0.80, /* Value between 0 and 1 */
        show_title: true, /* true/false */
        allow_resize: true, /* Resize the photos bigger than viewport. true/false */
        default_width: 500,
        default_height: 344,
        counter_separator_label: '/', /* The separator for the gallery counter 1 "of" 2 */
        theme: 'pp_default', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
        horizontal_padding: 20, /* The padding on each side of the picture */
        hideflash: false, /* Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto */
        wmode: 'opaque' /* Set the flash wmode attribute */
    });
});
  </script>   
  <script src="<?//= Url::to('@web/frontend/web/js/gallery/jquery.prettyPhoto.js') ?>"></script> 
<script type="text/javascript">
   /*  $(document).ready(function ($){
      $('.flexslider').flexslider({
            animation: "slide",
      useCSS: false,
      animationLoop: true,
      smoothHeight: true,
       });
     });*/
  </script>
     
  </div>
  </div>

  <div class="col-md-3 col-xs-12 home-page general-results" style="margin-top: 40px;">
    </div>
     
</section>



 


<!--SECTION LAST PHOTO-->
<section id="news-section">
    <div class="container">
        <div class="top-match col-xs-12 col-md-12">
            
        </div>
    </div>
 
</section>
<!--SECTION CLIENTS-->
 <style>
 #cboxLoadedContent{
  width: 962px !important;
   overflow: hidden !important;
    height: 609px !important;
 }
 #cboxContent{
   width: 962px !important;
   height: 609px !important;
   float: left;
 }
 .cboxPhoto{
  cursor: pointer;
   width: 962px !important;
   height: auto !important;
   float: none;
 }
 #cboxOverlay {
    background: #555;
   }
 #colorbox{
 display: block;
 visibility: visible;
 top: 2471px;
 left: 185px !important; 
 position: absolute;
 width: 962px !important;
 height: 609px !important;
 opacity: 1;
 }
 
 
 .flex-control-nav{
  display: none;
 }
  .mart{
    float:left; 
    /*height: 200px;*/
     
  }
  
  .fa-camera{
    color:#fff;
    font-size: 40px;
  }
  .hovereffect {
  overflow: hidden;
  position: relative;
  text-align: center;
  cursor: pointer;
}

.hovereffect .overlay {
  position: absolute;
  overflow: hidden;
  width: 80%;
  height: 80%;
  left: 10%;
  top: 10%;
  /*border-bottom: 1px solid #FFF;
  border-top: 1px solid #FFF;*/
  -webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
  transition: opacity 0.35s, transform 0.35s;
  -webkit-transform: scale(0,1);
  -ms-transform: scale(0,1);
  transform: scale(0,1);
}

.hovereffect:hover .overlay {
  opacity: 1;
  filter: alpha(opacity=100);
  -webkit-transform: scale(1);
  -ms-transform: scale(1);
  transform: scale(1);
}

.hovereffect img {
  display: block;
  position: relative;
  -webkit-transition: all 0.35s;
  transition: all 0.35s;
}
hr{
  width:280px;
}
.hovereffect:hover img {
  /*filter: url('data:image/svg+xml;charset=utf-8,<svg xmlns="http://www.w3.org/2000/svg"><filter id="filter"><feComponentTransfer color-interpolation-filters="sRGB"><feFuncR type="linear" slope="0.6" /><feFuncG type="linear" slope="0.6" /><feFuncB type="linear" slope="0.6" /></feComponentTransfer></filter></svg>#filter');
  
  */
 filter: brightness(0.6);
  -webkit-filter: brightness(0.6);
}

.hovereffect h2 {
  text-transform: uppercase;
  text-align: center;
  position: relative;
  font-size: 17px;
  background-color: transparent;
  color: #FFF;
  padding: 1em 0;
  opacity: 0;
  filter: alpha(opacity=0);
  -webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
  transition: opacity 0.35s, transform 0.35s;
  -webkit-transform: translate3d(0,-100%,0);
  transform: translate3d(0,-100%,0);
}

.hovereffect a, .hovereffect p {
  color: #FFF;
  padding: 1em 0;
  opacity: 1;
  filter: alpha(opacity=0);
  -webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
  transition: opacity 0.35s, transform 0.35s;
  -webkit-transform: translate3d(0,100%,0);
  transform: translate3d(0,100%,0);
}

.hovereffect:hover a, .hovereffect:hover p, .hovereffect:hover h2 {
  opacity: 1;
  filter: alpha(opacity=100);
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
}
a:hover, a:focus {
    color: #2a6496;
    text-decoration: none;
}
.portfolio img {
  border: none; 
}
.test123{
  display: none;
}


#cboxPrevious{
    border: none !important;
    outline: none !important;
}
#cboxNext{
    border: none !important;
    outline: none !important;
}
#cboxPrevious:before {
    content: "\f190";
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
    text-decoration: inherit;
/*--adjust as necessary--*/
    color: #000;
    font-size: 30px;
    padding-right: 0.5em;
    position: absolute;
    top: -380px;
    left: 30px;
    padding: 8px;
    background: #fff;
    border-radius: 15px;
   
}
#cboxNext:before {
    content: "\f18e";
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
    text-decoration: inherit;
/*--adjust as necessary--*/
    color: #000;
    font-size: 30px;
    padding-right: 0.5em;
    position: absolute;
    top: -380px;
    right: 30px;
    padding: 8px;
    background: #fff;
    border-radius: 15px;
  
}
</style>


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-75818757-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-75818757-1');
</script>

