<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\KslAcademyRegSerach */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ksl-academy-reg-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'academy_name') ?>

    <?= $form->field($model, 'owner') ?>

    <?= $form->field($model, 'contact_number') ?>

    <?= $form->field($model, 'email_id') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'no_of_courts_clay') ?>

    <?php // echo $form->field($model, 'no_of_courts_hard') ?>

    <?php // echo $form->field($model, 'no_of_courts_others') ?>

    <?php // echo $form->field($model, 'no_of_courts_flood') ?>

    <?php // echo $form->field($model, 'no_of_coaches') ?>

    <?php // echo $form->field($model, 'office') ?>

    <?php // echo $form->field($model, 'pro_shop') ?>

    <?php // echo $form->field($model, 'cafeteria') ?>

    <?php // echo $form->field($model, 'toilet') ?>

    <?php // echo $form->field($model, 'room') ?>

    <?php // echo $form->field($model, 'gym') ?>

    <?php // echo $form->field($model, 'warm_up') ?>

    <?php // echo $form->field($model, 'water') ?>

    <?php // echo $form->field($model, 'other_information') ?>

    <?php // echo $form->field($model, 'remarks') ?>

    <?php // echo $form->field($model, 'active_status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
