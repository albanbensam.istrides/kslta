<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\KslAcademyRegSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ksl Academy Regs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-academy-reg-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ksl Academy Reg', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'academy_name:ntext',
            'owner',
            'contact_number',
            'email_id:email',
            // 'address:ntext',
            // 'no_of_courts_clay',
            // 'no_of_courts_hard',
            // 'no_of_courts_others',
            // 'no_of_courts_flood',
            // 'no_of_coaches',
            // 'office',
            // 'pro_shop',
            // 'cafeteria',
            // 'toilet',
            // 'room',
            // 'gym',
            // 'warm_up',
            // 'water',
            // 'other_information',
            // 'remarks:ntext',
            // 'active_status',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
