<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\KslAcademyReg */
/* @var $form yii\widgets\ActiveForm */
?>

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<?php

$form = ActiveForm::begin(['enableAjaxValidation' => true,'enableClientValidation' => true]);

?>
<div class="top-score-title col-md-12 right-title">
<h3><?= Html::encode($this->title) ?></h3>
</div>
<div class="col-md-12">

<div class="col-md-6">

    <?= $form -> field($model, 'academy_name') -> textInput([ 'class' => 'form-control'])->label('Academy Name') ?>
</div>
<div class="col-md-6">
    <?= $form -> field($model, 'owner') -> textInput([ 'class' => 'form-control']) ?>

</div>

<div class="col-md-6">

    <?= $form -> field($model, 'contact_number') -> textInput([ 'class' => 'form-control']) ?>
</div>
<div class="col-md-6">
    <?= $form -> field($model, 'email_id') -> textInput([ 'class' => 'form-control']) ?>
</div>

<div class="col-md-6">

    <?= $form -> field($model, 'address') -> textarea(['class' => 'form-control', 'rows' => '3']) ?>
</div>
<div class="col-md-6">
    <?= $form -> field($model, 'no_of_courts_clay') -> textInput([ 'class' => 'form-control']) ?>
</div>

<div class="col-md-6">

    <?= $form -> field($model, 'no_of_courts_hard') -> textInput(['class' => 'form-control']) ?>
</div>
<div class="col-md-6">
    <?= $form -> field($model, 'no_of_courts_others') -> textInput([ 'class' => 'form-control']) ?>
</div>
<div class="col-md-6">

    <?= $form -> field($model, 'no_of_courts_flood') -> textInput(['class' => 'form-control']) ?>
</div>
<div class="col-md-6">
    <?= $form -> field($model, 'no_of_coaches') -> textInput(['class' => 'form-control']) ?>
</div>
<div class="col-md-6">

    <div class="col-md-2">
    <?= $form -> field($model, 'office') ->radioList(['Yes' => 'Yes', 'No' => 'No']) ?>
    </div>
    <div class="col-md-2">
    <?= $form -> field($model, 'pro_shop') -> radioList([ 'Yes' => 'Yes', 'No' => 'No']) ?>
    </div>
    <div class="col-md-2">
    <?= $form -> field($model, 'cafeteria') -> radioList(['Yes' => 'Yes', 'No' => 'No']) ?>
    </div>
    <div class="col-md-2">
        <?= $form -> field($model, 'room') -> radioList(['Yes' => 'Yes', 'No' => 'No']) ?>
    </div>
    <div class="col-md-2">
    <?= $form -> field($model, 'gym') -> radioList(['Yes' => 'Yes', 'No' => 'No']) ?>
    </div>
    <div class="col-md-2">

    <?= $form -> field($model, 'warm_up') -> radioList(['Yes' => 'Yes', 'No' => 'No']) ?>
    </div>
</div>


<div class="col-md-12">
    <div class="col-md-2">
    <?= $form -> field($model, 'water') -> radioList(['Yes' => 'Yes', 'No' => 'No']) ?>
    </div>
    <div class="col-md-4">
    <?= $form -> field($model, 'toilet') -> dropDownList(['Male' => 'Male', 'Female' => 'Female'],['class' => 'form-control','multiple' => "true"]) ?>
    </div>
    <div class="col-md-4">
    <?= $form -> field($model, 'other_information') -> textInput(['class' => 'form-control']) ?>
    </div>
</div>

<div class="col-md-6">
    <?= $form -> field($model, 'remarks') -> textarea(['class' => 'form-control',   'rows' => "2"]) ?>
</div>





</div>





<div class="form-group pull-right">

<button type="submit" class="btn btn-info">Submit</button>
</div>

<div id="alertmessage" style="color:red;"></div>
<div id="message" style="color:green;"></div>


<?php ActiveForm::end(); ?>

<style>
    .myCalendar {
    cursor: pointer;
}
</style><link href="../frontend/web/js/dcalendar.picker.css" rel="stylesheet" type="text/css">
<script src="../frontend/web/js/dcalendar.picker.js"></script>
<script>
//$('#checkindate').dcalendarpicker();
//$('#checkoutdate').dcalendarpicker();

$('select').select2({
  //selectOnClose: true,
  allowClear: true
});

</script>

