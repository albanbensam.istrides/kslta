<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\KslAcademyReg */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ksl Academy Regs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-academy-reg-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'academy_name:ntext',
            'owner',
            'contact_number',
            'email_id:email',
            'address:ntext',
            'no_of_courts_clay',
            'no_of_courts_hard',
            'no_of_courts_others',
            'no_of_courts_flood',
            'no_of_coaches',
            'office',
            'pro_shop',
            'cafeteria',
            'toilet',
            'room',
            'gym',
            'warm_up',
            'water',
            'other_information',
            'remarks:ntext',
            'active_status',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
