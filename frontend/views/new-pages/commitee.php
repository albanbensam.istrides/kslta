<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\PageContents;
use backend\models\ImagesView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\modelsTournamentScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this -> title = 'Committee - Karnataka State Lawn Tennis Association';
$this -> params['breadcrumbs'][] = $this -> title;
?>

<style>
	.back-img-shop {
	    background: transparent url('<?= Url::to('@web/backend/web/'.$pageModel->page_content_img) ?>') no-repeat bottom center;
	    -webkit-background-size: cover;
	    -moz-background-size: cover;
	    -o-background-size: cover;
	    background-size: cover;
}

</style>

<section class="drawer">
	<div class="col-md-12 size-img back-img-shop">
		<div class="effect-cover">
			<h3 class="txt-advert animated"><?= $pageModel->page_content_top_main_heading ?></h3>
			<p class="txt-advert-sub">
				<?= $pageModel->page_content_top_sub_heading ?>
			</p>
		</div>
	</div>
	</section>
<?php 
	$committeeModel = ImagesView::find()->where(['image_title' => 'commitee'])->orderBy(['image_over_title' => 'SORT_ASC'])->all();
	
	$committeeRowView = '';
	foreach ($committeeModel as $key => $value) {

		$imgView = Url::to('@web/backend/web/'.$value->image_file_location);

		if($value->image_over_title == 1){
			$committeeRowView.='<div class="col-md-3 col-md-offset-3 other-videotitle">
                    	<p>'.$value->image_top_title.'</p>
                        <p class="othervideo-date">'.$value->image_sub_title.'</p>
                        <img src="'.$imgView.'" />
                    </div>';
		}
		elseif($value->image_over_title == 2){
			$committeeRowView.='<div class="col-md-3 col-md-offset-right-3 other-videotitle">
                    	<p>'.$value->image_top_title.'</p>
                        <p class="othervideo-date">'.$value->image_sub_title.'</p>
                        <img src="'.$imgView.'" />
                    </div>';
		}
		elseif ($value->image_over_title == 7) {
			$committeeRowView.='<div class="col-md-3 col-md-offset-2 other-videotitle">
                    	<p>'.$value->image_top_title.'</p>
                        <p class="othervideo-date">'.$value->image_sub_title.'</p>
                        <img src="'.$imgView.'" />
                    </div>';
		}else{
			$committeeRowView.='<div class="col-md-3 other-videotitle">
                    	<p>'.$value->image_top_title.'</p>
                        <p class="othervideo-date">'.$value->image_sub_title.'</p>
                        <img src="'.$imgView.'" />
                    </div>';
		}
		
	}
?>

<section id="single_news" class="container secondary-page">
        <div class="general general-results">
            <div class="top-score-title col-md-9">
                <div class="video-desc">
                    <h3 class="video-other-old">Committee <span>Members</span><span class="point-little">.</span></h3>

                    <?= $committeeRowView ?>
                    <!-- <div class="col-md-3 col-md-offset-3 other-videotitle">
                        <p>Shri SM Krishna</p>
                        <p class="othervideo-date">President</p>
                        <img src="img/committee/SM-Krishna-President.jpg" />
                    </div>
                    <div class="col-md-3 other-videotitle">
                        <p>Shri R.Ashoka</p>
                        <p class="othervideo-date">Sr.Vice President</p>
                        <img src="img/committee/R-Ashoka-Sr.Vice-President.jpg" />
                    </div> 
                    <div class="col-md-3 other-videotitle"></div> <br clear="all" />
                    <div class="col-md-3 other-videotitle">
                        <p>Shri.Radhakrishna</p>
                        <p class="othervideo-date">Vice President</p>
                        <img src="img/committee/Shri-Radhakrishna-Vice-President.jpg" />
                    </div>
                    <div class="col-md-3 other-videotitle">
                        <p>M.Lakshminarayan IAS</p>
                        <p class="othervideo-date">Vice President</p>
                        <img src="img/committee/Shri.M-Lakshminarayanan-I-A-S-Vice-President.jpg" />
                    </div>
                    <div class="col-md-3 other-videotitle">
                        <p>Shri K.Jairaj, I.A.S.</p>
                        <p class="othervideo-date">Vice President</p>
                        <img src="img/committee/Shri-K.Jairaj-I.A.S-Vice-President.jpg" />
                    </div>
                    <div class="col-md-3 other-videotitle">
                        <p>Shri.Aravind Sitaraman</p>
                        <p class="othervideo-date">Vice President</p>
                        <img src="img/committee/Shri.Aravind-Sitaraman-Vice-President.jpg" />
                    </div> <br clear="all" />
                    <div class="col-md-3 col-md-offset-1 other-videotitle">
                        <p>Shri.C.S.Sunder Raju</p>
                        <p class="othervideo-date">Hon.Secretary</p>
                        <img src="img/committee/Shri.C.S.Sunder-Raju-Hon.Secretary.jpg" />
                    </div>
                    <div class="col-md-3 other-videotitle">
                        <p>Shri.P.R.Ramaswamy</p>
                        <p class="othervideo-date">Hon.Jt.Secretary</p>
                        <img src="img/committee/Shri.P.R.Ramaswamy-Hon-Jt-Secretary.jpg" />
                    </div>
                    <div class="col-md-3 other-videotitle">
                        <p>Shri B.N.S.Reddy IPS</p>
                        <p class="othervideo-date">Hon.Treasurer</p>
                        <img src="img/committee/Shri-B.N.S.Reddy-IPS-Hon-Treasurer.jpg" />
                    </div> -->
                </div>
            </div><!--Close Top Match-->

            <?= $this -> render('_sidebar') ?> 
            </div>
    </section>
	

