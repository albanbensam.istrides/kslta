<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\models\KslNewsTbl;
?>
<div class="col-md-3 right-column">
				<div class="top-score-title col-md-12 right-title">
					<h3>Latest News</h3>

					<!-- News Listing -->
					<?php
						$topnews_list=KslNewsTbl::find()->where(['news_link_status' => 'A', 'news_flat' => 'A'])->orderBy('news_lastupdated_date desc')->limit(4)->all(); 

			foreach ($topnews_list as $mainvalue) {
              //  $img_url_top = Url::to('@web/backend/web/'.$mainvalue->news_content_image);
                    $string_cutting_data = ucfirst($mainvalue->news_editor_content);

                     if (strlen($string_cutting_data) > 85) {
                                 // truncate string
                                    $stringCuted = substr($string_cutting_data, 0, 185);
                                // make sure it ends in a word so assassinate doesn't become ass...
                                $string_cutting_data = substr($stringCuted, 0, strrpos($stringCuted, ' ')).'... <a href="/this/story"></a>'; 
                            }


                         echo '<div class="right-content">
						<p class="news-title-right">
							'.$mainvalue->news_title.'
						</p>
						<div  style="font-size:13px;">
						<p class="txt-right" >
							'.$string_cutting_data.'
						</p>
						</div>
						<a href="'.Url::toRoute(['index.php/newsview', 'v' => $mainvalue->news_id]).'" style="text-decoration: none;"><i class="fa fa-angle-double-right img-djoko ca-more"src="'.$img_url_top.'"  alt="'.$mainvalue->news_title.'" /></i>more...</a>
					</div>';
				}

					 ?>

					<!-- <div class="right-content">
						<p class="news-title-right">
							James Ward clinches AirAsia Open
						</p>
						<p class="txt-right">
							James Ward of Great Britain played the best tennis of the week to clinch the AirAsia Open, $50,000 Challenger title defeating tournament top seed  
						</p>
						<a href="oct25.php" class="ca-more"><i class="fa fa-angle-double-right"></i>more...</a>
					</div>
					<div class="right-content">
						<p class="news-title-right">
							Underprivileged kids on ‘Magic Bus’ at AirAsia Open
						</p>
						<p class="txt-right">
							Two under privileged children from the Capital City of Delhi had a field day and had a wish of theirs come true when they got to meet and interact  
						</p>
						<a href="oct24-magic-buds.php" class="ca-more"><i class="fa fa-angle-double-right"></i>more...</a>
					</div>
					<div class="right-content">
						<p class="news-title-right">
							Saketh-Sanam clinch doubles crown
						</p>
						<p class="txt-right">
							The second seeded Indian pair of Saketh Myneni and Sanam Singh came from behind to claim the AirAsia Open, $50,000 Challenger doubles title with a  
						</p>
						<a href="oct24.php" class="ca-more"><i class="fa fa-angle-double-right"></i>more...</a>
					</div>
					<div class="right-content">
						<p class="news-title-right">
							Saketh lone Indian in last four
						</p>
						<p class="txt-right">
							Saketh Myneni was the last Indian standing when he defeated No 5 seed Yannick Mertens of Belgium in straight sets 6-3, 7-6 (6) to move into the last 
						</p>
						<a href="oct23.php" class="ca-more"><i class="fa fa-angle-double-right"></i>more...</a>
					</div> -->
					<!--<div class="right-content">
						<p class="news-title-right">
							Bega shocks Somdev to make quarters
						</p>
						<p class="txt-right">
							Unseeded Italian Alessandro Bega put his best foot forward to oust tournament top seed Somdev DevVarman in straight sets to coast into the last eight 
						</p>
						<a href="oct22.php" class="ca-more"><i class="fa fa-angle-double-right"></i>more...</a>
					</div>
					
					<div class="right-content">
											<p class="news-title-right">
												Saketh sails into last eight
											</p>
											<p class="txt-right">
												Tournament 3rd seed Saketh Myneni faced a few challenges from Australian Dane Propoggia before prevailing 6-4, 7-6 (9) to move into the last eight 
											</p>
											<a href="oct21.php" class="ca-more"><i class="fa fa-angle-double-right"></i>more...</a>
										</div>
										<div class="right-content">
											<p class="news-title-right">
												Sanam ousts Germain to move into round 2
											</p>
											<p class="txt-right">
												Unseeded Indian Sanam Singh caused the biggest upset of the AirAsia Open, $50,000 Challenger tournament accounting for tournament 8th seed Germain 
											</p>
											<a href="oct20.php" class="ca-more"><i class="fa fa-angle-double-right"></i>more...</a>
										</div>-->
					
				</div>
				
			</div>