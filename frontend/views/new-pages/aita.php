<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\PageContents;
use yii\helpers\Url;
use backend\models\ImagesView;
/* @var $this yii\web\View */
/* @var $searchModel backend\modelsTournamentScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this -> title = 'AITA-Women - Karnataka State Lawn Tennis Association';
$this -> params['breadcrumbs'][] = $this -> title;
?>

<style>
	.back-img-shop {
	    background: transparent url('<?= Url::to('@web/backend/web/'.$pageModel->page_content_img) ?>') no-repeat bottom center;
	    -webkit-background-size: cover;
	    -moz-background-size: cover;
	    -o-background-size: cover;
	    background-size: cover;
}

</style>

<section class="drawer">
	<div class="col-md-12 size-img back-img-shop">
		<div class="effect-cover">
			<h3 class="txt-advert animated"><?= $pageModel->page_content_top_main_heading ?></h3>
			<p class="txt-advert-sub">
				<?= $pageModel->page_content_top_sub_heading ?>
			</p>
		</div>
	</div>
	
	<!-- <section id="single_news" class="container secondary-page">
		<div class="general general-results">
			<div class="top-score-title col-md-9">
				<h3><?= $pageModel->page_content_heading ?></h3>
				<p class="desc_news">
					<?= $pageModel->page_main_content ?>
				</p>
				
				
				
			</div>
			<? //= $this -> render('_sidebar') ?> 
	</section> -->
<?php 
	$imgModel = ImagesView::find()->where(['image_title' => 'tournament'])->orderBy(['image_top_title' => 'SORT_ASC'])->all();

	

?>

    
    <section id="single_news" class="container secondary-page">
		<div class="general general-results">
			<div class="top-score-title col-md-9" style="margin-top: 40px;">
           		<h3 style="margin-top: 0px; text-align: center; margin-top: 50px;" class="news-title n-match">KSLTA Karnataka Times AITA-Women's Singles Draw</h3>
           		<img src="<?= Url::to('@web/backend/web/'.$imgModel[1]->image_file_location) ?>"/>
           		<br />
           		<h3 style="margin-top: 0px; text-align: center; margin-top: 50px;" class="news-title n-match">KSLTA Karnataka Times AITA-Women's Doubles Draw</h3>
           		<img src="<?= Url::to('@web/backend/web/'.$imgModel[2]->image_file_location) ?>"/>
           		<br />
           		<h3 style="margin-top: 0px; text-align: center; margin-top: 50px;" class="news-title n-match">Order of Play Friday</h3>
           		<img src="<?= Url::to('@web/backend/web/'.$imgModel[3]->image_file_location) ?>"/>
			</div>
			
			<div class="col-md-3 col-lg-3 col-sm-12 col-xs-12" style="margin-top: 40px;">
				<a href=""><img src="<?= Url::to('@web/backend/web/'.$imgModel[0]->image_file_location) ?>"  alt="Announcement" style="margin-bottom: 20px;"/></a>
				<br />
	        </div>
        </div>
    </section> 
</section>
