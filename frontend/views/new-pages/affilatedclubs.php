<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\PageContents;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\modelsTournamentScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this -> title = 'Affiliated Clubs - Karnataka State Lawn Tennis Association';
$this -> params['breadcrumbs'][] = $this -> title;
$this->registerMetaTag([
   'name' => $pagemodel->page_meta_name,
    'content' => $pagemodel->page_meta_content,
	]);
	
?><script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<style>
	.back-img-shop {
	    background: transparent url('<?= Url::to('@web/backend/web/'.$pageModel->page_content_img) ?>') no-repeat bottom center;
	    -webkit-background-size: cover;
	    -moz-background-size: cover;
	    -o-background-size: cover;
	    background-size: cover;
}

</style>
<script type="application/javascript" src="http://www.theciv.com/wp-content/themes/toolbox/js/jquery-1.9.1.js" /></script>

<section class="drawer">
	<div class="col-md-12 size-img back-img-shop">
		<div class="effect-cover">
			<h3 class="txt-advert animated"><?= $pageModel->page_content_top_main_heading ?></h3>
			<p class="txt-advert-sub">
				<?= $pageModel->page_content_top_sub_heading ?>
			</p>
		</div>
	</div>
	</section>
	<section id="single_news" class="container secondary-page">
		<div class="general general-results">
			<div class="top-score-title col-md-9">
				<h3><?= $pageModel->page_content_heading ?></h3>
				<p class="desc_news">
					<?= $pageModel->page_main_content ?>
				</p>
				
				
				
			</div><!--Close Top Match-->
			<?= $this -> render('_sidebar') ?> 
	</section>