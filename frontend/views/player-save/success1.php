<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modelsTournamentScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this -> title = 'KSLTA - Karnataka State Lawn Tennis Association';
$this -> params['breadcrumbs'][] = $this -> title;
?>
     
<section class="drawer">
	<div class="col-md-12 size-img back-img">
		<div class="effect-cover">
			<h3 class="txt-advert animated">Contact Us</h3>
			<!--
			<p class="txt-advert-sub">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
						</p>-->
			
		</div>
	</div>
	<section id="contact" class="secondary-page">
		<div class="general">
			<!--Google Maps-->
			<!--
			<div id="map_container">
							<div id="map_canvas"></div>
						</div>-->
			
			<div class="container">
				<div class="content-link col-md-12">
					<div id="contact_form" class="top-score-title col-md-8 align-center">
						<h3>Request Submitted <span>Successfully</span><span class="point-little">.</span></h3>
						 
					</div>
					
				</div>
			</div>
		</div>
	</section>