<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel backend\modelsTournamentScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this -> title = 'Tournament Schedules';
$this -> params['breadcrumbs'][] = $this -> title;
?>
