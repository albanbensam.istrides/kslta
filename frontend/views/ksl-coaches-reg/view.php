<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\KslCoachesReg */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Ksl Coaches Regs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-coaches-reg-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'dob',
            'gender',
            'p_address:ntext',
            'contact_no',
            'mob_no',
            'email_id:email',
            'e_qual',
            'tennis_qual',
            'certificate:ntext',
            'remarks:ntext',
            'active_status',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
