<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\KslCoachesRegSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ksl Coaches Regs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-coaches-reg-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ksl Coaches Reg', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'dob',
            'gender',
            'p_address:ntext',
            // 'contact_no',
            // 'mob_no',
            // 'email_id:email',
            // 'e_qual',
            // 'tennis_qual',
            // 'certificate:ntext',
            // 'remarks:ntext',
            // 'active_status',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
