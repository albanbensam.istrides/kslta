<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\models\KslAcademyReg */
/* @var $form yii\widgets\ActiveForm */
?>

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<?php

$form = ActiveForm::begin(['enableAjaxValidation' => true,'enableClientValidation' => true]);

?>
<div class="top-score-title col-md-12 right-title">
<h3><?= Html::encode($this->title) ?></h3>
</div>
<div class="col-md-12">

<div class="col-md-6">

    <?= $form -> field($model, 'name') -> textInput([ 'class' => 'form-control']) ?>
</div>
<div class="col-md-6">
    <?= $form -> field($model, 'dob') -> textInput([ 'class' => 'form-control']) ?>
</div>

<div class="col-md-6">
    <?= $form -> field($model, 'gender') ->radioList(['Male' => 'Male', 'Female' => 'Female']) ?>
</div>

<div class="col-md-6">
    <?= $form -> field($model, 'p_address') -> textArea([ 'rows' => '2']) ?>
</div>

<div class="col-md-6">
    <?= $form -> field($model, 'contact_no') -> textInput([ 'class' => 'form-control']) ?>
</div>

<div class="col-md-6">
    <?= $form -> field($model, 'mob_no') -> textInput([ 'class' => 'form-control']) ?>
</div>

<div class="col-md-6">
    <?= $form -> field($model, 'email_id') -> textInput([ 'class' => 'form-control']) ?>
</div>


<div class="col-md-6">
    <?= $form -> field($model, 'e_qual') -> textInput([ 'class' => 'form-control']) ?>
</div>


<div class="col-md-6">
    <?= $form -> field($model, 'tennis_qual') -> textInput([ 'class' => 'form-control']) ?>
</div>


<div class="col-md-6">
    <?= $form -> field($model, 'certificate') -> textInput([ 'class' => 'form-control']) ?>
</div>

<div class="col-md-6">
    <?= $form -> field($model, 'remarks') -> textInput([ 'class' => 'form-control']) ?>
</div>
</div>





<div class="form-group pull-right">

<button type="submit" class="btn btn-info">Submit</button>
</div>

<div id="alertmessage" style="color:red;"></div>
<div id="message" style="color:green;"></div>


<?php ActiveForm::end(); ?>

<style>
    .myCalendar {
    cursor: pointer;
}
</style><link href="<?php echo Url::home(true); ?>/frontend/web/js/dcalendar.picker.css" rel="stylesheet" type="text/css">
<script src="<?php echo Url::home(true); ?>/frontend/web/js/dcalendar.picker.js"></script>
<script>
$('#kslcoachesreg-dob').dcalendarpicker();
//$('#checkoutdate').dcalendarpicker();

$('select').select2({
  //selectOnClose: true,
  allowClear: true
});

</script>


