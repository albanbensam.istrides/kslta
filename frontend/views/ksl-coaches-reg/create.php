<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\KslCoachesReg */

$this->title = 'Coaches Registeration';
$this->params['breadcrumbs'][] = ['label' => 'Ksl Coaches Regs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-coaches-reg-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
