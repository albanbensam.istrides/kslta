<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\KslPlayerRegisterV1 */

$this->title = 'Player Registeration';
$this->params['breadcrumbs'][] = ['label' => 'Ksl Player Register V1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-player-register-v1-create">

    <!-- <h1><?//= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
