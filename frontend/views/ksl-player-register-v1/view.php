<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\KslPlayerRegisterV1 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ksl Player Register V1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-player-register-v1-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'last_name',
            'dob',
            'place_of_birth',
            'father_name',
            'mother_name',
            'guardian_details',
            'address:ntext',
            'contact_no',
            'email_id:email',
            'active_status',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
