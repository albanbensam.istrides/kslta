<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\KslPlayerRegisterV1Serach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ksl Player Register V1s';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ksl-player-register-v1-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ksl Player Register V1', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'first_name',
            'last_name',
            'dob',
            'place_of_birth',
            // 'father_name',
            // 'mother_name',
            // 'guardian_details',
            // 'address:ntext',
            // 'contact_no',
            // 'email_id:email',
            // 'active_status',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
