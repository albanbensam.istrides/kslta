<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\KslTournamentsTbl;
use backend\models\GalleryPageModel;
use backend\models\KslGalleryModel;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
$request = Yii::$app->request;


/* @var $this yii\web\View */
/* @var $searchModel backend\modelsTournamentScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this -> title = 'Tournament';
$this -> params['breadcrumbs'][] = $this -> title;
?>
 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
        <script src="<?php echo Url::to('@web/frontend/web/lightGallery/dist/js/lightgallery-all.min.js');?>"></script> 
        <script src="<?php echo Url::to('@web/frontend/web/lightGallery/lib/jquery.mousewheel.min.js');?>"></script>
         <script src=" <?php echo Url::to('@web/frontend/web/lightGallery/dist/js/jquery.colorbox.js');?>"></script>
<!--SECTION CONTAINER SLIDER-->
 <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.3.2/css/lightgallery.css" />
    <script src='https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js'></script>
       <script src="<?php echo Url::to('@web/frontend/web/lightGallery/demo/js/lightgallery.min.js'); ?>"></script>
        <script src="<?php echo Url::to('@web/frontend/web/lightGallery/demo/js/lg-fullscreen.js');?>"></script>
        <script src="<?php echo Url::to('@web/frontend/web/lightGallery/demo/js/lg-thumbnail.js');?>"></script>
        <script src="<?php echo Url::to('@web/frontend/web/lightGallery/demo/js/lg-video.js');?>"></script>
        <script src="<?php echo Url::to('@web/frontend/web/lightGallery/demo/js/lg-autoplay.js');?>"></script>
        <script src="<?php echo Url::to('@web/frontend/web/lightGallery/demo/js/lg-zoom.js');?>"></script>
        <script src=" <?php echo Url::to('@web/frontend/web/lightGallery/demo/js/lg-hash.js');?>"></script>
        <script src=" <?php echo Url::to('@web/frontend/web/lightGallery/demo/js/lg-pager.js');?>"></script> -->
       
 <script src="https://www.facebook.com/rsrc.php/v3ipwU4/ym/l/en_US/f_MPpVOF3lH.js"></script>
 <link href="frontend/web/lightGallery/dist/css/lightgallery.css" rel="stylesheet">
 <link href="frontend/web/lightGallery/dist/css/colorbox.css" rel="stylesheet">
 <script>
			$(document).ready(function(){
				//Examples of how to assign the Colorbox event to elements
				$(".group1").colorbox({rel:'group1'});
				$(".group2").colorbox({rel:'group2', transition:"fade"});
				$(".group3").colorbox({rel:'group3', transition:"none", width:"75%", height:"75%"});
				$(".group4").colorbox({rel:'group4', slideshow:true});
				$(".ajax").colorbox();
				$(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
				$(".vimeo").colorbox({iframe:true, innerWidth:500, innerHeight:409});
				$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
				$(".inline").colorbox({inline:true, width:"50%"});
				$(".callbacks").colorbox({
					onOpen:function(){ alert('onOpen: colorbox is about to open'); },
					onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
					onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
					onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
					onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
				});

				$('.non-retina').colorbox({rel:'group5', transition:'none'})
				$('.retina').colorbox({rel:'group5', transition:'none', retinaImage:true, retinaUrl:true});
				
				//Example of preserving a JavaScript event for inline calls.
				$("#click").click(function(){ 
					$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
					return false;
				});
			});
		</script>
<section class="drawer">
	<div class="col-md-12 size-img back-img-shop">
		<div class="effect-cover">
			<h3 class="txt-advert animated">Gallery </h3>
			<p class="txt-advert-sub">
				Photo gallery with the best action in the tournament.
			</p>
		</div>
	</div>
	<section id="summary" class="container secondary-page">
		<div class="general general-results tournaments">
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="margin-top: 40px;">
				 <body>
		 
		<?php $home_gallery = GalleryPageModel::find()->groupBy('gal_image_rootid')->all(); ?>
				
				
				<?php  
				$i=1;
				$class='';
				foreach ($home_gallery as  $value) {
					$homegallery1 = KslGalleryModel::find()->where(['gallery_id'=>$value->gal_image_rootid])->all();
					foreach ($homegallery1 as $key ) {
						$title=$key->gallery_title;
					}
					
				//	$image_data = Url::to('@web/backend/web/galleryimage/gallery_pages/'.$value->gal_image_path);
					 $image_='<img class="gallery_data" style="width: 280px; height: 156px;" src='.Url::to('@web/backend/web/galleryimage/gallery_pages/'.$value->gal_image_path).' alt='.$value->gal_image_rootid.' />';
					
					 $home_gallery1 = GalleryPageModel::find()->where(['gal_image_rootid'=>$value->gal_image_rootid])->all();
					$k=1;
					foreach ($home_gallery1 as $values) {
						 $image_data = Url::to('@web/backend/web/galleryimage/gallery_pages/'.$values->gal_image_path);
					if($k==1){
						$class= 'test123';
						?>
						<div class="col-md-3 hovereffect" style="margin-bottom: 30px;" ><a class="group<?php echo $i;?>"  href="<?php echo $image_data;?>" ><?php echo $image_; ?><div class="overlay">
                 <i class="fa fa-camera" aria-hidden="true"></i>
                  <p style="padding-top: 5px;"><?php echo $title;?></p>
           		 </div></a> </div>
			<?php	} 
					else{?>
					 
					<p style="margin: 0px 0px 0px 0px;"><a class="group<?php echo $i;?> <?php echo $class;?>"  href="<?php echo $image_data;?>">
									
							
					
					
					</a></p>
					
					
					<?php
					}
					
					++$k;
					
					}?>
					
					<script>
			$(document).ready(function(){
								
				$(".group"+<?php echo $i; ?>).colorbox({rel:'group'+<?php echo $i; ?>, transition:"fade"});
				
			});
			</script>
					
				<?php 
				++$i;
				} 
				?>
	
				
		<div class="gallerydown" style="padding: 7px 16px 10px 0px;">
		 
		
		<div class="toper2" style="padding-top: 14px">
			<?php
			/* $home_gallery = GalleryPageModel::find()->groupBy('gal_image_rootid')->orderBy('gal_image_rootid desc')->all(); 
		
   			
			foreach ($home_gallery as $gallery_imgvalue) {
				$link_data = Url::to('@web/backend/web/galleryimage/gallery_pages/'.$gallery_imgvalue->gal_image_path);
				$image_data='<img class="gallery_data" style="width: 280px; height: 156px;" src="'.Url::to('@web/backend/web/galleryimage/gallery_pages/'.$gallery_imgvalue->gal_image_path).'" alt="KALABURAGI OPEN GALLERY." />';
				echo '<div class="col-md-3 col-xs-12">	
           		<div class="mart">		
             <a href="'.$link_data.'" rel="prettyPhoto[portfolio]">'.$image_data.'</a>
            <div class="image" style="padding-top: 10px;">
         
             <p style="padding-top: 5px;"></p>
             </div>
          </div>
		 
		  </div>
		  ';
			}
			/*   <img src="frontend/web/images/camera.png" alt=""/>&nbsp;&nbsp;<span class="gallery" style="#11cbc6">Gallery</span>*/
			 ?> 
		
	  
		  </div> 
		  
		 
	</div>
	</div>
			
	</section>
</section>
<style>
	.mart{
		float:left; 
		height: 250px;
		 
	}
</style>
	
		<style type="text/css" media="screen">
			* { margin: 0; padding: 0; }
			.pp_social{
				display:none !important;
			}
 #cboxPrevious:before {
    content: "\f190";
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
    text-decoration: inherit;
/*--adjust as necessary--*/
    color: #000;
    font-size: 30px;
    padding-right: 0.5em;
    position: absolute;
    top: -380px;
    left: 30px;
    padding: 8px;
    background: #fff;
    border-radius: 15px;
        border: none !important;
    outline: none !important;
}
#cboxNext:before {
    content: "\f18e";
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
    text-decoration: inherit;
/*--adjust as necessary--*/
    color: #000;
    font-size: 30px;
    padding-right: 0.5em;
    position: absolute;
    top: -380px;
    right: 30px;
    padding: 8px;
    background: #fff;
    border-radius: 15px;
       border: none !important;
    outline: none !important;
}	
#cboxNext:focus{
    outline: none;
    border-color: inherit;
    -webkit-box-shadow: none;
     box-shadow: none;
}
#cboxPrevious:focus{
    outline: none;
    border-color: inherit;
    -webkit-box-shadow: none;
     box-shadow: none;
}
		</style>
<script>
	$(function () {
    "use strict";
    $(".portfolio a").hover(function () {
        $(this).children("img").animate({ opacity: 0.55 }, "fast");
    }, function () {
        $(this).children("img").animate({ opacity: 1.0 }, "slow");
    });

    $("a[rel^='prettyPhoto']").prettyPhoto({
        animation_speed: 'fast', /* fast/slow/normal */
        slideshow: 5000, /* false OR interval time in ms */
        autoplay_slideshow: false, /* true/false */
        opacity: 0.80, /* Value between 0 and 1 */
        show_title: true, /* true/false */
        allow_resize: true, /* Resize the photos bigger than viewport. true/false */
        default_width: 500,
        default_height: 344,
        counter_separator_label: '/', /* The separator for the gallery counter 1 "of" 2 */
        theme: 'pp_default', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
        horizontal_padding: 20, /* The padding on each side of the picture */
        hideflash: false, /* Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto */
        wmode: 'opaque' /* Set the flash wmode attribute */
    });
});
</script>
<style>
#cboxPrevious{
    border: none !important;
    outline: none !important;
}
#cboxNext{
    border: none !important;
    outline: none !important;
}
  #cboxLoadedContent{
 	width: 962px !important;
 	 overflow: hidden !important;
 	  height: 609px !important;
 }
 #cboxContent{
   width: 962px !important;
   height: 609px !important;
   float: left;
 }
 .cboxPhoto{
  cursor: pointer;
   width: 962px !important;
   height: auto !important;
   float: none;
 }
 #cboxOverlay {
    background: #555;
   }
 #colorbox{
 display: block;
 visibility: visible;
 top: 2471px;
 left: 185px !important; 
 position: absolute;
 width: 962px !important;
 height: 609px !important;
 opacity: 1;
 }
  
 .flex-control-nav{
 	display: none;
 }
	.mart{
		float:left; 
		/*height: 200px;*/
		 
	}
	
	.fa-camera{
		color:#fff;
		font-size: 40px;
	}
	.hovereffect {
  overflow: hidden;
  position: relative;
  text-align: center;
  cursor: pointer;
}

.hovereffect .overlay {
  position: absolute;
  overflow: hidden;
  width: 80%;
  height: 80%;
  left: 10%;
  top: 10%;
  /*border-bottom: 1px solid #FFF;
  border-top: 1px solid #FFF;*/
  -webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
  transition: opacity 0.35s, transform 0.35s;
  -webkit-transform: scale(0,1);
  -ms-transform: scale(0,1);
  transform: scale(0,1);
}

.hovereffect:hover .overlay {
  opacity: 1;
  filter: alpha(opacity=100);
  -webkit-transform: scale(1);
  -ms-transform: scale(1);
  transform: scale(1);
}

.hovereffect img {
  display: block;
  position: relative;
  -webkit-transition: all 0.35s;
  transition: all 0.35s;
}
hr{
	width:280px;
}
.hovereffect:hover img {
  /*filter: url('data:image/svg+xml;charset=utf-8,<svg xmlns="http://www.w3.org/2000/svg"><filter id="filter"><feComponentTransfer color-interpolation-filters="sRGB"><feFuncR type="linear" slope="0.6" /><feFuncG type="linear" slope="0.6" /><feFuncB type="linear" slope="0.6" /></feComponentTransfer></filter></svg>#filter');
  
  */
 filter: brightness(0.6);
  -webkit-filter: brightness(0.6);
}

.hovereffect h2 {
  text-transform: uppercase;
  text-align: center;
  position: relative;
  font-size: 17px;
  background-color: transparent;
  color: #FFF;
  padding: 1em 0;
  opacity: 0;
  filter: alpha(opacity=0);
  -webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
  transition: opacity 0.35s, transform 0.35s;
  -webkit-transform: translate3d(0,-100%,0);
  transform: translate3d(0,-100%,0);
}

.hovereffect a, .hovereffect p {
  color: #FFF;
  padding: 1em 0;
  opacity: 1;
  filter: alpha(opacity=0);
  -webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
  transition: opacity 0.35s, transform 0.35s;
  -webkit-transform: translate3d(0,100%,0);
  transform: translate3d(0,100%,0);
}

.hovereffect:hover a, .hovereffect:hover p, .hovereffect:hover h2 {
  opacity: 1;
  filter: alpha(opacity=100);
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
}
a:hover, a:focus {
    color: #2a6496;
    text-decoration: none;
}
.portfolio img {
	border: none;	
}
</style>