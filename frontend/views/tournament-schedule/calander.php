<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\KslTournamentsTbl;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\modelsTournamentScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this -> title = 'Calendar - Karnataka State Lawn Tennis Association';
$this -> params['breadcrumbs'][] = $this -> title;
?>
<?php $all_tournament=KslTournamentsTbl::find()->orderby(['tourn_from_date'=> SORT_ASC])->all();
$calender_data=array();
foreach($all_tournament as $one_tournament){
	$date_key=date('M Y', strtotime($one_tournament->tourn_from_date));
	$calender_data[$date_key][]=$one_tournament;
}
?>
<section class="drawer">
    <div class="col-md-12 size-img back-img">
        <div class="effect-cover">
        <h3 class="txt-advert animated">ATP Challenger Bangalore - Calendar</h3>
        <p class="txt-advert-sub animated">Only the best eight singles players and best eight doubles teams.</p>
        </div>
    </div>
    <section id="summary" class="container secondary-page">
      <div class="general general-results tournaments">
           
           <div id="c-calend" class="top-score-title right-score col-md-9">
                <h3><?php echo date("Y"); ?> <span>CALENDAR</span><span class="point-little">.</span></h3>
                <!--
                <p class="txt-right">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                                   quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                <p class="txt-right txt-dd-second">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>-->
              
           <?php
           $calender_txt='';
           $section_val=1;
					if (count($calender_data) > 0) {
						foreach ($calender_data as $month_key => $onemonth) {
							$calender_txt.='<div class="accordion" id="section'.($section_val++).'"><i class="fa fa-calendar-o"></i>'.$month_key.'<span></span></div>';
							$calender_txt1='';
							$calender_txt2='';
							$need_money='';
							if(count($onemonth)>0){
								foreach ($onemonth as $one_event){									
									if($one_event -> tourn_price_money!=""){$need_money="yes";}
								}							
								foreach ($onemonth as $one_event){
									$calender_txt2.='<div class="col-md-3 t1"><p><p>'.date('d M Y', strtotime($one_event -> tourn_from_date)).'</p></div>';
									if($need_money=="yes"){
										 $calender_txt2.='<div class="col-md-4 t2">'.$one_event -> tourn_title.'</p></div>';
										$tourn_price_money='';
										if($one_event -> tourn_price_money!=""){$tourn_price_money="".$one_event -> tourn_price_money;}
										$calender_txt2.='<div class="col-md-2 t3"><p>'.$tourn_price_money.'</p></div>';
			                            $calender_txt2.='<div class="col-md-2 t5"><p>'.$one_event -> tourn_venue_city.'</p></div>';	
									}else{
			                            $calender_txt2.='<div class="col-md-5 t2">'.$one_event -> tourn_title.'</p></div>';
			                            $calender_txt2.='<div class="col-md-3 t5"><p>'.$one_event -> tourn_venue_city.'</p></div>';	
		                            }
									$calender_txt2.='<div class="col-md-1 t6"><a href="'.Url::toRoute(['tournament/'.$one_event->tourn_id]).'">View</a></div>';						
									$calender_txt2.='<div class="acc-footer"></div>';
								}
								if($need_money=="yes"){
									$calender_txt1.='<div class="acc-content">
		                            <div class="col-md-3 acc-title">Date</div>
		                            <div class="col-md-4 acc-title">Event</div>
		                            <div class="col-md-2 acc-title">Price Money</div>
		                            <div class="col-md-2 acc-title">Venue</div>';
								}else{
									$calender_txt1.='<div class="acc-content">
		                            <div class="col-md-3 acc-title">Date</div>
		                            <div class="col-md-5 acc-title">Event</div>
		                            <div class="col-md-3 acc-title">Venue</div>';	
								}
								$calender_txt.=$calender_txt1.$calender_txt2.'</div>';
							}
					}
					}
				echo $calender_txt;
				?>
           </div><!--Close Top Match-->
                 <?= $this -> render('_sidebar') ?>     
       </div>
    </section> 
    <script type="text/javascript" src="<?php echo Url::base(); ?>/frontend/web/js/jquery-1.7.1.min.js"></script>
    	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    	<script src="<?php echo Url::base(); ?>/frontend/web/js/jquery.accordion.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                "use strict";
                $('.accordion').accordion({ defaultOpen: 'section1' }); //some_id section1 in demo
            });
        });
    </script>