<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\PageContents;
use backend\models\ImagesView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\modelsTournamentScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this -> title = 'Committee - Karnataka State Lawn Tennis Association';
$this -> params['breadcrumbs'][] = $this -> title;
$this->registerMetaTag([
   //'name' => $pagemodel->page_meta_name,
   //'content' => $pagemodel->page_meta_content,
	]);
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<style>
	.back-img-shop {
	    background: transparent url('<?= Url::to('@web/backend/web/'.$pageModel->page_content_img) ?>') no-repeat bottom center;
	    -webkit-background-size: cover;
	    -moz-background-size: cover;
	    -o-background-size: cover;
	    background-size: cover;
}

</style>

<section class="drawer">
	<div class="col-md-12 size-img back-img-shop">
		<div class="effect-cover">
			<h3 class="txt-advert animated"><?= $pageModel->page_content_top_main_heading ?></h3>
			<p class="txt-advert-sub">
				<?= $pageModel->page_content_top_sub_heading ?>
			</p>
		</div>
	</div>
	</section>
<?php 
	$committeeModel = ImagesView::find()->where(['image_title' => 'commitee'])->orderBy(['image_over_title' => 'SORT_ASC'])->all();
	
	$committeeRowView = '';
	foreach ($committeeModel as $key => $value) {

		$imgView = Url::to('@web/backend/web/'.$value->image_file_location);

		if($value->image_over_title == 1){
			$committeeRowView.='<div class="col-md-3 col-md-offset-3 ">
                    	<p>'.$value->image_top_title.'</p>
                        <p class="othervideo-date">'.$value->image_sub_title.'</p>
                        <img src="'.$imgView.'"  style="margin-bottom:40px;"/>
                    </div>';
		}
elseif($value->image_over_title == 2){
            $committeeRowView.='<div class="col-md-3 col-md-offset-right-3 ">
                        <p>'.$value->image_top_title.'</p>
                        <p class="othervideo-date">'.$value->image_sub_title.'</p>
                        <img src="'.$imgView.'" style="margin-bottom:40px;"/>
                    </div>';
        }
        elseif ($value->image_over_title == 7) {
			$committeeRowView.='<div class="col-md-3 col-md-offset-1 ">
                    	<p>'.$value->image_top_title.'</p>
                        <p class="othervideo-date">'.$value->image_sub_title.'</p>
                        <img src="'.$imgView.'" style="margin-bottom:40px;"/>
                    </div>';
		}else{
			$committeeRowView.='<div class="col-md-3 ">
                    	<p>'.$value->image_top_title.'</p>
                        <p class="othervideo-date">'.$value->image_sub_title.'</p>
                        <img src="'.$imgView.'" style="margin-bottom:40px;"/>
                    </div>';
		}
		
	}
?>


	<section id="single_news" class="container secondary-page">
		<div class="general general-results">
			<div class="top-score-title col-md-9">
				<div class="video-desc">
                    <h3 class="video-other-old">Committee <span>Members</span><span class="point-little">.</span></h3>

                    <?= $committeeRowView ?>
                </div>
			</div><!--Close Top Match-->

			<?= $this -> render('_sidebar') ?> 
			</div>
	</section>