<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\modelsTournamentScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this -> title = $pagemodel->page_content_title;
$this -> params['breadcrumbs'][] = $this -> title;
$this->registerMetaTag([
   'name' => $pagemodel->page_meta_name,
    'content' => $pagemodel->page_meta_content,
	]);
?>

<?php if($pagemodel->page_content_img != '') { ?>
<style>
  .back-img-shop {
      background: transparent url('<?= Url::to('@web/backend/web/'.$pagemodel->page_content_img) ?>') no-repeat bottom center;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
}

</style>

<?php } ?>


<section class="drawer">
  <div class="col-md-12 size-img back-img-shop">
    <div class="effect-cover">
      <h3 class="txt-advert animated"><?= $pagemodel->page_content_top_main_heading ?></h3>
      <p class="txt-advert-sub">
         <?= $pagemodel->page_content_top_sub_heading ?>
      </p>
    </div>
  </div>
  <section id="summary" class="container secondary-page">
    <div class="general general-results tournaments">
      <div class="top-score-title right-score col-md-9">
        <h3><?= $pagemodel->page_content_heading ?></h3>
        <!-- Category Filter -->
        <!--
        <dl class="group">
                  <dd>
                    <ul class="filter group albumFilter">
                      <li data-filter="*" class="current">
                        <a  href="#">ALL</a>
                      </li>
                      <li data-filter=".cat1">
                        <a  href="#">ATP</a>
                      </li>
                      <li data-filter=".cat2">
                        <a  href="#">WTP</a>
                      </li>
                      <li data-filter=".cat3">
                        <a  href="#">BEAUTY</a>
                      </li>
                      <li data-filter=".cat4">
                        <a  href="#">MEETING</a>
                      </li>
                    </ul>
                  </dd>
                </dl>-->
        
        <!-- Portfolio Items -->
        <ul class="portfolio group albumContainer">

          <!-- Portfolio Items -->
        <?php 
              /* Image View */
              foreach ($gallerymodel as $val) {
                    $imgPath = Url::to('@web/backend/web/galleryimage/gallery_pages/'.$val->gal_image_path);
                    echo '<li class="item block cat2">
                          <a href='.$imgPath.' rel="prettyPhoto[portfolio]"><img src='.$imgPath.'  alt="" /></a>
                        </li>';
              }

        ?>

          
        </ul>
      </div><!--Close Top Match-->
      <div class="col-md-3 right-column">
      
      </div>  
       <?= $this -> render('_facilitysidemenu') ?>
     <?= $this -> render('_sidebar') ?>
  </section>
</section>
 <script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    "use strict";
    $(".portfolio a").hover(function() {
      $(this).children("img").animate({
        opacity : 0.75
      }, "fast");
    }, function() {
      $(this).children("img").animate({
        opacity : 1.0
      }, "slow");
    });
    $("a[rel^='prettyPhoto']").prettyPhoto({
      animation_speed : 'fast', /* fast/slow/normal */
      slideshow : 5000, /* false OR interval time in ms */
      autoplay_slideshow : false, /* true/false */
      opacity : 0.80, /* Value between 0 and 1 */
      show_title : true, /* true/false */
      allow_resize : true, /* Resize the photos bigger than viewport. true/false */
      default_width : 500,
      default_height : 344,
      counter_separator_label : '/', /* The separator for the gallery counter 1 "of" 2 */
      theme : 'pp_default', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
      horizontal_padding : 20, /* The padding on each side of the picture */
      hideflash : false, /* Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto */
      wmode : 'opaque' /* Set the flash wmode attribute */
    });
  }); 
</script>