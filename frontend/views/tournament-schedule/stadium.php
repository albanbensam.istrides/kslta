<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\modelsTournamentScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this -> title = $pagemodel->page_content_title;
$this -> params['breadcrumbs'][] = $this -> title;
?>
<section class="drawer">
  <div class="col-md-12 size-img back-img-shop">
    <div class="effect-cover">
      <h3 class="txt-advert animated">KSLTA FACILITIES</h3>
      <p class="txt-advert-sub">
        Photo gallery with the best in class.
      </p>
    </div>
  </div>
  <section id="summary" class="container secondary-page">
    <div class="general general-results tournaments">
      <div class="top-score-title right-score col-md-9">
        <h3>KSLTA <span>STADIUM</span><span class="point-little">.</span></h3>
        <!-- Category Filter -->
        <!--
        <dl class="group">
                  <dd>
                    <ul class="filter group albumFilter">
                      <li data-filter="*" class="current">
                        <a  href="#">ALL</a>
                      </li>
                      <li data-filter=".cat1">
                        <a  href="#">ATP</a>
                      </li>
                      <li data-filter=".cat2">
                        <a  href="#">WTP</a>
                      </li>
                      <li data-filter=".cat3">
                        <a  href="#">BEAUTY</a>
                      </li>
                      <li data-filter=".cat4">
                        <a  href="#">MEETING</a>
                      </li>
                    </ul>
                  </dd>
                </dl>-->
        
        <!-- Portfolio Items -->
        <ul class="portfolio group albumContainer">

          <?php 
              /* Image View */
              foreach ($gallerymodel as $val) {
                    $imgPath = Url::to('@web/backend/web/galleryimage/gallery_pages/'.$val->gal_image_path);
                    echo '<li class="item block cat2">
                          <a href='.$imgPath.' rel="prettyPhoto[portfolio]"><img src='.$imgPath.'  alt="" /></a>
                        </li>';
              }

        ?>

        </ul>
      </div><!--Close Top Match-->
      <div class="col-md-3 right-column">
        <div class="top-score-title prod-cat col-md-12">
          <h3>Other Facilities</h3>
          <div class="products-content">
            <a href="swimming-pool.php" class="news-title-right">Swimming Pool</a>
            <a href="club-house.php" class="news-title-right">Guest Room</a>
            <a href="bar-restaurant.php" class="news-title-right">Bar & Restaurant</a>
            <a href="party-hall.php" class="news-title-right">Party Hall & VIP Lounge</a>
            <a href="tennis-billiards.php" class="news-title-right">Table Tennis & Billiards</a>
            <a href="stadium.php" class="news-title-right">Stadium</a>
          </div>
        </div>
      </div>  
      <?= $this -> render('_sidebar') ?> 
  </section>
</section>
 <script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    "use strict";
    $(".portfolio a").hover(function() {
      $(this).children("img").animate({
        opacity : 0.75
      }, "fast");
    }, function() {
      $(this).children("img").animate({
        opacity : 1.0
      }, "slow");
    });
    $("a[rel^='prettyPhoto']").prettyPhoto({
      animation_speed : 'fast', /* fast/slow/normal */
      slideshow : 5000, /* false OR interval time in ms */
      autoplay_slideshow : false, /* true/false */
      opacity : 0.80, /* Value between 0 and 1 */
      show_title : true, /* true/false */
      allow_resize : true, /* Resize the photos bigger than viewport. true/false */
      default_width : 500,
      default_height : 344,
      counter_separator_label : '/', /* The separator for the gallery counter 1 "of" 2 */
      theme : 'pp_default', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
      horizontal_padding : 20, /* The padding on each side of the picture */
      hideflash : false, /* Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto */
      wmode : 'opaque' /* Set the flash wmode attribute */
    });
  }); 
</script>


<script type="text/javascript">

    $(window).load(function () {
        "use strict";
        var $container = $('.albumContainer');
        $container.isotope({
            filter: '*',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });

        $('.albumFilter li').on('click', function (e) {
            $('.albumFilter .current').removeClass('current');
            $(this).addClass('current');

            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });
            return false;
        });
    });

</script>