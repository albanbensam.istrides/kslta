<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\KslTournamentsTbl;
use backend\models\GalleryPageModel;
use backend\models\KslGalleryModel;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
$request = Yii::$app->request;
/* @var $this yii\web\View */
/* @var $searchModel backend\modelsTournamentScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this -> title = 'Tournament';
$this -> params['breadcrumbs'][] = $this -> title;
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>


<?php 
if (Yii::$app->request->get('t')){
 	$tournament_id=$request->get('t');
	$one_drawsheet=ArrayHelper::toArray(KslTournamentsTbl::find()->where(['tourn_id'=>$tournament_id])->all());

	$galleryArr = array();//Gallery Choose Array
	$pageHeading = '';
	foreach ($tornmultiGallery as $galleryvalue) {
		array_push($galleryArr,$galleryvalue->gallery_id);
	}


	/* For Heading */

	foreach ($galleryDetails as $gvalue) {
		$pageHeading = $gvalue->gallery_title;
	}

				

	/* Heading End */
	
	$mainContentGallery = 0;
	if(isset($_GET['listgal'])){
		$mainContentGallery = $_GET['listgal'];
		$pageHeading = $galleryManage->gallery_title;
	}else{
		$mainContentGallery = $galleryArr[0];
	}	

	$galleyData = GalleryPageModel::find()->where(['gal_image_rootid' => $mainContentGallery])->orderby(['gal_image_order'=> SORT_ASC])->all();


	
}
	?> 
	
        
<section class="drawer">
	<div class="col-md-12 size-img back-img-shop">
		<div class="effect-cover">
			<h3 class="txt-advert animated"><?= $one_drawsheet[0]['tourn_title'] ?></h3>
			<p class="txt-advert-sub">
				Photo gallery with the best action in the tournament.
			</p>
		</div>
	</div>
	<section id="summary" class="container secondary-page">
		<div class="general general-results tournaments">
			<div class="top-score-title right-score col-md-9">
				<h3><?= $pageHeading  ?><span class="point-little">.</span></h3>
				<!-- Category Filter -->
				<!--
				<dl class="group">
									<dd>
										<ul class="filter group albumFilter">
											<li data-filter="*" class="current">
												<a  href="#">ALL</a>
											</li>
											<li data-filter=".cat1">
												<a  href="#">ATP</a>
											</li>
											<li data-filter=".cat2">
												<a  href="#">WTP</a>
											</li>
											<li data-filter=".cat3">
												<a  href="#">BEAUTY</a>
											</li>
											<li data-filter=".cat4">
												<a  href="#">MEETING</a>
											</li>
										</ul>
									</dd>
								</dl>-->
				
				<!-- Portfolio Items -->
				<ul class="portfolio group albumContainer">
					<?php 
              /* Image View */
              foreach ($galleyData as $val) {
                    $imgPath = Url::to('@web/backend/web/galleryimage/gallery_pages/'.$val->gal_image_path);
                    echo '<li class="item block cat2">
                          <a href='.$imgPath.' rel="prettyPhoto[portfolio]"><img src='.$imgPath.'  alt="" /></a>
                        </li>';
			              }

			        ?>

				</ul>
			</div><!--Close Top Match-->
			<div class="col-md-3 right-column">
				<div class="top-score-title prod-cat col-md-12">
	                <h3>Gallery</h3>

	                <?php 

	                for($k = 0; $k < count($galleryArr) ; $k++){
						$galleyLinkRedirectionData = KslGalleryModel::find()->where(['gallery_id' => $galleryArr[$k]])->all();
							
			              foreach ($galleyLinkRedirectionData as $valforgallerylinks)
			               {
			                    //$imgPath = Url::to('@web/backend/web/galleryimage/gallery_pages/'.$val->gal_image_path);
			                    // echo '<li class="item block cat2">
			                    //       <a href='.$imgPath.' rel="prettyPhoto[portfolio]"><img src='.$imgPath.'  alt="" /></a>
			                    //     </li>';

			              echo '<div class="products-content ksltaGalleryList">
				                    <a href="'.Url::toRoute(['index.php/tourn-gallery-show', 't' => $tournament_id, 'listgal'=>$valforgallerylinks->gallery_id]).'" class="news-title-right" id = "'.$valforgallerylinks->gallery_id.'" >'.$valforgallerylinks->gallery_title.'</a>
				                </div>';
						    }

					}
              
			        ?>

				</div>
			</div>	
			 <?= $this -> render('_sidebar') ?>
	</section>
	
</section>

