<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\KslTournamentsTbl;
use backend\models\TournamentSchedule;
use backend\models\TournamentsEventResult;
use backend\models\TournamentsEventMatche;
use backend\models\PlayerRegister;
use backend\models\TournamentEventModel;
use backend\models\EventMatches;
use backend\models\DropdownManagement;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
$request = Yii::$app->request;


$this -> title = 'Tournament';
$this -> params['breadcrumbs'][] = $this -> title;
?>
<?php 
if (Yii::$app->request->get('t')){
 	$tournament_id=$request->get('t');
	$one_tournament=ArrayHelper::toArray(KslTournamentsTbl::find()->where(['tourn_id'=>$tournament_id])->all());
}
function getDatesFromRange($start, $end, $format = 'Y-m-d') {
    $array = array();
    $interval = new DateInterval('P1D');

    $realEnd = new DateTime($end);
    $realEnd->add($interval);

    $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

    foreach($period as $date) { 
        $array[] = $date->format($format); 
    }

    return $array;
}
$tournament_title='';
if(count($one_tournament[0])>0){
	$dates_tournament = getDatesFromRange($one_tournament[0]['tourn_from_date'], $one_tournament[0]['tourn_to_date']);
	$tournament_title=$one_tournament[0]['tourn_title'];
}

$event_result_db=ArrayHelper::toArray(TournamentsEventResult::find()->where(['ter_tournament_id'=>$tournament_id])->all());
$event_result_array=array();
foreach($event_result_db as $one_result){
	$event_result_array[$one_result['ter_event_id']][$one_result['ter_player_id']]=$one_result;
}
$event_result_db1=ArrayHelper::toArray(TournamentsEventMatche::find()->where(['tem_tournament_id'=>$tournament_id])->all());
$event_matches_array=array();
foreach($event_result_db1 as $one_result){
	$event_matches_array[$one_result['tem_event_id']][$one_result['tem_player_id']]=$one_result;
}
//$player_register_db=ArrayHelper::toArray(PlayerRegister::find()->where(['tourn_id'=>$tournament_id])->all());
$player_register_db=ArrayHelper::toArray(PlayerRegister::find()->all());
$player_details_array=array();
foreach($player_register_db as $one_player){
	$player_details_array[$one_player['reg_player_id']]=$one_player;
}
$model_matches = ArrayHelper::toArray(TournamentSchedule::find()->where(['ts_tournament_id'=>$tournament_id])->all());
$final_array=array();
foreach($model_matches as $event_list){	
	$tournament_result=json_decode($event_list['ts_tournament_result']);
	$i=0;$j=0;
	foreach($tournament_result->results[0] as $one_key=>$oneresult){
			foreach($oneresult as $key_match=>$one_match){
				foreach($one_match as $one_player_result){								
					if(is_string($one_player_result)){
					/*	echo $one_match[0];
						echo "<br>";	*/					
					}	
					if(isset($event_matches_array[$event_list['ts_event_id']][$i]['tem_scheduled_date'])){						
						if(isset($event_result_array[$event_list['ts_event_id']][$i]['ter_player_id'])){
							$final_array[$event_matches_array[$event_list['ts_event_id']][$i]['tem_scheduled_date']][$event_list['ts_event_id']]['r'][$j][]=$event_result_array[$event_list['ts_event_id']][$i];
						}else{
							$final_array[$event_matches_array[$event_list['ts_event_id']][$i]['tem_scheduled_date']][$event_list['ts_event_id']]['m'][$j][]=$event_matches_array[$event_list['ts_event_id']][$i];
						}
					}			
					//echo $key_match."--".$i."<br>";
					//echo "<br>";	
					$i++;
				}
				$j++;
			}
	}
	
	
	foreach($tournament_result->teams as $one_key=>$oneresult){
		//	echo $one_key."<br>";
			//print_r($oneresult);
	}
}	

$db_events_cs = TournamentEventModel::find()->where(['tourn_id' => $tournament_id])->all();    
		$db_events_matches = EventMatches::find()->where(['em_tournament_id' => $tournament_id])->all();		
       if(count($db_events_cs)>0){
        	$id_db_all=array();
			$id_db_cat=array();
        	foreach($db_events_cs as $one_event){
				$id_db_all[]=$one_event->event_category;
				$id_db_all[]=$one_event->event_subcategory;
				$id_db_cat[$one_event->event_id]=$one_event->event_subcategory;																									
			}
			if(count($id_db_all)>0){
				$event_names = ArrayHelper::map(DropdownManagement::find()->FilterWhere(array('in', 'dropdown_id', $id_db_all))->all(), 'dropdown_id', 'dropdown_value');
			}
			
		foreach($db_events_cs as $one_event){
			$category_name="";
			$subcategory_name="";
			if(isset($event_names[$one_event->event_category])){
				$category_name=$event_names[$one_event->event_category];			
			}
			if(isset($event_names[$one_event->event_subcategory])){
				$subcategory_name=$event_names[$one_event->event_subcategory];			
			}
			$id_db_cat[$one_event->event_id]=$category_name." ".$subcategory_name;	
		}
		}
//echo "<pre>";
//print_r($id_db_cat);		
?>
<section class="drawer">
    <div class="col-md-12 size-img back-img-shop">
        <div class="effect-cover">
            <h3 class="txt-advert animated"><?php echo $tournament_title;  ?> - BANGALORE, INDIA</h3>
            <!--<p class="txt-advert-sub">OP SAT and QS</p>-->
        </div>
    </div>
</section>
<section id="current-match">
	<div  class="container">
		<div class="next-match-news top-match col-xs-12 col-md-12">
			<div class="tabs animated-slide-2 matches-tbs"><br /><br />
				<p class="news-title-right">
					Tournament Day
				</p>
				<ul class="tab-links-matches">
					<?php
					$tab_val=1;
					$day_count=count($dates_tournament); 
					foreach($dates_tournament as $one_date){
						$activ_cls='';
						if($day_count==$tab_val){$activ_cls='class="active"';}
						echo '<li '.$activ_cls.'><a href="#tab'.$tab_val++.'">'.date("M j<\s\u\p>S</\s\u\p\>",strtotime($one_date)).'</a></li>;';
					} ?>					
				</ul>
				<br /><br />
				<div class="tab-content">
					<?php
					$tab_val=1; 
					foreach($dates_tournament as $one_date){
						$activ_cls='';
						if($day_count==$tab_val){$activ_cls='active';}
						?>
						<div id="tab<?php echo $tab_val++;?>" class="tab <?php echo $activ_cls;?>">
						
						<?php 
						$string_order_play='';
									if(isset($final_array[$one_date])){
									foreach($final_array[$one_date] as $match_key=>$match_details){										
										if(isset($match_details['m']) ){					
											foreach($match_details['m'] as $one_match){
											$player_reg_id=trim($one_match[0]['tem_player_reg_id']);
											$player_reg_id1=trim($one_match[1]['tem_player_reg_id']);
											if($player_reg_id!="" && $player_reg_id1!=""){	
											$player1=$player_details_array[$player_reg_id]['reg_first_name']." ".$player_details_array[$player_reg_id]['reg_last_name'];
											$player2=$player_details_array[$player_reg_id1]['reg_first_name']." ".$player_details_array[$player_reg_id1]['reg_last_name'];									
											
											//$one_match[0]['tem_location'] 
											//$one_match[0]['tem_scheduled_time']
											
										$string_order_play.='<div class="other-match col-md-5">
									<div class="match-team-list">
										<img class="t-img1" style="width: 25px;" src="images/flags/india.png" alt=""/>
										<span>'. $player1 .'</span>
										<span class="txt-vs"> - vs - </span>
										<span>'. $player2.'</span>
										<img class="t-img2" style="width: 25px;" src="images/flags/india.png" alt=""/>
										<p>
											'.$one_match[0]['tem_location'].$one_match[0]['tem_scheduled_time'].'
										</p>
									</div>
									</div>';
											
											}
											}
										}										
									}
									//isset 
									if($string_order_play!=""){ ?>
									
						<h3 style="margin-top: 0px;" class="news-title n-match">ORDER OF<span>&nbsp;PLAY</span><span class="point-little">.</span></h3>
						<p class="subtitle">
							<?php echo date("l, M, Y",strtotime($one_date)); ?>
						</p>
						<?php echo $string_order_play;
						echo '<br clear="all" /><br />';
									} 
						} ?>
						
						
						
						<section id="allmatch" class="container secondary-page">
							<div class="general-results">
								<div class="top-score-title right-score col-md-12">
									<h3>MATCH <span>RESULTS</span><span class="point-little">.</span></h3>					
									<?php
									if(isset($final_array[$one_date])){
									foreach($final_array[$one_date] as $match_key=>$match_details){
										if(isset($match_details['r'])){
											$div_title='';
											if(isset($id_db_cat[$match_key])){												
												$div_title='<p style="text-align: center;">'.$tournament_title.' - '.$id_db_cat[$match_key].'Qualifying Results – First Round</p>';
											}else{
												$div_title='<p style="text-align: center;">'.$tournament_title.' - '.$id_db_cat[$match_key].'Qualifying Results – First Round</p>';
											}
											foreach($match_details['r'] as $one_match){
											$player_reg_id=trim($one_match[0]['ter_player_reg_id']);
											$player_reg_id1=trim($one_match[1]['ter_player_reg_id']);	
											if($player_reg_id!="" && $player_reg_id1!=""){	
											$player1=$player_details_array[$player_reg_id]['reg_first_name']." ".$player_details_array[$player_reg_id]['reg_last_name'];
											$player2=$player_details_array[$player_reg_id1]['reg_first_name']." ".$player_details_array[$player_reg_id1]['reg_last_name'];									
											
											$set_player1=explode(",",$one_match[0]['ter_set']);
											$set_player2=explode(",",$one_match[1]['ter_set']);
											$player_set_td1='';
											$player_set_td2='';
											$player_set_td='';
											$yt=1;
											foreach($set_player1 as $one_set){
												$player_set_td1.='<td class="fpt">'.$one_set.'</td>';
												$player_set_td.='<td class="fpt">Set '.$yt++.'</td>';
											}
											foreach($set_player2 as $one_set){
												$player_set_td2.='<td class="fpt">'.$one_set.'</td>';
											}
											$set_win1='';
											$set_win2='';
											if($one_match[0]['ter_result']>$one_match[1]['ter_result']){
												$set_win1='<i class="fa fa-check"></i>';
											}elseif($one_match[0]['ter_result']<$one_match[1]['ter_result']){
												$set_win2='<i class="fa fa-check"></i>';
											}
											
											echo $div_title;
											?>
										<div class="top-score-title right-score col-md-6">
										<div class="main">
											<div class="tab-content">
												<div id="tab1111" class="tab active">
													<table class="match-tbs">
														<tr class="match-sets">
															<td class="fpt">Players</td><?php echo $player_set_td; ?>
														</tr>
														<tr>
															<td class="fpt"><?php echo $player1;  ?><?php echo $set_win1;  ?></td><?php echo $player_set_td1; ?>
														</tr>
														<tr>
															<td class="fpt fpt2"><?php echo $player2;  ?><?php echo $set_win2;  ?></td><?php echo $player_set_td2; ?>
														</tr>
													</table>
												</div>
											</div>
										</div>
									</div>
											<?php
											}
											}
										}
									}
									}else{
										echo "Result till not uploaded.";
									} ?>
									
									
									
																	
								</div>	
							</div>
						</section>
						</div>
					<?php } ?>
					
				</div>
			</div>	
		</div>
	</div>
</section>
