<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\models\States;
use backend\models\Affilatedclubs; 
/* @var $this yii\web\View */
/* @var $searchModel backend\modelsTournamentScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
 
$this -> title = 'KSLTA - Karnataka State Lawn Tennis Association';
$this -> params['breadcrumbs'][] = $this -> title;
 
?><script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <section class="drawer">
    <div class="col-md-12 size-img back-img">
        <div class="effect-cover">
        <h3 class="txt-advert animated">AFFILATED CLUBS</h3>
        <p class="txt-advert-sub animated">ALL INDIA ACCOCIATION.</p>
        </div>
    </div>
   </section>
    <section id="summary" class="container secondary-page">
      <div class="general general-results tournaments">      
          <div class="top-score-title col-md-9">
          		<h3>AFFILATED CLUBS.</h3>
          	<div class="col-md-12">
          		
          		<div class="col-md-4">
          	<?php 				 
	 			 $model=new States();  	  
	 			 $form = ActiveForm::begin();	  
   				 $d=States::find()->all();
    			$a= ArrayHelper::map($d ,'state_id','state_name'); 
				 $b=array(); 
				
   			 echo $form->field($model, 'state_id')->dropDownList($a,['prompt'=>'All', 'class'=>'selected form-control',
       'onchange'=>'$.get("'.Url::to(['tournament-schedule/dropstate']).'", { id: $(this).val()}).done(function( data ) {$( "#variant").html( data );});'] )->label('State') ;
			 ?>
		</div>
</div>
<div class="col-md-12">
<div id="variant">
		    <?php
		    $html_output='';
		    $th=0;
			$th_m=0;
			//echo "<pre>"; print_r($Affilatedclubs1); die;
		    foreach($Affilatedclubs1 as $one_club){
		    $th_m=$th%3 ;  
		    if($th_m==0){
		    $html_output.='<div id="info-company" class="affclubs">';
		    }
		    if(isset($a[$one_club->state_id])){
		    $state_name=$a[$one_club->state_id];
			}
			$html_output.='<div class="col-md-offset-1 col-md-3" style="width: 285px; margin-left: 15px; height: 250px;bottom: -15px; padding: 20px;background: rgba(204, 204, 204, 0.32);"><h4 class="head"  style="color: #428bca;"><strong>'. $state_name.'</strong></h4><h5><strong>'. $one_club->name.'</strong></h5>';
			if($one_club->address1!=''){
				$html_output.='<p style="line-height: 20px;">'.$one_club->address1.'</p>';
			}
			
			if($one_club->address2!=''){
				$html_output.='<p >'.$one_club->address2.'</p>';
			}
				if($one_club->mobile!=''){
				$html_output.='<p >'.$one_club->mobile.'</p>';
			}
				if($one_club->phone!=''){ 	
				$html_output.='<p >'.$one_club->phone.'</p>';
			}
			if($one_club->fax!=''){
				$html_output.='<p >'.$one_club->fax.'</p>';
			}
			if($one_club->email!=''){
				$html_output.='<p >'.$one_club->email.'</p>';
			}
			if($one_club->website!=''){				
				//$html_output.='<p>'.$one_club->website.'</p>';				
				$html_output.='<p ><a href="http://'.$one_club->website.'"target="_blank">'.$one_club->website.'</a></p>';				
				
			}
			$html_output.='</div>';
			 if($th_m==2){
			$html_output.='</div><div class="clearfix">&nbsp;</div>';
			 }
			$th++;
			
		}
		if($th_m!=2){
			$html_output.='</div><div class="clearfix">&nbsp;</div>';
		}
			 echo $html_output; ?>
		     	
		    </div> </div>
</div>
         
          
                 <?= $this -> render('_sidebar') ?>     
       </div>
    </section>
	 <?php ActiveForm::end(); ?>
	 

<style>
	.selected{
		color: #f5f5f5 !important;
    vertical-align: middle;
    background-color: #2673cf !important;
	}
	.head {
    margin-top:0px; !important;
}
p {
    letter-spacing: 0.03em !important;
}
body {
	font-size: 13px;
}

</style>