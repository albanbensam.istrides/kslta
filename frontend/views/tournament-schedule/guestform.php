<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Guestroombooking;
use yii\bootstrap\Modal;
use dosamigos\datepicker\DateRangePicker;
use kartik\datecontrol\Module;

/* @var $this yii\web\View */
/* @var $model backend\models\Guestroombooking */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$model = new Guestroombooking();
//$form = ActiveForm::begin(['action' => ['index.php/player-save/guestform']]);

$form = ActiveForm::begin(['id'=>'event_quer']);

?>
<div class="top-score-title col-md-12 right-title">
<h3>Room Booking</h3>
</div>
<div class="col-md-12">


	<?= $form -> field($model, 'checkindate') -> textInput(['required' => 'required', 'class' => 'form-control', 'placeholder' => 'DD-MM-YYYY',  'data-required' => "true", 'id'=>'checkindate']) ?>
</div>
<div class="col-md-12">
	<?= $form -> field($model, 'checkoutdate') -> textInput(['required' => 'required', 'class' => 'form-control', 'placeholder' => 'DD-MM-YYYY',  'data-required' => "true", 'id'=>'checkoutdate']) ?>

</div>

<div class="col-md-12">

	<?php echo $form -> field($model, 'adultcount') -> dropDownList(['' => 'Select', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'],['id'=>'adultcount']); ?>

</div>
<div class="col-md-12">
	<?php echo $form -> field($model, 'childrencount') -> dropDownList(['' => 'Select', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'],['id'=>'childrencount']); ?>

</div>

<div class="col-md-12">

	<?= $form -> field($model, 'name') -> textInput(['maxlength' => true, 'required' => 'required','id'=>'name']) ?>
</div>
<div class="col-md-12">
	<?= $form -> field($model, 'mobile') -> textInput(['maxlength' => true, 'required' => 'required','id'=>'mobile','onchange'=>'mobilevalidate(this.value)']) ?>
<div id="mobileerror"style="color:red"></div>
</div>

<div class="col-md-12">

	<?= $form -> field($model, 'email') -> textInput(['maxlength' => true, 'type' => 'email', 'required' => 'required','id'=>'email','onchange'=>'emailvalidate(this.value)']) ?>
<div id="emailerror"style="color:red"></div>
</div>
<!--<div class="col-md-12">

<?php echo $form->field($model, 'status')->dropDownList(['' => 'Select','hide' => 'Hide', 'show' => 'Show']); ?>

</div>-->

<div class="form-group pull-right">

<button type="button" class="btn btn-info" id="myModal">Submit</button>
</div>

<div id="alertmessage" style="color:red;"></div>
<div id="message" style="color:green;"></div>


<?php ActiveForm::end(); ?>

<style>
	.myCalendar {
    cursor: pointer;
}
</style><link href="../frontend/web/js/dcalendar.picker.css" rel="stylesheet" type="text/css">
<script src="../frontend/web/js/dcalendar.picker.js"></script>
<script>
$('#checkindate').dcalendarpicker();
$('#checkoutdate').dcalendarpicker();
</script>

<script>
	 $('#myModal').click(function(e){
       e.preventDefault(); 
   
     var a=document.getElementById("checkindate").value;
     var b=document.getElementById("checkoutdate").value;
     var c=document.getElementById("adultcount").value;   
     var d=document.getElementById("name").value;
     var e=document.getElementById("name").value;
     var f=document.getElementById("mobile").value;
     var g=document.getElementById("email").value;
       if((a=='') || (b=='') || (c=='') || (d=='') || (e=='') || (f=='') || (g=='')) 
       {   
       // alert('All Feeled Are Manitury');
         $('#alertmessage').html('<h5>All Feeled Are Manitury</h5>').show().fadeOut(4000);
      }
      else
      {
     // alert('else');
       var controlAction = '<?php echo Yii::$app -> homeUrl . 'index.php/guestformsubmit'; ?>';

    $.ajax({
    url: controlAction,
    type: 'POST',
    data : $('#event_quer').serialize(),
    success: function (result) {
    if(result =='s'){
  		
        //alert('GustRoom Booking Successfully'); 
        $('#message').html('<h5>GustRoom Booking Successfully</h5>').show().fadeOut(3000);
    	 $("#event_quer")[0].reset();    	
         }

    },
    error: function (error) {
    //alert(error.responseText);
    //$('#modal-nasaconfirm').modal('hide');
    //location.reload();
    }
    
    });

}
    });
    
   function mobilevalidate(txt)
  {
  	 
	 var check=/^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;  
  	 var mobile=document.getElementById("mobile").value;  	 
  	 if(!(mobile.match(check)))
  	 {  	
  	  $('#mobileerror').html('<h5>Mobile Number Is Invalid</h5>').show().fadeOut(5000);  	 
  	  	 document.getElementById('mobile').focus(); 
   		 //return false;   	  	
  	 }  	
  }   
   
  function emailvalidate(txt)
  {
  	 
	 var check= /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/; 
  	 var email=document.getElementById("email").value;  	 
  	 if(!(email.match(check)))
  	 {  	
  	  $('#emailerror').html('<h5>Email Format Is Invalid</h5>').show().fadeOut(5000);
  	  	 document.getElementById('email').focus(); 
   		 //return false; 
  	 }
  	
  } 
  
 
</script>

