<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TournamentSchedule */

$this->title = 'Update Tournament Schedule: ' . ' ' . $model->ts_id;
$this->params['breadcrumbs'][] = ['label' => 'Tournament Schedules', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ts_id, 'url' => ['view', 'id' => $model->ts_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tournament-schedule-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
