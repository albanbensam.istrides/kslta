<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use backend\models\KslNewsTbl;
use yii\helpers\ArrayHelper;

$request = Yii::$app->request;
/* @var $this yii\web\View */
/* @var $searchModel backend\modelsTournamentScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this -> params['breadcrumbs'][] = $this -> title;
?>
<?php 
if (Yii::$app->request->get('v')){
 	$news_id=$request->get('v');
	$all_news=ArrayHelper::toArray(KslNewsTbl::find()->where(['news_id'=>$news_id])->all());
	$temp=$all_news[0]['news_meta_name'];
	$temp1=$all_news[0]['news_meta_content'];
	$this -> title = $all_news[0]['news_title'];
	if($temp || $temp1){
	$this->registerMetaTag([
    'name' => $temp,
	
    'content' => $temp1,
	]);}
}
	?>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<section class="drawer">
	<div class="col-md-12 size-img back-img-shop">
		<div class="effect-cover">
			<h3 class="txt-advert animated">NEWS & EVENTS</h3>
			<p class="txt-advert-sub">
				News - Match - Player
			</p>
		</div>
	</div>
	<section id="news" class="container secondary-page">
		<div class="general general-results players">
			
			<div class="top-score-title right-score col-md-12">
				<?php 
					$news_txt='';
					if (count($all_news)>0){ ?>
				<section id="single_news" class="container secondary-page">
      <div class="general general-results">
           <div class="top-score-title col-md-9">
		    
                <h3><?=$all_news[0]['news_title']?><span class="point-little">.</span></h3>
                <img src="<?=Url::to('@web/backend/web/'.$all_news[0]['news_content_image'])?>" alt="<?=$all_news[0]['news_title']?>"/><br /><br />
                <p class="desc_news"><b><?= $all_news[0]['news_located_city'].", ".date("d F, Y", strtotime($all_news[0]['news_posted_at'])) ?></b>&nbsp; &nbsp;<div style="font-size:13px;" ><?= $all_news[0]['news_editor_content'] ?></div>  </p>
                
                
                <br />
                </div>
               <!-- <h3 class="desc_news" style="margin-top: 0px; margin-bottom: 0px;"><b>Results: (all Indians): (Prefix denotes seeding): </b><span class="point-little">.</span></h3>
                <p class="desc_news">Singles final: (1) Prerna Bhambri bt (2) Natasha Palha 6-0, 6-4.</p>

               
				<br /><br />
           </div><!--Close Top Match-->
           <?= $this -> render('_sidebar') ?> 
        </section>
        <?php } ?>
				<!--
				<div class="col-md-12 news-page-page">
									<span class="news-page-active">1</span><span>2</span><span>3</span><span class="page-point">....</span><span>10</span>
								</div>-->
				
			</div><!--Close Top Match-->
		</div>

	</section>