<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use backend\models\KslNewsTbl;
$request = Yii::$app->request;
use yii\widgets\ListView;
use backend\models\KslNewsTblSearch;
/* @var $this yii\web\View */
/* @var $searchModel backend\modelsTournamentScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this -> title = $pagemodel->page_content_title;
$this -> params['breadcrumbs'][] = $this -> title;
?>
<section class="drawer">
	<div class="col-md-12 size-img back-img-shop">
		<div class="effect-cover">
			<h3 class="txt-advert animated">NEWS & EVENTS</h3>
			<p class="txt-advert-sub">
				News - Match - Player
			</p>
		</div>
	</div>
	<section id="news" class="container secondary-page">
		<div class="general general-results players">				
				<?php 
					/*if (Yii::$app->request->get('t')){
					 	$tournament_id=$request->get('t');
						$all_news=KslNewsTbl::find()->where(['news_link_status'=>'A','news_tournament_id'=>$tournament_id])->orderby(['news_posted_at'=> SORT_DESC])->all();
					}else{
						$all_news=KslNewsTbl::find()->where(['news_link_status'=>'A'])->orderby(['news_posted_at'=> SORT_DESC])->all();
					}
					$news_txt='';
					foreach($all_news as $one_news){					
						/*$news_txt.='<div class="col-md-12 news-page">
						<div style="width: 270px; height: auto;">
							<img class="img-djoko" src="'.Url::to('@web/backend/web/'.$one_news->news_content_image).'" alt="'.ucfirst($one_news->news_located_city).'" />
						</div>
						<div class="col-md-10 data-news-pg">
							<p class="news-dd">
								'.ucfirst($one_news->news_located_city).' '. date("M, d Y",strtotime($one_news->news_from_date)).'
							</p>
							<h3>'.$one_news->news_title.'<h3> 	
							<p>
								'.substr(strip_tags($one_news->news_editor_content),0,400).'                  
							</p>
							<div class="col-md-12 news-more">
								<a href="'.Url::toRoute(['index.php/newsview', 'v' => $one_news->news_id]).'" class="ca-more"><i class="fa fa-angle-double-right"></i>more...</a>
							</div>
						</div>
					</div>';*/
				/*$news_txt1.='<div class="col-xs-6 col-md-6 box-top-txt">
                <div class="col-md-4 slide-cont-img"><a href="single_news.html"><img class="scale_image" src="'.Url::to('@web/backend/web/'.$one_news->news_content_image).'" alt="'.ucfirst($one_news->news_located_city).'><i class="fa fa-video-camera"></i></a></div>
                <div class="event_date dd-date">'.date("M, d Y",strtotime($one_news->news_from_date)).' <div class="post_theme"></div></div><h4>'.$one_news->news_title.'</h4>
                <p>'.substr(strip_tags($one_news->news_editor_content),0,150).'  </p>
              </div>';
					}*/
					
				?>				
				
			<!--<div class="top-score-title right-score col-md-12">
				<h3>NEWS <span>ARCHIVES</span><span class="point-little">.</span></h3>
				<?php //echo $news_txt;  ?>
			</div>-->			
			<div class="col-xs-12 col-md-12 top-slide-info">
				<?php //echo $news_txt1;  ?>
             
             <?php  $searchModel = new KslNewsTblSearch();
             if (Yii::$app->request->get('t')){
					$tournament_id=$request->get('t');
              $dataProvider = $searchModel->tournamentsearchfrontend(Yii::$app->request->queryParams,$tournament_id);
			 }else{
			 	 $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
			 }
       
             //$searchModel->news_tournament_id=8;
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
			//    $searchModel->news_tournament_id = 8; ?>
             <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'id' => 'news-list-data',
                'itemOptions' => ['class' => 'item'],
                /* 'itemView' => function ($model, $key, $index, $widget) {
                  return Html::a(Html::encode($model->news_id), ['view', 'id' => $model->news_id]).$model->news_title;
                  }, */
                'itemView' => '_newsviewall',
            ])
            ?>
            </div>
		</div>
	</section>