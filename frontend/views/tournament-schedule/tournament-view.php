<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\KslTournamentsTbl;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
$request = Yii::$app->request;
use backend\models\KslNewsTbl;
use backend\models\Drawsheet;
use backend\models\TournamentGallery;
use backend\models\TournamentSchedule;
/* @var $this yii\web\View */
/* @var $searchModel backend\modelsTournamentScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this -> title = 'Air Asia Open';
//$this -> params['breadcrumbs'][] = $this -> title;
?>
<?php 
if (Yii::$app->request->get('v')){
 	$tournament_id=$request->get('v');
	$one_tournament=ArrayHelper::toArray(KslTournamentsTbl::find()->where(['tourn_id'=>$tournament_id])->all());
}
	?>
	<section class="drawer">
	<div class="col-md-12 size-img back-img-shop">
		<div class="effect-cover">
			<h3 class="txt-advert animated">TOURNAMENT <?= date("Y") ?></h3>
			<p class="txt-advert-sub">
				KARNATAKA STATE LAWN TENNIS ASSOCIATION
			</p>
		</div>
	</div>
	</section>
<!--SECTION CONTAINER SLIDER-->
<section class="container secondary-page">
	<div class="top-score-title right-score col-md-9">
		<?php if (count($one_tournament)>0) {
			$this -> title = strtoupper($one_tournament[0]['tourn_title']);
		$this -> params['breadcrumbs'][] = strtoupper($one_tournament[0]['tourn_title']);
			 ?>
				<h3 style="margin-bottom: 30px;"><?php echo date("F Y",strtotime($one_tournament[0]['tourn_from_date'])); ?></h3>
                <div class="col-md-12 atp-single-player">
                  <?php 
				  if($one_tournament[0]['tourn_tournament_image']!=""){ 
				  	$image_url=Url::to('@web/backend/web/'.$one_tournament[0]['tourn_tournament_image']);?>
                   <div class="col-md-4">
                  <img class="img-djoko" src="<?=$image_url?>" alt="">
                  </div>
                   <div class="col-md-8 rank-player">
                  <?php }else{ ?>
                  	<div class="col-md-12 rank-player">
                 <?php  }  ?>
                  	 <h3 style="margin-top: 25px; margin-bottom: 5px; text-align: center;"><?= strtoupper($one_tournament[0]['tourn_title'])?></h3>
                	 <h3 style="margin-top: 10px;text-align: center; font-size: 24px; text-transform: lowercase;"><span><?php echo  ucfirst($one_tournament[0]['tourn_venue']);?></span></h3>
                	<p> <?= strtoupper($one_tournament[0]['tourn_description'])?></p>
                     <div class="tab_news" style="text-align: center;">
				<?php 
				$tournament_id=$one_tournament[0]['tourn_id'];
				$t_news_count=KslNewsTbl::find()->where(['news_link_status'=>'A','news_tournament_id'=>$tournament_id])->orderby(['news_posted_at'=> SORT_DESC])->count(); 
				if($t_news_count>0){?>
				<a href="<?=Url::toRoute(['/news', 't' => $one_tournament[0]['tourn_id']])?>" class="tag">NEWS</a>
				<?php }  ?>
				<?php if($one_tournament[0]['tourn_factsheet']!=""){  ?>
				<a href="<?=Url::toRoute(['/backend/web/'.$one_tournament[0]['tourn_factsheet']])?>" class="tag" download>FACESHEET</a>
				<?php }  ?>
				<?php $records = Drawsheet::find()->where(['draw_tournamentid'=>$tournament_id])->orderby(['draw_datetime'=> SORT_DESC])->count(); 
				 if($records>0){
				 ?>
				<a href="<?=Url::toRoute(['/draw', 't' => $one_tournament[0]['tourn_id']])?>" class="tag">DRAW</a>
				<?php }  ?>
				<?php $model_matches = TournamentSchedule::find()->where(['ts_tournament_id'=>$tournament_id])->count();
				if($model_matches>0){ ?>
				<a href="<?=Url::toRoute(['/daily-schedule', 't' => $one_tournament[0]['tourn_id']])?>" class="tag">DAILY SCHEDULE</a>
				<?php }  ?>
				<?php  $tournamentModel = TournamentGallery::find()->where(['tourn_id' => $tournament_id])->count();
				if($tournamentModel>0){
				 ?>
				<a href="<?=Url::toRoute(['/tourn-gallery-show', 't' => $one_tournament[0]['tourn_id']])?>" class="tag">GALLERY</a>
				<?php }  ?>
			</div>
                  </div>
                </div>
                <?php }  ?>
           </div>
	
	 <?= $this -> render('_sidebar') ?>
</section>
<!-- SECTION NEWS SLIDER -->