<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel backend\modelsTournamentScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this -> title = 'Tournament Schedules';
$this -> params['breadcrumbs'][] = $this -> title;
?>
<section class="drawer">
    <div class="col-md-12 size-img back-img">
        <div class="effect-cover">
        <h3 class="txt-advert animated">ATP Challenger Bangalore - Calendar</h3>
        <p class="txt-advert-sub animated">Only the best eight singles players and best eight doubles teams.</p>
        </div>
    </div>
    <section id="summary" class="container secondary-page">
      <div class="general general-results tournaments">
          <div class="col-md-9 ">
          	
          </div>
                 <?= $this -> render('_sidebar') ?>     
       </div>
    </section>
    

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                "use strict";
                $('.accordion').accordion({ defaultOpen: 'section1' }); //some_id section1 in demo
            });
        });
    </script>