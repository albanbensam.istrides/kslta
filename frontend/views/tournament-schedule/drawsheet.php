<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Drawsheet;
use backend\models\DropdownManagement;
use backend\models\TournamentEventModel;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
$request = Yii::$app -> request;
/* @var $this yii\web\View */
/* @var $searchModel backend\modelsTournamentScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this -> title = 'Drawsheet';
$this -> params['breadcrumbs'][] = $this -> title;
$this->registerMetaTag([
   'name' => $pagemodel->page_meta_name,
    'content' => $pagemodel->page_meta_content,
	]);
?>

<?php 

if (Yii::$app->request->get('t')){
	$tournament_id=$request->get('t');
$records="";
$records = Drawsheet::find()->where(['draw_tournamentid'=>$tournament_id])->orderby(['draw_datetime'=> SORT_DESC])->all();
// pass this $records to you view and then in you view you can iterate over it 
$path_list=array();
foreach($records as $result){  	
  	$path_list[ $result->draw_eventid][]=$result->draw_filepath;
  }
}




$rows = TournamentEventModel::find()->where(['tourn_id' =>$tournament_id ])->all();
//print_r($rows);die; 
        $event_namelist=array();
        if(count($rows)>0){
        	//echo"fdsfs";
        	$id_db_all=array();
			$id_db_cat=array();
        	foreach($rows as $one_event){
				$id_db_all[]=$one_event->event_category;
				$id_db_all[]=$one_event->event_subcategory;
			$id_db_cat[$one_event->event_category][$one_event->event_id]=$one_event->event_subcategory;																								
			}
			if(count($id_db_all)>0){
				$event_names = ArrayHelper::map(DropdownManagement::find()->FilterWhere(array('in', 'dropdown_id', $id_db_all))->all(), 'dropdown_id', 'dropdown_value');
			}
			foreach($id_db_cat as $one_categoryid=>$one_subcategory){
				$one_category_txt='';
				if(isset($event_names[$one_categoryid])){
					 $one_category_txt=$event_names[$one_categoryid];
				}
				// echo '<optgroup label="'.$one_category_txt.'">';
						foreach($one_subcategory as $event_id=>$one_event){
						$one_subcategory_txt='';
						if(isset($event_names[$one_event])){
							 $one_subcategory_txt=$event_names[$one_event];
						}
		           
		    } 
		    $text=$one_category_txt.' '.$one_subcategory_txt;
					$event_namelist[$event_id]=$one_category_txt.' '.$one_subcategory_txt;
			}
				}
?>
<section id="single_news" class="container secondary-page">
<div class="top-score-title col-md-9">
<?php
			foreach ($event_namelist as $one_eventid => $one_eventname) {				
			if(isset($path_list[$one_eventid])){
		    ?><h3 style="font-size:30px;"><?php echo $one_eventname; ?></h3>
<br>
<?php
foreach($path_list[$one_eventid] as $result){
?>



	<div class="general general-results">		
			<?php $html_tmp = Url::to('@web/backend/web/' . $result); ?><img src="<?php echo $html_tmp; ?>"style="width:100%;"
			style="width:100px;">
			<br />
			<br />
		</div>
	


<?php }
	}
	}
?></div>
<div class="top-score-title col-md-3"></div>
</section>
