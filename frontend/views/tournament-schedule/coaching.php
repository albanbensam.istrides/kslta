<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use backend\models\Guestroombooking;
/* @var $this yii\web\View */
/* @var $searchModel backend\modelsTournamentScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this -> title = $coaching->coaching_page_title;
$this->registerMetaTag([
   'name' => $coaching->coaching_meta_tag_name,
    'content' => $coaching->coaching_meta_tag_description,
	]);
 
//echo Url::to('@web/backend/web/'.$coaching->coaching_top_background_img);
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<?php 

if($type="advance" || $type=="regular" || $type=="weekend"){  ?>
	
	<style>
	.back-img-shop {
	    background: transparent url('<?= Url::to('@web/backend/web/'.$coaching->content_portfolio_img) ?>') no-repeat bottom center;
	    -webkit-background-size: cover;
	    -moz-background-size: cover;
	    -o-background-size: cover;
	    background-size: cover;
}

</style>

<section class="drawer">
	<div class="col-md-12 size-img back-img-shop">
		<div class="effect-cover">
			<h3 class="txt-advert animated"><?= $coaching->coaching_top_main_heading   ?></h3>
			<p class="txt-advert-sub">
				<?= $coaching->coaching_top_sub_heading ?>
			</p>
		</div>
	</div>
	</section>
	<section id="summary" class="container secondary-page">
    <div class="general general-results tournaments">
      <div class="top-score-title right-score col-md-9">
        <h3><?= $coaching->coaching_content_heading ?><span class="point-little">.</span></h3>
        <!-- Category Filter -->
         

       <ul class="portfolio " style="margin-left: 0%;">
        <!-- Portfolio Items -->
         <?php  $url = Yii::$app->request->BaseUrl;  ?>
        <script src="<?= Url::to($url.'/frontend/web/js/jssor.slider.min.js') ?>" ></script>
   <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_SlideshowTransitions = [
              {$Duration:1200,x:0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:-0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:-0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:-0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:-0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,$Cols:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$Rows:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$Cols:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:-0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,$Rows:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:-0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$SlideOut:true,$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,$Delay:20,$Clip:3,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,$Delay:20,$Clip:3,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,$Delay:20,$Clip:12,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,$Delay:20,$Clip:12,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2}
            ];

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Cols: 5,
                $SpacingX: 5,
                $SpacingY: 5,
                $Align: 390
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 980;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <style>
        /* jssor slider loading skin spin css */
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from {
                transform: rotate(0deg);
            }

            to {
                transform: rotate(360deg);
            }
        }


        .jssora106 {display:block;position:absolute;cursor:pointer;}
        .jssora106 .c {fill:#fff;opacity:.3;}
        .jssora106 .a {fill:none;stroke:#000;stroke-width:350;stroke-miterlimit:10;}
        .jssora106:hover .c {opacity:.5;}
        .jssora106:hover .a {opacity:.8;}
        .jssora106.jssora106dn .c {opacity:.2;}
        .jssora106.jssora106dn .a {opacity:1;}
        .jssora106.jssora106ds {opacity:.3;pointer-events:none;}

        .jssort101 .p {position: absolute;top:0;left:0;box-sizing:border-box;background:#000;}
        .jssort101 .p .cv {position:relative;top:0;left:0;width:100%;height:100%;border:2px solid #000;box-sizing:border-box;z-index:1;}
        .jssort101 .a {fill:none;stroke:#fff;stroke-width:400;stroke-miterlimit:10;visibility:hidden;}
        .jssort101 .p:hover .cv, .jssort101 .p.pdn .cv {border:none;border-color:transparent;}
        .jssort101 .p:hover{padding:2px;}
        .jssort101 .p:hover .cv {background-color:rgba(0,0,0,6);opacity:.35;}
        .jssort101 .p:hover.pdn{padding:0;}
        .jssort101 .p:hover.pdn .cv {border:2px solid #fff;background:none;opacity:.35;}
        .jssort101 .pav .cv {border-color:#fff;opacity:.35;}
        .jssort101 .pav .a, .jssort101 .p:hover .a {visibility:visible;}
        .jssort101 .t {position:absolute;top:0;left:0;width:100%;height:100%;border:none;opacity:.6;}
        .jssort101 .pav .t, .jssort101 .p:hover .t{opacity:1;}
    </style>
    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:700px;height:480px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="<?= Url::to($url.'/frontend/web/svg/loading/static-svg/spin.svg')?>" />
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:700px;height:380px;overflow:hidden;">
        	 <?php 
              /* Image View */
               
			                foreach ($gallerymultiple as $val) {
                    $imgPath = Url::to('@web/backend/web/galleryimage/gallery_pages/'.$val->gal_image_path);
                    echo '
                    <div>
                <img data-u="image" src="'.$imgPath.'" />
                <img data-u="thumb" src="'.$imgPath.'" />
            		</div>
                   ';
              }

        ?>
            <a data-u="any" href="https://www.jssor.com" style="display:none">javascript slider</a>
        </div>
        <!-- Thumbnail Navigator -->
        <!-- <div data-u="thumbnavigator" class="jssort101" style="position:absolute;left:0px;bottom:0px;width:700px;height:100px;background-color:#000;" data-autocenter="1" data-scale-bottom="0.75">
            <div data-u="slides">
                <div data-u="prototype" class="p" style="width:190px;height:84px;">
                    <div data-u="thumbnailtemplate" class="t"></div>
                    <svg viewBox="0 0 16000 16000" class="cv">
                        <circle class="a" cx="8000" cy="8000" r="3238.1"></circle>
                        <line class="a" x1="6190.5" y1="8000" x2="9809.5" y2="8000"></line>
                        <line class="a" x1="8000" y1="9809.5" x2="8000" y2="6190.5"></line>
                    </svg>
                </div>
            </div>
        </div> -->
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora106" style="width:55px;height:55px;top:162px;left:30px;" data-scale="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                <polyline class="a" points="7930.4,5495.7 5426.1,8000 7930.4,10504.3 "></polyline>
                <line class="a" x1="10573.9" y1="8000" x2="5426.1" y2="8000"></line>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora106" style="width:55px;height:55px;top:162px;right:30px;" data-scale="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                <polyline class="a" points="8069.6,5495.7 10573.9,8000 8069.6,10504.3 "></polyline>
                <line class="a" x1="5426.1" y1="8000" x2="10573.9" y2="8000"></line>
            </svg>
        </div>
    </div>
    <script type="text/javascript">jssor_1_slider_init();</script> 
        </ul>
        <div class="tabs animated-slide-2" style="margin-top: -80px;">
          <div class="result-filter">
            <ul class="tab-links">
              <li class="active" style="background: #0D4C93;">
                <a href="#tab1111" style="background: #0D4C93;color:#fff;"><?= $coaching->coaching_tab_one_title ?></a>
              </li>
              <li style="background: #0D4C93;">
                <a href="#tab2222" style="background: #0D4C93;color:#fff;"><?= $coaching->coaching_tab_two_title ?></a>
              </li>
                <?php if(($coaching->coaching_file_upload!="") || ($coaching->coaching_tab_three_content_1!="")){  ?>
              <li style="background: #0D4C93;">
                <a href="#tab3333" style="background: #0D4C93;color:#fff;"><?= $coaching->coaching_tab_three_title ?></a>
              </li>
              <?php } ?>
            </ul>
          </div>
          <div class="tab-content-point">
            <div id="tab1111" class="tab active">
              <div class="col-md-12 content-more-det">
              <div class="bulleted_list">
                   <div class="col-md-12" style="padding: 25px;">
                         <?= $coaching->coaching_tab_one_content ?>
                    </div>
                </div>
              </div>
            </div>
            <div id="tab2222" class="tab">
              <div class="col-md-12 content-more-det" style="margin-bottom: 50px;">
                     <?= $coaching->coaching_tab_two_content_1 ?>
              </div>
              
             
            </div>
           
            <div id="tab3333" class="tab">
            <div class="col-md-12 content-more-det" style="margin-bottom: 50px;">
<?php if($coaching->remove_content_img!=1){   ?>
  <a href=<?= Url::to('@web/backend/web/'.$coaching->coaching_file_upload) ?> target="_blank"><img src=<?= Url::to('@web/backend/web/uploads/img/download-icon.png') ?> /></a>
<?php }else{
	
} ?>
              
              
               <p><?php echo $coaching->coaching_tab_three_content_1 ?></p>

               </div>
            
            </div> 
            
          </div>
          
        </div> 
         
      </div><!--Close Top Match--> 
      
      <div class="col-md-3 right-column">
       
      </div>  
       
        <?// = $this -> render('_facilitysidemenu') ?>
     <?= $this -> render('_sidebar') ?>
  </section>
  <script>
	 $(function () {
        "use strict";

        /***TABS RESULT (HOME PAGE) ****/

        $('.tabs .tab-links a').on('click', function (e) {
            var currentAttrValue = $(this).attr('href');

            // Show/Hide Tabs
            $('.tabs ' + currentAttrValue).slideDown(400).siblings().slideUp(400);

            // Change/remove current tab to active
            $(this).parent('li').addClass('active').siblings().removeClass('active');

            e.preventDefault();
        });

        /***TABS RESULT (MATCH PAGE) ****/

        $('.tabs .tab-links-match a').on('click', function (e) {
            var currentAttrValue = $(this).attr('href');

            // Show/Hide Tabs
            $('.tabs ' + currentAttrValue).show().siblings().hide();

            // Change/remove current tab to active
            $(this).parent('li').addClass('active').siblings().removeClass('active');

            e.preventDefault();
        });

        /***TABS RESULT (MATCHES PAGE) ****/

        $('.tabs .tab-links-matches a').on('click', function (e) {
            var currentAttrValue = $(this).attr('href');

            // Show/Hide Tabs
            $('.tabs ' + currentAttrValue).slideDown(400).siblings().slideUp(400);

            // Change/remove current tab to active
            $(this).parent('li').addClass('active').siblings().removeClass('active');

            e.preventDefault();
        });
        
        $('.tabs .tab-links2 a').on('click', function (e) {
            var currentAttrValue = $(this).attr('href');

            // Show/Hide Tabs
            $('.tabs ' + currentAttrValue).slideDown(400).siblings().slideUp(400);

            // Change/remove current tab to active
            $(this).parent('li').addClass('active').siblings().removeClass('active');

            e.preventDefault();
        });

        /***TABS RESULT (MATCH PAGE) ****/

        $('.tabs .tab-links2-match a').on('click', function (e) {
            var currentAttrValue = $(this).attr('href');

            // Show/Hide Tabs
            $('.tabs ' + currentAttrValue).show().siblings().hide();

            // Change/remove current tab to active
            $(this).parent('li').addClass('active').siblings().removeClass('active');

            e.preventDefault();
        });

        /***TABS RESULT (MATCHES PAGE) ****/

        $('.tabs .tab-links2-matches a').on('click', function (e) {
            var currentAttrValue = $(this).attr('href');

            // Show/Hide Tabs
            $('.tabs ' + currentAttrValue).slideDown(400).siblings().slideUp(400);

            // Change/remove current tab to active
            $(this).parent('li').addClass('active').siblings().removeClass('active');

            e.preventDefault();
        });
    });
</script>
 <script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
  
    "use strict";
    $(".portfolio a").hover(function() {
      $(this).children("img").animate({
        opacity : 0.75
      }, "fast");
    }, function() {
      $(this).children("img").animate({
        opacity : 1.0
      }, "slow");
    });
    $("a[rel^='prettyPhoto']").prettyPhoto({
      animation_speed : 'fast', /* fast/slow/normal */
      slideshow : 5000, /* false OR interval time in ms */
      autoplay_slideshow : false, /* true/false */
      opacity : 0.80, /* Value between 0 and 1 */
      show_title : true, /* true/false */
      allow_resize : true, /* Resize the photos bigger than viewport. true/false */
      default_width : 500,
      default_height : 344,
      counter_separator_label : '/', /* The separator for the gallery counter 1 "of" 2 */
      theme : 'pp_default', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
      horizontal_padding : 20, /* The padding on each side of the picture */
      hideflash : false, /* Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto */
      wmode : 'opaque' /* Set the flash wmode attribute */
    });
  }); 
</script>


<script type="text/javascript">

    $(window).load(function () {
        "use strict";
        var $container = $('.albumContainer');
        $container.isotope({
            filter: '*',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });

        $('.albumFilter li').on('click', function (e) {
            $('.albumFilter .current').removeClass('current');
            $(this).addClass('current');

            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });
            return false;
        });
    });

</script>
<?php } else{  ?>
	
	<section class="drawer">
  <div class="col-md-12 size-img back-img-shop" style="background:transparent url('<?php echo Url::to('@web/backend/web/'.$coaching->coaching_top_background_img); ?>') no-repeat bottom center;">
    <div class="effect-cover">
      <h3 class="txt-advert animated"><?= $coaching->coaching_top_main_heading ?></h3>
      <p class="txt-advert-sub">
       <?= $coaching->	coaching_top_sub_heading ?>
      </p>
    </div>
  </div>
  <section id="summary" class="container secondary-page">	
    <div class="general general-results tournaments">
      <div class="top-score-title right-score col-md-9">
       <!--  <h3>KSLTA REGULAR<span>&nbsp; TENNIS COACHING PROGRAMME</span><span class="point-little">.</span></h3> -->
       <h3><?= $coaching->coaching_content_heading ?><span class="point-little"></span></h3>
       <?php if($coaching->content_portfolio_img!=""){  ?>
        <div class="content-img-zoom">
          <img src=<?= Url::to('@web/backend/web/'.$coaching->content_portfolio_img); ?> />
        </div>
        <?php }  ?>
        <!-- Portfolio Items -->
              
        <br clear="all" />
        <div class="tabs animated-slide-2">
          <div class="result-filter">
            <ul class="tab-links">
              <li class="active" style="background: #0D4C93;">
                <a href="#tab1111" style="background: #0D4C93;color:#fff;"><?= $coaching->coaching_tab_one_title ?></a>
              </li>
              <li style="background: #0D4C93;">
                <a href="#tab2222" style="background: #0D4C93;color:#fff;"><?= $coaching->coaching_tab_two_title ?></a>
              </li>
                <?php if(($coaching->coaching_file_upload!="") OR ($coaching->coaching_tab_three_content_1!="")){  ?>
              <li style="background: #0D4C93;">
                <a href="#tab3333" style="background: #0D4C93;color:#fff;"><?= $coaching->coaching_tab_three_title ?></a>
              </li>
              <?php } ?>
            </ul>
          </div>
          <div class="tab-content-point">
            <div id="tab1111" class="tab active">
              <div class="col-md-12 content-more-det">
              <div class="bulleted_list">
                   <div class="col-md-12" style="padding: 25px;">
                         <?= $coaching->coaching_tab_one_content ?>
                    </div>
                </div>
              </div>
            </div>
            <div id="tab2222" class="tab">
              <div class="col-md-12 content-more-det" style="margin-bottom: 50px;">
                     <?= $coaching->coaching_tab_two_content_1 ?>
              </div>
              
             
            </div>
           
            <div id="tab3333" class="tab">
            <div class="col-md-12 content-more-det" style="margin-bottom: 50px;">
<?php if($coaching->coaching_file_upload!=""){?>
  <a href=<?= Url::to('@web/backend/web/'.$coaching->coaching_file_upload) ?> target="_blank"><img src=<?= Url::to('@web/backend/web/uploads/img/download-icon.png') ?> /></a>
<?php } ?>
              
              
               <p><?php echo $coaching->coaching_tab_three_content_1 ?></p>

               </div>
            
            </div>



          </div>
        </div>
        <br clear="all" />
      </div>
      <!--Close Top Match-->
       
     
     
          
      <?= $this -> render('_sidebar') ?> 
  </section>
</section>
 <script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    "use strict";
    $(".portfolio a").hover(function() {
      $(this).children("img").animate({
        opacity : 0.75
      }, "fast");
    }, function() {
      $(this).children("img").animate({
        opacity : 1.0
      }, "slow");
    });
    $("a[rel^='prettyPhoto']").prettyPhoto({
      animation_speed : 'fast', /* fast/slow/normal */
      slideshow : 5000, /* false OR interval time in ms */
      autoplay_slideshow : false, /* true/false */
      opacity : 0.80, /* Value between 0 and 1 */
      show_title : true, /* true/false */
      allow_resize : true, /* Resize the photos bigger than viewport. true/false */
      default_width : 500,
      default_height : 344,
      counter_separator_label : '/', /* The separator for the gallery counter 1 "of" 2 */
      theme : 'pp_default', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
      horizontal_padding : 20, /* The padding on each side of the picture */
      hideflash : false, /* Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto */
      wmode : 'opaque' /* Set the flash wmode attribute */
    });
  }); 
</script>


<script type="text/javascript">

    $(window).load(function () {
        "use strict";
        var $container = $('.albumContainer');
        $container.isotope({
            filter: '*',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });

        $('.albumFilter li').on('click', function (e) {
            $('.albumFilter .current').removeClass('current');
            $(this).addClass('current');

            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });
            return false;
        });
    });

</script>
	
<?php } ?>
