<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\modelsTournamentScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this -> title = $pagemodel->page_content_title;
$this -> params['breadcrumbs'][] = $this -> title;
?>

<?php if($tabmodel->coaching_top_background_img != '') { ?>
<style>
  .back-img-shop {
      background: transparent url('<?= Url::to('@web/backend/web/'.$tabmodel->coaching_top_background_img) ?>') no-repeat bottom center;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
}

</style>

<?php } ?>


<section class="drawer">
  <div class="col-md-12 size-img back-img-shop">
    <div class="effect-cover">
      <h3 class="txt-advert animated"><?= $tabmodel->coaching_top_main_heading ?></h3>
      <p class="txt-advert-sub">
        <?= $tabmodel->coaching_top_sub_heading ?>
      </p>
    </div>
  </div>

  
  <section id="shop" class="container secondary-page">
    <div class="general general-results">
      <div class="top-score-title col-md-9">
        <h3><?= $tabmodel->coaching_page_title ?></h3>
        <div class="col-md-12 content-zoom" style="margin-bottom: 25px;">
          <div class="content-img-zoom">
          
            <img id="img_01" src=<?php echo Url::to('@web/backend/web/uploads/'.'img/guest-house.jpg'); ?> />
          </div>
        </div>
        <br clear="all" />
        <div class="tabs animated-slide-2">
          <div class="result-filter">
            <ul class="tab-links">
              <li class="active" style="background: #0D4C93;">
                <a href="#tab1111" style="background: #0D4C93;"><?= $tabmodel->coaching_tab_one_title ?></a>
              </li>
              <li style="background: #0D4C93;">
                <a href="#tab2222" style="background: #0D4C93;"><?= $tabmodel->coaching_tab_two_title ?></a>
              </li>
              <li style="background: #0D4C93;">
                <a href="#tab3333" style="background: #0D4C93;"><?= $tabmodel->coaching_tab_three_title ?></a>
              </li>
            </ul>
          </div>
          <div class="tab-content-point">
            <div id="tab1111" class="tab active">
              <div class="col-md-12 content-more-det">
               <!--  <h3 style="margin-top: 20px;"><b>Terms and Conditions</b></h3> -->
                <div class="bulleted_list">
                  <?= $tabmodel->coaching_tab_one_content ?>
                </div>
              </div>
            </div>
            <div id="tab2222" class="tab">
              <div class="col-md-12 content-more-det" style="margin-bottom: 50px;">
                  <div class="col-md-6 content-more-det" style="margin-bottom: 50px;">
                    <?= $coachingmodel->coaching_tab_two_content_1 ?>
                  </div>
                  
                  <div class="col-md-6 content-more-det" style="margin-bottom: 50px;">
                    <?= $coachingmodel->coaching_tab_two_content_2 ?>
                  </div>
              </div>
            </div>
            <div id="tab3333" class="tab">
              <div class="col-md-12 content-more-det" style="margin-bottom: 50px;">
               <!--  <h3 style="margin-top: 20px;"><b>Catering</b></h3> -->
                <div class="bulleted_list">
                  <div class="acc-content">
                          <?= $tabmodel->coaching_tab_two_content_2  ?>
                        </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div><!--Close Top Match-->
      
        
      <div class="col-md-3 right-column">
        <div class="top-score-title col-md-12 right-title">
          <h3>Photos</h3>

          <ul class="right-last-photo">
            <?php 
              /* Image View */
              foreach ($gallerymodel as $val) {
                    $imgPath = Url::to('@web/backend/web/galleryimage/gallery_pages/'.$val->gal_image_path);
                    echo '<li>
                            <a href='.$imgPath.' rel="prettyPhoto[portfolio]">
                            <div class="jm-item second">
                              <div class="jm-item-wrapper">
                                <div class="jm-item-image">
                                  <img src='.$imgPath.' alt="" />
                                  <div class="jm-item-description">
                                    <div class="jm-item-button">
                                      <i class="fa fa-plus"></i>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div> </a>
                          </li>';
              }

        ?>
            
          </ul>
        </div>
       
      </div>
       <?= $this -> render('_facilitysidemenu') ?>
     <?= $this -> render('_sidebar') ?> 
    </div>  
  </section>
 </section>
 <script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    "use strict";
    $(".portfolio a").hover(function() {
      $(this).children("img").animate({
        opacity : 0.75
      }, "fast");
    }, function() {
      $(this).children("img").animate({
        opacity : 1.0
      }, "slow");
    });
    $("a[rel^='prettyPhoto']").prettyPhoto({
      animation_speed : 'fast', /* fast/slow/normal */
      slideshow : 5000, /* false OR interval time in ms */
      autoplay_slideshow : false, /* true/false */
      opacity : 0.80, /* Value between 0 and 1 */
      show_title : true, /* true/false */
      allow_resize : true, /* Resize the photos bigger than viewport. true/false */
      default_width : 500,
      default_height : 344,
      counter_separator_label : '/', /* The separator for the gallery counter 1 "of" 2 */
      theme : 'pp_default', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
      horizontal_padding : 20, /* The padding on each side of the picture */
      hideflash : false, /* Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto */
      wmode : 'opaque' /* Set the flash wmode attribute */
    });
  }); 
</script>