<?php

/* @var $this \yii\web\View */
/* @var $content string */
use frontend\assets\KsltaAsset2;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;
//AppAsset::register($this);
KsltaAsset2::register($this);
$this->title = 'KSLTA - Karnataka State Lawn Tennis Association';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="The stadium was originally built in 1976 to stage the first ever Davis Cup in Bangalore. The entire stadium was renovated fully to International Standards in October 2000 for conducting the ATP Tour World Championships.">
    <meta name="keywords" content="KSLTA, Karnataka Tennis, Karnataka State Lawn Tennis Association, Indian Tennis, Tennis Bangalore, Tennis Coaching in Bangaore, Tennis Association Bangalore">
    <meta name="author" content="KSLTA">
    <meta name="Owner" content="KSLTA" />
    <meta name="Copyright" content="2015 KSLTA" />
    <meta name="robots" content="index, follow" />
    <meta http-equiv="Content-Language" content="en" />
    <meta name="geo.placename" content="Cubbon Park, Bangalore, Karnataka, India" />
    <meta name="google-site-verification" content="" />
    <link rel="canonical" href="http://www.kslta.com/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="shortcut icon" type="image/png" href="<?php echo Url::to('@web/frontend/web/img/favicon.png');  ?>" />
    
    <?= $this->render('kslta_heading_webfiles', [
        'model' => $model,
    ]) ?>

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?= $this->render('kslta_header', [
        'model' => $model,
    ]) ?>

     <?= $content ?>  

<?= $this->render('kslta_footer', [
        'model' => $model,
    ]) ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
