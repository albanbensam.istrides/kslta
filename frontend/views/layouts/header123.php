<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\models\KslNewsTbl;
use backend\models\SliderBanner;
use backend\models\GalleryPageModel;
use backend\models\ImagesView;
use backend\models\KslGalleryTitle;
use backend\models\HomepageNews;
use backend\models\TournamentsEventResult;
use backend\models\PlayerRegister;
use backend\models\Headlines;
use backend\models\MainMenu;

use backend\models\SubMenu;
use backend\models\KslTournamentsTbl;
use backend\models\HomeNotification;



/* @var $this yii\web\View */

$this -> title = "Home";
?>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="author" content="KSLTA">
<meta name="Owner" content="KSLTA" />
<meta name="Copyright" content="2015 KSLTA" />
<meta name="robots" content="index, follow" />
<meta http-equiv="Content-Language" content="en" />
<meta name="geo.placename" content="Cubbon Park, Bangalore, Karnataka, India" />
<meta name="google-site-verification" content="" />
<link rel="canonical" href="http://www.kslta.com/">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!--<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />-->
    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'/>
    <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'/>-->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,300,200,500,600,700,800,900' rel='stylesheet' type='text/css'/>
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</head>
<?php $currenturl = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);?>
<body>
<!--SECTION TOP LOGIN-->
     
      <!--SECTION MENU -->
     <section class="container box-logo">
        <header>
			<div class="content-logo col-md-12" style="background: none;">
				<div class="logo">
					<a href="<?php echo Url::toRoute(['/index']) ?>"><img src="<?php echo Url::base(); ?>/frontend/web/img/logo2.png" alt="" /></a>
				</div>
          		<div class="bt-menu"><a href="#" class="menu"><span>&equiv;</span> Menu</a></div>
         		<div class="box-menu">
		            <nav id="cbp-hrmenu" class="cbp-hrmenu">
					<ul id="menu ksltamenustyle nav navbar-nav">  
							<?php

		$mainMenuModel = MainMenu::find()->orderBy(['menu_order_no' => 'SORT_ASC'])->all();
           $mainMenuContent = '';
								
								$isActive = '';
								$increMent = 1;
								$ksltaUrlActive = 'active';
								foreach($mainMenuModel as $menuValue){
									
									$menuId = $menuValue->menu_id;
									$subMenuContent = '';
									$headActiveLink = '';
									$subMenuModel = SubMenu::find()->where(['menu_id'=>$menuId])->orderBy(['sub_menu_order_no' => 'SORT_ASC'])->all();
									if(count($subMenuModel) >0){
										$listContent = '';
										
										foreach($subMenuModel as $subValue){
											$goSubLink = lcfirst($subValue->sub_menu_link);
				$listContent.='<li  class="sub_drop nav navbar-nav"><a href="'.Url::toRoute(['/'.$goSubLink]).'"><span>'.$subValue->sub_menu_name.'</span></a></li>';
										}
										
										$subMenuContent.=' 
										        <ul class="menu-pages ksltamenudyna dropdown-menu">
										        	'.$listContent.'
													
										          </ul>
		                                   ';
										$headActiveLink = '#';
									}else{
										$goLink = lcfirst($menuValue->menu_link);
										$headActiveLink = Url::toRoute(['/'.$goLink]);
									}
									if($increMent == 1 || $subMenuContent==""){
										
										$mainMenuContent .= '<li class="lnk-menu kslmainmenu"><a class="nav" href="'.Url::toRoute(['/'.$menuValue->menu_link]).'">'.$menuValue->menu_name.'</a></li>';
									}else{
										$mainMenuContent .= '<li class="dropdown"><a class="nav dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true" href="'.$headActiveLink.'">'.$menuValue->menu_name.'</a>'.$subMenuContent.'</li>';
									}
									
									$increMent++;
									
								}
									?>
									
								
							
							<?= $mainMenuContent ?>
							
							
							</ul>
						 
  </div>
							
							
					</nav>
					</div>
				</div>
			</header>
		</section>
		
		<section class="content-quicklink" >
			<div class="container">
					<!-- <div class="box-support2"> -->
						<div class="box-menu">
							<nav id="cbp-hrmenu" class="cbp-hrmenu" style="margin-top: 0px;">
								
		<div class="col-md-2 left-con" style=" color: #fff; height: 40px;">
          <ul id="menu" style="float: left;"> 
									<li><p class="support-info new" style="padding: 12px 0 0 20px!important;">SPOTLIGHT ON</p></li></ul>
           </div>
           
           <div class="col-md-7 left-con" style="background: #2673cf;height: 6px; border-bottom: 40px solid #e7e7e7; border-left: 24px solid transparent;    margin-left: -67px;">
           <ul id="menu" style="float: left;margin: 11px 0px 0px 14px;"> 
           	<li><a target="_blank" href="fileupload/Entry_Form_Players_Wheel_Chair.pdf" style="font-weight: 900; font-size: 16px; color: #0D4C93;" >TABEBUIA Open 2017 Entry Form</a>&nbsp;&nbsp;&nbsp;&nbsp;
           	<a target="_blank" href="fileupload/Fact-Sheet-IWTT-Tabebuia-open-2017.pdf" style="font-weight: 900; font-size: 16px; color: #0D4C93;" >TABEBUIA Open-17 Fact Sheet</a>
           		<!--<?php 
					 $model = HomeNotification::find()->orderBy(['order_no'=>'SORT_ASC'])->limit(3)->all();
					 if($model){
					 foreach ($model as $value) {
						 echo '<a href="'.Url::toRoute(['/'.$value->notification_url]).'" style="font-weight: 900; font-size: 16px; color: #0D4C93;">'.$value->notification_name.' &nbsp;&nbsp;&nbsp;&nbsp;</a>';
					 }
					 }
				?>-->
           		
           		 </li>
           	 
									<!-- <li><a href="summer-camp-2016.php" style="#0D4C93;">SUMMER CAMP 2016</a></li> --></ul>
           </div>
           
           <div class="col-md-3 left-con" style="background: #e7e7e7;height: 33px; border-bottom: 40px solid #2673cf; border-left: 24px solid transparent;">
          <ul id="menu" class="cbp-hrmenu" style="float: right;"> 
          	<li><p class="support-info" style="color:#fff; margin: 13px 0px -8px 6px;" ><a href="memberlogin" target="_blank" style="color: #fff;">MEMBER LOGIN</a></p> &nbsp;</li>
			<li style="margin: -34px -40px 10px 130px; padding: 8px 0 0 0;"><a href="#"><img src="<?php echo Url::base(); ?>/frontend/web/images/login.png" alt=""></a></li>
          	</ul>
           </div>
           <!--
									<li <?php if($currenturl=='about-kslta.php'){ echo 'class="lnk-menu2 active2"';} elseif ($currenturl=='commitee.php') { echo 'class="lnk-menu2 active2"';} else { echo 'class="link-menu2"';}?>><a  href="#">Daily Schedule</a></li>
																		<li <?php if($currenturl=='about-kslta.php'){ echo 'class="lnk-menu2 active2"';} elseif ($currenturl=='commitee.php') { echo 'class="lnk-menu2 active2"';} else { echo 'class="link-menu2"';}?>><a  href="#">Draw</a></li>
																		<li <?php if($currenturl=='about-kslta.php'){ echo 'class="lnk-menu2 active2"';} elseif ($currenturl=='commitee.php') { echo 'class="lnk-menu2 active2"';} else { echo 'class="link-menu2"';}?>><a  href="#">News</a></li>
																		<li <?php if($currenturl=='about-kslta.php'){ echo 'class="lnk-menu2 active2"';} elseif ($currenturl=='commitee.php') { echo 'class="lnk-menu2 active2"';} else { echo 'class="link-menu2"';}?>><a  href="#">Gallery</a></li>-->
           
            </nav>
           </div>
      <!-- </div> -->
      </div>
     
		</section>
	
           <style>
           	.support-info .new{
           		padding: 12px 0 0 58px!important;
           	}
           		.content-quicklink{
           		    width: 100%;
				    float: left;
				    padding: 0px 0px;
				    background: #2673cf none repeat;
			}
			.ksltamenudyna {
				padding: 20px !important;
   				margin-top: 10px !important;
			}
			
			.sub_drop {
				    padding: 5px !important;
			}
			a:hover, a:focus {
    color: #2a6496;
    text-decoration: none;
}
           </style>
           
           <script>
          
           	/*	$('body').on(' click', '.sub_menu', function(e) {
           			 
           			$(this).toggleClass("cbp-hropen"); 
           			
           		});*/
           		 
     		/*	 $('body').on(' blur', '.cbp-hropen', function(e) {
     			 	 
           			 $(this).removeClass("cbp-hropen"); 
           			 
           		});*/
     			 
     			 
     			  
           </script>
           
        