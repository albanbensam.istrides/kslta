<?php
use yii\helpers\Html;
use yii\helpers\Url;
use backend\models\KslNotification;
use backend\models\MainMenu;
use backend\models\SubMenu;
?>

  <?php $currenturl = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);?>
     <section class="container box-logo">
        <header>
			<div class="content-logo col-md-12" style="background: none;">
				<div class="logo">
					<a href="<?= Url::base('http'); ?>"><img src="<?php echo Url::to('@web/frontend/web/img/logo2.png');  ?>"  alt="" /></a>
					
				</div>
          		<div class="bt-menu"><a href="#" class="menu"><span>&equiv;</span> Menu</a></div>
         		<div class="box-menu">
		            <nav id="cbp-hrmenu" class="cbp-hrmenu">
					<ul id="menu ksltamenustyle">  
							<?php

		$mainMenuModel = MainMenu::find()->orderBy(['menu_order_no' => 'SORT_ASC'])->all();
           $mainMenuContent = '';
								
								$isActive = '';
								$increMent = 1;
								$ksltaUrlActive = 'active';
								foreach($mainMenuModel as $menuValue){
									
									$menuId = $menuValue->menu_id;
									$subMenuContent = '';
									$headActiveLink = '';
									$subMenuModel = SubMenu::find()->where(['menu_id'=>$menuId])->orderBy(['sub_menu_order_no' => 'SORT_ASC'])->all();
									if(count($subMenuModel) >0){
										$listContent = '';
										
										foreach($subMenuModel as $subValue){
											$goSubLink = lcfirst($subValue->sub_menu_link);
				$listContent.='<li><a href="'.Url::toRoute(['/'.$goSubLink]).'"><span>'.$subValue->sub_menu_name.'</span></a></li>';
										}
										
										$subMenuContent.='<div class="cbp-hrsub sub-little">
		                                  <div class="cbp-hrsub-inner"> 
		                                      <div class="content-sub-menu">
										        <ul class="menu-pages ksltamenudyna">
										        	'.$listContent.'
													
										          </ul>
		                                        </div>
		                                    </div>
		                                </div>';
										$headActiveLink = '#';
									}else{
										$goLink = lcfirst($menuValue->menu_link);
										$headActiveLink = Url::toRoute(['/'.$goLink]);
									}
									if($increMent == 1){
										$mainMenuContent .= '<li class="lnk-menu kslmainmenu"><a class="nav" href="'.Url::base('http').'">'.$menuValue->menu_name.'</a></li>';
									}else{
										$mainMenuContent .= '<li class="link-menu kslmainmenu"><a class="nav" href="'.$headActiveLink.'">'.$menuValue->menu_name.'</a>'.$subMenuContent.'</li>';
									}
									
									$increMent++;
									
								}
									?>
									
								
							
							<?= $mainMenuContent ?>
							</ul>
							
							
					</nav>
					</div>
				</div>
			</header>
		</section>
		
		<section class="content-quicklink" style="box-shadow: 0 3px 2px #939393;">
			<div class="container">
					<div class="box-support2">
						<div class="box-menu">
							<nav id="cbp-hrmenu" class="cbp-hrmenu2" style="margin-top: 0px;">
								<ul id="menu" class="cbp-hrmenu"> 
									<!--<li><p class="support-info">Quick Link:</p></li>-->
									<?php 	
					
						$all_news=KslNotification::find()->where(['notify_status'=>'Active'])->orderby(['notify_id'=> SORT_ASC])->all();
					
					foreach($all_news as $one_news)
					{
							$href_data='';
						if(strstr($one_news->notify_link,Url::base('http'))){
							$href_data=$one_news->notify_link;
						}else{
							$href_data=Url::to('@web/'.$one_news->notify_link);
						}
						          echo  '<li><a href="'.$href_data.'" style="font-weight: 900; font-size: 20px; color:'.$one_news->notify_color.';" target="_blank"">'.$one_news->notify_text.'</a></li>';
					} ?>
									<!--<li><a href="member-series-june-2016.php" style="font-weight: 900; font-size: 20px;">INTRA CLUB TOURNAMENT FOR MEMBERS FROM 3rd-5th JUNE</a></li>
									<li><a href="tallent-series-june-2016-u14.php" style="font-weight: 900; font-size: 20px; color: #F4FA4E;">AITA TALENT SERIES FOR B&G U 14</a></li>-->
									
									<!--<li><a href="summer-camp-2016.php">SUMMER CAMP 2016</a></li>
									
									<li <?php if($currenturl=='about-kslta.php'){ echo 'class="lnk-menu2 active2"';} elseif ($currenturl=='commitee.php') { echo 'class="lnk-menu2 active2"';} else { echo 'class="link-menu2"';}?>><a  href="#">Daily Schedule</a></li>
														<li <?php if($currenturl=='about-kslta.php'){ echo 'class="lnk-menu2 active2"';} elseif ($currenturl=='commitee.php') { echo 'class="lnk-menu2 active2"';} else { echo 'class="link-menu2"';}?>><a  href="#">Draw</a></li>
																		<li <?php if($currenturl=='about-kslta.php'){ echo 'class="lnk-menu2 active2"';} elseif ($currenturl=='commitee.php') { echo 'class="lnk-menu2 active2"';} else { echo 'class="link-menu2"';}?>><a  href="#">News</a></li>
																		<li <?php if($currenturl=='about-kslta.php'){ echo 'class="lnk-menu2 active2"';} elseif ($currenturl=='commitee.php') { echo 'class="lnk-menu2 active2"';} else { echo 'class="link-menu2"';}?>><a  href="#">Gallery</a></li>-->
									
								</ul>
							</nav>
						</div>		
					</div>
				</div>
		</section>
		
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>