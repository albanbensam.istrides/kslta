 <?php 
use yii\helpers\Html;
use yii\helpers\Url;
?>
<!--SECTION FOOTER--> 
    <section id="footer-tag">
           <div class="container">
             <div class="col-md-12">
              <div class="col-md-3">
                 <h3>About Us</h3>
                 <p>The stadium was originally built in 1976 to stage the first ever Davis Cup in Bangalore. The entire stadium was renovated fully to International Standards in October 2000 for conducting the ATP Tour World Championships. This was the first time that the tournament was being held in South Asia and Karnataka got the chance to host what can be called the biggest tournament ever to be played in the country...</p>
              </div>
              <div class="col-md-3 cat-footer">
                <div class="footer-map"></div>
                <h3 class='last-cat'>Categories</h3>
                <ul class="last-tips">
                  <li><a href="<?=Url::toRoute(['/calander']);?>">Players</a></li>
                  <li><a href="<?=Url::toRoute(['/coaching?type=regular']);?>">Coaches</a></li>
                   <li><a href="<?=Url::toRoute(['/calander']);?>">Calander</a></li>
                  <li><a href="<?=Url::toRoute(['/tennis-billiards']);?>">Facilities</a></li>
                  <li><a href="<?=Url::toRoute(['/calander']);?>">Club House</a></li>
                  <li><a href="<?=Url::toRoute(['/calander']);?>">Gallery</a></li>
                </ul>
              </div>
              <div class="col-md-3">
                 <h3>ARCHIVES</h3>
                 <ul class="footer-last-news">
                  

                 	<li><img src="<?php echo Url::to('@web/frontend/web/img/James-Ward.jpg');  ?>" alt="James Ward clinches AirAsia Open" /><a href="oct21.php"><p>James Ward clinches AirAsia Open...</p></a></li>
                 	<li><img src="<?php echo Url::to('@web/frontend/web/img/Magic-Buds.jpg');  ?>" alt="Magic Buds" /><a href="oct24-magic-buds.php"><p>Underprivileged kids on ‘Magic Bus’ at AirAsia Open...</p></a></li>
                 	<li><img src="<?php echo Url::to('@web/frontend/web/img/Saketh-Sanam-clinch-doubles-crown.jpg');  ?>" alt="Saketh-Sanam clinch doubles crown" /><a href="oct24.php"><p>Saketh-Sanam clinch doubles crown...</p></a></li>
                 	<li><img  src="<?php echo Url::to('@web/frontend/web/img/Saketh-lone-Indian-in-last-four.jpg');  ?>" alt="Saketh lone Indian in last four" /><a href="oct23.php"><p>Saketh lone Indian in last four...</p></a></li>
                 	<!--<li><img src="img/yuki-withdraws-Airasia-Open.jpg" alt="yuki-withdraws-Airasia-Open.jpg" /><a href="oct17.php"><p>Yuki withdraws from Air Asia Open...</p></a></li>-->
                    <!--<li><img src="img/AirAsia.JPG" alt="press meet" /><a href="oct16.php"><p>Yuki to lead challenge in Air Asia Open...</p></a></li>-->
                    <!--
                    <li><img src="img/gallery/2.jpg" alt="" /><p>Fusce risus metus, placerat in consectetur eu...</p></li>
                                        <li><img src="img/gallery/3	.jpg" alt="" /><p>Fusce risus metus, placerat in consectetur eu...</p></li>-->
                    
                 </ul>
              </div>
              <div class="col-md-3 footer-newsletters">
                <h3>Newsletters</h3>
                <form method="post">     
                    <div class="name">
                        <label for="name">* Name:</label><div class="clear"></div>
                        <input id="name" name="name" type="text" placeholder="e.g. Mr. John Doe" required=""/>
                    </div>
                    <div class="email">
                        <label for="email">* Email:</label><div class="clear"></div>
                        <input id="email" name="email" type="text" placeholder="example@domain.com" required=""/>
                    </div>
                    <div id="loader">
                                <input type="submit" value="Submit"/>
                        </div>
                </form>
              </div>
              <!--
              <div class="col-xs-12">
                              <ul class="social">
                                    <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                    <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                    <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href=""><i class="fa fa-digg"></i></a></li>
                                    <li><a href=""><i class="fa fa-rss"></i></a></li>
                                    <li><a href=""><i class="fa fa-youtube"></i></a></li>
                                    <li><a href=""><i class="fa fa-tumblr"></i></a></li>
              
                                  </ul>
                            </div>-->
              
             </div>
           </div>
    </section>
    <footer>
           <div class="col-md-12 content-footer">
		<p>© 2015 kslta - Karnataka State Lawn Tennis Association. Developed and Maintained by <a href="http://www.defocusstudio.com" target="_blank">DeFocus Studio</a> </p>
      </div>
	</footer>



<script language="JavaScript1.1">
<!--
// Open a window of the desired size in the center of the screen.

function openLSHDWindow(year, wkno, eventid, tour, lang, ref_file, width, height, hasScrollBars) {
	// ADD NAME FIELD and make sure it get's focus!!!
	var theWidth = width;
	var theHeight = height;
	var scrollBars = "scrollbars";
	if (hasScrollBars == false) scrollBars = "scrollbars=0";
	if ((theWidth == "")||(theWidth == null)) theWidth =1042;
	if ((theHeight == "")||(theHeight == null)) theHeight =631;
	var theLeft = (screen.availWidth - theWidth)/2;
	var theTop = (screen.availHeight - theHeight)/2;
	var strCheckRef = escape(ref_file);

	var lsURL = "http://www.protennislive.com/lshd/main.html?year="+year+"&wkno="+wkno+"&eventid="+eventid+"&tour="+tour+"&lang="+lang+"&ref="+strCheckRef;

	var popupWin = window.open(lsURL, '_' + Math.round(Math.random() * 1000000),'top='+theTop+',left='+theLeft+',menubar=0,toolbar=0,location=0,directories=0,status=0,'+scrollBars+',width='+theWidth+', height='+theHeight);
}
//-->
</script>


    <script type="text/javascript">
        $(document).ready(function () {

         // $('.menuone').addClass('active');
            $(function () {
                "use strict";
                $('.accordion2').accordion({ defaultOpen: 'final' }); //some_id section1 in demo
                $('.accordion').accordion({ defaultOpen: 'semi-final' }); //some_id section1 in demo
            });
        });

        
        $(function() { 
    $('#cbp-hrmenu').on('click','.nav', function ( e ) {
        e.preventDefault();
        $(this).parents('.kslmainmenu').find('.active').removeClass('active').end().end().addClass('active');
        $(activeTab).show();
    });
});
    </script>