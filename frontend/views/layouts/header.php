<?php
   use yii\helpers\Html;
   use yii\helpers\Url;
   use yii\helpers\ArrayHelper;
   use backend\models\KslNewsTbl;
   use backend\models\SliderBanner;
   use backend\models\GalleryPageModel;
   use backend\models\ImagesView;
   use backend\models\KslGalleryTitle;
   use backend\models\HomepageNews;
   use backend\models\TournamentsEventResult;
   use backend\models\PlayerRegister;
   use backend\models\Headlines;
   use backend\models\MainMenu;
   
   use backend\models\SubMenu;
   use backend\models\KslTournamentsTbl;
   use backend\models\HomeNotification;
   use backend\models\KslNotification;
   
   
   /* @var $this yii\web\View */
   
   $this -> title = "Home";
   ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="author" content="KSLTA">
      <meta name="Owner" content="KSLTA" />
      <meta name="Copyright" content="2015 KSLTA" />
      <meta name="robots" content="index, follow" />
      <meta http-equiv="Content-Language" content="en" />
      <meta name="geo.placename" content="Cubbon Park, Bangalore, Karnataka, India" />
      <meta name="google-site-verification" content="" />
      <link rel="canonical" href="http://www.kslta.com/">
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <!--<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />-->
      <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'/>
      <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'/>-->
      <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,300,200,500,600,700,800,900' rel='stylesheet' type='text/css'/>
      <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
      <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
      <script src="frontend/web/popup/swc.js"></script>
      <link href="frontend/web/popup/swc.css" rel="stylesheet">
      <style>
         .mnu1{
         padding: 12px 0 0 20px!important;
         }
         .mnu2{
        width: 73%;
    background: #2673cf;
    height: 6px;
    border-bottom: 40px solid #e7e7e7;
    border-left: 24px solid transparent;
    margin-left: -67px;
    /* padding: 0px 6px 10px 60px; */
         }
         .mnu2ul{
         float: left!important;margin: 11px 0px 0px 14px!important;
         }
         .mnu3{
         background: #e7e7e7;height: 33px;     width: 13%; border-bottom: 40px solid #2673cf; border-left: 24px solid transparent;
         }
         .mnu3li{
         margin: -34px -40px 10px 110px; padding: 8px 0 0 0;
         }
         .mnu3p{
         color:#fff; margin: 13px 0px -8px -10px;
         }
		 .mnudiv{
			 color: #fff; height: 40px;
		 }
		 .mnu3a{
			 color: #fff;
		 }
		 .navbar-toggle .icon-bar{
			 border:1px solid #fff;
		 }
		 .navbar-toggle{
			 background-color: #1e599b;
		 }
		  @media only screen and (max-width:640px){
			  .mnu2{
				  width: 115%; 
          background: #fff;  
				  padding:0px 6px 10px 60px!important;
          height:unset;  
          border-bottom: 1px solid #e7e7e7;  
          border-left: 04px solid transparent;  
          text-align: center;
			  }
			  .mnu3 {
				  background: #e7e7e7;
				  height: 33px;
                  width: unset;
                  border-bottom: 40px solid #2673cf;
                border-left: none;  
                  padding: 0px 9px 0px 45px;
             }
			 .mnu3li {
				margin: -34px 0px 0px 165px;
				padding: 8px 0 0 0;
				}
			  
		  }
		 
		 @media only screen and (max-width:414px){
			 .mnu2 {
				width: 123.5%; 
				background: #fff;  
				height:unset;  
				border-bottom: 1px solid #e7e7e7;  
				border-left: 04px solid transparent;  
        text-align: center;
				/* margin-left: 0px;*/
				
			}
			.mnu3 {
			 background: #0d4c93;
				height: 33px;
				width: 100%;
				border-bottom: none;
				border-left: 24px solid transparent;
				
           }
		   .mnu3li{
				margin: -34px -40px 10px 160px; padding: 8px 0 0 0;
			}
			.mnudiv{
				height: 40px;
				background-color: #0d4c93;
				font-weight: bold;
				border-bottom: 1px solid #ccc;
				color: #fff;
			}
			.mnu3a{
			 color: #fff;
			 font-weight: bold;
			 font-size: 17px;
		 }
		 .mnu3p{
			 margin: 7px 0px -8px -4px;
		 }
		 
		 }
		 
      </style>
   </head>
   <?php $currenturl = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);?>
   <body>
      <!--SECTION TOP LOGIN-->
      <!--SECTION MENU -->
      
      
    
      
      
      <section class="container box-logo">
         <header>
            <div class="content-logo col-md-12" style="background: none;">
              <!-- <div class="logo">
                  <a href="<?php echo Url::toRoute(['/index']) ?>"><img src="<?php echo Url::base(); ?>/frontend/web/img/logo2.png" alt="" /></a>
               </div> -->
			    <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".box-menu">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
		   <div class="logo">
           <a href="<?php echo Url::toRoute(['/index']) ?>"><img src="<?php echo Url::base(); ?>/frontend/web/img/logo2.png" alt="" /></a>
        </div>
        </div>
			   
              <!-- <div class="bt-menu">
                   <a   data-toggle="dropdown" href="#" class="menu dropdown-toggle"><span>&equiv;</span> Menu</a>  
                  <button type="button" class=" " data-toggle="collapse" data-target=".box-menu" aria-expanded="true">
                  Menu                       
                  </button>
                 <button type="button" class=" " data-toggle="collapse" data-target=".box-menu1" aria-expanded="true">
                  Sub Menu                       
                  </button> 
               </div> -->
               <div class="box-menu">
                  <nav id="cbp-hrmenu" class="cbp-hrmenu">
                     <ul id="menu ksltamenustyle nav navbar-nav">  
                        <?php
                           $mainMenuModel = MainMenu::find()->orderBy(['menu_order_no' => 'SORT_ASC'])->all();
                                    $mainMenuContent = '';
                           						
                           						$isActive = '';
                           						$increMent = 1;
                           						$ksltaUrlActive = 'active';
                           						foreach($mainMenuModel as $menuValue){
                           							
                           							$menuId = $menuValue->menu_id;
                           							$subMenuContent = '';
                           							$headActiveLink = '';
                           							$subMenuModel = SubMenu::find()->where(['menu_id'=>$menuId])->orderBy(['sub_menu_order_no' => 'SORT_ASC'])->all();
                           							if(count($subMenuModel) >0){
                           								$listContent = '';
                           								
                           								foreach($subMenuModel as $subValue){
                           									$goSubLink = lcfirst($subValue->sub_menu_link);
                           		$listContent.='<li  class="sub_drop nav navbar-nav"><a href="'.Url::toRoute(['/'.$goSubLink]).'"><span>'.$subValue->sub_menu_name.'</span></a></li>';
                           								}
                           								
                           								$subMenuContent.=' 
                           								        <ul class="menu-pages ksltamenudyna dropdown-menu">
                           								        	'.$listContent.'
                           											
                           								          </ul>
                                                              ';
                           								$headActiveLink = '#';
                           							}else{
                           								$goLink = lcfirst($menuValue->menu_link);
                           								$headActiveLink = Url::toRoute(['/'.$goLink]);
                           							}
                           							if($increMent == 1 || $subMenuContent==""){
                           								
                           								$mainMenuContent .= '<li class="lnk-menu kslmainmenu"><a class="nav" href="'.$headActiveLink.'">'.$menuValue->menu_name.'</a></li>';
                           							}else{
                           								$mainMenuContent .= '<li class="dropdown"><a class="nav dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true" href="'.$headActiveLink.'">'.$menuValue->menu_name.'</a>'.$subMenuContent.'</li>';
                           							}
                           							
                           							$increMent++;
                           							
                           						}
                           							?>
                        <?= $mainMenuContent ?>
                     </ul>
               </div>
               </nav>
            </div>
            </div>
         </header>
      </section>
      <section class="content-quicklink" >
         <div class="container">
            <!-- <div class="box-support2"> -->
            <!-- <div class="box-menu">-->
            <div class=" box-menu2">
               <nav id="cbp-hrmenu" class="cbp-hrmenu" style="margin-top: 0px;">
                  <div class=" ">
                     <div class=" ">
                        <div class="col-md-2 col-xs-12 left-con mnudiv" style=" ">
                           <ul id="menu" style="float: left;">
                              <li>
                                 <p class="support-info new mnu1" style="">SPOTLIGHT ON</p>
                              </li>
                           </ul>
                        </div>
                        <div class="col-md-8 col-xs-12 left-con mnu2"  >
                           <ul id="menu" class="mnu2ul"  >
                              <li>
                                 <?php 	
                                    $all_news=KslNotification::find()->where(['notify_status'=>'Active'])->orderby(['notify_id'=> SORT_ASC])->all();
                                    $count = count($all_news); $j=0;
                                    foreach($all_news as $one_news)
                                    {
                                    	$href_data='';
                                    if(strstr($one_news->notify_link,Url::base('http'))){
                                    	$href_data=$one_news->notify_link; 
                                    }else{
                                     
                                    	$href_data=Url::to('@web/'.$one_news->notify_link);
                                    }
                                             $str='';
                                    		if($j>0){$str='<span style="color:#2673cf;">/</span>';};
                                    		if($count==1){
                                    		  echo  '<a href="'.$href_data.'" style="font-weight: 900; font-size: 20px; color:'.$one_news->notify_color.';" target="_blank"">'.$one_news->notify_text.'&nbsp;</a>';
                                    		}elseif($count==2){
                                    			$string_cutting = trim($one_news->notify_text);
                                    			
                                    			if (strlen($string_cutting) > 20) {
                                                               // truncate string
                                                               if (strlen($string_cutting) > 30) {
                                                                  $string_cutting = substr($string_cutting, 0, 45);
                                    		 }else{
                                    		 	 $string_cutting = substr($string_cutting, 0, 30);
                                    		 }
                                                              // make sure it ends in a word so assassinate doesn't become ass...
                                                                  $string_cutting = substr($string_cutting, 0, strrpos($string_cutting, ' ')).'&nbsp;&nbsp;'; 
                                    		    
                                                          		}
                                    			
                                    			 echo   $str.' <a href="'.$href_data.'" style="font-weight: 900; font-size: 16px; color:'.$one_news->notify_color.';" target="_blank"">&nbsp;'.$string_cutting.'&nbsp;</a>';
                                    		}elseif($count==3){
                                    			 $string_cutting = trim($one_news->notify_text);
                                    				if (strlen($string_cutting) > 20) {
                                                               // truncate string
                                                                  $string_cutting = substr($string_cutting, 0, 40);
                                    		
                                                              // make sure it ends in a word so assassinate doesn't become ass...
                                                                  $string_cutting = substr($string_cutting, 0, strrpos($string_cutting, ' ')).'&nbsp;&nbsp;'; 
                                    		    
                                                          		}										
                                    			 echo  $str.' <a href="'.$href_data.'" style="font-weight: 900; font-size: 18px; color:'.$one_news->notify_color.';" target="_blank""> &nbsp;'.$string_cutting.'&nbsp;</a>';
                                    		}elseif($count==4){
                                    			 $string_cutting = trim($one_news->notify_text);
                                    				if (strlen($string_cutting) > 20) {
                                                               // truncate string
                                                                  $string_cutting = substr($string_cutting, 0, 40);
                                    		
                                                              // make sure it ends in a word so assassinate doesn't become ass...
                                                                  $string_cutting = substr($string_cutting, 0, strrpos($string_cutting, ' ')).'&nbsp;&nbsp;'; 
                                    		    
                                                          		}										
                                    			 echo  $str.' <a href="'.$href_data.'" style="font-weight: 900; font-size: 18px; color:'.$one_news->notify_color.';" target="_blank""> &nbsp;'.$string_cutting.'&nbsp;</a>';	 
                                    		}$j++;
                                    } ?>
                                 <?php 
                                    /* $model = HomeNotification::find()->orderBy(['order_no'=>'SORT_ASC'])->limit(3)->all();
                                     if($model){
                                     foreach ($model as $value) {
                                    	 echo '<a href="'.Url::toRoute(['/'.$value->notification_url]).'" style="font-weight: 900; font-size: 16px; color: #0D4C93;">'.$value->notification_name.' &nbsp;&nbsp;&nbsp;&nbsp;</a>';
                                     }
                                     }*/
                                    ?>
                              </li>
                              <!-- <li><a href="summer-camp-2016.php" style="#0D4C93;">SUMMER CAMP 2016</a></li> -->
                           </ul>
                        </div>
                        <div class="col-md-1 col-xs-12 left-con mnu3"  >
                           <ul id="menu" class="cbp-hrmenu" style="float: left;">
                              <li>
                                 <p class="support-info mnu3p"   ><a href="memberlogin" class="mnu3a" target="_blank" >MEMBER LOGIN</a></p>
                                 &nbsp;
                              </li>
                              <li class="mnu3li"><a href="#"><img src="<?php echo Url::base(); ?>/frontend/web/images/login.png" alt=""></a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <!--
                     <li <?php if($currenturl=='about-kslta.php'){ echo 'class="lnk-menu2 active2"';} elseif ($currenturl=='commitee.php') { echo 'class="lnk-menu2 active2"';} else { echo 'class="link-menu2"';}?>><a  href="#">Daily Schedule</a></li>
                     									<li <?php if($currenturl=='about-kslta.php'){ echo 'class="lnk-menu2 active2"';} elseif ($currenturl=='commitee.php') { echo 'class="lnk-menu2 active2"';} else { echo 'class="link-menu2"';}?>><a  href="#">Draw</a></li>
                     									<li <?php if($currenturl=='about-kslta.php'){ echo 'class="lnk-menu2 active2"';} elseif ($currenturl=='commitee.php') { echo 'class="lnk-menu2 active2"';} else { echo 'class="link-menu2"';}?>><a  href="#">News</a></li>
                     									<li <?php if($currenturl=='about-kslta.php'){ echo 'class="lnk-menu2 active2"';} elseif ($currenturl=='commitee.php') { echo 'class="lnk-menu2 active2"';} else { echo 'class="link-menu2"';}?>><a  href="#">Gallery</a></li>-->
               </nav>
            </div>
            <!-- </div> -->
         </div>
      </section>
      <style>
         .support-info .new{
         padding: 12px 0 0 58px!important;
         }
         .content-quicklink{
         width: 100%;
         float: left;
         padding: 0px 0px;
         background: #2673cf none repeat;
         }
         .ksltamenudyna {
         padding: 20px !important;
         margin-top: 10px !important;
         }
         .sub_drop {
         padding: 5px !important;
         }
         a:hover, a:focus {
         color: #2a6496;
         text-decoration: none;
         }
      </style>
      <script>
         /*	$('body').on(' click', '.sub_menu', function(e) {
         		 
         		$(this).toggleClass("cbp-hropen"); 
         		
         	});*/
         	 
         /*	 $('body').on(' blur', '.cbp-hropen', function(e) {
         
         		 $(this).removeClass("cbp-hropen"); 
         		 
         	});*/
         
         
         
      </script>