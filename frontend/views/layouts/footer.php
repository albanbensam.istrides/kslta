    <?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\models\KslNewsTbl;
use backend\models\SliderBanner;
use backend\models\GalleryPageModel;
use backend\models\ImagesView;
use backend\models\KslGalleryTitle;
use backend\models\HomepageNews;
use backend\models\TournamentsEventResult;
use backend\models\PlayerRegister;
use backend\models\Headlines;
use backend\models\MainMenu;

use backend\models\FooterMenu;
use backend\models\KslTournamentsTbl;
use backend\models\HomeNotification;

?>
    
    
    
       <!--SECTION FOOTER--> 
    <section id="footer-tag" style="padding-top: 0px;">
    	<div class="col-md-12">
    	<div class="col-md-3 left-con" style="background: white;">
           <img src="<?php echo Url::base(); ?>/frontend/web/img/logo2.png" class="footer-logo" alt="">
           </div>
           <!-- <div class="col-md-10 right-con" id="parallelogram">
           <div class="col-md-3 drop"> -->
           	<!-- <select class="selectpicker">
			  <option>English</option>
			  <option>Tamil</option>
			  <option>Hindi</option>
			</select> -->

           <!-- </div> -->
            <div class="col-md-9 foot-con right-con">
            	<p class="text">
            		<?php
            		$model=FooterMenu::find()->orderBy(['order_number'=>'SORT_ASC'])->all();
					if($model){
					foreach ($model as  $value) {
						if($value->menu_name=="calender"){
							echo '<a href="calander" class="test">calender</a>&nbsp;&nbsp;&nbsp;';
						}else{
						echo '<a href="'.$value->menu_link.'" class="test">'.$value->menu_name.'</a>&nbsp;&nbsp;&nbsp;';
					}
					}
					}
            		 ?>
            		
            		<!-- <a href="" class="test">Terms of Service</a>  /  <a href="" class="test">Privacy Policy</a>  /  <a href="" class="test">Downloads</a>  /  <a href="" class="test">Contact FIFA</a>  /  <a href="" class="test">Mobile Apps</a>  /  <a href="" class="test">Widgets</a> -->
            		
            		
            		</p>
            		<div class="copy">
          	 <span class="lefts"> &copy;2018 kslta-Karnataka State Lawn Tennis Association. Maintained by <a href="http://www.defocusstudio.com/" target="_blank">DeFocus Studio</a>  </span> 
          </div> 
          <span class="rights" style="float: right;">
          				
          				<a target="_blank" href="https://www.facebook.com/pages/The-KSLTA/348263411921591" class="social-icons"><i class="fa fa-facebook"></i></a>
                        <a target="_blank" href="https://twitter.com/hashtag/kslta" class="social-icons"><i class="fa fa-twitter"></i></a>
                       <!-- <a target="_blank" href="https://plus.google.com/"><i class="fa fa-google-plus"></i></a>-->
                        <a target="_blank" href="https://www.youtube.com/watch?v=8GBSqV6_LD8" class="social-icons"><i class="fa fa-youtube"></i></a>
                        <a target="_blank" href="http://www.instapu.com/tag/kslta"class="social-icons"><i class="fa fa-instagram"></i></a>
                        <a target="_blank" href="https://in.linkedin.com/company/karnataka-state-lawn-tennis-association---india" class="social-icons"><i class="fa fa-linkedin"></i></a>
                       
          </span>
           </div><br>
          
           </div>
    </section>
    <footer>
           <!-- <div class="col-md-12 content-footer"> -->
			
      </div>
     </div>
	</footer> 

 
</body>
</html>

<script language="JavaScript1.1">
<!--
// Open a window of the desired size in the center of the screen.

function openLSHDWindow(year, wkno, eventid, tour, lang, ref_file, width, height, hasScrollBars) {
	// ADD NAME FIELD and make sure it get's focus!!!
	var theWidth = width;
	var theHeight = height;
	var scrollBars = "scrollbars";
	if (hasScrollBars == false) scrollBars = "scrollbars=0";
	if ((theWidth == "")||(theWidth == null)) theWidth =1042;
	if ((theHeight == "")||(theHeight == null)) theHeight =631;
	var theLeft = (screen.availWidth - theWidth)/2;
	var theTop = (screen.availHeight - theHeight)/2;
	var strCheckRef = escape(ref_file);

	var lsURL = "http://www.protennislive.com/lshd/main.html?year="+year+"&wkno="+wkno+"&eventid="+eventid+"&tour="+tour+"&lang="+lang+"&ref="+strCheckRef;

	var popupWin = window.open(lsURL, '_' + Math.round(Math.random() * 1000000),'top='+theTop+',left='+theLeft+',menubar=0,toolbar=0,location=0,directories=0,status=0,'+scrollBars+',width='+theWidth+', height='+theHeight);
}
//-->
</script>

<style>
#footer-tag {
     background-color: #fff; 
}
#footer-tag .col-md-12 {
     padding: 0px 0px 0px 0px; 
    overflow: hidden;
}
.right-con{
		background: #fff !important;
		height: 194px;
		border-bottom: 110px solid #2673cf;
   		border-left: 50px solid transparent;
	}
	.copy{
		margin: 45px 0px 0px 0px;
		float: left;
		color:#fff;
		 font-size:13px;
	}
	.copy a{
		color: #ccc;
		font-size:13px;
	}
	.copy a:hover{
		color: #999999;
		font-size:13px;
	}
	 p.text, a.test {
		margin: 100px 35px 0px 0px;
		color:#fff;
		font-size:13px;
		text-align: right;
	}
	
	p.text, a.test:hover {
	 	margin: 100px 35px 0px 0px;
		color:#fefefe;
		font-size:13px;
	    text-align: right;
	    text-transform: uppercase;
	}
	.rights{
		margin: 45px 35px 0px 0px;
		color:#fff;
		float:right;
		margin-right: 8.5%;
	}
	.social-icons{
		font-size:17px;
		padding:5px;
		color:rgba(238, 238, 238, 0.62);
	}
	.social-icons:hover{
		font-size:17px;
		padding:5px;
		color:#eee;
	}
	.footer-logo{
		    margin: 110px 0px 0px 60px;
		width: 225px;
	}
	 
/*	#parallelogram { 
    height: 0;
    border-bottom: 100px solid red;
    border-left: 30px solid transparent;
    margin-left: 10px;
}*/
	/*.social-icons{
		font-size:17px;
		padding:5px;
		color:rgba(238, 238, 238, 0.62);
	}
	.social-icons:hover{
		font-size:17px;
		padding:5px;
		color:#eee;
	}
	
	.lefts{
		margin: 64px 0px 0px 0px; 
		color:rgba(238, 238, 238, 0.62);
		
	}
	.rights{
		margin: 64px 0px 0px 0px; 
		color:#fff;
		float:right;
		margin-right: 16.5%;
	}
	lectpicker{
		    background: #336296;
    color: #fff;
    border: 1px rgba(253, 253, 253, 0.52) solid;
	}
	#footer-tag{
		 background: #fff; 
		 height: 180px;
	}
	
	
	 
	.drop{
		margin: 64px 0px 0px 0px;
	}*/
	a:hover, a:focus {
    color: #2a6496;
    text-decoration: none;
}
</style>
