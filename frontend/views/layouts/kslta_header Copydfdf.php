<?php
use yii\helpers\Html;
use yii\helpers\Url;
use backend\models\KslNotification;
?>

    <!-- <section class="content-top-login">
			<div class="container">
				<div class="col-md-12">
					<div class="box-support">
						<p class="support-info">
							<i class="fa fa-envelope-o"></i> info@kslta.com
						</p>
					</div>
					
					<div class="box-login">
						
																						<a href='login.php'>Login</a>
										
																					</div>
					
					
					
				</div>
			</div>
		</section>-->
      <!--SECTION MENU --><?php $currenturl = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);?>
     <section class="container box-logo">
        <header>
			<div class="content-logo col-md-12" style="background: none;">
				<div class="logo">
					<a href="<?= Url::base(); ?>"><img src="<?php echo Url::to('@web/frontend/web/img/logo2.png');  ?>"  alt="" /></a>
					
				</div>
          		<div class="bt-menu"><a href="#" class="menu"><span>&equiv;</span> Menu</a></div>
         		<div class="box-menu">
		            <nav id="cbp-hrmenu" class="cbp-hrmenu">
							    <ul id="menu">    
		                            <li <?php if($currenturl=='' || $currenturl=='ksltacms'){ echo 'class="lnk-menu active"';} else { echo 'class="link-menu"';}?>><a  href="<?= Url::base();  ?>">Home</a></li>
		                            <li <?php if($currenturl=='about-kslta' || $currenturl=='commitee'){ echo 'class="lnk-menu active"';} else { echo 'class="link-menu"';}?>>
										<a href="#">About</a>
		                                <div class="cbp-hrsub sub-little">
		                                  <div class="cbp-hrsub-inner"> 
		                                      <div class="content-sub-menu">
										        <ul class="menu-pages">
										        	<li><a href="<?= Url::toRoute(['index.php/about-kslta']);  ?>"><span>KSLTA</span></a></li>
										        	<!--<li><a href=""><span>KSLTA Stadium</span></a></li>-->
											        <li><a href="commitee"><span>Managing Committee</span></a></li>
											        <!--
													<li><a href="#"><span>Former Presidents & Hon. Secretaries</span></a></li>
													<li><a href="#"><span>ITF</span></a></li>
													<li><a href="#"><span>AITA</span></a></li>
													<li><a href="#"><span>ATP Tour</span></a></li>
													<li><a href="#"><span>WTA Tour</span></a></li>
													<li><a href="#"><span>Affiliated clubs</span></a></li>
													<li><a href="#"><span>Other state Associations</span></a></li>-->
													
										          </ul>
		                                        </div>
		                                    </div>
		                                </div>
									</li>
								<!--<li <?php if($currenturl=='players.php'){ echo 'class="lnk-menu active"';}?>><a href="players.php">Players</a></li>-->
		                        <!--
								<li><a href="#">Coaches</a>
																	<div class="cbp-hrsub sub-little">
																		<div class="cbp-hrsub-inner"> 
																			<div class="content-sub-menu">
																				<ul class="menu-pages">
																					<li><a href="#"><span>Junior Boys</span></a></li>
																					<li><a href="#"><span>Junior Girls</span></a></li>
																					<li><a href="#"><span>Pro Tennis</span></a></li>
																					<li><a href="#"><span>Seniors</span></a></li>
																				</ul>
																			</div>
																		</div>
																	</div>    
																</li>-->
								<li <?php if($currenturl=='coaching'){ echo 'class="lnk-menu active"';} ?>><a href="#">Coaching</a>
									<div class="cbp-hrsub sub-little">
		                            	<div class="cbp-hrsub-inner"> 
		                                	<div class="content-sub-menu">
										        <ul class="menu-pages">
										        	<li><a href="<?= Url::toRoute(['index.php/coaching', 'type' => 'regular']);  ?>"><span>Regular Coaching</span></a></li>
										        	<li><a href="<?= Url::toRoute(['index.php/coaching', 'type' => 'advance']);  ?>"><span>Advance Coaching</span></a></li>
										        	<li><a href="<?= Url::toRoute(['index.php/coaching', 'type' => 'weekend']);  ?>"><span>Weekend Coaching</span></a></li>
						                        	
						                        </ul>
		                                	</div>
		                                </div>
		                            </div>
								</li>
								<li <?php if($currenturl=='calander'){ echo 'class="lnk-menu active"';}?>><a href="<?= Url::toRoute(['index.php/calander']);  ?>">Calendar</a></li>
		                        <li <?php if($currenturl=='tournament'){ echo 'class="lnk-menu active"';} elseif ($currenturl=='oct-19-gallery.php') { echo 'class="lnk-menu active"';} elseif ($currenturl=='news.php') { echo 'class="lnk-menu active"';} elseif ($currenturl=='draw.php') { echo 'class="lnk-menu active"';} elseif ($currenturl=='players.php') { echo 'class="lnk-menu active"';} else { echo 'class="link-menu"';}?>><a href="#">Events</a>
		                        	<div class="cbp-hrsub sub-little">
		                            	<div class="cbp-hrsub-inner"> 
		                                	<div class="content-sub-menu">
										        <ul class="menu-pages">
						                        	<li><a href="<?= Url::toRoute(['index.php/tournament', 'v' => '13']);  ?>"><span>Air Asia Open</span></a>
						                        	<li><a href="rtmnc-gallery.php"><span>RTNMC FEB 2016</span></a>
						                        		<!--
														<div class="">
																													<div class="cbp-hrsub-inner"> 
																														<div class="content-sub-menu">
																															<ul class="menu-pages">
																																<li><a href="news.php"><span>News</span></a></li>
																																<li><a href="draw.php"><span>Draw</span></a></li>
																																<li><a href="daily-schedule.php"><span>Daily Schedule</span></a></li>
																																<li><a href="oct-19-gallery.php"><span>Gallery</span></a></li>
																															</ul>
																														</div>
																													</div>
																												</div>-->
														
						                        	</li>
						                        </ul>
		                                	</div>
		                                </div>
		                            </div>
		                        </li>
		                        <li <?php if($currenturl=='bar-restaurant' ||  $currenturl=='party-hall' ||  $currenturl=='guest-room' ||  $currenturl=='swimming-pool' ||  $currenturl=='gym' ||  $currenturl=='tennis-billiards'){ echo 'class="lnk-menu active"';}else { echo 'class="link-menu"';}?>><a href="#">Facilities</a>
		                        	<div class="cbp-hrsub sub-little">
		                            	<div class="cbp-hrsub-inner"> 
		                                	<div class="content-sub-menu">
										        <ul class="menu-pages">
										        	<li><a href="<?= Url::toRoute(['index.php/bar-restaurant']);  ?>"><span>Bar & Restaurant</span></a></li>
										        	<li><a href="<?= Url::toRoute(['index.php/party-hall']);  ?>"><span>Party Halls</span></a></li>
										        	<li><a href="<?= Url::toRoute(['index.php/guest-room']);  ?>"><span>Guest Room</span></a></li>
										        	<li><a href="<?= Url::toRoute(['index.php/swimming-pool']);  ?>"><span>Swimming Pool</span></a></li>
										        	<li><a href="<?= Url::toRoute(['index.php/gym']);  ?>"><span>Health Club</span></a></li>
										        	<!--<li><a href="stadium.php"><span>TENNIS</span></a></li>-->
										        	<li><a href="<?= Url::toRoute(['index.php/tennis-billiards']);  ?>"><span>Table Tennis & Billiards</span></a></li>
						                        	
						                        </ul>
		                                	</div>
		                                </div>
		                            </div>
		                        </li>
		                        <!--<li <?php if($currenturl=='#'){ echo 'class="lnk-menu active"';}?>><a href="#">Club House</a></li>
		                        <li <?php if($currenturl=='#'){ echo 'class="lnk-menu active"';}?>><a href="#">Gallery</a></li>-->
		                        <li <?php if($currenturl=='contact'){ echo 'class="lnk-menu active"';}?>><a href="<?= Url::toRoute(['index.php/contact']);  ?>">Contact</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</header>
		</section>
		
		<section class="content-quicklink" style="box-shadow: 0 3px 2px #939393;">
			<div class="container">
					<div class="box-support2">
						<div class="box-menu">
							<nav id="cbp-hrmenu" class="cbp-hrmenu2" style="margin-top: 0px;">
								<ul id="menu" class="cbp-hrmenu"> 
									<!--<li><p class="support-info">Quick Link:</p></li>-->
									<?php 	
					
						$all_news=KslNotification::find()->where(['notify_status'=>'A'])->orderby(['notify_id'=> SORT_ASC])->all();
					
					foreach($all_news as $one_news)
					{
						          echo  '<li><a href="'.Url::to('@web/index.php/'.$one_news->notify_link).'" style="font-weight: 900; font-size: 20px; color:'.$one_news->notify_color.';">'.$one_news->notify_text.'</a></li>';
					} ?>
									<!--<li><a href="member-series-june-2016.php" style="font-weight: 900; font-size: 20px;">INTRA CLUB TOURNAMENT FOR MEMBERS FROM 3rd-5th JUNE</a></li>
									<li><a href="tallent-series-june-2016-u14.php" style="font-weight: 900; font-size: 20px; color: #F4FA4E;">AITA TALENT SERIES FOR B&G U 14</a></li>-->
									
									<!--<li><a href="summer-camp-2016.php">SUMMER CAMP 2016</a></li>
									
									<li <?php if($currenturl=='about-kslta.php'){ echo 'class="lnk-menu2 active2"';} elseif ($currenturl=='commitee.php') { echo 'class="lnk-menu2 active2"';} else { echo 'class="link-menu2"';}?>><a  href="#">Daily Schedule</a></li>
																		<li <?php if($currenturl=='about-kslta.php'){ echo 'class="lnk-menu2 active2"';} elseif ($currenturl=='commitee.php') { echo 'class="lnk-menu2 active2"';} else { echo 'class="link-menu2"';}?>><a  href="#">Draw</a></li>
																		<li <?php if($currenturl=='about-kslta.php'){ echo 'class="lnk-menu2 active2"';} elseif ($currenturl=='commitee.php') { echo 'class="lnk-menu2 active2"';} else { echo 'class="link-menu2"';}?>><a  href="#">News</a></li>
																		<li <?php if($currenturl=='about-kslta.php'){ echo 'class="lnk-menu2 active2"';} elseif ($currenturl=='commitee.php') { echo 'class="lnk-menu2 active2"';} else { echo 'class="link-menu2"';}?>><a  href="#">Gallery</a></li>-->
									
								</ul>
							</nav>
						</div>		
					</div>
				</div>
		</section>
		
