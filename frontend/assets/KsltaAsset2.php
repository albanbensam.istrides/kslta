<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */

class KsltaAsset2 extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
   	 	'frontend/web/css/site.css',   
   		'frontend/web/css/bootstrap.css',  
        'frontend/web/css/fonts/font-awesome-4.1.0/css/font-awesome.min.css',
        'frontend/web/css/own/owl.carousel.css',
        'frontend/web/css/own/owl.theme.css',
        'frontend/web/css/jquery.bxslider.css',
        'frontend/web/css/jquery.jscrollpane.css',
        'frontend/web/css//minislide/flexslider.css',
        'frontend/web/css/component.css',
        'frontend/web/css/prettyPhoto.css',
        'frontend/web/css/style_dir.css',
        'frontend/web/css/responsive.css',
        'frontend/web/css/animate.css',
        'backend/web/plugins/multiselect/sumoselect.css',
        'backend/web/plugins/datepicker/datepicker3.css',
        'backend/web/plugins/daterangepicker/daterangepicker-bs3.css',
    ];
    public $js = [  
        'backend/web/plugins/multiselect/jquery.sumoselect.js',
        'backend/web/plugins/datepicker/moment.min.js',
   		'backend/web/plugins/daterangepicker/daterangepicker.js',
   		'backend/web/plugins/datepicker/bootstrap-datepicker.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}

