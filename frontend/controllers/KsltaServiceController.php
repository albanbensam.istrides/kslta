<?php

namespace frontend\controllers;

use yii\helpers\Url;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use backend\models\ApiUser;

use yii\web\UploadedFile;
class KsltaServiceController extends Controller
{  
	/** 
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
 public function beforeAction($action) {
	$params = (Yii::$app->request->headers);
	if($authorization=$params['authorization']){		
		$this->enableCsrfValidation = false;
    	return parent::beforeAction($action);
	}else{
		$list['status'] = 'error';
		$list['message'] = 'Bad Request ';
		$response=$list;
		echo json_encode($response);
		die;
	}
} 
function authorization(){
	$params = (Yii::$app->request->headers);
	$authorization=$params['authorization'];
	$authorization=str_replace('Bearer', '', $authorization);
	$authorization=trim($authorization);
	$user_data_role = ApiUser::find()
					->where(['auth_key'=>$authorization])
					->one();
	if($user_data_role){
		return $user_data_role;
	}else{
		return false;
	}
}
public function actionUploadMemberInvoice()
{
		$list = array();
		$list['status'] = 'error';
		$list['message'] = 'Invalid Authorization Request!';
		if($user_data_role=$this->authorization()){
        $postd=(Yii::$app ->request ->rawBody);
        $requestInput = json_decode($postd,true);
		$field_check=array('api_method','User_ac_no','bill_number','total_amount','rec_no');
		$is_error = '';
		foreach ($field_check as $one_key) {
			  $key_val =isset($requestInput[$one_key]);
				if ($key_val == '') {
					$is_error = 'yes';
					$is_error_note = $one_key;
					break;
			  }
		}
		if ($is_error == "yes") {
			  $list['message'] = $is_error_note . ' is Mandatory.';			 
		}else{
			
			$list['message'] = 'Data Saved Successfully.';
		}
		}
		//print_r($employe_data);
 		$response=$list;
		return json_encode($response);
}
public function actionUploadMemberPayment()
{
		$list = array();
		$list['status'] = 'error';
		$list['message'] = 'Invalid Authorization Request!';
		if($user_data_role=$this->authorization()){
        $postd=(Yii::$app ->request ->rawBody);
        $requestInput = json_decode($postd,true);
		$field_check=array('api_method','User_ac_no','pay_date','pay_month','pay_year','amount','type');
		$is_error = '';
		foreach ($field_check as $one_key) {
			  $key_val =isset($requestInput[$one_key]);
				if ($key_val == '') {
					$is_error = 'yes';
					$is_error_note = $one_key;
					break;
			  }
		}
		if ($is_error == "yes") {
			  $list['message'] = $is_error_note . ' is Mandatory.';			 
		}else{
			
			$list['message'] = 'Data Saved Successfully.';
		}
		}
		//print_r($employe_data);
 		$response=$list;
		return json_encode($response);
}


}


