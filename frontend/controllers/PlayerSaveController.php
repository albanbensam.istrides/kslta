<?php

namespace frontend\controllers;

use Yii;
use backend\models\TournamentSchedule;
use backend\models\PageContents;
use backend\models\GalleryPageModel;
use backend\models\KslGalleryModel;
use backend\models\CoachingPages;
use backend\models\TournamentGallery;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\UploadedFile;
use backend\models\PlayerRegister;
use backend\models\PlayerRegisterSearch;
use backend\models\TournamentEventModel;
use yii\helpers\ArrayHelper;
use backend\models\DropdownManagement;
use backend\models\PlayerEvents;
use backend\models\Guestroombooking;

/**
 * TournamentScheduleController implements the CRUD actions for TournamentSchedule model.
 */
class PlayerSaveController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function actionEventvaluelist($id){
      $rows = TournamentEventModel::find()->where(['tourn_id' => $id])->all(); 
        //echo '<option>Select Event</option>'; 
        if(count($rows)>0){
        	$id_db_all=array();
			$id_db_cat=array();
        	foreach($rows as $one_event){
				$id_db_all[]=$one_event->event_category;
				$id_db_all[]=$one_event->event_subcategory;
				$id_db_cat[$one_event->event_category][$one_event->event_id]=$one_event->event_subcategory;																									
			}
			if(count($id_db_all)>0){
				$event_names = ArrayHelper::map(DropdownManagement::find()->FilterWhere(array('in', 'dropdown_id', $id_db_all))->all(), 'dropdown_id', 'dropdown_value');
			}
			foreach($id_db_cat as $one_categoryid=>$one_subcategory){
				$one_category_txt='';
				if(isset($event_names[$one_categoryid])){
					$one_category_txt=$event_names[$one_categoryid];
				}
				echo '<optgroup label="'.$one_category_txt.'">';
						foreach($one_subcategory as $event_id=>$one_event){
						$one_subcategory_txt='';
						if(isset($event_names[$one_event])){
							$one_subcategory_txt=$event_names[$one_event];
						}
		                echo '<option value="'.$event_id.'">'.$one_category_txt.' '.$one_subcategory_txt.'</option>';
		            }
				echo '</optgroup>';
			}			
        }
      
    }
public function actionPlayersave(){
	 $model = new PlayerRegister();
		if ($model->load(Yii::$app->request->post())){
        $myStr = rand(0, 9999);
		$playerregister_post=Yii::$app->request->post('PlayerRegister');		
        if(isset($playerregister_post['reg_player_id'])){
            $model->reg_player_id =  $playerregister_post['reg_player_id'];
        }
        else{
            $model->reg_player_id = date("dmy").$myStr ; //random registeration id generation
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        		//echo $model->reg_id;die;
        		if(count($playerregister_post['tourn_event_id'])>0){
					foreach($playerregister_post['tourn_event_id'] as $one_event){
								if($one_event!=""){
								$event_dbdata[]=['ue_userid'=> $model->reg_id,'ue_tournamentid'=>$playerregister_post['tourn_id'],'ue_eventid'=>$one_event];
								}
							}	
							$modelplayerevent = new PlayerEvents();		
							$modelplayerevent->inset_playerevent_data($event_dbdata);
				}
            Yii::$app->getSession()->setFlash('success', 'Player details has been created successfully.');
           return $this->redirect(['success']);
        } 
        }else {
            //Yii::$app->getSession()->setFlash('error', 'Something went wrong !');
            
        }
}
 public function actionSuccess()
    {
    	return $this->render('success');
    }
    /* playerregister End */
    
     public function actionGuestform()
	    {
    	//echo "hai";
		 
			  $model = new Guestroombooking();
			  if ($model->load(Yii::$app->request->post())){
			  	
			 $checkindate=$_POST['Guestroombooking']['checkindate'];
			 $model->checkindate=$checkindate;	
			 
			 $checkoutdate=$_POST['Guestroombooking']['checkoutdate'];
			$model->checkoutdate=$checkoutdate;				 
			
			$adultcount=$_POST['Guestroombooking']['adultcount'];
			$model->adultcount=$adultcount;	
			
			$childrencount=$_POST['Guestroombooking']['childrencount'];
			$model->childrencount=$childrencount;	
			
			$name=$_POST['Guestroombooking']['name'];
			$model->name=$name;
			
			$mobile=$_POST['Guestroombooking']['mobile'];
			$model->mobile=$mobile;
			
			$email=$_POST['Guestroombooking']['email'];
			$model->email=$email;		
			
			$model->status="Request";
			
			//$model->createdate=date('Y-m-d');
					
			//$model->save();
			if($model->save())
			{
				echo "s";
			}
			
		
			  } 
			
		//  return $this->redirect(['success1']);
    }

public function actionSuccess1()
    {
    	return $this->render('success1');
    }
	/*public function actionAffilatedstatesearch()
   {
    	return $this->render('affilatedstatesearch');
    } */  

}
