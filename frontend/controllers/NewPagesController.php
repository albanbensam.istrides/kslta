<?php

namespace frontend\controllers;

use Yii;
use backend\models\TournamentSchedule;
use backend\models\PageContents;
use backend\models\GalleryPageModel;
use backend\models\KslGalleryModel;
use backend\models\CoachingPages;
use backend\models\TournamentGallery;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\UploadedFile;
use backend\models\PlayerRegister;
use backend\models\PlayerRegisterSearch;
use backend\models\TournamentEventModel;
use yii\helpers\ArrayHelper;
use backend\models\DropdownManagement;
use backend\models\PlayerEvents;



class NewPagesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    public function actionCubbonparkbangalore()
    {
    
      $pageModel = PageContents::find()->where(['page_name'=> 'CUBBON PARK'])->one();

       return $this->render('cubbonpark', [
                'pageModel' => $pageModel,
            ]);
    }


    public function actionCommitee()
    {
    
      $pageModel = PageContents::find()->where(['page_name'=> 'commitee'])->one();

       return $this->render('commitee', [
                'pageModel' => $pageModel,
            ]);
    }

    public function actionAffilatedclubs()
    {
        
      $pageModel = PageContents::find()->where(['page_name'=> 'affilated-clubs'])->one();
	 
       return $this->render('affilatedclubs', [
                'pageModel' => $pageModel,
            ]);
    }

    public function actionAita()
    {
        
      $pageModel = PageContents::find()->where(['page_name'=> 'aita-tournament'])->one();

       return $this->render('aita', [
               'pageModel' => $pageModel,
            ]);
    }
	
	

    
}
