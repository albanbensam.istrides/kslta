<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use backend\models\ApiUser;
class KsltaApiLoginController extends Controller
{  
	/** 
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
 public function beforeAction($action) {    
	//$params = (Yii::$app->request->headers);
	//if($authorization=$params['authorization']){		
		$this->enableCsrfValidation = false;
    	return parent::beforeAction($action);
	/*}else{
		$list['status'] = 'error';
		$list['message'] = 'Bad Request ';
		$response['Output'][]=$list;
		echo json_encode($response);
		die;
	}*/
} 
public function actionApilogin()
{
	$list = array();
	$postd=(Yii::$app ->request ->rawBody);
	
	$requestInput = json_decode($postd,true);
//log table
	/*$log_model=new WorkshopApiLog();
	$log_model->event='crm-login';
	//$log_model->event_key='crm_app';
	$log_model->data=$postd;
	$log_id='';
	if($log_model->save()){
		$log_id=$log_model->autoid;
	}*/
	
	
	$list['status'] = 'error';
	$list['message'] = 'Nill';
		
		$field_check=array('username','password');
		 $is_error = '';
		 foreach ($field_check as $one_key) {
			  $key_val =isset($requestInput[$one_key]);
				if ($key_val == '') {
					$is_error = 'yes';
					$is_error_note = $one_key;
					break;
			  }
		} 

		if ($is_error == "yes") {
			  $list['status'] = 'error';
			  $list['message'] = $is_error_note . ' is Mandatory.';
			 
		}else{
		$user_data_role = ApiUser::find()
					->where(['email'=>$requestInput['username']])
					->andWhere(['status_flag'=>'A'])
					//->andWhere(['<>','login_type',''])
					->one(); 
					//print_r($user_data_role);die;
		if(count($user_data_role)>0){
			 $password=$requestInput['password'];
			 $haspassword=$user_data_role->password_hash;
			if(Yii::$app->security->validatePassword($password,$haspassword)){
				$list['status'] = "success";
				$list['message'] = "Logged In successfully";
				$list['authorization'] = $user_data_role->auth_key;
			}else{
				$list['message'] = 'Invalid Login';
			}
		}else{
			$list['message'] = 'Invalid Login';
		}
	}
	
	 $response= $list;
	 /*	$log_model=WorkshopApiLog::findOne($log_id);
		 if($log_model){
		 	$log_model->response=json_encode($response);
			 $log_model->save();
		 }
	 */
  return json_encode($response);
}

}
