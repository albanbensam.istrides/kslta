<?php

namespace frontend\controllers;

use Yii;
use backend\models\TournamentSchedule;
use backend\models\PageContents;
use backend\models\GalleryPageModel;
use backend\models\KslGalleryModel;
use backend\models\CoachingPages;
use backend\models\TournamentGallery;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\UploadedFile;
use backend\models\PlayerRegister;
use backend\models\PlayerRegisterSearch;
use backend\models\TournamentEventModel;
use yii\helpers\ArrayHelper;
use backend\models\DropdownManagement;
use backend\models\PlayerEvents;
use backend\models\Affilatedclubs;
use backend\models\States;
use yii\filters\AccessControl;
/**
 * TournamentScheduleController implements the CRUD actions for TournamentSchedule model.
 */
class TournamentScheduleController extends Controller
{
    public function behaviors()
    {
        return [
            /*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],*/
             
        
			
        ];
    }
	public function beforeAction($action) {
    $this->enableCsrfValidation = false;
    return parent::beforeAction($action);
}

    /**
     * Lists all TournamentSchedule models.
     * @return mixed
     */
    public function actionIndex()
    {
    	
    	$page_key=Yii::$app->request->get('v');
		if($page_key!=""){
			$page_name='pagegeneral';
			 if($page_key=='contact' || $page_key=='contactus'){
				$page_name='contact';
			}elseif($page_key=='affilatedclubs' || $page_key=='affilated_clubs'){
				$page_name='affilatedstatesearch';
				 $Affilatedclubs=Affilatedclubs::find()->all(); 
				 return $this->render($page_name,['Affilatedclubs1'=>$Affilatedclubs]);
			}
			
			elseif($page_key=='aboutkslta'){
				$page_name='about-kslta'; 
				 $pagedataModel = PageContents::find()->where(['page_name' => $page_key])->one();
				 if($pagedataModel){ 
		      	//$tabModel = CoachingPages::find()->where(['coaching_page_name' => 'guest-room'])->one();
		      	$galleryModel = GalleryPageModel::find()->where(['gal_image_rootid' => $pagedataModel->gallery_id])->all();
				
			      return $this->render($page_name, [
			                 'pageModel' => $pagedataModel,
			                 //'tabmodel' => $tabModel,
			                 'gallerymodel' => $galleryModel,
			                 'page_key'=>$page_key
			            ]);
		 }
			}
			
			elseif($page_key=='commitee'){
				
				$page_name='commitee'; 
				 $pagedataModel = PageContents::find()->where(['page_name' => $page_key])->one();
				 if($pagedataModel){ 
		      	//$tabModel = CoachingPages::find()->where(['coaching_page_name' => 'guest-room'])->one();
		      	$galleryModel = GalleryPageModel::find()->where(['gal_image_rootid' => $pagedataModel->gallery_id])->all();
				 
			      return $this->render($page_name, [
			                 'pageModel' => $pagedataModel,
			                 //'tabmodel' => $tabModel,
			                 'gallerymodel' => $galleryModel,
			                 'page_key'=>$page_key
			            ]);
		 }
			}
			
		 $pagedataModel = PageContents::find()->where(['page_name' => $page_key])->one();
		 if($pagedataModel){ 
      	//$tabModel = CoachingPages::find()->where(['coaching_page_name' => 'guest-room'])->one();
      	$galleryModel = GalleryPageModel::find()->where(['gal_image_rootid' => $pagedataModel->gallery_id])->all();
		// echo $page_name; die;
	      return $this->render($page_name, [
	                 'pageModel' => $pagedataModel,
	                 'page_name' => $page_name,
	                 'gallerymodel' => $galleryModel,
	                 'page_key'=>$page_key
	            ]);
		 }else{
		  
		 	$coachingModel = CoachingPages::find()->where(['coaching_page_name' => $page_key])->one();
				if($coachingModel){
              $galManageModel = KslGalleryModel::find()->where(['gallery_id' => $coachingModel->gallery_id])->one();

              $galMultipleModel = GalleryPageModel::find()->where(['gal_image_rootid' => $coachingModel->gallery_id])->all();
			 
			  return $this->render('coaching', [
                   'coaching' => $coachingModel,
                   'galManagedata' => $galManageModel, 
                   'gallerymultiple' => $galMultipleModel,
                   'page_key'=>$page_key
            ]);
				}else{ 		 	
			 return $this->redirect(['/']);
		}
		 }
		}else{
 		 	 return $this->redirect(['/']);
		}
    }


    /* Coaching Dynamic Controller Start */
     public function actionCoaching(){
			
			      if(isset($_GET['type'])){
              $type = $_GET['type'];
              $coachingModel = CoachingPages::find()->where(['coaching_page_name' => $type])->one();

              $galManageModel = KslGalleryModel::find()->where(['gallery_id' => $coachingModel->gallery_id])->one();

              $galMultipleModel = GalleryPageModel::find()->where(['gal_image_rootid' => $coachingModel->gallery_id])->all();  
         if($coachingModel){
         //	echo $type; die;
          return $this->render('coaching', [
                   'coaching' => $coachingModel,
                   'type' =>$type,
                   'galManagedata' => $galManageModel, 
                   'gallerymultiple' => $galMultipleModel,
            ]);
		 }else{ 		 	
			 return $this->redirect(['/']);
		}
		  }else{ 		 	
			 return $this->redirect(['/']);
		}
     }
      /* Coaching Dynamic Controller End */
    public function actionCalander()
    {
 		 return $this->render('calander');
    }

    public function actionContact()
    {
         return $this->render('contact');
    }

	public function actionAboutkslta()
    {
    
      $pageModel = PageContents::find()->where(['page_name'=> 'ABOUT KSLTA'])->one();

       return $this->render('about-kslta', [
                'pageModel' => $pageModel,
            ]);
    }


 /*   public function actionRegularcoaching(){

        $tabModel = CoachingPages::find()->where(['coaching_page_name' => 'regular'])->one();
      $galleryModel = GalleryPageModel::find()->where(['gal_image_rootid' => $pagedataModel->gallery_id])->all();
      return $this->render('regularcoaching', [
                 'tabmodel' => $tabModel,
                 'gallerymodel' => $galleryModel
            ]);

    }

    public function actionAdvancedcoaching(){

      $tabModel = CoachingPages::find()->where(['coaching_page_name' => 'advance'])->one();
      $galleryModel = GalleryPageModel::find()->where(['gal_image_rootid' => $pagedataModel->gallery_id])->all();
      return $this->render('advancedcoaching', [
                 'tabmodel' => $tabModel,
                 'gallerymodel' => $galleryModel
            ]);


    }
    public function actionWeekendcoaching(){
       $tabModel = CoachingPages::find()->where(['coaching_page_name' => 'weekend'])->one();
      $galleryModel = GalleryPageModel::find()->where(['gal_image_rootid' => $pagedataModel->gallery_id])->all();
      return $this->render('weekendcoaching', [
                 'tabmodel' => $tabModel,
                 'gallerymodel' => $galleryModel
            ]);
    }*/
    public function actionTournament(){
        return $this->render('tournament-view');
    }


  /* public function actionBarrestaurant(){

      $pagedataModel = PageContents::find()->where(['page_name' => 'Bar'])->one();
	    //echo $controler_url_id=Yii::$app->controller->id;
		// echo $active_url_id=Yii::$app->controller->action->id;
      $galleryModel = GalleryPageModel::find()->where(['gal_image_rootid' => $pagedataModel->gallery_id])->all();
        
      return $this->render('barrestaurant', [
                 'pagemodel' => $pagedataModel,
                 'gallerymodel' => $galleryModel
            ]);

    }*/

    public function actionPartyhall(){
    	 
      $pagedataModel = CoachingPages::find()->where(['coaching_top_main_heading' => 'Party Hall'])->one();
	 
      $galleryModel = GalleryPageModel::find()->where(['gal_image_rootid' => $pagedataModel->gallery_id])->all();
 
      return $this->render('partyhall', [
                 'pagemodel' => $pagedataModel,
                 'gallerymodel' => $galleryModel
            ]);

    }

    public function actionGuestroom(){
     
      $pagedataModel = PageContents::find()->where(['page_name' => 'Guest'])->one();
      $tabModel = CoachingPages::find()->where(['coaching_page_name' => 'guest-room'])->one();
      $galleryModel = GalleryPageModel::find()->where(['gal_image_rootid' => $pagedataModel->gallery_id])->all();
      return $this->render('guestroom', [
                 'pagemodel' => $pagedataModel,
                 'tabmodel' => $tabModel,
                 'gallerymodel' => $galleryModel
            ]);

    }
	
	  

   /* public function actionSwimmingpool(){
      $pagedataModel = PageContents::find()->where(['page_name' => 'Swimming'])->one();
      $galleryModel = GalleryPageModel::find()->where(['gal_image_rootid' => $pagedataModel->gallery_id])->all();
      return $this->render('swimmingpool', [
                 'pagemodel' => $pagedataModel,
                 'gallerymodel' => $galleryModel
            ]);
    }*/

/*
    public function actionGym(){
      $pagedataModel = PageContents::find()->where(['page_name' => 'Gym'])->one();
      $galleryModel = GalleryPageModel::find()->where(['gal_image_rootid' => $pagedataModel->gallery_id])->all();

      return $this->render('gym', [
                 'pagemodel' => $pagedataModel,
                 'gallerymodel' => $galleryModel
            ]);
    }
    public function actionTennisbilliards(){
      $pagedataModel = PageContents::find()->where(['page_name' => 'Ttennis'])->one();
      $galleryModel = GalleryPageModel::find()->where(['gal_image_rootid' => $pagedataModel->gallery_id])->all();

      return $this->render('tennisbilliards', [
                 'pagemodel' => $pagedataModel,
                 'gallerymodel' => $galleryModel
            ]);

    }*/

    
 public function actionNewsall(){
        return $this->render('news_all');
    }
 public function actionNewsview(){ 	
        return $this->render('news_view');
    }
  public function actionDrawsheet(){ 	
        return $this->render('drawsheet');
    }
 public function actionGalleryview(){ 	
        return $this->render('galleryview');
    }


    /* Tournament Gallery Multiple View Controllser Start */
    public function actionTournamentgalleryview(){
        $tournamenetId = $_GET['t'];
        $tournamentModel = TournamentGallery::find()->where(['tourn_id' => $tournamenetId])->all();
        $tournGalleryId = 0;
        foreach ($tournamentModel as $tournvalue) {
            $tournGalleryId = $tournvalue->gallery_id;
            break;
        }
        $galModelData = KslGalleryModel::find()->where(['gallery_id' => $tournGalleryId])->all();
        if(isset($_GET['listgal'])){
            $galManageModel = KslGalleryModel::find()->where(['gallery_id' => $_GET['listgal'] ])->one();
        }else{
          $galManageModel = '';
        }
        return $this->render('tournamentgalleryview', [
                 'tornmultiGallery' => $tournamentModel,
                 'galleryDetails' => $galModelData,
                 'galleryManage' => $galManageModel, 
            ]);
    }

    /* Tournament Gallery Multiple View Controllser End */





/* Tournament Daily schedule Start */
     public function actionDailyschedule(){ 
        return $this->render('daily_schedule');
     }
     /* Tournament Daily schedule End */
     
    /*Stadium */
      public function actionStadium(){
    //  echo '<pre>';
    //  print_r('sdlfk');
    //  exit();
      
    }
    /* Stadium End */
    
        /*playerregister */
      public function actionPlayerregister(){
      	$this->layout = 'playerregisterlayout';
     	 $playerregister = new PlayerRegister();
		 return $this->render('playerregister', [
                'model' => $playerregister,
            ]);
    
      
    }

    /* playerregister End */
    public function actionDropstate($id)
    {
      			if($id!=''){
        $Affilatedclubs1= Affilatedclubs::find()->where(['state_id'=>$id])->all();  
					}else{
						  $Affilatedclubs1= Affilatedclubs::find()->all(); 
					}   	  
		 $html_output='';
		    $th=0;
			$th_m=0;
			$d=States::find()->all();
    		$a= ArrayHelper::map($d ,'state_id','state_name');			
		    foreach($Affilatedclubs1 as $one_club){
		    $th_m=$th%3 ;  
		    if($th_m==0){
		    $html_output.='<dir id="info-company" class="affclubs">';
		    }
			if(isset($a[$one_club->state_id])){
		    $state_name=$a[$one_club->state_id];
			}
			$html_output.='<div class="col-md-offset-1 col-md-3" style="width: 285px; margin-left: 15px; height: 250px;bottom: -15px; padding: 20px;background: rgba(204, 204, 204, 0.32);"><h4  class="head"  style="color: #428bca;"><strong>'. $state_name.'</strong></h4><h5><strong>'. $one_club->name.'</strong></h5>';
			if($one_club->address1!=''){
				$html_output.='<p style="line-height: 20px;">'.$one_club->address1.'</p>';
			}
			
			if($one_club->address2!=''){
				$html_output.='<p >'.$one_club->address2.'</p>';
			}
			if($one_club->mobile!=''){
				$html_output.='<p >'.$one_club->mobile.'</p>';
			}
			if($one_club->phone!=''){
				$html_output.='<p >'.$one_club->phone.'</p>';
			}
			if($one_club->fax!=''){
				$html_output.='<p >'.$one_club->fax.'</p>';
			}
			if($one_club->email!=''){
				$html_output.='<p >'.$one_club->email.'</p>';
			}
			if($one_club->website!=''){				
				//$html_output.='<p>'.$one_club->website.'</p>';				
				$html_output.='<p ><a href="http://'.$one_club->website.'"target="_blank">'.$one_club->website.'</a></p>';			
				
			}
			$html_output.='</div>';
			 if($th_m==2){
			$html_output.='</div><div class="clearfix">&nbsp;</div>';
			 }
			$th++;
			
		} 
		if($th_m!=0){
			$html_output.='</div><div class="clearfix">&nbsp;</div>';
		}
		if($html_output!=''){
			 echo $html_output;
			}else{
				echo "No Result Found.";
			}
		   
	//	   $page_name='affilatedstatesearch';		  
		// return $this->render($page_name);
		/*  return $this->render($page_name, [
                'model' => $Affilatedclubs,
            ]);*/
    } 
      public function actionGall($id)
    {
 	
		$home_gallery = GalleryPageModel::find()->where(['gal_image_rootid'=>$id])->all();
		
		foreach ($home_gallery as $values) {
			
			$image = Url::to('@web/backend/web/galleryimage/gallery_pages/'.$values->gal_image_path);
			$img = "<li id=".$id." data-responsive='' data-src='".$image."' ><a href='".$image."'>
                 <img class='img-responsive' src='".$image."' style='width: 50%;'><span style='margin-right: 45%;'></span>
                    </a>
                </li>";
				echo $img;
		}
    
    }
   
}
