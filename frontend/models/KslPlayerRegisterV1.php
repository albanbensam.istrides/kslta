<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ksl_player_register_v1".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $dob
 * @property string $place_of_birth
 * @property string $father_name
 * @property string $mother_name
 * @property string $guardian_details
 * @property string $address
 * @property string $contact_no
 * @property string $email_id
 * @property string $active_status
 * @property string $created_at
 * @property string $updated_at
 */
class KslPlayerRegisterV1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ksl_player_register_v1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dob', 'created_at', 'updated_at'], 'safe'],
            [['address'], 'string'],
            [['first_name', 'last_name', 'place_of_birth', 'father_name', 'mother_name', 'guardian_details'], 'string', 'max' => 255],
            //[['contact_no'], 'string', 'max' => 30],
            [['email_id', 'active_status'], 'string', 'max' => 50],
            [[ 'first_name','last_name','dob','place_of_birth','father_name','address','contact_no','email_id'], 'required'],
            [['contact_no'], 'integer','message'=>'Contact Number must be Number Format'],
            [['email_id'], 'email'],
            [['email_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'dob' => 'Date of Birth',
            'place_of_birth' => 'Place Of Birth',
            'father_name' => 'Father Name',
            'mother_name' => 'Mother Name',
            'guardian_details' => 'Guardian Details',
            'address' => 'Address',
            'contact_no' => 'Contact No',
            'email_id' => 'Email ID',
            'active_status' => 'Active Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
