<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ksl_academy_reg".
 *
 * @property integer $id
 * @property string $academy_name
 * @property string $owner
 * @property string $contact_number
 * @property string $email_id
 * @property string $address
 * @property string $no_of_courts_clay
 * @property string $no_of_courts_hard
 * @property string $no_of_courts_others
 * @property string $no_of_courts_flood
 * @property string $no_of_coaches
 * @property string $office
 * @property string $pro_shop
 * @property string $cafeteria
 * @property string $toilet
 * @property string $room
 * @property string $gym
 * @property string $warm_up
 * @property string $water
 * @property string $other_information
 * @property string $remarks
 * @property string $active_status
 * @property string $created_at
 * @property string $updated_at
 */
class KslAcademyReg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ksl_academy_reg';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['academy_name', 'address', 'remarks'], 'string'],
            [['created_at', 'updated_at','toilet'], 'safe'],
            [['owner'], 'string', 'max' => 255],
            [['no_of_courts_clay', 'no_of_courts_hard', 'no_of_courts_others', 'no_of_courts_flood', 'no_of_coaches', 'office', 'pro_shop', 'cafeteria', 'room', 'gym', 'warm_up', 'water', 'other_information', 'active_status'], 'string', 'max' => 50],
            [[ 'academy_name','owner','contact_number','email_id','address'], 'required'],
            [['contact_number'], 'integer','message'=>'Contact Number must be Number Format'],
            [['email_id'], 'email'],
            [['email_id'], 'unique'],
        ];
    }

    public function isNumeric($attribute, $params)
    {
        if (!is_numeric($this->contact_number))
            $this->addError($attribute, Yii::t('app', 'Mobile Number Should Be in Number Format'));
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'academy_name' => 'Academy Name',
            'owner' => 'Owner',
            'contact_number' => 'Contact Number',
            'email_id' => 'Email ID',
            'address' => 'Address',
            'no_of_courts_clay' => 'No Of Courts Clay',
            'no_of_courts_hard' => 'No Of Courts Hard',
            'no_of_courts_others' => 'No Of Courts Others',
            'no_of_courts_flood' => 'No Of Courts Flood Light',
            'no_of_coaches' => 'Total No Of Other Coaches',
            'office' => 'Office',
            'pro_shop' => 'Pro Shop',
            'cafeteria' => 'Cafeteria',
            'toilet' => 'Toilet',
            'room' => 'Room',
            'gym' => 'Gym',
            'warm_up' => 'Warm Up',
            'water' => 'Water',
            'other_information' => 'Other Information',
            'remarks' => 'Remarks',
            'active_status' => 'Active Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
