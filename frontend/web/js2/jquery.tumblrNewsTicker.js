(function($) {
	
	var defaults = {
		time:	5000,
		title:	'Tumblr News Ticker',
		blog:	'http://tzinenewsdemo.tumblr.com'
	};
	
	$.fn.tumblrNewsTicker = function(options) {
		
		// Handle the default settings
		var settings = $.extend({}, defaults, options);
		
		// Remove trailing slashes and Assemble the Tumblr API URL 
		var url = settings.blog.replace(/\/$/,'') + "/api/read/json?num=20&type=text&callback=?";
		
		this.append('<div class="tn-container">\
						<h2 class="tn-header">'+ settings.title +'</h2>\
						<div class="tn-data"><ul></ul></div>\
						<div class="tn-footer"></div>\
					</div>');

		var postList = this.find('.tn-data ul');
		//alert(url);
		//Get the posts as json	
	//	$.getJSON(url, function(data) {
			/*var tumblr_api_read = {"tumblelog":{"title":"News","description":"","name":"amazing-jquery-plugins","timezone":"","cname":false,"feeds":[]},
						"posts":[
						{"regular-title":"44Easy Responsive Gallery & Lightbox Plugin For jQuery","regular-body":"","tags":[]},
						{"regular-title":"66Minimal Instagram-style Sticky Header Plugin With jQuery - Stickish","regular-body":"","tags":[]},
						{"regular-title":"4444Tiny SVG Panning And Zooming Plugin With jQuery - Zoom SVG","regular-body":"","tags":[]},
						{"regular-title":"4444Tiny SVG Panning And Zooming Plugin With jQuery - Zoom SVG","regular-body":"","tags":[]},
						{"regular-title":"4444Tiny SVG Panning And Zooming Plugin With jQuery - Zoom SVG","regular-body":"","tags":[]},
						{"regular-title":"4444Tiny SVG Panning And Zooming Plugin With jQuery - Zoom SVG","regular-body":"","tags":[]},
						]};*/
			
			data=tumblr_api_read;
			$.each(data.posts, function(i,posts){ 
				
				// Remove any HTML tags
				var title = posts['regular-title'].replace(/<\/?[^>]+>/gi, '');
				//var title =	tumblr_api_read;
				// Calculate the human-readable relative time
				var time = $.timeago( new Date( posts['unix-timestamp'] * 1000 ) );
				
				postList.append('<li class="tn-post">\
									<a href="' + posts.url + '" target="_blank">'+title+' </a>\
									<span>' + time + '</span>\
								</li>');
			});
	
			// Show the first 5 news items
	
			postList.find('li')
					.slice(0,4)
					.show()
					.last()
					.addClass('no-border');

			// Rotate posts every settings.time ms 
			// (only if they are more than the limit)
			
			if(data.posts.length > 4) {
					
				setInterval(function() {
						
					var posts =  postList.find('li');
					
					posts.first().slideUp('slow', function() {
						$(this).appendTo(postList);
					})
					
					posts.eq(3).removeClass('no-border');
					posts.eq(4).slideDown('slow').addClass('no-border');
							
				}, settings.time);
				
			}
			
		//});
		
	 	return this;
	 	
	};
})(jQuery);