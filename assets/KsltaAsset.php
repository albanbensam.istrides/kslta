<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */

class KsltaAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
   		'frontend/web/css/site.css',   
   		'frontend/web/css/bootstrap.css',  
        'frontend/web/css/fonts/font-awesome-4.1.0/css/font-awesome.min.css',
        'frontend/web/css/own/owl.carousel.css',
        'frontend/web/css/own/owl.theme.css',
        'frontend/web/css/jquery.bxslider.css',
        'frontend/web/css/jquery.jscrollpane.css',
        'frontend/web/css//minislide/flexslider.css',
        'frontend/web/css/component.css',
        'frontend/web/css/prettyPhoto.css',
        'frontend/web/css/style_dir.css',
        'frontend/web/css/responsive.css',
        'frontend/web/css/animate.css',
    ];
    public $js = [
    	'frontend/web/js/jquery-1.10.2.js',
        'frontend/web/js/jquery-migrate-1.2.1.min.js',
        'frontend/web/js/jquery.transit.min.js',
        'frontend/web/js/menu/modernizr.custom.js',
        'frontend/web/js/menu/cbpHorizontalMenu.js',
        'frontend/web/js/minislide/jquery.flexslider.js',
        'frontend/web/js/circle/jquery-asPieProgress.js',
        'frontend/web/js/circle/rainbow.min.js',
        'frontend/web/js/gallery/jquery.prettyPhoto.js',
        'frontend/web/js/gallery/isotope.js',
        'frontend/web/js/jquery.ui.totop.js',
        'frontend/web/js/custom.js',
        'frontend/web/js/jquery.bxslider.js',
        'frontend/web/js/jquery.easing.1.3.js',
        'frontend/web/js/jquery.mousewheel.js',
        'frontend/web/js/own/owl.carousel.js',
        'frontend/web/js/jquery.countdown.js',
        'frontend/web/js/custom_ini.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}

