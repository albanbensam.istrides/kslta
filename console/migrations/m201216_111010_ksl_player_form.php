<?php

use yii\db\Migration;

/**
 * Class m201216_111010_ksl_player_form
 */
class m201216_111010_ksl_player_form extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201216_111010_ksl_player_form cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201216_111010_ksl_player_form cannot be reverted.\n";

        return false;
    }
    */
}
